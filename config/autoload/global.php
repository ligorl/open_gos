<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'db' => array(
        'driver' => 'pdo_mysql',
        'dsn' => 'mysql:dbname=f11_mon;host=localhost',
        'username' => 'root',
//        'password' => 'cDFngGHB',
        'password' => '',
        'charset' => 'utf8',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
        //PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARSET 'utf8'",
        //PDO::MYSQL_ATTR_INIT_COMMAND => "set_client='utf8'",
        //PDO::MYSQL_ATTR_INIT_COMMAND => "set character_set_results='utf8'",
        //PDO::MYSQL_ATTR_INIT_COMMAND => "SET collation_connection = 'utf8_general_ci",
        ),
        //подключение к базе рееста
        'adapters' => [
            'db_reestr' => [
                'driver' => 'pdo_mysql',
                'dsn' => 'mysql:dbname=reestr_new;host=localhost',
                'username' => 'root',
//                'password' => 'cDFngGHB',
                'password' => '',
                'charset' => 'utf8',
                'driver_options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                ]
            ],
            'lic_reestr' => [
                'driver' => 'pdo_mysql',
                'dsn' => 'mysql:dbname=lic_reestr;host=localhost',
                'username' => 'root',
//                'password' => 'cDFngGHB',
                'password' => '',
                'charset' => 'utf8',
                'driver_options' => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                ]
            ],
        ],
    ),
    //отправка сообщений в задачу
    'reestr_prostoy' => [
        'id' => '805683',
        'password' => 'a2329ea76edf9e23fd1e11ce98be7ee1',
    ],
    'lic_reestr_prostoy' => [
        'id' => '852306',
        'password' => '77a594f380e14085f361d9432ba204c6',
    ],
    //Подключение к бд с экспертами
    'db_experts' => array(
        'driver' => 'pdo_mysql',
        'dsn' => 'mysql:dbname=f11_mon_experts_open;host=localhost',
        'username' => 'root',
//        'password' => 'cDFngGHB',
        'password' => '',
        'charset' => 'utf8',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
        ),
    ),
    //подключение к ЛОД на сервере
    'db_lod' => array(
        'driver' => 'pdo_mysql',
        'dsn'            => 'mysql:dbname=f11_mon_lod_open;host=localhost',
        'username'       => 'root',
        'password'       => '',
        'charset'        => 'utf8',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
        ),
    ),
    /*
      'db' => array(
      'driver' => 'pdo_mysql',
      'dsn'            => 'mysql:dbname=f11_mon;host=localhost',
      //'dsn'            => 'mysql:dbname=f11_mon;host=192.168.0.35',
      'username'       => 'root',
      'password'       => 'cDFngGHB',
      'charset'        => 'utf8',
      'driver_options' => array(
      PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
      //PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARSET 'utf8'",
      //PDO::MYSQL_ATTR_INIT_COMMAND => "set_client='utf8'",
      //PDO::MYSQL_ATTR_INIT_COMMAND => "set character_set_results='utf8'",
      //PDO::MYSQL_ATTR_INIT_COMMAND => "SET collation_connection = 'utf8_general_ci",
      ),
      ),
      /*
      'db' => array(
      'driver' => 'pdo_sqlsrv',
      'dsn'            => 'sqlsrv:Server=WIN-OMNUFPIERNB;Database=ISGA',
      'username'       => 'dbuser',
      'password'       => 'cFkn17b9pO',
      'options' => array('buffer_results' => true)
      ), */
    /*
      'db' => array(
      'driver'         => 'Pdo',
      'dsn'            => 'odbc:host=sqlServer12;dbname=ISGA',
      'charset'        =>  'UTF-8',
      'username'       => 'dbuser',
      'password'       => 'cFkn17b9pO',
      'pdotype'       => 'odbc',
      ), */
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Db\Adapter\AdapterAbstractServiceFactory',
        ),
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
);
