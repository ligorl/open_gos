/*
Добавление поля кол-во разрешенных к загрузке файлов
 */

ALTER TABLE `isga_users` ADD `max_count_upload_files` INT NOT NULL DEFAULT '5' AFTER `Source`;
