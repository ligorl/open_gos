<?php
header("Content-Type: text/html; charset=UTF-8");
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors',1);
mb_internal_encoding('UTF-8');


//exec('../syncram.sh');
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

if (! function_exists('array_column')) {
    /*
    *Функция доступна только в php 5.5 однако очень удобна в использовании, 
    * позволяет получить одномерный масив состоящий из значений выбранного ключа 
    * 
    */
    function array_column(array $input, $columnKey, $indexKey = null)
    {
        $array = array();
        foreach ($input as $value) {
            if (!array_key_exists($columnKey, $value)) {
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            } else {
                if (!array_key_exists($indexKey, $value)) {
                    return false;
                }
                if (!is_scalar($value[$indexKey])) {
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}
// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
//phpinfo();die;
include_once 'init_autoloader.php';

use Illuminate\Database\Capsule\Manager;

/** @var \Interop\Container\ContainerInterface $container */
$container = require __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'autoload'.DIRECTORY_SEPARATOR.'eloquent.global.php';
//var_dump($container);exit();
$capsule = new Manager();
//$capsule->getConnection($container->get('config'));
//var_dump($container['eloquent']);
//exit();
foreach ($container['eloquent'] as $name => $connect){
    $capsule->addConnection($connect,$name);
}
//$capsule->addConnection($container->get('config')['redmine'],'redmine');
//$capsule->addConnection($container->get('config')['worked_time'],'worked_time');
$capsule->setAsGlobal();
$capsule->bootEloquent();

//$libDir = $zf2Path;
//print_r($libDir);die;


// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
