<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">
<xsl:template match="/">
  <html>
  <body>
  <style>
  .tablexls td{border:1px solid #ddd;padding:5px;}
  .tablexls{margin-bottom:10px;}
  .tablexls th,.headth{background:#3f6bb3 ;color:#fff}
  .qr{float:left}
  </style>
  <img class='qr' src='qr.png' />
  <h2>СВИДЕТЕЛЬСТВО О ГОСУДАРСТВЕННОЙ АККРЕДИТАЦИИ</h2>
    <table class="tablexls" border="1">
      <tr style="background:#9acd32">
        <th>Наименование показателя</th>
        <th>Значение показателя</th>
      </tr>
	  <tr>
		<td>Наименование аккредитационного органа</td>
		<td><xsl:value-of select="Certificate/ControlOrgan"/></td>
	  </tr>
	  <tr>
		<td>№ свидетельства</td>
		<td><xsl:value-of select="Certificate/RegNumber"/></td>
	  </tr>
	  <tr>
		<td>Дата свидетельства</td>
		<td><xsl:value-of select="Certificate/IssueDate"/></td>
	  </tr>
	  <tr>
		<td>Настоящее свидетельство выдано</td>
		<td><xsl:value-of select="Certificate/EduOrgFullName"/></td>
	  </tr>
	  <tr>
		<td>Место нахождения юридического лица, место жительства для индивидуального предпринимателя о государственной аккредитации образовательной деятельности по основным общеобразовательным программам в отношении каждого уровня общего образования, указанным в приложении к настоящему свидетельству</td>
		<td><xsl:value-of select="Certificate/PostAddress"/></td>
	  </tr>
	  <tr>
		<td>Основной государственный регистрационный номер юридического лица (индивидуального предпринимателя) (ОГРН)</td>
		<td><xsl:value-of select="Certificate/OGRN"/></td>
	  </tr>
	   <tr>
		<td>Идентификационный номер налогоплательщика</td>
		<td><xsl:value-of select="Certificate/INN"/></td>
	  </tr>
	  <tr>
		<td>Срок действия свидетельства до</td>
		<td><xsl:value-of select="Certificate/EndDate"/></td>
	  </tr>
	  
	  <tr>
		<td>Должность уполномоченного лица</td>
		<td><xsl:value-of select="Certificate/HeadPost"/></td>
	  </tr>
	  <tr>
		<td>ФИО уполномоченного лица</td>
		<td><xsl:value-of select="Certificate/HeadName"/></td>
	  </tr>
	  <tr>
		<td>Серия</td>
		<td><xsl:value-of select="Certificate/SerialNumber"/></td>
	  </tr>
	  <tr>
		<td>Номер</td>
		<td><xsl:value-of select="Certificate/FormNumber"/></td>
	  </tr>
     
    </table>
	
	<i>Настоящее свидетельство имеет приложение (приложения), являющееся его неотъемлемой частью. Свидетельство без приложения (приложений) недействительно.</i>
	
	<xsl:for-each select="Certificate/Supplements/Supplement">
		<table class="tablexls" border="1">
			<tr style="background:#9acd32">
				<th>Наименование показателя</th>
				<th>Значение показателя</th>
			</tr>
			<tr>
				<td>Полное наименование юридического лица или его филиала</td>
				<td><xsl:value-of select="EduOrgFullName"/></td>
			</tr>
			<tr>
				<td>Место нахождения юридического лица или его филиала</td>
				<td><xsl:value-of select="PostAddress"/></td>
			</tr>
			<tr>
				<td>Распорядительный документ аккредитационного органа о государственной аккредитации</td>
				<td><xsl:value-of select="AccredDoc"/></td>
			</tr>
			<tr>
				<td>Распорядительный документ аккредитационного органа о переоформлении свидетельства о государственной аккредитации</td>
				<td><xsl:value-of select="AccredDocRestruct"/></td>
			</tr>
		</table>
		<xsl:for-each select="EducationalPrograms/EducationalProgram">
		<br /><br />
			<table class="tablexls" border="1">
				<tr class='headth'><td colspan='3' style="text-align:center;"><h4><xsl:value-of select="EduLevelName"/></h4></td></tr>
				<tr style="background:#9acd32">
					<th>Коды укрупненных групп профессий, специальностей и направлений подготовки профессионального образования</th>
					<th>Наименования укрупненных групп профессий, специальностей и направлений подготовки профессионального образования</th>
					<th>Уровень образования</th>
				</tr>
				<xsl:for-each select="Programms/Programm">
					<tr>
						<td><xsl:value-of select="ProgrammCode"/></td>
						<td><xsl:value-of select="ProgrammName"/></td>
						<td><xsl:value-of select="EduLevelShortName"/></td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:for-each>
	</xsl:for-each>
	
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>