console.log('загрузка index.page.js');
var serverLink = null;
var filter = {};
var contentColumns = 1;
var sort = "";
var procAnswerGlobal = null;

var afterServerQueryFunction = function () {
};
/**
 * Функция вешает обработчики на пагинацию
 * @private
 */
_paginationHandler = function () {
    $("body").undelegate(".route-table .content .page", "click");
    $("body").delegate(".route-table .content .page", "click", function () {
        var page = $(this).attr("page");
        _serverQuery(page);
    });
};
/**
 * Функция обрабатывает запросы к серверу
 * @param page - страница
 * @param procAnswer - Каллбек на ответ
 * @private
 */
_serverQuery = function (page, procAnswer) {
    $.ajax({
        type: 'POST',
        url: serverLink,
        data: {
            filter: filter,
            page: page,
            width: $(window).width(),
            sort: sort
        },
        beforeSend: function () {
            //отображаем крутилку
            $(".content_td tbody.content").html("").append(
                    $("<tr><td colspan='" + contentColumns + "'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
                    ).fadeIn(500);
        },
        success: function (answer) {
            if (typeof (procAnswer) == 'function') {
                procAnswer(answer);
            }
            $(".content_td tbody.content .loading-img").fadeOut(500, function () {
                answer = $(answer)
                $(".content_td tbody.content").html("").append($(answer).find(".content > tr"));
                $("#pagination").html("").append($(answer).find("#pagination span"));
                $("#report-generate-count-span").html("").append($(answer).find("#report-generate-count-span"));
                _paginationHandler();
                afterServerQueryFunction();
                resizeContent($(".container .viewzone"));
                //if(serverLink === '/islod/index/licenses-page'){
                //    license.applyStateStyle();
                //}
                if (typeof (procAnswerGlobal) == 'function') {
                    procAnswerGlobal(answer);
                }

            });
            //дергним счетчики
            getCount();
        },
        error: function(e){
            $(".content_td tbody.content .loading-img").fadeOut(500, function () {
                
            });
            //сбросим сообщения
            useTopHoverTitle("",1);
            useTopHoverTitleError("Ошибка связи");
            console.error('ошибка связи при формированиии');
            console.log(e);
            console.log(e.responseText);            
        }
    });
};


/**
 * Обработчики фильтра и пагинации для новых заявлений
 */
indexNewHandlers = function () {
    console.log('лицензии - новые - добавляем обработчеки ');
    //Инициализируем фильтр
    serverLink = "/eo/license/new-page";
    //Количество колонок в таблице
    contentColumns = 3;
    //вешаем обработчики пагинации
    changeSortField(".content_td .content .viewzone", true);
    filter = {};
    _paginationHandler();

    procAnswerGlobal = function(answer){
        newRequestClickAction(answer);
        $(".date-field").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy",
        });
        jQuery(".date-field").datepicker("option", "maxDate", '');
        jQuery(".date-field").datepicker("option", "minDate", '');
        /**
         * ограничем по дате окончания
         */
        jQuery('input.SearchDate[name="dateIssueBegin"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueEnd"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "maxDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате начала
         */
        jQuery('input.SearchDate[name="dateIssueEnd"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueBegin"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "minDate", elementFromValue);
                }
        );
    }

    $("body").off( "click", ".license-filter .btApply");
    $("body").on("click", ".license-filter .btApply", function () {

        filter = {
            "licenseProcedure": $(".licenseProcedure", "#filter").val() || '',
            "licenseReason": $(".licenseReason", "#filter").val() || '',
            "fillingDateBegin": $(".fillingDateBegin", "#filter").val() || '',
            "fillingDateEnd": $(".fillingDateEnd", "#filter").val() || ''

        };
        _serverQuery(1);
    });
   //Делаем запрос данных
   _serverQuery(1);
};



/**
 * Обработчики фильтра и пагинации для все дела
 */
indexAllDealHandlers = function () {
    console.log('лицензии - все дела - добавляем обработчеки ');
    //Инициализируем фильтр
    serverLink = "/eo/license/all-deal-page";
    //Количество колонок в таблице
    contentColumns = 5;
    //вешаем обработчики пагинации
    changeSortField(".content_td .content .viewzone", true);
    filter = {};
    _paginationHandler();

    procAnswerGlobal = function(answer){
        //newRequestClickAction(answer);
        $(".date-field").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy",
        });
        jQuery(".date-field").datepicker("option", "maxDate", '');
        jQuery(".date-field").datepicker("option", "minDate", '');
        /**
         * ограничем по дате окончания
         */
        jQuery('input.SearchDate[name="dateIssueBegin"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueEnd"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "maxDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате начала
         */
        jQuery('input.SearchDate[name="dateIssueEnd"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueBegin"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "minDate", elementFromValue);
                }
        );
    }

    $("body").off( "click", ".license-filter .btApply");
    $("body").on("click", ".license-filter .btApply", function () {
        filter = {           
            "fillingDealStatus":    $(".fillingDealStatus", "#filter").val() || '',
            "fillingDealNumber":    $(".fillingDealNumber", "#filter").val() || '',
            "licenseProcedure":     $(".licenseProcedure", "#filter").val() || '',
            "licenseReason":        $(".licenseReason", "#filter").val() || '',
            "fillingDateBegin":     $(".fillingDateBegin", "#filter").val() || '',
            "fillingDateEnd":       $(".fillingDateEnd", "#filter").val() || ''
        };
        _serverQuery(1);
    });
   //Делаем запрос данных
   _serverQuery(1);
};






/**
 * Обработчики фильтра и пагинации для новых заявлений
 */
indexSendedHandlers = function () {
    //Инициализируем фильтр
    serverLink = "/eo/license/sended-page";
    //Количество колонок в таблице
    contentColumns = 7;
    //вешаем обработчики пагинации
    changeSortField(".content_td .content .viewzone", true);
    filter = {};    
    _paginationHandler();

    procAnswerGlobal = function(answer){
        //newRequestClickAction(answer);
        sendedRequestClickAction(answer);
        $(".date-field").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy",
        });
        /**
         * ограничем по дате окончания
         */
        jQuery('input.SearchDate[name="dateIssueBegin"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueEnd"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "maxDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате начала
         */
        jQuery('input.SearchDate[name="dateIssueEnd"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueBegin"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "minDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате окончания
         */
        jQuery('input.SearchDate[name="dateSubmissionBegin"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateSubmissionEnd"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "maxDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате начала
         */
        jQuery('input.SearchDate[name="dateSubmissionEnd"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateSubmissionBegin"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "minDate", elementFromValue);
                }
        );
    }

    $("body").off( "click", ".license-filter .btApply");
    $("body").on("click", ".license-filter .btApply", function () {

        filter = {
            "licenseProcedure": $(".licenseProcedure", "#filter").val() || '',
            "licenseReason": $(".licenseReason", "#filter").val() || '',
            "fillingDateBegin": $(".fillingDateBegin", "#filter").val() || '',
            "fillingDateEnd": $(".fillingDateEnd", "#filter").val() || '',
            "submissionDateBegin": $(".SubmissionDateBegin", "#filter").val() || '',
            "submissionDateEnd": $(".SubmissionDateEnd", "#filter").val() || ''

        };
        _serverQuery(1);
    });
   //Делаем запрос данных
   _serverQuery(1);
};

/**
 * Обработчики фильтра и пагинации для новых заявлений
 */
indexReplyHandlers = function () {
    //Инициализируем фильтр
    serverLink = "/eo/license/reply-page";
    //Количество колонок в таблице
    contentColumns = 7;
    //вешаем обработчики пагинации
    changeSortField(".content_td .content .viewzone", true);
    filter = {};
    _paginationHandler();

    procAnswerGlobal = function(answer){
        //newRequestClickAction(answer);
        replydRequestClickAction(answer);
        $(".date-field").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy",
        });
        /**
         * ограничем по дате окончания
         */
        jQuery('input.SearchDate[name="dateIssueBegin"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueEnd"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "maxDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате начала
         */
        jQuery('input.SearchDate[name="dateIssueEnd"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateIssueBegin"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "minDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате окончания
         */
        jQuery('input.SearchDate[name="dateSubmissionBegin"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateSubmissionEnd"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "maxDate", elementFromValue);
                }
        );
        /**
         * ограничем по дате начала
         */
        jQuery('input.SearchDate[name="dateSubmissionEnd"]').datepicker(
                'option',
                'beforeShow',
                function ( input, inst ){
                    var elementFromChange = jQuery('input.SearchDate[name="dateSubmissionBegin"]');
                    var elementFromValue  = jQuery(elementFromChange).val();
                    jQuery(input).datepicker("option", "minDate", elementFromValue);
                }
        );
    }

    $("body").off( "click", ".license-filter .btApply");
    $("body").on("click", ".license-filter .btApply", function () {

        filter = {
            "licenseProcedure": $(".licenseProcedure", "#filter").val() || '',
            "licenseReason": $(".licenseReason", "#filter").val() || '',
            "fillingDateBegin": $(".fillingDateBegin", "#filter").val() || '',
            "fillingDateEnd": $(".fillingDateEnd", "#filter").val() || '',
            "submissionDateBegin": $(".SubmissionDateBegin", "#filter").val() || '',
            "submissionDateEnd": $(".SubmissionDateEnd", "#filter").val() || ''

        };
        _serverQuery(1);
    });
   //Делаем запрос данных
   _serverQuery(1);
};

indexNotifyHandlers = function () {
    //Инициализируем фильтр
    serverLink = "/eo/license/notify-page";
    //Количество колонок в таблице
    contentColumns = 7;
    //вешаем обработчики пагинации
    changeSortField(".content_td .content .viewzone", true);
    filter = {};
    _paginationHandler();
    sendedRequestClickAction = function(answer){
        console.log('добавим обработчик тыка по табличке');
        $('table.intable.license_table.license-table-content tbody tr ').undelegate('td','click');
        $('table.intable.license_table.license-table-content tbody tr ').delegate('td','click', function(){
            console.log('был тыка по табличке');
            $('table.intable.license_table.license-table-content tbody tr').removeClass('select');
            var tr = jQuery(this).parents('tr')[0];
            jQuery(tr).addClass('select');
        });
        $('table.intable.license_table.license-table-content tbody tr ').undelegate('td','dblclick');
        $('table.intable.license_table.license-table-content tbody tr ').delegate('td','dblclick', function(){
            console.log('был  тык-тык тыка по табличке');
            $('table.intable.license_table.license-table-content tbody tr').removeClass('select');
            var tr = jQuery(this).parents('tr')[0];
            jQuery(tr).addClass('select');
           // newRequestOpenAction(this);
        });
    }

    procAnswerGlobal = function(answer){
        //newRequestClickAction(answer);
        //notifyRequestClickAction(answer);
        $(".date-field").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy",
        });
    }

    $("body").off( "click", ".license-filter .btApply");
    $("body").on("click", ".license-filter .btApply", function () {
        filter = {
            "documentTypeId": $(".documentTypeCatalog", "#filter").val() || '',
            "registrationNumber": $(".registrationNumber", "#filter").val() || ''
        };
        _serverQuery(1);
    });

    $("body").undelegate("input.Search", "keyup");
    $("body").delegate("input.Search", "keyup", function (eventObject) {
        if (eventObject.keyCode == 13) {            
            jQuery('.btApply', '#filter').click();
        }
    });                

   //Делаем запрос данных
   _serverQuery(1);
};



/**
 * Обработчики фильтра и пагинации для /#certificates
 */
/*
indexLicensesHandlers = function () {
    //Инициализируем фильтр
    serverLink = "/islod/index/licenses-page";
    //Количество колонок в таблице
    contentColumns = 6;
    //вешаем обработчики пагинации
    changeSortField(".content_td .content .viewzone", true);
    _paginationHandler();
    $("body").undelegate(".license_table.intable .content tr", "dblclick");
    $("body").delegate(".license_table.intable .content tr", "dblclick", function () {
        var id = $(this).attr("id").split("_")[1];
        var title = $(this).attr("dialog-title");
        license._get_license_info(id, title);
    });

    $('body').off('click','.inventory-button');
    $('body').on('click','.inventory-button',function(){
        var selectorName = 'inventory-info-dialog',
            page = _getPage().inventory(),
            title = 'Формирование описи лицензий и приложений',
            height = Math.round(screen.height * 0.87);

        if( page){
            createStandartDialog({
               className : selectorName,
               data : page,
               title : title,
               height: 250,
			   width: 550,
            });
        }
    });

    $('body').off('click','.inventory-and-supplement-button');
    $('body').on('click','.inventory-and-supplement-button',function(){
        var selectorName = 'inventory-info-dialog',
            page = _getPage().inventoryAndUser(),
            title = 'Формирование описи лицензий и приложений',
            height = Math.round(screen.height * 0.87);

        if( page){
            createStandartDialog({
               className : selectorName,
               data : page,
               title : title,
               height: 250,
			   width: 550,
            });
        }
    });

    $("body").off( "click", ".license-filter .btApply");
    $("body").on("click", ".license-filter .btApply", function () {

        filter = {
            "EONameAndEoInn": $(".EONameAndEoInn", "#filter").val(),
            "LicenseRegNum": $(".LicenseRegNum", "#filter").val(),
            "Region": $(".Region", "#filter").val(),
            "State": $(".sState", "#filter").val(),
            "dtDateStart": $(".dtDateStart", "#filter").val(),
            "dtDateEnd": $(".dtDateEnd", "#filter").val()

        };
        _serverQuery(1);
    });

    $("body").undelegate(".license-filter", "keyup");
    $("body").delegate(".license-filter", "keyup", function ( eventObject) {
         if(  eventObject.keyCode == 13){
            filter = {
                "EONameAndEoInn": $(".EONameAndEoInn", "#filter").val(),
                "LicenseRegNum": $(".LicenseRegNum", "#filter").val(),
                "Region": $(".Region", "#filter").val(),
                "State": $(".sState", "#filter").val(),
                "dtDateStart": $(".dtDateStart", "#filter").val(),
                "dtDateEnd": $(".dtDateEnd", "#filter").val()

            };
            _serverQuery(1);
         }

    });

    $(".date-field").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy"
    });
    license.applyStateStyle();
    //Отправка лицензий в реестр
    $('body').undelegate('[data-action="sendToReestr"]', 'click');
    $('body').delegate('[data-action="sendToReestr"]', 'click', function () {
        var dialogTitle = 'Отправка лицензий в реестр';
        license.showSendToReestrDialog(dialogTitle);
    });
};
*/
/*
indexOrgsHandlers = function () {
    //Инициализируем фильтр
    filter = {
        "Search": $(".Search", "#filter").val(),
        "Region": $(".Region", "#filter").val(),
        "Branch": $("select.Branch").val(),
        "lod_stateCode" : $("#filter .lod_stateCode").val(),
    };
    //Ссылка на скрипт сервера
    serverLink = "/islod/index/orgs-page";
    //Количество колонок в таблице
    contentColumns = 4;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function () {
        //вешаем обработчик открытия подробной инфо о сертификатах
        OrganizationListBinder().unbind();
        OrganizationListBinder().bind();
    }
    afterServerQueryFunction();
    changeSortField(".content_td .content .viewzone", true);
    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаем обработчик открытия подробной инфо о сертификатах
     //$("tr",".ron-orgs").click(function(){
     //var id = $(this).attr("id").split("_")[1];
     //var title = $(this).attr("dialog-title");
     //_orgDialog(id, title);
     //});


}
*/
/*
indexDocumentsHandlers = function () {
    //Инициализируем фильтр
    filter = {
        "number": $(".regNumber", "#filter").val(),
        "dtDateStart": $(".dtDateStart", "#filter").val(),
        "dtDateEnd": $(".dtDateEnd", "#filter").val()
    };
    //Ссылка на скрипт сервера
    serverLink = "/islod/index/documents";
    //Количество колонок в таблице
    contentColumns = 4;
    afterServerQueryFunction();
    //вешаем обработчики пагинации
    _paginationHandler();
    $("body").undelegate(".btApply", "click");
    $("body").delegate(".btApply", "click", function (eventObject) {
        filter = {
            "number": $(".regNumber", "#filter").val(),
            "documentType":$(".documentType", "#filter").val(),
            "dtDateStart": $(".dtDateStart", "#filter").val(),
            "dtDateEnd": $(".dtDateEnd", "#filter").val()
        };
        _serverQuery(1);
    });
    $("body").undelegate(".documents-filter", "keyup");
    $("body").delegate(".documents-filter", "keyup", function ( eventObject) {
         if(  eventObject.keyCode == 13){
            filter = {
                "number": $(".regNumber", "#filter").val(),
                "documentType":$(".documentType", "#filter").val(),
                "dtDateStart": $(".dtDateStart", "#filter").val(),
                "dtDateEnd": $(".dtDateEnd", "#filter").val()
            };
            _serverQuery(1);
         }

    });
    $(".date-field").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy"
    });
    $('body').undelegate('.document-row', 'click');
    $('body').delegate('.document-row', 'click', function (event) {
        if (event.target.nodeName != 'SPAN') {
            affairs.showDocumentInfo($(this).attr("document-id"), $(this).attr("title"));
        }
    });


}
*/
function filterReset(){
    $("#filter input, #filter select ").each(function( i,el){
        $( el).val('');
    });
    $("#filter .btApply").click();
}

$('document').ready(function(){
    /**
     *  костыль для combobox - скрывает selectmenu при прокрутке страницы
     */
    document.addEventListener('wheel',function(e){

        if(  $('.ui-autocomplete').length >0){
            var cy = e.clientY,
                cx = e.clientX,
                scrollAnotherEl = true;

            $('.ui-autocomplete').each(function(i , el){
                var x = $(el).position().left ,
                    y = $(el).position().top ,
                    h = $(el).height(),
                    w = $(el).width();

                    if(
                            cy >= y &&
                            cx >= x &&
                            cy <= y+h &&
                            cx <= x+w
                    ){
                        scrollAnotherEl = false;
                    }
            });

            if( scrollAnotherEl){
                $('.ui-autocomplete').hide();
            }
        }
    })
});

/**
 * обработчик и навешивание событий для вкладке дел на главной странице, по сути дубль affairs.page.js но выхода другого нет (
 */
function indexAffairHandlers(container) {

    _paginationHandler = function () {
        $("body").undelegate(".page", "click");
        $("body").delegate(".page", "click", function () {
            searchApplication(this);
        });
    };
        function initFilterHandlers(container) {
        //Обработчик смены типа процедуры
        $("select.SearchType", container).change(function(){
            var value = $(this).val();
        $(".SearchReasons", container).val("0");
            //Если выбрали "Не выбрано" отображаем все причины
        if(value == "0"){
        $(".SearchReasons option", container).show();
        } else {
        //Прячем причины в списке
            $(".SearchReasons option", container).hide();
            $(".SearchReasons option[value=0]", container).show();
        $(".SearchReasons option[parentId="+value+"]", container).show();
        }
    });

    //Обработчик кнопки Сбросить
            $(".btClear", container).click(function(){
            $(".SearchOrg",container).val("");
        $(".dtDateStart",container).val("");
        $(".dtDateEnd",container).val("");
        $(".SearchRegNumber",container).val("");
        $(".SearchType",container).val("0");
        $(".SearchReasons",container).val("0");
    });

        //обработчик нажатия на кнопку ентер
            $(".SearchOrg, .SearchRegNumber",container).keypress(function(e){
        if(e.keyCode==13){
$(".btApply", container).click();
        }
    });
}
$(document).ready(function () {
    _paginationHandler();
    $("body").undelegate(".btApply", "click");
    $("body").delegate(".btApply", "click", function () {
        searchApplication();
    });

    $("body").delegate("input.Search", "keyup", function (eventObject) {
        if (eventObject.keyCode == 13) {
            searchApplication();
        }
    });
    $("body").undelegate(".row_appl", "click")
    $('body').delegate('.row_appl', 'click', function () {
        $('.row_appl').removeClass('active_appl');
        $(this).addClass('active_appl');
    });
    $("body").undelegate(".row_deal", "click")
    $('body').delegate('.row_deal', 'click', function () {
        $('.row_deal').removeClass('active_deal');
        $(this).addClass('active_deal');
    });

    $('body').undelegate('.active_deal', 'click');
    $('body').delegate('.active_deal', 'click', function () {
       affairs.openEditAffair();
    });

    $('body').undelegate('#procedureSelect', 'change');
    $('body').delegate('#procedureSelect', 'change', function () {
       affairs.getReasonForProcedure($(this).val());
    });

    initFilterHandlers();
});
}

 function searchApplication(_this) {

        var page = $(_this).attr('page');

        if (page === undefined) {
            page = 1;
        }
        var event = $(".btn-warning").attr('event') + '/' + page;
        var keep_selector = $(".btn-warning").attr('controller');
        var destination_link = '/eo/' + keep_selector + '/' + event;
        $.ajax({
            type: 'POST',
            url: destination_link,
            data: $("#filterForm").serialize(),
            beforeSend: function () {
                $(".content_td .intable_content").html("").append($("<tr><td colspan='6'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")).fadeIn(500);
            },
            success: function (answer) {
                $(".content_td .intable_content").fadeOut(500, function () {
                    answer = $(answer);
                    $(".content_td .content").html("").append(answer);
                    resizeContent($(".container .viewzone"));
                    specialTableResize();
                    _paginationHandler();
                    //Запускаем обработчики
                    var handlerName = keep_selector + event.charAt(0).toUpperCase() + event.substr(1) + "Handlers";
                    if (window[handlerName])
                        window[handlerName]();
                    affairs.getReasonForProcedure($("#procedureSelect").val(), $("#reasonHiddenInput").val());
                });
            }
        });
    }


 /**
 * дергаем счетчики
 *
 * @returns {undefined}
 */
function getCount(){
    console.log('дергаем счетчики');
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: "/eo/license/get-count",
                //data: data,
                beforeSend: function(){
                },
                success: function (answer) {
                    if( 'undefined' == typeof(answer.result) || 'Ok'!= answer.result ){
                        console.error('лицензирование - получаем счетчики - ответ некоректен');
                        console.log(answer);
                    }
                    else{
                        if( 'object' == typeof(answer.data) || {} == answer.data ){
                            for( var i in answer.data ){
                                var count = answer.data[i];
                                jQuery('ul.steps_div li.btn a span.num[step="'+i+'"] ').text(count);
                            }
                        }
                        else{
                            console.error('лицензирование - получаем счетчики - ответ пуст');
                            console.log(answer);
                        }
                    }
                },
                error:function(e){
                    //сбросим сообщения
                    useTopHoverTitle("",1);
                    useTopHoverTitleError("Ошибка связи");
                    console.error('ошибка связи при переходе при удалении');
                    console.log(e);
                    console.log(e.responseText);
                }
            });
}


    /**
     * выводим дилог редактирования заявления
     * @returns {undefined}
     */
    function newRequestOpenAction(){
        console.log('котим открыть заявление');
        if($('table.intable.license_table.license-table-content tbody tr.select').length){
            var reqId = $('table.intable.license_table.license-table-content tbody tr.select').attr('requetsId');
            if ( 'undefined' == typeof(reqId) ){
                console.log('открытай - лицензироканий - в строке не указано заявление');
                return 0;
            }            
            if ( '' == reqId){
                useTopHoverTitleError('Выберите заявление');
            }
            else{
                request.openEdit(reqId);
            }
        }
        else {
            useTopHoverTitleError('Выберите заявление');
        }
    }
    
    

console.log('загрузка index.page.js закончена');


