/**
 * Класс для работы с фреймами для создания/редактирования заявления
 * @constructor
 */
var RequestFrames = function(){
    var self = this;
    /**=============================================================
     * Приватные данные
     *=============================================================*/
    _private = {};
    _private.container = null;
    _private.content_container = null;
    _private.buttons_container = null;

    _private.data = {
        OPData:{
            leftLevelList: {}
        }
    };
    _private.data.noSave = true;

    _private.data.saveProcess = {
        refreshDeal: false,
        isSave : false,
        frames: {},
        frameNameByTag: {
            "filial": "Филиал",
            "newFilial": "Открытие нового филиала",
            "changeLicTitle": "Изменение наименования лицензиата",
            "newOP": "Новые ОП",
            "currentOP": "Действующие ОП",
            "stopOP": "Прекращение реализации ОП",
            "changeOpName": "Изменение наименования ОП",
            "placesOD": "Места осуществления ОД",
            "openOD": "Открытие ОД по адресам",
            "stopOD": "Прекращение ОД по адресам",
            "reorgLic": "Реорганизация",
            "reorg": "Реорганизация",
            "general" : "Свойства заявления"
        }
    };

    _private.frames = {};
    _private.frames.active = {}; //Список всех фреймов в диалоге
    _private.frames.current = "general"; //Фрейм что активен в текущий момент

    /**=============================================================
     * Приватные методы
     *=============================================================*/

    /**
     * Метод инициализирует общие обработчики, типа переходов по вкладкам,
     */
    _private.initGeneralHandlers = function(){
        //Вешаем обработчик на закрытие
        $(".btClose", _private.container).click(function(){
            $(_private.container).remove();
            if(_private.data.saveProcess.refreshDeal){
                //рефрешим диалог с делом
                //закрыть дело
                jQuery('.dialog-deal-edit').remove();
                //открыть дело
                affairs.openEditAffair();
                //рефрешим списки в таблице
                $(".dealFilterWrap .btApply").click();
                //
                _private.data.saveProcess.refreshDeal = false;
            }
        });



        //Вешаем обработчик на кнопку сохранить
        $(".btSaveAll", _private.container).click(function(){
            //if( 'undefined' == typeof(btSaveAllFlag) || btSaveAllFlag ){
            //    return 0;
            //}
            //btSaveAllFlag = true;
            //хочу чтоб филиалы сохранялись толлько при тыке по кнопке,
            //а не когда  не пойми что сохранялку дергает
            _private.frames.filial.isSave = true;
            self.save();
            _private.frames.filial.isSave = false;
        });

        //Вешаем обработчик на переключение вкладок
        $(".tab", _private.container).click(function(){
            $(".tab", _private.container).removeClass("active");
            $(this).addClass("active");

            //Перед сменой пробуем сохранить текущий фрейм
            // _private.saveCurrentFrame();

            //Запоминаем какой фрейм активный
            _private.frames.current = $(this).attr("tab-container");

            //
            $(".content-block",_private.container).hide();
            $(".content-block."+$(this).attr("tab-container"),_private.container).show();

            $(".buttons-block",_private.container).hide();
            $(".buttons-block."+$(this).attr("tab-container"),_private.container).show();

            var selected_tab = $(this).attr("tab-container");

            //Если прописаны действия при переключении, выполняем их
            var currentFrame = null;

            switch(_private.frames.current){
                case "filial": currentFrame="filial";break;
                case "new-filial": currentFrame="newFilial";break;
                case "change-lic-title": currentFrame="changeLicTitle";break;
                case "new-op": currentFrame="newOP";break;
                case "current-op": currentFrame="currentOP";break;
                case "stop-op": currentFrame="stopOP";break;
                case "change-op-name": currentFrame="changeOpName";break;
                case "places-od": currentFrame="placesOD";break;
                case "open-od": currentFrame="openOD";break;
                case "stop-od": currentFrame="stopOD";break;
                case "reorg-lic": currentFrame="reorgLic";break;
                case "reorg": currentFrame="reorg";break;
            }
            if(currentFrame != null && _private.frames[currentFrame] && _private.frames[currentFrame].onTab) {
                _private.frames[currentFrame].onTab();
            }
        });

        //Устанавливаем высоту контейнера
        $(".content-container", _private.container ).height(
            $(".dialog-request-edit.ui-dialog-content").height()- $(".tabs-container",_private.container).height()-60//105
        );
        //Сохраняем ссылку на контент контейнер
        _private.content_container = $(".content-container", _private.container);
        //сохраняем ссылку на контейнер с кнопками
        _private.buttons_container = $(".buttons-container", _private.container);
        //Вешаем обработчики фреймов
        _private.initFrameHandlers();
    }

    /**
     * Метод инициализирует фрейсы что были подгружены
     */
    _private.initFrameHandlers = function(){
        if($(".content-block.general",_private.container).length > 0) {
            _private.frames.active.general = true;
            _private.frames.general.init();
        }
        if($(".content-block.filial",_private.container).length > 0) {
            _private.frames.active.filial = true;
            _private.frames.filial.init();
        }
        if($(".content-block.new-filial",_private.container).length > 0){
            _private.frames.active.newFilial = true;
            _private.frames.newFilial.init();
        }
        if($(".content-block.change-lic-title",_private.container).length > 0){
            _private.frames.active.changeLicTitle = true;
            _private.frames.changeLicTitle.init();
        }
        if($(".content-block.new-op",_private.container).length > 0) {
            _private.frames.active.newOP = true;
            _private.frames.newOP.init();
        }
        if($(".content-block.current-op",_private.container).length > 0) {
            _private.frames.active.currentOP = true;
            _private.frames.currentOP.init();
        }
        if($(".content-block.change-op-name",_private.container).length > 0) {
            _private.frames.active.changeOpName = true;
            _private.frames.changeOpName.init();
        }
        if($(".content-block.stop-op",_private.container).length > 0) {
            _private.frames.active.stopOP = true;
            _private.frames.stopOP.init();
        }
        if($(".content-block.places-od",_private.container).length > 0) {
            _private.frames.active.placesOD = true;
            _private.frames.placesOD.init();
        }
        if($(".content-block.open-od",_private.container).length > 0) {
            _private.frames.active.openOD = true;
            _private.frames.openOD.init();
        }
        if($(".content-block.stop-od",_private.container).length > 0) {
            _private.frames.active.stopOD = true;
            _private.frames.stopOD.init();
        }
        if($(".content-block.reorg-lic",_private.container).length > 0) {
            _private.frames.active.reorgLic = true;
            _private.frames.reorgLic.init();
        }
        if($(".content-block.reorg",_private.container).length > 0) {
            _private.frames.active.reorg = true;
            _private.frames.reorg.init();
        }
        if($(".content-block.document",_private.container).length > 0) {
            _private.frames.active.document = true;
            _private.frames.document.init();
        }
        _private.data.noSave = false;
    }

    /**
     * Метод отрабатывает сохранение текущего фрейма
     */
    _private.saveCurrentFrame = function(initFromButton){
        //Если пока не нужно обрабатывать сохранения
        if(_private.data.noSave)
            return;

        //Если надо сохранить фрейм с основными данными, но инициация прошла не по кнопке, то не обрабатываем сохранение
        if(_private.frames.current == "general" && !initFromButton)
            return;

        var currentFrame = null;

        switch(_private.frames.current){
            case "filial": currentFrame="filial";break;
            case "new-filial": currentFrame="newFilial";break;
            case "change-lic-title": currentFrame="changeLicTitle";break;
            case "new-op": currentFrame="newOP";break;
            case "current-op": currentFrame="currentOP";break;
            case "stop-op": currentFrame="stopOP";break;
            case "change-op-name": currentFrame="changeOpName";break;
            case "places-od": currentFrame="placesOD";break;
            case "open-od": currentFrame="openOD";break;
            case "stop-od": currentFrame="stopOD";break;
            case "reorg-lic": currentFrame="reorgLic";break;
            case "reorg": currentFrame="reorg";break;
        }
        if(initFromButton && _private.frames.current=="general")
            currentFrame="general";

        //Если метод сохранения есть для фрейма, то вызываем его
        if(currentFrame != null && _private.frames[currentFrame] && _private.frames[currentFrame].save){
            _private.frames[currentFrame].save(initFromButton);
        } else if(initFromButton && currentFrame != null && _private.frames[currentFrame] && !_private.frames[currentFrame].save){
            useTopHoverTitle("Успешно сохранено");
        }
    }

    /**
     * Метод проходит по всем активным фреймам и вызывает метод save если есть
     */
    _private.saveAllFrames = function(closeAfterSave){
        //Отображаем оверлей
        $(".loading-overlay").show();

        var saveProcess = _private.data.saveProcess;
        saveProcess.isSave = true;

        //Проценты сохранения
        var currentPartDone = 0;
        var allParts = 0;

        //Проходим по всем текущим фреймам и запускаем метод сохранения
        for(var frame in _private.frames.active){
            if(_private.frames[frame] && _private.frames[frame].save) {
                saveProcess.frames[frame] = {
                    answer: null,
                    error: null
                };
                _private.frames[frame].save();
                allParts++;
            }
        }

        //Вешаем по таймауту проверку на сохранение
        var timer = setInterval(function(){
            currentPartDone = 0;
            //var test = true;
            for(var id in saveProcess.frames){
                if(saveProcess.frames[id].answer !== null)
                    currentPartDone++;
                /*else{
                    if(test){
                        saveProcess.frames[id].answer = "success";
                        test = false;
                    }
                }*/
            }
            //Меняем прогресс бар
            var percent = Math.floor(currentPartDone*100/allParts);
            if(percent == 99) percent = 100;
            //$(".progressbar",dContent).progressbar("option", "value",percent);
            //$(".percent",dContent).text(percent);
            //Проверяем на завершение
            if(currentPartDone == allParts) {
                clearInterval(timer);
                finalSaveWork();
            }
        },300);

        var finalSaveWork = function(){
            //Проходим по всем отработанным фреймам и смотрим есть ли фреймы с ошибками
            var errStr = "";
            var errorFramesCount = 0;
            for(var id in saveProcess.frames){
                if(saveProcess.frames[id].answer == "error"){
                    errorFramesCount ++;
                    errStr += "<br>"+saveProcess.frameNameByTag[id];
                }
            }
            var returnString = "Сохранение прошло успешно";
            if(errorFramesCount > 0){
                returnString = "";
                if(errorFramesCount == 1)
                    returnString = "При сохранении фрейма:"+errStr+"<br>Произоша ошибка."
                if(errorFramesCount > 1)
                    returnString = "При сохранении фреймов:"+errStr+"<br>Произошли ошибки.";
            }

            //dContent.find(".process").html(returnString);
            //$(".btButton", dContent).removeClass("disabled");
            //if(errorFramesCount==0 && true){
            //    $(".saveProcess-dialog").remove();
            //}

            //рефрешим диалог на той же вкладке что была
            _private.frames.functions.refreshFrames(_private.frames.current);
            $(".loading-overlay").fadeOut(250);
            if(errorFramesCount > 0)
                useTopHoverTitleError(returnString, 3001);
            else
                useTopHoverTitle(returnString, 3001);
        }
    }

    /**
     * Метод что каждый фрейм вызывает после сохранения
     * @param frame
     * @param result
     * @param error
     */
    _private.frameSaved = function(frame, result, error){
        var saveProcess = _private.data.saveProcess;
        if(saveProcess.frames[frame]){
            saveProcess.frames[frame].answer = result;
            saveProcess.frames[frame].error = error;
        }
    }

    //Данные
    _private.frames.OPLevelsData = null;

    //Всякие функции общие для нескольких фреймов
    _private.frames.functions = {};

    _private.frames.functions.changeFilial = function(container, type){
        $(".rightSideBar", container).html('');
        $.ajax({
            url: '/eo/requests/get-filial-levels/' + $(".active_fil",container).attr('id_org') + "/" + $(".active_fil",container).attr('id_req'),
            type: "POST",
            data: {
                type: type
            },
            success: function (answer) {
                $(".change_fill.active_fil",container).parents("li").eq(0).find("ul").remove();
                $(".change_fill.active_fil",container).parents("li").eq(0).append($(answer));

                //добавляем ряды, если надо из сохраненного ранее в выборе уровней
                _private.frames.functions.updateLeftOPLevelList(container);
            }
        });
    };

    _private.frames.functions.changePlacesFilial = function(container, type){

        $(".rightSideBar", container).html('<div style="text-align: center; margin-top:30px">Загрузка...</div>');
        $.ajax({
            url: '/eo/requests/get-filial-places-od/' + $(".filial.active",container).attr('id_org'),
            data:{
                type: type
            },
            type: "POST",
            success: function (answer) {
                $(".rightSideBar", container).html($(answer));
                //Выставляем высоту
                $(".filial-odplaces-content",container).height(_private.content_container.height()-75);
                //Подсчитываем количество и меняем счетчик
                var cCounter = $(".frame-container .filial.active",container).parent().find(".anket-programs-counter");
                cCounter.text($(".form-block",container).length);
            }
        });
    };

    _private.frames.functions.changeLevel = function(container, type){
        _private.saveCurrentFrame();
        $(".rightSideBar", container).html('<div style="text-align: center; margin-top:30px">Загрузка...</div>');

        var data = {};
        if($(".active-level",container).length>0 && $(".active-level",container).attr('id-level')!="")
            data["levelId"] = $(".active-level",container).attr('id-level');

        data["filialId"] = $(".active_fil",container).attr('id_org');
        if(type)
            data["type"] = type;

        data["requestId"] = $("#requestId", _private.container).val();

        $.ajax({
            url: '/eo/requests/get-level-op',
            data: data,
            type: "POST",
            success: function (answer) {
                $(".rightSideBar", container).html($(answer));
                //Выставляем высоту
                $(".level-op-content",container).height(_private.content_container.height()-80);

                if(type == "new-op" || type=="change-op")
                    _private.frames.functions.updateOpList(container);
            }
        });
    }

    /**
     * Диалог выбора филиала
     * @param container
     * @param callback
     * @param type
     */
    _private.frames.functions.selectFilialDialog = function(container, callback, type, createCallback){
        dialog = {
            width: 646,
            height: 520
        };
        //id организации
        var orgId = $("#filial_orgId",container).val();
        //id заявления
        var requestId = $("#requestId", _private.container).val();

        var dContent = $(
            '<div class="dialog-select-filial">' +
                "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

        var dialogTitle = "Выбор филиалов";
        if(type=="new") dialogTitle = "Просмотр филиалов";

        $(dContent).dialog({
            title: dialogTitle,
            width: dialog.width,
            height: dialog.height,
            modal: true,
            close: function () {
                $(".dialog-select-filial").remove();
            },
            open: function () {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/requests/get-filials-select",
                    data: {
                        orgId: orgId,
                        requestId: requestId,
                        type: type
                    },
                    success: function (answer) {
                        $(dContent).html("").append(answer);

                        //Обработчик нажатия на кнопку отмена
                        $(".btCancel",dContent).click(function(){
                            $(".dialog-select-filial").remove();
                        });

                        //Обработчик нажатия на кнопку Создать
                        $(".btCreate",dContent).click(function(){
                            var addCallback = function(){
                                $(".dialog-select-filial").remove();
                                _private.frames.functions.selectFilialDialog(container, callback, type, createCallback);
                            };
                            var orgId = $("#filial_orgId",container).val();
                            organization.createNewOrganizationInfoDialog(orgId,addCallback);
                        });

                        //Обработчик нажатия на кнопку Выбрать
                        $(".btAdd", dContent).click(function(){
                            //собираем список выбранных айдишников и передаем его в колбэк
                            var filialIds = [];
                            $(".selected_org:checked",dContent).each(function(i,item){
                                filialIds.push($(item).val())
                            });
                            //проверяем если были выбраны филиалы
                            if(filialIds.length==0){
                                useTopHoverTitleError("Нет выбранных филиалов");
                                return;
                            }
                            //Запускаем колбэк, передаем в него массив выбранных айдишников
                            if(callback)
                                callback(filialIds);
                        });

                        //Обработчик нажатия на выбрать все
                        $(".select_all", dContent).click(function(){
                            if($(this).is(":checked")){
                                $(".filials_container", dContent).find("input[type=checkbox]").prop("checked",true);
                            } else {
                                $(".filials_container", dContent).find("input[type=checkbox]").prop("checked",false);
                            }
                        });

                        //Вешаем обработчик поиска
                        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
                            return function( elem ) {
                                arg = arg.toLowerCase();
                                var strText = $(elem).text().toLowerCase();
                                var valText = $(elem).val().toLowerCase();
                                if(
                                    strText.indexOf(arg) >= 0
                                    || valText.indexOf(arg) >= 0
                                )
                                    return true;
                                else
                                    return false;
                            };
                        });

                        console.log('Обработчик ввода текста в поиск #1');
                        $('.search-input',dContent).keyup(function(e) {
                            console.log('Обработчик ввода текста в поиск #11');
                            if(e.which == 27){
                                clearTimeout($.data(this, 'timer'));
                                clearSearch();
                                e.stopPropagation();
                                return;
                            }
                            clearTimeout($.data(this, 'timer'));
                            var wait = setTimeout(search, 500);
                            $(this).data('timer', wait);

                        });

                        function search(){
                            var searchVal = $('.search-input',dContent).val();
                            if(searchVal == ""){
                                clearSearch();
                                return;
                            }
                            $("#filteredTable tr", dContent).hide();
                            $("#filteredTable .org_title:contains("+searchVal+")", dContent).parents("tr").show();
                            //рисуем шапку
                            $("#filteredTable th:first", dContent).parent().show();
                            if( $("#filteredTable tr:visible", dContent).length < 2 ){
                                $("#filteredTable ", dContent).append('<tr class="row-empty-search level"> <td  colspan="3"> Филиалов не найдено </td> </tr>')
                            }
                        }

                        function clearSearch(){
                            $('#search-input',dContent).val("");
                            $("#filteredTable tr", dContent).show();
                            $("#filteredTable tr.row-empty-search", dContent).remove()
                        }
                    }
                });
            }
        })
    };

    /**
     * Диалог создания филиала
     * @param container
     * @param callback
     * @param type
     */
    _private.frames.functions.createFilialDialog = function(container, callback, type){
        dialog = {
            width: 646,
            height: 390
        };
        //id организации
        var orgId = $("#filial_orgId",container).val();
        //id заявления
        var requestId = $("#requestId", _private.container).val();

        var dContent = $(
            '<div class="dialog-create-filial">' +
            "<div style='text-align: center;margin-top:30px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

        var dialogTitle = "Создание филиала";

        $(dContent).dialog({
            title: dialogTitle,
            width: dialog.width,
            height: dialog.height,
            modal: true,
            close: function () {
                $(".dialog-create-filial").remove();
            },
            open: function () {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/islod/requests/get-filial-create",
                    data: {
                        orgId: orgId,
                        requestId: requestId
                    },
                    success: function (answer) {
                        $(dContent).html("").append(answer);

                        //Обработчик нажатия на кнопку отмена
                        $(".btCancel", dContent).click(function () {
                            $(".dialog-create-filial").remove();
                        });

                        //Обработчик нажатия на кнопку Создать
                        $(".btCreate", dContent).click(function () {
                            //Проверка на заполненные обязательные поля
                            var isError = false;

                            $(".required",dContent).each(function(i, dField){
                                if($(dField).val()==""){
                                    $(dField).parents("td").eq(0).addClass("isError");
                                    isError = true;
                                }
                            });

                            if(isError)
                                return;
                            //Собираем данные по полям
                            var fd = new FormData();
                            var test = [];
                            $(".dataField", dContent).each(function(i, item){
                                var iName = $(item).attr("name");
                                var fType = $(item).attr("fType");
                                switch(fType){
                                    case "val":
                                        var iValue = $(item).val();
                                        fd.append(iName,iValue);
                                        test.push({name:iName,val:iValue});
                                        break;
                                }

                            });
                            console.log(test);


                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                data: fd,
                                processData: false,
                                contentType: false,
                                url: "/eo/requests/add-filial/"+type,
                                success: function(answer) {
                                    //Проверяем если ошибка
                                    if(answer.result && answer.result=="error"){
                                        useTopHoverTitleError(answer.error);
                                        return;
                                    }
                                    //Если все хорошо
                                    if(answer.result && answer.result=="success"){
                                        if(callback)
                                            callback();
                                    }
                                }
                            });
                        });
                    }
                });
            }
        });
    };

    /**
     * Метод выводит диалог для выбора адреса
     */
    _private.frames.functions.selectAddressDialog = function(container, type){
        dialog = {
            width: 656,
            height: 525
        };
        //id организации
        var orgId = $(".leftSideBar .filial.active",container).attr("id_eiis_org");
        var orgInfId = $(".leftSideBar .filial.active",container).attr("id_org");

        //id заявления
        var requestId = $("#requestId", _private.container).val();

        var dContent = $(
            '<div class="dialog-select-address">' +
            "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

        var dialogTitle = "Выбор адресов";
        $(dContent).dialog({
            title: dialogTitle,
            width: dialog.width,
            height: dialog.height,
            modal: true,
            close: function () {
                $(".dialog-select-address").remove();
            },
            open: function () {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/requests/get-address-select",
                    data: {
                        requestId: requestId,
                        orgId: orgId,
                        orgInfId: orgInfId,
                        type: type
                    },
                    success: function (answer) {
                        $(dContent).html("").append(answer);

                        //Обработчик нажатия на кнопку отмена
                        $(".btCancel",dContent).click(function(){
                            $(".dialog-select-address").remove();
                        });

                        //Обработчик нажатия на кнопку Выбрать
                        $(".btAdd", dContent).click(function(){
                            var self = $(this);
                            if(self.hasClass("isClicked"))
                                return;

                            self.addClass("isClicked");

                            //Собираем список айди адресов
                            var addrIds = [];
                            $(".selected_adr:checked",dContent).each(function(i,item){
                                addrIds.push($(item).val());
                            });
                            //проверяем если были выбраны адреса
                            if(addrIds.length==0){
                                useTopHoverTitleError("Адреса не были выбраны");
                                return;
                            }
                            //Отправляем список на сервер чтобы там скопировало и добавило
//id заявления
                            var filialId = $(".leftSideBar .filial.active",container).attr("id_org");
                            //Отправляем запрос на добавление выбранных филиалов
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                data: {
                                    addrIds : addrIds,
                                    filialId: filialId,
                                    type: type
                                },
                                url: "/eo/requests/add-selected-address",
                                success: function(answer) {
                                    //Проверяем если ошибка
                                    if(answer.result && answer.result=="error"){
                                        useTopHoverTitleError(answer.error);
                                        self.removeClass("isClicked");
                                        return;
                                    }
                                    //Если все хорошо
                                    if(answer.result && answer.result=="success"){
                                        //Закрываем диалоги
                                        $(".dialog-select-address").remove();
                                        //перерисовываем контент
                                        $(".frame-container .filial.active",container).click();
                                        self.removeClass("isClicked");
                                    }
                                }
                            });


                        });

                        //Обработчик нажатия на выбрать все
                        $(".select_all", dContent).click(function(){
                            if($(this).is(":checked")){
                                $("#filteredTable", dContent).find("input[type=checkbox]").prop("checked",true);
                            } else {
                                $("#filteredTable", dContent).find("input[type=checkbox]").prop("checked",false);
                            }
                        });

                        //Вешаем обработчик поиска
                        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
                            return function( elem ) {
                                arg = arg.toLowerCase();
                                var strText = $(elem).text().toLowerCase();
                                var valText = $(elem).val().toLowerCase();
                                if(
                                    strText.indexOf(arg) >= 0
                                    || valText.indexOf(arg) >= 0
                                )
                                    return true;
                                else
                                    return false;
                            };
                        });

                       console.log('Обработчик ввода текста в поиск #2');
                        $('.search-input',dContent).keyup(function(e) {
                            $("#filteredTable .no-results", dContent).remove();
                            if(e.which == 27){
                                clearTimeout($.data(this, 'timer'));
                                clearSearch();
                                e.stopPropagation();
                                return;
                            }
                            clearTimeout($.data(this, 'timer'));
                            var wait = setTimeout(search, 500);
                            $(this).data('timer', wait);

                        });

                        function search(){
                            var searchVal = $('.search-input',dContent).val();
                            if(searchVal == ""){
                                clearSearch();
                                return;
                            }

                            $("#filteredTable tr", dContent).hide();
                            $("#filteredTable .adr_title:contains("+searchVal+")", dContent).parents("tr").show();
                            if($("#filteredTable tr:visible", dContent).length == 0){
                                $("#filteredTable", dContent).append($('<tr class="no-results"><td colspan="4" class="td_level" style="text-align: center">Записи не найдены</td></tr>'));
                            }
                            //вернем шапку таблички
                            $("#filteredTable th:first", dContent).parent().show();
                        }

                        function clearSearch(){
                            $('#search-input',dContent).val("");
                            $("#filteredTable tr", dContent).show();
                        }
                    }
                });
            }
        })
    };

    /**
     * Метод инициирует поиск на вкладках с адресами
     * @param container
     */
    _private.frames.functions.initODSearch = function(container){
        //Вешаем обработчик поиска
        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
            return function( elem ) {
                arg = arg.toLowerCase();
                var strText = $(elem).text().toLowerCase();
                var valText = $(elem).val().toLowerCase();
                if(
                    strText.indexOf(arg) >= 0
                    || valText.indexOf(arg) >= 0
                )
                    return true;
                else
                    return false;
            };
        });

        console.log('Обработчик ввода текста в поиск #3');
        $(container).delegate('.search-input',"keyup",function(e) {
            console.log('ввод в строке поиска');
            if(e.which == 27){
                clearTimeout($.data(this, 'timer'));
                $('.search-input',container).val("")
                search();
                e.stopPropagation();
                return;
            }
            clearTimeout($.data(this, 'timer'));
            var wait = setTimeout(search, 500);
            $(this).data('timer', wait);
        });

        function search(){
            var searchVal = $('.search-input',container).val();

            if(searchVal == ""){
                clearSearch();
                return;
            }

            $(".filial-odplaces-content .form-block", container).hide();
            $(".filial-odplaces-content .form-block .dataField[name=postcode]:contains("+searchVal+")", container).parents(".form-block").show();
            $(".filial-odplaces-content .form-block .dataField[name=address]:contains("+searchVal+")", container).parents(".form-block").show();
        }

        function clearSearch(){
            $(".filial-odplaces-content .form-block", container).show();
        }
    };

    _private.frames.functions.placesAddrSave = function(container, type, isFromButton){
        //Сохраняем ссылку на счетчик
        var cCounter = $(".frame-container .filial.active",container).parent().find(".anket-programs-counter");
        //Проходим по всем блокам с формами
        var saveData = [];
        $(".form-block",container).each(function(i,item){
            var formContainer = $(item);
            //Проходим по полям
            //Собираем объект с данными для добавления
            var fd = {};
            $(".dataField", formContainer).each(function(i, item){
                var iName = $(item).attr("name");
                var fType = $(item).attr("fType");
                switch(fType){
                    case "val":
                        var iValue = $(item).val();
                        fd[iName] =iValue;
                        break;
                }
            });
            if(!fd["id"] && fd["address"]=="" && fd["postcode"]=="")
                return true;

            saveData.push(fd);
        });


        var frameName = "";
        switch(type){
            case "places":
                frameName = "placesOD";
                break;
            case "open":
                frameName = "openOD";
        }

        if(saveData.length>0)
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: {
                    data: JSON.stringify(saveData),
                    filial: $(".frame-container .filial.active", container).attr("id_org"),
                    type: type
                },
                url: "/eo/requests/places-od-save",
                success: function(answer) {

                    //Проверяем если ошибка
                    if(answer.result && answer.result=="error"){
                        console.log(answer);
                        _private.frameSaved(frameName, answer.result);
                        return;
                    }
                    //Если все хорошо
                    if(answer.result && answer.result=="success"){
                        //обновляем количество
                        cCounter.text(saveData.length);
                        //перерисовываем список
                        _private.frames.functions.changePlacesFilial(container, type);
                        //
                        _private.frameSaved(frameName, answer.result);
                    }
                }
            });
        else {
            _private.frameSaved(frameName, "success");
        }
    }

    /**
     * Метод выводит диалог для добавления ОП
     * @param container
     * @param type
     */
    _private.frames.functions.addOPDialog = function(container, type){
        dialog = {
            width: 646,
            height: 520
        };
        //id организации
        var orgId = $(".leftSideBar .change_fill.active_fil",container).attr("id_org");

        //id заявления
        var requestId = $("#requestId", _private.container).val();

        var dContent = $(
            '<div class="dialog-add-op">' +
            "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

        var dialogTitle = "Выбор из действующих образовательных программ";
        $(dContent).dialog({
            title: dialogTitle,
            width: $(window).width()-20,
            height: $(window).height()-40,
            modal: true,
            close: function () {
                $(".dialog-add-op").remove();
            },
            open: function () {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/requests/select-current-op-dialog",
                    data: {
                        requestId: requestId,
                        orgId: orgId,
                        type: type
                    },
                    success: function (answer) {
                        $(dContent).html("").append(answer);
                        resizeContent($(".op-dialog-list .fields-container .viewzone"));
                        specialTableResize(".op-dialog-list");
                        //Обработчик нажатия на кнопку отмена
                        $(".btCancel",dContent).click(function(){
                            $(".dialog-add-op").remove();
                        });

                        //Обработчик нажатия на кнопку Выбрать
                        $(".btAdd", dContent).click(function(){
                            var self = $(this);
                            if(self.hasClass("work"))
                                return;

                            self.addClass("work");
                            //Если ничего не выбрано
                            if($(".op-container input:checked", dContent).length==0){
                                useTopHoverTitleError("Не выбрано программ");
                                return 0;
                            }
                            //Иначе посылаем запрос на добавление
                            var data = {
                                requestOrganizationInfoId: orgId,
                                type: type,
                                programs: []
                            };

                            $(".op-container input:checked", dContent).each(function(i,item){
                                var id = $(item).attr("id").split("_")[1];
                                data.programs.push(id);
                            });

                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                url: "/eo/requests/add-current-op",
                                data: data,
                                success: function (answer) {
                                    //Проверяем если ошибка
                                    if(answer.result && answer.result=="error"){
                                        useTopHoverTitleError(answer.error);
                                        self.removeClass("work")
                                        return;
                                    }
                                    //Если все хорошо
                                    if(answer.result && answer.result=="success"){
                                        self.removeClass("work")
                                        //Закрываем диалоги
                                        $(".dialog-add-op").remove();
                                        //перерисовываем контент
                                        $(".leftSideBar .change_fill.active_fil",container).click();
                                    }
                                }
                            });
                        });

                        //обработчик смены селектами с уровнями
                        $("select.levelsList", dContent).change(function(){
                            var levelId = $(this).val();
                                search();
                            });


                        //Вешаем обработчик поиска
                        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
                            return function( elem ) {
                                arg = arg.toLowerCase();
                                var strText = $(elem).text().toLowerCase();
                                var valText = $(elem).val().toLowerCase();
                                if(
                                    strText.indexOf(arg) >= 0
                                    || valText.indexOf(arg) >= 0
                                )
                                    return true;
                                else
                                    return false;
                            };
                        });

                        console.log('Обработчик ввода текста в поиск #5');
                        $('.search-input',dContent).keyup(function(e) {
                            if(e.which == 27){
                                clearTimeout($.data(this, 'timer'));
                                $('#search-input',dContent).val("")
                                search();
                                e.stopPropagation();
                                return;
                            }
                            clearTimeout($.data(this, 'timer'));
                            var wait = setTimeout(search, 500);
                            $(this).data('timer', wait);
                        });

                        function search(){
                            var searchVal = $('.search-input',dContent).val();
                            var levelsListVal = $("select.levelsList", dContent).val();

                            if(searchVal == "" && levelsListVal==0){
                                clearSearch();
                                return;
                            }

                            var levelsText = "";
                            if(levelsListVal!=0){
                                levelsText="[data-level="+levelsListVal+"]";
                            }

                            $(".op-dialog-list .op-container .program-row", dContent).hide();
                            $(".op-dialog-list .op-container .program-row:contains("+searchVal+")"+levelsText, dContent).show();
                        }

                        function clearSearch(){
                            $(".op-dialog-list .op-container .program-row", dContent).show();
                        }
                        
                        console.log('открытая - карточка лицении - прекращение ОП - выбрать- вешаем тык по строке таблички');
                        $('tr td',dContent).click(function(e) {
                            console.log('тык по строке таблички');
                            if( jQuery('input',this).length ){
                                return 0;
                            }            
                            var parent = jQuery( this ).parents( 'tr' ).first();
                            jQuery( parent ).find( 'input' ).click();
                        });
                    }
                });
            }
        })
    }
    /**
     * изменялка раздмела, чтоб много раз не справивалось
     */
    jQuery(window).resize(function(){
        jQuery(".dialog-add-op").dialog( "option", "height", $(window).height()-40 );
        jQuery(".dialog-add-op").dialog( "option", "width",  $(window).width()-20 );
    });



    /**
     * Метод перегружает диалог со всеми фреймами
     */
    _private.frames.functions.refreshFrames = function(startFrame){
        //сохраняем текущий фрейм
        //_private.saveCurrentFrame();
        //
        var requestId = $("#requestId", _private.container).val();
        var isTest = $("#isTest", _private.container).val();
        if ( 'undefined' == typeof(requestId) ){
            return 0;
        }

        _private.container.html('<div class="dialog-request-edit">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');


        if (isTest == "true")
            requestId = "test";

        console.info('преред дерг 4');
        console.log(requestId);

        //Отсылаем запрос на получение диалога
        $.ajax({
            type: 'POST',
            dataType: "html",
            url: "/eo/requests/edit/" + requestId,
            data:{
                frame: startFrame
            },
            success: function (answer) {
                $(".loading-img", _private.container).fadeOut(500, function () {
                    _private.container.html("").append($(answer));
                    RequestFrames.init();
                });
            }
        });
    };

    /**
     * метод добавляет строку с адресом обр деятельности
     * @param tableContainer
     */
    _private.frames.functions.addODPlace = function(container){
        var num = 1;
        if($(".filial-odplaces-content",container).find(".num").length > 0)
            num = parseInt($(".filial-odplaces-content",container).find(".num").last().text())+1;

        $(".filial-odplaces-content",container).append($(
            '<tr class="form-block">'+
                '<td class="num" width="10%">'+num+'</td>'+
                '<td width="20%">' +
                    '<input type="hidden" class="dataField" fType="val" name="requestOrganizationInfoId" value="'+$(".frame-container .filial.active",container).attr("id_org")+'">'+
                    '<input type="text" class="dataField" fType="val" name="postcode" placeholder="Введите индекс">'+
                '</td>'+
                '<td width="65%"><input style="width: 100%" type="text" class="dataField" fType="val" name="address" placeholder="Введите адрес"></td>'+
                '<td style="width:5%;text-align: center"><span class="btRemove minus-icon" id="a_self"><span>-</span></span></td>'+
            '</tr>'
        ))
    },

    /**
     * Метод пересчитывает порядковые номера для адресов
     */
    _private.frames.functions.recalculateODPlacesNumbers = function(container){
        var num = 1;
        $(".filial-odplaces-content",container).find(".num").each(function(i, item){
            $(item).text(num++);
        });
    }

    /**
     * Метод пересчитывает количество оп для того чтобы правильно отображать слева
     * @param container
     * @param orgId
     */
    _private.frames.functions.recalculateOPNumber = function(container, orgId){
        //количество
        var LevelsData = _private.data.OPData.leftLevelList[orgId];
        for(var id in LevelsData){
            var levelData = LevelsData[id];
            if(levelData.id != "fc85e0a4c4b248a1acf6499c298ea58a")
                $(".level-list .level-elem[id-level="+levelData.id+"]", container).find(".level-programs-counter").text(levelData.sublevels.length);
        }
        //подсчитываем мэйн количество
        var parent = $(".active_fil",container).parent();
        var sum = 0;
        parent.find(".level-programs-counter").each(function(i,item){
            sum += parseInt($(item).text())
        });
        parent.find(".filial-programs-counter").text(sum);
    }


    _private.frames.functions.selectOPLevel = function(container, type){
        dialog = {
            width: 646,
            height: 550
        };

        var dContent = $(
            '<div class="dialog-select-op-level">' +
            "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

        var dialogTitle = "Добавление уровней образования";
        $(dContent).dialog({
            title: dialogTitle,
            width: dialog.width,
            height: dialog.height,
            modal: true,
            close: function () {
                $(".dialog-select-op-level").remove();
            },
            open: function () {
                console.log( 'рон - карточка заявления - новые оп - загрузка диалога выбора уровней образования' );
                if( !$(".active_fil",container).length ){
                    jQuery(".change_fill:first",container).addClass( 'active_fil' );
                }
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/requests/get-op-level-dialog",
                    data: {
                        type: type,
                        filialId: $(".active_fil",container).attr("id_org")
                    },
                    success: function (answer) {
                        $(dContent).html("").append(answer);

                        //Обработчик нажатия на кнопку отмена
                        $(".btCancel",dContent).click(function(){
                            $(".dialog-select-op-level").remove();
                        });

                        //Обработчик нажатия на кнопку Выбрать
                        $(".btAdd", dContent).click(function(){
                            //Если ничего не выбрано
                            if($("input:checked", dContent).length==0){
                                useTopHoverTitleError("Не выбрано уровней");
                                return false;
                            }

                            // Собираем выделенные данные
                            var parentLevels = {};
                            var lvlIds = [];
                            var levelsToProgramCreate = [];
                            $("input:checked",dContent).each(function(i,item){
                                var parentLevel = $(item).parents(".level-row");
                                var parentLevelId = parentLevel.attr("data-parent");
                                var levelId = $(item).attr("data-id");
                                var levelName = $(item).parent().find("label").text();

                                if(!parentLevels[parentLevelId]){
                                    parentLevels[parentLevelId] = {
                                        id: parentLevelId,
                                        name:  parentLevel.find(".level-name").text(),
                                        sublevels: []
                                    };
                                }

                                parentLevels[parentLevelId]["sublevels"].push({
                                    id: levelId,
                                    name: levelName,
                                    noForm: ($(item).attr("data-noForm")=="true")?true:false
                                });

                                //Проверяем если надо создать программу по умолчанию для этого уровня
                                if(!$(item).prop("disabled") && $(item).attr("data-noForm")=="true")
                                    levelsToProgramCreate.push(levelId);
                            });

                            var orgId = $(".active_fil",container).attr("id_org");

                            //Задаем подготовленный объект в фреймы
                            _private.data.OPData.leftLevelList[orgId] = parentLevels;



                            //Отправляем запрос на создание программ по умолчанию
                            if(levelsToProgramCreate.length>0) {
                                $.ajax({
                                    type: 'POST',
                                    dataType: "json",
                                    data: {
                                        levelIds:levelsToProgramCreate,
                                        orgId: orgId,
                                        type: type,
                                    },
                                    url: "/eo/requests/create-level-programs",
                                    success: function (answer) {
                                        //Проверяем если ошибка
                                        if (answer.result && answer.result == "error") {
                                            useTopHoverTitleError("Произошла ошибка при создании программ по умолчанию для выбранных уровней");
                                            return;
                                        }
                                        //Если все хорошо
                                        if (answer.result && answer.result == "success") {
                                            //Апдейтим список слева
                                            _private.frames.functions.updateLeftOPLevelList(container);

                                            $(".dialog-select-op-level").remove();
                                            //кликаем на текущий выбранный уровень
                                            _private.frames.functions.recalculateOPNumber(container, orgId);
                                        }
                                    }
                                });
                            } else {
                                _private.frames.functions.updateLeftOPLevelList(container);

                                $(".dialog-select-op-level").remove();
                            }


                        });

                        //Проставляем выбранное состояние чекбоксам в зависимости от предыдущего выбора
                        var orgId = $(".active_fil",container).attr("id_org");
                        var selectedLevels = _private.data.OPData.leftLevelList[orgId];
                        for(var id in selectedLevels){
                            for(var k =0; k<selectedLevels[id]["sublevels"].length; k++){
                                var sId = selectedLevels[id]["sublevels"][k].id;
                                $("input#lvl_"+sId, dContent).prop("checked", true);
                            }
                        }
                    }
                });
            }
        })
    };

    _private.frames.functions.initOPHandlers = function(container, type){
        //Обработчик поиска
        //Вешаем обработчик поиска
        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
            return function( elem ) {
                arg = arg.toLowerCase();
                var strText = $(elem).text().toLowerCase();
                var valText = $(elem).val().toLowerCase();
                if(
                    strText.indexOf(arg) >= 0
                    || valText.indexOf(arg) >= 0
                )
                    return true;
                else
                    return false;
            };
        });

        //Обработчик ввода текста в поиск
        $(container).delegate(".search-field", "keyup", function(e) {
            console.log('тык по кнопкас 54');
            if(e.which == 27){
                clearTimeout($.data(this, 'timer'));
                clearSearch();
                e.stopPropagation();
                return;
            }
            clearTimeout($.data(this, 'timer'));
            var wait = setTimeout(search, 500);
            $(this).data('timer', wait);
        });

        function search(){
            var searchVal = $('.search-field',container).val();
            if(searchVal == ""){
                clearSearch();
                return;
            }
            $(".intable tr", container).hide();
            $(".intable tr.op-row td:contains("+searchVal+")", container).parents("tr").show();
            //рисуем шапку таблички
            $(".intable tr:first ", container).show();
        }

        function clearSearch(){
            $('.search-fieldt',container).val("");
            $(".intable tr", container).show();
        }
        //==
        //Обработчик удаления оп
        $(container).delegate(".btRemoveOP","click", function(){
            var self = $(this);
            if(self.hasClass("inWork"))
                return;

            if(confirm("Вы действительно хотите удалить эту ОП?")){
                self.addClass("inWork");
                var id = $(this).attr("data-id");

                var selfBt = $(this);

                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    data: {
                        opId : id
                    },
                    url: "/eo/requests/remove-op",
                    success: function(answer) {
                        //Проверяем если ошибка
                        if(answer.result && answer.result=="error"){
                            useTopHoverTitleError(answer.error);
                            return;
                        }
                        //Если все хорошо
                        if(answer.result && answer.result=="success"){
                            //Удаляем строку
                            selfBt.parents("tr").eq(0).remove();
                            //перерисовываем контент
                            //$(".leftSideBar .change_fill.active_fil",newOPData.container).click();
                            //меняем счетчик
                            _private.frames.functions.changeLevelCounter(container, "-");
                        }
                        self.removeClass("inWork");
                    }
                });
            }
        });


        //================== Обработка смены селектов
        $(".rightSideBar", container).delegate(".pType","change", function(){
            var parent = $(this).parents("tr").eq(0);
            var eProgramsSelect = parent.find(".ePrograms");
            var QualificationsSelect = parent.find("select.Qualifications");

            eProgramsSelect.html("");
            QualificationsSelect.html("");
            QualificationsSelect.eq(0).pqSelect( "refreshData" );

            //Заполняем селект с программами
            var pTypeId = $(this).val();
            if(_private.data["eProgramsByType"][pTypeId]) {
                for (var j = 0; j < _private.data["eProgramsByType"][pTypeId].length; j++) {
                    var eProgram = _private.data["eProgramsByType"][pTypeId][j];
                    eProgramsSelect.append($('<option data-code="' + eProgram.Code + '" data-name="' + eProgram.Name + '" value="' + eProgram.Id + '">' + eProgram.Code + ' ' + eProgram.Name + '</option>'));
                }
                eProgramsSelect.trigger('chosen:updated');
                eProgramsSelect.change();
            }
        });

        $(".rightSideBar", container).delegate(".ePrograms","change", function(){
            var parent = $(this).parents("tr").eq(0);
            var QualificationsSelect = parent.find("select.Qualifications");

            QualificationsSelect.html("");
            QualificationsSelect.eq(0).pqSelect( "refreshData" );

            //отсылаем запрос
            var epId = $(this).val();
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: {
                    epId : epId
                },
                url: "/eo/requests/get-ep-qualifications",
                success: function(answer) {
                    //Проверяем если ошибка
                    if(answer.result && answer.result=="error"){
                        useTopHoverTitleError(answer.error);
                        return;
                    }
                    //Если все хорошо
                    if(answer.result && answer.result=="success"){
                        //заполняем селект
                        for(var j =0; j <  answer.list.length; j++){
                            var qualification = answer.list[j];
                            QualificationsSelect.append($('<option value="'+qualification.Id+'">'+qualification.Name+'</option>'));
                        }
                        QualificationsSelect.eq(0).find("option").prop("selected",true);
                        QualificationsSelect.eq(0).pqSelect("refreshData");
                    }
                }
            });
        });

        //Обработчик добавления ОП
        $(".rightSideBar", container).delegate(".btAddOP", "click", function(){
            //Собираем данные с формы
            var form = $(this).parents(".formData");
            var fd = {};
            $(".dataField", form).each(function(i, item){
                var iName = $(item).attr("name");
                var fType = $(item).attr("fType");
                switch(fType){
                    case "val":
                        var iValue = $(item).val();
                        fd[iName]=iValue;
                        break;
                }

            });
            fd["requestOrganizationInfoId"] = $(".leftSideBar .change_fill.active_fil", container).attr("id_org");
            switch(type){
                case "new-op":
                    fd["typeOfActivity"] = "OPENING";
                    break;
                case "change-op":
                    fd["typeOfActivity"] = "EDITING";
                    break;
            }

            //отправляем запрос на добавление на сервер

            $.ajax({
                type: 'POST',
                dataType: "json",
                data: fd,
                url: "/eo/requests/add-new-op",
                success: function(answer) {
                    //Проверяем если ошибка
                    if(answer.result && answer.result=="error"){
                        useTopHoverTitleError(answer.error);
                        return;
                    }
                    //Если все хорошо
                    if(answer.result && answer.result=="success"){
                        //Имена квалификаций
                        var qualNames = [];
                        var qualValues = $("select.Qualifications",form).val();
                        if(qualValues)
                            for(var n = 0; n<qualValues.length; n++){
                                var qualValue = qualValues[n];
                                var qualText = $("select.Qualifications option[value="+qualValue+"]",form).text();
                                qualNames.push(qualText);
                            }
                        qualNames = qualNames.join(", ");

                        //Добавляем новый ряд
                        form.before(
                            $('<tr class="op-row">' +
                                '<td width="70" class="td_num">&nbsp;</td>'+
                                '<td width="150" style="text-align: center;">'+$(".ePrograms option[value="+$(".ePrograms",form).val()+"]",form).attr("data-code")+'</td>'+
                                '<td width="29%" style="text-align: left;">'+$(".ePrograms option[value="+$(".ePrograms",form).val()+"]",form).attr("data-name")+'</td>'+
								'<td width="29%" style="text-align: left;">'+$(".pType option[value="+$(".pType",form).val()+"]",form).text()+'</td>'+
                                '<td>'+qualNames+'</td>'+
                                '<td style="width:50px"><span data-id="'+answer.id+'" class="btRemoveOP" ><span>-</span></span></td>'+
                                '</tr>')
                        );
                        //перебиваем нумерацию
                        var num = 1;
                        form.parent().find(".td_num").each(function(i, item){
                            $(item).text(num++);
                        });
                        //Очищаем форму

                        //меняем счетчик
                        _private.frames.functions.changeLevelCounter(container, "+");
                    }
                }
            });
        });
    }

    /**
     * Метод меняет счетчики на уровнях, при добавлении/удалении ОП
     *
     * @param container
     * @param action
     */
    _private.frames.functions.changeLevelCounter = function(container, action){
        switch(action){
            case "+":
                $(".active-level .level-programs-counter",container).text(parseInt($(".active-level .level-programs-counter",container).text())+1);
                var cnt = $(".active_fil",container).parent().find(".filial-programs-counter");
                cnt.text(parseInt(cnt.text())+1);
                break;
            case "-":
                var cnt = $(".active_fil",container).parent().find(".filial-programs-counter");
                cnt.text(parseInt(cnt.text())-1);
                var count = parseInt($(".active-level .level-programs-counter",container).text());
                if(count == 1){
                    //удаляем левел и из списка если там такой есть
                    $(".active-level",container).remove();
                    $(".rightSideBar",container).html("");

                    var orgId = $(".active_fil",container).attr("id_org");
                    if(_private.data.OPData.leftLevelList[orgId])
                        delete _private.data.OPData.leftLevelList[orgId];

                    $(".active_fil",container).find(".level-elem").first().click();
                } else
                    $(".active-level .level-programs-counter",container).text(count-1);
                break;
        }

    };

    /**
     * Метод актуализирует слева список уровней у выбранного филиала, когда выбираются новые уровни
     * @param container
     */
    _private.frames.functions.updateLeftOPLevelList = function(container){
        //получаем айди текущего открытого филиала
        var orgId = $(".active_fil",container).attr("id_org");
        //данные по подуровням
        var sublevelData = _private.data.OPData.leftLevelList[orgId];
        var ulContainer = $(".active_fil",container).parent().find("ul");

        //Запоминаем, были ли уровни внутри филиала, до добавления выбранных
        var isEmptyFilial = true;
        if(ulContainer.find("li").length>0)
            isEmptyFilial = false;


        //Проходим по всем верхним уровням
        for(var id in sublevelData){
            var level = sublevelData[id];

            if(ulContainer.find(".level-elem[id-level="+level.id+"]").length == 0){
                ulContainer.append($('<li class="level-elem" id-level="'+level.id+'"><a class="level-name">-'+level.name+'</a> (<span class="level-programs-counter">0</span>)</li>'));
            }
        }

        //сортируем
        if(sublevelData){
            var items = $('li', ulContainer).get();
            items.sort(function (a, b) {
                var keyA = $(a).find("a").text();
                var keyB = $(b).find("a").text();

                if (keyA < keyB) return -1;
                if (keyA > keyB) return 1;
                return 0;
            });
            $.each(items, function (i, li) {
                ulContainer.append(li);
            });
        }

        //Если у филиала не было уровней до добавления
        if(isEmptyFilial || $(".rightSideBar *", container).length == 0){
            //Кликаем на первый элемент
            ulContainer.find("li").first().click();
        } else {
            //иначе пробуем дорисовать блоки и формы что выбрались вот только что
            if($(".level-elem.active-level",container).attr("id-level")=="fc85e0a4c4b248a1acf6499c298ea58a")
                _private.frames.functions.updateOpList(container);
            else {
                $(".level-elem.active-level",container).click();
            }
        }
    };

    _private.frames.functions.updateOpList = function(container){

        //========= Заполняем формы

        //Вычленяем из нужный нам уровень
        var orgId = $(".active_fil",container).attr("id_org");
        var levelId = $(".active-level",container).attr('id-level');

        if(_private.data.OPData.leftLevelList[orgId] && _private.data.OPData.leftLevelList[orgId][levelId]) {
            var sublevelData = _private.data.OPData.leftLevelList[orgId][levelId];

            for (var i = 0; i < sublevelData.sublevels.length; i++) {
                var sublevel = sublevelData.sublevels[i];
                //Если для этого уровня не нужно рисовать форму, продолжаем
                if (sublevel.noForm)
                    continue;
                //Проверяем чтобы такого ряда не было
                if ($(".level-op-content .level-row[data-id=" + sublevel.id + "]", container).length > 0)
                    continue;
                //==================== Подготовка данных для формы
                //типы программ
                var pTypeOptions = [];
                var firstPType = null;

                if (_private.data["pTypesByLevel"] && _private.data["pTypesByLevel"][sublevel.id]) {
                    for (var j = 0; j < _private.data["pTypesByLevel"][sublevel.id].length; j++) {
                        var pType = _private.data["pTypesByLevel"][sublevel.id][j];
                        if (!firstPType) firstPType = pType;
                        pTypeOptions.push($('<option value="' + pType.Id + '">' + pType.Name + '</option>'));
                    }
                }

                //обра программы eProgramsByType
                var eProgramsOptions = [];
                var firstEPrograms = null;

                if (firstPType && _private.data["eProgramsByType"] && _private.data["eProgramsByType"][firstPType.Id]) {
                    for (var j = 0; j < _private.data["eProgramsByType"][firstPType.Id].length; j++) {
                        var eProgram = _private.data["eProgramsByType"][firstPType.Id][j];
                        if (!firstEPrograms) firstEPrograms = eProgram;
                        eProgramsOptions.push($('<option value="' + eProgram.Id + '" data-code="' + eProgram.Code + '" data-name="' + eProgram.Name + '">' + eProgram.Code + ' ' + eProgram.Name + '</option>'));
                    }
                }

                //форма
                var form = $('<tr class="formData">' +
                    '<td>' +
                    '<input type="hidden" name="educationLevelId" class="dataField" fType="val"  value="' + sublevel.id + '" />' +
                    '<select name="eduProgramTypeId" class="dataField pType" fType="val"></select></td>' +
                    '<td colspan="2"><select name="educationProgramCatalogItemId" class="dataField ePrograms" fType="val" ></select></td>' +
                    '<td><select name="qualificationId" class="dataField Qualifications" fType="val" ></select></td>' +
                    '<td style="width:50px"><span class="btAddOP plus-icon" ><span>+</span></span></td></tr>');

                //данные в форму
                form.find(".pType").append(pTypeOptions);
                form.find(".ePrograms").append(eProgramsOptions);
                //==================== Добавляем форму
                $(".level-op-content", container).append(
                    $('<tr><td style="text-align: center" colspan="5" class="level-row" data-id="' + sublevel.id + '">' + sublevel.name + '</td></tr>')
                ).append(form);

            }
        }

        //Подключаем плагин автокомплита
        container.find(".ePrograms, .pType").chosen();

        container.find(".Qualifications").attr("multiple","multiple");
        var n = 0;
        container.find(".Qualifications").each(function(i,item){
            $(item).attr("id","qual"+n++).pqSelect({
                multiplePlaceholder: 'Выбор квалификаций',
                checkbox: true, //adds checkbox to options
                maxDisplay: 1,
                search: false
            });
        });

        //Дергаем обработчик смены селекта с программами, чтобы подгрузились квалификации
        container.find(".ePrograms").change();
    }

    /**
     * Свойства
     */
    _private.frames.general = {
        container: null,

        init: function(){
            var generalData = _private.frames.general;
            generalData.container = $(".content-block.general",_private.container);

            $(".date-field", generalData.container).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd.mm.yy"
            });

            //Обработчик добавления оплаты
            $(".add-payment",generalData.container).click(function(){
                console.log('Отображаем диалог добавления 1');
                var dContent = $(
                    '<div class="dialog-add-payment">' +
                    "<div><div class='loading-img' style='margin-top:20px;margin-left: 130px;padding-bottom: 30px;'><img src='/img/loading.gif' /></div></div>" +
                    '</div>');

                $(dContent).dialog({
                    title: "Платежное поручение",
                    width: 400,
                    //height: 380,
                    modal: true,
                    close: function () {
                        $(".dialog-add-payment").remove();
                    },
                    open: function () {
                        //Отсылаем запрос на получение диалога
                        $.ajax({
                            type: 'POST',
                            dataType: "html",
                            data: {
                                requestId: $("#requestId", _private.container).val()
                            },
                            url: "/eo/requests/get-payment-dialog",
                            success: function (answer) {
                                $(".loading-img", dContent).fadeOut(500, function () {
                                    dContent.html("").append($(answer));

                                    //вешаем дейтпикер
                                    $(".date-field", dContent).datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        dateFormat: "dd.mm.yy"
                                    });

                                    //Обработчики

                                    $(".btCancel", dContent).click(function(){
                                        $(".dialog-add-payment").remove();
                                    });

                                    $(".btSave", dContent).click(function(){
                                        console.log('добавим платежку 2');
                                        
                                       //Проходим по всем полям
                                        var formContainer = dContent;

                                        //Проходим по полям
                                        //Собираем объект с данными для добавления
                                        var fd = {};
                                        var formData = new FormData();
                                        $(".dataField", formContainer).each(function(i, item){
                                            var iName = $(item).attr("name");
                                            var fType = $(item).attr("fType");
                                            switch(fType){
                                                case "val":
                                                    var iValue = $(item).val();
                                                    fd[iName] = iValue;
                                                    formData.append(iName, iValue);
                                                    break;
                                                case "date":
                                                    var iValue = $(item).val();
                                                    var date = moment(iValue,"DD.MM.YYYY").format("YYYY-MM-DD");
                                                    fd[iName] = date;
                                                    formData.append(iName, date);
                                                    break;
                                                case "file":
                                                    var iValue = $(item).val();
                                                    if( 'undefined' == typeof(iValue) || '' == iValue ){
                                                        break;
                                                    }
                                                    var fileData =  $(item)[0].files[0];                                                
                                                    formData.append(iName, fileData);
                                                    break;
                                            }
                                        });

                                        $.ajax({
                                            type: 'POST',
                                            dataType: "json",
                                            data: formData,
                                            processData: false,
                                            contentType: false,
                                            url: "/eo/requests/add-payment",
                                            success: function(answer) {
                                                //Проверяем если ошибка
                                                if(answer.result && answer.result=="error"){
                                                    useTopHoverTitleError("Произошла ошибка при сохранении фрейма 'Изменение наименования лицензиата'");
                                                    return;
                                                }
                                                //Если все хорошо
                                                if(answer.result && answer.result=="success"){
                                                    $("tbody.payments-list",generalData.container).append(answer.row);
                                                    $(".dialog-add-payment").remove();
                                                }
                                            }
                                        });
                                    });
                                });
                            }
                        });
                    }
                })
            });

            //Обработка удаления оплаты
            $("body").undelegate(".btRemove", "click");
            $(".payments-list",generalData.container).undelegate(".btRemove", "click");
            $(".payments-list",generalData.container).delegate(".btRemove", "click", function(){
                if(confirm("Вы действительно хотите удалить этот платеж?")){
                    var id = $(this).attr("id").split("_")[1];

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            paymentId : id
                        },
                        url: "/eo/requests/remove-payment",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Удаляем из списка
                                $(".payments-list",generalData.container).find("#p_"+id).remove();
                            }
                        }
                    });
                }
            });

        //Обработка Редактирование оплаты
        $(".payments-list",generalData.container).off( "click", ".btEdit");
        $("body").off( "click", ".btEdit");
        $(".payments-list",generalData.container).on( "click", ".btEdit", function(){
            console.log('Отображаем диалог добавления 2');
            var dContent = $(
                    '<div class="dialog-add-payment">' +
                    "<div><div class='loading-img' style='margin-top:40px'><img src='/img/loading.gif' /></div></div>" +
                    '</div>'),
                currentPayment = $(this),
                idPaymentSelector = $(this).attr('id'),
                idPayment = idPaymentSelector.split('_')[1];

            $(dContent).dialog({
                title: "Платежное поручение",
                width: 400,
                //height: 380,
                modal: true,
                close: function () {
                    $(".dialog-add-payment").remove();
                },
                open: function () {
                    //Отсылаем запрос на получение диалога
                    $.ajax({
                        type: 'POST',
                        dataType: "html",
                        data: {
                            LicenseRequestPaymentId: idPayment,
                        },
                        url: "/eo/requests/get-payment-dialog",
                        success: function (answer) {
                            $(".loading-img", dContent).fadeOut(500, function () {
                                dContent.html("").append($(answer));

                                //вешаем дейтпикер
                                $(".date-field", dContent).datepicker({
                                    changeMonth: true,
                                    changeYear: true,
                                    dateFormat: "dd.mm.yy"
                                });

                                //Обработчики

                                $(".btCancel", dContent).click(function(){
                                    $(".dialog-add-payment").remove();
                                });

                                $(".btSave", dContent).click(function(){

//                                    inputCurrencyValidator( $('.dialog-add-payment input[name="SumNeeded"]') );
//                                    inputCurrencyValidator( $('.dialog-add-payment input[name="Sum"]') );

                                   //Проходим по всем полям
                                    var formContainer = dContent;

                                    //Проходим по полям
                                    //Собираем объект с данными для добавления
                                    var fd = {};
                                    var formData = new FormData();
                                    $(".dataField", formContainer).each(function(i, item){
                                        var iName = $(item).attr("name");
                                        var fType = $(item).attr("fType");
                                        switch(fType){
                                            case "val":
                                                var iValue = $(item).val();
                                                fd[iName] = iValue;
                                                formData.append(iName, iValue);
                                                break;
                                            case "date":
                                                var iValue = $(item).val();
                                                var date = moment(iValue,"DD.MM.YYYY").format("YYYY-MM-DD");
                                                fd[iName] = date;
                                                formData.append(iName, date);
                                                break;
                                            case "file":
                                                var iValue = $(item).val();
                                                if( 'undefined' == typeof(iValue) || '' == iValue ){
                                                    break;
                                                }
                                                var fileData =  $(item)[0].files[0];                                                
                                                formData.append(iName, fileData);
                                                break;
                                        }
                                    });

                                    $.ajax({
                                        type: 'POST',
                                        dataType: "json",
                                        data: fd,
                                        data: formData,
                                        processData: false,
                                        contentType: false,
                                        url: "/eo/requests/add-payment",
                                        success: function(answer) {
                                            //Проверяем если ошибка
                                            if(answer.result && answer.result=="error"){
                                                useTopHoverTitleError("Произошла ошибка при сохранении фрейма 'Изменение наименования лицензиата'");
                                                return;
                                            }
                                            //Если все хорошо
                                            if(answer.result && answer.result=="success"){
                                                $(currentPayment).parent().parent().html(
                                                        $(answer.row).html()
                                                    );
                                                $(".dialog-add-payment").remove();
                                            }
                                        }
                                    });
                                });
                            });
                        }
                    });
                }
            });
        });

            //Обработка кнопки "Изменить заявление"
            $(".btChangeRequest",generalData.container).click(function(){
                console.log('открытая - лицензироване - заявление - редактировать - тык');
                if(confirm("Вы действительно хотите изменить заявление? Информация, введенная во вкладки, может быть утеряна.")){
                    request.openAdd($(this).attr("id").split("_")[1]);
                    //$(_private.container).remove();
                }
            });

            //инициируем подфрейм выбора приложений к лицензии
            if($("#choise-supp-frame", generalData.container).length > 0){
                generalData.initChoiseSupp();
            }            
            
            console.log('лицензирование - карточка заявления - дергаем проверку подписей');
            jQuery('.request-sign-check').show();            
            jQuery('.request-sign-check').click();

        },



        /**
         * Метод cохраняет изменения в фрейме
         */
        save: function(isFromButton){
            var generalData = _private.frames.general;
            var saveData = {};
            var isError = false;

            //заявление
            var fd = {};
            $(".requestData .dataField",generalData.container).each(function(i, item){
                var iName = $(item).attr("name");
                var fType = $(item).attr("fType");

                isError =  RequestFrames.checkValueEmpty( item ) || isError;

                switch(fType){
                    case "val":
                        var iValue = $(item).val();
                        fd[iName] =iValue;
                        break;
                    case "date":
                        var iValue = $(item).val();
                        var date = moment(iValue,"DD.MM.YYYY").format("YYYY-MM-DD");
                        fd[iName] = date;
                        break;
                    case "cb":
                        if($(item).is(":checked"))
                            var iValue = 1;
                        else
                            var iValue = 0;
                        fd[iName] =iValue;
                        break;
                }
            });
            saveData.request = fd;

            //Организация
            var fd = {};
            $(".orgInfoData .dataField",generalData.container).each(function(i, item){
                var iName = $(item).attr("name");
                var fType = $(item).attr("fType");

                isError = RequestFrames.checkValueEmpty( item ) || isError;

                switch(fType){
                    case "val":
                        var iValue = $(item).val();
                        fd[iName] =iValue;
                        break;
                }
            });
            saveData.orgInfo = fd;
            if( isError ){
                return 0;
            }
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: {
                    data: JSON.stringify(saveData)
                },
                url: "/eo/requests/save-general",
                success: function(answer) {
                    //Проверяем если ошибка
                    if(answer.result && answer.result=="error"){
                        console.log(answer);
                        console.log(answer.errorDescription);
                        if( 'object' == typeof(answer.errorDescription) ){
                            for( var i in answer.errorDescription ){
                                for( var j in answer.errorDescription[i] ){
                                    var iFix = i;
                                    if( 'orgInfo' == i ){
                                        iFix = 'orgInfoData';
                                    }
                                        console.log(answer.errorDescription[i][j]);
                                        //var elementName=
                                        var str = '.'+iFix+' .dataField[name="'+j+'"]';
                                        jQuery(str, generalData.container).parent().addClass('has-error');
                                }
                            }
                        }
                        _private.frameSaved("general",answer.result,"Произошла ошибка при сохранении фрейма 'Свойства заявления'");
                        return;
                    }
                    //Если все хорошо
                    if(answer.result && answer.result=="success"){
                        var reqId = answer.id;
                        _private.frameSaved("general",answer.result);
                        _private.data.saveProcess.refreshDeal = true;
                    }
                }
            });
            
        },

        /**
         * Метод инициирует фрейм выбора приложений к лицензий
         */
        initChoiseSupp: function(){
            var generalData = _private.frames.general;
            var choiseData = {};
            choiseData.container = $("#choise-supp-frame", generalData.container);

            //Диалог выбора приложений для добавления
            $(".btChoiseSupplement", choiseData.container).click(function(){
                dialog = {
                    width: Math.round((screen.width / 100) * 82) - 20,
                    height: 520
                };
                //id заявления
                var requestId = $("#requestId", _private.container).val();
                var licenseId = $(this).parents("tr").eq(0).attr("data-lic");

                var dContent = $(
                    '<div class="dialog-choise-supplement">' +
                    "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
                    '</div>');

                var dialogTitle = "Выбор приложения";

                $(dContent).dialog({
                    title: dialogTitle,
                    width: dialog.width,
                    height: dialog.height,
                    modal: true,
                    close: function () {
                        $(".dialog-choise-supplement").remove();
                    },
                    open: function () {
                        $.ajax({
                            type: 'POST',
                            dataType: "html",
                            url: "/eo/requests/dialog-select-supplement",
                            data: {
                                requestId: requestId,
                                licenseId: licenseId
                            },
                            success: function (answer) {
                                $(dContent).html("").append(answer);

                                //Обработчик нажатия на кнопку отмена
                                $(".btCancel",dContent).click(function(){
                                    $(".dialog-choise-supplement").remove();
                                });

                                //Обработчик нажатия на кнопку Выбрать
                                $(".btAdd", dContent).click(function(){
                                    //собираем список выбранных айдишников и передаем его на сервер
                                    var suppIds = [];
                                    var rows= {};
                                    $(".selected_supp:checked",dContent).each(function(i,item){
                                        suppIds.push($(item).val())
                                        rows[$(item).val()] = $(item).attr("rowTitle");
                                    });
                                    //проверяем если были выбраны филиалы
                                    if(suppIds.length==0){
                                        useTopHoverTitleError("Нет выбранных приложений");
                                        return;
                                    }

                                    var requestId = $("#requestId", _private.container).val();
                                    //Отправляем запрос на добавление выбранных филиалов
                                    $.ajax({
                                        type: 'POST',
                                        dataType: "json",
                                        data: {
                                            suppIds : suppIds,
                                            requestId: requestId
                                        },
                                        url: "/eo/requests/add-selected-supplements",
                                        success: function(answer) {
                                            //Проверяем если ошибка
                                            if(answer.result && answer.result=="error"){
                                                useTopHoverTitleError(answer.error);
                                                return;
                                            }
                                            //Если все хорошо
                                            if(answer.result && answer.result=="success"){
                                                //Закрываем диалоги
                                                $(".dialog-choise-supplement").remove();
                                                //Добавляем
                                                //$(".supp-row",choiseData.container).remove();
                                                for(var id in rows){
                                                    $(".lic_"+licenseId+".supplements", choiseData.container).find(".supps-container").append($(
                                                        '<div class="supp-row">' +
                                                        '    <div class="remove-supp" id="s_'+id+'"></div>'+
                                                        '    <div class="title">'+rows[id]+'</div>'+
                                                        '</div>')
                                                    );
                                                }
                                            }
                                        }
                                    });

                                });

                                //Обработчик нажатия на выбрать все
                                $(".select_all", dContent).click(function(){
                                    if($(this).is(":checked")){
                                        $(".filials_container", dContent).find("input[type=checkbox]").prop("checked",true);
                                    } else {
                                        $(".filials_container", dContent).find("input[type=checkbox]").prop("checked",false);
                                    }
                                });

                                //Вешаем обработчик поиска
                                $.expr[":"].contains = $.expr.createPseudo(function(arg) {
                                    return function( elem ) {
                                        arg = arg.toLowerCase();
                                        var strText = $(elem).text().toLowerCase();
                                        var valText = $(elem).val().toLowerCase();
                                        if(
                                            strText.indexOf(arg) >= 0
                                            || valText.indexOf(arg) >= 0
                                        )
                                            return true;
                                        else
                                            return false;
                                    };
                                });

                                console.log('Обработчик ввода текста в поиск #7');
                                $('.search-input',dContent).keyup(function(e) {
                                    if(e.which == 27){
                                        clearTimeout($.data(this, 'timer'));
                                        $('.search-input',dContent).val("")
                                        search();
                                        e.stopPropagation();
                                        return;
                                    }
                                    clearTimeout($.data(this, 'timer'));
                                    var wait = setTimeout(search, 500);
                                    $(this).data('timer', wait);
                                });

                                function search(){
                                    var searchVal = $('.search-input',dContent).val();

                                    if(searchVal == ""){
                                        clearSearch();
                                        return;
                                    }

                                    $("#filteredTable .tbody-container tr", dContent).hide();
                                    $("#filteredTable .tbody-container .search_td:contains("+searchVal+")", dContent).parents("tr").show();
                                }

                                function clearSearch(){
                                    $("#filteredTable .tbody-container tr", dContent).show();
                                }
                            }
                        });
                    }
                })
            });

            //крестик удаления приложения из списка
            $(".supps-container",choiseData.container).delegate(".remove-supp","click", function(){
                var btDelete = $(this);
                if(confirm("Вы действительно хотите удалить?")){
                    var id = $(this).attr("id").split("_")[1];
                    //id заявления
                    var requestId = $("#requestId", _private.container).val();

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            suppId : id,
                            requestId: requestId
                        },
                        url: "/eo/requests/remove-supp",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                                btDelete.parents(".supp-row").remove();
                            }
                        }
                    });
                }
            });
        }
    };

    /**
     * Филиал
     */
    _private.frames.filial = {
        container: null,
        buttons: null,

        init: function(){
            var filialData = _private.frames.filial;
            filialData.container = $(".content-block.filial",_private.container);
            filialData.buttons = $(".filial", _private.buttons_container);

            //Высоту устанавливаем таблицы
            $(".filials-content",filialData.container).height(_private.content_container.height()-64);

            //клик по кнопке выбора филиала, диалог выбора филиала
            $(".btAddFilial", filialData.buttons).click(function(){
                //Функция колюэк после выбора филиала
                var fCallback = function(filialIds){
                    //id заявления
                    var requestId = $("#requestId", _private.container).val();
                    //Отправляем запрос на добавление выбранных филиалов
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            filialIds : filialIds,
                            requestId: requestId
                        },
                        url: "/eo/requests/add-selected-filials",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Закрываем диалоги
                                $(".dialog-select-filial").remove();
                                //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                                //_private.frames.functions.refreshFrames("filial");
                                _private.saveAllFrames(true);
                            }
                        }
                    });

                };

                //Функция колбэк после создания филиала
                var fCreateCallback = function(){
                    //Закрываем диалоги
                    $(".dialog-create-filial").remove();
                    $(".dialog-select-filial").remove();
                    //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                    //_private.frames.functions.refreshFrames();
                    _private.saveAllFrames(true);
                }
                _private.frames.functions.selectFilialDialog(filialData.container, fCallback, "choise", fCreateCallback);
            });

            //Клик по иконкам удаления филиала
            $(".btRemove", filialData.container).click(function(){
                if(confirm("Вы действительно хотите удалить этот филиал?")){
                    var id = $(this).attr("id").split("_")[1];

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            filialId : id
                        },
                        url: "/eo/requests/remove-filial",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                                //_private.frames.functions.refreshFrames("filial");
                                _private.saveAllFrames(true);
                            }
                        }
                    });
                }
            });
        },

        //сохранялка филиала на заявлении
        save: function(isButtonPress){
            var sendData = {};
            var isError = false;

            jQuery(".filial-item").each(function (indx, element){
                var elName = jQuery(element).attr('id');
                var val = jQuery(element).val();
                sendData[elName] = val;
                //fd.append(elName , val);
                isError = RequestFrames.checkValueEmpty( element ) || isError;
            })
            if({} == sendData){
                useTopHoverTitle('Нет данных для сохранения', 2001);
                return 0;
            }
            if( isError ){
                return 0;
            }

            var prefix = 'ЛОД - фреймы - филиалы - сохранёлка: ';
            console.info(prefix + 'начинаем отсылку');
            $.ajax({
                type: 'POST',
                //timeout: 5000,
                dataType: "json",
                //data: fd,
                data: sendData,
                //processData: false,
                //contentType: false,
                url: '/eo/affairs/save-filial',
                beforeSend: function(){

                },
                success: function(answer) {
                    var message = '';
                    if(typeof (answer.message) != 'undefined' && answer.message != ''){
                        message = ' ' + answer.message;
                    }
                    var consoleMsg = '';
                    if(typeof (answer.console) != 'undefined' && answer.console != ''){
                        consoleMsg = ' ' + answer.console;
                    }
                    if(answer.status == 'Error'){
                        if(message == ''){
                            message = "Ошибка загрузки документа";
                        }
                        console.error(prefix+'Ошибка заливки');
                        console.log(prefix + consoleMsg);
                        _private.frameSaved("filial","error", prefix + consoleMsg);
                        return ;
                    }
                    if(answer.status == 'Ok'){
                        if(message == ''){
                            message = "Сохранено";
                        }
                        console.info(prefix + 'удачно');
                        _private.frameSaved("filial","success");
                        return ;
                    }
                    if(message == ''){
                        message = "Ошибка при формирвоании";
                    }

                    _private.frameSaved("filial","error",message);
                    console.info(prefix + 'Непонятно чегото');
                    console.log(answer);
                },
                error:function(e){
                    _private.frameSaved("filial","error",prefix + 'ошибка связи при формированиии');
                },
                complete:function(){
                    console.info(prefix + 'впихивание окончено');
                }
            });
            return 0;
        }
    };

    /**
     * Создание нового филиала
     */
    _private.frames.newFilial = {
        container: null,
        init: function(){
            var filialData = _private.frames.newFilial;
            filialData.container = $(".content-block.new-filial",_private.container);
            filialData.buttons = $(".new-filial", _private.buttons_container);

            //Высоту устанавливаем таблицы
            $(".filials-content",filialData.container).height(_private.content_container.height()-45);

            //Функция колюэк после выбора филиала
            var fCallback = function(){};

            //Функция колбэк после создания филиала
            var fCreateCallback = function(){
                //Закрываем диалоги
                $(".dialog-create-filial").remove();
                $(".dialog-select-filial").remove();
                //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                //_private.frames.functions.refreshFrames();
                _private.saveAllFrames(true);
            }

            //клик по кнопке выбора филиала, диалог выбора филиала
            $(".btAddFilial", filialData.buttons).click(function(){
                _private.frames.functions.selectFilialDialog(filialData.container, fCallback, "new", fCreateCallback);
            });

            //Клик по иконкам удаления филиала
            $(".btRemove", filialData.container).click(function(){
                if(confirm("Вы действительно хотите удалить этот филиал?")){
                    var id = $(this).attr("id").split("_")[1];

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            filialId : id
                        },
                        url: "/eo/requests/remove-filial",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                                //_private.frames.functions.refreshFrames();
                                _private.saveAllFrames(true);
                            }
                        }
                    });
                }
            });
        }
    };

    /**
     * Изменение наименования лицензиата
     * @type {{container: null}}
     */
    _private.frames.changeLicTitle = {
        container: null,
        buttons: null,

        init: function(){
            var changeLicTitleData = _private.frames.changeLicTitle;
            changeLicTitleData.container = $(".content-block.change-lic-title",_private.container);
            changeLicTitleData.buttons = $(".change-lic-title", _private.buttons_container);

            //Высоту устанавливаем таблицы
            $(".filials-content",changeLicTitleData.container).height(_private.content_container.height()-45);

            //Функция колюэк после выбора филиала
            var fCallback = function(filialIds){
                //id заявления
                var requestId = $("#requestId", _private.container).val();
                //Отправляем запрос на добавление выбранных филиалов
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    data: {
                        filialIds : filialIds,
                        requestId: requestId
                    },
                    url: "/eo/requests/add-selected-filials-edit",
                    success: function(answer){
                        //Проверяем если ошибка
                        if(answer.result && answer.result=="error"){
                            useTopHoverTitleError(answer.error);
                            return;
                        }
                        //Если все хорошо
                        if(answer.result && answer.result=="success"){
                            //Закрываем диалоги
                            $(".dialog-select-filial").remove();
                            //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                            //_private.frames.functions.refreshFrames();
                            _private.saveAllFrames(true);
                        }
                    }
                });
            };

            //клик по кнопке выбора филиала, диалог выбора филиала
            $(".btAddFilial", changeLicTitleData.buttons).click(function(){
                _private.frames.functions.selectFilialDialog(changeLicTitleData.container, fCallback, "edit");
            });

            //Клик по иконкам удаления филиала
            $(".btRemove", changeLicTitleData.container).click(function(){
                if(confirm("Вы действительно хотите удалить этот филиал?")){
                    var id = $(this).attr("id").split("_")[1];

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            orgId : id
                        },
                        url: "/eo/requests/remove-filial-edit",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Перегружаем диалог редактирования так как филиалы используются во многих фреймах
                                //_private.frames.functions.refreshFrames();
                                _private.saveAllFrames(true);
                            }
                        }
                    });
                }
            });
        },
        /**
         * Метод сохраняет изменения в фрейме
         */
        save: function(isFromButton){
            var changeLicTitleData = _private.frames.changeLicTitle;
            //Проходим по всем блокам с формами
            var saveData = [];
            $(".form-block",changeLicTitleData.container).each(function(i,item){
                var formContainer = $(item);
                //Проходим по полям
                //Собираем объект с данными для добавления
                var fd = {};
                $(".dataField", formContainer).each(function(i, item){
                    var iName = $(item).attr("name");
                    var fType = $(item).attr("fType");
                    switch(fType){
                        case "val":
                            var iValue = $(item).val();
                            fd[iName] =iValue;
                            break;
                    }
                });
                saveData.push(fd);
            });
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: {
                    data: JSON.stringify(saveData)
                },
                url: "/islod/requests/change-lic-title-save",
                success: function(answer) {
                    //Проверяем если ошибка
                    if(answer.result && answer.result=="error"){
                        console.log(answer);
                        _private.frameSaved("changeLicTitle", answer.result, "Произошла ошибка при сохранении фрейма 'Изменение наименования лицензиата'");
                        return;
                    }
                    //Если все хорошо
                    if(answer.result && answer.result=="success"){
                        var reqId = answer.id;
                        _private.frameSaved("changeLicTitle", answer.result);
                    }
                }
            });
        }
    };

    /**
     * Новые ОП
     */
    _private.frames.newOP = {
        container: null,
        buttons: null,

        init: function(){
            var newOPData = _private.frames.newOP;
            newOPData.container = $(".content-block.new-op",_private.container);
            newOPData.buttons = $(".new-op", _private.buttons_container);

            //Обработчик смены фииала
            $(newOPData.container).delegate(".change_fill", "click", function () {
                $(".change_fill.active_fil", newOPData.container).parents("li").eq(0).find("ul").remove();

                $(".change_fill", newOPData.container).removeClass('active_fil');
                $(this).addClass('active_fil');
                $(".change_fill.active_fil", newOPData.container).parents("li").eq(0).append($("<ul><li>- Загрузка...</li></ul>"));

                _private.frames.functions.changeFilial(newOPData.container, "new-op");
            });

            //Обработчик смены уровня
            $(newOPData.container).delegate(".level-elem", "click", function () {
                $(".level-elem", newOPData.container).removeClass('active-level');
                $(this).addClass('active-level');

                _private.frames.functions.changeLevel(newOPData.container, "new-op");
            });

            // сделаем активной первый филиал
            //$('.change_fill:first', newOPData.container).trigger('click');

            //Обработчик нажатия на кнопку
            $(".selectLevels", newOPData.buttons).click(function(){
                console.log( 'хотим рон - карточка заявления - новые оп - загрузка диалога выбора уровней образования  22' );
                _private.frames.functions.selectOPLevel(newOPData.container, "new-op");
            });

            _private.frames.functions.initOPHandlers(newOPData.container,"new-op");
        },

        onTab: function(){
            var newOPData = _private.frames.newOP;
            // сделаем активной первый филиал
            $('.change_fill:first', newOPData.container).trigger('click');
        }
    };

    /**
     * Текущие ОП
     */
    _private.frames.currentOP = {
        container: null,
        buttons: null,

        init: function(){
            var currentOPData = _private.frames.currentOP;
            currentOPData.container = $(".content-block.current-op",_private.container);
            currentOPData.buttons = $(".current-op", _private.buttons_container);

            //Обработчик смены фииала
            $(currentOPData.container).delegate(".change_fill", "click", function () {
                $(".change_fill.active_fil", currentOPData.container).parents("li").eq(0).find("ul").remove();

                $(".change_fill", currentOPData.container).removeClass('active_fil');
                $(this).addClass('active_fil');
                $(".change_fill.active_fil", currentOPData.container).parents("li").eq(0).append($("<ul><li>- Загрузка...</li></ul>"));

                _private.frames.functions.changeFilial(currentOPData.container, "current-op");
            });

            //Обработчик смены уровня
            $(currentOPData.container).delegate(".level-elem", "click", function () {
                $(".level-elem", currentOPData.container).removeClass('active-level');
                $(this).addClass('active-level');

                _private.frames.functions.changeLevel(currentOPData.container, "current-op");
            });

            // сделаем активной первый филиал
            $('.change_fill:first', currentOPData.container).trigger('click');

            //Обработчик нажатия на кнопку
            $(".selectOp", currentOPData.buttons).click(function(){
                _private.frames.functions.addOPDialog(currentOPData.container, "current-op");
            });

            _private.frames.functions.initOPHandlers(currentOPData.container,"current-op");
        }
    };

    /**
     * Прекращение ОП
     */
    _private.frames.stopOP = {
        container: null,
        buttons: null,

        init: function(){
            var stopOPData = _private.frames.stopOP;
            stopOPData.container = $(".content-block.stop-op",_private.container);
            stopOPData.buttons = $(".stop-op", _private.buttons_container);

            //Обработчик смены фииала
            $(stopOPData.container).delegate(".change_fill", "click", function () {
                $(".change_fill.active_fil", stopOPData.container).parents("li").eq(0).find("ul").remove();

                $(".change_fill", stopOPData.container).removeClass('active_fil');
                $(this).addClass('active_fil');
                $(".change_fill.active_fil", stopOPData.container).parents("li").eq(0).append($("<ul><li>- Загрузка...</li></ul>"));

                _private.frames.functions.changeFilial(stopOPData.container, "stop-op");
            });

            //Обработчик смены уровня
            $(stopOPData.container).delegate(".level-elem", "click", function () {
                $(".level-elem", stopOPData.container).removeClass('active-level');
                $(this).addClass('active-level');

                _private.frames.functions.changeLevel(stopOPData.container, "stop-op");
            });

            // сделаем активной первый филиал
            $('.change_fill:first', stopOPData.container).trigger('click');

            //Обработчик нажатия на кнопку
            $(".selectOp", stopOPData.buttons).click(function(){
                _private.frames.functions.addOPDialog(stopOPData.container, "stop-op");
            });

            _private.frames.functions.initOPHandlers(stopOPData.container, "stop-op");
        }
    };


    /**
     * Изменение ОП
     */
    _private.frames.changeOpName = {
        container: null,
        buttons: null,

        init: function(){
            var changeOPData = _private.frames.changeOpName;
            changeOPData.container = $(".content-block.change-op-name",_private.container);
            changeOPData.buttons = $(".change-op-name", _private.buttons_container);

            //Обработчик смены фииала
            $(changeOPData.container).delegate(".change_fill", "click", function () {
                $(".change_fill.active_fil", changeOPData.container).parents("li").eq(0).find("ul").remove();

                $(".change_fill", changeOPData.container).removeClass('active_fil');
                $(this).addClass('active_fil');
                $(".change_fill.active_fil", changeOPData.container).parents("li").eq(0).append($("<ul><li>- Загрузка...</li></ul>"));

                _private.frames.functions.changeFilial(changeOPData.container, "change-op");
            });

            //Обработчик смены уровня
            $(changeOPData.container).delegate(".level-elem", "click", function () {
                $(".level-elem", changeOPData.container).removeClass('active-level');
                $(this).addClass('active-level');

                _private.frames.functions.changeLevel(changeOPData.container, "change-op");
            });

            // сделаем активной первый филиал
            //$('.change_fill:first', newOPData.container).trigger('click');

            //Обработчик нажатия на кнопку
            $(".selectLevels", changeOPData.buttons).click(function(){
                console.log( 'хотим рон - карточка заявления - новые оп - загрузка диалога выбора уровней образования  44' );
                _private.frames.functions.selectOPLevel(changeOPData.container, "change-op");
            });

            _private.frames.functions.initOPHandlers(changeOPData.container, "change-op");
        },

        onTab: function(){
            var changeOPData = _private.frames.changeOpName;
            // сделаем активной первый филиал
            $('.change_fill:first', changeOPData.container).trigger('click');
        }
    };

    /**
     * Меcта осуществления ОД
     */
    _private.frames.placesOD = {
        container: null,
        buttons: null,

        init: function(){
            var placesODData = _private.frames.placesOD;
            placesODData.container = $(".content-block.places-od",_private.container);
            placesODData.buttons = $(".places-od", _private.buttons_container);

            //Обработчик смены филиала
            $(placesODData.container).delegate(".filial", "click", function () {
                //сохраняем текущие данные
                _private.saveCurrentFrame();
                //
                $(".filial", placesODData.container).removeClass('active');
                $(this).addClass('active');

                _private.frames.functions.changePlacesFilial(placesODData.container, "places");
            });

            // сделаем активной первый филиал
            $('.filial:first', placesODData.container).trigger('click');

            //Обработчик кнопки добавления адреса
            $(".btAddAddr", placesODData.buttons).click(function(){
                _private.frames.functions.addODPlace(placesODData.container);
            });

            //Обработчик кнопки выбора адресов из диалога
            $(".btChoiseAddr", placesODData.buttons).click(function(){
                _private.frames.functions.selectAddressDialog(placesODData.container, "places");
            });

            //Клик по иконкам удаления филиала
            $(placesODData.container).delegate(".btRemove","click", function(){
                if(confirm("Вы действительно хотите удалить этот адрес?")){
                    var id = $(this).attr("id").split("_")[1];
                    //Если этот адрес еще не был сохранен на сервер, просто удаляем строку
                    if(id=="self"){
                        $(this).parents("tr").eq(0).remove();
                        //пересчитываем порядковые номера
                        _private.frames.functions.recalculateODPlacesNumbers(placesODData.container);
                        return;
                    }

                    var selfBt = $(this);

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            adrId : id
                        },
                        url: "/eo/requests/remove-od-address",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Удаляем строку
                                selfBt.parents("tr").eq(0).remove();
                                //Уменьшаем счетчик слева
                                var cCounter = $(".frame-container .filial.active",placesODData.container).parent().find(".anket-programs-counter");
                                cCounter.text(parseInt(cCounter.text())-1);
                                //пересчитываем порядковые номера
                                _private.frames.functions.recalculateODPlacesNumbers(placesODData.container);
                            }
                        }
                    });
                }
            });

            //обработчики поиска
            _private.frames.functions.initODSearch(placesODData.container);
        },

        //Метод сохраняет текущие изменения в адресах
        save: function(isFromButton){
            var placesODData = _private.frames.placesOD;
            _private.frames.functions.placesAddrSave(placesODData.container, "places", isFromButton)
        }
    };
    
    /**
     * Открытие образовательной деятельности
     */
    _private.frames.openOD = {
        container: null,
        buttons: null,

        init: function(){
            var openODData = _private.frames.openOD;
            openODData.container = $(".content-block.open-od",_private.container);
            openODData.buttons = $(".open-od", _private.buttons_container);

            //Обработчик смены филиала
            $(openODData.container).delegate(".filial", "click", function () {
                //сохраняем текущие данные
                _private.saveCurrentFrame();
                //
                $(".filial", openODData.container).removeClass('active');
                $(this).addClass('active');

                _private.frames.functions.changePlacesFilial(openODData.container, "open");
            });

            // сделаем активной первый филиал
            $('.filial:first', openODData.container).trigger('click');

            //Обработчик кнопки добавления адреса
            $(".btAddAddr", openODData.buttons).click(function(){
                _private.frames.functions.addODPlace(openODData.container);
            });

            //Обработчик кнопки выбора адресов из диалога
            $(".btChoiseAddr", openODData.buttons).click(function(){
                _private.frames.functions.selectAddressDialog(openODData.container, "open");
            });

            //Клик по иконкам удаления филиала
            $(openODData.container).delegate(".btRemove","click", function(){
                if(confirm("Вы действительно хотите удалить этот адрес?")){
                    var id = $(this).attr("id").split("_")[1];
                    //Если этот адрес еще не был сохранен на сервер, просто удаляем строку
                    if(id=="self"){
                        $(this).parents("tr").eq(0).remove();
                        //пересчитываем порядковые номера
                        _private.frames.functions.recalculateODPlacesNumbers(openODData.container);
                        return;
                    }

                    var selfBt = $(this);

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            adrId : id
                        },
                        url: "/eo/requests/remove-od-address",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Удаляем строку
                                selfBt.parents("tr").eq(0).remove();
                                //Уменьшаем счетчик слева
                                var cCounter = $(".frame-container .filial.active",openODData.container).parent().find(".anket-programs-counter");
                                cCounter.text(parseInt(cCounter.text())-1);
                                //пересчитываем порядковые номера
                                _private.frames.functions.recalculateODPlacesNumbers(openODData.container);
                            }
                        }
                    });
                }
            });

            //обработчики поиска
            _private.frames.functions.initODSearch(openODData.container);
        },

        //Метод сохраняет текущие изменения в адресах
        save: function(isFromButton){
            var openODData = _private.frames.openOD;
            _private.frames.functions.placesAddrSave(openODData.container, "open", isFromButton)
        }
    };
    
    /**
     * Прекращение образовательной деятельности
     */
    _private.frames.stopOD = {
        container: null,
        buttons: null,

        init: function(){
            var stopODData = _private.frames.stopOD;
            stopODData.container = $(".content-block.stop-od",_private.container);
            stopODData.buttons = $(".stop-od", _private.buttons_container);

            //Обработчик смены филиала
            $(stopODData.container).delegate(".filial", "click", function () {
                //сохраняем текущие данные
                _private.saveCurrentFrame();
                //
                $(".filial", stopODData.container).removeClass('active');
                $(this).addClass('active');

                _private.frames.functions.changePlacesFilial(stopODData.container, "close");
            });

            // сделаем активной первый филиал
            $('.filial:first', stopODData.container).trigger('click');

            //Обработчик кнопки выбора адресов из диалога
            $(".btChoiseAddr", stopODData.buttons).click(function(){
                _private.frames.functions.selectAddressDialog(stopODData.container, "close");
            });

            //Клик по иконкам удаления филиала
            $(stopODData.container).delegate(".btRemove","click", function(){
                if(confirm("Вы действительно хотите удалить этот адрес?")){
                    var id = $(this).attr("id").split("_")[1];
                    //Если этот адрес еще не был сохранен на сервер, просто удаляем строку
                    if(id=="self"){
                        $(this).parents("tr").eq(0).remove();
                        //пересчитываем порядковые номера
                        _private.frames.functions.recalculateODPlacesNumbers(openODData.container);
                        return;
                    }

                    var selfBt = $(this);

                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            adrId : id
                        },
                        url: "/eo/requests/remove-od-address",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError(answer.error);
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Удаляем строку
                                selfBt.parents("tr").eq(0).remove();
                                //Уменьшаем счетчик слева
                                var cCounter = $(".frame-container .filial.active",openODData.container).parent().find(".anket-programs-counter");
                                cCounter.text(parseInt(cCounter.text())-1);
                                //пересчитываем порядковые номера
                                _private.frames.functions.recalculateODPlacesNumbers(openODData.container);
                            }
                        }
                    });
                }
            });

            //обработчики поиска
            _private.frames.functions.initODSearch(stopODData.container);
        }
    };
    
    /**
     * Реорганизованный лицензиат
     */
    _private.frames.reorgLic = {
        container: null,
        buttons: null,

        init: function(){
            var reorgLicData = _private.frames.reorgLic;
            reorgLicData.container = $(".content-block.reorg-lic",_private.container);
        },

        //Метод сохраняет поля
        save: function(isFromButton){
            var reorgLicData = _private.frames.reorgLic;

            //Если организация не выбрана, то не запускаем сохранялку
            var reorgId = $("#Id_organization",reorgLicData.container).val();
            if(reorgId == ""){
                _private.frameSaved("reorgLic", "error", "Реорганизованная организация не выбрана");
                return;
            }


            //Сохранение
            //Проходим по всем элементам формы
            var fd = {};
            $(".dataField", reorgLicData.container).each(function(i, item){
                var iName = $(item).attr("name");
                var fType = $(item).attr("fType");
                switch(fType){
                    case "val":
                        var iValue = $(item).val();
                        fd[iName] =iValue;
                        break;
                }
            });

            //Отправляем на сервер
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: fd,
                url: "/islod/requests/reorg-lic-save",
                success: function(answer) {
                    //Проверяем если ошибка
                    if(answer.result && answer.result=="error"){
                        _private.frameSaved("reorgLic", answer.result,"Произошла ошибка при сохранении фрейма");
                        return;
                    }
                    //Если все хорошо
                    if(answer.result && answer.result=="success"){
                        _private.frameSaved("reorgLic", answer.result);
                    }
                }
            });
        }
    }

    /**
     * Реорганизация
     */
    _private.frames.reorg = {
        container: null,
        buttons: null,

        init: function(){
            var reorgData = _private.frames.reorg;
            reorgData.container = $(".content-block.reorg",_private.container);
            reorgData.buttons = $(".reorg", _private.buttons_container);

            //Высоту устанавливаем таблицы
            $(".reorgs-content",reorgData.container).height(_private.content_container.height()-45);

            //Выбор диалога
            $(".btAddOrg", reorgData.buttons).click(function(){
                _private.saveCurrentFrame();
                request.selectOrgDialog(function(orgId){
                    var num = parseInt($(".reorgs-content tr", reorgData.container).last().find("td").first().text());
                    var dealId = $("#dealId",reorgData.container).val();
                    var reqOrgId = $("#orgId",reorgData.container).val();

                    //Отправляем на сервер
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            organizationId: reqOrgId,
                            reorganizedOrganizationId: orgId,
                            requestId: $("#requestId", _private.container).val()
                        },
                        url: "/islod/requests/add-reorg",
                        success: function(answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError("Произошла ошибка при сохранении фрейма");
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                //Перегружаем диалог редактирования
                                //_private.frames.functions.refreshFrames("reorg");
                                _private.saveAllFrames(true);
                                //reorgData.refreshContent();
                            }
                        }
                    });
                });
            });

            //обработчик кнопки удаления
            $(".reorgs-content",reorgData.container).delegate(".btRemove","click", function(){
                if(confirm("Вы действительно хотите удалить запись?")) {
                    var self = $(this);
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: "/islod/requests/reorg-remove",
                        data: {
                            reorgId: self.attr("id").split("_")[1]
                        },
                        success: function (answer) {
                            //Проверяем если ошибка
                            if(answer.result && answer.result=="error"){
                                useTopHoverTitleError("Произошла ошибка при удалении");
                                return;
                            }
                            //Если все хорошо
                            if(answer.result && answer.result=="success"){
                                self.parents("tr").eq(0).remove();
                                //Перегружаем диалог редактирования
                                //_private.frames.functions.refreshFrames("reorg");
                                _private.saveAllFrames(true);
                            }
                        }
                    });
                }
            });
        },

        refreshContent: function(){
            var reorgData = _private.frames.reorg;
            $(".reorgs-content",reorgData.container).html("<tr><td colspan='5'><div class='loading-img' style='margin-top:0px;text-align: center;'><img src='/img/loading.gif' /></div></td></tr>");

            //отправляем запрос на список оп

            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/islod/requests/get-reorg-list",
                data: {
                    requestId: $("#requestId", _private.container).val()
                },
                success: function (answer) {
                    $(".reorgs-content",reorgData.container).html(answer);
                }
            });
        }
    }
    
    
    /**
     * Документы
     */
    _private.frames.document = {
        container: null,
        buttons: null,

        init: function(){
            var documentData = _private.frames.document;
            documentData.container = $(".content-block.document",_private.container);
            documentData.buttons = $(".document", _private.buttons_container);
            
            jQuery( documentData.buttons ).delegate( ".btAddDocumentDop" , "click" , function(){                
                console.log( ' тык доп док ' ); 
                request.documentDopAdd();
            });
        }
    }

    /**=============================================================
     * Публичные методы
     *=============================================================*/

    /**
     * Метод инициализирует работу с диалогом
     */
    this.init = function(){
        _private.container = $(".dialog-request-edit");
        _private.frames.current = "general";
        _private.initGeneralHandlers();
    };

    /**
     * Метод вызывает сохранение извне
     */
    this.save = function(notButton){
        console.log("Обработчик сохранения текущего фрейма");

        /*var fromButton = true;
        if(notButton)
            fromButton = false;
            */
        //_private.saveCurrentFrame(fromButton);
        _private.saveAllFrames();
    }

    /**
     * Метод через которые задаем данные для диалога выбора оп
     */
    this.setOPDialogData = function(data){
        _private.frames.OPLevelsData = data;
        console.log(data);
    };

    this.setData = function(dataName, data){
        _private.data[dataName] = data;
    }

    this.getData = function(dataName){
        return _private.data[dataName];
    }

    this.needRefreshDeal = function(){
        return _private.data.saveProcess.refreshDeal;
    }
    
    /**
     * проверим значение на пустое
     * 
     * @param {type} item     - елемент, который курим
     * @param {type} hasError - уже ошибка? 
     * @returns {Boolean}
     */
    this.checkValueEmpty = function( item , hasError){
        if( 'boolean' != typeof( hasError) ){
            hasError = false;
        }
        jQuery(item).parent().find('.dynamic-flash').remove();
        jQuery(item).parent().removeClass('has-error');
        var isRequired = jQuery( item ).is( '[required]' );
        if( isRequired  ){
            var val = jQuery(item).val();
            val = jQuery.trim(val);
            if( 'udefined' == typeof(val) || '' == val){
                jQuery(item).parent().addClass('has-error');   
                jQuery(item).parent().prepend('<div class="form-control-error-flash dynamic-flash">Введите значение!</div>')                        
                hasError = true;
                useTopHoverTitleError('Обязательные поля не заполнены')
            }
        }    
        return hasError;
    }

    
};
var RequestFrames = new RequestFrames();