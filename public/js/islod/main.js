var isHashChangerClick = false;

$(document).ready(function(){
    if( 'undefined' != typeof(flagLastMainLoadjsIslod) ){
        return 0;
    }
    flagLastMainLoadjsIslod = true;
    console.info('запускаем обработчики загрузки /js/islod/main.js');

    //Вешаем обработчик для кнопок шагов
    $(".btn",".steps_div").click(function(){
        //меняем внешний вид
        $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
        $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
    });

    bindIndexEvents();
    eventTopHoverClick();

    $(window).unbind('hashchange');
    $(window).bind('hashchange', function(){
        if(isHashChangerClick){
            isHashChangerClick = false;
            return;
        }
        if(window.location.hash){
            $("a[href='"+window.location.hash+"']").parent().click();
        }else{
            window.location.hash = $(".btn-lg").attr('event');
            var event=$(".btn>a",".steps_div").first().attr('event');
            var controller=$(".btn>a",".steps_div").first().attr('controller');
            indexEventsAjaxHandler( $(".btn>a",".steps_div").first(), event, controller );
        }
    });
    if(window.location.hash){
        $("a[href='"+window.location.hash+"']").parent().click();
    }else{
        window.location.hash = $(".btn-lg").attr('event');
        var event=$(".btn>a",".steps_div").first().attr('event');
        var controller=$(".btn>a",".steps_div").first().attr('controller');
        indexEventsAjaxHandler($(".btn>a",".steps_div").first(), event,controller);
    }

    $(window).resize(function(){
        resizeContent($(".container .viewzone"));
        specialTableResize();
    });

    console.log('Обработчик скрытия левого меню /public/js/islod/main.js');
    $(".steps_td").on('click',".toggleButton",function(){
        $('body').toggleClass('icons');
        $('#head').toggleClass('icons');
        $('.route .steps_td').toggleClass('icons');
        $('.steps_td .nav li').toggleClass('icons');
        $('.steps_td .nav li a').toggleClass('icons');
        $('.steps_td .nav li a span.num').toggleClass('icons');
        $(this).toggleClass('icons');
        if($(this).hasClass('icons')){
            setCookie('menu','icons');
        }else{
            setCookie('menu','');
        }
    });

});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function bindIndexEvents() {
    console.log('Вешаем обработчик на переключение вкладок /public/js/islod/main.js');
    //проблема - этот обработчик вешается много раз, а потом запускается множество раз
    // если его сбрасывать, то он убивается и во время обработки, решиб пооставить влаг
   if( 'undefined' != typeof(bindIndexFlagjsIslodMain) ){
        console.log('уже есть');
        return 0;
    }
    bindIndexFlagjsIslodMain = true;
    $('.btn').unbind('click');
    $('.btn').click(function() {
        isHashChangerClick = true;
        var event = $(this).attr('event');
        var _this = this;
        var controller = $(this).attr('controller');
        if (event) {
            $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
            $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
            indexEventsAjaxHandler(_this, event,controller);
        }
    });
    console.log('повешен');
}

function indexEventsAjaxHandler(_this, event ,keep_selector) {
    console.log('обработчик тыка по елементам меню слева 3');
    if (!event) return false;
        var destination_link = '/eo/'+keep_selector+'/' + event;

    $.ajax({
        type: 'POST',
        url: destination_link,
        beforeSend: function(){
            //ресайзимблок с контентом
			resizeContent($(".viewzone"));
			$(".content_td .content").html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
            //$(".content_td .content").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);            //

        },
        success: function(answer) {

            $(".content_td .content .loading-img").fadeOut(500, function(){
                answer = $(answer)

                $(".content_td .content").html("").append(answer);
                resizeContent($(".viewzone", answer));
                resizeContent($(".viewzone"));
                specialTableResize();
                event = event.split("-").join("");
                //проверяем на установлено уже или нет
                //надо для если устанавливается при автопереходе

                if(jQuery('.active_appl').length<1){


                //если это переходы по шагам, то выделяем в таблице первую запись
                    if(event.indexOf("step/") != -1 || event == "declarationreview"){
                        $(".viewzone .intable tbody.intable_content tr, .viewzone .intable tbody.content tr").first().addClass("active_appl");
                        //вешаем обработчик по дабл клику
                        $(".content_td").undelegate(".active_appl", "click").delegate(".active_appl", "click", function(){
                            var id = $(this).attr("id").split("_")[1];
                            request.openEdit();
                        });
                        if(keep_selector == "affairs"){
                            if(initFilterHandlers)
                                initFilterHandlers(answer);
                        }
                        return;
                    }
                    if(event == "expertise"){
                        $(".viewzone .intable tbody.content tr").first().addClass("active_appl");
                        //вешаем обработчик по дабл клику
                        $(".content_td").undelegate(".active_appl", "click").delegate(".active_appl", "click", view_expertise);
                    }
                    if(event == "experts" || event == "expertsnew" || event == "expertsorg" || event == "dieexperts" || event == "failexperts"){
                        $(".viewzone .intable tbody.content tr").first().addClass("active_appl");
                        //вешаем обработчик по дабл клику
                        $(".content_td").undelegate(".active_appl", "click").delegate(".active_appl", "click", view_expert_card);
                    }
                    if( event == "excerpt/licenses/1" ){
                        $('body').off('click','.page-excert');
                        $('body').on('click','.page-excert',function(){
                            var num = $(this).html();
                            excerpt.licensePage(num);
                        });
                        $('body').off('click', '.button.excerpt-this');
                        $('body').on('click', '.button.excerpt-this' ,function(){
                            excerpt.excerptThis();
                        });
                        event = 'excerpt';
                    }
                    if(event == "agreements"  || event == "agreements/organisations" || event == "agreements/experts" ){
                        $(".viewzone .intable tbody.content tr").first().addClass("active_appl");
                        //вешаем обработчик по дабл клику
                        $(".content_td").undelegate(".active_appl", "dblclick").delegate(".active_appl", "dblclick", agreementsEdit);
                    }
                }

                //Запускаем обработчики
                event = event.replace('/','');
                if(keep_selector=="affairs"){
                    var handlerName = "indexAffairHandlers";
                    affairs.getReasonForProcedure($("#procedureSelect").val(),$("#reasonHiddenInput").val());
                }else{
                    var handlerName = keep_selector+event.charAt(0).toUpperCase() + event.substr(1)+"Handlers";
                }
                console.log(handlerName);

                if(window[handlerName])
                    window[handlerName]();
            })
        },
        error:function(e){
            //сбросим сообщения
            useTopHoverTitle("",1);
            useTopHoverTitleError("Ошибка связи");
            $(".content_td .content .loading-img").fadeOut(500, function(){

            });
            console.error('ошибка связи при переходе по меню');
            console.log(e);
            console.log(e.responseText);
        }
    });
}

function resizeContent(content){
    var wHeight = $(window).height();
    var delta = 190;
    if($(content).attr("data-heightdelta"))
        delta -= parseInt($(content).attr("data-heightdelta"));

    var contentHeight = wHeight - delta;
    $(content).height(contentHeight);
}
/*специальный ресайз для таблиц что бы прокрутка была только для tbody*/
function specialTableResize(parent_el){
    if (parent_el === undefined) {
        parent_el = '';
    }
    var viewzonHeight = $(parent_el+" .viewzone").height();
    var headHeight = $(parent_el+" .viewzone .intable thead").height();
    var needHeight = viewzonHeight - headHeight;
    var tbodyWidth = $(parent_el+" .viewzone .intable tbody.content").outerWidth();
    var trWidth = $(parent_el+" .viewzone .intable tbody.content tr").outerWidth();
    var realwidth = tbodyWidth - trWidth;
    $(parent_el+" .viewzone .intable thead").css("width", "calc( 100% - " + realwidth + "px )");
    $(parent_el+" .viewzone .intable tbody").height(needHeight);
}
/**
 * возвращает количество пикселей при проценте от текущего размера экрана
 * @param {type} percent
 * @returns {
 *             width
 *             height
 *          }
 */
function percentageOfTheScreen( percent ){
    var percent = percent || 100;
    return {
            width : Math.round( (screen.width/100)*percent  ),
            height: Math.round( (screen.height/100)*percent )
        };
}
/**
 * В специальной верхней зоне выводит текст из параметра text,
 * и прячет его через время указанное в милисекундах в параметре time
 *
 * @param string text
 * @param int time
 */
function useTopHoverTitle(text, time) {
    if(typeof(time) == 'undefined'){
        time = 2000;
    }
    if(typeof(time) == 'string'){
        time = parseInt(time);
    }
    if( isNaN(time) ){
        time = 2000;
    }
    $("#hover-title").html(text);
    if( $("#hover-title:hidden") ){
        //чтоб не блымало
        $("#hover-title").fadeIn("fast");
    }
    if( 'undefined' != typeof(hoverTimerId)){
        //если уже запущен таймер, аннулируем его, чтоб не пропадало
        clearTimeout(hoverTimerId);
    }
    if( time > 0 ){
        hoverTimerId = setTimeout(function () {
            $("#hover-title").fadeOut('fast');
        }, time);
    }
}

/**
 * В специальной верхней зоне выводит текст из параметра text,
 * и прячет его через время указанное в милисекундах в параметре time
 * Для ошибки
 *
 * @param string text
 * @param int time
 */
function useTopHoverTitleError(text, time) {
    if(typeof(time) == 'undefined'){
        time = 2000;
    }
    if(typeof(time) == 'string'){
        time = parseInt(time);
    }
    if( isNaN(time) ){
        time = 2000;
    }
    if( !$("#hover-title-error").length ){
        jQuery('body').prepend(' <div id="hover-title-error"></div>');
    }
    $("#hover-title-error").html(text);
    $("#hover-title-error").fadeIn("fast");
    setTimeout(function () {
        $("#hover-title-error").fadeOut('fast');
    }, time);
}

/**
 * Вешаем событие на клик по всплывающему окну.
 */
function eventTopHoverClick() {
    $("body").undelegate("#hover-title", "click");
    $("body").delegate("#hover-title", "click", function () {
        $(this).toggle();
    });
}
/**
 * Вешает событие на смену сортировки в таблицах
 *
 * @param string ident - передается внешний идентификатор внутри которого есть tr c классом sortable
 * @param srting main - имя функции которую нужно вызвать после смены соортировки, данные о поле сортировки записываются в #sort_field - общий для всех прописан layout
 * @returns устанавливает значение сортировке в виде "Name ASC"
 */
changeSortField = function (ident,main) {
    $("body").undelegate(ident+" .sortable", "click");
    $("body").delegate(ident+" .sortable", "click", function () {
        if ($(this).hasClass("sort_desc")) {
            $(ident+" .sortable").removeClass("sort_asc");
            $(ident+" .sortable").removeClass("sort_desc");
            $(ident+" .sortable").addClass("sort_both");
            $(this).removeClass("sort_both");
            $(this).addClass("sort_asc");
            $(" #sort_field").val($(this).attr("field-name") + " ASC");
        } else {
            if ($(this).hasClass("sort_both")) {
                $(".sortable").removeClass("sort_asc");
                $(".sortable").removeClass("sort_desc");
                $(".sortable").addClass("sort_both");
                $(this).removeClass("sort_both");
                $(this).addClass("sort_asc");
                $(" #sort_field").val($(this).attr("field-name") + " ASC");
            } else {
                if ($(this).hasClass("sort_asc")) {
                    $(ident+" .sortable").removeClass("sort_asc");
                    $(ident+" .sortable").removeClass("sort_desc");
                    $(ident+" .sortable").addClass("sort_both");
                    $(this).removeClass("sort_both");
                    $(this).addClass("sort_desc");
                    $(" #sort_field").val($(this).attr("field-name") + " DESC");
                }
            }

        }
        if(main === true){
            sort = $("#sort_field").val();
            _serverQuery(1);
        }else{
            sort = $("#sort_field").val();
            eval(main+"()");
        }

    });
};

/**
 * Посылает синхронный пост запрос с указанными параметрами
 * @param {type} url
 * @param {type} postData
 * @param {type} NoSuccesMessage
 * @returns {Object|String|data|undefined}
 */
function _noAsyncPostRequest(url,postData,NoSuccesMessage){
    var url = url || '#',
        postData = postData || {},
        NoSuccesMessage = NoSuccesMessage || 'Ошибка: перезагрузите страницу, и попробуйте еще раз!',
        retData = undefined;

    $.ajax({
            type: 'POST',
            url: url,
            data: postData,
            async:false,
            success:function (data,status){
                if( status !='success'){
                    useTopHoverTitleError( NoSuccesMessage );
                     console.log(status,data);
                }else{
                    retData = data;
                }
            }
    });

    return retData;
}

/**
 * Проверяет есть ли пустота в полях с выбранными селекторами
 * @param {type} selectorArr
 * @param {type} emptyCallbck
 * каллбэк для пустых полей получает пустое поле
 * @param {type} noEmptyCallbck
 * каллбэк для не пустых полей получает не пустое поле
 * @returns {Boolean}
 */
function _requiredFieldsHasEmpty( selectorArr ,emptyCallbck ,noEmptyCallbck){
    var hasEmpty = false;

    for( var key in selectorArr){
        var val = selectorArr[key];

        if( $(val).length == 0){
            console.log('Error: element('+val+') does not exsist!');
            hasEmpty = true;
        }else{
            $(val).each(function(i,el){
                if( $(el).val().split(' ')[0] == ""){
                    hasEmpty = true;

                    if( typeof(emptyCallbck) == 'function'){
                        emptyCallbck(el);
                    }
                }else{

                    if( typeof(noEmptyCallbck) == 'function'){
                        noEmptyCallbck(el);
                    }
                }
            });
        }
    }
    return hasEmpty;
}

/*
 * mvc система на js и вспомогательные функции
 */
/**
delete all space
@param {string} str
@return {string}
@author Ruslan Gg
*/
function delSpaces(str){

	if( typeof(str) !== 'string' ){
		throw new TypeError('Unexpect argument type!');
	}

	str = str.split(' ');
	var returnStr = '';

	if( str.length > 1){
		for( var temp in str ){
			returnStr += str[temp];
		}
	}else{
		returnStr = str[0];
	}

	return returnStr;
}

/**
check on empty
@param data
@requires delSpaces()
@return {boolean}
@author Ruslan Gg
*/
function empty(data){

	switch( typeof(data) ){
		case 'string':
			data = delSpaces(data);

			if( data.length === 0 ){
				return true;
			}
		break;
		case 'undefined':
			return true;
		break;
		case 'object':
            if( data == null){
                return true;
            }

            var len = Object.keys( data ).length;

			if( len === 0 ){
				return true;
			}
		break;
	}
    return false;
}

/**
 * Замещает стандартные настройки текущими
 * @param {type} defaultObject
 * @param {type} providerObject
 * @returns {Boolean}
 */
function settingSetter( defaultObject, providerObject){
    var
        defObjType = typeof(defaultObject),
        providerObject = providerObject || {};

    if(
        defObjType != 'object'
    ){
       console.log('Error: Undefined data type! ('+defObjType+')');
       return false;
    }

    for( var defKey in defaultObject ){
        var curentData = defaultObject[ defKey ];

        if( !empty( providerObject[ defKey])){
            var curentProviderData = providerObject[ defKey],
                curDefType = typeof( curentData),
                curProvType = typeof( curentProviderData);

            if(
                curDefType == 'object' &&
                curProvType == 'object'
            ){
                var lenCur = Object.keys( curentData ).length,
                    provCur = Object.keys( curentProviderData ).length;
                if(
                        lenCur == 0 &&
                        provCur > 0
                ){
                    defaultObject[ defKey ] = curentProviderData;
                }else{
                    settingSetter( curentData ,curentProviderData);
                }

            }else{

                if(
                    curDefType == 'object' &&
                    curProvType != 'object'
                ){
                    console.log( 'Warning: It was assumed that the provider will include setting!');
                }

                defaultObject[ defKey ] = curentProviderData;
            }
        }
    }

    return true;
}

/**
 * Проверка на верхний регистр
 * @param {type} $str
 * @returns {Boolean}
 */
function isUpper($str)
{
    if ($str === $str.toUpperCase()){
        return true;
    }else{
       return false;
    }
}

/**
 * функция создающие преднастроенный стандартный диалог
 * @param {type} setting
 * {
            className : "имя класса который добавляеться в боду, и на основе которого создаеться дилог",
            data: "данные для вставки в окно",
            title: "заголовок",
            buttons: -,
            binder: объект, либо фабрика возвращающая объект с методами bind и unbind
    }
 * @returns {undefined}
 */
function createStandartDialog( setting){
    var dialog = percentageOfTheScreen( 84 ),
        _setting = {
            className : 'unknow',
            data:'',
            title:'unnamed',
            buttons:{},
            binder:{},
            afterClose:null,
            width:dialog.width,
            height:dialog.height,
            preCreated:null,
            modal: true
        };

        settingSetter( _setting, setting);

        if( typeof( _setting['preCreated'] ) == 'function'){
                    _setting['preCreated'](_setting);
        }

        var selClass = '.'+_setting['className'];
        $('body').append('<div class="'+_setting['className']+'"></div>');
        return $( selClass).dialog({
            title: _setting['title'],
            width: _setting['width'],
            height: _setting['height'],
            modal: _setting['modal'],
            close: function () {

                if( !empty( _setting['binder'].unbind )){
                    _setting['binder'].unbind();
                }

                $( selClass).remove();

                if( typeof( _setting['afterClose'] ) == 'function'){
                    _setting['afterClose']();
                }
            },
            buttons: _setting['buttons'],
            open: function () {
                $( selClass).append( _setting['data']);
                if( !empty( _setting['binder'].bind )){
                    _setting['binder'].bind();
                }
            }
        });
}

/**
 * Контейнер с анимацией
 * @returns {Function}
 */
function _getAnimation(){
    return new function(){
        this.dContent = function(){
            return $("<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" );
        }
    }
}
function arrayToAttribute( arr){
    var retStr = '';
    if( !empty(arr)){
        for(var key in arr){
            var elem = arr[ key];
            retStr += key+'="'+elem+'" ';
        }
    }
    return retStr;
}
function createDualHtmlElement( tagName, append ,attrArray){
    var attr = arrayToAttribute( attrArray);
    return '<'+tagName+' '+attr+' >'+append+'</'+tagName+'>';
}

/**
 * Функция для поиска скрывает не подхоящие под строку поиска строки таблицы
 * Строка поиска
 * @param {string} searchStr
 * Селектор для выборки строк
 * @param {string} trSelector
 * массив номеров столбцов для поиска
 * @param {array int} tdNumberToSearchArr
 * Каллбек
 * @param {function} callbck( row, isValidRowFromSearchCondition, rowCount)
 * @returns {undefined}
 */
function tableSearchFilter( searchStr, trSelector , tdNumberToSearchArr, callbck ){
    $( trSelector).each( function( rowCount , rowNode){
        var stringToProc = '';

        $( rowNode).find('td').each( function( tdCount , tdNode){
            if( tdNumberToSearchArr.indexOf( tdCount) !== -1){
                stringToProc += $( tdNode).html();
            }
        });

        var searchUpper = searchStr.toUpperCase(),
            procStrUpper = stringToProc.toUpperCase(),
            isValidTr = null;

        if( procStrUpper.indexOf( searchUpper) !== -1){
            $( rowNode).show();
            isValidTr = true;
        }else{
            $( rowNode).hide();
            isValidTr = false;
        }
        if( typeof(callbck)=='function'){
            callbck($( rowNode), isValidTr, rowCount);
        }
    });
}
function activateFileUpload(url_out ,class_elem ,show ,requestIsFromOrg){
    var requestIsFromOrg = requestIsFromOrg || false;
    if (show == undefined) {
        show = false;
    } else {
        $(".docType.active").click();
    }
    var ret;
    $(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = url_out;
    $(class_elem).fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            var elem = this;
            $.each(data.result.files, function (index, file) {
                if(file.errors.length<1){
                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div id="'+file.fileId+'"><span class="MultiFile-remove" onclick="window_delete_application_file(\''+file.fileId+'\', 1)">x</span><a target="_blank" href="'+file.filePath+'" download="'+file.fileName+'">'+file.fileName+'</a></div>')     ;
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').width("0%");
                    $('.FileListWrap.'+$(elem).attr("codeType")).parent(".containerWrapFile").css('border','none');
                    ret = 'ok';
                    useTopHoverTitle("Загрузка завершена", 2000);
                    if(show){
                        affairs.showDocumentInfo($("#currentIdDocument").val());
                    }else{
                        $(".docType.active").click();
                    }
                    if( requestIsFromOrg){
                        organization._getDocumentInfoFromOrg('.tab-notice');
                    }
                }else{
                    var error_string = data.result.files[0].errors.join(',');

                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div class="wrap_file_answer"><a class="MultiFile-remove remove_answer" >x</a> <span style="color:red;" class="red">'+data.originalFiles[0].name+'(Ошибки при загрузке файла: '+error_string+' )</span></div>');
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').css('width','0%');
                     useTopHoverTitle("Возникли ошибки при загрузке", 2000);
                }
                 $('.progress .progress-bar.'+$(elem).attr("codeType")).parent('.progress').hide();
                console.error( url );
                console.error( error_string );                        

                 ret = 'error';
            });


        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            useTopHoverTitle("Идет загрузка файла "+progress+"% ...", 2000);
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').parent('.progress').show();
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
return ret;
}
/**
 * проверяет пустоту полей и пустым полям добавляет класс empty_field возвращает кол-во пустых полей
 * @param {str} selector
 * @returns {Number}
 */
function checkInput(selector) {
    var count = 0;
    $(selector).each(function () {
        if ($(this).val() != '') {
            $(this).removeClass('empty_field');
        } else {
            $(this).addClass('empty_field');
            count++;
        }
    });
    return count;
}

(function( $ ) {
	/*
  навешивает саморасширяемую текст-зону вместо инпута
  записывает все данные из нее в инпут убирает при снятии фокуса
  created by Герасимов Руслан
  */
  $.fn.expandableInput = function() {
  	var tempTextarea = null,
    		getHeight = function( fontSize, inputWidth, strLen, lineHeight){
                console.log(fontSize, inputWidth, strLen, lineHeight);
        	var
          	fontSize_d = Math.ceil( fontSize/2),
            strSize = strLen*fontSize_d,
            lines = Math.ceil( strSize/inputWidth);

            if( lineHeight == 'normal'){
            	lineHeight = 2;
            }else{
                lineHeight = parseInt(lineHeight);
            }
            var heig = Math.ceil( lines*(fontSize+lineHeight));
            if( heig == 0){
             heig = fontSize+lineHeight;
            }
            console.log(heig);
            return heig;
        };

  	this.on(
    	'click focuson',
      function(){
        var
          widthInp = $(this).width(),
          outWidthInp = $(this).outerWidth(),
          fontSize = parseInt( $(this).css('font-size')),
          that = this;

      	$(this).hide();
        $(this).wrap('<span class="jquery-input-expandabler-ui" ></span>');
        $(this).parent().prepend('<textarea class="jquery-textarea-expandabler-ui" ></textarea>');
        tempTextarea = $(this).parent().find('textarea.jquery-textarea-expandabler-ui');
        $(tempTextarea).val( $(this).val());

        $(tempTextarea).width(widthInp);
        $(tempTextarea).outerWidth( outWidthInp);
        $(tempTextarea).css({
            'font-size' : fontSize,
            'height' : getHeight(
                          fontSize,
                          $(this).outerWidth(),
                          $(this).val().length,
                          $(tempTextarea).css('line-height')
                          )
        });
        $(tempTextarea).focus();
        $(tempTextarea).on(
        	'keyup keypress',
          function(){
          	$(that).val($(this).val());
            var h =  getHeight(
          								fontSize,
                          $(this).outerWidth(),
                          $(this).val().length,
                          $(this).css('line-height')
                          )
            $(this).css('height',h);
          }
        );
        $(tempTextarea).on(
        	'focusout onchange',
          function(){
          	$(this).remove();
            $(that).unwrap();
            $(that).show();
          }
        );
      }
    );
  	}
  })(jQuery);


                
