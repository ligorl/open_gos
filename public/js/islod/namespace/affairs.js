var affairs = {};

/**
 * окно редлактирование дела
 */
affairs.openEditAffair = function (idAffairs, title) {
   
    var dialog = {
        width: Math.round(screen.width * 0.83),
        height: Math.round(screen.height * 0.85)
    };

    var title = title || $(".active_deal").attr("dialog-title") || $(".intable.deals-from-org tr.active").attr("title");

    var dContent = $(
            '<div class="dialog-deal-edit">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

    var affairId = idAffairs  || $(".intable.deals-from-org tr.active").attr("id")|| $(".active_deal").attr("id").split("_")[1];
    var expertId = $("#selectedExpert").val();
    var expertStr = '';
    if (expertId !== undefined && expertId != '') {
        expertStr = '/' + expertId;
    }
    $(dContent).dialog({
        title: title,
        width: dialog.width + 150,
        height: dialog.height - 50,
        modal: true,
        autoOpen: false,
        close: function () {
            //$(".btn-warning").click(); Не нужно закрывать
            
            $(".dialog-deal-edit").remove();
        },
        open: function () {
          
        }
    });
     $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/eo/affairs/edit/" + affairId,
        success: function (answer) {
            if (answer == "error") {
                useTopHoverTitle("Вы не являетесь исполнителем по этому делу");
                $(dContent).dialog("close");
                return false;
            } else {
                $(dContent).dialog("open");
            }
            $(".loading-img", dContent).fadeOut(500, function () {
                dContent.html("").append($(answer));
                $(".content-block.deal").height($(".dialog-deal-edit.ui-dialog-content").height() - 105);
                $(document).ready(function () {
                    $(".dialog-deal-edit .btAdd").click(function () {
                        var expertId = $("#selectedExpert").val();
                        var expertStr = '';
                        if (expertId !== undefined && expertId != '') {
                            expertStr = '/' + expertId;
                        }
                        $.ajax({
                            type: 'POST',
                            dataType: "html",
                            url: "/islod/affairs/save/" + affairId + expertStr,
                            success: function (answer) {
                                if (answer == "ok") {
                                    searchApplication("#pagination .current-page");
                                    useTopHoverTitle("Данные успешно сохранены");
                                } else {
                                    useTopHoverTitle("Произошла ошибка во время сохранения");
                                }
                            }
                        });
                    });
                    //Обработчик кнопки создания заявления

                    //Обработка удаления оплаты
                    $("body").undelegate(".btRemove", "click");
                    $("body").delegate(".btRemove", "click", function () {
                        if (confirm("Вы действительно хотите удалить этот платеж?")) {
                            var id = $(this).attr("id").split("_")[1];
                            var that = this;

                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                data: {
                                    paymentId: id
                                },
                                url: "/eo/requests/remove-payment",
                                success: function (answer) {
                                    //Проверяем если ошибка
                                    if (answer.result && answer.result == "error") {
                                        useTopHoverTitleError(answer.error);
                                        return;
                                    }
                                    //Если все хорошо
                                    if (answer.result && answer.result == "success") {
                                        //Удаляем из списка
                                        $(that).parent().parent().remove();
                                    }
                                }
                            });
                        }
                    });

                    //Обработка Редактирование оплаты
                    $("body").off("click", ".btEdit");
                    $("body").on("click", ".btEdit", function () {

                        if ($(".dialog-add-payment").length > 0) {
                            $(".dialog-add-payment").remove();
                        }

                        console.log('Отображаем диалог добавления 3');
                        var dContent = $(
                                '<div class="dialog-add-payment">' +
                                "<div><div class='loading-img' style='margin-top:40px'><img src='/img/loading.gif' /></div></div>" +
                                '</div>'),
                                currentPayment = $(this),
                                idPaymentSelector = $(this).attr('id'),
                                idPayment = idPaymentSelector.split('_')[1];

                        $(dContent).dialog({
                            title: "Платежное поручение",
                            //width: 600,
                            //height: 380,
                            modal: true,
                            close: function () {
                                $(".dialog-add-payment").remove();
                            },
                            open: function () {
                                //Отсылаем запрос на получение диалога
                                $.ajax({
                                    type: 'POST',
                                    dataType: "html",
                                    data: {
                                        LicenseRequestPaymentId: idPayment,
                                    },
                                    url: "/eo/requests/get-payment-dialog",
                                    success: function (answer) {
                                        $(".loading-img", dContent).fadeOut(500, function () {
                                            dContent.html("").append($(answer));

                                            //вешаем дейтпикер
                                            $(".date-field", dContent).datepicker({
                                                changeMonth: true,
                                                changeYear: true,
                                                dateFormat: "dd.mm.yy"
                                            });

                                            //Обработчики

                                            $(".btCancel", dContent).click(function () {
                                                $(".dialog-add-payment").remove();
                                            });

                                            $(".btSave", dContent).click(function () {
                                                console.log('добавим платежку 1');

//                                                        inputCurrencyValidator($('.dialog-add-payment input[name="SumNeeded"]'));
//                                                        inputCurrencyValidator($('.dialog-add-payment input[name="Sum"]'));

                                                //Проходим по всем полям
                                                var formContainer = dContent;

                                                //Проходим по полям
                                                //Собираем объект с данными для добавления
                                                var fd = {};
                                                var formData = new FormData();
                                                $(".dataField", formContainer).each(function(i, item){
                                                    var iName = $(item).attr("name");
                                                    var fType = $(item).attr("fType");
                                                    switch(fType){
                                                        case "val":
                                                            var iValue = $(item).val();
                                                            fd[iName] = iValue;
                                                            formData.append(iName, iValue);
                                                            break;
                                                        case "date":
                                                            var iValue = $(item).val();
                                                            var date = moment(iValue,"DD.MM.YYYY").format("YYYY-MM-DD");
                                                            fd[iName] = date;
                                                            formData.append(iName, date);
                                                            break;
                                                        case "file":
                                                            var iValue = $(item).val();
                                                            if( 'undefined' == typeof(iValue) || '' == iValue ){
                                                                break;
                                                            }
                                                            var fileData =  $(item)[0].files[0];                                                
                                                            formData.append(iName, fileData);
                                                            break;
                                                    }
                                                });

                                                $.ajax({
                                                    type: 'POST',
                                                    dataType: "json",
                                                    data: formData,
                                                    processData: false,
                                                    contentType: false,
                                                    url: "/eo/requests/add-payment",
                                                    success: function (answer) {
                                                        //Проверяем если ошибка
                                                        if (answer.result && answer.result == "error") {
                                                            useTopHoverTitleError("Произошла ошибка при сохранении фрейма 'Изменение наименования лицензиата'");
                                                            return;
                                                        }
                                                        //Если все хорошо
                                                        if (answer.result && answer.result == "success") {
                                                            $(currentPayment).parent().parent().html(
                                                                    $(answer.row).html()
                                                                    );
                                                            $(".dialog-add-payment").remove();
                                                        }
                                                    }
                                                });
                                            });
                                        });
                                    }
                                });
                            }
                        });
                    });


                    $(".dialog-deal-edit .btCreateRequest").click(function () {
                        var orgId = null;
                        if ($("#DealOrganizationId", dContent).length > 0)
                            orgId = $("#DealOrganizationId", dContent).val();

                        request.openAdd(null, orgId, affairId);
                        $(".dialog-deal-edit").remove();
                    });
                    //--
                    $('body').undelegate('.supplLiElem', 'click');
                    $('body').delegate('.supplLiElem', 'click', function () {
                        licenseSupplement.createSupplementInfoDialog($(this).attr("id_suppl"));
                    });
                    $('body').undelegate('.licenseOpenBut', 'click');
                    $('body').delegate('.licenseOpenBut', 'click', function () {
                        if ($("#licNumb_" + $(this).attr("license-id")).val() == "") {
                            license.licenseSaveLicNumber($(this).attr("license-id"));
                        }
                        license._get_license_info($(this).attr("license-id"), $(this).attr("title"));
                    });

                    $('body').undelegate('.openRequestButt', 'click');
                    $('body').delegate('.openRequestButt', 'click', function () {
                        request.openEdit($(this).attr("data-id-request"));
                    });
                    $('body').undelegate('.moveRequestButt', 'click');
                    $('body').delegate('.moveRequestButt', 'click', function () {
                        affairs.getDialogMove($(this).attr("data-id-request"), $(this).attr("data-title"));
                    });
                    $('body').undelegate('.outRequestButt', 'click');
                    $('body').delegate('.outRequestButt', 'click', function () {
                        affairs.getDialogOut($(this).attr("data-id-request"), $(this).attr("data-title"), $(this).attr("data-new-numb"));
                    });
                    $('body').undelegate('.buttonAffais ', 'click');
                    $('body').delegate('.buttonAffais ', 'click', function () {
                        affairs.changeStatusDealDialog($(this).attr("data-next-status"), $(this).attr("data-message"), $(this).attr("data-comm"))
                    });
                    $('body').undelegate('.dialog-deal-edit .tabs-container .tab', 'click');
                    $('body').delegate('.dialog-deal-edit .tabs-container .tab', 'click', function () {
                        affairs.changeAffairsTab(this);
                    });
                    $('body').undelegate('#listOfDocumentTypeWrapper .docType', 'click');
                    $('body').delegate('#listOfDocumentTypeWrapper .docType', 'click', function () {
                        $("#listOfDocumentTypeWrapper .docType").removeClass("active");
                        $(this).addClass("active");
                        affairs.getDealDocumentByParrentType($("#DealId").val(), $(this).attr("parent-type-id"));
                    });
                    $('body').undelegate('#listOfDocumentWrapper .document-row', 'click');
                    $('body').delegate('#listOfDocumentWrapper .document-row', 'click', function (event) {
                        if (event.target.nodeName != 'SPAN') {
                            affairs.showDocumentInfo($(this).attr("document-id"), $(this).attr("title"));
                        }
                    });

                    $('body').undelegate('#listOfDocumentTypeWrapper .btAddDocument', 'click');
                    $('body').delegate('#listOfDocumentTypeWrapper .btAddDocument', 'click', function () {
                        affairs.showDocumentInfo($(this).attr("document-id"), $(this).attr("title"));
                    });

                    $('body').undelegate('.deal-document-block .document-unselected-row .removeFileType', 'click');
                    $('body').delegate('.deal-document-block .document-unselected-row .removeFileType', 'click', function () {
                        affairs.removeDocumentFile($(this).attr("type-file"), $(this).attr("document-id"));
                    });
                    $('body').undelegate('.deal-document-block .document-unselected-row .reMakeFileType', 'click');
                    $('body').delegate('.deal-document-block .document-unselected-row .reMakeFileType', 'click', function () {
                        affairs.reMakeDocumentFile($(this).attr("type-file"), $(this).attr("document-id"));
                    });
                    $('body').undelegate('.deal-document-block .document-unselected-row .newFileType', 'click');
                    $('body').delegate('.deal-document-block .document-unselected-row .newFileType', 'click', function () {
                        activateFileUpload('/islod/affairs/save-files/' + $(this).attr("document-id"), '.multi');
                        $("input[name=" + $(this).attr("real-type-name") + "]").first().click();

                    });

                    $('body').undelegate('#current-notification-info-form .removeFileType', 'click');
                    $('body').delegate('#current-notification-info-form .removeFileType', 'click', function () {
                        affairs.removeDocumentFile($(this).attr("type-file"), $(this).attr("document-id"), true);
                    });
                    $('body').undelegate('#current-notification-info-form .reMakeFileType', 'click');
                    $('body').delegate('#current-notification-info-form .reMakeFileType', 'click', function () {
                        affairs.reMakeDocumentFile($(this).attr("type-file"), $(this).attr("document-id"), true);
                    });
                    $('body').undelegate('#current-notification-info-form .newFileType', 'click');
                    $('body').delegate('#current-notification-info-form .newFileType', 'click', function () {
                        activateFileUpload('/islod/affairs/save-files/' + $(this).attr("document-id"), '.multi', true);
                        $("input[name=" + $(this).attr("real-type-name") + "]").first().click();
                    });
                    /* $('body').undelegate('.btOpenEO', 'click');
                     $('body').delegate('.btOpenEO', 'click', function () {
                     organization.createOrganizationInfoDialog($(this).attr(""))
                     
                     });*/
                    $('body').undelegate('.requestrow', 'click');
                    $('body').delegate('.requestrow', 'click', function () {
                        $('.hideTbody').hide();
                        $('.requestrow').removeClass('active_req');
                        $(this).addClass('active_req');
                        $('.hideTbody[id_body=' + $(this).attr("id_row") + ']').show();

                    });
                    $('body').undelegate('.active_req', 'click');
                    $('body').delegate('.active_req', 'click', function () {
                        $('.hideTbody').hide();
                        $('.requestrow').removeClass('active_req');
                        $('.hideTbody[id_body=' + $(this).attr("id_row") + ']').hide();
                    });
                    $('body').undelegate('.active_req', 'click');
                    $('body').delegate('.active_req', 'click', function () {
                        $('.hideTbody').hide();
                        $('.requestrow').removeClass('active_req');
                        $('.hideTbody[id_body=' + $(this).attr("id_row") + ']').hide();
                    });
                    $('body').undelegate('.buttonDocument ', 'click');
                    $('body').delegate('.buttonDocument ', 'click', function () {
                        affairs.changeStatusDocumentDialog(
                                $(this).attr("data-next-status"),
                                $(this).attr("data-message"),
                                $(this).attr("data-document-id"),
                                $(this).attr("data-require")
                                );
                    });
                    $('body').undelegate('.deal-history-reestr-block .buttonReestr ', 'click');
                    $('body').delegate('.deal-history-reestr-block .buttonReestr ', 'click', function () {
                        var defMessage = $(this).attr("data-message"),
                                that = $(this);

                        switch ($(this).attr("data-next-status")) {
                            case 'SEND_BY_MAIL':
                                defMessage = {
                                    title: $(this).attr("data-message"),
                                    message: $(this).attr("data-message") + '<table class="fields-table"><tr><td>Дата отправки по почте</td><td><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                                    handler: function (dialog, function_data) {
                                        $('input[name="date-attached-to-action"]').datepicker({
                                            changeMonth: true,
                                            changeYear: true,
                                            dateFormat: "dd.mm.yy"
                                        });
                                        function_data['beforeSaveCallBck'] = function () {
                                            _getData().changeLicAndSuppDate({
                                                id: $(that).attr("data-id"),
                                                type: $(that).attr("data-element-type"),
                                                input: 'DateSending',
                                                date: $('input[name="date-attached-to-action"]').val()
                                            });
                                        };
                                    }
                                };
                                break;
                            case 'RECEIVED_BY_APPLICANT':
                                defMessage = {
                                    title: $(this).attr("data-message"),
                                    message: $(this).attr("data-message") + '<table class="fields-table"><tr><td>Дата вручения</td><td><div class="relative_block"><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                                    handler: function (dialog, function_data) {
                                        $('input[name="date-attached-to-action"]').datepicker({
                                            changeMonth: true,
                                            changeYear: true,
                                            dateFormat: "dd.mm.yy"
                                        });
                                        function_data['beforeSaveCallBck'] = function () {
                                            _getData().changeLicAndSuppDate({
                                                id: $(that).attr("data-id"),
                                                type: $(that).attr("data-element-type"),
                                                input: 'DateOnHands',
                                                date: $('input[name="date-attached-to-action"]').val()
                                            });
                                        };
                                    }
                                };
                                break;
                        }

                        affairs.changeStatusReestrDialog(
                                $(this).attr("data-id"),
                                $(this).attr("data-element-type"),
                                $(this).attr("data-type-code"),
                                $(this).attr("data-next-status"),
                                defMessage,
                                $(this).attr("data-comment")
                                );
                    });
                    $('body').undelegate('#listOfHistoryReestrTypeWrapper .reestrType', 'click');
                    $('body').delegate('#listOfHistoryReestrTypeWrapper .reestrType', 'click', function () {
                        $("#listOfHistoryReestrTypeWrapper .reestrType").removeClass("active");
                        $(this).addClass("active");
                        affairs.ReestrChangeType($(this).attr("data-deal-id"), $(this).attr("data-type"));
                    });
                    $('#listOfHistoryReestrTypeWrapper .reestrType').first().click();
                    $('#listOfDocumentTypeWrapper .docType').first().click();

                    $('body').undelegate('.btRechangeReestr', 'click');
                    $('body').delegate('.btRechangeReestr', 'click', function () {
                        affairs.rechangeReestrDialog($("#DealId").val());
                    });
                    $('body').undelegate('.deleteSupplement', 'click');
                    $('body').delegate('.deleteSupplement', 'click', function () {
                        affairs.deallocationSupplement($(this).attr("id_suppl"),$("#DealId").val());
                    });

                });
            });
        }
    });
   
}

affairs.getDialogMove = function (requestId, title) {
    var dialog = {
        width: Math.round((screen.width / 100) * 28),
        height: Math.round((screen.height / 100) * 20)
    };
    var dContent = $(
            '<div class="dialog-request-move">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: "Перемещение заявления №" + title,
        width: dialog.width,
        height: dialog.height,
        modal: true,
        buttons: [
            {
                text: "Сохранить",
                "class": 'left saveButLicense',
                click: function () {
                    affairs.changeDealForRequest();
                }
            },
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $(".dialog-request-move").remove();
        },
        open: function () {
            //Отсылаем запрос на получение диалога
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/islod/affairs/move-to-affairs-dialog/" + requestId,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));
                        $(".content-block.deal").height($(".dialog-deal-edit.ui-dialog-content").height() - 105);
                    });
                }
            });
        }
    });
}
affairs.changeDealForRequest = function () {
    var requestId = $("#requestId").val();
    var dealId = $("#dealNumber").val();
    if (dealId === undefined || requestId === undefined) {
        useTopHoverTitle("Не выбрано дело", 3000);
        $(".dialog-request-move").remove();
    } else {
        $.ajax({
            type: 'POST',
            dataType: "html",
            url: "/islod/affairs/move-to-affairs-save/" + requestId + "/" + dealId,
            success: function (answer) {
                if (answer == 1) {
                    $(".dialog-request-move").remove();
                    $(".row_deal.active_deal").click();
                }
            }
        });
    }
}
affairs.getDialogOut = function (requestId, title, newNumb) {
    var dialog = {
        width: Math.round((screen.width / 100) * 40),
        height: Math.round((screen.height / 100) * 26)
    };
    var dContent = $(
            '<div class="dialog-request-out">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: "Выделение заявления №" + title,
        width: dialog.width,
        height: dialog.height,
        modal: true,
        buttons: [
            {
                text: "Сохранить",
                "class": 'left saveButLicense',
                click: function () {
                    affairs.outRequestOfDeal(requestId);
                }
            },
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $(".dialog-request-move").remove();
        },
        open: function () {
            $(".loading-img", dContent).fadeOut(200, function () {
                var str = "<div><p>Вы действительно хотите выделить в новое дело заявление №" + title + " ?</p> <p>Будет создано новое дело " + newNumb + ". </p> </div>";
                dContent.html("").append(str);
            });
        }
    });
}
affairs.outRequestOfDeal = function (requestId) {

    if (requestId === undefined) {
        useTopHoverTitle("Не выбрано заявление", 3000);
        $(".dialog-request-out").remove();
    } else {
        $.ajax({
            type: 'POST',
            dataType: "html",
            url: "/islod/affairs/out-request-of-affair/" + requestId,
            success: function (answer) {
                if (answer == 1) {
                    $(".dialog-request-out").remove();
                    $(".row_deal.active_deal").click();
                }
            }
        });
    }
}

affairs.changeStatusDealDialog = function (nextStat, message, comm) {
    if (comm == 1) {
        var height = 24;
    } else {
        var height = 17;
    }
    var dialog = {
        width: Math.round((screen.width / 100) * 35),
        height: Math.round((screen.height / 100) * height)
    };
    var dContent = $(
            '<div class="dialog-request-change">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: message,
        width: dialog.width,
        height: dialog.height,
        modal: true,
        buttons: [
            {
                text: "Да",
                "class": 'left saveButLicense',
                click: function () {
                    affairs.changeStatusDeal(nextStat);
                }
            },
            {
                text: "Нет",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $(".dialog-request-change").remove();
        },
        open: function () {
            $(".loading-img", dContent).fadeOut(200, function () {
                var str = "<div><p>" + message + "</p>";
                if (comm == 1) {
                    str += "<p>Комментарий: </p><input type='text' id='commentInput'></div>";
                } else {
                    str += "</div>";
                }

                dContent.html("").append(str);
            });
        }
    });
};

affairs.changeStatusDeal = function (nextStatus,affairId,reopen) {
    var affairId = $("#DealId").val() || affairId;
    if (affairId === undefined) {
        useTopHoverTitle("Не выбрано дело", 3000);
        $(".dialog-request-out").remove();
    } else {
        $.ajax({
            type: 'POST',
            dataType: "html",
            data: {"comment": $("#commentInput").val()},
            url: "/islod/affairs/change-status-affair/" + affairId + "/" + nextStatus,
            beforeSend: function (xhr) {
                            $(".loading-overlay").show();
                        },
            success: function (answer) {
                //Все нормально
                if (answer == 1) {
                    useTopHoverTitle("Статус дела изменен", 3000);
                    if(reopen!==0){
                        $(".dialog-deal-edit").remove();
                        $(".dialog-request-change").remove();
                        affairs.openEditAffair(affairId, $(".row_deal.active_deal").attr("dialog-title"));
                    }
                    $(".loading-overlay").hide();
                } else {
                    useTopHoverTitle(answer, 3000);
                    $(".loading-overlay").hide();
                }
            },
            error: function () {
                useTopHoverTitle("Возникла ошибка при сохранении дела", 3000);
            }
        });
    }
}
affairs.changeAffairsTab = function (element) {
    $(".tab-block").removeClass("active-tab");
    $("." + $(element).attr("tab-container")).addClass("active-tab");
    $(".dialog-deal-edit  .tab").removeClass("active");
    $(element).addClass("active");
    resizeContent($(".deal-document-block .viewzone"));
    specialTableResize(".deal-document-block");
	resizeContent($(".deal-history-reestr-block .viewzone"));
    specialTableResize(".deal-history-reestr-block");
}

affairs.getDealDocumentByParrentType = function (dealId, typeId) {
    if (dealId === undefined || typeId === undefined) {
        useTopHoverTitle("Не выбрано дело", 3000);
    } else {
        $.ajax({
            type: 'POST',
            dataType: "html",
            data: {"comment": $("#commentInput").val()},
            url: "/islod/affairs/get-document-by-deal-and-type/" + dealId + "/" + typeId,
            beforeSend: function (xhr) {
                $(".deal-document-block #listOfDocumentWrapper tbody").html("<tr class='loading-img'><td><img src='/img/loading.gif' /></td></tr>");
            },
            success: function (answer) {
                $(".deal-document-block #listOfDocumentWrapper").html(answer);
                resizeContent($(".deal-document-block .viewzone"));
                specialTableResize(".deal-document-block");
                $('body').undelegate('#listOfDocumentWrapper .document-row .btRemoveDoc', 'click');
                $('body').delegate('#listOfDocumentWrapper .document-row .btRemoveDoc', 'click', function () {
                    affairs.deleteDocumentDialog($(this).attr("document-id"));
                });
            },
            error: function () {
                useTopHoverTitle("Возникла ошибка при получении данных", 3000);
            }
        })
    }
}

affairs.showDocumentInfo = function (idDocument, message) {
    if (message == undefined) {
        message = "Добавление документа";
    }
    var dialog = {
        width: Math.round((screen.width / 100) * 75),
        height: Math.round((screen.height / 100) * 58),
    };
    var dContent = $(
            '<div class="dialog-document-show">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: message,
        width : dialog.width,
        height : dialog.height,
        position: {my: "center", at: "top", of: window},
        modal: true,
        buttons: [
            {
                text: "Сохранить",
                "class": 'right',
                click: function () {
                    affairs.saveDocumentInfo();
                }
            },
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                    $(".docType.active").click();
                }
            }
        ],
        close: function () {
            $(".dialog-document-show").remove();
            $(".docType.active").click();
        },
        open: function () {
            $(".loading-img", dContent).fadeOut(200, function () {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    data: {"id": idDocument, "parentType": $(".docType.active").attr("parent-type-id") || ''},
                    url: "/islod/affairs/get-document-dialog/" + idDocument,
                    success: function (answer) {
                        dContent.html("").append(answer);
                        $('.dateInput').datepicker();
                        $('body').undelegate('#documentTypeId', 'change');
                        $('body').delegate('#documentTypeId', 'change', function () {
                            var curr_type = $(this).val();
                            $("#documentReasonId").val("");
                            $("#documentReasonId option").removeClass("show");
                            $("#documentReasonId option").removeClass("hide");
                            $("#documentReasonId option.first_option").addClass("show");
                            $("#documentReasonId option." + curr_type).addClass("show");
                            if ($("#documentReasonId option.show").length > 1) {
                                $("#rowReasonDocument").show();
                            } else {
                                $("#rowReasonDocument").hide();
                            }
                            affairs.getSignatoriesForDoc(curr_type);
                        });
                    },
                    error: function () {
                        useTopHoverTitle("Возникла ошибка при получении данных", 3000);
                    }
                });
            });
        }
    });
};
affairs.saveDocumentInfo = function () {
    $.ajax({
        type: 'POST',
        data: $("#current-notification-info-form").serialize(),
        url: "/islod/affairs/save-document-info/" + $("#DealId").val(),
        beforeSend:function(){
            $(".loading-overlay").show();
        },
        success: function (answer) {
            $(".docType.active").click();
            $(".dialog-document-show").remove();
            $(".loading-overlay").hide();
        },
        error: function () {
            useTopHoverTitle("Возникла ошибка при сохранении данных", 3000);
        }
    });
}
affairs.removeDocumentFile = function (type, idDoc, show, requestIsFromOrg) {
    var requestIsFromOrg = requestIsFromOrg || false;
    if (!confirm("Вы действительно хотите удалить этот файл?")) {
        return 0;
    }
    if (show == undefined) {
        show = false;
    }
    $.ajax({
        type: 'POST',
        data: $("#current-notification-info-form").serialize(),
        url: "/islod/affairs/remove-document-file/" + idDoc + "/" + type,
        success: function (answer) {
            if (answer == "ok") {
                useTopHoverTitle("Файл успешно удален", 2000);
                var id_doc = $("#currentIdDocument").val();
                $(".dialog-document-show").remove();
                if (show) {
                    affairs.showDocumentInfo(id_doc, 'Редактирование документа');
                } else {
                    $(".docType.active").click();
                }
                if (requestIsFromOrg) {
                    organization._getDocumentInfoFromOrg('.tab-notice');
                }
            }
        },
        error: function () {
            useTopHoverTitle("Возникла ошибка при удалении данных", 3000);
        }
    });
}

/**
 * тык по иконке хочу переформатировать документ
 *
 * @param {type} type
 * @param {type} idDoc
 * @param {type} show
 * @returns {Number|Boolean}
 */
affairs.reMakeDocumentFile = function (type, idDoc, show) {
    if (!confirm("Вы уверены, что хотите переформировать документ")) {
        return 0;
    }
    var licenseId = idDoc
    if (typeof (licenseId) == 'undefined' && licenseId == '') {
        useTopHoverTitleError('не задана лицензия');
        return 0;
    }
    var prefix = 'ЛОД - формирование документа: ';
    console.info(prefix + 'начинаем');
    $.ajax({
        //type: 'POST',
        //timeout: 5000,
        dataType: "json",
        //data: data,
        url: "/islod/index/remake-document/document/" + licenseId,
        beforeSend: function () {
            //колесико вперед
            jQuery('#listOfDocumentWrapper .fields-table.intable').css('visibility', 'hidden');
            jQuery('#listOfDocumentWrapper ').prepend(
                    '<div class="krutilka-lod"></div>'
                    );
        },
        success: function (answer) {
            var message = '';
            if (typeof (answer.message) != 'undefined' && answer.message != '') {
                message = ' ' + answer.message;
            }
            var consoleMsg = '';
            if (typeof (answer.console) != 'undefined' && answer.console != '') {
                consoleMsg = ' ' + answer.console;
            }
            if (answer.status == 'Error') {
                useTopHoverTitleError("Ошибка формирования\n" + message, 2001);
                console.error(prefix + 'Ошибка формирования');
                console.log(prefix + consoleMsg);
                return;
            }
            if (answer.status == 'Ok') {
                if ('' == message) {
                    message = "Переформировано";
                }
                useTopHoverTitle(message, 2001);
                //обновим ссылку
                var link = '#';
                if (typeof (answer.link) != 'undefined' && answer.link != '') {
                    link = answer.link;
                }
                var fileName = 'Документ.docx';
                if (typeof (answer.fileName) != 'undefined' && answer.fileName != '') {
                    fileName = answer.fileName;
                }
                //jQuery('a.license-print-link').removeClass('hiden');
                // в перечне документов
                jQuery('a.document-link[document-id="' + idDoc + '"]').attr('href', '/uploads/' + link);
                jQuery('a.document-link[document-id="' + idDoc + '"]').attr('download', '' + fileName);
                //форма редлактирования
                //jQuery('#current-notification-info-form a[document-id="' + idDoc + '"]').attr('href', '/uploads/' + link);
                //jQuery('#current-notification-info-form a[document-id="' + idDoc + '"]').attr('download', '' + fileName);
                jQuery('#current-notification-info-form a.document-link[document-id="' + idDoc + '"]').text(fileName);
                //jQuery('a.document-link').text(fileName);
                console.info(prefix + 'макет  переформирован');
                return;
            }
            useTopHoverTitleError("Ошибка при формирвоании\n", 2001);
            console.info(prefix + 'Непонятно чегото');
            console.log(answer);
        },
        error: function (e) {
            useTopHoverTitleError("Ошибка связи");
            console.error(prefix + 'ошибка связи при формированиии');
            console.log(e);
            console.log(e.responseText);
        },
        complete: function () {
            console.info(prefix + 'формирование окончено');
            //крутилку убрать
            jQuery('#listOfDocumentWrapper .krutilka-lod').remove();
            jQuery('#listOfDocumentWrapper .fields-table.intable').css('visibility', 'visible');
        }
    });
    return false;
}


affairs.changeStatusDocumentDialog = function (nextStat, message, documentId, require, dealNumber, dealId) {

    var dialog = {
        width: Math.round((screen.width / 100) * 35),
        height: Math.round((screen.height / 100) * 22)
    },
    dealNumber = dealNumber || $("#DealNumber").val(),
    dealId = dealId || $("#DealId").val();
            dContent = $(
                    '<div class="dialog-document-change-status">' +
                    "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
                    '</div>'
                    );

    $(dContent).dialog({
        title: "Документ для дела № " + dealNumber,
        width: dialog.width,
        modal: true,
        buttons: [
            {
                text: "Да",
                "class": 'left saveButLicense',
                click: function () {
                    affairs.changeStatusDocument(nextStat, documentId, require,dealId);
                }
            },
            {
                text: "Нет",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $(".dialog-document-change-status").remove();
        },
        open: function () {
            $.ajax({
                type: 'POST',
                dataType: "html",
                data: {"requireField": require},
                url: "/islod/affairs/change-status-document-dialog/" + documentId,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(200, function () {
                        dContent.html("<div>" + message + "</div>").append(answer);
                        $('input.date').datepicker();
                    });

                }
            });
        }
    });
};
affairs.changeStatusDocument = function (nextStatus, documentid, require,dealId) {
    if (checkInput("#FormChangeStatusDocument .listOfField input.require") > 0) {
        useTopHoverTitle("Заполните все обязательные поля !", 3000);
        return false;
    }
    var activeItem = $(".active_deal").attr("id");
    var affairId = null;
    if(!activeItem){
        affairId = $('.active-deal').attr('data-deal-id');
    }else{
        affairId = $(".active_deal").attr("id").split("_")[1];
    }
    affairId = affairId||dealId;
    if (affairId === undefined) {
        useTopHoverTitle("Не выбрано дело", 3000);
        $(".dialog-request-out").remove();
    } else {
        $.ajax({
            type: 'POST',
            dataType: "html",
            data: $("#FormChangeStatusDocument").serialize(),
            url: "/islod/affairs/change-status-document/" + documentid + "/" + nextStatus,
            success: function (answer) {
                if(nextStatus=="RECEIVED_ANSWER"){
                    affairs.changeStatusDeal("RECEIVE_ANSWER",affairId,0);
                }
                useTopHoverTitle(answer, 5000);
                $(".dialog-document-change-status").remove();
                $("#listOfDocumentTypeWrapper .docType.active").click();

                if( $('.organization-info-dialog').length>0 ){
                    var orgId = $('.current-organization-dialog-id').val(),
                        tabPage = _getPage().documentFromOrg(orgId);

                    if (tabPage) {
                        $('.tab-notice').html(tabPage);
                    }
                }
            },
            error: function () {
                useTopHoverTitle("Возникла ошибка при смене статуса документа", 3000);
            }
        });
    }
}
affairs.deleteDocumentDialog = function (documentId) {
    var dialog = {
        width: Math.round((screen.width / 100) * 35)
    };
    var dContent = $(
            '<div class="dialog-document-delete">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: "Документ для дела № " + $("#DealNumber").val(),
        width: dialog.width,
        modal: true,
        buttons: [
            {
                text: "Да",
                "class": 'left saveButLicense',
                click: function () {
                    affairs.deleteDocument(documentId);
                }
            },
            {
                text: "Нет",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $(".dialog-document-delete").remove();
        },
        open: function () {
            $(".loading-img", dContent).fadeOut(200, function () {
                var str = "<div><p>Вы уверены что хотите удалить документ ?</p>";
                str += "</div>";
                dContent.html("").append(str);
            });
        }
    });
}
affairs.deleteDocument = function (documentId) {
    var activeItem = $(".active_deal").attr("id");
    var affairId = null;
    if(!activeItem){
        affairId = $('.active-deal').attr('data-deal-id');
    }else{
        affairId = $(".active_deal").attr("id").split("_")[1];
    }
    affairId = affairId||dealId;
    if (affairId === undefined) {
        useTopHoverTitle("Не выбрано дело", 3000);
        $(".dialog-request-out").remove();
    } else {
        $.ajax({
            type: 'POST',
            dataType: "html",
            url: "/islod/affairs/delete-document/" + documentId,
            success: function (answer) {
                useTopHoverTitle(answer, 5000);
                $(".dialog-document-delete").remove();
                $("#listOfDocumentTypeWrapper .docType.active").click();
            },
            error: function () {
                useTopHoverTitle("Возникла ошибка при удалении документа", 3000);
            }
        });
    }
}

affairs.ReestrChangeType = function (dealId, typeId) {
    if (dealId === undefined || typeId === undefined) {
        useTopHoverTitle("Не выбрано дело", 3000);
    } else {
        $.ajax({
            type: 'POST',
            dataType: "html",
            url: "/islod/affairs/get-history-reestr/" + dealId + "/" + typeId,
            beforeSend: function (xhr) {
                $(".deal-history-reestr-block #listOfHistoryReestrWrapper tbody").html("<tr class='loading-img'><td><img src='/img/loading.gif' /></td></tr>");
            },
            success: function (answer) {
                $(".deal-history-reestr-block #listOfHistoryReestrWrapper").html(answer);
                resizeContent($(".deal-history-reestr-block .viewzone"));
                specialTableResize(".deal-history-reestr-block");
            },
            error: function () {
                useTopHoverTitle("Возникла ошибка при получении данных", 3000);
            }
        })
    }
};
/**
 * Загружаем дела из базы
 * @param int id
 * @param function beforeHandler
 * @param function successHandler
 * @param function alwaysHandler
 * @returns undefined
 */
affairs.loadDealsForExecutor = function (id, beforeHandler, successHandler, alwaysHandler) {
    var url = '/islod/affairs/loadDealsForExecutor';
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
            id: id
        },
        success: function (response) {
            successHandler(response);
        },
        beforeSend: function () {
            beforeHandler();
        },
        complete: function () {
            alwaysHandler();
        }
    });
};

affairs.openDealAtWork = function () {
    var url = '/islod/affairs/dealAtWork';
    var container = $('.content .container');
    var buttonBlock = $('#btActions-block');
    var paginator = $('#pagination');

    var dealControl = {};
    var filterControl = {};

    var dealControlHandler = function () {

        dealControl = {
            openDeal: $('[data-action="openDeal"]'),
            openOrganization: $('[data-action="openOrganization"]')
        };

        dealControl.openDeal.on('click', function () {
            var dialogTitle = $(this).attr('data-dialog-title');
            var dealId = $(this).attr('data-deal-id');
            $('.active-deal').removeClass('active-deal');
            $(this).addClass('active-deal');
            affairs.openEditAffair(dealId, dialogTitle);
        });
        dealControl.openOrganization.on('click', function () {
            var organizationId = $(this).attr('data-organization-id');
            organization.createOrganizationInfoDialog(organizationId);
        });

    };

    var filterControlHandler = function () {
        filterControl = {
            executorList: $('[data-filter="executor"]')
        };

        if (filterControl.executorList !== undefined) {
            filterControl.executorList.on('change', function () {

                $('[data-content="deal"]').hide();
                $('.affair-loading').show();

                var id = $('[data-filter="executor"] :selected').val();
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'html',
                    data: {
                        id: id
                    },
                    success: function (response) {
                        var content = $('[data-content="deal"]', response);
                        $('[data-content="deal"]').replaceWith(content);
                        dealControlHandler();
                    }
                }).always(function () {
                    $('.affair-loading').hide();
                    $('[data-content="deal"]').show();
                    $(window).trigger('resize');
                });
            });
        }
    };


    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        success: function (response) {
            container.empty().append(response);
            buttonBlock.empty();
            paginator.empty();
            $(window).trigger('resize');

            dealControlHandler();
            filterControlHandler();

            $('.affair-block').on('click', function () {
                $('.affair-active').removeClass('affair-active');
                $(this).addClass('affair-active');
            });
        }
    });
};








affairs.changeStatusReestrDialog = function (id, elemType, stateType, nextStat, message, comm, afterSaveCallbck) {
    if (comm == 1) {
        var height = 24;
    } else {
        var height = 17;
    }
    var dialog = {
        width: Math.round((screen.width / 100) * 35),
        height: Math.round((screen.height / 100) * height)
    };

    var function_data = {
        beforeSaveCallBck : null
    };

    var title = '',
        body = '';

        if( typeof(message) == 'string'){
            title = message,
            body = message;
        }

        if( typeof(message) == 'object'){
            if(!empty(message['title'])){
                title = message['title'];
            }
            if(!empty(message['message'])){
                body = message['message'];
            }
        }


    var dContent = $(
            '<div class="dialog-reestr-change">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: title,
        width: dialog.width,
        modal: true,
        buttons: [
            {
                text: "Да",
                "class": 'left saveButLicense',
                click: function () {
                    affairs.changeStatusReestr(id, elemType, stateType, nextStat, afterSaveCallbck, function_data['beforeSaveCallBck']);
                }
            },
            {
                text: "Нет",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $(".dialog-reestr-change").remove();
        },
        open: function () {
            $(".loading-img", dContent).fadeOut(200, function () {
                var str = "<div><p>" + body + "</p>";
                if (comm == 1) {
                    str += "<p>Комментарий: </p><input type='text' id='commentInput'></div>";
                } else {
                    str += "</div>";
                }

                dContent.html("").append(str);

                if( typeof(message) == 'object'){
                    if(typeof(message['handler']) == 'function'){
                        message['handler']( $(dContent), function_data );
                    }
                }
            });
        }
    });

    
};

affairs.changeStatusReestr = function (id, elemType, stateType, nextStat, afterSaveCallbck, beforeSaveCallBck) {
    if (typeof (beforeSaveCallBck) == 'function') {
                beforeSaveCallBck();
    }
    $.ajax({
        type: 'POST',
        dataType: "html",
        data: {"comment": $("#commentInput").val(), "elemType": elemType, "stateType": stateType, "nextState": nextStat},
        url: "/islod/affairs/change-status-reestr/" + id,
        success: function (answer) {
            //Все нормально

            if (answer == 1) {
                useTopHoverTitle("Статус изменен", 3000);
                $(".dialog-reestr-change").remove();
                $(".reestrType.active").click();
            } else {
                useTopHoverTitle(answer, 3000);
            }

            if (typeof (afterSaveCallbck) == 'function') {
                afterSaveCallbck();
            }
        },
        error: function () {
            useTopHoverTitle("Возникла ошибка при смене статуса", 3000);
        }
    });
}
affairs.rechangeReestrDialog = function (id) {
    var dialog = {
        width: Math.round((screen.width / 100) * 35)
    };
    var dContent = $(
            '<div class="dialog-reestr-rechange">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: "Переформирование лицензий и приложений",
        width: dialog.width,
        modal: true,
        buttons: [
            {
                text: "Да",
                "class": 'left saveButLicense',
                click: function () {
                    $.ajax({
                        type: 'POST',
                        dataType: "html",
                        url: "/islod/affairs/rechange-reestr/" + id,
                        beforeSend: function (xhr) {
                            $(".loading-overlay").show();
                        },
                        success: function (answer) {
                            //Все нормально
                            if (answer == 1) {
                                $(".loading-overlay").hide();
                                useTopHoverTitle("Проекты переформированы", 3000);
                                $(".dialog-reestr-rechange").remove();
                                $(".reestrType.active").click();
                            } else {
                                $(".loading-overlay").hide();
                                useTopHoverTitle(answer, 3000);
                            }
                        },
                        error: function () {
                            useTopHoverTitle("Возникла ошибка при смене статуса", 3000);
                        }
                    });
                }
            },
            {
                text: "Нет",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        close: function () {
            $(".dialog-reestr-rechange").remove();
        },
        open: function () {
            $(".loading-img", dContent).fadeOut(200, function () {
                dContent.html("").append("При переформировании проектов лицензий (приложений) существующие проекты лицензии (приложений) будут заменены переформированными проектами. Действительно переоформить ? ");
            });
        }
    });
};

affairs.getSignatoriesForDoc = function (docType) {
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/islod/affairs/get-signatories-for-doc/" + docType,
        success: function (answer) {
            $("td#signatories").html(answer);
        },
        error: function () {
            useTopHoverTitle("Возникла ошибка при получении подписантов", 3000);
        }
    });
}

affairs.getReasonForProcedure = function(idProcedure,idReason){
    if(idProcedure==undefined ||idProcedure==0){
        idProcedure = "all";
    }
    if(idReason==undefined ||idReason==0){
        var url = "/islod/affairs/get-reason-for-procedure/" + idProcedure;
    }else{
        var url = "/islod/affairs/get-reason-for-procedure/" + idProcedure +"/"+idReason;
    }
    
    if((idReason==undefined ||idReason==0)&&(idProcedure==undefined ||idProcedure==0)){
        var url = "/islod/affairs/get-reason-for-procedure/all" ;
    }
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: url,
        success: function (answer) {
            $("select#reasonSelect").html(answer);
        },
        error: function () {
            useTopHoverTitle("Возникла ошибка при получении причин", 3000);
        }
    });
}

affairs.deallocationSupplement = function(idSupplement,idDeal){
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/islod/affairs/deallocation-supplement/"+idSupplement+"/"+idDeal,
        success: function (answer) {
            if(answer=="ok"){
                affairs.rechangeReestrDialog(idDeal);
            }else{
                useTopHoverTitle("Возникла ошибка при откреплении приложения", 3000);
            }
        },
        error: function () {
            useTopHoverTitle("Возникла ошибка при откреплении приложения", 3000);
        }
    });
}