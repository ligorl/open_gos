var users = users || {};

users.getUserDialog = function( userId){
    var selectorName = 'user-dialog',
        page = _getPage().getUser( userId),
        title = $( page).find('.hidden-data input[name="dialog-title"]').val();

    createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
           height : 415,
           binder : userBinder().bind(),
           buttons: [
                {
                    text: "Сохранить",
                    "class": 'right ',
                    click: function () {
                        var data = $('.user-dialog-block form').serializeArray();
                        _save().user( data);
                        $(this).dialog('close');
                        userListBinder().getUsers();
                    }
                },
                {
                    text: "Отменить",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });
}

users.getNewDialog = function( userId){
    var selectorName = 'user-dialog',
        page = _getPage().newUser(),
        title = $( page).find('.hidden-data input[name="dialog-title"]').val();

    createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
           height : 420,
           binder : userBinder().bind(),
           buttons: [
                {
                    text: "Сохранить",
                    "class": 'right ',
                    click: function () {
                        var data = $('.user-dialog-block form').serializeArray();
                        _save().user( data);
                        $(this).dialog('close');
                        userListBinder().getUsers();
                    }
                },
                {
                    text: "Отменить",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });
}

users.getRoleMatrix = function( ){
    var selectorName = 'role-matrix-dialog',
        page = _getPage().roleMatrix(),
        title = 'Матрица ролей';
	    dialog = {
        height: Math.round((screen.height / 100) * 82) - 50
		};

    createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
		   height: dialog.height,
          // binder : userBinder().bind(),
           buttons: [
                {
                    text: "Добавить роль",
                    "class": 'right ',
                    click: function () {
                        users.addRole( this);
                    }
                },
                {
                    text: "Сохранить",
                    "class": 'right ',
                    click: function () {
                        var matrixData = {};
                        $('.role-matrix-block .role-matrix tbody tr').each(function( i ,tr){
                            var rigth = $(tr).attr('key');
                            $(tr).children().each(function( n, td){
                                if(n>0){
                                    var role = $(td).attr('role');
                                    var roleid = $(td).attr('roleid');
                                    matrixData[role] = matrixData[role] || {};
                                    matrixData[role]['id'] = roleid;
                                    matrixData[role][rigth] = 0;
                                    if( $(td).find('input').prop('checked')){
                                         matrixData[role][rigth] = 1;
                                    }
                                }
                            });
                        });
                        _save().roleMatrix( matrixData);
                        $(this).dialog('close');
                    }
                },
                {
                    text: "Отменить",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });
	resizeContent($(".role-matrix-block .viewzone"));
    specialTableResize(".role-matrix-block");
}

users.addRole = function( matrixDialog){
    var selectorName = 'user-add-role-dialog',
        page = '<div class="add-role-dialog"><input type="text" name="role-name" /> <span class="error error-block"> </span></div>',
        title = 'Добавить роль';

    createStandartDialog({
           width: 430,
           height: 200,
           className : selectorName,
           data : page,
           title : title,
           buttons: [
                {
                    text: "Сохранить",
                    "class": 'right ',
                    click: function () {
                        var name = $('.add-role-dialog input[name="role-name"]').val(),
                            answer = _save().role(name);

                        if( !empty( answer['message'])){
                            $('.add-role-dialog .error-block').html( answer['message']);
                            $('.add-role-dialog input[name="role-name"]').css({'border-color':'#FF0000'});
                        }else{
                            $( this).dialog('close');
                            $( matrixDialog).dialog('close');
                            users.getRoleMatrix();
                        }   
                    }
                },
                {
                    text: "Отменить",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });
}