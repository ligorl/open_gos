/**
 * функционал обработки диалогов приастоновки лицензии/приложений
 * @type type
 */
licenseStop = {};

    //украдено  trunk/module/Ron/view/ron/declaration/get-application-fillials.phtml
    licenseStop.filterKeyUp = function(inputElement){
            console.log('искалка');
            $('#filteredTable tbody tr.reorg_string').hide();
            var s1 = $(inputElement).val();
            if (s1 == '') {
                $('#filteredTable tbody tr.reorg_string').show();
                //hideFailSearchNotification();
            } else {
                var something_was_found = false;
                $('#filteredTable .reorg_string').each(function(i,item){
                    var text = $(item).html();
                    console.log(text);
                    if (licenseStop.detectPluralSubstringInclusions(s1, text)) {
                        $(item).show();
                        something_was_found = true;
                    }
                });

                if (something_was_found) {
                    licenseStop.hideFailSearchNotification();
                } else {
                    licenseStop.showFailSearchNotification();
                }
                //because making registry independend search $('#filteredTable td.name_org:contains("' + s1 + '")').parent('tr').show();
            }
        }

    licenseStop.showFailSearchNotification = function() {
        $('tr.notification_row').show();
    }

    licenseStop.hideFailSearchNotification = function() {
        $('tr.notification_row').hide();
    }

    licenseStop.detectPluralSubstringInclusions = function(needle, haystack) {
        var splited_needle  = needle.split(' ');
        var result = true;
        if (splited_needle.length>0) {
            for (var i=0; i<splited_needle.length; i++ ) {
                if (!this.lameSubstri(splited_needle[i],haystack)) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    licenseStop.lameSubstri = function (needle, haystack) {
        var lower_needle = needle.toLowerCase();
        var lower_haystack = haystack.toLowerCase();
        if (lower_haystack.indexOf(lower_needle)>=0) {
            return true;
        }
        return false;
    }

    /**
     * переносим выбранце филиалы в форму филиалов
     * @param {type} initElement
     * @returns {undefined}
     */
    licenseStop.butonAddToList = function(initElement){
        console.log('добавляем в перечень филиалов филиал')
        var orgAll = jQuery(initElement).parents('.license-stop-suplement-cheked-parent').first().find('input.selected_org:checked');
        jQuery('.license-stop-dialog-suplement-items tr').detach();
        if( jQuery(orgAll).length > 0){
            jQuery('.license-stop-dialog-suplement-items tr').detach();

            var img = jQuery('<img >');
            jQuery(img).attr('src',"/img/islod/delete.png");
            jQuery(img).attr('alt',"Удалить филиал");
            jQuery(img).attr('onclick',"licenseStop.removeFromFilial(this);");
            var imgTd = jQuery('<td></td>');
            jQuery(imgTd).append( img );

            jQuery(orgAll).each(function(ind, elem){
                var orgId = jQuery(elem).val();
                var OneRow   = jQuery(elem).parents('tr:first');
                var number   = jQuery('.supp-stop-number', OneRow).text();
                var orgName  = jQuery('.supp-stop-org-name', OneRow).text();
                var orgDoc   = jQuery('.supp-stop-org-doc', OneRow).text();
                if (orgDoc != ''){
                    orgDoc = ' (' + orgDoc +')';
                }
                orgName = 'Приложение ' + number + ' ' +orgName;
                var vuzinput = jQuery('<input>');
                jQuery(vuzinput).attr('type',"hidden");
                jQuery(vuzinput).attr('name','org['+orgId+']');
                jQuery(vuzinput).attr('value',orgId);
                var vuzTd = jQuery('<td></td>');
                jQuery(vuzTd).append(vuzinput);
                jQuery(vuzTd).append(orgName);
                var oneRow =  jQuery('<tr></tr>');
                jQuery(oneRow).append(imgTd.clone()).append(vuzTd);
                jQuery('.license-stop-dialog-suplement-items')
                    .append(oneRow);
            });
        }
        jQuery(initElement).parents('.license-stop-suplement-cheked-parent').first().parent().dialog('close');
    }

    /**
     * проставим галочки в выбилрлку у уже выбраных
     * @param {type} elementIt
     * @returns {undefined}
     */
    licenseStop.checkAlredyCheked = function(elementIt){
        console.log('отметим уде отмеченные');
        jQuery('.license-stop-dialog-suplement-items input').each(function(ind, elem){
            var id = jQuery(elem).val();
            jQuery('.license-stop-suplement-cheked-parent input.selected_org[value="'+id+'"]').prop('checked',true);
        });
    }

    /**
     * сохранило успшно
     * @returns {undefined}
     */
    licenseStop.sendResultOk = function(){
        var licForm = $('#licenseForm'),
            title = $('.license-dialog-title').val(),
            licId = $(licForm).find('input[name="Id"]').val();
    
        licenseStop.isSendBlocked = false;
        jQuery('.krutilka-lod').remove();
        jQuery('.license-stop-dialog-parent').show();
        useTopHoverTitle("Сохранено", 2001);
        console.log("успешно записало  закрываем окошко 1");
        //закрыть окошко выбора лицении лицензии
        jQuery('.license-stop-dialog-parent').parent().dialog('close');
         console.log("успешно записало  закрываем окошко 2");
        //закрыть окошко карточку лицензии
        jQuery('.supplement-info-dialog').parent().dialog('close');
         console.log("успешно записало  закрываем окошко 3");
        //перегрузить страничку с лицензиями
         console.log("успешно записало  закрываем окошко 4");
        //jQuery('.dialog-license-info').parent().dialog('close');
        jQuery('.dialog-license-info').parent().remove();
        var curentPage = jQuery('#pagination .current-page').text();
        console.log('текущая страница'+curentPage)
        _serverQuery(curentPage);
        
        license._get_license_info( licId, title);
    }

    /**
     * при записи получили ошибку
     * ошшибка ушла в iframe
     * там ее и выцепливаем
     * @returns {undefined}
     */
    licenseStop.sendResultError = function(){
        licenseStop.isSendBlocked = false;
        jQuery('.krutilka-lod').remove();
        jQuery('.license-stop-dialog-parent').show();
        var iframe =  document.getElementById('license-stop-dialog-iframe');
        var iframeDoc = iframe.contentWindow.document;
        var resMessage =  jQuery('#resultMessage', iframeDoc).text();
        var resConsole =  jQuery('#resultConsole', iframeDoc).text();
        if(typeof (resMessage) == 'undefined' || resMessage == ''){
            resMessage = 'Ошибка при сохранении';
        }
        useTopHoverTitleError(resMessage, 2004);
        if(typeof (resConsole) != 'undefined' && resConsole != ''){
            console.log(resConsole);
        }

    }

    /**
     * проверка перед отправкой на сервер, чтоб если в фиилиалах, то были выбраны филиалы
     * @returns {Boolean}
     */
    licenseStop.checkBeforeSendToServer = function() {
        if(licenseStop.isSendBlocked ){
            useTopHoverTitleError("запрос в обработке", 2003);
            return false;
        }
        //проверить если необязательные поля и есть

        //сбросим сообщение об ошибках
        var iframe =  document.getElementById('license-stop-dialog-iframe');
        var iframeDoc = iframe.contentWindow.document;
        if(jQuery('#resultMessage', iframeDoc).length){
            jQuery('#resultMessage', iframeDoc).remove();
        }
        if(jQuery('#resultConsole', iframeDoc).length){
            jQuery('#resultConsole', iframeDoc).remove();
        }
        if(
            'filial' == jQuery('#license-stop-dialog-form input:checked[name="licenseStopIs"]').val()
            &&
            jQuery('#license-stop-dialog-form .license-stop-dialog-suplement-items tr').length < 1
        ){
            console.log('хотим отправить на сервер - не выбрано приложеня');
            useTopHoverTitleError("Не выбраны приложения", 2003);
            return false;
        }
        //влепим крутилку
        var uId = 'id-'+getRandomInt(1000,9999)+'-'+getRandomInt(1000,9999);
        jQuery('.license-stop-dialog-parent').parent().prepend(
               '<div id="'+uId+'" class="krutilka-lod"></div>'
                //альтернативная крутилк, малость косая
                //"<div  id='"+uId+"' class='loading-img'><img src='/img/loading.gif' /></div> "
        );
        jQuery('.license-stop-dialog-parent').hide();
        //таймер на 30 сек
        setTimeout(function () {
            if(jQuery('#'+uId).length){
                jQuery('#'+uId).remove();
                useTopHoverTitleError("Не дождались ответа", 2003);
                console.error("Не дождались ответа");
                licenseStop.isSendBlocked = false;
                jQuery('.license-stop-dialog-parent').show();
            }
        }, 180000);//время ожидания мили секунд
        licenseStop.isSendBlocked = true;
        return true;
    }
    /**
     * отправляем на сервер
     * @returns {undefined}
     */
    licenseStop.sendToServer = function() {
        jQuery('#license-stop-dialog-form input[type="submit"]').click();
    }

    /**
     * уберем филиал из списка филиалов
     * @param {type} element
     * @returns {undefined}
     */
    licenseStop.removeFromFilial = function(element){
        jQuery(element).parents('tr:first').remove();
    }

    licenseStop.isSendBlocked = false;

    /**
     * получи случайное число в диапозоне
     * @param {type} min
     * @param {type} max
     * @returns {Number}
     */
    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    /**
     * вешалка обработчиков
     * на кллендари
     *
     * @returns {undefined}
     */
    licenseStop.initDialog = function(){
        $(".input-date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy"
        });
        $(".input-date").each(function(i,el){
            if( $(el).val() == '01.01.1970'){
                $(el).val('');
            }
        });
    }

    /**
     * скрывалка/показывалка перечня выраных филиалов
     *
     * @param {type} element
     * @returns {undefined}
     */
    licenseStop.licenseStopDialogCheckSuplement = function(element){
        if(jQuery(element).val() == 'filial'){
            jQuery(element).parents('.license-stop-dialog-parent').first().find('.license-stop-dialog-suplement-view').show();
        } else {
            jQuery(element).parents('.license-stop-dialog-parent').first().find('.license-stop-dialog-suplement-view').hide();
        }
    }

    /**
     * рисовалка диалога выбора филиалов
     *
     * @param {type} licenseId
     * @param {type} whereSelected
     * @returns {Number}
     */
    licenseStop.licenseStopDialogOpenSuplement = function(licenseId, whereSelected, addGet){
        if( typeof (addGet) == 'undefined' ){
            addGet = {};
        }
        if(typeof (licenseId) == 'undefined' || licenseId == ''){
            useTopHoverTitleError('не задана лицензия');
            return 0;
        }
        var prefix = 'ЛОД - вывод диалога выбора приложений yuigy: ';
        console.info(prefix + 'начинаем');
        var dContent = $(
                '<div class="license-stop-suplement-cheked-dialog">'
                + '</div>'
            );
        if(typeof (whereSelected) != 'undefined' && whereSelected != ''){

        }
        //ширина окошка динамически в зависимости от масштаба
        var widthWindow = jQuery(window).innerWidth();
        widthWindow = widthWindow - 50;
        var heightWindow = jQuery(window).innerHeight();
        heightWindow = heightWindow - 200;

        $(dContent).dialog({
                title: 'Выбор приложения',
                width: widthWindow,
                height: heightWindow,
                //position: 'center',
                position: {
                    my: "center center",
                    at: "center center",
                    of: window
                },
                modal: true,
                close: function(event, ui) {
                    $(this).remove();
                },
                open: function() {
                    licenseSlopDoDialogLoad(dContent, licenseId,'/islod/index/get-license-supplement-to-stop', addGet);
                },

        });
        jQuery(dContent).css('overflow-y', 'hidden');

    }

    /*
     * обработчик мышки, раскрашивает строчку в серый цвет
     */
    licenseStop.setItrowBkgrGrey = function(element){
        var row = jQuery(element);
        var allTd = jQuery(row).parent().children().children();
        jQuery(allTd).removeClass('bk-grd-grey');
        jQuery(row).children().addClass('bk-grd-grey');
        jQuery(jQuery(row)).mouseleave(function(){
           var row = jQuery(this);
           jQuery(row).children().removeClass('bk-grd-grey');
        });
    }

    /**
     * тык по строчке
     * @param {type} element
     * @returns {undefined}
     */
    licenseStop.clickToRow = function(element){
        jQuery('input[type="checkbox"]',element).click();
    }

    licenseStop.onChangeInput = function(element){
        console.log('изменение в елементе');
        if( jQuery(element).is('[required]') ){

        } else {
            var form = jQuery(element).parents('form')[0];
            var inputAll = jQuery(form).find('input.notValidCheking');
            var emptyAll = true
            jQuery(inputAll).each(function(ind,el){
               var valEl = jQuery(el).val();
               valEl = valEl.toString()
               if( '' != valEl  ){
                   emptyAll = false;
               }
            });
            if( emptyAll ){
                jQuery(form).find('select').attr('required', false);
            } else {
                jQuery(form).find('select').attr('required', 'required');
            }
        }
    }



