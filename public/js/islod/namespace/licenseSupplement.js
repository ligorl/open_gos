var licenseSupplement = {};
/**
 * Создание диалога для приложения лицензии
 */
licenseSupplement.createSupplementInfoDialog = function(supplementId){
    var selectorName = 'supplement-info-dialog',
        page = _getPage({rootPath : '/eo/license/'}).supplementInfoFromId( supplementId),
        title = $(page).find('.supplement-dialog-title').val();
	var height = Math.round(screen.height * 0.87);
    if( page){
        createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
		   height: height - 80,
           binder : supplementInfoBinder(),
           buttons: [
               /*
            {
                text: "Сохранить",
                "class": 'left saveLicenseSupplement',
                click: function () {
                    licenseSupplement.saveLicenseSupplement();
                }
            },
            */
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
            ]
        });

         $(".input-date-supplement:not([readonly])").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "dd.mm.yy"
                    });
    }
}

/**
 * Сохраненеи инфы о приложениях 
 */
licenseSupplement.saveLicenseSupplement = function(){
    var data = $("form#supplementInfoForm").serialize();
    _save().supplementInfo( data);
}

/**
 * Работа с вкладками в окне информации о сертификате 
 */
licenseSupplement.supplementInfoTabActionCaller = function(tabButton){
    var tabName = $( tabButton).attr("tab-container"),
        containerClass = '.'+tabName;
        
    switch( tabName){
        case 'education-programs':
            if( empty( $( containerClass).html())){
                organization._getEducationProgramsTab( containerClass);
                resizeContent($(".education-programs .viewzone"));
                specialTableResize(".education-programs");
            }
        break;
        case 'education-location-list':
            if( empty( $( containerClass).html())){
                licenseSupplement.getEducationLocationList( containerClass);
                resizeContent($(".education-location-list .viewzone"));
                specialTableResize(".education-location-list ");
            }
        break;
        default:
        break;
    }
}

licenseSupplement.getEducationLocationList = function( containerClass){
    var supplementId = $('.current-supplement-dialog-id').val(),
        page = _getPage({rootPath : '/eo/license/'}).educationLocationList( supplementId);

    if( page){
        $( containerClass).html( page);
    }
}