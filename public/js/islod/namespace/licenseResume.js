/**
 * fileName: licenseResume.js
 * функционал обработки диалогов возобновления действия лицензии
 * @type type
 */
licenseResume = {};

    /**
     * сохранило успшно
     * @returns {undefined}
     */
    licenseResume.sendResultOk = function(){
        var licForm = $('#licenseForm'),
            title = $('.license-dialog-title').val(),
            licId = $(licForm).find('input[name="Id"]').val();
     
        licenseResume.isSendBlocked = false;
        jQuery('.krutilka-lod').remove();
        jQuery(this.content).show();
        useTopHoverTitle("Сохранено", 2001);
        console.log("успешно записало  закрываем окошко 1");
        //закрыть окошко выбора лицении лицензии
        jQuery(this.dialog).dialog('close');
         console.log("успешно записало  закрываем окошко 2");
        //закрыть окошко карточку лицензии
         jQuery('.supplement-info-dialog').dialog('close');
         console.log("успешно записало  закрываем окошко 3");
        //перегрузить страничку с лицензиями
         console.log("успешно записало  закрываем окошко 4");
        //jQuery('.dialog-license-info').parent().dialog('close');
        jQuery('.dialog-license-info').parent().remove();
        var curentPage = jQuery('#pagination .current-page').text();
        console.log('текущая страница'+curentPage)
        _serverQuery(curentPage);
        
        license._get_license_info( licId, title);
    }

    /**
     * при записи получили ошибку
     * ошшибка ушла в iframe
     * там ее и выцепливаем
     * @returns {undefined}
     */
    licenseResume.sendResultError = function(){
        licenseResume.isSendBlocked = false;
        jQuery('.krutilka-lod').remove();
        jQuery(this.content).show();
        var iframeId =  jQuery('form', this.content).attr('target');
        var iframe =  document.getElementById(iframeId);
        var iframeDoc = iframe.contentWindow.document;
        var resMessage =  jQuery('#resultMessage', iframeDoc).text();
        var resConsole =  jQuery('#resultConsole', iframeDoc).text();
        if(typeof (resMessage) == 'undefined' || resMessage == ''){
            resMessage = 'Ошибка при сохранении';
        }
        useTopHoverTitleError(resMessage, 2004);
        if(typeof (resConsole) != 'undefined' && resConsole != ''){
            console.log(resConsole);
        }

    }

    /**
     * проверка перед отправкой на сервер, чтоб если в фиилиалах, то были выбраны филиалы
     * @returns {Boolean}
     */
    licenseResume.checkBeforeSendToServer = function() {
        console.log('диалог возобновление действия лицензии - подготовка к отправке на сервер');
        if(this.isSendBlocked ){
            useTopHoverTitleError("запрос в обработке", 2003);
            return false;
        }
        //проверка перед отправкой

        //сбросим сообщение об ошибках
        var iframeId =  jQuery('form', this.content).attr('target');
        var iframe =  document.getElementById(iframeId);
        var iframeDoc = iframe.contentWindow.document;
        if(jQuery('#resultMessage', iframeDoc).length){
            jQuery('#resultMessage', iframeDoc).remove();
        }
        if(jQuery('#resultConsole', iframeDoc).length){
            jQuery('#resultConsole', iframeDoc).remove();
        }

        //влепим крутилку
        var uId = 'id-'+getRandomInt(1000,9999)+'-'+getRandomInt(1000,9999);
        jQuery(this.dialog).prepend(
               '<div id="'+uId+'" class="krutilka-lod"></div>'
                //альтернативная крутилк, малость косая
                //"<div  id='"+uId+"' class='loading-img'><img src='/img/loading.gif' /></div> "
        );
        jQuery(this.content).hide();
        //таймер на 30 сек
        var actionClass = this;
        setTimeout(function () {
            if(jQuery('#'+uId).length){
                jQuery('#'+uId).remove();
                useTopHoverTitleError("Не дождались ответа", 2003);
                console.error("Не дождались ответа");
                actionClass.isSendBlocked = false;
                jQuery(actionClass.content).show();
            }
        }, 4*60*1000);//<- врямя ожидания ответа в милиСекундах
        licenseResume.isSendBlocked = true;
        return true;
    }

    /**
     * отправляем на сервер
     * @returns {undefined}
     */
    licenseResume.sendToServer = function(element) {
        jQuery('input[type="submit"]', this.content).click();
    }

    /**
     * флаг блокировки при отправке
     */
    licenseResume.isSendBlocked = false;

    /**
     * получи случайное число в диапозоне
     * @param {type} min
     * @param {type} max
     * @returns {Number}
     */
    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    /**
     * содержимое диалога
     * @type type
     */
    licenseResume.conten = null;
    
    /**
     * указка на диалог
     * @type type
     */
    licenseResume.dialog = null;
    
    /**
     * инициализация диалога
     * @param {type} element
     * @returns {licenseResume.initDialog}
     */
    licenseResume.initDialog = function(element){
        console.log('диалог возобновление лицензии - инициализируем диалог');
        //изменяе высоту окошку, а то плоххо отсчитываются
        //var heightWindow = jQuery(window).innerHeight();
        //heightWindow = heightWindow - 30;
        //jQuery(dContent).dialog( "option", "height", heightWindow );
        if(jQuery(element).length < 1 ){
            element = '.license-resume-dialog-parent';
        }
        this.content = jQuery(element);
        this.dialog  = jQuery(this.content).parent();

        $(".input-date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy"
        });
        $(".input-date").each(function(i,el){
            if( $(el).val() == '01.01.1970'){
                $(el).val('');
            }
        });

        //зафиксируем высоту
        var height = jQuery('#license-resume-dialog-form',this.content).outerHeight();
        $(this.content).parent().innerHeight(height);
		//зафиксируем высоту
        var height = jQuery('#license-stop-dialog-form',this.content).outerHeight();
        $(this.content).parent().innerHeight(height);

        //отцентровать окно
        var widthWindow = jQuery(window).innerWidth();
        //widthWindow = widthWindow - 50;
        var heightWindow = jQuery(window).innerHeight();
        //heightWindow = heightWindow - 10;
        //heightWindow ='auto';
        
        //ширина - равна ширине карточке лицензии
        //var licenseDialogWidth = jQuery('.dialog-certificate-info-container').innerWidth();
        //jQuery(this.dialog).css('max-width', licenseDialogWidth + 'px');

        var width = jQuery(this.dialog).dialog( "option", "width" );
        //var width = jQuery(this.dialog).innerWidth();
        var height = jQuery(this.dialog).dialog( "option", "height" );
        var height = jQuery(this.dialog).outerHeight();
        var top =  (jQuery(window).innerHeight() - height)/2;
        var left = (jQuery(window).innerWidth() - width)/2;
        jQuery(this.dialog).dialog( "option", "position", [top, left] );

        //ограничим изменения окошка
        jQuery(this.dialog).css('min-width', '600px');
        jQuery(this.dialog).css('min-height', height+'px');        
        

        console.log('диалог возобновление лицензии - окончание инициализируем диалог');

    }

    /**
     * скрывалка/показывалка перечня выраных филиалов
     *
     * @param {type} element
     * @returns {undefined}
     */
    licenseResume.licenseResumeDialogCheckSuplement = function(element){
        if(jQuery(element).val() == 'filial'){
            jQuery(element).parents('.license-stop-dialog-parent').first().find('.license-stop-dialog-suplement-view').show();
        } else {
            jQuery(element).parents('.license-stop-dialog-parent').first().find('.license-stop-dialog-suplement-view').hide();
        }
    }

    /**
     * рисовалка диалога выбора филиалов
     *
     * @param {type} licenseId
     * @param {type} whereSelected
     * @returns {Number}
     */
    licenseResume.licenseResumeDialogOpenSuplement = function(licenseId, whereSelected){
        if(typeof (licenseId) == 'undefined' || licenseId == ''){
            useTopHoverTitleError('не задана лицензия');
            return 0;
        }
        var prefix = 'ЛОД - вывод диалога выбора приложений yuigy: ';
        console.info(prefix + 'начинаем');
        var dContent = $(
                '<div class="license-stop-suplement-cheked-dialog">'
                + '</div>'
            );
        if(typeof (whereSelected) != 'undefined' && whereSelected != ''){

        }
        //ширина окошка динамически в зависимости от масштаба
        var widthWindow = jQuery(window).innerWidth();
        widthWindow = widthWindow - 50;
        var heightWindow = jQuery(window).innerHeight();
        heightWindow = heightWindow - 200;

        $(dContent).dialog({
                title: 'Выбор приложения',
                width: widthWindow,
                height: heightWindow,
                //position: 'center',
                position: {
                    my: "center center",
                    at: "center center",
                    of: window
                },
                modal: true,
                close: function(event, ui) {
                    $(this).remove();
                },
                open: function() {
                    licenseSlopDoDialogLoad(dContent, licenseId,'/islod/index/get-license-supplement-to-stop');
                },

        });
        jQuery(dContent).css('overflow-y', 'hidden');

    }

    /*
     * обработчик мышки, раскрашивает строчку в серый цвет
     */
    licenseResume.setItrowBkgrGrey = function(element){
        var row = jQuery(element);
        var allTd = jQuery(row).parent().children().children();
        jQuery(allTd).removeClass('bk-grd-grey');
        jQuery(row).children().addClass('bk-grd-grey');
        jQuery(jQuery(row)).mouseleave(function(){
           var row = jQuery(this);
           jQuery(row).children().removeClass('bk-grd-grey');
        });
    }

    /**
     * тык по строчке
     * @param {type} element
     * @returns {undefined}
     */
    licenseResume.clickToRow = function(element){
        jQuery('input[type="checkbox"]',element).click();
    }