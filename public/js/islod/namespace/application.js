var application = application || {};

application.info = function( id ){
    var selectorName = 'application-info-dialog',
        page = _getPage().applicationInfo({ id:id }),
        title = $(page).find('.hidden-block input[name="title"]').val(),
        dialog_1 = percentageOfTheScreen( 78 ),
        dialog_2 = percentageOfTheScreen( 96 );

        if( page){
            createStandartDialog({
                className : selectorName,
                data : page,
                title : title,
                binder : applicationBinder( ),
                width:dialog_2.width,
                height:dialog_1.height,
                buttons: [
                    {
                        text: "Закрыть",
                        "class": 'right',
                        click: function () {
                            $(this).dialog('close');
                        }
                    },
                ]
            });
            resizeContent( $(".application-info .viewzone") );
            specialTableResize();
            $(".application-info .viewzone tbody").height( $(".application-info .viewzone").height());
            $('.application-info table tr table tbody').css({height:'100%'});
        }
}

