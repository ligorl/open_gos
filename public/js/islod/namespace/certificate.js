var certificate = certificate || {};

certificate.supplementDialog = function( id ){
    var selectorName = 'certificate-supplement-info-dialog',
        page = _getPage().certificateSupplement({ id:id }),
        title = $(page).find('.hidden-block input[name="title"]').val(),
        dialog = percentageOfTheScreen( 80 );

        if( page){
            createStandartDialog({
                className : selectorName,
                data : page,
                title : title,
				width: dialog.width + 150,
                height:dialog.height,
                binder : certificateSuppementBinder( ),
                buttons: [
                    {
                        text: "Закрыть",
                        "class": 'right',
                        click: function () {
                            $(this).dialog('close');
                        }
                    },
                ]
            });

            resizeContent( $(".certificate-supplement .viewzone") );
            specialTableResize();

            var mainSpaceHeigh = $('.certificate-supplement-info-dialog').height(),
                procent = mainSpaceHeigh -((mainSpaceHeigh/100)*10 );
            $('.tab-certificate-supplement-info').height( procent );
            $('.tab-certificate-supplement-info tbody').height( procent );
            $('.tab-education-program').height( procent );
        }
}

