var isHashChangerClick = false;

$(document).ready(function(){
    //Вешаем обработчик для кнопок шагов    
    $(".btn",".steps_div").click(function(){
        //меняем внешний вид
        $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
        $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
    });

    bindIndexEvents();

    $(window).bind('hashchange', function(){
        if(isHashChangerClick){
            isHashChangerClick = false;
            return;
        }
        if(window.location.hash){
            $("a[href='"+window.location.hash+"']").parent().click();
        }else{
            window.location.hash = $(".btn-lg").attr('event');;
            var event=$(".btn>a",".steps_div").first().attr('event');
            var controller=$(".btn>a",".steps_div").first().attr('controller');
            indexEventsAjaxHandler($(".btn>a",".steps_div").first(), event, controller);
        }
    });
    if(window.location.hash){
        $("a[href='"+window.location.hash+"']").parent().click();
    }else{
        window.location.hash = $(".btn-lg").attr('event');
        var event=$(".btn>a",".steps_div").first().attr('event');
        var controller=$(".btn>a",".steps_div").first().attr('controller');
        indexEventsAjaxHandler($(".btn>a",".steps_div").first(), event,controller);
    }

    $(window).resize(function(){
        resizeContent($(".container .viewzone"));
        resizeContent($(".content_td .content"));
    });

});

function bindIndexEvents() {
    //проблема - этот обработчик вешается много раз, а потом запускается множество раз
    // если его сбрасывать, то он убивается и во время обработки, решиб пооставить влаг
    if( 'string' == typeof(bindIndexFlag) &&  bindIndexFlag == 'jsEoLicenseMain' ){
        return 0;
    }
    bindIndexFlag = 'jsEoLicenseMain';
    $('.btn').unbind('click');
    //Вешаем обработчик на переключение вкладок
    $('.btn').click(function() {
        isHashChangerClick = true;
        var event = $(this).attr('event');
        var _this = this;
        var controller = $(this).attr('controller');
        if (event) {
            $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
            $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
            indexEventsAjaxHandler(_this, event,controller);
        }
    });
}

function indexEventsAjaxHandler(_this, event ,keep_selector) {
    console.log('обработчик тыка по елементам меню слева 4');
    if (!event) return false;
        var destination_link = '/ron/'+keep_selector+'/' + event;

    $.ajax({
        type: 'POST',
        url: destination_link,
        beforeSend: function(){
            //ресайзимблок с контентом
			resizeContent($(".content_td .content"));
			$(".content_td .content").html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
            //$(".content_td .content").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);            //

        },
        success: function(answer) {
            $(".content_td .content .loading-img").fadeOut(500, function(){
                answer = $(answer)

                $(".content_td .content").html("").append(answer);
                resizeContent($(".viewzone", answer));

                event = event.split("-").join("");
                //проверяем на установлено уже или нет
                //надо для если устанавливается при автопереходе
                if(jQuery('.active_appl').length<1){
                //если это переходы по шагам, то выделяем в таблице первую запись
                    if(event == "expertise"){
                        $(".viewzone .intable tbody.content tr").first().addClass("active_appl");
                        //вешаем обработчик по дабл клику
                        /*$(".content_td").undelegate(".active_appl", "click").delegate(".active_appl", "click", view_expertise);*/
                    }
                    if(event == "experts" || event == "expertsnew"){
                        $(".viewzone .intable tbody.content tr").first().addClass("active_appl");
                        //вешаем обработчик по дабл клику
                        /*$(".content_td").undelegate(".active_appl", "click").delegate(".active_appl", "click", view_expert_card);*/
                    }
                    if(event == "agreements"){
                        $(".viewzone .intable tbody.content tr").first().addClass("active_appl");
                        //вешаем обработчик по дабл клику
                        /*$(".content_td").undelegate(".active_appl", "click").delegate(".active_appl", "click", view_expert_card);*/
                    }
                }

                //Запускаем обработчики
                var handlerName = keep_selector+event.charAt(0).toUpperCase() + event.substr(1)+"Handlers";

                if(window[handlerName])
                    window[handlerName]();

            })
        }
    });
}

function resizeContent(content){
    var wHeight = $(window).height();
    var delta = 180;
    if($(content).attr("data-heightdelta"))
        delta -= parseInt($(content).attr("data-heightdelta"));

    var contentHeight = wHeight - delta;
    $(content).height(contentHeight);
}

 /**
 * В специальной верхней зоне выводит текст из параметра text, 
 * и прячет его через время указанное в милисекундах в параметре time 
 * 
 * @param string text
 * @param int time
 */
function useTopHoverTitle(text, time) {
     if(!$("#hover-title").length){
         $('body').prepend('<div id="hover-title"></div>');
     }
    $("#hover-title").text(text);
    $("#hover-title").fadeIn("fast");
    setTimeout(function () {
        $("#hover-title").fadeOut('fast');
    }, time);
}

/**
 * В специальной верхней зоне выводит текст из параметра text,
 * и прячет его через время указанное в милисекундах в параметре time
 * Для ошибки
 *
 * @param string text
 * @param int time
 */
function useTopHoverTitleError(text, time) {
    if(typeof(time) == 'undefined'){
        time = 2000;
    }
    if(typeof(time) == 'string'){
        time = parseInt(time);
    }
    if( isNaN(time) ){
        time = 2000;
    }
    if(!$("#hover-title-error").length){
        $('body').prepend('<div id="hover-title-error"></div>');
    }
    $("#hover-title-error").text(text);
    $("#hover-title-error").fadeIn("fast");
    setTimeout(function () {
        $("#hover-title-error").fadeOut('fast');
    }, time);
}

/**
 * Вешаем событие на клик по всплывающему окну.
 */
function eventTopHoverClick() {
    $("body").undelegate("#hover-title", "click");
    $("body").delegate("#hover-title", "click", function () {
        $(this).toggle();
    });
}