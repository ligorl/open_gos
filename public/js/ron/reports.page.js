var filter = {};
var serverLink = null;
var contentColumns = 1;
var afterServerQueryFunction = function(){};

/**
 * обработка страницы вгрузки /ron/reports#unloading
 */
reportsUnloadingHandlers = function(){
    var ul_li = ".list-download-reports";
    
    
    $('span', ul_li).on("click", function(){
        var type = $(this).attr("id");
        if(type == "statements_in_status" || type == 'statements_in_work' || type == 'statements_op' || type == 'accredited_op')
            open_dialog("download", "<p>Подождите, идет сбор данных...</p>");
        else{
            open_dialog("download", "<p> Отчет в стадии разработки</p>", true); return;
        }
        get_count_items(type);
    })
}

/**
 * полчение колва записей которые надосформировать
 * @param {type} type
 * @returns {undefined}
 */
get_count_items = function(type){
    $(".download").html("<p>идет процесс полечения общего количества записей</p>");
    $.ajax({
        type: "POST",
        url: "/ron/reports/get-count-items-type",
        data: ({type: type}),
        success: function( total_count ){
            if(total_count > 0){
               $(".download").html("<p>Подождите, идет процесс генерирования Excel файла...</p><div id='progress'><div style='width: 99%;' class='bar'></div></div></div>");
                get_items_for_reports(type, total_count, 1, "" ); 
            }else{
                $(".download").html("<p>Нет данных для выгрузки</p>");
            }
            
        }
    });
}

get_items_for_reports = function(type, total_count, page, file_name)
{
    var step = 500, max_position = Math.ceil(total_count / step);
        if(page > max_position){
            console.log('formed data');
        //создать zip архив
        $.ajax({
            type: "POST",
            url: "/ron/reports/get-excel",
            data: ({file_name: file_name}),
            success: function(data){
                $(".download").dialog("close");
                open_dialog("download-items", "<p>Ссылка для скачивания  <a href='/public/reports/" + data + "'>файла</a></p>", true);
            }
        });
    }else{
        
        $.ajax({
            type: "POST",
            url: "/ron/reports/get-items-by-type",
            data: ({type: type, page : page, step: step, file_name: file_name}),
            success: function(file_name){
                page = page + 1;
                get_items_for_reports(type, total_count, page, file_name);
            }
        });
    }
    var width = Math.ceil((100*step*page)/total_count);
    if(width > 100) width=100;
    $('.bar', "#progress").width(width + '%');
    
}

create_txt_file = function(type){
    $.ajax({
        type: "POST",
        url: "/ron/reports/get-request-reports",
        data: ({type: type}),
        success: function(file_name){
            $(".download").html("<p>Подождите, идет процесс генерирования Excel файла...</p><div id='progress'><div style='width: 99%;' class='bar'></div></div></div>");
             $.ajax({
                type: "POST",
                url: "/ron/reports/get-excel",
                data: ({file_name: file_name}),
                success: function(file_name){
                    open_dialog("download", "<p>Ссылка для скачивания <a target='_blank' href='/public/reports/" + file_name + "'>Excel файла</p>");

                }
            });
        }
    });
}


/**
 * ф-я открытия лиалогового окна
 * @param {String} name идентификатор окна
 * @param {String} html содержиимое окна
 * @param {Boolen} close_button надо ли кнопку закрытия
 */
open_dialog = function(name, html, close_button, title_window){
    var title_window = title_window || "Уведомление";
    $('.' + name).remove();
    $("<div class='" + name + "'></div>")
        .html(html)
        .dialog({
            autoOpen:false,
            title: title_window,
            modal:true
        }).dialog("open");
        
    if(close_button){
       $('.' + name).dialog({
           buttons: [{
                text: "Закрыть",
                click: function(){$('.' + name).dialog("close");$('.' + name).remove();}
           }]
       }) 
    }
}

