var filter = {};
var serverLink = null;
var contentColumns = 1;
var afterServerQueryFunction = function(){};

/**
 * Функция вешает обработчики на пагинацию
 * @private
 */
_paginationHandler = function(){
    $(".page","#pagination").click(function(){
        var page = $(this).attr("page");
        _serverQuery(page);
    });
};

/**
 * Функция обрабатывает запросы к серверу
 * @param page - страница
 * @private
 */
_serverQuery = function(page){
    $.ajax({
        type: 'POST',
        url: serverLink,
        data: {
            filter: filter,
            page: page
        },
        beforeSend: function(){
            //отображаем крутилку
            $("tbody.content").html("").append(
                $("<tr><td colspan='"+contentColumns+"'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
            ).fadeIn(500);
        },
        success: function(answer) {
            $("tbody.content .loading-img").fadeOut(500, function(){
                answer = $(answer)
                $("tbody.content").html("").append($(answer).find(".content > tr"));
                $("#pagination").html("").append($(answer).find(".pagination > span"));
                _paginationHandler();
                afterServerQueryFunction();
            })
        }
    });
};

/**
 * Обработчики фильтра и пагинации для /#certificates
 */
indexCertificatesHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "Status" : $(".sState","#filter").val(),
        "Search" : $(".Search","#filter").val()
    };
    if($(".dtDateStart","#filter").val() != "")
        filter["DateStart"] = moment($(".dtDateStart","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
    if($(".dtDateEnd","#filter").val() != "")
        filter["DateEnd"] = moment($(".dtDateEnd","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
    //Ссылка на скрипт сервера
    serverLink = "/ron/index/certificates-page";
    //Количество колонок в таблице
    contentColumns = 8;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){
        //вешаем обработчик открытия подробной инфо о сертификатах
        $("tr",".ron-certificates").click(function(){
            var id = $(this).attr("id").split("_")[1];
            var title = $(this).attr("dialog-title");
            _certificateDialog(id, title);
        });
    }

    //Вешаем обработчики на календари
    $(".date-field", "#filter").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy"
    });
    $(".date-icon", "#filter").click(function(){
        $(this).parent().find("input").focus()
    })

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "Status" : $(".sState","#filter").val(),
            "Search" : $(".Search","#filter").val()
        };
        if($(".dtDateStart","#filter").val() != "")
            filter["DateStart"] = moment($(".dtDateStart","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        if($(".dtDateEnd","#filter").val() != "")
            filter["DateEnd"] = moment($(".dtDateEnd","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаем обработчик открытия подробной инфо о сертификатах
    $("tr",".ron-certificates").click(function(){
        var id = $(this).attr("id").split("_")[1];
        var title = $(this).attr("dialog-title");
        _certificateDialog(id, title);
    });

    //Вешаем обработчик на нажатие Enter на поиск
    $(".Search","#filter").keypress(function(e){
        if(e.keyCode==13){
            $(".btApply","#filter").click();
        }
    });
    
}

/**
 * Метод отрабатывает диалог с подробной информацие о сертификате
 * @param id
 * @param title
 * @private
 */
_certificateDialog =function(id, title){
    var dContent = $(
        '<div class="dialog-certificate-info">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "Свидетельство "+title,
        width: 1000,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-certificate-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по сертификату
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/index/certificate-info/"+id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Вешаем обработчик на переключение вкладок
                        $(".tab", dContent).click(function(){
                            $(".tab", dContent).removeClass("active");
                            $(this).addClass("active");

                            $(".content-block",dContent).hide();
                            $(".content-block."+$(this).attr("tab-container"),dContent).show();

                            $(".buttons-block",dContent).hide();
                            $(".buttons-block."+$(this).attr("tab-container"),dContent).show();
                        });
                        //Вешаем обработчик на открытие диалога с инфо по приложению
                        $(".supplements", dContent).delegate("tr", "click", function(){
                            var id = $(this).attr("id").split("_")[1];
                            var title = $(this).attr("dialog-title");
                            _supplementDialog(id, "Приложение "+title);
                        });

                        //Вешаем обработчики на поля даты
                        $(".date-field", dContent).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd.mm.yy"
                        });
                        $(".date-icon", dContent).click(function(){
                            $(this).parent().find("input").focus()
                        })
                        // Открывает панельку добавления нового распорядительногло документа
                        $(".btAddDocument",dContent).click(function(){
                            var odContent = $(
                                '<div class="dialog-orderDecument-add">' +
                                "<div style='text-align: center;margin-top:50px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
                                '</div>');
                            $(odContent).dialog({
                                title: "Распорядительный акт",
                                width: 640,
                                height: 430,
                                modal: true,
                                close: function() {
                                    $(".dialog, .dialog-orderDecument-add").remove();
                                },
                                open: function() {
                                    $.ajax({
                                        type: 'POST',
                                        dataType: "html",
                                        url: "/ron/index/certificate-document-add",
                                        success: function(answer) {
                                            //console.log(answer);
                                            $(".loading-img", odContent).fadeOut(500, function() {
                                                odContent.html("").append($(answer));
                                            });
                                        },
                                        error:function(e){
                                            console.log('ошибка при загрузке панели добавления распорядительного документа');
                                            console.log(e);
                                        }

                                    });
                                }
                            });
                        });

                        //Кнопки добавления новых полей загрузки
                        $(".addScanCertificate", dContent).click(function(){
                            var parent = $(this).parents("td");
                            parent.find(".field").append($('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanCertificate">'));
                        });
                        $(".addScanMaketCertificate", dContent).click(function(){
                            var parent = $(this).parents("td");
                            parent.find(".field").append($('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanMaketCertificate">'));
                        });

                        //Обработка сохранения свидетельства
                        $(".btSaveCert", dContent).click(function(){
                            //Находим все поля сертификата
                            var certFields = $(".content-block.certificate .certData",dContent);
                            //строим объект формы
                            var fd = new FormData();
                            certFields.each(function(i,item){
                                //name поля
                                var iName = $(item).attr("name");
                                //тип поля
                                var fType = $(item).attr("fType");
                                switch(fType){
                                    //Обработка дат
                                    case "date":
                                        var date = $(item).val();
                                        if(date != "")
                                            date = moment(date,"DD.MM.YYYY").format("YYYY-MM-DD")
                                        fd.append(iName,date);

                                        break;
                                    //Обрвботка файлов
                                    case "file":
                                        var fileData =  $(item)[0].files[0];
                                        if(typeof fileData != "undefined")
                                            fd.append(iName+"[]", fileData);
                                        break;
                                    //Действие по умолчанию
                                    default:
                                        fd.append(iName,$(item).val());
                                        break;
                                }
                            });
                            //Отправляем запрос на сервер
                            var id = $("#certificateId", dContent).val();
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                data: fd,
                                processData: false,
                                contentType: false,
                                url: "/ron/index/certificate-save/"+id,
                                success: function(answer) {
                                    //Проверяем если ошибка
                                    if(answer.result && answer.result=="error"){
                                        useTopHoverTitleError(answer.error);
                                        return;
                                    }
                                    //Если все хорошо выводим алерт и меняем/добавляем ссылки
                                    if(answer.result && answer.result=="success"){
                                        if(answer.ScanCertificate && answer.ScanCertificate != ""){
                                            var files = answer.ScanCertificate.split(";");
                                            $(".fileLink.ScanCertificate",dContent).html("");
                                            for(var i = 0; i< files.length; i++)
                                                $(".fileLink.ScanCertificate",dContent).append($("<a target='_blank' href='/uploads/"+files[i]+"'>"+files[i]+"</a><br>"));

                                            var parent = $("input[name=ScanCertificate]").parent();
                                            $("input[name=ScanCertificate]").remove();
                                            parent.html('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanCertificate">')
                                        }

                                        if(answer.ScanMaketCertificate && answer.ScanMaketCertificate != ""){
                                            var files = answer.ScanMaketCertificate.split(";");
                                            $(".fileLink.ScanMaketCertificate",dContent).html("");
                                            for(var i = 0; i< files.length; i++)
                                                $(".fileLink.ScanMaketCertificate",dContent).append($("<a target='_blank' href='/uploads/"+files[i]+"'>"+files[i]+"</a><br>"));

                                            var parent = $("input[name=ScanMaketCertificate]").parent();
                                            $("input[name=ScanMaketCertificate]").remove();
                                            parent.html('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanMaketCertificate">')
                                        }
                                        if(answer.newRow){
                                            $(".ron-certificates #cert_"+id).html($(answer.newRow).html());
                                        }
                                        useTopHoverTitle("Свидетельство успешно сохранено");
                                    }
                                }
                            });
                        })

                        //Обработчик добавления приложения
                        $(".btAddSupp", dContent).click(function(){
                            var certId = $("#certificateId", dContent).val();
                            _supplementDialog(null, "Добавление приложения к сертификату",  certId);
                        });

                        //============== Обработка пагинации
                        //функция вешает обработчики на пагинацию в этом диалоге
                        _certPaginationHandler = function(){
                            $(".pagination .page", dContent).click(function(){
                                var page = $(this).attr("page");
                                _suppServerQuery(page);
                            });
                        }
                        //Функция отправки запросов
                        _suppServerQuery = function(page){
                            $.ajax({
                                type: 'POST',
                                url: "/ron/index/certificate-supp-page",
                                data: {
                                    page: page,
                                    certId: $("#certificateId", dContent).val()
                                },
                                beforeSend: function(){
                                    //отображаем крутилку
                                    $("tbody.cert-supps-content", dContent).html("").append(
                                        $("<tr><td colspan='6'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
                                    ).fadeIn(500);
                                },
                                success: function(answer) {
                                    $("tbody.cert-supps-content .loading-img", dContent).fadeOut(500, function(){
                                        answer = $(answer)
                                        $("tbody.cert-supps-content", dContent).html("").append($(answer).find(".content > tr"));
                                        $(".pagination", dContent).html("").append($(answer).find(".pagination > span"));
                                        _certPaginationHandler();

                                    })
                                }
                            });
                        };
                        //вешаем обработчики на пагинацию
                        _certPaginationHandler();
                    });
                }
            });
        }
    });
}

/**
 * Метод отрабатывает диалог с подробной информацие о приложении сертификата сертификате
 * @param id
 * @param title
 * @private
 */
_supplementDialog = function(id, title, certId){
    console.log( 'рон - рисуем диалог приложений ' );
     
    var isCreateMode = false;
    if(id==null){
        isCreateMode = true;
    }
    if( 'undefined' ==  typeof(certId) ){
        console.log( 'рон - сертификат указан небыл ' );
        certId  = $("#certificateId.certData").val();
    }
    console.log( 'рон - ид сертификата  ' + certId  );

    var dContent = $(
        '<div class="dialog-certsupplement-info">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: title,
        width: 1200,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-certsupplement-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по приложению
            $.ajax({
                type: 'POST',
                dataType: "html",
                data: {
                    certId: certId
                },
                url: "/ron/index/certificate-supplement-info/"+id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Вешаем обработчик на переключение вкладок
                        $(".tab", dContent).click(function(){
                            if($(this).hasClass("disabled"))
                                return;

                            $(".tab", dContent).removeClass("active");
                            $(this).addClass("active");

                            $(".content-block",dContent).hide();
                            $(".content-block."+$(this).attr("tab-container"),dContent).show();

                            $(".buttons-block",dContent).hide();
                            $(".buttons-block."+$(this).attr("tab-container"),dContent).show();

                            //Показываем блок поиска
                            $(".tab-hide", dContent).hide();
                            $("."+$(this).attr("tab-container")+"-show",dContent).show();
                        });

                        //Вешаем обработчики на поля даты
                        $(".date-field", dContent).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd.mm.yy"
                        });
                        $(".date-icon", dContent).click(function(){
                            $(this).parent().find("input").focus()
                        })

                        //Кнопки добавления новых полей загрузки
                        $(".addScanCertificate", dContent).click(function(){
                            var parent = $(this).parents("td");
                            parent.find(".field").append($('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanCertificate">'));
                        });
                        $(".addScanMaketCertificate", dContent).click(function(){
                            var parent = $(this).parents("td");
                            parent.find(".field").append($('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanMaketCertificate">'));
                        });

                        //Функция сохранения
                        var saveSuppFunc = function(dContent, isCreateMode, data){
                            var id = "null";
                            if(!isCreateMode)
                                id = $("#supplementId", dContent).val();
                            //строим объект формы
                            var fd = new FormData();
                            //Находим все поля сертификата
                            var certFields = $(".content-block.supplement .certData",dContent);
                            certFields.each(function(i,item){
                                //name поля
                                var iName = $(item).attr("name");
                                //тип поля
                                var fType = $(item).attr("fType");
                                switch(fType){
                                    //Обработка дат
                                    case "date":
                                        var date = $(item).val();
                                        if(date != "")
                                            date = moment(date,"DD.MM.YYYY").format("YYYY-MM-DD")
                                        fd.append(iName,date);

                                        break;
                                    //Обрвботка файлов
                                    case "file":
                                        var fileData =  $(item)[0].files[0];
                                        if(typeof fileData != "undefined")
                                            fd.append(iName+"[]", fileData);
                                        break;
                                    //Действие по умолчанию
                                    default:
                                        fd.append(iName,$(item).val());
                                        break;
                                }
                            });

                            fd.append("fk_isgaCertificate",$("#certificateId.certData").val());

                            if(data && data.orderNumber){
                                fd.append("orderNumber",data.orderNumber);
                            }
                            if(data && data.orderId){
                                fd.append("orderId",data.orderId);
                            }

                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                data: fd,
                                processData: false,
                                contentType: false,
                                url: "/ron/index/certificate-supplement-save/"+id,
                                success: function(answer) {
                                    //Проверяем если ошибка
                                    if(answer.result && answer.result=="error"){
                                        useTopHoverTitleError(answer.error);
                                        return;
                                    }
                                    //Если все хорошо выводим алерт и меняем/добавляем ссылки
                                    if(answer.result && answer.result=="success"){
                                        if(answer.ScanCertificate && answer.ScanCertificate != ""){
                                            var files = answer.ScanCertificate.split(";");
                                            $(".fileLink.ScanCertificate",dContent).html("");
                                            for(var i = 0; i< files.length; i++)
                                                $(".fileLink.ScanCertificate",dContent).append($("<a target='_blank' href='/uploads/"+files[i]+"'>"+files[i]+"</a><br>"));

                                            var parent = $("input[name=ScanCertificate]").parent();
                                            $("input[name=ScanCertificate]").remove();
                                            parent.html('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanCertificate">')
                                        }
                                        if(answer.ScanMaketCertificate && answer.ScanMaketCertificate != ""){
                                            var files = answer.ScanMaketCertificate.split(";");
                                            $(".fileLink.ScanMaketCertificate",dContent).html("");
                                            for(var i = 0; i< files.length; i++)
                                                $(".fileLink.ScanMaketCertificate",dContent).append($("<a target='_blank' href='/uploads/"+files[i]+"'>"+files[i]+"</a><br>"));

                                            var parent = $("input[name=ScanMaketCertificate]").parent();
                                            $("input[name=ScanMaketCertificate]").remove();
                                            parent.html('<input type="file" fType="file" style="margin-bottom:2px" class="certData" name="ScanMaketCertificate">')
                                        }
                                        if(isCreateMode) {
                                            //Добавляем новый ряд в таблицу
                                            $(".cert-supps-content", ".dialog-certificate-info").append($(answer.newRow));
                                            //закрываем диалог
                                            $(".dialog, .dialog-certsupplement-info").remove();
                                            //пишем сообщение
                                            useTopHoverTitle("Приложение успешно добавлено");
                                        }
                                        else
                                            $("#supp_"+id, ".dialog-certificate-info").html($(answer.newRow).html());
                                        useTopHoverTitle("Приложение успешно сохранено");

                                    }
                                }
                            });
                        };

                        //Обработка сохранения приложения свидетельства
                        $(".btSaveCertSupp", dContent).click(function(){
                            //Отправляем запрос на сервер
                            if(isCreateMode){
                                //Добавляем на основе какого дока добавляется приложение
                                var hContent = ''+
                                    '<div class="switch-dialog">'+
                                    '<div class="text-block">Выберите или добавьте приказ, на основании которого добавляется приложение свидетельства</div><br><br>'+
                                    '<div class="switch-option selected"><input checked type="radio" id="rbWithNumber" value="withNumber" name="methodSave" /><label for="rbWithNumber">Приказ есть в базе данных: </label><input placeholder="Номер приказа" class="orderNumber"/></div>'+
                                    '<div class="switch-option"><input type="radio" id="rbNewOrder" value="new" name="methodSave" /><label for="rbNewOrder">Добавить новый приказ</label></div>'+
                                    '<div class="btContainer"><div class="bt btOk">Ok</div><div class="bt btCancel">Отмена</div></div>'
                                '</div>';

                                var hContent = $(hContent);
                                hContent.dialog({
                                    title: "Приказ",
                                    width: 500,
                                    height: 300,
                                    modal: true,
                                    close: function() {
                                        $(".switch-dialog").remove();
                                    },
                                    open: function() {
                                        //Кнопка отмены
                                        $(".btCancel",hContent).click(function(){
                                            $(".switch-dialog").remove();
                                        });

                                        //Переключение опции
                                        $("input[type='radio']", hContent).click(function(){
                                            $(".switch-option",hContent).removeClass("selected");
                                            $("input.orderNumber", hContent).prop("disabled", true);

                                            var parentRow = $(this).parents(".switch-option").eq(0);
                                            parentRow.addClass("selected");
                                            $("input.orderNumber",parentRow).prop("disabled", false);
                                        });

                                        //Обработчик кнопки ОК
                                        $(".btOk").click(function(){
                                            //Если первый пункт, проверяем чтобы был введен номер
                                            if($("#rbWithNumber",hContent).is(":checked")){
                                                if($(".orderNumber",hContent).val()==""){
                                                    useTopHoverTitleError("Введите номер документа");
                                                    return;
                                                }
                                                //добавляем номер в запрос
                                                saveSuppFunc(dContent, isCreateMode, {orderNumber:$(".orderNumber",hContent).val()});
                                                $(".switch-dialog").remove();
                                            };

                                            if($("#rbNewOrder",hContent).is(":checked")){
                                                //Функция что выполнится после добавления нового дока
                                                window.functionAfterAddDocument = function(idDocument){
                                                    saveSuppFunc(dContent, isCreateMode, {orderId:idDocument});
                                                    $(".switch-dialog").remove();
                                                };


                                                var odContent = $(
                                                    '<div class="dialog-orderDecument-add">' +
                                                    "<div style='text-align: center;margin-top:50px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
                                                    '</div>');
                                                $(odContent).dialog({
                                                    title: "Распорядительный акт",
                                                    width: 640,
                                                    height: 430,
                                                    modal: true,
                                                    close: function() {
                                                        $(".dialog, .dialog-orderDecument-add").remove();
                                                    },
                                                    open: function() {
                                                        $.ajax({
                                                            type: 'POST',
                                                            dataType: "html",
                                                            url: "/ron/index/certificate-document-add",
                                                            success: function(answer) {
                                                                //console.log(answer);
                                                                $(".loading-img", odContent).fadeOut(500, function() {
                                                                    odContent.html("").append($(answer));
                                                                });
                                                            },
                                                            error:function(e){
                                                                console.log('ошибка при загрузке панели добавления распорядительного документа');
                                                                console.log(e);
                                                            }

                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                saveSuppFunc(dContent, isCreateMode);

                            }
                        });

                        //====== Обработка поиска
                        //добавляем в jquery регистронезависимый contains
                        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
                            return function( elem ) {
                                arg = arg.toLowerCase();
                                var strText = $(elem).text().toLowerCase();
                                var valText = $(elem).val().toLowerCase();
                                if(
                                    strText.toLowerCase().indexOf(arg) >=  0
                                    || valText.toLowerCase().indexOf(arg) >= 0
                                )
                                    return true;
                                else
                                    return false;
                            };
                        });
                        //Обработчик ввода текста в поиск
                        $("input.searchText", dContent).keyup(function(e) {
                            if(e.which == 13){
                                search();
                                return;
                            }
                            if($("input.searchText", dContent).val()==""){
                                search();
                                return;
                            }
                        });
                        //Функция поиска
                        function search() {
                            var searchVal = $("input.searchText", dContent).val();
                            if (searchVal == "") {
                                clearSearch();
                                return;
                            }
                            $(".content-block.accredited tbody.content tr", dContent).hide();
                            $(".content-block.accredited tbody.content td:contains("+searchVal+")",dContent).parents("tr").show();
                        }
                        //функция отмены обработки поиска
                        function clearSearch(){
                            $(".content-block.accredited tbody.content tr", dContent).show();
                        }

                        //====== Обработка проставления галочек на аккредитовано/не аккредитовано и сохранение
                        //Если галочку меняли, то меняем ее состояние, мол изменена
                        $(".content-block.accredited .intable tbody.content",dContent).delegate(".program-cb", "change", function(){
                            $(this).attr("state","changed");
                        });
                        //Обработка сохранения
                        $(".btSaveNoAccr", dContent).click(function(){
                            //проверяем былил и изменены программы
                            if($(".program-cb[state=changed]",dContent).length == 0){
                                useTopHoverTitleError("Нет измененных программ для сохранения")
                                return;
                            }
                            //получаем список данных для сохранения
                            var hasStopped = false;
                            var hasCancelled = false;

                            var data = {
                                isNotAccredited: {},
                                isStopped: {},
                                isCanceled: {}
                            };
                            $(".programIsNotAccredited[state=changed]",dContent).each(function(i,item){
                                var cb = $(item);
                                data.isNotAccredited[cb.attr("name")] = cb.is(":checked")?"1":"0";
                            });
                            $(".programIsSuspended[state=changed]",dContent).each(function(i,item){
                                var cb = $(item);
                                data.isStopped[cb.attr("name").split("_")[1]] = cb.is(":checked")?"1":"0";
                                if(cb.is(":checked"))
                                    hasStopped = true;
                            });
                            $(".programIsCanceled[state=changed]",dContent).each(function(i,item){
                                var cb = $(item);
                                data.isCanceled[cb.attr("name").split("_")[1]] = cb.is(":checked")?"1":"0";
                                if(cb.is(":checked"))
                                    hasCancelled = true;
                            });

                            //Запрашиваем как сохранить данные для отмененных, если есть такие
                            if(hasStopped || hasCancelled){
                                var hContent = ''+
                                    '<div class="switch-dialog">'+
                                    '<div class="text-block">Выберите или добавьте приказ, на основании которого было приостановлено действие свидетельства в отношении выбранных аккредитованных программ</div>'+
                                    '<div class="switch-option selected"><input checked type="radio" id="rbWithNumber" value="withNumber" name="methodSave" /><label for="rbWithNumber">Приказ есть в базе данных: </label><input placeholder="Номер приказа" class="orderNumber"/></div>'+
                                    '<div class="switch-option"><input type="radio" id="rbNewOrder" value="new" name="methodSave" /><label for="rbNewOrder">Добавить новый приказ</label></div>'+
                                    '<div class="btContainer"><div class="bt btOk">Ok</div><div class="bt btCancel">Отмена</div></div>'
                                '</div>';

                                var hContent = $(hContent);
                                hContent.dialog({
                                    title: "Приказ",
                                    width: 500,
                                    height: 300,
                                    modal: true,
                                    close: function() {
                                        $(".switch-dialog").remove();
                                    },
                                    open: function() {
                                        //Кнопка отмены
                                        $(".btCancel",hContent).click(function(){
                                            $(".switch-dialog").remove();
                                        });

                                        //Переключение опции
                                        $("input[type='radio']", hContent).click(function(){
                                            $(".switch-option",hContent).removeClass("selected");
                                            $("input.orderNumber", hContent).prop("disabled", true);

                                            var parentRow = $(this).parents(".switch-option").eq(0);
                                            parentRow.addClass("selected");
                                            $("input.orderNumber",parentRow).prop("disabled", false);
                                        });

                                        //Обработчик кнопки ОК
                                        $(".btOk").click(function(){
                                            //Если первый пункт, проверяем чтобы был введен номер
                                            if($("#rbWithNumber",hContent).is(":checked")){
                                                if($(".orderNumber",hContent).val()==""){
                                                    useTopHoverTitleError("Введите номер документа");
                                                    return;
                                                }
                                                //Отсылаем запрос на сервер
                                                data.number = $(".orderNumber",hContent).val();
                                                $.ajax({
                                                    type: 'POST',
                                                    dataType: "json",
                                                    data: data,
                                                    url: "/ron/index/certificate-ap-changes",
                                                    success: function(answer) {
                                                        //Проверяем если ошибка
                                                        if(answer.result && answer.result=="error"){
                                                            useTopHoverTitleError(answer.error);
                                                            return;
                                                        }
                                                        //Если все хорошо выводим алерт и меняем/добавляем ссылки
                                                        if(answer.result && answer.result=="success"){
                                                            //убираем статус , что изменено
                                                            $(".program-cb[state=changed]",dContent).attr("state","not-changed");
                                                            $(".switch-dialog").remove();
                                                            useTopHoverTitle("Изменения успешно сохранены");
                                                        }
                                                    }
                                                });
                                            };

                                            if($("#rbNewOrder",hContent).is(":checked")){
                                                //Функция что выполнится после добавления нового дока
                                                window.functionAfterAddDocument = function(idDocument){
                                                    data.idDoc = idDocument;
                                                    $.ajax({
                                                        type: 'POST',
                                                        dataType: "json",
                                                        data: data,
                                                        url: "/ron/index/certificate-ap-changes",
                                                        success: function(answer) {
                                                            //Проверяем если ошибка
                                                            if(answer.result && answer.result=="error"){
                                                                useTopHoverTitleError(answer.error);
                                                                return;
                                                            }
                                                            //Если все хорошо выводим алерт и меняем/добавляем ссылки
                                                            if(answer.result && answer.result=="success"){
                                                                //убираем статус , что изменено
                                                                $(".program-cb[state=changed]",dContent).attr("state","not-changed");
                                                                $(".switch-dialog").remove();
                                                                useTopHoverTitle("Изменения успешно сохранены");
                                                            }
                                                        }
                                                    });
                                                };


                                                var odContent = $(
                                                    '<div class="dialog-orderDecument-add">' +
                                                    "<div style='text-align: center;margin-top:50px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
                                                    '</div>');
                                                $(odContent).dialog({
                                                    title: "Распорядительный акт",
                                                    width: 640,
                                                    height: 430,
                                                    modal: true,
                                                    close: function() {
                                                        $(".dialog, .dialog-orderDecument-add").remove();
                                                    },
                                                    open: function() {
                                                        $.ajax({
                                                            type: 'POST',
                                                            dataType: "html",
                                                            url: "/ron/index/certificate-document-add",
                                                            success: function(answer) {
                                                                //console.log(answer);
                                                                $(".loading-img", odContent).fadeOut(500, function() {
                                                                    odContent.html("").append($(answer));
                                                                });
                                                            },
                                                            error:function(e){
                                                                console.log('ошибка при загрузке панели добавления распорядительного документа');
                                                                console.log(e);
                                                            }

                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                //Отправляем запрос на сервер
                                $.ajax({
                                    type: 'POST',
                                    dataType: "json",
                                    data: data,
                                    url: "/ron/index/certificate-ap-changes",
                                    success: function(answer) {
                                        //Проверяем если ошибка
                                        if(answer.result && answer.result=="error"){
                                            useTopHoverTitleError(answer.error);
                                            return;
                                        }
                                        //Если все хорошо выводим алерт и меняем/добавляем ссылки
                                        if(answer.result && answer.result=="success"){
                                            //убираем статус , что изменено
                                            $(".program-cb[state=changed]",dContent).attr("state","not-changed");
                                            useTopHoverTitle("Изменения успешно сохранены");
                                        }
                                    }
                                });
                            }
                        })

                        //Кнопка добавления программ
                        $(".btAddProgram", dContent).click(function(){
                            var suppId = $("#supplementId",dContent).val();
                            _addAccrProgramDialog(suppId);
                        });

                        //Обработка выделения
                        $(".content-block.accredited .intable tbody.content",dContent).delegate("tr", "click", function(){
                            $(".content-block.accredited .intable tbody.content tr",dContent).removeClass("selected");
                            $(this).addClass("selected");
                        });

                        //Кнопка изменения программ
                        $(".btEditProgram", dContent).click(function(){
                            var suppId = $("#supplementId",dContent).val();
                            var progId = $(".content-block.accredited .intable tbody.content tr.selected",dContent).attr("id").split("_")[1];
                            _addAccrProgramDialog(suppId,  progId);
                        });
                    });
                }
            });
        }
    });
}

/**
 * Обработчики фильтра и пагинации для /#orgs
 */
indexOrgsHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "Search" : $(".Search","#filter").val(),
        "Region": $(".Region","#filter").val()
    };
    //Ссылка на скрипт сервера
    serverLink = "/ron/index/orgs-page";
    //Количество колонок в таблице
    contentColumns = 4;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){
        //вешаем обработчик открытия подробной инфо о сертификатах
        $("tr",".ron-orgs").click(function(){
            var id = $(this).attr("id").split("_")[1];
            var title = $(this).attr("dialog-title");
            _orgDialog(id, title);
        });
    }
    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "Search" : $(".Search","#filter").val(),
            "Region": $(".Region","#filter").val()
        };
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаем обработчик открытия подробной инфо о сертификатах
    $("tr",".ron-orgs").click(function(){
        var id = $(this).attr("id").split("_")[1];
        var title = $(this).attr("dialog-title");
        _orgDialog(id, title);
    });

    //Вешаем обработчик на нажатие Enter на поиск
    $(".Search","#filter").keypress(function(e){
        if(e.keyCode==13){
            $(".btApply","#filter").click();
        }
    });
}

/**
 * Метод отрабатывает диалог с подробной информацие об организации
 * @param id
 * @param title
 * @private
 */
_orgDialog =function(id, title){
    var dContent = $(
        '<div class="dialog-org-info">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: title,
        width: 1100,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-org-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по сертификату
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/index/org-info/"+id,
                success: function(answer) {
                    
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                         //sagaida: обработчик на кнопу добавления лицензии
                        $(".button_add_license", dContent).on("click", function(){
                            _open_dialog_add_license(id);
                        })
                        $(".button_add_application", dContent).on("click", function(){
                            _open_dialog_add_application_license(id);
                        })
                        
                        //Вешаем обработчик на переключение вкладок
                        $(".tab", dContent).click(function(){
                            $(".tab", dContent).removeClass("active");
                            $(this).addClass("active");

                            $(".content-block",dContent).hide();
                            $(".content-block."+$(this).attr("tab-container"),dContent).show();

                            $(".buttons-block",dContent).hide();
                            $(".buttons-block."+$(this).attr("tab-container"),dContent).show();
                        });
                        //Вешаем обработчик на смену селекта с выбранным сертификатом
                        $(".CertificatesSelect", dContent).change(function(){
                            var idCert = $(".CertificatesSelect", dContent).val();
                            //Чистим контент, рисуем крутилку
                            $(".certificateInfo",dContent).html("").append(
                                $("<tr><td colspan='"+contentColumns+"'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
                            ).fadeIn(500);;
                            //Отсылаем запрос на отрисовку контента по другому сертификату
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/index/org-certificate-info/"+idCert,
                                success: function(answer) {
                                    $(".loading-img", dContent).fadeOut(500, function() {
                                        $(".certificateInfo",dContent).html("").append($(answer).find(".content > tr"));
                                    });
                                }
                            });
                        });
                        
                        //Вешаем обработчик на смену селекта с выбранной лицензией
                        $(".licensesSelect", dContent).change(function(){
                            var idCert = $(".licensesSelect", dContent).val();
                            //Чистим контент, рисуем крутилку
                            $(".licenseInfo",dContent).html("").append(
                                $("<tr><td colspan='"+contentColumns+"'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
                            ).fadeIn(500);;
                            //Отсылаем запрос на отрисовку контента по другому сертификату
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/index/org-license-info/"+idCert,
                                success: function(answer) {
                                    $(".loading-img", dContent).fadeOut(500, function() {
                                        $(".licenseInfo",dContent).html("").append($(answer).find(".content > tr"));
                                    });
                                  
                                    $(".button_add_application",dContent).on("click", function(){
                                        _open_dialog_add_application_license(id);
                                    })
                                }
                            });
                        });

                        //Вешаем обработчик на смену селекта с филиалами
                        $("#affilates", dContent).change(function(){
                            var idAff = $("#affilates", dContent).val();
                            //Чистим контент, рисуем крутилку
                            $(".content-block.filials",dContent).find(".viewzone").html("").append(
                                $("<div style='text-align: center'><div class='loading-img'><img src='/img/loading.gif' /></div></div>")
                            ).fadeIn(500);;
                            //Отсылаем запрос на отрисовку контента по другому сертификату
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/index/org-affilate/"+idAff,
                                success: function(answer) {
                                    $(".loading-img", dContent).fadeOut(500, function() {
                                        $(".content-block.filials",dContent).find(".viewzone").html("").append($(answer));
                                        //Вешаем обработчики на поля даты для публичных аккредитаций
                                        var parBlock = $(".content-block.filials",dContent).find(".viewzone");
                                        $(".date-field", parBlockt).datepicker({
                                            changeMonth: true,
                                            changeYear: true,
                                            dateFormat: "dd.mm.yy"
                                        });
                                        $(".date-icon", parBlock).click(function(){
                                            $(this).parent().find("input").focus()
                                        })
                                    });
                                }
                            });
                        });

                        //Вешаем обработчик на удаление созданного учредителя
                        $(dContent).delegate(".founders-list .deleteFounder.added", "click", function(){
                            $(this).parent().remove();
                        });
                        //Вешаем обработчик на удаление существующих учредителей
                        $(dContent).delegate(".founders-list .deleteFounder.current", "click", function(){
                            var parent = $(this).parent();
                            parent.find(".orgInfo").attr("name","founders.removed[]");
                            parent.removeClass("current").addClass("removed");
                            $(this).removeClass("current").addClass("removed");
                        });
                        //Вешаем обработчик на возврат помеченных на удаление учредителей
                        $(dContent).delegate(".founders-list .deleteFounder.removed", "click", function(){
                            var parent = $(this).parent();
                            parent.find(".orgInfo").attr("name","founders.current[]");
                            parent.removeClass("removed").addClass("current");
                            $(this).removeClass("removed").addClass("current");
                        });

                        //Вешаем обработчик на добавление учредителя
                        $(dContent).delegate(".add-founder", "click", function(){
                            //Показываем диалог
                            var fContent = ""+
                                '<div class="founders-dialog">'+
                                    '<div class="search-block">' +
                                        '<input class="search" placeholder="Поиск">'+
                                    '</div>'+
                                    '<div class="founders-list">'+
                                        "<div style='text-align: center;margin-top:20px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
                                    '</div>'+
                                    '<div class="buttons-block">'+
                                        '<div class="bt btOk">ОК</div>'+
                                        '<div class="bt btCancel">Отмена</div>'+
                                    '</div>'+
                                '</div>';
                            fContent = $(fContent);
                            fContent.dialog({
                                title: "Добавление учредителя",
                                width: 400,
                                height: 400,
                                modal: true,
                                close: function() {
                                    $(".founders-dialog").remove();
                                },
                                open: function(){
                                    $.ajax({
                                        type: 'POST',
                                        dataType: "html",
                                        url: "/ron/index/get-founders",
                                        success: function (answer) {
                                            $(".loading-img", fContent).fadeOut(500, function() {
                                                $(".founders-list",fContent).html("").append($(".founder-row",answer));
                                                //=============== Обработка клика по пункту
                                                $(".founder-row input[type=checkbox]", fContent).change(function(){
                                                    var parent = $(this).parent();
                                                    if(parent.hasClass("active"))
                                                        parent.removeClass("active");
                                                    else
                                                        parent.addClass("active");
                                                });
                                            });
                                            //=============== Обработка кнопки Отмена
                                            $(".btCancel", fContent).click(function(){
                                                $(".founders-dialog").remove();
                                            });
                                            //=============== Обработка поиска
                                            //добавляем в jquery регистронезависимый contains
                                            $.expr[":"].contains = $.expr.createPseudo(function(arg) {
                                                return function( elem ) {
                                                    arg = arg.toLowerCase();
                                                    var strText = $(elem).text().toLowerCase();
                                                    var valText = $(elem).val().toLowerCase();
                                                    if(
                                                        strText.toLowerCase().indexOf(arg) >=  0
                                                        || valText.toLowerCase().indexOf(arg) >= 0
                                                    )
                                                        return true;
                                                    else
                                                        return false;
                                                };
                                            });
                                            //Обработчик ввода текста в поиск
                                            $("input.search", fContent).keyup(function(e) {
                                                if(e.which == 27){
                                                    clearTimeout($.data(this, 'timer'));
                                                    clearSearch();
                                                    return;
                                                }
                                                clearTimeout($.data(this, 'timer'));
                                                var wait = setTimeout(search, 500);
                                                $(this).data('timer', wait);
                                            });
                                            //Функция поиска
                                            function search(){
                                                var searchVal = $("input.search", fContent).val();
                                                $(".founder-row", fContent).hide();
                                                $(".founder-row > label:contains("+searchVal+")",fContent).parents(".founder-row").show();
                                            }
                                            //функция отмены обработки поиска
                                            function clearSearch(){
                                                $("input.search", fContent).val("");
                                                $(".founder-row", fContent).show();
                                            }
                                            //обработчик кнопки ОК
                                            $(".btOk", fContent).click(function(){
                                                //собираем данные
                                                var founders = [];
                                                $(".founder-row.active", fContent).each(function(i, item){
                                                    //id
                                                    var id = $(item).find("input").attr("id").split("_")[1];
                                                    var text = $(item).find("label").text();
                                                    founders[id] = text;
                                                });
                                                //Дорисовываем строки в список учредителей
                                                var foundersList = $(".founders-list:visible",dContent);
                                                for(var id in founders){
                                                    var fBlock = '<div class="founderBlock added" id="'+id+'">' +
                                                        '<input class="orgInfo" type="hidden" name="founders.added[]" value="'+id+'">'+
                                                        founders[id] +
                                                        '<span class="deleteFounder added ui-icon ui-icon-minusthick"></span>'+
                                                    '</div>';
                                                    foundersList.append(fBlock);
                                                }
                                                //======
                                                $(".founders-dialog").remove();
                                            });
                                        }
                                    })
                                }
                            })
                        });

                        //Вешаем обработчики на поля даты для публичных аккредитаций
                        $(".date-field", dContent).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd.mm.yy"
                        });
                        $(".date-icon", dContent).click(function(){
                            $(this).parent().find("input").focus()
                        })

                        //Вешаем обработчик добавления нового блока публичной аккредитации
                        $(dContent).delegate(".pubaccr-block span", "click", function(){
                            console.log( 'обработчик добавления нового блока публичной аккредитации 3' );
                            var dataType = $(this).attr("data-type");
                            console.log(dataType );
                            var tbodyClass = "";
                            switch(dataType){
                                case "addPubAccr":
                                    tbodyClass = "PubAccr";
                                    break;
                                case "addProfPubAccr":
                                    tbodyClass = "ProfPubAccr";
                                    break;
                                case "addIntPubAccr":
                                    tbodyClass = "IntPubAccr";
                                    break;
                            }
                            //Получаем набор блоков нужного нам класса
                            var pBlock = $(this).parents(".fields-table").eq(0);
                            var tbodyBlocks = $("."+tbodyClass, pBlock);
                            //клонируем первый
                            var clonedBlock = tbodyBlocks.first().clone();
                            clonedBlock.removeClass("firstTbody");
                            $("input, textarea",clonedBlock).val("");
                            //добавляем клонируемый блок в список
                            tbodyBlocks.last().after(clonedBlock);
                            //вешаем обработчик для выбора даты

                            $(".date-field", clonedBlock).removeClass("hasDatepicker");
                            $(".date-field", clonedBlock).removeAttr("id");
                            $(".date-field", clonedBlock).datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd.mm.yy"
                            });
                            $(".date-icon", clonedBlock).click(function(){
                                $(this).parent().find("input").focus();
                            })
                            //ставим фокус на новый блок
                            $("textarea",clonedBlock).focus();
                        });

                        //Вешаем обработчик на кнопку сохранения организации
                        $(".buttons-container .btSave", dContent).click(function(){
                            var button = $(this);
                            //Если идет отправка, не отрабатываем
                            if(button.prop("disabled"))
                                return;

                            //Чистим пустые блоки аккредитаций
                            removeEmptyBlocks("PubAccr");
                            removeEmptyBlocks("ProfPubAccr");
                            removeEmptyBlocks("IntPubAccr");

                            //Собираем данные с полей
                            var fieldsData = {};
                            $(".orgInfo", $(".fields-table:visible", dContent)).each(function(i, item){
                                var name = $(item).attr("name");
                                if(name.split(".").length>1){
                                    var parts = name.split(".");
                                    var level = fieldsData;
                                    for(var i = 0; i < parts.length-1; i++){
                                        part = parts[i];
                                        if(!level[part])
                                            level[part] = {};
                                        level = level[part];
                                    }
                                    var lastPart = parts[parts.length-1];
                                    if(lastPart.length>2 && lastPart.substring(lastPart.length-2,lastPart.length) == "[]"){
                                        lastPart = lastPart.substring(0,lastPart.length-2);;
                                        if(!level[lastPart])
                                            level[lastPart] = []
                                        level[lastPart].push($(item).val());
                                    } else {
                                        level[lastPart] = $(item).val();
                                    }
                                } else {
                                    fieldsData[name] = $(item).val();
                                }
                            });

                            //fieldsData["orgData"] = {};
                            fieldsData["orgData"]["PubAccr"] = getAccrData("PubAccr");
                            fieldsData["orgData"]["ProfPubAccr"] = getAccrData("ProfPubAccr");
                            fieldsData["orgData"]["IntPubAccr"] = getAccrData("IntPubAccr");

                            //Чтобы не было повторной отправки
                            button.prop("disabled",true);
                            button.addClass("disabled");

                            var sendData = {
                                fieldsData : fieldsData
                            };
                            console.log(sendData);


                            $.post(
                                "/ron/index/save-org-info",
                                sendData,
                                function(data){
                                    button.prop("disabled",false);
                                    button.removeClass("disabled");
                                    //Отрабатываем добавление и удаление учредителей
                                    $(".founderBlock.removed", $(".fields-table:visible", dContent)).remove();
                                    $(".founderBlock.added .deleteFounder.added", $(".fields-table:visible", dContent)).removeClass("added").addClass("current");
                                    $(".founderBlock.added", $(".fields-table:visible", dContent)).removeClass("added").addClass("current");
                                    if (data==='done'){
                                        //небольшая правка, если окно было открыто через заявку, то после правок заявку нужно перегрузить
                                        if($('div.viewzone table tr').is('.active_appl')){
                                                openEdit();
                                        }
                                        useTopHoverTitle("Данные записаны");
                                    }else{
                                        useTopHoverTitleError("Возникла ошибка при сохранении");
                                        console.log("Возникла ошибка при сохранении");
                                        console.log(data);
                                    }
                                }
                            )
                        });

                        //функция, что удаляет пустые блоки для ввода данных
                        //по общественным аккредитациям
                        var removeEmptyBlocks = function(className){
                            //Получаем набор блоков нужного нам класса
                            var tbodyBlocks = $("."+className,dContent);
                            //Проходим по всем найденным блокам
                            tbodyBlocks.each(function(i,item){
                                //Проверяем если это первый блок
                                //то пропускаем
                                if($(item).hasClass("firstTbody"))
                                    return true;
                                //Проверяем, если значения полей пустые
                                if( $("textarea",item).val() == "" && $("input",item).val()=="")
                                    $(item).remove();
                            })
                        }

                        //Функция проходит по всем блокам и собирает массив данных
                        //для возврата
                        var getAccrData = function(className){
                            var returnArray = [];
                            //Получаем набор блоков нужного нам класса
                            var tbodyBlocks = $("."+className, $(".fields-table:visible", dContent));
                            //Проходим по всем найденным блокам
                            tbodyBlocks.each(function(i,item){
                                returnArray.push({
                                    organization: $.trim($("textarea",item).val()),
                                    date: $.trim($("input.date-field",item).val()),
                                    requisite: $.trim($("input.requisite-field",item).val())
                                })
                            });
                            return returnArray;
                        }
                    });
                    
                }
            });
        }
    });
}

_get_license_info = function(dContent, org_id){
    var idCert = $(".licensesSelect", dContent).val();
    //Чистим контент, рисуем крутилку
    $(".licenseInfo",dContent).html("").append(
        $("<tr><td colspan=''><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
    ).fadeIn(500);;
    //Отсылаем запрос на отрисовку контента по другому сертификату
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/ron/index/org-license-info/"+idCert,
        success: function(answer) {
            $(".loading-img", dContent).fadeOut(500, function() {
                $(".licenseInfo",dContent).html("").append($(answer).find(".content > tr"));
            });
             $(".button_add_application").on("click", function(){
                _open_dialog_add_application_license(org_id);
            })
        }
    });
}

/**
 * Метод открывает popup добавления лицензии организации
 * @param {String} $id ид организации
 */

_open_dialog_add_license = function(id){
    $.ajax({
        type: "POST",
        url: "/ron/index/add-licence-dialog",
        data: ({id: id}),
        success: function(data){
            $('.dialog').remove();
            $("<div class='dialog'></div>").html(data)
                .dialog({
                    autoOpen:false,
                    title: "Добавление лицензии",
                    modal:true,
                    width: 700
            }).dialog("open");
            
            //пропишем правила поведения полей с датами
            var form = "#form_add_license";
            $("#txtFromDate", form).datepicker({
                maxDate: 0,
                changeMonth: true,
                changeYear: true,
                onSelect: function(selected) {
                    $("#txtToDate", form).datepicker("option","minDate", selected)
                }
            });
            $("#txtToDate", form).datepicker({
                changeMonth: true,
                changeYear: true,
                onSelect: function(selected) {
                   $("#txtFromDate", form).datepicker("option","maxDate", selected)
                }
            }); 

            $("#isTermless", form).on("change", function(){
                var prop = $(this).prop("checked");
                if(prop){
                   $("#txtToDate", form).prop("disabled", true).val(""); 
                }else{
                    $("#txtToDate", form).prop("disabled", false);
                }
              })
          
            //обработчик кнопки Сохранить
            $(".btSave", form).on("click", function(){_save_license(id, form)})
            $(form).on("submit", function(){_save_license(id, form);return false;})
        }
    });
}

/**
 * открытие диалогового окна добавления приложения к лицензии
 * @param Integer id - ид организации
 */
_open_dialog_add_application_license = function(id){
    console.log(id)
    $.ajax({
        type: "POST",
        url: "/ron/index/dialog-add-application-license",
        data: ({id: id}),
        success: function(data){
            $('.dialog-add-application-license').remove();
            $("<div class='dialog-add-application-license'></div>").html(data)
                .dialog({
                    autoOpen:false,
                    title: "Добавление приложения к лицензии",
					height = Math.round(screen.height * 0.87);
                    modal:true,
                    top: 5,
                    width: 900
                }).dialog("open");
            _get_edu_programs_by_level_id();
            $("#list_level_education", '.dialog-add-application-license').on("change", function(){_get_edu_programs_by_level_id()});
            $(".bt_add_program", '.dialog-add-application-license').on("click", function(){_add_program_in_view_table()})
            $(".save_application", '.dialog-add-application-license' ).on("click", function(){_save_application_form(id, "#form_add_application_license")})
        }
    });
}


/**
 * диалогое окно добавления приложения к лицензии: метод добавляет программу в отображающуюся таблицу
 */
_add_program_in_view_table = function(is_save){
    var is_save = is_save || false;
    var sel_edu_level = $("#list_level_education").val(), 
        program = $('#list_programs').prop("disabled"),
        edu_level_name = $("option[value = '"+sel_edu_level+"']", "#list_level_education").text(),
        td_for_item = '<tr>',
        program_id = ""
    ;

   td_for_item += "<td class='width355px'>" + edu_level_name + "</td>";
    
    if(!program){
        program_id = $("#list_programs").val();
        var code_ = $("option[value='" + program_id + "']", "#list_programs").attr("code"),
            program_name = $("option[value='" + program_id + "']", "#list_programs").attr("pr-name");
        code_ = (code_ == 'NULL') ? "" : code_;
        td_for_item += "<td class='width158px'>" + code_ + "</td>";
        td_for_item += "<td class='width355px'>" + program_name + "</td>";
    }else{
       td_for_item += "<td></td><td class='width355px'></td>"; 
    }
    td_for_item += '</tr>';
    var is_element_in_table = $("." + sel_edu_level + "_" + program_id).length;
    if(is_element_in_table){
        if(!is_save){
            useTopHoverTitleError("Данная образовательная программа уже добавлена в данную лицензию");
            return false;
        }
    }else{
        $("tbody",'.program-list-table').append(td_for_item); 
        $("#form_add_application_license").append("<input type='hidden' name='programs[]' class='" + sel_edu_level + "_" + program_id + "' value='" + sel_edu_level + "_" + program_id + "' />");
        $("option:first","#list_level_education").prop("selected", true);
        _get_edu_programs_by_level_id();
    }
    
   
}


/**
 * получение образовательных программ 
 * @param {Integer} id ид образовательного уровня
 */
_get_edu_programs_by_level_id = function(){
    var edu_educations_val =  $("#list_level_education").val(), 
        is_program = $("option[value = '"+edu_educations_val+"']", "#list_level_education").hasClass("have_program");

    $("#list_programs").empty()
    
    if(is_program){
        $('#list_programs').prop("disabled", false);
        $.ajax({
            type: "POST",
            url: "/ron/index/get-programs-by-education",
            dataType: "json",
            data: ({id: edu_educations_val}),
            success: function(data){
                if(data.mess == "empty"){
                    $('#list_programs').prop("disabled", true);
                }else{
                    for(var key in data){
                        var view_option = (data[key]['code'] != "") ? data[key]['code'] + ' - ' + data[key]['name'] : data[key]['name'];
                        var option = $('<option></option>')
                                        .html(view_option)
                                        .attr("code", data[key]['code'])
                                        .attr("pr-name", data[key]['name'])
                                        .val(key);
                        $('#list_programs').append(option);
                    }
                }
            }
        });
    }else{
        $('#list_programs').prop("disabled", true);
    }
}

/**
 * метод сохранения формы приложения к лицензии
 * @param {Integer} id ид организации
 * @param {String} form идентификатор формы
 */
_save_application_form = function(id, form){
    
    //проверим на валидность
    var valid_form = _valid_form_application_license(form);
    if(!valid_form){
        $('input', form).on("change", function(){_valid_form_application_license(form)})
        return false;
    }
    
    //сохраним в системе данные формы
    var license_id = $(".licensesSelect", '.dialog-org-info').val();
    var data = $(form).serialize();
    data += "&id_org=" + id + "&license_id="+license_id;
   // console.log(data);return;
    $.ajax({
        type: "POST",
        url: "/ron/index/save-application-license",
        data: data,
        success: function(data){
            _get_license_info('.dialog-org-info', id);
            $(".dialog-add-application-license").dialog("close");
        }
    });
}

_valid_form_application_license = function(form){
    var w_valid = true;
    var require = {
        'Number'         : $('input[name="Number"]', form).val(),
        'SerDoc'         : $('input[name="SerDoc"]', form).val(),
        'NumDoc'         : $('input[name="NumDoc"]', form).val(),
    };
    
    //удалим ошибки, если имелись
    $('input, select', form).removeClass('color_pink');
    
    //пробежимся по обяз элементам и выставим ошибки
    for(var name in require){
        require[name] = $.trim(require[name]);
        if(require[name] == ""){
            $('input[name="'+name+'"]', form).addClass('color_pink');
            w_valid = false;
        }
    }
    
    var count_programs = $("input[name='programs[]']", form).length;
    if(count_programs == 0){
       $('#list_level_education', form).addClass('color_pink'); 
       $('#list_level_education', form).addClass('color_pink'); 
       w_valid = false;
    }
    
    return w_valid;    
}


/**
 * Метод сохранения лицензии
 * @param Integer Id ид организации
 */
_save_license = function(id, form){
    //-------проверка на валидность
    var w_valid = _valid_save_license(form);
    if(!w_valid){
        $('input, select', form).change(function(){_valid_save_license(form)}).blur(function(){_valid_save_license(form)});
        return false;
    }
    
    //--------сохранение данных
    var data = $(form).serialize();
    $.ajax({
        type: "POST",
        url: "/ron/index/save-license-data/"+id,
        data: data,
        success: function(data){
            $(".licensesSelect").append("<option value='" + data + "'>Лицензия № "+$('input[name="LicenseRegNum"]', form).val()+" от "+$("#txtFromDate", form).val() +"</option>")
            $(".dialog").dialog("close");
            $(".licensesSelect").val(data)
            _get_license_info('.dialog-org-info', id);
            //_orgDialog(id);
            
        }
    });
}

/**
 * проверка формы сохранения лицензии
 */
_valid_save_license = function(form){
    var w_valid = true;
    var require = {
        'LicenseRegNum'         : $('input[name="LicenseRegNum"]', form).val(),
        'SerDoc'                : $('input[name="SerDoc"]', form).val(),
        'NumDoc'                : $('input[name="NumDoc"]', form).val(),
        'DateReg'               : $('input[name="DateReg"]', form).val(),
        'fk_eiisLicenseState'   : $('select[name="fk_eiisLicenseState"]', form).val()
    };
    //если не установлена галка на Бессрочная - добавим ещё одно поле в обязательные
    var prop_termLess = $("#isTermless", form).prop("checked");
    if(!prop_termLess){
        require.DateEnd = $('input[name="DateEnd"]', form).val()
    }
    
    //удалим ошибки, если имелись
    $('input, select', form).removeClass('color_pink');
    
    //пробежимся по обяз элементам и выставим ошибки
    for(var name in require){
        require[name] = $.trim(require[name]);
        if(require[name] == ""){
            switch(name){
                case 'fk_eiisLicenseState':{
                   $('select[name="'+name+'"]', form).addClass('color_pink');  break;   
                }
                default: {
                    $('input[name="'+name+'"]', form).addClass('color_pink');
                }
            }
            w_valid = false;
        }
    }
    
    return w_valid;
}




/**
 * Обработчики фильтра и пагинации для /#programs
 */
indexProgramsHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "ugsSelect" : $(".ugsSelect","#filter").val(),
        "eduLevelSelect": $(".eduLevelSelect","#filter").val(),
        "Search" : $(".Search","#filter").val()
    };
    //Ссылка на скрипт сервера
    serverLink = "/ron/index/programs-page";
    //Количество колонок в таблице
    contentColumns = 7;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){};

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "ugsSelect" : $(".ugsSelect","#filter").val(),
            "eduLevelSelect": $(".eduLevelSelect","#filter").val(),
            "Search" : $(".Search","#filter").val()
        };
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //Вешаем обработчик на нажатие Enter на поиск
    $(".Search","#filter").keypress(function(e){
        if(e.keyCode==13){
            $(".btApply","#filter").click();
        }
    });
}

/**
 * Обработчики фильтра и пагинации для /#orders
 */
indexOrdersHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "Type" : $(".Type","#filter").val(),
        "Kind" : $(".Kind","#filter").val()
    };
    if($(".dtDateStart","#filter").val() != "")
        filter["DateStart"] = moment($(".dtDateStart","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
    if($(".dtDateEnd","#filter").val() != "")
        filter["DateEnd"] = moment($(".dtDateEnd","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");

    //Ссылка на скрипт сервера
    serverLink = "/ron/index/orders-page";
    //Количество колонок в таблице
    contentColumns = 5;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){

    }

    //Вешаем обработчики на календари
    $(".date-field", "#filter").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy"
    });
    $(".date-icon", "#filter").click(function(){
        $(this).parent().find("input").focus()
    })

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "Type" : $(".Type","#filter").val(),
            "Kind" : $(".Kind","#filter").val()
        };
        if($(".dtDateStart","#filter").val() != "")
            filter["DateStart"] = moment($(".dtDateStart","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        if($(".dtDateEnd","#filter").val() != "")
            filter["DateEnd"] = moment($(".dtDateEnd","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаемо обработчик на смену типа
    $(".Type","#filter").change(function(){
        if($(this).val()!=""){
            $(".Kind option","#filter").hide();
            $(".Kind option[typeid='"+$(this).val()+"']","#filter").show();
            $(".Kind option[value='']","#filter").show();
            $(".Kind","#filter").val('');
        } else {
            $(".Kind option","#filter").show();
        }
    });
}

/**
 * Обработчики фильтра и пагинации для /#user-requests
 */
indexUserrequestsHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "Status" : $(".stateSelect","#filter").val(),
        "Search" : $(".Search","#filter").val()
    };

    //Ссылка на скрипт сервера
    serverLink = "/ron/index/user-requests-page";
    //Количество колонок в таблице
    contentColumns = 7;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){
        //вешаем обработчик открытия подробной инфо о сертификатах
        $("tr",".ron-userrequests").click(function(){
            $("tr.current",".ron-userrequests").removeClass("current");
            $(this).addClass("current");

            var id = $(this).attr("id").split("_")[1];
            var title = $(this).attr("dialog-title");
            _userregDialog(id, title);
        });
    }

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "Status" : $(".stateSelect","#filter").val(),
            "Search" : $(".Search","#filter").val()
        };
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаем обработчик открытия подробной инфо о сертификатах
    $("tr",".ron-userrequests").click(function(){
        $("tr.current",".ron-userrequests").removeClass("current");
        $(this).addClass("current");

        var id = $(this).attr("id").split("_")[1];
        var title = $(this).attr("dialog-title");
        _userregDialog(id, title);
    });
}

/**
 * Метод отрабатывает диалог с подробной информацие об заявке на регистрацию
 * @param id
 * @param title
 * @private
 */
_userregDialog =function(id, title){
    var dContent = $(
        '<div class="dialog-userreg-info">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: title,
        width: 1100,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-userreg-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по сертификату
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/index/user-reg-info/"+id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));

                        //Вешаем обработчик на переключение вкладок
                        $(".tab", dContent).click(function(){
                            if($(this).hasClass("disabled"))
                                return;

                            $(".tab", dContent).removeClass("active");
                            $(this).addClass("active");

                            $(".content-block",dContent).hide();
                            $(".content-block."+$(this).attr("tab-container"),dContent).show();

                            $(".buttons-block",dContent).hide();
                            $(".buttons-block."+$(this).attr("tab-container"),dContent).show();
                        });

                        //Обработка кнопок закрыть
                        $(".btClose", dContent).click(function(){
                            $(".dialog, .dialog-userreg-info").remove();
                        });

                        //Обработка кнопки Далее
                        $(".btNext", dContent).click(function(){
                            if(!$(".tab[tab-container=confirmation]", dContent).hasClass("disabled"))
                                $(".tab[tab-container=confirmation]", dContent).click();
                            else
                                $(".tab[tab-container=binding]", dContent).click();
                        });
                        $(".btNext2", dContent).click(function(){
                            if(!$(".tab[tab-container=confirmation]", dContent).hasClass("disabled"))
                                $(".tab[tab-container=confirmation]", dContent).click();
                        });

                        //Вешаем обработчик на переключение способа привязки
                        $(".binding input[type=radio]",dContent).change(function(){
                            $(".binding .cbSelected",dContent).removeClass("cbSelected");
                            $(this).parents("tr").eq(0).addClass("cbSelected");
                        });

                        //Обработка выбора результата поиска
                        $(".search-results", dContent).delegate(".result", "click", function(){
                            $(".search-results .result", dContent).removeClass("selected");
                            $(this).addClass("selected");
                        });

                        //============ Вешаем обработку поиска
                        // Функция поиска
                        var searchFunc = function(){
                            //Если на выделена строка с вариантом по поиску, то ничего не обрабатываем
                            if(!$("#cbBySearch").is(":checked"))
                                return;
                            //
                            var searchText = $.trim($(".tbBindingSearch",dContent).val());
                            //Проверяем если длина поиска меньше 3х cимволов
                            if(searchText.length < 3){
                                useTopHoverTitleError("Длина поискового запроса не может быть меньше 3 символов");
                                return;
                            }
                            //Отсылаем запрос на поиск
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/index/user-reg-search/" + searchText,
                                success: function (answer) {
                                    $(".search-results", dContent).html(answer);
                                }
                            });
                        };
                        // Функция очистки
                        var clearFunc = function(){
                            $(".binding .tbBindingSearch",dContent).val("");
                            $(".search-results", dContent).html('<div style="    color: #999999;padding: 5px;font-size: 16px;">Результаты поиска</div>');
                        };

                        //Обработчик нажатия на кнопку Поиск
                        $(".binding .btSearch",dContent).click(function(){
                            searchFunc();
                        });
                        //Обрвботчик нажатия на кнопку Очистить
                        $(".binding .btSearchCancel",dContent).click(function(){
                            clearFunc();
                        });
                        //Обработка нажатий enter и esc
                        $(".binding .tbBindingSearch",dContent).keypress(function(e){
                            if(e.keyCode==13){
                                searchFunc();
                            }
                        });

                        //============ Обработка кнопки Привязать
                        $(".btBind",dContent).click(function(){
                            var self = this;
                            if($(this).prop("disabled"))
                                return;
                            //Проверяем, если второй вариант и ничего не выбрано
                            if($("#cbBySearch").is(":checked") && $(".search-results .result.selected",dContent).length == 0){
                                useTopHoverTitleError("Не выброна организации для привязки");
                                return;
                            }

                            var regId = $("#userRegId", dContent).val();
                            var orgId = "";
                            if($("#cbBySearch").is(":checked")){
                                orgId = $(".search-results .result.selected",dContent).attr("id").split("_")[1];
                            }
                            if($("#cbByInn").is(":checked")){
                                orgId = $("#innOrgId",dContent).val();
                            }
                            //Отправляем запрос на привязку на сервер
                            $(this).prop("disabled",true);
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                url: "/ron/index/user-reg-bind/" + regId+"_"+orgId,
                                success: function (answer) {
                                    $(self).prop("disabled",false);
                                    if(!answer || !answer.result || answer.result != "success"){
                                        useTopHoverTitleError("При выполнении запроса произошла ошибка. Свяжитесь с администратором.");
                                        return;
                                    }
                                    //Меняем статус у выделенного ряда в таблице
                                    $(".current .ur_status",".ron-userrequests").text("Связана с организацией");
                                    //Меняем статус на первой вкладке
                                    $("#userReg_Status",dContent).text("Связана с организацией");
                                    //Задаем имя связанной организации на первой вкладке
                                    $("#userReg_LinkedOrg",dContent).text(answer.orgName);
                                    //Показываем кнопку далее2
                                    $(".btNext2",dContent).show();
                                    //Устанавливаем логин
                                    $("#userReg_Login",dContent).val(answer.login);
                                    //делаем доступной следующую вкладку
                                    $(".tab[tab-container=confirmation]",dContent).removeClass("disabled");
                                    //Переходим на след вкладку
                                    $(".tab[tab-container=confirmation]",dContent).click();
                                }
                            });
                        });

                        //============ Обработка кнопки Создать пользователя
                        $(".btCreateUser",dContent).click(function(){
                            var self = this;
                            if($(this).prop("disabled"))
                                return;
                            var login = $.trim($("#userReg_Login", dContent).val());
                            //Проверяем если логин введен
                            if(login == ""){
                                useTopHoverTitleError("Поле с логином должно быть заполнено");
                                return;
                            }
                            var regId = $("#userRegId", dContent).val();
                            //Отправляем запрос
                            $(this).prop("disabled",true);
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                url: "/ron/index/user-reg-create-user/" + regId+"*"+login,
                                success: function (answer) {
                                    $(self).prop("disabled",false);
                                    if(!answer || !answer.result || answer.result != "success"){
                                        if(answer && answer["error_type"] && answer["error_type"]=="user_exists"){
                                            useTopHoverTitleError("Пользователь с таким логином уже есть в базе данных");
                                        } else
                                            useTopHoverTitleError("При выполнении запроса произошла ошибка. Свяжитесь с администратором.");
                                        return;
                                    }
                                    //Меняем статус у выделенного ряда в таблице
                                    $(".current .ur_status",".ron-userrequests").text("Обработана");
                                    //Закрываем диалог
                                    $(".dialog, .dialog-userreg-info").remove();
                                    //выводим сообщение, что все хорошо
                                    $( "<div class='dialog-message'>Пользователь был успешно создан.</div>" ).dialog({
                                        resizable: false,
                                        title: "Пользователь создан",
                                        height:180,
                                        modal: true,
                                        close: function(){
                                            $(".dialog, .dialog-message").remove();
                                        },
                                        buttons: {
                                            "Ok": function() {
                                                $(".dialog, .dialog-message").remove();
                                            }
                                        }
                                    });
                                }
                            });
                        });
                    });
                }
            });
        }
    });
}

/**
 * Обработчики фильтра и пагинации для /#errors
 */
indexErrorsHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "Status" : $(".stateSelect","#filter").val(),
        "Search" : $(".Search","#filter").val()
    };

    //Ссылка на скрипт сервера
    serverLink = "/ron/index/errors-page";
    //Количество колонок в таблице
    contentColumns = 5;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){
        //вешаем обработчик открытия подробной инфо о баг репортах
        $("tr",".ron-errors").click(function(){
            $("tr.current",".ron-errors").removeClass("current");
            $(this).addClass("current");

            var id = $(this).attr("id").split("_")[1];
            var title = $(this).attr("dialog-title");
            _errorDialog(id, title);
        });
    }

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "Status" : $(".stateSelect","#filter").val(),
            "Search" : $(".Search","#filter").val()
        };
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаем обработчик открытия подробной инфо о баг репортах
    $("tr",".ron-errors").click(function(){
        $("tr.current",".ron-errors").removeClass("current");
        $(this).addClass("current");

        var id = $(this).attr("id").split("_")[1];
        var title = $(this).attr("dialog-title");
        _errorDialog(id, title);
    });
}

/**
 * Метод отрабатывает диалог с подробной информацие о баг репорте
 * @param id
 * @param title
 * @private
 */
_errorDialog =function(id, title){
    var dContent = $(
        '<div class="dialog-error-info">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: title,
        width: 1100,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-userreg-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по сертификату
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/index/error-info/"+id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));

                        //Обработка кнопок закрыть
                        $(".btClose", dContent).click(function(){
                            $(".dialog, .dialog-error-info").remove();
                        });

                        //Обработка нажатия на кнопку Исправлено
                        $(".btDone", dContent).click(function(){
                            var self = this;
                            if($(this).prop("disabled"))
                                return;

                            var errId = $("#errId", dContent).val();

                            //Отправляем запрос на привязку на сервер
                            $(this).prop("disabled",true);
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                url: "/ron/index/error-done/" + errId,
                                success: function (answer) {
                                    $(self).prop("disabled",false);
                                    if(!answer || !answer.result || answer.result != "success"){
                                        useTopHoverTitleError("При выполнении запроса произошла ошибка. Свяжитесь с администратором.");
                                        return;
                                    }
                                    //Меняем статус у выделенного ряда в таблице
                                    $(".current .err_status",".ron-errors").text("Исправлено");
                                    //Меняем статус на первой вкладке
                                    $("#error_Status",dContent).text("Исправлено");
                                    //Прячем кнопку
                                    $(self).hide();
                                }
                            });
                        });
                    });
                }
            });
        }
    });
}

/**
 * Метод отображает диалог добавления аккр программы в приложение к сертификату
 * @param suppId - id приложения к которому будет добавляться программа
 * @private
 */
_addAccrProgramDialog = function(suppId,  progId){
    var aContent = $(
        '<div class="dialog-add-accr-program">' +
        "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');

    var titlePart = "Добавление ";
    var isEdit = false;
    var link = "";
    if(progId){
        isEdit = true;
        link += '/'+progId;
        titlePart = "Изменение "
    }

    $(aContent).dialog({
        title: titlePart + "образовательной программы",
        width: 700,
        height: 450,
        modal: true,
        close: function () {
            $(".dialog-add-accr-program").remove();
        },
        open: function () {
            //Отсылаем запрос на получение формы
            $.ajax({
                type: 'POST',
                dataType: "html",
                data: {
                    suppId : suppId
                },
                url: "/ron/index/supp-add-program-dialog"+link,
                success: function(answer) {
                    $(".loading-img", aContent).fadeOut(500, function() {
                        aContent.html("").append($(answer));

                        //Обработчики смены селектов
                        var sLevel = $(".select_EduProgramType",aContent);
                        var sUGS = $(".select_EnlargedGroupSpeciality",aContent);
                        var tUGS = $(".tb_EnlargedGroupSpeciality",aContent);

                        var sProgram = $(".select_EduProgram",aContent);

                        sLevel.change(function(){
                            var levelId = sLevel.val();

                            sUGS.html("");
                            tUGS.val("");
                            sProgram.html("");

                            if(addProgramData[levelId]){
                                for(var ugsId in addProgramData[levelId])
                                    sUGS.append($("<option>",{value:ugsId}).text(addProgramData[levelId][ugsId]["UgsCode"]));

                                sUGS.change();
                            }
                        });

                        sUGS.change(function(){
                            var levelId = sLevel.val();
                            var ugsId = sUGS.val();
                            tUGS.val("");
                            sProgram.html("");

                            if(addProgramData[levelId] && addProgramData[levelId][ugsId]){
                                var currentUGS = addProgramData[levelId][ugsId];
                                tUGS.val(currentUGS["UgsName"]);

                                for(var programId in addProgramData[levelId][ugsId]["programs"])
                                    sProgram.append($("<option>",{value:programId}).text(currentUGS["programs"][programId]));
                            }
                        });

                        //Позволять в год вводить только цифры
                        $(".StartYear", aContent).keydown(function(event) {
                            // Разрешаем: backspace, delete, tab и escape
                            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
                                    // Разрешаем: Ctrl+A
                                (event.keyCode == 65 && event.ctrlKey === true) ||
                                    // Разрешаем: home, end, влево, вправо
                                (event.keyCode >= 35 && event.keyCode <= 39)) {
                                // Ничего не делаем
                                return;
                            }
                            else {
                                // Обеждаемся, что это цифра, и останавливаем событие keypress
                                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                                    event.preventDefault();
                                }
                            }
                        });

                        //обработка сохранения
                        $(".btSave",aContent).click(function(){
                            // Собираем данные
                            var data = {};
                            $(".data-field",aContent).each(function(i,item){
                                data[$(item).attr("name")] = $(item).val();
                            });

                            //Отправляем запрос на сохранение
                            $.ajax({
                                type: 'POST',
                                dataType: "json",
                                data: data,
                                url: "/ron/index/supp-add-program",
                                success: function (answer) {
                                    //Проверяем если ошибка
                                    if(answer.result && answer.result=="error"){
                                        useTopHoverTitleError(answer.error);
                                        return;
                                    }
                                    //Если все хорошо выводим алерт и меняем/добавляем
                                    if(answer.result && answer.result=="success"){
                                        if(isEdit){
                                            $(".dialog-certsupplement-info .content-block.accredited .intable tbody.content tr.selected").html($(answer.row).html());
                                            $(".dialog-add-accr-program").remove();
                                            useTopHoverTitle("Программа успешно изменена");
                                        } else {
                                            $(".dialog-add-accr-program").remove();
                                            $(".dialog-certsupplement-info .content-block.accredited .intable tbody.content").append($(answer.row));
                                            useTopHoverTitle("Программа успешно добавлена");
                                        }
                                    }
                                }
                            });
                        })
                    });
                },
            });
        }
    });
}


function filterReset(){
 $(".Search","#filter").val('');
 $(".dtDateStart","#filter").val('');
 $(".dtDateEnd","#filter").val('');
}