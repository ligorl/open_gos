/*
 *
 * @param {type} _this
 * @returns {undefined}
 * Р¤СѓРЅРєС†РёСЏ РїРѕРёСЃРєР° РїРѕ Р·Р°СЏРІРєР°Рј.
 */

function searchApplication(_this) {
    var page = $(_this).attr('page');
    if (page === undefined) {
        page = 0;
    }
    var event = $(".btn-warning").attr('event') + '/' + page;
    var keep_selector = $(".btn-warning").attr('controller');
    var destination_link = '/eo/' + keep_selector + '/' + event;
    $.ajax({
        type: 'POST',
        url: destination_link,
        data: $("#filterForm").serialize(),
        beforeSend: function () {
            $(".content_td .intable_content").html("").append($("<tr><td colspan='6'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")).fadeIn(500);
        },
        success: function (answer) {
            $(".content_td .intable_content").fadeOut(500, function () {
                answer = $(answer);
                $(".content_td .content").html("").append(answer);
                resizeContent($(".viewzone", answer));
                //Р—Р°РїСѓСЃРєР°РµРј РѕР±СЂР°Р±РѕС‚С‡РёРєРё
                var handlerName = keep_selector + event.charAt(0).toUpperCase() + event.substr(1) + "Handlers";
                if (window[handlerName])
                    window[handlerName]();
            });
        }
    });
}

/*
 * РЈРґР°Р»СЏРµС‚ Р·Р°СЏРІРєСѓ Рё РїРµСЂРµРіСЂСѓР¶Р°РµС‚ С‚РµРєСѓС‰СѓСЋ РІРєР»Р°РґРєСѓ
 */
function deleteApplication() {
    if ($('.row_appl').is('.active_appl')) {
        if (confirm("Р’С‹ СѓРІРµСЂРµРЅС‹ С‡С‚Рѕ С…РѕС‚РёС‚Рµ СѓРґР°Р»РёС‚СЊ Р·Р°СЏРІРєСѓ ?")) {
            $.ajax({
                url: '/eo/declaration/delete-declaration/' + $('.row_appl.active_appl').attr('id_appl'),
                success: function (answer) {
                    $('.btn-warning').click();
                }
            });
        }
    } else {
        useTopHoverTitleError("РџРѕР¶Р°Р»СѓР№СЃС‚Р°, РІС‹Р±РµСЂРёС‚Рµ Р·Р°РїРёСЃСЊ");
    }
}

/*
 * РћРєРЅРѕ СЂРµРґР°РєС‚РёСЂРѕРІР°РЅРёСЏ/СЃРѕР·РґР°РЅРёСЏ Р·Р°СЏРІРєРё
 *//*
function openEdit(id_appl) {
    if (typeof(id_appl)=='undefined'||typeof(id_appl)=='object'){
        var id_appl = $('.row_appl.active_appl').attr('id_appl');
    }
    //Р§С‚РѕР±С‹ РЅРµ РѕР±СЂР°Р±Р°С‚С‹РІР°Р»Рѕ РїСЂРё РєСѓС‡Рµ РєР»РёРєРѕРІ
    if($('.row_appl.active_appl').attr("state")=="opening")
        return;
    //
    if (typeof(id_appl)!='undefined'||typeof(id_appl)!='object') {
        $('.row_appl.active_appl').attr("state","opening");
        $.ajax({
            url: '/eo/declaration/edit/' +id_appl ,
            success: function (answer) {
                $("#popup_win").html(answer);
                $("#popup_win").show();
                resizeContent($(".container .viewzone"));
                if ($("#type_declaration").val() == '41152d9a-3e6a-4c6b-9a6b-53e246eb8638') {
                    $('a[tab="2"]').parent('li').hide();
                    $('a[tab="3"]').parent('li').hide();
                }
                getButtonByStep();
                $(".input-date").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy",
                });
                $('.row_appl.active_appl').attr("state","none");
            }
        });
    } else {
 useTopHoverTitleError("РџРѕР¶Р°Р»СѓР№СЃС‚Р°, РІС‹Р±РµСЂРёС‚Рµ Р·Р°РїРёСЃСЊ");
    }
};*/

function getLicenseByIdOrg(id) {
    $.ajax({
        url: '/eo/declaration/get-licenses-by-org/' + id,
        success: function (answer) {
            $("#licensedWrap").html(answer);
        }
    });
}
function getCertificateByIdOrg(id) {
    $.ajax({
        url: '/eo/declaration/get-certificates-by-org/' + id,
        success: function (answer) {
            $("#cetificateWrap").html(answer);
        }
    });
}
function searchOrgReorg(){
    if( $("#filteredTable tbody").height()-$("#filteredTable").scrollTop() == 359 &&  $("#nextPage").val()<= $("#maxPage").val() ){
                if($(".searchOrganization").val()==''){
                    var url='/eo/declaration/get-edu-org/'+$("#nextPage").val();
                }else{
                    var url='/eo/declaration/get-edu-org/'+$("#nextPage").val()+'/'+$(".searchOrganization").val();
                }
                $.ajax({
                    url: url,
                    beforeSend: function (xhr) {
                     $(".loading-overlay").show();
                       $("#getNextResult").parent('td').parent('tr').remove();
                       $("#nextPage").remove();
                       $("#maxPage").remove();
                    },
                    success: function(answer){
                        $(".loading-overlay").fadeOut(250, function(){
                            $("#filteredTable tbody").append(answer);
                        })
                    },
                    error:function(e){
                        useTopHoverTitleError("Р’РѕР·РЅРёРєР»Р° РѕС€РёР±РєР°");
                        $(".loading-overlay").fadeOut(250);
                        console.log("РћС€РёР±РєР°");
                        console.log(e);
                    }
                });
            }
}
function searchOrgAdd(){
    var nextPage=$("#nextPage").val();
    nextPage=parseInt(nextPage);
    var maxPage=$("#maxPage").val();
    maxPage=parseInt(maxPage);
    if(/* $("#filteredTable tbody").height()-$("#filteredTable").scrollTop() == 359
            && */  nextPage<= maxPage ){
                if($(".searchOrganization").val()==''){
                    var url='/eo/declaration/get-organizations-select/'+$("#nextPage").val();
                }else{
                    var url='/eo/declaration/get-organizations-select/'+$("#nextPage").val()+'/'+$(".searchOrganization").val();
                }
                $.ajax({
                    url: url,
                    beforeSend: function (xhr) {
                     $(".loading-overlay").show();
                       $("#getNextResult").parent('td').parent('tr').remove();
                       $("#nextPage").remove();
                       $("#maxPage").remove();
                    },
                    success: function(answer){
                        $(".loading-overlay").fadeOut(250, function(){
                            $("#filteredTable tbody").append(answer);
                        })
                    },
                    error:function(e){
                        useTopHoverTitleError("Р’РѕР·РЅРёРєР»Р° РѕС€РёР±РєР°");
                        $(".loading-overlay").fadeOut(250);
                        console.log("РћС€РёР±РєР°");
                        console.log(e);
                    }
                });
            }
}
function openAdd() {

    $.ajax({
        url: '/eo/declaration/edit',
        success: function (answer) {
            max_show_tab = 0;
            $("#popup_win").html(answer);
            $("#popup_win").show();
            resizeContent($(".container .viewzone"));
            $(".input-date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd.mm.yy",
            });
            $.ajax({
                url: '/eo/declaration/get-application-reason/' + $(this).val(),
                success: function (answer) {
                    $("#reason_td").html(answer);
                }
            });
        }
    });
}

/**
 * С€Р°Рі 5
 * Р”РёР°Р»РѕРі РґР»СЏ РєРЅРѕРїРѕС‡РєРё РїРµСЂРµС…РѕРґР° РїРѕ С€Р°РіР°Рј
 * Р”РѕР±Р°РІР»СЏРµРј Рє Р·Р°СЏРІР»РµРЅРёСЋ СЂР°СЃРїРѕСЂСЏРґРёС‚РµР»СЊРЅС‹Р№ РґРѕРєСѓРјРµРЅС‚
 * (РєРѕРіРґР° РґРѕРєСѓРјРµРЅС‚ Р±СѓРґРµС‚ РїРѕРґРїРёСЃР°РЅ).
 *
 */
function applicationAddOrderDocument(){
    console.log('С…РѕС‚РёРј РґРѕР±Р°РІРёС‚ СЂР°СЃРїРѕСЂСЏРґРёС‚РµР»СЊРЅС‹Р№ РґРѕРєСѓРјРµРЅС‚ Рє Р·Р°СЏРІР»РµРЅРёСЋ');
    var dContent = $(
        '<div class="dialog-orderDecument-add">' +
            "<div style='text-align: center;margin-top:40px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
            title: "Р Р°СЃРїРѕСЂСЏРґРёС‚РµР»СЊРЅС‹Р№ Р°РєС‚",
            width: 640,
            height: 400,
            modal: true,
            close: function() {
                $(".dialog, .dialog-orderDecument-add").remove();
            },
            open: function() {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/declaration/application-document-add/" + $('.row_appl.active_appl').attr('id_appl'),
                    success: function(answer) {
                        console.log(answer);
                         $(".loading-img", dContent).fadeOut(500, function() {
                             dContent.html("").append($(answer));
                             $(".date-calendar").html('Р”Р°С‚Р° РґРѕРєСѓРјРµРЅС‚Р°');
                             //$("#noticie_button-add a").attr('onclick','addNewOrderDocumentFromDeclaration()');
                         });

                    },
                    error:function(e){
                        console.log('РѕС€РёР±РєР° РїСЂРё Р·Р°РіСЂСѓР·РєРµ РїР°РЅРµР»Рё РґРѕР±Р°РІР»РµРЅРёСЏ СЂР°СЃРїРѕСЂСЏРґРёС‚РµР»СЊРЅРѕРіРѕ РґРѕРєСѓРјРµРЅС‚Р°');
                        console.log(e);
                    }

                });
            }
    });

}

function addError(selectorName,hasError){
    jQuery(selectorName).parent().removeClass('has-error');
    if (jQuery(selectorName).val()===''){
        jQuery(selectorName).parent().addClass('has-error');
        return true;
    }
    return hasError;
}

function addNewOrderDocumentFromDeclaration(){
    //РїСЂРѕРІРµСЂСЏРµРј РЅР° РЅР°Р»РёС‡РёРµ Р·РЅР°С‡РµРЅРёР№
    var hasError=false;
    hasError = addError('.document-field-type',hasError);
    hasError = addError('.document-field-kind',hasError);
    hasError = addError('.document-field-number',hasError);
    hasError = addError('.document-field-date',hasError);

    if(hasError){
        useTopHoverTitleError("Р•СЃС‚СЊ РЅРµ Р·Р°РїРѕР»РЅРµРЅС‹Рµ РїРѕР»СЏ");
        return 0;
    }
console.log(jQuery('#CertificateValue').attr('idcert'));
    var sendData={
        "Kind":jQuery('.document-field-kind').val(),
        "Number":jQuery('.document-field-number').val(),
        "DateSign":jQuery('.document-field-date').val(),
        "CertificateId":jQuery('#CertificateValue').attr('idcert')
    };

    //РѕС‚РїСЂР°РІР»СЏРµРј РЅР° СЃРµСЂРІРµСЂ
    $.ajax({
        type: 'POST',
        dataType: "text",
        url: "/eo/index/certificate-document-new",
        data:sendData,
        success: function(answer) {
            //if (answer==='Ok'){

                jQuery(".content-block.documents tbody").append(
                '<tr><td>'
                +jQuery('.document-field-type option:selected').text()
                +'</td><td>'
                +jQuery('.document-field-kind option:selected').text()
                +'</td><td>'
                +jQuery('.document-field-number').val()
                +'</td><td>'
                +jQuery('.document-field-date').val()
                +'</td></tr>'
                    );
                //console.log();
                $(".dialog, .dialog-orderDecument-add").remove();
                showEP()
            //}else{
           //     useTopHoverTitleError('РћС€РёР±РєР° РїСЂРё Р·Р°РїРёСЃРё');
           // }

        },
        error:function(e){
            useTopHoverTitleError('РћС€РёР±РєР° РїСЂРё Р·Р°РїРёСЃРё');
            console.log('РѕС€РёР±РєР° РїСЂРё СЃРѕС…СЂР°РЅРµРЅРёРё РґРѕР±Р°РІР»РµРЅРёСЏ СЂР°СЃРїРѕСЂСЏРґРёС‚РµР»СЊРЅРѕРіРѕ РґРѕРєСѓРјРµРЅС‚Р°');
            console.log(e);
        }

    });

    //РїРѕР»СѓС‡Р°РµРј СЂРµР·СѓР»СЊС‚Р°С‚

}
/*
 *РџРѕР»СѓС‡РµРЅРёРµ РєРЅРѕРїРѕРє РґР»СЏ РѕРєРЅР° РЅР° РѕРїСЂРµРґРµР»РµРЅРѕРј С€Р°РіРµ
 */
function getButtonByStep() {
    $("#popup_win #but_wrap_child_right #btActions-block li").each(function(indx, element){
        if(!jQuery(element).hasClass('btn_app_card_edit_default')){
            jQuery(element).remove();
        }
    });
    console.log($("#popup_win #but_wrap_child_right #btActions-block"));
    var idAppl=jQuery("#id_application_val_id").val();
    $.ajax({
        url: '/eo/declaration/get-button-by-step/' + $("#now-step").val()+'/'+idAppl,
        async:false,
        timeout:2000,
        success: function (answer) {
            $("#popup_win #but_wrap_child_right #btActions-block").append(answer);
        }
    });

}
/*
 * @param string new_stat РќРѕРІС‹Р№ СЃС‚Р°С‚СѓСЃ Р·Р°СЏРІРєРё
 */
function changeStatus(new_stat, id_appl, more_func, bt_click,isConfirm,after_func) {
    if (typeof(isConfirm)==='undefined'){
        isConfirm=1;
    }
    if(isConfirm){
        if (!confirm('Р’С‹ СѓРІРµСЂРµРЅС‹ С‡С‚Рѕ С…РѕС‚РёС‚Рµ РёР·РјРµРЅРёС‚СЊ СЃС‚Р°С‚СѓСЃ Р·Р°СЏРІРєРё ?')) {
            return 0;
        }
    }
    if (bt_click === undefined){
        bt_click=0;
    }
    more_func = more_func || 0;
    if (id_appl === undefined || id_appl === 0) {
        id_appl = $("#id_application_val").val();
    }
    $.ajax({
        type: "POST",
        url: '/eo/declaration/change-status-declaration',
        data: "newStatus=" + new_stat + "&idAppl=" + id_appl + "",
        success: function (answer) {
            if (answer == 'ok') {
                if (more_func != 0) {
                    eval(more_func);
                }
                useTopHoverTitle("РЎС‚Р°С‚СѓСЃ Р·Р°СЏРІРєРё РёР·РјРµРЅРµРЅ!");
                if (bt_click ) {
                    $(".btn-warning").click();
                    max_show_tab = 0;
                }
                if (typeof(after_func)=='function'){
                    after_func();
                }
                if(typeof(after_func)=='string'){
                    eval(after_func);
                }
            }
        }
    });
    $('.ui-dialog').hide();
    $('.ui-widget-overlay').hide();
}

/*
 * РћРєРЅРѕ СЂРµРґР°РєС‚РёСЂРѕРІР°РЅРёСЏ/СЃРѕР·РґР°РЅРёСЏ Р·Р°СЏРІРєРё
 */
function showDeclaration(appId) {
    if(appId==undefined){
        appId = $('.row_appl.active_appl').length;
        if(appId>0){
            appId = $('.row_appl.active_appl').attr('id_appl');
        }else{
            useTopHoverTitleError("РџРѕР¶Р°Р»СѓР№СЃС‚Р°, РІС‹Р±РµСЂРёС‚Рµ Р·Р°РїРёСЃСЊ");
        }
    }
    if (appId) {
        $.ajax({
            url: '/eo/declaration/edit/' + appId,
            success: function (answer) {
                $("#popup_win").html(answer);
                $("#popup_win").show();
                resizeContent($(".container .viewzone"));
                if ($("#type_declaration").val() == '41152d9a-3e6a-4c6b-9a6b-53e246eb8638') {
                    $('a[tab="2"]').parent('li').hide();
                    $('a[tab="3"]').parent('li').hide();
                }
                $("#but_wrap_child_left").hide();
                $(/*'#but_wrap_child_left*/ '#btActions-Noticie ').parent().parent().show();
                $("#fields_form input").attr('disabled', "disabled");
                getButtonByStep();
                $(".input-date").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy",
                });
            }
        });
    } else {
        useTopHoverTitleError("РџРѕР¶Р°Р»СѓР№СЃС‚Р°, РІС‹Р±РµСЂРёС‚Рµ Р·Р°РїРёСЃСЊ");
    }
}
;

/*
 * РїРµСЂРµС…РѕРґ РїРѕ РІРєР»Р°РґРєР°Рј РЅР° С„РѕСЂРјРµ СЂРµРґР°РєС‚РёСЂРѕРІР°РЅРёСЏ
 *//*
function showHideTabs(show, hide) {
    $(".show_tab[tab='" + hide + "']").removeClass('active');
    $(".show_tab[tab='" + show + "']").addClass("active");
    $(".tabs-panel").removeClass("show-panel");
    $("#tab" + show + "").addClass('show-panel');
    $("#sendform1").attr('numb', show);
    if(show != 4){$("#button_load_dop_docs").remove(); }
}*/

function getFillialsPrograms(){
            $.ajax({
                url: '/eo/declaration/get-fillials-programs/' + $("#id_application_val").val(),
                type: "POST",
                beforeSend: function () {
                    $(".loading-overlay").show();
                },
                success: function (answer) {
                    $(".leftSideBar").html(answer);
                    $(".loading-overlay").fadeOut(250);
                },
                error:function(e){
                    useTopHoverTitleError("Р’РѕР·РЅРёРєР»Р° РѕС€РёР±РєР°");
                    $(".loading-overlay").fadeOut(250);
                    console.log("РћС€РёР±РєР°");
                    console.log(e);
                }
            });
}



function handleResponse2(mes) {
    $(".loading-overlay").hide();
    if (mes == "error") {
        useTopHoverTitleError("Р’РѕР·РЅРёРєР»Рё РѕС€РёР±РєРё РїСЂРё РґРѕР±Р°РІР»РµРЅРёРё Р·Р°СЏРІРєРё!");
    } else {
        useTopHoverTitle('Р—Р°СЏРІРєР° РЅР° СЌРєСЃРїРµСЂС‚РёР·Сѓ СѓРґР°С‡РЅРѕ СЃРѕР·РґР°РЅР°');
        $('#popup_win2 #cboxClose').click();
        getButtonByExpertise();
    }
}

/*
 * РїРѕР»СѓС‡РµРЅРёРµ СЃРїРёСЃРєР° Р·Р°РіСЂСѓР¶РµРЅРЅС‹С… С„Р°Р№Р»РѕРІ.
 */
/*
function getFilesAjax(){
    $.ajax({
        url: '/eo/declaration/application-files/'+$("#id_application_val").val(),
        beforeSend: function () {
                $(".loading-overlay").show();
        },
        success: function(answer){
            $("#tab4").html(answer);
            $('#button_load_dop_docs').remove();
            if($(".importantFiles .maySent").length==0 && ($(".active_appl").attr('id_status') === undefined || $(".active_appl").attr('id_status') == '38D871AC-BF1D-469E-AFC1-7A6CD55EF1B6') ){
                $("#sendform1").hide();
                $("#but_wrap_child_right #btActions-block").prepend('<li class="filelink sendApplication"><a onclick="sendToSend()" href="javascript:;"><div></div><span class="text">РћС‚РїСЂР°РІРёС‚СЊ</span></a></li>');
            }else{
                $(".maySent").parents('.containerWrapFile').css('border','1px solid red');
                $(".maySent").focus();
                $("#but_wrap_child_right #btActions-block .sendApplication").remove();
            }
            $(".loading-overlay").fadeOut(250);
        },
        error:function(e){
 useTopHoverTitleError("Р’РѕР·РЅРёРєР»Р° РѕС€РёР±РєР°");
            $(".loading-overlay").fadeOut(250);
            console.log("РћС€РёР±РєР°");
            console.log(e);
        }
});
}*/

/**
 * С„-С†РёСЏ Р·Р°РєСЂС‹С‚РёСЏ РґРёР°Р»РѕРіРѕРІРѕРіРѕ РѕРєРЅР° Р·Р°РіСЂСѓР·РєРё РґРѕРїРѕР»РЅРёС‚РµР»СЊРЅРѕРіРѕ РґРѕРє-С‚Р° Рё РІС‹РІРѕРґР° СЃРѕРѕР±С‰С‰РµРЅРёСЏ
 */
function closeDialogAddDopDocs(){
    $(".loading-overlay").hide();
    var count_files = 1, ending;
    count_files = $(".MultiFile-label", "#dop_files_list_upload").length;
    ending = (count_files > 1) ? 'С‹' : '' ;


    $('.dialog').remove();
    $("<div class='dialog'></div>").html("<p>Р”РѕРєСѓРјРµРЅС‚" + ending + " СѓСЃРїРµС€РЅРѕ РґРѕР±Р°РІР»РµРЅ" + ending + " РІ СЃРёСЃС‚РµРјСѓ</p><div class='nav_footer' style='float:right'><ul><li><a href='javascript:;' onclick='$(\".dialog\").dialog(\"close\");$(\".dialog\").remove();'>Р—Р°РєСЂС‹С‚СЊ</a></li></ul></div>")
        .dialog({
            autoOpen:false,
            title: "РЈРІРµРґРѕРјР»РµРЅРёРµ",
            width:500,
            modal:true
        }).dialog("open");
}

/**
 * Р·Р°РіСЂСѓР·РєР° РґРѕРїРѕР»РЅРёС‚РµР»СЊРЅРѕРіРѕ РґРѕРєСѓРјРµРЅС‚Р° РІ СЃРёСЃС‚РµРјСѓ
 */
function load_dop_file_in_system(){
    var form_id = "#dop_files_list_upload",
        count_files = 0;

    //РїСЂРѕРІРµСЂРєР° СѓСЃС‚Р°РЅРѕРІР»РµРЅ РѕР±СЏР·Р°С‚РµР»СЊРЅРѕСЃС‚СЊ РїРѕР»РµР№
    count_files = $(".FileListWrap div", form_id).length;
    if(count_files == 0){
       $("input[type='file']", form_id).after('<span style="color:red" class="error_text">РќРµРѕР±С…РѕРґРёРјРѕ РІС‹Р±СЂР°С‚СЊ С„Р°Р№Р»</span>');
       return false;
    }

    //СЃРѕС…СЂР°РЅРµРЅРёРµ РґРѕРєСѓРјРµРЅС‚Р° РЅР° СЃРµСЂРІРµСЂРµ
    $(".loading-overlay").show();
    closeDialogAddDopDocs();
    getFilesAjax();
    //$(form_id).submit();

}

/**
 * РѕРєРЅРѕ СѓРґР°Р»РµРЅРёРµ РґРѕРєСѓРјРµРЅС‚Р°
 * @param {Integer} file_id РёРґ СѓРґР°Р»СЏРµРјРѕРіРѕ С„Р°Р№Р»Р°
 * @param {Integer} table_flag value = 1 - СѓРґР°Р»СЏРµРј РёР· С‚Р°Р±Р»РёС†С‹ isga_ApplicationFiles, 2 - РёР· С‚Р°Р±Р»РёС†С‹ isga_ApplicationDocumentGroups
 */
function window_delete_application_file(file_id, table_flag){
    var buttons = '<div class="nav_footer" style="float:right"><ul><li><a href="javascript:;" onclick="delete_application_file(\'' + file_id +'\', ' + table_flag + ')">РЈРґР°Р»РёС‚СЊ</a></li><li><a href="javascript:;" onclick="$(\'.dialog\').dialog(\'close\');$(\'.dialog\').remove();">РћС‚РјРµРЅР°</a></li></ul></div>';
    $('.dialog').remove();
    $("<div class='dialog'></div>").html("<p class='message_for_user'>РЈРґР°Р»РёС‚СЊ РґРѕРєСѓРјРµРЅС‚?</p>" + buttons)
        .dialog({
            autoOpen:false,
            title: "РЈРІРµРґРѕРјР»РµРЅРёРµ",
            modal:true
        }).dialog("open");
}

/**
 * СѓРґР°Р»РµРЅРёРµ РґРѕРєСѓРјРµРЅС‚Р°
 * @param {Integer} file_id РёРґ СѓРґР°Р»СЏРµРјРѕРіРѕ С„Р°Р№Р»Р°
 * @param {Integer} table_flag value = 1 - СѓРґР°Р»СЏРµРј РёР· С‚Р°Р±Р»РёС†С‹ isga_ApplicationFiles, 2 - РёР· С‚Р°Р±Р»РёС†С‹ isga_ApplicationDocumentGroups
 */
function delete_application_file(file_id, table_flag){
    $.ajax({
        type: "POST",
        url: "/eo/declaration/delete-application-files",
        dataType: 'json',
        beforeSend: function () {
            $(".loading-overlay").show();
        },
        data: ({file_id : file_id, table_flag : table_flag}),
        success: function(data){
            $(".loading-overlay").hide();
            handleResponse(4);
            var message = "Р”РѕРєСѓРјРµРЅС‚ СѓСЃРїРµС€РЅРѕ СѓРґР°Р»РµРЅ"
            if(data.response == 'error'){
               message = data.message;
            }

            $('.dialog').dialog("close");$('.dialog').remove();
            var buttons = '<div class="nav_footer" style="float:right"><ul><li><a href="javascript:;" onclick="$(\'.dialog\').dialog(\'close\');$(\'.dialog\').remove();">Р—Р°РєСЂС‹С‚СЊ</a></li></ul></div>';
            $("<div class='dialog'></div>").html("<p>" + message + buttons + "</p>")
                .dialog({
                        autoOpen:false,
                        title: "РЈРІРµРґРѕРјР»РµРЅРёРµ",
                        modal:true
                }).dialog("open")

        }
    });
}

/**
 * С„СѓРЅРєС†РёСЏ РѕС‚РєСЂС‹С‚РёСЏ РѕРєРЅР° Р—Р°РіСЂСѓР·РєРё РґРѕРїРѕР»РЅРёС‚РµР»СЊРЅС‹С… РґРѕРєСѓРјРµРЅС‚РѕРІ
 */
function loadDopDocs(){
    var id_application = $("#id_application_val").val();

    //РїРѕРґРіСЂСѓР·РёРј С„РѕСЂРјСѓ РґР»СЏ Р·Р°РіСЂСѓР·РєРё
    $.ajax({
        url: '/eo/declaration/application-dop-files/' + id_application,
        beforeSend: function () {
            $(".loading-overlay").show();
        },
        success: function(response){
            $(".loading-overlay").hide();
            $('.dialog').remove();
            $("<div class='dialog'></div>").html("<p class='segoe'>" + response + "</p>")
                .dialog({
                    autoOpen: false,
                    title: "Р—Р°РіСЂСѓР·РєР° РґРѕРєСѓРјРµРЅС‚Р°",
                    modal: true,
                    width: 600
                }).dialog("open")
        }
    });



}

/*
 /*
  * РћРўРїСЂР°РІРєР° Р·Р°СЏРІРєРё РЅР° РІРєР»Р°РґРєСѓ РѕС‚РїСЂР°РІР»РµРЅРЅС‹Рµ
  */
/*
 function sendToSend(){
     if($(".importantFiles .maySent").length>0){
 useTopHoverTitleError("Р’С‹ РЅРµ Р·Р°РіСЂСѓР·РёР»Рё РѕРґРёРЅ РёР· РѕР±СЏР·Р°С‚РµР»СЊРЅС‹С… РґРѕРєСѓРјРµРЅС‚РѕРІ.");
         $(".maySent").parents('.containerWrapFile').css('border','1px solid red');
         $(".maySent").focus();
     }else{
        $.ajax({
            url: '/eo/declaration/send-to-send/'+$("#id_application_val").val(),
            beforeSend: function () {
                $(".loading-overlay").show();
            },
            success: function(answer){
                console.log(answer);
                if ( answer == 'Ok' ) {
                    console.log(answer);
                    useTopHoverTitle('Р’Р°С€Рµ Р·Р°СЏРІР»РµРЅРёРµ РЅР°РїСЂР°РІР»РµРЅРѕ РІ Р РѕСЃРѕР±СЂРЅР°РґР·РѕСЂ',2000)
                    $('.btn-warning').click();
                    $(".loading-overlay").fadeOut(250);
                }
                if( answer == 'OkNo' ) {
                    console.log(answer);
                    //useTopHoverTitleError("Р—Р°СЏРІРєР° РѕС‚РїСЂР°РІР»РµРЅР°");
                    $('.btn-warning').click();
                    $(".loading-overlay").fadeOut(250);
                }
                if ( answer == 'Unsigned') {
                    showPop('/eo/declaration/get-form-signed/'+$("#id_application_val").val());
                    $(".loading-overlay").show();
                }

            },
            error:function(e){
 useTopHoverTitleError("Р’РѕР·РЅРёРєР»Р° РѕС€РёР±РєР°");
                $(".loading-overlay").fadeOut(250);
                console.log("РћС€РёР±РєР°");
                console.log(e);
            }
        });
     }
 }*/
function showPop(url,showWaitWindow) {
    url=encodeURI(url);//РїРµСЂСЃРѕРЅР°Р»СЊРЅРѕ РґР»СЏ РР•
    if(showWaitWindow == '1')
        $(".loading-overlay").show();
    $.ajax({
        type: 'POST',
        url: url,
        success: function (answer) {
            $("#popupWindow").html(answer);
            $("#popupWindow").show();
        }
    });
}
$(document).ready(function () {
    $("body").delegate(".btCancel","click",function(){
        $("#filterForm")[0].reset();
    });
    /*$("body").delegate("#filterForm .btApply", "click", function () {
        searchApplication(this);
    });*/
    $("body").delegate("#appSearchInput", "keyup", function (e) {
        if(e.which==13){
            searchApplication(this);
            if(e.stopPropagation) e.stopPropagation();
            if(e.preventDefault) e.preventDefault();
        }
    });
    $('body').undelegate('.row_appl', 'click');
    $('body').delegate('.row_appl', 'click', function () {
        $('.row_appl').removeClass('active_appl');
        $(this).addClass('active_appl');
    });
   /* $("body").delegate("#sendform1", "click", function () {
        if ($(this).attr('numb') == 1) {
            if(jQuery('#Id_organization').val()==''){
    useTopHoverTitleError('РќРµ РІС‹Р±СЂР°РЅР° РѕСЂРіР°РЅРёР·Р°С†РёСЏ');
                return 0;
            }
            var step = jQuery("#now-step").val();
            var countSelected=jQuery("#reason_td .check_req:checked").length;
            if(countSelected<1 && step==1){
    useTopHoverTitleError('РќРµ РІС‹Р±СЂР°РЅРѕ РѕСЃРЅРѕРІР°РЅРёР№ Р·Р°СЏРІР»РµРЅРёСЏ');
                return 0;
            }
            //$(".loading-overlay").show();
            $("#submitFirstForm").click();
            //$("#edit_declaration"+$(this).attr('numb')).submit();
        } else {
            handleResponse($(this).attr('numb'), 1);
        }
    });
    $("body").delegate("#sendform2", "click", function () {
        $("#popup_win2 #submitFirstForm").click();
    });*/
    $("body").delegate(".page","click",function(){
        searchApplication(this);
    });
    $("#wrapReorg").delegate("#getNextResult","click",function(){
            searchOrgAdd();
        });
        $("#popupWindow").delegate(".searchOrganization","change",function(){
            if($("#add_reorg_struct").hasClass('clicked')){
                showPop('/eo/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
            }else{
                showPop('/eo/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
            }
        });
        //РѕР±СЂР°Р±РѕС‚С‡РёРє С‚С‹РєРѕРІ РїРѕ РіР°Р»С‡РєР°Рј РїСЂРёС‡РёРЅ/РѕСЃРЅРѕРІР°РЅРёР№ Р·Р°СЏРІР»РµРЅРёР№
        $("body").delegate(".check_req","click",function(){
            if($(".check_req:checked").length>0){
                var isset = true;
            }else{
                var isset = false;
            }
            if(isset === false){
                $(".check_req").prop('disabled',false);
            }else{
                $(".check_req").prop('disabled',true);
                $(".check_req[groupe_id='"+$(this).attr('groupe_id')+"']").prop('disabled',false);
            }
            //РµСЃР»Рё Р·Р°Р±Р°РЅРµРЅР° С‚РёРї РґСѓРїР»РёРєР°С‚Р°, С‚Рѕ РЅРµР»СЊР·СЏ РІС‹Р±СЂР°С‚СЊ РїРёСЂС‡РёРЅС‹
            //(РЅРµС‚ РїСЂРµРґС‹РґСѓС‰РµРіРѕ СЃРІРёРґРµС‚РµР»СЊСЃС‚РІР°)
            //Р±Р°РЅРёРј РґРѕРѕРєСЂРµРґРёС‚Р°С†РёСЋ Рё РїРµСЂРµРѕС„РѕСЂРјР»РµРЅРёРµ
            var appDuplicatType=jQuery("#type_declaration option[code='DublicateCertificate']").attr('disabled');
            if(appDuplicatType !== undefined){
                jQuery('#reason_td .check_req[groupe_id="1"]').attr('disabled','disabled');
                //jQuery('#reason_td .check_req[groupe_id="3"]').attr('disabled','disabled');
            }

        });
        /*$("#popupWindow").undelegate("#add_org","click");
        $("#popupWindow").delegate("#add_org","click",function(){
            $("#Id_organization").val($('.selected_org:checked').val());
            $("#nameOrganization").val($('.selected_org:checked').attr('org_name'));
            getLicenseByIdOrg($('.selected_org:checked').val());
            getCertificateByIdOrg($('.selected_org:checked').val());
            $("#popupWindow").hide();
        });*/

        $('body').delegate('.level-elem', 'click', function () {
            $('.level-elem').removeClass('active-level');
            $(this).addClass('active-level');
            getUgsList();

        });
        $('body').delegate('.programCol', 'change', function () {
            saveProgramField(this);
        });
        $('body').delegate('.ugs_row', 'click', function () {
            $('.ugs_row').removeClass('active_ugs_row');
            $(this).addClass('active_ugs_row');
            getProgramsByUgs();
        });
        $('body').delegate('#show_all_program', 'click', function () {
            setTimeout(function () {
                if ($("#show_all_program").is(":checked")) {
                    $('.ugs_row').each(function (i) {
                        $(this).click();
                    });
                } else {
                    $('.ugs_row:first').click();
                }
            },500);
        });
        $("body").delegate(".remove_answer","click",function(){
            $(this).parent('.wrap_file_answer').remove();
        });
        $("body").delegate("#search_direct","keyup",function(){
            var search_str = $("#search_direct").val().toLowerCase();
            if(search_str==""){ $(".list_of_direct li").show(); return 1;}
            $(".list_of_direct li label").each(function(i){
                var text = $(this).text().toLowerCase();
                if(text.indexOf(search_str)===-1){
                    $(this).parent('li').hide();
                }else{
                    $(this).parent('li').show();
                }
            });
        });
        $("body").delegate(".select_all_direct","click",function(){
            if($(this).is(":checked")){
                $(".list_of_direct li input[type=checkbox]").prop('checked', true);
            }else{
                $(".list_of_direct li input[type=checkbox]").prop('checked', false);
            }
        });
});
function showEP(){
    var dContent = $(
        '<div class="dialog-orderDecument-add">' +
            "<div style='text-align: center;'><div class='loading-img'><img src='/img/loading.gif' /></div><div class='leftSideBar'></div><div class='rightSideBar'></div></div>"+
        '</div>');
    $(dContent).dialog({
            title: "Р’С‹Р±РѕСЂ РѕР±СЂР°Р·РѕРІР°С‚РµР»СЊРЅС‹С… РїСЂРѕРіСЂР°РјРј",
            width: 1170,
            height: 600,
            modal: true,
            close: function() {
                $(".dialog, .dialog-orderDecument-add").remove();
            },
            open: function() {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/declaration/get-fillials-programs/"+$('.row_appl.active_appl').attr('id_appl')+'/1',
                    success: function(answer) {
                         $(".loading-img", dContent).fadeOut(500, function() {
                             $(".ui-dialog-titlebar-close").hide();
                             $(".dialog-orderDecument-add").prepend('<p>Р’С‹Р±РµСЂРёС‚Рµ РЅСѓР¶РЅС‹Рµ РѕР±СЂР°Р·РѕРІР°С‚РµР»СЊРЅС‹Рµ РїСЂРѕРіСЂР°РјРјС‹ Рё РЅР°Р¶РјРёС‚Рµ РєРЅРѕРїРєСѓ "Р—Р°РєСЂС‹С‚СЊ".</p>');
                             $(".dialog-orderDecument-add .leftSideBar").html("").append(answer);
                             $(".dialog-orderDecument-add .leftSideBar").append('<style>.allhidden{display:table-cell;}</style>');
                             $('.allhidden',dContent).css('display','table-cell');
                             //jQuery('.program-checked').css('display','table-cell');
                             //$(".dialog-orderDecument-add").append('<a class="right-close" type="button" onclick="changeStatus(\'AF475ADD-6ED4-43F1-9E66-C9C2BF4E2DB9\',0,\'changeBeforeAddOrderDocument()\',0,0,\'changeAfterAddOrderDocument()\');" href="javascript:;"><span class="text">Р—Р°РєСЂС‹С‚СЊ</span></a>');
                             //$(".dialog-orderDecument-add").append('<a class="right-close" type="button" onclick="changeBeforeAddOrderDocument();changeAfterAddOrderDocument();" href="javascript:;"><span class="text">Р—Р°РєСЂС‹С‚СЊ</span></a>');
                         });
                    },
                    error:function(e){
                        console.log('РѕС€РёР±РєР° РїСЂРё Р·Р°РіСЂСѓР·РєРµ РїР°РЅРµР»Рё РґРѕР±Р°РІР»РµРЅРёСЏ СЂР°СЃРїРѕСЂСЏРґРёС‚РµР»СЊРЅРѕРіРѕ РґРѕРєСѓРјРµРЅС‚Р°');
                        console.log(e);
                    }
                });
            }
    });
}

var appIdMemory=0;
function changeBeforeAddOrderDocument(){
    appIdMemory=$('.row_appl.active_appl').attr('id_appl');
}

function changeAfterAddOrderDocument(){
                //РїРѕСЃР»Рµ Р·Р°РєСЂС‹С‚РёСЏ Р°РІС‚РѕРјР°С‚РѕРј РїРµСЂРµРїСЂС‹РіРёРІР°РµРј РЅР° СЃР»РµРґ С€Р°Рі
                //Рё РѕС‚РєСЂС‹РІР°РµРј РєР°СЂС‚РѕС‡РєСѓ Р·Р°СЏРІР»РµРЅРёСЏ
                 $(".dialog, .dialog-orderDecument-add").remove();
                 jQuery('.btn.btn-success[event="step/6"]').click();
                //jQuery('.btn-warning').removeClass('btn-lg');
                //jQuery('.btn-warning').addClass('btn-success');
                //jQuery('.btn-warning').removeClass('btn-warning');
                //jQuery('.btn.btn-success[event="step/6"]').addClass('btn-warning');
                //jQuery('.btn-warning').addClass('btn-lg');
                //jQuery('.btn-warning').removeClass('btn-success');
                //jQuery('#now-step').val(6);
                //showDeclaration(appIdMemory);
                //jQuery('.btn-warning').click();

}

function sendEPChecked(elem){
    if($(elem).prop('checked')){
        var status = 1;
    }else{
        var status = 0;
    }
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/eo/declaration/change-status-ep/"+$(elem).attr('attr_id')+'/'+status,
    });
//$(".dialog, .dialog-orderDecument-add").remove();
}
function openDialogLoadXML() {
    var content = $('#dialog-loadXML');
    content.dialog({
        title: 'РџРѕРёСЃРє Р·Р°СЏРІР»РµРЅРёСЏ',
        width: '95%',
        height: 600,
        open: function () {
            $('#divFormLoadXml').css('display', 'block');
            $('#formLoadXml', '#dialog-loadXML')[0].reset();
            $('#divDiffData', '#dialog-loadXML').empty();
            $('#btInWork').css('display', 'none');
        },
        close: function () {
            //$(".btn-warning").click();
        }
    });
}

function setData() {
    $('#divFormLoadXml').toggle();
    $('#divDiffData').html($("#frameLoadXml").contents().find("#resultData").html());
}

function loadXMLResponse(data) {
    data = jQuery.parseJSON(data);
    if (data.status === 'OK') {
        $('#divFormLoadXml').toggle();
        $('#divDiffData').html(decodeURIComponent(data.html));
    } else {
        useTopHoverTitleError(data.message);
    }
}

function echoDivDiffData(app_id, file_name) {
    $.ajax({
        url: '/eo/declaration/get-diff-data/' + app_id + '/' + file_name,
        type: "POST",
        success: function (answer) {
            $('#divDiffData').html(answer);
        }
    });
}

function applyDiffData() {
    $.ajax({
        url: '/eo/declaration/apply-diff-data/' + app_edit_data_id,
        type: "POST",
        success: function (answer) {
            useTopHoverTitle(answer);
        }
    });
}

function notifyShow(){

}
/*
  * Ajax РѕС‚РїСЂР°РІРєР° С„Р°Р№Р»РѕРІ РЅР° СЃРµСЂРІРµСЂ
  */
 function SendFile() {
    //РѕС‚РїСЂР°РІРєР° С„Р°Р№Р»Р° РЅР° СЃРµСЂРІРµСЂ
    $(".loading-overlay").show();
    $("#files_list_upload").submit();
}

function btnBackToRegistre(){
    var id_appl = $(".supplements.intable_content tr.active_appl").attr('id_appl');
    if (id_appl === undefined || id_appl === 0) {
        useTopHoverTitleError("Р’С‹Р±РµСЂРёС‚Рµ Р·Р°СЏРІР»РµРЅРёРµ") ;
        return 0;
    }
        changeStatus('0FE99A13-98BB-4235-97B2-B65819AB34AF',id_appl,0,1);
}

function btnBackToNotifyPositivSign(){
    var id_appl = $(".supplements.intable_content tr.active_appl").attr('id_appl');
    if (id_appl === undefined || id_appl === 0) {
        useTopHoverTitleError("Р’С‹Р±РµСЂРёС‚Рµ Р·Р°СЏРІР»РµРЅРёРµ") ;
        return 0;
    }
        changeStatus('342fa3ba-27a5-421c-9faa-3784e3e7f7a9',id_appl,0,1);
}


/**
 * РєРЅРѕРїРєР° РЅР°Р·Р°Рґ РЅР° С€Р°РіРµ РџРѕРґРіРѕС‚РѕРІРєР° СЂР°СЃРїРѕСЂСЏРґРёС‚РµР»СЊРЅРѕРіРѕ РґРѕРєСѓРјРµРЅС‚Р°
 *
 * @returns {Number}
 */
function btnBackFromOrderDocStep(){
    var id_appl = $(".supplements.intable_content tr.active_appl").attr('id_appl');
    if (id_appl === undefined || id_appl === 0) {
        useTopHoverTitleError("Р’С‹Р±РµСЂРёС‚Рµ Р·Р°СЏРІР»РµРЅРёРµ") ;
        return 0;
    }
    var id_status = $(".supplements.intable_content tr.active_appl").attr('id_status');
    id_status = id_status.toUpperCase();
    //РєРѕСЂРѕС‚РєРѕРµ
    if(id_status == '26BDDB50-A743-489D-A543-377F024F11FE'){
        changeStatus('E4435D27-3A66-49B6-B1A6-8804AB318C01',id_appl,0,1);
        return 0;
    }
    //Р”Р»РёРЅРЅРѕРµ
    if(id_status == '502B4245-08F3-4CFF-BF51-907058B93AA9'){
        changeStatus('344F1AC9-CCE7-4A5F-A478-39ABDEDA5B21',id_appl,0,1);
        return 0;
    }
    useTopHoverTitleError('РќРµ Р·РЅР°СЋ РєСѓРґР° РёРґС‚Рё');
}



function getEduLevel() {
    $.ajax({
        url: '/eo/declaration/get-edu-program-type/' + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org'),
        type: "POST",
        success: function (answer) {
            $('.level-list').addClass('old-level-list');
            $('.old-level-list').remove();
            $('.active_fil').parent('li').append(answer);
            $('.level-list').show(250);
            getUgsList();
        }
    });
}/*
function getUgsList() {
    $.ajax({
        url: '/eo/declaration/get-ugs-by-level/' + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org')+'/'+$(".level-elem.active-level").attr('id-level'),
        type: "POST",
        success: function (answer) {
            $('.rightSideBar').html(answer);
            getProgramsByUgs();
        }
    });
}*/
function getDirectByUgs(id){
    $('.ugs_row').removeClass('active_ugs_row');
    $('.ugs_row[ugs_id="'+id+'"]').addClass('active_ugs_row');
    var dContent = $(
        '<div class="dialog-directugs-info">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "РќР°РїСЂР°РІР»РµРЅРёРµ РїРѕРґРіРѕС‚РѕРІРєРё",
        width: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-directugs-info").remove();
        },
        open: function() {
            //РћС‚СЃС‹Р»Р°РµРј Р·Р°РїСЂРѕСЃ РЅР° РїРѕР»СѓС‡РµРЅРёРµ РёРЅС„Рѕ РїРѕ Р·Р°СЏРІР»РµРЅРёСЋ
            $.ajax({
                type: 'POST',
                dataType: "html",

                url: '/eo/declaration/get-direct-by-ugs/'  + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org') + '/' + $('.active-level').attr('id-level') + '/' + id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Р’РµС€Р°РµРј РѕР±СЂР°Р±РѕС‚С‡РёРєРё

                        //РЈСЃС‚Р°РЅР°РІР»РёРІР°РµРј РІС‹СЃРѕС‚Сѓ
                        $(".dialog-content",dContent).height($(dContent).height()-20);
                    });
                }
            });
        }
    });
}
function sendCheckDirect(){
    $.ajax({
        type: 'POST',
        data:$("#direct_form").serialize()+"&appl="+ $(".active_fil").attr('id_appl')+"&org="+$(".active_fil").attr('id_org')+"&level="+$('.active-level').attr('id-level'),
        url: '/eo/declaration/save-checked-direct',
        success: function (answer) {
             $(".dialog, .dialog-directugs-info").remove();
             getProgramsByUgs();
        }
    });
}
/*
function getProgramsByUgs() {
    $.ajax({
        type: 'POST',
        dataType: "html",
        async: false,
        url: '/eo/declaration/get-program-by-ugs/' + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org') + '/' + $('.active-level').attr('id-level') + '/' + $('.active_ugs_row').attr('ugs_id'),
        success: function (answer) {
            if(!$("#show_all_program").is(":checked")){
                $('.ugs_block').removeClass('active_ugs_block');
            }
            $('.ugs_block[ugs_id="'+$('.active_ugs_row').attr('ugs_id')+'"]').html(answer);
            $('.ugs_block[ugs_id="'+$('.active_ugs_row').attr('ugs_id')+'"]').addClass('active_ugs_block');
            tableProgramsByUgsHeadering();
        }
    });
}*/
function saveProgramField(elem) {
    console.log('СЂРѕРЅ - Р°РєСЂРµРґРёС‚Р°С†РёСЏ - РєР°РЅСЂС‚РѕС‡РєР° Р·Р°СЏРІР»РµРЅРёСЏ - РїСЂРѕРіСЂР°РјРјС‹ - С‚С‹Рє РїРѕ С„Р»Р°Р¶РєСѓ - 3');
    var id = $(elem).parent().parent().attr('id_program');
    var fieldName = $(elem).attr('name');
    var fieldVal = $(elem).val();
    
    if( jQuery(elem).is('[type="checkbox"]') ){
        if( jQuery(elem).is(':checked') ){
            fieldVal = 1;
        }
        else{
            fieldVal = 0;
        }
    }
    
    $.ajax({
        type: 'POST',
        dataType: "html",
        data: fieldName+"="+fieldVal,
        url: '/eo/declaration/save-field-program-application/'+id,
    });
}
function deleteProgramAppl(id){
     $.ajax({
        type: 'POST',
        dataType: "html",
        url: '/eo/declaration/delete-program-application/'+id,
        success:function(){
             getProgramsByUgs()
        }
    });
}

function getEditComments(){
    var url = '/eo/declaration/get-edit-comment/' + $("#id_application_val").val();
    $.ajax({
        url: url ,
        type: "POST",
        success: function (answer) {
            $('#tab11').html(answer)
        },
        error:function(e){
            console.log('РѕС€РёР±РєР° РїСЂРё РїРѕР»СѓС‡РµРЅРёРё РєРѕРјРјРµРЅС‚Р°СЂРёРµРІ');
            console.log(e);
        }

    });
}


/*РїСЂРёРіСЂСѓР¶Р°РµРј РєРЅРѕРїРєРё РЅР° С€Р°РіРµ РђРєСЂРµРґРёС‚Р°С†РёРѕРЅРЅР°СЏ СЌРєСЃРїРµСЂС‚РёР·Р°
 * СЃС‚Р°С‚СѓСЃ Р¤РѕСЂРјРёСЂРѕРІР°РЅРёРµ СЌРєСЃРїРµСЂС‚РЅРѕР№ РіСЂСѓРїРїС‹ 968BE98A-A5B9-404D-B97B-233784AF7AA0
 *
 * РµСЃР»Рё СЌРєСЃРїРµСЂС‚РёР·Р° РЅРµ СЃРѕР·РґР°РЅР°, С‚Рѕ РєРЅРѕРїРєСѓ РќР°Р·РЅР°С‡РёС‚СЊ СЌРєСЃРїРµСЂС‚РёР·Сѓ
 * РµСЃР»Рё СЃРѕР·РґР°РЅР°, С‚Рѕ РєРЅРѕРїРєРё РџРѕРґРіРѕС‚РѕРІРєР° РїСЂРёРєР°Р·Р°, РџРѕРґР±РѕСЂ СЌРєСЃРїРµСЂС‚РѕРІ
 *
 * СЃР»РёР·Р·Р°РЅРѕ СЃ getButtonByStep
 *
 * @returns {undefined}
 */
function getButtonByExpertise() {
    $("#popup_win #but_wrap_child_right #btActions-block li").each(function(indx, element){
        if(!jQuery(element).hasClass('btn_app_card_edit_default')){
            jQuery(element).remove();
        }
    });
    $('.btn_expertise_class').remove();
    var idAppl=jQuery("#id_application_val_id").val();
    $.ajax({
        url: '/eo/declaration/get-button-by-expertise/'+idAppl,
        async:false,
        timeout:2000,
        success: function (answer) {
            $("#popup_win #but_wrap_child_right #btActions-block").append(answer);
        }
    });
}
/*
 * РїРѕРґРєР»СЋС‡РµРЅРёРµ fileupload РґР»СЏ СЂР°Р·Р»РёС‡РЅС‹С… Р·Р°РіСЂСѓР·РѕРє
 */
/*
function activateFileUpload(url_out,class_elem){
    $(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = url_out;
    $(class_elem).fileupload({
        url: url+'/'+$("#id_application_val").val(),
        dataType: 'json',
        done: function (e, data) {
            var elem = this;
            $.each(data.result.files, function (index, file) {
                if(file.errors.length<1){
                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div id="'+file.fileId+'"><span class="MultiFile-remove" onclick="window_delete_application_file(\''+file.fileId+'\', 1)">x</span><a target="_blank" href="'+file.filePath+'" download="'+file.fileName+'">'+file.fileName+'</a></div>')     ;
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').width("0%");
                    $('.FileListWrap.'+$(elem).attr("codeType")).parent(".containerWrapFile").css('border','none');
                }else{
                    var error_string = data.result.files[0].errors.join(',');

                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div class="wrap_file_answer"><a class="MultiFile-remove remove_answer" >x</a> <span style="color:red;" class="red">'+data.originalFiles[0].name+'(РћС€РёР±РєРё РїСЂРё Р·Р°РіСЂСѓР·РєРµ С„Р°Р№Р»Р°: '+error_string+' )</span></div>');
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').css('width','0%');
                }
                 $('.progress .progress-bar.'+$(elem).attr("codeType")).parent('.progress').hide();
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').parent('.progress').show();
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
}*/
function recallApplication(id){
    var dContent = $(
        '<div class="dialog-recall-application">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "РћС‚Р·С‹РІ Р·Р°СЏРІР»РµРЅРёСЏ",
        width: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-recall-application").remove();
        },
        buttons: [
                               {
                    text: "РЎРѕС…СЂР°РЅРёС‚СЊ",
                    "class": 'right saveButExpert',
                    click: function() {
                       sendRecallForm(id)
                    }
                },
                {
                    text: "Р—Р°РєСЂС‹С‚СЊ",
                    "class": 'right closeDialogRecall',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
           ],
        open: function() {
            //РћС‚СЃС‹Р»Р°РµРј Р·Р°РїСЂРѕСЃ РЅР° РїРѕР»СѓС‡РµРЅРёРµ РёРЅС„Рѕ РїРѕ Р·Р°СЏРІР»РµРЅРёСЋ
            $.ajax({
                dataType: "html",

                url: '/eo/declaration/recall-application/' + id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Р’РµС€Р°РµРј РѕР±СЂР°Р±РѕС‚С‡РёРєРё

                        //РЈСЃС‚Р°РЅР°РІР»РёРІР°РµРј РІС‹СЃРѕС‚Сѓ
                        $(".dialog-content",dContent).height($(dContent).height()-20);
                        $(".input-date").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd.mm.yy"
                        });
                    });
                }
            });
        }
    });

}
function sendRecallForm(id){
    var form = document.forms.RecallForm;

    var formData = new FormData(form);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/eo/declaration/recall-application/' + id);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                data = $.parseJSON(xhr.responseText);
                if (typeof data.fileRecall != 'undefined') {
                    var file_errors = data.fileRecall;
                    for(var file_error in file_errors){
                       $("#error_fileRecall").append(file_errors[file_error]);
                    }

                }
                if (typeof data.numberRecall != 'undefined') {
                    $("#error_numberRecall").html(data.numberRecall);
                }
                console.log(data);
                if(data == '1'){
                   $('.btn-warning').click();
                   $(".closeDialogRecall").click();;
                }
            }
        }
    };

    xhr.send(formData);
}