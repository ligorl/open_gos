function Captcha() {
    this.url = null;
    this.captcha = null;
    this.disabledSubmit = true,
            this.reset = function () {
                var self = this;
                if (!self.url) {
                    throw 'Url is undefined';
                }
                self.captcha.empty();
                self.captcha.QapTcha({
                    disabledSubmit: self.disabledSubmit,
                    url: self.url
                });
            };

    this.setUrl = function (url) {
        this.url = url;
    };
    this.setCaptcha = function (captcha) {
        this.captcha = captcha;
    };
    this.setDisabledSubmit = function (disabledSubmit) {
        this.disabledSubmit = disabledSubmit;
    };
}
function SearchEngine() {
    this.currentPage = 1;
    this.orderColumn = null;
    this.orderType = 'ASC';
    this.formData = null;
    this.onSuccess = null;
    this.onError = null;
    this.onStart = null;
    this.onEnd = null;
    this.url = null;
    this.dataType = 'html';

    this.processSearch = function (currentPage) {
        var self = this;

        if (!self.url) {
            throw 'Url is undefined';
        }

        self.currentPage = currentPage;
        self.onStart();
        var order = '',
            page  = '';
        if (self.orderColumn !== null) {
            order = '&order=' + self.orderColumn + '&direction=' + self.orderType;
        }
        if(currentPage !== undefined){
            page = '?page=' + self.currentPage + order;
        }
        $.ajax({
            url: self.url + page,
            type: 'POST',
            data: self.formData,
            dataType: self.dataType,
            success: function (response) {
                self.onSuccess(response);
            },
            error: function (error) {
                self.onError(error);
            }
        }).always(function () {
            self.onEnd();
        });
    };

    this.setFormData = function (formData) {
        this.formData = formData;
    };
    this.setDataType = function (dataType) {
        this.dataType = dataType;
    };
    this.setOrderColumn = function (orderColumn) {
        this.orderColumn = orderColumn;
    };
    this.setOrderType = function (orderType) {
        this.orderType = orderType;
    };
    this.setUrl = function (url) {
        this.url = url;
    };
    this.setOnSuccess = function (onSuccess) {
        this.onSuccess = onSuccess;
    };
    this.setOnError = function (onError) {
        this.onError = onError;
    };
    this.setOnStart = function (onStart) {
        this.onStart = onStart;
    };
    this.setOnEnd = function (onEnd) {
        this.onEnd = onEnd;
    };
}
;


function Report() {
    this.url = null;
    this.onStart = null;
    this.onEnd = null;
    this.onSuccess = null;
    this.onError = null;
    this.form = null;

    this.sendReport = function () {
        var self = this;
        var formData = self.form.serialize();
        $.ajax({
            url: self.url,
            data: formData,
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                self.onSuccess(response);
            },
            error: function (error) {
                self.onError(error);
            }
        }).always(function () {
            self.onEnd();
        });
    };
    this.setForm = function (form) {
        this.form = form;
    };
    this.setUrl = function (url) {
        this.url = url;
    };
    this.setOnSuccess = function (onSuccess) {
        this.onSuccess = onSuccess;
    };
    this.setOnError = function (onError) {
        this.onError = onError;
    };
    this.setOnStart = function (onStart) {
        this.onStart = onStart;
    };
    this.setOnEnd = function (onEnd) {
        this.onEnd = onEnd;
    };
}
    /**
     * Сравнение двух программ
     * @param jQuery a
     * @param jQuery b
     * @returns boolean
     */
    var compareProgramm = function (a, b) {
        var comparisionSelectors = [
            '[data-column="UGSCode"]',
            '[data-column="UGSName"]',
            '[data-column="LevelName"]'
        ];
        for (var key in comparisionSelectors){
            var comparision = comparisionSelectors[key];
            if (a.find(comparision).text() !== b.find(comparision).text()) {
                return false;
            }
        }
        return true;
    };
    
    /**
     * Объединение ячеек
     * @param jQuery a first
     * @param jQuery b current
     * @param Boolean c если undefined то удаляем ячейку
     */
    var mergeRows = function(a,b,c){
        var mergeColumns = [
            '[data-column="UGSCode"]',
            '[data-column="UGSName"]'
            //'[data-column="LevelName"]'
        ];
        for(var key in mergeColumns){
            var column = mergeColumns[key];
            var tableColumn = a.find(column);
            var rowSpan = tableColumn.attr('rowspan') | 0;
            
            tableColumn.attr('rowspan',rowSpan + 1);
            if(!c){
                b.find(column).remove();
            }
        }
    };
    /**
     * Объединяем ячейки программ
     */
    function mergeEqualCells(programmRows){
        var first = $(programmRows[0]);
        for (var i = 1; i < programmRows.length; i++) {
            var current = $(programmRows[i]);
            if (compareProgramm(first, current)) {
                mergeRows(first, current);
                if(i === programmRows.length - 1){
                    mergeRows(first, current,true);
                }
            } else {
                mergeRows(first, current,true);
                first = $(programmRows[i]);
            }
        }
    }
    