/************************************************************************
 *************************************************************************
 @Name :       	QapTcha - jQuery Plugin
 @Revison :    	4.2
 @Date : 		06/09/2012  - dd/mm/YYYY
 @Author:     	 ALPIXEL Agency - (www.myjqueryplugins.com - www.alpixel.fr)
 @License :		 Open Source - MIT License : http://www.opensource.org/licenses/mit-license.php
 
 **************************************************************************
 *************************************************************************/
jQuery.QapTcha = {
    build: function (options)
    {
        var defaults = {
            disabledSubmit: true,
            autoRevert: true,
            url: undefined
        };

        if (this.length > 0)
            return jQuery(this).each(function (i) {
                /** Vars **/
                var
                        opts = $.extend(defaults, options),
                        $this = $(this),
                        form = $('form').has($this),
                        bgSlider = jQuery('<div>', {'class': 'bgSlider form-control'}),
                        Slider = jQuery('<div>', {'class': 'Slider'}),
                        inputQapTcha = jQuery('<input>', {name: 'captcha', type: 'hidden'});
                /** Disabled submit button **/
                reset();

                /** Construct DOM **/
                bgSlider.appendTo($this);
                inputQapTcha.appendTo($this);
                Slider.appendTo(bgSlider);
                $this.show();
                Slider.draggable({
                    revert: function () {
                        if (parseInt(Slider.css("left")) > (bgSlider.width() - Slider.width() - 1))
                            return false;
                        else
                            return true;
                    },
                    containment: bgSlider,
                    axis: 'x',
                    stop: function (event, ui) {
                        if (ui.position.left > (bgSlider.width() - Slider.width() - 1))
                        {
                            $.post(opts.url, {
                                action: 'captcha',
                                value: inputQapTcha.attr('value')
                            }, success, 'json');
                        }
                    }
                });

                function reset() {
                    inputQapTcha.attr('value', generatePass(32));
                    if (opts.disabledSubmit) {
                        form.find('input[type=\'submit\']').prop('disabled', true);
                    }
                    Slider.animate({
                        left: 0
                    });
                }

                function success(data) {
                    if (data.status === 'OK') {
                        form.find('input[type=\'submit\']').prop('disabled', false);
                    } else {
                        reset();
                    }
                }

                function generatePass(nb) {
                    var chars = 'azertyupqsdfghjkmwxcvbn23456789AZERTYUPQSDFGHJKMWXCVBN';
                    var pass = '';
                    for (i = 0; i < nb; i++) {
                        var wpos = Math.round(Math.random() * chars.length);
                        pass += chars.substring(wpos, wpos + 1);
                    }
                    return pass;
                }
            });
    }
};
jQuery.fn.QapTcha = jQuery.QapTcha.build;