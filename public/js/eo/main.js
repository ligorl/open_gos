var isHashChangerClick = false;
var intervalIdentificator = null;

$(document).ready(function(){

    $('body').on('click','.show-hide-case',function(){
        if( $('.case').css('display') == 'none'){
            $('.case').show();
            $('.show-hide-case').text('Скрыть склонения');
        }else{
            $('.case').hide();
            $('.show-hide-case').text('Показать склонения');
        }
    });

    if( 'undefined' != typeof(flagLastMainLoadjsEo) ){
        return 0;
    }
    flagLastMainLoadjsEo = true;
    console.info('запускаем обработчики загрузки /js/eo/main.js');

    //Вешаем обработчик для кнопок шагов
    $(".btn",".steps_div").click(function(){
        //меняем внешний вид
        $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
        $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
    });

    bindIndexEvents();

    $(window).unbind('hashchange');
    $(window).bind('hashchange', function(){
        if(isHashChangerClick){
            isHashChangerClick = false;
            return;
        }
        if(window.location.hash){
            $("a[href^='"+window.location.hash+"']").parent().click();
        }else{
            window.location.hash = $(".btn-lg").attr('event');;
            var event=$(".btn>a",".steps_div").first().attr('event');
            var controller=$(".btn>a",".steps_div").first().attr('controller');
            indexEventsAjaxHandler($(".btn>a",".steps_div").first(), event, controller);
        }
    });
    if(window.location.hash){
        $("a[href^='"+window.location.hash+"']").parent().click();
    }else{
        window.location.hash = $(".btn-lg").attr('event');
        var event=$(".btn>a",".steps_div").first().attr('event');
        var controller=$(".btn>a",".steps_div").first().attr('controller');
        indexEventsAjaxHandler($(".btn>a",".steps_div").first(), event,controller);
    }


    $(window).resize(function(){
        resizeContent($(".container .viewzone"));
        resizeContent($(".content_td .content"));
    });

    console.log('Обработчик скрытия левого меню /public/js/eo/main.js');
    $(".steps_td").on('click','.toggleButton',function(){
        $('body').toggleClass('icons');
        $('#head').toggleClass('icons');
        $('.route .steps_td').toggleClass('icons');
        $('.steps_td .nav li').toggleClass('icons');
        $('.steps_td .nav li a').toggleClass('icons');
        $('.steps_td .nav li a span.num').toggleClass('icons');
        $(this).toggleClass('icons');
        if($(this).hasClass('icons')){
            setCookie('menu','icons');
        }else{
            setCookie('menu','');
        }
        /*
        console.log('лицензии - скроем/покажем наименова пунктов меню ');
        var icons = getCookie('menu');
        if ( 'icons' ===  icons ){
            $('body').removeClass('icons');
            $('#head').removeClass('icons');
            $('.route .steps_td').removeClass('icons');
            $('.steps_td .nav li').removeClass('icons');
            $('.steps_td .nav li a').removeClass('icons');
            $('.steps_td .nav li a span.num').removeClass('icons');
            $(this).removeClass('icons');
            setCookie('menu','');
        } else {
            $('body').addClass('icons');
            $('#head').addClass('icons');
            $('.route .steps_td').addClass('icons');
            $('.steps_td .nav li').addClass('icons');
            $('.steps_td .nav li a').addClass('icons');
            $('.steps_td .nav li a span.num').addClass('icons');
            $(this).addClass('icons');
            setCookie('menu','icons');

        }*/
    });

});



function showPop(url,showWaitWindow){
    if(showWaitWindow == '1')
        $(".loading-overlay").show();
    $.ajax({
        type: 'POST',
        url: url,
        success: function(answer) {
           $("#popupWindow").html(answer);
           $("#popupWindow").show();
        }
    });
}


function bindIndexEvents() {
    //проблема - этот обработчик вешается много раз, а потом запускается множество раз
    // если его сбрасывать, то он убивается и во время обработки, решиб пооставить влаг
    if( 'undefined' != typeof(bindIndexFlagjsEoMain) ){
        return 0;
    }
    bindIndexFlagjsEoMain = true;
    $('.btn').unbind('click');
    //Вешаем обработчик на переключение вкладок
    $('.btn').click(function() {
        isHashChangerClick = true;
        var event = $(this).attr('event');
        var _this = this;
        var controller = $(this).attr('controller');
        if (event) {
            $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
            $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
            indexEventsAjaxHandler(_this, event,controller);
        }
    });

    changeAffilatesSelect();
    changeLicenseSupplementsSelect();
    changeCertificateSupplementsSelect();
    changeCertificateSelect();
    btPubAccrHandlers();
    btSaveHandlers();
    btSendErrorHandler();
    searchHandlers();
}

function indexEventsAjaxHandler(_this, event ,keep_selector) {
    console.log('обработчик тыка по елементам меню слева 2');
    if (!event) return false;
        var destination_link = '/eo/'+keep_selector+'/' + event;

    $.ajax({
        type: 'POST',
        url: destination_link,
        beforeSend: function(){
            //ресайзимблок с контентом
			resizeContent($(".content_td .content"));
			$(".content_td .content").html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
            //$(".content_td .content").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);            //

            //если это не вкладка с общими сведениями
            //то прячем кнопку сохранения
            $(".button, .sumField",".buttons-container").hide();
            $(".btSendError",".buttons-container").show();

            $(".btSave",".buttons-container").removeClass("isAffiliates");
            if(event == "info"){
                $(".btSave",".buttons-container").show();
                $(".btPubAccr",".buttons-container").show();
            }
            if(event == "affiliates"){
                $(".btSave",".buttons-container").addClass("isAffiliates");
                $(".btSave",".buttons-container").show();
            }
            if(event == "calculator"){
                $(".btSendError",".buttons-container").hide();
                $(".sumField",".buttons-container").show();
                $(".btClearAll",".buttons-container").show();
                $(".btFromApplication",".buttons-container").show();
            }
            if(keep_selector == "declaration"){
                getAllCount();
            }
            
            if( event == "excerpt/certificates/1" ){
                        $('body').off('click','.page-excert');
                        $('body').on('click','.page-excert',function(){
                            var num = $(this).html();
                            excerpt.certificatePage(num);
                        });
                        $('body').off('click', '.button.excerpt-this');
                        $('body').on('click', '.button.excerpt-this' ,function(){
                            excerpt.excerptThis();
                        });
                        event = 'excerpt';
            }
            
        },
        success: function(answer) {
            $(".content_td .content .loading-img").fadeOut(500, function(){
                answer = $(answer)
                $(".content_td .content").html("").append(answer);
                resizeContent($(".viewzone", answer));

                $(".date-field",".viewzone").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy"
                });
                $(".date-icon",".viewzone").click(function(){
                    $(this).parent().find("input").focus()
                })

                //Если вкладка с калькулятором, инициируем калькулятор
                if(event=="calculator"){
                    Calculator.init();
                    //прячем поиск
                    $("#filter").hide();
                } else {
                    $("#filter").show();
                }
                //дергним перерисовку чтоб ровно отрисовало
                if( 'function' == typeof( $(window).resize )){
                    $(window).resize();
                }
            })
        },
        error:function(e){
            //сбросим сообщения
            useTopHoverTitle("",1);
            useTopHoverTitleError("Ошибка связи");
            $(".content_td .content .loading-img").fadeOut(500, function(){

            });
            console.error('ошибка связи при переходе по меню');
            console.log(e);
            console.log(e.responseText);
        }
    });
}

function resizeContent(content){
    var wHeight = $(window).height();
    var delta = 180;
    if($(content).attr("data-heightdelta"))
        delta -= parseInt($(content).attr("data-heightdelta"));

    var contentHeight = wHeight - delta;
    $(content).height(contentHeight);
}

function changeAffilatesSelect(){
    $(".content_td .content").delegate("#affilates","change",function(){
        var affId = $(this).val();
        $.ajax({
            type: 'POST',
            url: "/eo/index/get-affilate-info/"+affId,
            beforeSend: function(){
                $(".content_td .content .viewzone").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);
            },
            success: function(answer) {
                $(".content_td .content .viewzone .loading-overlay").fadeOut(500, function(){
                    answer = $(answer)
                    $(".content_td .content .viewzone").html("").append(answer);
                })
            }
        });
    });
}

function changeLicenseSupplementsSelect(){
    $(".content_td .content").delegate("#license-supplements","change",function(){
        var suppId = $(this).val();
        $.ajax({
            type: 'POST',
            url: "/eo/index/get-licensesupplement-info/"+suppId,
            beforeSend: function(){
                $(".content_td .content tbody.supplements").html("").append(
                    $("<td colspan='4'><div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div></td>")
                ).fadeIn(500);
            },
            success: function(answer) {
                $('.lic-state .status').text($("#license-supplements option:selected").attr('status'));
                var state = $("<div>").html(answer).find(".lic_supp_state").detach();
                $(".lic-state").html(state.find("td").html())
                $(".content_td .content tbody.supplements .loading-overlay").fadeOut(500, function(){
                    answer = $(answer)
                    $(".content_td .content tbody.supplements").html("").append(answer);
                })
            }
        });
    });
}

function changeCertificateSupplementsSelect(){
    $(".content_td .content").delegate("#certificate-supplements","change",function(){
        var suppId = $(this).val();
        $.ajax({
            type: 'POST',
            url: "/eo/index/get-certificatesupplement-info/"+suppId,
            beforeSend: function(){
                $(".content_td .content tbody.supplements").html("").append(
                    $("<td colspan='5'><div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div></td>")
                ).fadeIn(500);
            },
            success: function(answer) {
                var state = $("<div>").html(answer).find(".cert_supp_state").detach();
                $(".supp-state").html(state.find("td").html())
                $(".content_td .content tbody.supplements .loading-overlay").fadeOut(500, function(){
                    answer = $(answer);
                    $(".content_td .content tbody.supplements").html("").append(answer);
                })
            }
        });
    });
}

function changeCertificateSelect(){
    $(".content_td .content").delegate("#certificates","change",function(){
        var certId = $(this).val();
        $.ajax({
            type: 'POST',
            url: "/eo/index/get-certificate-info/"+certId,
            beforeSend: function(){
                $(".content_td .content tbody.certificate").html("").append(
                    $("<td colspan='2'><div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div></td>")
                ).fadeIn(500);
            },
            success: function(answer) {
                $(".content_td .content tbody.certificate .loading-overlay").fadeOut(500, function(){
                    answer = $(answer)
                    $(".content_td .content tbody.certificate").html("").append(answer);
                })
            }
        });
    });
}

/**
 * Обработчики связанные с добавлением новых блоков для ввода общественной
 * аккредитации
 */
function btPubAccrHandlers(){
    if( 'undefined' == typeof($().tipsy) ){
        return 0;
    }
    $('.btPubAccr',".buttons-container").tipsy({
        html: true,
        trigger: "manual",
        gravity: "sw",
        title: function() {
            return ""+
                '<div class="menu-block">'+
                    '<div class="menu-item" data-type="addPubAccr">Добавить общественную аккредитацию</div>'+
                    '<div class="menu-item" data-type="addProfPubAccr">Добавить профессионально-общественную аккредитацию</div>'+
                    '<div class="menu-item" data-type="addIntPubAccr">Добавить международную аккредитацию</div>'+
                '</div>'
        }
    });

    var flag = true;
    $(".btPubAccr").click(function(e) {
        if(flag){
            $('.btPubAccr',".buttons-container").tipsy("show");

            if(e.stopPropagation) e.stopPropagation();
            if(e.preventDefault) e.preventDefault();
            flag = false;
        }else{
            $('.btPubAccr',".buttons-container").tipsy("hide");
            flag = true;
        }
    });

    $("body").click(function(){
        $('.btPubAccr',".buttons-container").tipsy("hide");
        flag = true;
    });

    //обработчик добавления нового блока публичной аккредитации
    $("body").delegate(".tipsy .menu-item", "click", function(){
        console.log( 'обработчик добавления нового блока публичной аккредитации 1' );
        var dataType = $(this).attr("data-type");
        console.log(dataType );
        var tbodyClass = "";
        switch(dataType){
            case "addPubAccr":
                tbodyClass = "PubAccr";
                break;
            case "addProfPubAccr":
                tbodyClass = "ProfPubAccr";
                break;
            case "addIntPubAccr":
                tbodyClass = "IntPubAccr";
                break;
        }
        //Получаем набор блоков нужного нам класса
        var tbodyBlocks = $("."+tbodyClass,".viewzone");
        //клонируем первый
        var clonedBlock = tbodyBlocks.first().clone();
        clonedBlock.removeClass("firstTbody");
        $("input, textarea",clonedBlock).val("");
        //добавляем клонируемый блок в список
        tbodyBlocks.last().after(clonedBlock);
        //вешаем обработчик для выбора даты

        $(".date-field", clonedBlock).removeClass("hasDatepicker");
        $(".date-field", clonedBlock).removeAttr("id");
        $(".date-field", clonedBlock).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy"
        });
        $(".date-icon", clonedBlock).click(function(){
            $(this).parent().find("input").focus();
        })
        //ставим фокус на новый блок
        $("textarea",clonedBlock).focus();
    });
}

/**
 * Обработчики сохранения данных по организации
 */
function btSaveHandlers(){
    // Клик по кнопке сохранения
    $(".btSave",".buttons-container").click(function(){
        var button = $(this);
        //Флаг, если это филиалы
        var isAffilates = $(this).hasClass("isAffiliates");

        //Удаляем пустые блоки для ввобда общественных аккредитаций
        //Если это не филиалы

        if(!isAffilates){
            removeEmptyBlocks("PubAccr");
            removeEmptyBlocks("ProfPubAccr");
            removeEmptyBlocks("IntPubAccr");
        }

        //Собираем данные с полей
        var fieldsData = {};
        $(".data-field",".viewzone").each(function(i, item){
            fieldsData[$(item).attr("name")] = $(item).val();
        });

        //собираем данные с общественных аккредитаций
        if(!isAffilates) {
            fieldsData["PubAccr"] = getAccrData("PubAccr");
            fieldsData["ProfPubAccr"] = getAccrData("ProfPubAccr");
            fieldsData["IntPubAccr"] = getAccrData("IntPubAccr");
        }

        button.prop("disabled",true);
        button.addClass("disabled");

        var sendData = {
            fieldsData : fieldsData
        };
        if(isAffilates)
            sendData.affId = $("#affilates").val();

        $.post(
            "/eo/index/save-org-data",
            sendData,
            function(data){
                button.prop("disabled",false);
                button.removeClass("disabled");
                if (data==='done'){
                    useTopHoverTitle("Данные записаны");
                } else {
                    useTopHoverTitleError("Возникла ошибка при сохранении");
                }
            }
        )
    });

    //функция, что удаляет пустые блоки для ввода данных
    //по общественным аккредитациям
    var removeEmptyBlocks = function(className){
        //Получаем набор блоков нужного нам класса
        var tbodyBlocks = $("."+className,".viewzone");
        //Проходим по всем найденным блокам
        tbodyBlocks.each(function(i,item){
            //Проверяем если это первый блок
            //то пропускаем
            if($(item).hasClass("firstTbody"))
                return true;
            //Проверяем, если значения полей пустые
            if( $("textarea",item).val() == "" && $("input",item).val()=="")
                $(item).remove();
        })
    }

    //Функция проходит по всем блокам и собирает массив данных
    //для возврата
    var getAccrData = function(className){
        var returnArray = [];
        //Получаем набор блоков нужного нам класса
        var tbodyBlocks = $("."+className,".viewzone");
        //Проходим по всем найденным блокам
        tbodyBlocks.each(function(i,item){
            returnArray.push({
                organization: $.trim($("textarea",item).val()),
                date: $.trim($("input.date-field",item).val()),
                requisite: $.trim($("input.requisite-field",item).val())
            })
        });
        return returnArray;
    }
}

function searchHandlers(){
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function( elem ) {
            var strText = $(elem).text()//.toLowerCase();
            var valText = $(elem).val()//.toLowerCase();
            if(
                strText.indexOf(arg)//.toLowerCase()) >= 0
                || valText.indexOf(arg)//.toLowerCase()) >= 0
                )
                return true;
            else
                return false;
        };
    });

    //Обработчик ввода текста в поиск
    $('#filter').keyup(function(e) {
        // Добавил проверку что бы убрать обработку для вкладки "Заявления"
        if($(".nav li:first").hasClass('active')){
            if(e.which == 27){
                clearTimeout($.data(this, 'timer'));
                clearSearch();
                return;
            }
            clearTimeout($.data(this, 'timer'));
            var wait = setTimeout(search, 500);
            $(this).data('timer', wait);
        }
    });

    function search(){
        var searchVal = $('#filter').val();
        $("tr",".viewzone").hide();
        $(".left-title:contains("+searchVal+")",".viewzone").parents("tr").show();
        $(".right-value:contains("+searchVal+")",".viewzone").parents("tr").show();
        $("textarea:contains("+searchVal+")",".viewzone").parents("tr").show();
        $("input:contains("+searchVal+")",".viewzone").parents("tr").show();
    }

    function clearSearch(){
        $('#filter').val("");
        $("tr",".viewzone").show();
    }
}

function btSendErrorHandler(){
    //функция обновляет капчу
    var refreshCaptcha = function(content){
        //Отправляем запрос на получение идентификатора капчи
        $.ajax({
            type: 'GET',
            url: "/captcha",
            dataType: "text",
            success: function(answer) {
                var id = answer;
                //меняем ссылку на картинку капчи
                $(".captcha-img" ,content).attr("src","/captcha_dir/"+id+".png");
                //меняем значение hidden input
                $(".captcha-id", content).val(id);
                //посылаем запрос на удаление картинки капчи
                $.ajax({
                    type: 'GET',
                    url: "/captcha/index/update/"+id
                })
            }
        });
    }

    $(".btSendError",".buttons-container").click(function(){
        $('body').append('<div class="ui-widget-overlay ui-front" style="z-index: 101;"></div>');
        var content = $("#dialog-sendError");
        content.dialog({
            title: "Сообщение об ошибке",
            width: 600,
            height: 470,
            open: function(){
                $("body .loading-overlay").show();
                var self = this;
                refreshCaptcha(content);
                //Создаем таймер на 5 мин, что будет обновлять капчу каждые 5 мин
                intervalIdentificator = setInterval(function(){
                    refreshCaptcha(content);
                },300000);
                //Вешаем обработчик на кнопку обновить, чтобы обновлять капчу вручную
                $(".refresh-span", content).unbind("click").click(function(){
                    clearInterval(intervalIdentificator);
                    refreshCaptcha(content);
                    intervalIdentificator = setInterval(function(){
                        refreshCaptcha(content);
                    },300000);
                });
                //Вешаем обработчик на кнопку Отмена
                $(".btCancel", content).unbind("click").click(function(){
                    $( self ).dialog( "close" );
                    $(".ui-front").remove();
                    $("body .loading-overlay").fadeOut(500);
                });
                //инициализируем форму
                $( "#errorForm" ).ajaxForm({
                    success: function(data){
                        $("input, textarea",content).removeClass("error-field");
                        //Если есть ошибки
                        if(data["errors"]){
                            for(var id in data["errors"]){
                                $("."+id,content).addClass("error-field");
                                useTopHoverTitleError("Заполните обязательные поля");
                            }
                            //обновляем капчу
                            refreshCaptcha(content);
                        } else {
                            //закрываем диалог,
                            $( self ).dialog( "close" );
                            //Чистим значения диалога
                            $("input", content).val("");
                            $("textarea", content).val("");

                            var parentFile = $(".fFile", content).parent();
                            parentFile.html("").append($('<input type="file" class="field fFile" name="data[fFile]" accept="image/jpeg,image/png,image/gif" style="color: #2a6496;">'));

                            $("body .loading-overlay").fadeOut(500);
                            //выводим сообщение, мол все хорошо
                            useTopHoverTitle("Спасибо за ваше сообщение. Сообщение успешно отправлено.")
                        }
                        $(".btSend", content).attr("sending","false");
                    },
                    dataType: "json"
                });
                //Вешаем обработчик на кнопку Отправить
                $(".btSend", content).unbind("click").click(function(){
                    if($(this).attr("sending")=="true")
                        return;
                    $(this).attr("sending","true");

                    $(".field",content).removeClass("error-field");
                    $( "#errorForm" ).submit();
                });
            },
            close: function(){
                clearInterval(intervalIdentificator);
                $(".ui-front").remove();
                $("body .loading-overlay").fadeOut(500);
            }
        });
    });
}

function backShow(){
    var url;
    url='/eo/declaration/back/'+$("#id_application_val").val();
    $.ajax({
        type: 'POST',
        url: url,
        success: function (answer) {
            if (answer !== 'available') {
                $(".loading-overlay").show();
                $('body').append('<div id="back_win" class="back_win_my" style="z-index:100001"></div>');
                $(".back_win_my").html(answer);
                $(".back_win_my").show();
                $(".back_win_my").delegate("#back_close", "click", function () {
                    $(".back_win_my").remove();
                    $(".loading-overlay").fadeOut(250);
                });
            }else{
                useTopHoverTitleError('Заявления об отзыве уже отправлено');
            }
        },
        error:function(){
            useTopHoverTitleError('Ошибка связи');
        }
    });
}

//изменяем ширины полей шапки таблички
function tableProgramsByUgsHeadering(){
        var header=jQuery('#edu_programs thead').html();
        jQuery('.edu_programs_table_header thead').html('');
        jQuery('.edu_programs_table_header thead').append(header);

        jQuery('.edu_programs_table_header table').width( jQuery('#edu_programs').width());

        var header2 = jQuery('.edu_programs_table_header thead  th');
        //console.log(header2);
        jQuery('#edu_programs thead th').each(function(i,el){
            var h = jQuery(el).height();
           jQuery(header2[i]).height(h);

            var w = jQuery(el).innerWidth();
            w=Math.ceil(w);

            w = jQuery(el).width();
           jQuery(header2[i]).width(w);
           jQuery(el).width(w);

           var fs=jQuery(el).css('font-size');
           jQuery(header2[i]).css('font-size',fs);

        });
    }

 /**
 * В специальной верхней зоне выводит текст из параметра text,
 * и прячет его через время указанное в милисекундах в параметре time
 *
 * @param string text
 * @param int time
 */
function useTopHoverTitle(text, time) {
    if(typeof(time) == 'undefined'){
        time = 2000;
    }
    if(typeof(time) == 'string'){
        time = parseInt(time);
    }
    if( isNaN(time) ){
        time = 2000;
    }
    if( !$("#hover-title").length ){
        jQuery('body').prepend(' <div id="hover-title"></div>');
    }
    $("#hover-title").text(text);
    if( $("#hover-title:hidden") ){
        //чтоб не блымало
        $("#hover-title").fadeIn("fast");
    }
    if( 'undefined' != typeof(hoverTimerId)){
        //если уже запущен таймер, аннулируем его, чтоб не пропадало
        clearTimeout(hoverTimerId);
    }
    if( time > 0 ){
        hoverTimerId = setTimeout(function () {
            $("#hover-title").fadeOut('fast');
        }, time);
    }
}

/**
 * Вешаем событие на клик по всплывающему окну.
 */
function eventTopHoverClick() {
    $("body").undelegate("#hover-title", "click");
    $("body").delegate("#hover-title", "click", function () {
        $(this).toggle();
    });
}



function activateFileUpload(url_out,class_elem){
    var ret, buttons = {};
    $(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = url_out;
    $(class_elem).fileupload({
        url: url+'/'+$("#id_application_val").val(),
        dataType: 'json',
        done: function (e, data) {
            var elem = this;

			if(typeof(data.result.error) == "string"){
				useTopHoverTitle(data.result.error, 10000);
                console.error( url );
                console.error( data.result.error );
				$('.progress .progress-bar.'+$(elem).attr("codeType")+'').css('width','0%');
				$('.progress .progress-bar.'+$(elem).attr("codeType")).parent('.progress').hide();
				return false;
			}

            $.each(data.result.files, function (index, file) {

                if(file.errors.length < 1){
                    buttons.del = '<span class="MultiFile-remove" onclick="window_delete_application_file(\''+file.fileId+'\', 1)">x</span>';
                    buttons.valid = '<span title="Выполняется проверка подписи файла" class="spinner_verify_sign"></span>';
                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div id="'+file.fileId+'">'+ buttons.del +'<a target="_blank" href="'+file.filePath+'" download="'+file.fileName+'">'+file.fileName+'</a></div>')     ;
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').width("0%");
                    $('.FileListWrap.'+$(elem).attr("codeType")).parent(".containerWrapFile").css('border','none');
                    //Добавим перегрузка вкладки
                    handleResponse(4);
                    ret = 'ok'
                }else{
                    var error_string = data.result.files[0].errors.join(',');

                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div class="wrap_file_answer"><a class="MultiFile-remove remove_answer" >x</a> <span style="color:red;" class="red">'
                            +data.originalFiles[0].name
                            +'(Ошибки при загрузке файла: '
                            +error_string
                            +' )</span></div>');
                    console.error( url );
                    console.error( error_string );
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').css('width','0%');
                }
                 $('.progress .progress-bar.'+$(elem).attr("codeType")).parent('.progress').hide();

                 ret = 'error';
            });

        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').parent('.progress').show();
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
return ret;
}



function activateFileUploadSign( url_out , class_elem , option ){
    var ret, buttons = {};
    if( 'undefined' == typeof(option) ){
        option = {};
    }
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = url_out;
        var addGetSting = '';
        if( 'string' == typeof( option.addGetString ) ){
            addGetSting = '?'+option.addGetString;
        }
        $(class_elem).fileupload({
            url: url+'/'+$("#id_application_val").val()+addGetSting,
            dataType: 'json',
            send: function (e, data) {
                console.log( 'Начинается загрузка заявления' );
                useTopHoverTitle( 'Начинается загрузка заявления' , 100000 );
            },
            done: function (e, data) {
                var elem = this;
                console.info( 'рон - отсылка файлов подписаного файла на сервер - выполнено' );
                console.log( data );
                useTopHoverTitle( 'Заявление загружено'  );
                if(typeof(data.result.error) == "string"){
                        useTopHoverTitleError(data.result.error);
                        $('.progress .progress-bar.'+$(elem).attr("codeType")+'').css('width','0%');
                        $('.progress .progress-bar.'+$(elem).attr("codeType")).parent('.progress').hide();
                        console.error( url );
                        console.error( data.result.error );
                        return false;
                }

                if( typeof(data.result.status) == "string" && 'Uploading' == data.result.status ){
                    useTopHoverTitle( 'Заявление загружено'  );
                    console.log( 'Заявление загружено' );
                    useTopHoverTitle( 'Проведем распоковку заявления'  );
                    //console.log( 'Проведем распоковку заявления' );

                    var result = ''

                    $.ajax({
                        type: 'POST',
                        url: "/eo/declaration/save-sign-file/"+certId+'?step=signFileCount',
                        //async: false,
                        dataType: 'json',

                        success: function(answer) {
                            console.log( 'получен перечень файлов для выдергивания' );
                            console.log( answer );
                            if( 'Ok' == answer.status ){
                                result = 'Ok';
                                var signFile = answer.data.fileName;
                                if( 'object' == typeof(signFile) ){
                                        for( var ind in signFile ){
                                            result = '';
                                            useTopHoverTitle( 'Проведем распоковку файла'  );
                                            console.log( 'Проведем распоковку файла' + ind + ' ' + signFile[ind] )
                                            $.ajax({
                                                type: 'POST',
                                                url: "/eo/declaration/save-sign-file/"+$("#id_application_val").val()+'?step=signExtract&fileId='+ind,
                                                dataType: 'json',
                                                async: false,
                                                success: function(answer) {
                                                    console.log( answer );
                                                    if( 'Ok' == answer.status){
                                                        useTopHoverTitle( "файл проверен " + signFile );
                                                        console.log( 'Загрузим фай л с подписью -Подписи валидны' + signFile );
                                                        result = 'Ok';
                                                    }
                                                    else{
                                                        useTopHoverTitleError("отшибка",1);
                                                        if( 'string' == typeof(answer.console) ){
                                                            console.error( answer.console );
                                                        }
                                                    }
                                                    //result = '1'
                                                },
                                                error: function (e) {
                                                    console.log('Ошибка при выдергивании данных из подписаных ');
                                                    console.log(e);
                                                    console.log(e.responseText);
                                                    result = '-1' ;
                                                }
                                            });
                                            if( 'Ok' != result ){
                                                break;
                                            }
                                        }
                                        
                                        console.log(' ура');
                                }
                            }
                            else{
                                useTopHoverTitleError( "отшибка" );
                            }
                            //result = '1'
                        },
                        error: function (e) {
                            console.log('Ошибка при получини справочника причин из базы');
                            console.log(e);
                            result = '-1' ;
                        }
                    });




                    return false;
                }

                 if(typeof(data.result.status) == "string"){
                //    sendToSend();
                //    hidePopupWin();
                //    return false;
                }

                $.each(data.result.files, function (index, file) {

                    if(file.errors.length < 1){
                        buttons.del = '<span class="MultiFile-remove" onclick="window_delete_application_file(\''+file.fileId+'\', 1)">x</span>';
                        buttons.valid = '<span title="Выполняется проверка подписи файла" class="spinner_verify_sign"></span>';
                        $('.FileListWrap.'+$(elem).attr("codeType"))
                            .append('<div id="'+file.fileId+'">'+ buttons.del +'<a target="_blank" href="'+file.filePath+'" download="'+file.fileName+'">'+file.fileName+'</a></div>')     ;
                        $('.progress .progress-bar.'+$(elem).attr("codeType")+'').width("0%");
                        $('.FileListWrap.'+$(elem).attr("codeType")).parent(".containerWrapFile").css('border','none');
                        //Добавим перегрузка вкладки
                        handleResponse(4);
                        ret = 'ok'
                    }else{
                        var error_string = data.result.files[0].errors.join(',');

                        $('.FileListWrap.'+$(elem).attr("codeType"))
                            .append('<div class="wrap_file_answer"><a class="MultiFile-remove remove_answer" >x</a> <span style="color:red;" class="red">'+data.originalFiles[0].name+'(Ошибки при загрузке файла: '+error_string+' )</span></div>');
                        $('.progress .progress-bar.'+$(elem).attr("codeType")+'').css('width','0%');
                    }
                     $('.progress .progress-bar.'+$(elem).attr("codeType")).parent('.progress').hide();
                    console.error( url );
                    console.error( error_string );

                     ret = 'error';
                });

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress .progress-bar.'+$(this).attr("codeType")+'').parent('.progress').show();
                $('.progress .progress-bar.'+$(this).attr("codeType")+'').css(
                    'width',
                    progress + '%'
                );
            },
            fail: function (e, data) {
                console.error( 'ошибка при отправке файла' );
                useTopHoverTitle( "" , 1 );
                useTopHoverTitleError( "Ошибка при отпправке файла" , 1 );
                console.log( data );
                console.log(data._response.jqXHR.responseText);
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
return ret;
}


/**
 * ф-ция для отображения диалогового окна с информацией о подписи
 */
var get_sign_info = function(sign_id)
{
    if(sign_id != ''){
        $.ajax({
            type: "POST",
            url: "/eo/sign/get-dialog-sign-info",
            data: ({sign_id : sign_id}),
            success: function(data){
                $('.dialog_sign_info').remove();
                $("<div class='dialog_sign_info'></div>").html( data )
                    .dialog({
                            autoOpen:false,
                            title: "Информация о проверке подписи файла",
                            modal: true,
                            width: 800,
                            buttons: { "Закрыть": function() { $(this).dialog("close"); } }
                    }).dialog("open")
            }
        });
    }

}
/*
 * Получения кол-ва заявок по всем шагам
 */
function getAllCount(){
    $.ajax({
        type: 'POST',
        url: '/eo/declaration/get-count-all-step',
        dataType:"json",
        success: function (answer) {
            for (key in answer) {
                $(".num[step="+key+"]").text(answer[key]);
            }
        }
    });
}

function _addOpOrg(){
    var id_org = $(".active_fil").attr('id_org');
    var id_appl = $(".active_fil").attr('id_appl');
    if(!id_org || !id_appl){
        return false;
    }
    var levels = $(".active_fil").parent().find('.level-elem');
    var arr_level = [];
    if(levels.length){
        levels.each(function(){
            arr_level.push($(this).attr('id-level'));
        })
    }
    var name_org = $(".active_fil").text();
    if(confirm('Вы уверены, что хотите добавить все программы в "'+name_org.trim()+'"?')){
        $(".loading-overlay").show();
        $.ajax({
            type: 'POST',
            data: {id_appl:id_appl, id_org:id_org, arr_level:arr_level},
            url: '/eo/declaration/save-all-direct',
            success: function (answer) {
                $(".loading-overlay").fadeOut(250);
                if(answer == 'Ok'){
                    $('.show_tab.active').click();
                }else{
                    useTopHoverTitleError('Произошел какой то технический сбой');
                }
            }
        });
    }
}

/**
 * В специальной верхней зоне выводит текст из параметра text,
 * и прячет его через время указанное в милисекундах в параметре time
 * Для ошибки
 *
 * @param string text
 * @param int time
 */
function useTopHoverTitleError(text, time) {
    if(typeof(time) == 'undefined'){
        time = 2000;
    }
    if(typeof(time) == 'string'){
        time = parseInt(time);
    }
    if( isNaN(time) ){
        time = 2000;
    }
    if( !$("#hover-title-error").length ){
        jQuery('body').prepend(' <div id="hover-title-error"></div>');
    }
    $("#hover-title-error").html(text);
    $("#hover-title-error").fadeIn("fast");
    setTimeout(function () {
        $("#hover-title-error").fadeOut('fast');
    }, time);
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function notifyLoadReasons() {
    $.ajax({
        url: "/eo/declaration/notify-comments-reference/" + $('.active_appl').attr('id_appl'),
        dataType: "json",
        success: function (answer) {
            console.log("стандартные причины");
            console.log(answer);
            notifyReasonReference = {};
            for (var key in answer) {
                notifyReasonReference[answer[key]['Id']] = answer[key]['Name'];
            }
            notifyReasonsPut();
        },
        error: function (e) {
            console.log('Ошибка при получини справочника причин из базы');
            console.log(e);
            //и всеравно выводим причины
            notifyReasonsPut();
        }
    });
}

/**
 * если в базе есть причины, покажем их
 * @returns {Number}
 */
function notifyReasonsPut(type) {
    //if (type == undefined){
    //    type = 'negativ';
    //}
    console.log("хотим вставить сохраненные причины ");
    setMaxShowTab(6);//разрешаем прыгать на табку
    var url = "/eo/declaration/notify-comments-get/"+ $('.active_appl').attr('id_appl');
    $.ajax({
        url: url,
        dataType: "json",
        success: function (answer) {
            console.log("причины загружены ");
            console.log(answer);
            for (var key in answer) {
                var element = answer[key];
                if (element['comment'] !== undefined) {
                    notifyReasonAdd(element['comment']);
                }
                if (element['reference'] !== undefined) {
                    notifyReasonAddFromReference(element['reference']);
                }
            }
            //заблокировать причины
            console.log("заблокировать причины");
            console.log(blockedNotifyComents);
            if (blockedNotifyComents!==0){
                jQuery(".notify-reason input").attr('readonly','readonly');
                jQuery(".notify-reason select").attr('disabled','disabled');
                jQuery(".notify-reason .del-button").parent().remove();
            }
        },
        error:function(e){
            console.log('Ошибка при получении причин из базы');
            console.log(e);
        }
    });

}

/*
 * просто счетчик причин несоответствий
 * (чтоб ид отчичались)
 * @type Number
 */
var notifyReasonCount = 0;

/**
 *  Добавляем текстовое  поле причины
 *
 *   @param reasonNew - текст причины, если есть
 */
function notifyReasonAdd(reasonNew) {
    if (reasonNew === undefined) {
        reasonNew = '';
    }
    ++notifyReasonCount;
    var collection = $('.notify-reason');
    var notifyNum = collection.length + 1;
    $('#notify-form tbody').append('<tr class="notify-reason">\n\
                    <td clacc="title_form">\
                        Причина ' + notifyNum + '\
                    </td>\
                    <td style="padding-right: 0px;">\n\
                        <textarea  class="form-control notify-reason-item" type="text" name="reasons[' + notifyReasonCount + '][comment]"  placeholder="Введите причину" onchange="notifyReasonChange(this);" ntype="input"  rows="3">' + reasonNew + '</textarea>\
                    </td>\
                </tr>'
    );
    $('#notify-form tbody').children().last().find('input').focus();
}


/**
 * хэш справочника причин на вид
 * Ид : Описоние
 *
 * @type type
 */
var notifyReasonReference = {};

/**
 * Добавляем причину из справочинка в виде выпадающего меню
 *
 * @param {type} reasonSelected - Ид причины
 * @returns {undefined}
 */
function notifyReasonAddFromReference(reasonSelected) {
    if (reasonSelected === undefined) {
        reasonSelected = '';
    }
    ++notifyReasonCount;
    var collection = $('.notify-reason');
    var notifyNum = collection.length + 1;
    var references;
    var notifyReasonReferencePut = '';
    //обозначаем выделеную, если она (предпологалось гасить, уже выбраную)
    for (var key in notifyReasonReference) {
        var selectIt = '';
        if (reasonSelected === key) {
            selectIt = 'selected';
        }
        notifyReasonReferencePut = notifyReasonReferencePut
            + '<option value="'
            + key
            + '" '
            + selectIt
            + ' >'
            + notifyReasonReference[key]
            + '</option>';
    }
    //добавляем элемент
    $('#notify-form tbody').append('<tr class="notify-reason">\n\
                    <td clacc="title_form">\
                        Причина ' + notifyNum +
        '</td>'
        + '<td style="padding-right: 0px;">'
        + '<select class="form-control notify-reason-item" disabled name="reasons[' + notifyReasonCount + '][reference]" value="" placeholder="Введите причину" onchange="notifyReasonChange(this);" ntype="select">'
        + notifyReasonReferencePut
        + '</select>\
                    </td>\
                </tr>'
    );
    //ставим фокус
    $('#notify-form tbody').children().last().find('select').focus();
}

/**
 *Тык по красному крестику возле поля причины
 *удаляем причину из списка
 *
 * @param {type} element -указательна элемент
 * @returns {Number} ничего
 */
function notifyReasonDel(element,isYes) {
    if (isYes===undefined){
        isYes=false;
    }
    if (!isYes && !confirm("Удалить причину?")) {
        return 0;
    }
    $(element).parents('.notify-reason').remove();
    //переименовываем заголовки причин
    var collection = $('.notify-reason');
    var l = collection.length;
    for (var i = 0; i < l; i++) {
        var td = $(collection[i]).find('td');
        $(td[0]).text('Причина ' + (i + 1));
    }
    //var $('notify-reason');
}

function morphyOrgName() {
    var orgId = $('.current-organization-id').val();

    $.ajax({
        url: "/eo/index/morphy-organization",
        type:'POST',
        data:{'id':orgId},
        success: function (answer) {
            var status = answer['status'],
                data = answer['data'];

            if( status == 'ok'){
                $('[name="NameGenCase"]').val(data['genitive']);
                $('[name="NameAccCase"]').val(data['accusative']);
                $('[name="NameDatCase"]').val(data['dative']);
                $('[name="NameInstrCase"]').val(data['ablative']);
            }
        },
        error: function (e) {
            console.log('Ошибка');
        }
    });
}

