var dateDayData;

$(document).ready(function(){
    $('#loadmask-1').hide();
    $.fn.setOptionVisible = function (visible) {
        return this.each(function () {
            if (visible && $(this).parent().get(0).tagName == 'SPAN') $(this).unwrap().removeAttr('disabled');
            else if ($(this).parent().get(0).tagName != 'SPAN') $(this).attr('disabled', 'disabled').wrap("<span>").parent().hide();
        });
    };

    // закрытие окна
    $('#cboxClose, #btn-close').click(function(){
        $("#popup_win").hide();
    });

    var appealTypesSelect = $('.field-appeal-type SELECT');
    var appealOptions = $('OPTION', appealTypesSelect);
    var serviceReasonSelect = $('.field-service-reason SELECT');
    var serviceReasonOptions = $('OPTION', serviceReasonSelect);
    var appealReasons = $('.field-appeal-reason .checkbox');
    var onlyBoundFields = $('.form-group.only-delivery, .form-group.only-reception');


    var preFilledForm = ($('.field-service-type INPUT:checked').length != 0);

    $('OPTION[data-hide]').setOptionVisible(false).removeAttr('data-hide');

    /**
     * Обработчик изменения радио батона.
     */
    $('.field-service-type INPUT').change(function () {
        var selectedType = $('.field-service-type INPUT:checked').val();
        var appealNeededOptions = appealOptions.filter('[data-code$="_' + selectedType + '"]');

        // Скрываем не нужные option
        appealOptions.setOptionVisible(false);

        // Показываем нужнуе option
        appealNeededOptions.setOptionVisible(true);

        // Назначаем выбраным первые option.
        if (!preFilledForm) {
            appealTypesSelect.val(appealNeededOptions.first().val());
        }
        appealTypesSelect.trigger('change');
    });

    /**
     * Обработчик изменения select Тип обращения.
     */
    appealTypesSelect.change(function () {
        var selectedType = $('.field-service-type INPUT:checked').val();
        var selectedAppeal = appealTypesSelect.val();
        var selectedAppealCode = appealOptions.filter('[value="' + selectedAppeal + '"]').attr('data-code');

        var appealType = selectedAppealCode.split('_')[0];
        onlyBoundFields.addClass('hidden')
            .find('INPUT, SELECT').attr('disabled', 'disabled');
        onlyBoundFields.filter('.only-' + appealType)
            .removeClass('hidden')
            .find('INPUT, SELECT').removeAttr('disabled');

        var serviceReasonNeededOptions = serviceReasonOptions.filter('[data-appeal-code="' + selectedAppealCode + '"]');
        serviceReasonOptions.setOptionVisible(false);
        serviceReasonNeededOptions.setOptionVisible(true);
        if (!preFilledForm) {
            serviceReasonSelect.val(serviceReasonNeededOptions.first().val());
        }
        serviceReasonSelect.trigger('change');
    });

    /**
     * Обработчик select Основание услуги.
     */
    serviceReasonSelect.change(function () {
        var selectedReason = serviceReasonSelect.val();
        var selectedReasonCode = serviceReasonOptions.filter('[value="' + selectedReason + '"]').attr('data-code');

        var appealReasonsNeeded = appealReasons.filter('[data-reason-code="' + selectedReasonCode + '"]');
        appealReasons.addClass('hidden').find('INPUT').attr('disabled', 'disabled'); // скрыть все, заблокировать

        if (!preFilledForm) {
            appealReasons.find('INPUT').prop('checked', false);
        }

        appealReasonsNeeded.removeClass('hidden').find('INPUT').removeAttr('disabled'); // показать нужные, разблокировать

        if ($(this).val() == '85877df2e68a4df7b3d6b7dc76b08d3f') {
            $('.field-appeal-reason INPUT[value="a382cea16f1c4cf9a729d4f8d0d8a8a0"]').prop('checked', true);
        }

        if ($(this).val() == '7d5c997617f04f898e0fbc3c3bb75241') {
            $('.field-appeal-reason INPUT[value="24e060cb-6384-4009-b9ce-93a775678732"]').prop('checked', true);
        }

        $('.appeal-reason-row').removeClass('hidden');

        if ($('.appeal-reason-row INPUT:visible').length == 0) {
            $('.appeal-reason-row').addClass('hidden');
        }
    });

    // Индивидуальная обработка checkbox (государственная аккредитация в отношении ранее не аккредитованных образовательных программ)
    $('.field-appeal-reason INPUT').change(function () {
        if($(this).attr('value') === '67f97827-57f9-4a0c-b2c7-d9806969bef6'){
            var isChecked = $(this).is(':checked');
            if(isChecked){
                $('.field-appeal-reason INPUT').attr('disabled', 'disabled');
                $(this).removeAttr('disabled');
            }else{
                $('.field-appeal-reason INPUT').removeAttr('disabled');
            }
        }else{
            if($('.field-appeal-reason INPUT:checked').length){
                $('input[value="67f97827-57f9-4a0c-b2c7-d9806969bef6"]').attr('disabled', 'disabled');
            }else{
                $('input[value="67f97827-57f9-4a0c-b2c7-d9806969bef6"]').removeAttr('disabled');
            }
        }
    });

    /**
     * Обработчик checkbox Требуется сверка?.
     */
    $('.check-unverified-documents INPUT').change(function () {
        var isChecked = $(this).is(':checked');
        if (isChecked) {
            $('.group-documents-number').removeClass('hidden-by-option');
            $('.field-documents-number INPUT').removeAttr('disabled');
        } else {
            $('.group-documents-number').addClass('hidden-by-option');
            $('.field-documents-number INPUT').attr('disabled', 'disabled');
        }
    }).trigger('change');

    $('.field-branches-number INPUT').change(function () {
        var n = $(this).val();
        if (n == 0) {
            $('.group-check-branches-only').addClass('hidden-by-option');
            $('.field-check-branches-only INPUT').attr('disabled', 'disabled');
        } else {
            $('.group-check-branches-only').removeClass('hidden-by-option');
            $('.field-check-branches-only INPUT').removeAttr('disabled');
        }
    });

    // запуск процеса раставления нужных данных по форме
    if (preFilledForm) {
        $('.field-service-type INPUT:checked').trigger('change');
    } else {
        $('.field-service-type INPUT').first().click().trigger('change');
    }

    preFilledForm = false;


    $('#btn-continue').click(function(){
        step1();
    });

    $('#btn-save').click(function(){
        step2();
    });
    $('#btn-back').click(function(){
        back();
    });

    $('input').click(function(){
        $(this).removeClass("error-field");
    });

    $('input[name=cell_phone]').mask('+7 (999) 999-99-99');
});

/**
 * Инициализируем дату и время.
 */
function initDatepicker() {
    $('.field-date-time OPTION').addClass('hidden').attr('disabled', 'disabled');

    // Ставим первую возможную дату.
    var getFirstAvailableDay = function() {
        if (window.dateDayData) {
            var dateString = Object.keys(window.dateDayData)[0];
            var atoms = dateString.split('-');
            return new Date(atoms[0], atoms[1] - 1, atoms[2]);
        } else return null;
    };

    $('input[name=date_day]').datepicker({
        language: 'ru',
        orientation: 'bottom',
        beforeShowDay: function(date) {
            var dateFormatted = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
            if (window.dateDayData[dateFormatted] !== undefined) {
                return [true,"","Свободно"];

            }
            return [false,"","Нет приёма"];
        },
        onSelect: setTime
    }).datepicker('setDate', getFirstAvailableDay());

    function setTime(date, e) {
        console.log(date);
        var d;
        if(e){
             d = e.currentYear + '-' + (e.currentMonth + 1) + '-' + e.currentDay;
        }else{
             d = date;
        }
        var times = window.dateDayData[d];
        if (window.dateDayData[d]) {
            var select = $('.field-date-time SELECT');
            var option = $('.field-date-time OPTION');
            select.empty();

            for (var key in times){
                select.append('<option>'+times[key]+'</option>');
            }

            option.addClass('hidden').attr('disabled', 'disabled');

            option.each(function () {
                var v = $(this).text();
                if (times.indexOf(v) != -1) $(this).removeClass('hidden').removeAttr('disabled');
            });

            select.val(times[0]);

        }
    }

    setTime(Object.keys(window.dateDayData)[0]);
}

/**
 * Первый шаг регистрации.
 */
function step1(){
    var data = getDataFormStep1();

    $.ajax({
        type: 'POST',
        url: '/eo/index/step1',
        data: data,
        success: function(answer) {
            if(answer.status == 'ok'){
                window.dateDayData = answer.dateTime;
                initDatepicker();
                $('#loadmask-1').show();
                $('#loadmask-2').hide();
                $('#btn-continue').hide();
                $('#btn-save').show();

                return true;
            }
            useTopHoverTitleError('Произошла ошибка, не коректные данные');
            return false;
        }
    });
}

var fieldAddError = function (fieldError) {
    for (var key in fieldError){
        $('input[name='+ key +']').addClass("error-field");
    }
};

/**
 * Второй шаг регистрации.
 */
function step2(){

    var data = getDataFormStep2();

    console.log(data);
    $.ajax({
        type: 'POST',
        url: '/eo/index/step2',
        data: data,
        success: function(answer) {
            if(answer.status == 'ok'){
                $('#loadmask-2').show();
                openDialog(answer.content);
                return true;
            }
            if(answer.status == 'error-mail'){
                useTopHoverTitleError('При отправке сообщения произошла ошибка');
                return false;
            }
            if(answer.status == 'error-valid'){
                useTopHoverTitleError('Заполните правильно все поля');
                fieldAddError(answer.fieldError);
                return false;
            }
            useTopHoverTitleError('Произошел какой то технический сбой');
            return false;
        }
    });
}

/**
 * Сбор данных для превого этапа регистрации.
 *
 * @returns {{}}
 */
function getDataFormStep1() {
    var appealReason = new Array();
    var appeal_type = $('select[name=appeal_type]').val();
    $('input[name="appeal_reason[]"]:checked').each(function() {appealReason.push($(this).val());});
    var data = {};

    if(appeal_type === '08c58690c64c4d00af201b33583e0adf' || appeal_type === 'cb6c9be69d404d1e8678355bbd6501ab'){
        data = {
            service_type: $('input[name=service_type]:checked').val(),
            appeal_type: appeal_type,
            service_reason: $('select[name=service_reason]').val(),
            appeal_reason: appealReason,
            issue_documents_number: $('select[name=issue_documents_number]').val(),
        };
    }else{
        data = {
            service_type: $('input[name=service_type]:checked').val(),
            appeal_type: appeal_type,
            service_reason: $('select[name=service_reason]').val(),
            appeal_reason: appealReason,
            branches_number: $('input[name=branches_number]').val(),
            check_branches_only: $('input[name=check_branches_only]').prop("checked"),
            programs_number: $('select[name=programs_number]').val(),
            check_needed: $('input[name=check_needed]').prop("checked"),
            documents_number: $('select[name=documents_number]').val()
        };
    }

    return data;
}

/**
 * Сбор данных для второго этапа регистрации.
 */
function getDataFormStep2() {
     var data = {
        date_day:   $('input[name=date_day]').val(),
        date_time:  $('select[name=date_time]').val(),
        last_name:  $('input[name=last_name]').val(),
        first_name: $('input[name=first_name]').val(),
        middle_name:$('input[name=middle_name]').val(),
        phone:      $('input[name=phone]').val(),
        cell_phone: $('input[name=cell_phone]').val(),
        email:      $('input[name=email]').val(),
    };

    return data;
}

function openDialog(content) {
    var dContent = $('<div class="request-new"></div>');
    $(dContent).dialog({
        title: "ЗАЯВКА ПОДАНА В ЭЛЕКТРОННУЮ ОЧЕРЕДЬ",
        width: '50%',
        height: '400',
        position: { my: "center center", at: "center center", of: window },
        modal: true,
        close: function(event, ui) {
            $(this).remove();
        },
        open: function() {
            $(dContent).append(content);

        },
        buttons:[
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ]
    });
}


/**
 * Шаг назад
 */
function back() {
    $('#loadmask-1').hide();
    $('#loadmask-2').show();
    $('#btn-continue').show();
    $('#btn-save').hide();
    $('.step-2 INPUT').val('');
    $('.step-2 OPTION').empty();
}