var Calculator = function(){
	var _calculator = this;
	var _private = {
		/**=====================================================
		 * Приватные данные
		 *====================================================*/

	    // блоки с которыми работает калькулятор
	    blocks: {
	    	main: null,
	    	options: null,
	    	info: null,
	    	sum: null,
	    	optionsRow: null,
	    	infoText: null,
	    	infoFull: null,
	    	base: null
	    },
	    data: {
	    	EduProgramTypes : {},
	    	currentPaymentType : "1",
	    	currentBaseVal: "1",
	    	isReset: false
	    },
	    count: {
	    	isLowGrade: false,
	    	addedEduPrograms: {},
	    	MidEdParentEGSIds: {},
	    	HiEdParentEGSIds: {}
	    },

		/**=====================================================
		 * Приватные методы
		 *====================================================*/
		 
		/**
		 * Метод инициирует данные для работы
		 */
		initData: function(){
			_private.blocks.main = $(".viewzone");
			_private.blocks.options = $(".calculator-options",_private.blocks.main);
			_private.blocks.optionsRow = $("tr",_private.blocks.options).eq(0).clone();
			_private.blocks.infoFull = $(".calculator-info",_private.blocks.main);
			_private.blocks.info = $(".info-content",_private.blocks.main);
			_private.blocks.sum = $(".sumField .sum",".buttons-container");
			_private.blocks.infoText = $(".infoText",_private.blocks.main);
			_private.blocks.base = $(".baseSelectRow", _private.blocks.main);
			//собираем данные для активного типа обр программ
			var eduProgramTypeId = $(".selectEduProgramTypes",_private.blocks.optionsRow).val();
			_private.data.EduProgramTypes[eduProgramTypeId] = [];

			$(".selectEduPrograms",_private.blocks.optionsRow).find("option").each(function(i,item){
				var eduProgram = {
					Id: $(item).attr("value"),
					Name: $(item).attr("data-Name"),
					Code: $(item).attr("data-Code"),
					ParentEGSId: $(item).attr("data-ParentEGSId")
				}
				_private.data.EduProgramTypes[eduProgramTypeId].push(eduProgram);				
			});			
		},

		 /**
		  * Метод отвечает за установку обработчиков
		  */
		initHandlers: function(){
			//Обработка нажатий на кнопку +
			$(_private.blocks.options).delegate(".bt-add-line", "click", function(){
				
				var newClonedBlock = $(_private.blocks.optionsRow).clone();

				$(_private.blocks.options).append(newClonedBlock);
				$(".selectEduPrograms",newClonedBlock).combobox();


				$(this).hide();
				$(this).parent().find(".bt-remove-line").show();
			});

			//Обработка нажатий на кнопку -
			$(_private.blocks.options).delegate(".bt-remove-line", "click", function(){
				if($("tr",_private.blocks.options).length > 1){
					$(this).parents("tr").eq("0").remove();					
				}
			});		

			//Обработка смены вида пошлины
			$("#paymentType",_private.blocks.main).change(function(){
				var newVal = $(this).val();
				//Сравниваем изменилось ли значение
				if(newVal == _private.data.currentPaymentType && !_private.data.isReset)
					return true;
				_private.data.isReset = false;
				//Утснавливаем новое значение как текущее
				_private.data.currentPaymentType = newVal;
				_private.blocks.base.hide();


				_private.blocks.sum.text(0);
				switch(newVal){
					case "1":
						_private.blocks.infoText.show();
						_private.blocks.options.show();
						_private.blocks.options.html("");
						_private.blocks.options.append(_private.blocks.optionsRow.clone());
						_private.blocks.infoFull.show();
						_private.blocks.info.html("");
						//Вешаем автокомплит
						$(".selectEduPrograms",_private.blocks.options).combobox();

						break;
					case "2":
						_private.blocks.infoText.show();
						_private.blocks.options.show();
						_private.blocks.options.html("");
						_private.blocks.infoFull.show();
						_private.blocks.info.html("");
						_private.blocks.base.show();

						_private.blocks.base.show();
						_private.blocks.base.find("select").val("1");
						_private.data.currentBaseVal = "1";

						var newClonedBlock = _private.blocks.optionsRow.clone();						
						_private.blocks.options.append(newClonedBlock);
						$(".selectEduPrograms",_private.blocks.options).combobox();

						break;
					case "3":
					case "4":
						_private.blocks.infoText.hide();
						_private.blocks.options.hide();
						_private.blocks.options.html("");
						_private.blocks.infoFull.hide();
						_private.blocks.info.html("");
						$(".btCount",".buttons-container").click();
						break;
				}
			})

			//Обработка смены основания
			$("#baseType",_private.blocks.main).change(function(){
				var baseVal = $(this).val();
				if(baseVal == _private.data.currentBaseVal)
					return true;
				_private.blocks.sum.text(0);
				_private.data.currentBaseVal = baseVal;

				if(baseVal == "1"){
					_private.blocks.infoText.show();
					_private.blocks.options.show();
					_private.blocks.options.html("");
					_private.blocks.options.append(_private.blocks.optionsRow.clone());
					_private.blocks.infoFull.show();
					_private.blocks.info.html("");
					//Вешаем автокомплит
					$(".selectEduPrograms",_private.blocks.options).combobox();
				} else {
					_private.blocks.options.hide();
					_private.blocks.infoText.hide();
					_private.blocks.infoFull.hide();
					_private.blocks.options.html("");
					_private.blocks.infoFull.hide();
					_private.blocks.info.html("");
					$(".btCount",".buttons-container").click();
				}
			})

			//Обработка смены типа обр Программы
			$(_private.blocks.options).delegate(".selectEduProgramTypes","change", function(){
				
				var parentRow = $(this).parents("tr").eq(0);
				
				var self = this;
				$(this).prop("disabled",true);

				var eduProgramTypeId = $(this).val();

				$(".ui-combobox", parentRow).removeClass("disabled");
				$(".ui-combobox input", parentRow).prop("disabled",false);
				//Если это Начальное, Среднее или Общее образование,
				//то обрабатываем особо
				
				//Начальное
				if(
					eduProgramTypeId == "7E6176CC-0E8B-40E5-806F-4DB3559DE9FC"
					|| eduProgramTypeId == "18737366-D87A-4D7B-9D09-54533BE6A265"
					|| eduProgramTypeId == "5E30E928-A3B9-A62E-2D6B-53DCB80F8D2B"
					){
					$(".ui-combobox", parentRow).addClass("disabled");
					$(".ui-combobox input", parentRow).prop("disabled",true).val("");
					$(".selectEduPrograms",parentRow).html("");
					$(this).prop("disabled",false);
				} 
				//Проверяем, если в данных пока хранится инфо о списке обр программ
				else if(_private.data.EduProgramTypes[eduProgramTypeId]){
					// Заполняем список значениями
					$(".ui-combobox",parentRow).hide();
					var pb = $( "<div>" ).progressbar({
				      value: 0,
				      max: _private.data.EduProgramTypes[eduProgramTypeId].length
				    });

				    $(".selectEduPrograms",parentRow).html("");
					for(var i = 0; i < _private.data.EduProgramTypes[eduProgramTypeId].length; i++){
						var eduProgram = _private.data.EduProgramTypes[eduProgramTypeId][i];
						$(".selectEduPrograms",parentRow).append(
	                		$("<option>")
	                			.attr("value",eduProgram.Id)
	                			.attr("data-Code",eduProgram.Code)
	                			.attr("data-ParentEGSId",eduProgram.ParentEGSId)
	                			.text(eduProgram.Code +" "+ eduProgram.Name)

	                	);
	                	if(i%10==0)
	                		pb.progressbar("value",i);
					}
					pb.remove();
	                //Вешаем автокомплит
					$(".selectEduPrograms",parentRow).combobox("refresh");
					$(".ui-combobox",parentRow).show();
				    $(self).prop("disabled",false);
				} else {
					// ================= Заправшиваем список с сервера
					// Прячем список и отображаем прогресс бар
					$(".ui-combobox",parentRow).hide();
					var pb = $( "<div>" ).progressbar({
				      value: false
				    });
				    $(".selectEduPrograms",parentRow).parent().append(pb);
				    //Запускаем аякс запрос за списком
				    $.ajax({
			            type: 'POST',
			            dataType: "json",
			            url: "/eo/index/get-edu-programs/"+eduProgramTypeId,
			            success: function(answer) {
			                _private.data.EduProgramTypes[eduProgramTypeId] = answer;
			                //Заполняем данными селект с выбором программ
			                //и рисуем прогресс
			                pb.progressbar({
			                	max: answer.length,
			                	value: 0
			                })
			                $(".selectEduPrograms",parentRow).html("");
			                for(var i = 0; i < answer.length; i++){
			                	var eduProgram = answer[i];
			                	$(".selectEduPrograms",parentRow).append(
			                		$("<option>")
			                			.attr("value",eduProgram.Id)
			                			.attr("data-Code",eduProgram.Code)
			                			.attr("data-ParentEGSId",eduProgram.ParentEGSId)
			                			.text(eduProgram.Code +" "+ eduProgram.Name)

			                	);
			                	if(i%10==0)
			                		pb.progressbar("value",i);
			                }
			                pb.remove();
			                $(".selectEduPrograms",parentRow).combobox("refresh");
							$(".ui-combobox",parentRow).show();
			                $(self).prop("disabled",false);
			            }
			        });
				}
			});	

			//Вешаем обработчик на кнопку рассчитать
			$(".btCount",".buttons-container").unbind("click").click(function(){
				_calculator.count();
			});
			//Вешаем обработчик на кнопку сбросить
			$(".btReset",".buttons-container").unbind("click").click(function(){
				_calculator.reset();
			});

			//Вешаем автокомплит
			$(".selectEduPrograms",_private.blocks.options).combobox();
		},

		/**
		 * Метод сбрасывает значения, что используются при подсчете
		 */
		resetCountData: function(){
			_private.count = {
				isLowGrade: false,
				addedEduPrograms: {},
				MidEdParentEGSIds: {},
				HiEdParentEGSIds: {}
			}
			_private.blocks.info.html("");
		},

		/**
		 * Метод подсчитывает сумму
		 */
		countSum: function(){
			var sum = 0;
			$(".amount",_private.blocks.info).each(function(i,item){
				var val = parseInt($(item).text().replace(" ",""));
				sum+=val;
			})
			sum+="";
			_private.blocks.sum.text(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
		},

		/**
		 * Метод подсчитывает первый случай
		 */
		countCase1: function(){
			//проходим по данным
			$("tr",_private.blocks.options).each(function(i,item){
				var EduProgramTypesId = $(".selectEduProgramTypes", item).val();
				//Проверяем если такая ОП уже была добавлена

				if(EduProgramTypesId == "7E6176CC-0E8B-40E5-806F-4DB3559DE9FC")
					var checkEduProgramId = "first";
				else if(EduProgramTypesId == "18737366-D87A-4D7B-9D09-54533BE6A265")
					var checkEduProgramId = "base";
				else if(EduProgramTypesId == "5E30E928-A3B9-A62E-2D6B-53DCB80F8D2B")
					var checkEduProgramId = "middle";
				else
					var checkEduProgramId = $(".selectEduPrograms", item).val();

				if(_private.count.addedEduPrograms[checkEduProgramId])
					return true;
				//
				switch(EduProgramTypesId){
					//Начальное общее образование
					case "7E6176CC-0E8B-40E5-806F-4DB3559DE9FC":
						var tr="<tr><td>Начальное общее образование</td>";
						tr+="<td>-</td><td class='amount'>";
						if(_private.count.isLowGrade){
							tr+="0"
						} else {
							_private.count.isLowGrade = true;
							tr+="15 000";
						}
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Основное общее образование
					case "18737366-D87A-4D7B-9D09-54533BE6A265":
						var tr="<tr><td>Основное общее образование</td>";
						tr+="<td>-</td><td class='amount'>";
						if(_private.count.isLowGrade){
							tr+="0"
						} else {
							_private.count.isLowGrade = true;
							tr+="15 000";
						}
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Среднее общее образование
					case "5E30E928-A3B9-A62E-2D6B-53DCB80F8D2B":
						var tr="<tr><td>Среднее общее образование</td>";
						tr+="<td>-</td><td class='amount'>";
						if(_private.count.isLowGrade){
							tr+="0"
						} else {
							_private.count.isLowGrade = true;
							tr+="15 000";
						}
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Среднее профессиональное образование – программы подготовки квалифицированных рабочих, служащих
					case "A113E1BF-E7C0-1B4F-6698-CDD3446F8AAE":
					//Среднее профессиональное образование – программы подготовки специалистов среднего звена
					case "FC8E716E-07E2-325E-3174-FD83E19590FF":
						var tr="<tr><td>Среднее профессиональное образование</td>";
						tr+="<td>"+$(".selectEduPrograms :selected", item).text()+"</td><td class='amount'>";

						var ParentEGSId = $(".selectEduPrograms :selected", item).attr("data-ParentEGSId");

						if(!_private.count.MidEdParentEGSIds[ParentEGSId]){
							tr+="35 000";
							_private.count.MidEdParentEGSIds[ParentEGSId] = true;
						} else {
							tr+="0";
						}						

						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Высшее образование – бакалавриат
					case "C420030B-0B27-CE31-BAB4-CA5DDC09231E":
						var tr="<tr><td>Высшее образование – бакалавриат</td>";
						tr+="<td>"+$(".selectEduPrograms :selected", item).text()+"</td><td class='amount'>";
						var ParentEGSId = $(".selectEduPrograms :selected", item).attr("data-ParentEGSId");
						if(!_private.count.HiEdParentEGSIds[ParentEGSId]){
							tr+="100 000";
							_private.count.HiEdParentEGSIds[ParentEGSId] = true;
						} else {
							tr+="0";
						}						
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Высшее образование – специалитет
					case "DEEE7F04-1B7E-F674-C33F-DE54E4BF3FE8":
						var tr="<tr><td>Высшее образование – специалитет</td>";
						tr+="<td>"+$(".selectEduPrograms :selected", item).text()+"</td><td class='amount'>";
						var ParentEGSId = $(".selectEduPrograms :selected", item).attr("data-ParentEGSId");
						if(!_private.count.HiEdParentEGSIds[ParentEGSId]){
							tr+="100 000";
							_private.count.HiEdParentEGSIds[ParentEGSId] = true;
						} else {
							tr+="0";
						}	
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Высшее образование – магистратура
					case "11B8054B-2D5F-1438-B505-F2D5BF9E860E":
						var tr="<tr><td>Высшее образование – магистратура</td>";
						tr+="<td>"+$(".selectEduPrograms :selected", item).text()+"</td><td class='amount'>";
						var ParentEGSId = $(".selectEduPrograms :selected", item).attr("data-ParentEGSId");
						if(!_private.count.HiEdParentEGSIds[ParentEGSId]){
							tr+="100 000";
							_private.count.HiEdParentEGSIds[ParentEGSId] = true;
						} else {
							tr+="0";
						}	
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Высшее образование – программы подготовки научно-педагогических кадров в аспирантуре
					case "6AD5D02F-3C03-CB68-7A54-2F39861347C3":
						var tr="<tr><td>Высшее образование – программы подготовки научно-педагогических кадров в аспирантуре</td>";
						tr+="<td>"+$(".selectEduPrograms :selected", item).text()+"</td><td class='amount'>";
						var ParentEGSId = $(".selectEduPrograms :selected", item).attr("data-ParentEGSId");
						if(!_private.count.HiEdParentEGSIds[ParentEGSId]){
							tr+="100 000";
							_private.count.HiEdParentEGSIds[ParentEGSId] = true;
						} else {
							tr+="0";
						}	
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Высшее образование – программы подготовки научно-педагогических кадров в адъюнктуре
					case "682A57F0-8BDA-BC62-7B50-EB03E85797BD":
						var tr="<tr><td>Высшее образование – программы подготовки научно-педагогических кадров в адъюнктуре</td>";
						tr+="<td>"+$(".selectEduPrograms :selected", item).text()+"</td><td class='amount'>";
						var ParentEGSId = $(".selectEduPrograms :selected", item).attr("data-ParentEGSId");
						if(!_private.count.HiEdParentEGSIds[ParentEGSId]){
							tr+="100 000";
							_private.count.HiEdParentEGSIds[ParentEGSId] = true;
						} else {
							tr+="0";
						}	
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
					//Высшее образование – программы ординатуры
					case "18A29655-FBF3-FE51-9AEC-02F7FA8DDD99":
						var tr="<tr><td>Высшее образование – программы ординатуры</td>";
						tr+="<td>"+$(".selectEduPrograms :selected", item).text()+"</td><td class='amount'>";
						var ParentEGSId = $(".selectEduPrograms :selected", item).attr("data-ParentEGSId");
						if(!_private.count.HiEdParentEGSIds[ParentEGSId]){
							tr+="100 000";
							_private.count.HiEdParentEGSIds[ParentEGSId] = true;
						} else {
							tr+="0";
						}	
						tr+="</td></td>";
						_private.blocks.info.append($(tr));
						break;
				}
				var EduProgramId = $(".selectEduPrograms", item).val();
				_private.count.addedEduPrograms[EduProgramId] = true;
			});
			_private.countSum();
		}
	}

	/**=====================================================
	 * Публичные методы
	 *====================================================*/
	
	/**
	 * Метод инициализирует калькулятор, вешает обработчики и т.д.
	 */
	this.init = function(){
		//Подготавливаем данныедля работы
		_private.initData();
		//Вешаем обработчики
		_private.initHandlers();
	}

	/**
	 * Метод расчитывает сумму
	 */
	this.count = function(){
		var type = $("#paymentType",_private.blocks.main).val();
		_private.resetCountData();
		switch(type){
			case "1":
				_private.countCase1();
				break;
			case "2":
				//Проверяем если выбран первый пункт, то подсчитываем выбранные добавленные обр программы,
				//если нет, то просто выводим 3 000
				if(_private.data.currentBaseVal == "1"){
					_private.countCase1();
				} else {
					_private.blocks.sum.text("3 000");
				}
				break;
			case "3":
				_private.blocks.sum.text("3 000");
				break;
			case "4":
				_private.blocks.sum.text("300");
				break;
		}
	}

	this.reset = function(){
		_private.data.isReset = true;
		$("#paymentType",_private.blocks.main).change();
	}
}
var Calculator = new Calculator();