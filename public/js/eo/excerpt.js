var excerpt = excerpt || {};

excerpt.licensePage = function(num){
    var num = num || 1,
        dContent = $('.content');
    dContent.html("<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>");
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/eo/license/excerpt/licenses/"+num,
        success: function (answer) {
            $(".loading-img", dContent).fadeOut(500, function () {
                dContent.html("").append(answer);
            });
        }
    });
};

excerpt.certificatePage = function(num){
    var num = num || 1,
        dContent = $('.content');
    dContent.html("<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>");
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/eo/license/excerpt/certificates/"+num,
        success: function (answer) {
            $(".loading-img", dContent).fadeOut(500, function () {
                dContent.html("").append(answer);
            });
        }
    });
};

excerpt.excerptThis = function(){
    var dialog = {
            width: Math.round((screen.width / 100) * 40),
            height: Math.round((screen.height / 100) * 25)
        },
        body = $('.excerpt-dial').append('<div class="excerpt-this-dial"></div>'),
        dial = $('.excerpt-this-dial');

    dial.dialog({
        title: "Запрос на выписку из реестра",
        width: dialog.width,
        height: dialog.height,
        modal: true,
        close: function () {
            dial.remove();
        },
        open: function () {
            var title = $('.excerpt-list-data .excerpt-this').attr('title'),
                type = $('.excerpt-list-data .excerpt-this').attr('etype'),
                buttons = '<div class="button-block"><div class="button yes btButton">Да</div><div class="button no btButton">Нет</div></div>';
            dial.html('<div class="answer">'+title+'</div>'+buttons);
            
            $('.excerpt-this-dial .button.yes').off('click');
            $('.excerpt-this-dial .button.no').off('click');
            $('.excerpt-this-dial .button.no').on('click',function(){
                dial.remove();
            });
            $('.excerpt-this-dial .button.yes').on('click',function(){
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: "/eo/license/excerpt-this",
                    data: {
                        type : type
                    },
                    success: function (answer) {
                    }
                });
                dial.remove();
                        
                if(type=='licenses'){
                    excerpt.licensePage();
                }
                if(type=='certificates'){
                    excerpt.certificatePage();
                }
            });
            
        }
    });
};


