var request = {};

/**
 * Открывает окно создание заявления
 * @param reqId - если задано то форма корректируется под форму редактирования
 * @param organizationId - если задано, то в качестве выбранной организации выбирается переданная организация
 */
request.openAdd = function (reqId, organizationId, dealId) {
    var dialog = {
        width: Math.round((screen.width / 100) * 87),
        height: Math.round((screen.height / 100) * 87) - 100
    };

    var dContent = $(
            '<div class="dialog-request-edit">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

    var data = {};
    if(reqId)
        data.requestId = reqId;
    if(organizationId)
        data.organizationId = organizationId;
    if(dealId)
        data.dealId = dealId;

    $(dContent).dialog({
        title: (reqId)?"Изменение заявления":"Новое заявление",
        width: dialog.width,
        height: dialog.height,
        modal: true,
        close: function () {
            $(".dialog-request-edit").remove();
        },
        open: function () {
            //Отсылаем запрос на получение диалога
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/add",
                data: data,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));
                    });
                }
            });
        }
    });
};

/**
 * Открывает окно редактирования
 * @param {bool} isTest
 */
request.openEdit = function (isTest) {
    var dialog = {
        width: Math.round(screen.width * 0.945),
        height: Math.round(screen.height * 0.843)
    };

    var dContent = $(
            '<div class="dialog-request-edit">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

    console.log(isTest);
    console.log(requestId);
    var requestId = "test";
    if (!isTest)
        requestId = $(".row_appl.active_appl").attr("id").split("_")[1];
    if (isTest && isTest !== true) {
        requestId = isTest;
    }
    console.info('преред дерг 2');
    console.log(requestId);    

    $(dContent).dialog({
        title: "Редактирование заявления",
        width: dialog.width,
        height: dialog.height - 67,
        modal: true,
        close: function () {
            //RequestFrames.save(true);
            $(".dialog-request-edit").remove();
            if(RequestFrames.needRefreshDeal()){
                //рефрешим диалог с делом
                //закрыть дело
                jQuery('.dialog-deal-edit').remove();
                //открыть дело
                affairs.openEditAffair();
                //рефрешим списки в таблице
                $(".dealFilterWrap .btApply").click();
            }
        },
        open: function () {
            //Отсылаем запрос на получение диалога
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/edit/" + requestId,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));
                        RequestFrames.init();
                    });
                }
            });
        }
    });
};

request.openEditFrames = function () {
    request.openEdit(true);
};

request.searchOrg = function(){
    url = encodeURI(url);//персонально для ИЕ
    $.ajax({
        type: 'POST',
        url: url,
        success: function (answer) {
            $("#popupWindow").html(answer);
            $("#popupWindow").show();
        }
    });
}

/**
 * творим случ число
 * 
 * @param {type} id
 * @returns {Number}
 */
var getRandomInt = function getRandomInt(min, max) {
                      return Math.floor(Math.random() * (max - min)) + min;
                    }
                    
/**
 * делаем запрос  на этот вуз запись на прием в у вуза,
 * если есть то получим фио записавшегося и впихиваем это в форму
 *
 * @param {type} id - ид вуза
 * @returns {Number}
 */
request.getFioFromEqueue = function (id){
        if( 'undefinsed' == typeof (id) || '' == id ){
            return 0;
        }
        $("#prescriptions-container").html("Запрос информации...");
        $.ajax({
            url: '/islod/requests/get-fio-from-equeue/' + id,
            dataType: 'json',
            type: 'POST',
            success: function (answer) {
                console.log('дела - новое заявление - выбор организации - запрос в очередь - ответ получен');
                //результирующий отраблтчик
                var doResult = function(i){
                            if ( 'undefined' == typeof (i) ){
                                i = 0;
                            }
                            i = parseInt(i);
                            if ( 'undefined' != typeof (answer['data']) ){
                                //answer = jQuery.parseJSON(answer);
                                if ( "feb3633090b54d709c7a476f0982c3d6" == jQuery('#admissionType.dataField[name="admissionType"]').val() ){
                                    if ( 'undefined' != typeof (answer['data'][i]) ){
                                        if ( 'undefined' != typeof (answer['data'][i]['lastName'])
                                            &&
                                            '' == jQuery('.dataField[name="requestRepresentativeLastName"]').val()
                                            ){
                                            jQuery('.dataField[name="requestRepresentativeLastName"]').val(answer['data'][i]['lastName']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['firstName'])
                                            &&
                                            '' == jQuery('.dataField[name="requestRepresentativeFirstName"]').val()
                                            ){
                                            jQuery('.dataField[name="requestRepresentativeFirstName"]').val(answer['data'][i]['firstName']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['middleName'])
                                            &&
                                            '' == jQuery('.dataField[name="requestRepresentativeMiddleName"]').val()
                                            ){
                                            jQuery('.dataField[name="requestRepresentativeMiddleName"]').val(answer['data'][i]['middleName']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['phone'])
                                            &&
                                            '' == jQuery('.dataField[name="mobilePhoneNumber"]').val()
                                            ){
                                            jQuery('.dataField[name="mobilePhoneNumber"]').val(answer['data'][i]['phone']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['email'])
                                            &&
                                            '' == jQuery('.dataField[name="eMail"]').val()
                                            ){
                                            jQuery('.dataField[name="eMail"]').val(answer['data'][i]['email']);
                                        }
                                    }
                                }
                                // если из очереди приехала процедура, по выберем ее
                                if ( 'undefined' != typeof (answer['data'][i]) ){
                                    if ( 'undefined' != typeof (answer['data'][i]['lisensingProcedureId']) ){
                                        if( jQuery('.dataField[name="licensingProcedureId"] option[value="'+answer['data'][i]['lisensingProcedureId']+'"]').length){
                                            jQuery('.dataField[name="licensingProcedureId"] option[value="'+answer['data'][i]['lisensingProcedureId']+'"]').prop('selected','selected');
                                            jQuery('.dataField[name="licensingProcedureId"] option[value="'+answer['data'][i]['lisensingProcedureId']+'"]').change();
                                            //проставим галочки по причинам, если есть
                                            if ( 'undefined' != typeof (answer['data'][i]['licenseReasonAllId']) ){
                                                for (var i in answer['data'][i]['licenseReasonAllId']){
                                                    if( jQuery('.cbField[type="checkbox"][value="'+answer['data'][i]['licenseReasonAllId'][i]+'"]').length){
                                                        jQuery('.cbField[type="checkbox"][value="'+answer['data'][i]['licenseReasonAllId'][i]+'"]').prop('checked','checked');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                

                //разбираем ответ  
                request.getFioFromEqueueParse(answer, doResult);
                /*
                if ( 'undefined' != typeof (answer['data']) ){
                    //определим количесто приехавших зашисей очередей
                    var colEque = answer['data'].length;
                    if(colEque>1){
                        //если много записей
                        jQuery('<div class="select-eque-dialog"></div>').dialog({
                           'title': 'Выбор записи из очереди',
                           'modal': true,
                           'width':440,
                           'open': function(){
                                jQuery(this).append('<form><table class="select-eque-dialog-table" ></table></form>');
                                for (var i in answer['data']){
                                    var title = '';
                                    if ( 'undefined' != typeof (answer['data'][i]['lisensingProcedureTitle']) ){
                                        title = answer['data'][i]['lisensingProcedureTitle'];
                                    }
                                    else{
                                        title = 'не определена причина';
                                    }
                                    var id = 'select-eque-dialog-input-id-'+getRandomInt(1000,9999)+getRandomInt(1000,9999)+getRandomInt(1000,9999);
                                    var label = jQuery('<label></label>').append(title);
                                    jQuery(label).attr('for', id);
                                    var input = jQuery('<input >');
                                    jQuery(input).attr('type', 'radio');
                                    jQuery(input).attr('id', id);
                                    jQuery(input).attr('name', 'radioName');
                                    jQuery(input).attr('value',i);
                                    jQuery(input).attr('class', 'select-eque-dialog-input');
                                    var td1 = jQuery('<td></td>').append(label);
                                    var td2 = jQuery('<td></td>').append(input);
                                    var tr  = jQuery('<tr></tr>').append(td1);
                                    tr.append(td2);
                                    jQuery(this).find('.select-eque-dialog-table').append(tr);
                                }
                           },
                           'close': function (){
                                jQuery(this).remove();
                            },
                            'buttons':{
                                'Сохранить': function(){
                                    if(jQuery(this).find('input.select-eque-dialog-input:checked').length){
                                        doResult( jQuery(this).find('input.select-eque-dialog-input:checked').val() );
                                        jQuery(this).dialog('close');
                                    }
                                    else{
                                        useTopHoverTitleError('выберите запись из очереди');
                                    }
                                },
                                'Отмена': function (){
                                    jQuery(this).dialog('close');
                                }
                            }

                        });
                    }
                    else{
                        //усли одна запись
                        doResult( );
                    }
                }
                */
            },
            error:function(e){
                //сбросим сообщения
                useTopHoverTitle("",1);
                useTopHoverTitleError("Ошибка связи");
                console.error('дела - новое заявление - выбор организации - ошибка связи при дергании данных очереди');
                console.log(e);
                console.log(e.responseText);
            },
            
        });
    }
    
/**
 * разборка 
 * @param {type} answer
 * @returns {undefined}
 */    
request.getFioFromEqueueParse = function (answer, doResult){  
                if ( 'undefined' != typeof (answer['data']) ){
                    //определим количесто приехавших зашисей очередей
                    var colEque = answer['data'].length;
                    if(colEque>1){
                        //если много записей
                        jQuery('<div class="select-eque-dialog"></div>').dialog({
                           'title': 'Выбор записи из очереди',
                           'modal': true,
                           'width':440,
                           'open': function(){
                                jQuery(this).append('<form><table class="select-eque-dialog-table" ></table></form>');
                                for (var i in answer['data']){
                                    var title = '';
                                    if ( 'undefined' != typeof (answer['data'][i]['lisensingProcedureTitle']) ){
                                        title = answer['data'][i]['lisensingProcedureTitle'];
                                    }
                                    else{
                                        title = 'не определена причина';
                                    }
                                    var id = 'select-eque-dialog-input-id-'+getRandomInt(1000,9999)+getRandomInt(1000,9999)+getRandomInt(1000,9999);
                                    var label = jQuery('<label></label>').append(title);
                                    jQuery(label).attr('for', id);
                                    var input = jQuery('<input >');
                                    jQuery(input).attr('type', 'radio');
                                    jQuery(input).attr('id', id);
                                    jQuery(input).attr('name', 'radioName');
                                    jQuery(input).attr('value',i);
                                    jQuery(input).attr('class', 'select-eque-dialog-input');
                                    var td1 = jQuery('<td></td>').append(label);
                                    var td2 = jQuery('<td></td>').append(input);
                                    var tr  = jQuery('<tr></tr>').append(td1);
                                    tr.append(td2);
                                    jQuery(this).find('.select-eque-dialog-table').append(tr);
                                }
                           },
                           'close': function (){
                                jQuery(this).remove();
                            },
                            'buttons':{
                                'Сохранить': function(){
                                    if(jQuery(this).find('input.select-eque-dialog-input:checked').length){
                                        doResult( jQuery(this).find('input.select-eque-dialog-input:checked').val() );
                                        jQuery(this).dialog('close');
                                    }
                                    else{
                                        useTopHoverTitleError('выберите запись из очереди');
                                    }
                                },
                                'Отмена': function (){
                                    jQuery(this).dialog('close');
                                }
                            }

                        });
                    }
                    else{
                        //усли одна запись
                        doResult( );
                    }
                }
    
}


/**
 * Метод обрабатывает диалог выбора организации
 * @param callback - функция обработки результатов выбора
 */
request.selectOrgDialog = function(callback){

    function getPrescriptions(id){
        $("#prescriptions-container").html("Запрос информации...");
        $.ajax({
            url: '/islod/requests/get-prescriptions-by-org/' + id,
            success: function (answer) {
                $("#prescriptions-container").html($(answer));
                $("#prescriptions-container")
            },
            type: 'POST',
            dataType: "html"
        });
    }



    function searchOrgAdd(page, needClear, type){
        if(page == null || page != 0){
            var nextPage=$("#nextPage").val();
            nextPage=parseInt(nextPage);
            var maxPage=$("#maxPage").val();
            maxPage=parseInt(maxPage);
            page= nextPage;
        }

        if(/* $("#filteredTable tbody").height()-$("#filteredTable").scrollTop() == 359
             && */page==0 || nextPage<= maxPage ){
            if($(".searchOrganization").val()==''){
                var url='/eo/requests/get-organizations-select/'+page;
            }else{
                var url='/eo/requests/get-organizations-select/'+page+'/'+$(".searchOrganization").val();
            }
            $.ajax({
                url: url,
                beforeSend: function (xhr) {
                    $(".loading-overlay").show();
                    $("#getNextResult").parent().parent().remove();
                    $("#nextPage").remove();
                    $("#maxPage").remove();
                },
                success: function(answer){
                    console.info("дела - новое дело - заявление - получение еще организаций - ответ получен");
                    //почемуто рисовалка дергается 2 раза, вставим клин
                    var isAlredyShow = false;
                    $(".loading-overlay").fadeOut(250, function(){
                        console.info("дела - новое дело - заявление - получение еще организаций - вставим ответ");
                        if( isAlredyShow ){
                            return 0;
                        }
                        if(needClear){
                            $(".dialog-org-select").html("").append(answer);
                        } else {
                            $("#filteredTable tbody").append(answer);
                        }
                        isAlredyShow = true;
                    })
                },
                error:function(e){
                    useTopHoverTitleError("Возникла ошибка");
                    $(".loading-overlay").fadeOut(250);
                    console.log("Ошибка");
                    console.log(e);
                }
            });
        }
    }

    //Функция

    var dialog = {
        width: 646,
        height: 446
    };

    var dContent = $(
        '<div class="dialog-org-select">' +
            "<div style='text-align: center;margin-top:50px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
        '</div>');

    $(dContent).dialog({
        title: "Выбор организации",
        width: dialog.width,
        height: dialog.height,
        modal: true,
        close: function () {
            $(".dialog-org-select").remove();
        },
        open: function () {
            //Отсылаем запрос на получение диалога
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/get-organizations-select/0",
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));

                        //фокус в сирчинпут
                        var el=document.getElementById('filt_fillials');
                        el.focus();
                        el.setSelectionRange(el.value.length,el.value.length);

                        //обработчики
                        dContent.undelegate("#getNextResult");
                        dContent.delegate("#getNextResult","click",function(){
                            searchOrgAdd();
                        });
                        dContent.undelegate(".searchOrganization","change");
                        dContent.delegate(".searchOrganization","change",function(){
                            /*
                            if($("#add_reorg_struct").hasClass('clicked')){
                                showPop('/ron/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
                            }else{
                                showPop('/ron/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
                            }*/
                            searchOrgAdd(0, true);
                        });
                        $(dContent).delegate("#add_org","click", function(){
                            if(callback) {
                                var idOrg = $('.selected_org:checked').val();
                                var orgName = $('.selected_org:checked').attr('org_name');
                                var lawAddress = $('.selected_org:checked').attr('lawaddress');
                                callback(idOrg, orgName, lawAddress);
                                $(".dialog-org-select").remove();
                            } else {
                                $("#Id_organization").val($('.selected_org:checked').val());
                                $("#nameOrganization").val($('.selected_org:checked').attr('org_name'));
                                getPrescriptions($('.selected_org:checked').val());
                                request.getFioFromEqueue($('.selected_org:checked').val());
                                $(".dialog-org-select").remove();
                            }
                        });
                        //
                        $("#filteredTable", dContent).undelegate(".reorg_string","click");
                        $("#filteredTable", dContent).delegate(".reorg_string","click",function(){
                            console.log('тык по строчке');
                            console.log(this);
                            jQuery("input",this).prop("checked",true);
                        });

                        $(dContent).delegate(".btClose", "click", function(){
                            $(".dialog-org-select").remove();
                        });
                    });
                }
            });
        }
    });
}

/**
 * Функция отображает дилог с списком предписаний
 * @param orgId
 */
dialog_showPrescriptions = function(orgId){
    var dContent = $(
        '<div class="dialog-prescriptions">' +
        "<div style='text-align: center;margin-top:40px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "Неисполненные предписания организации",
        width: 800,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-prescriptions").remove();
        },
        open: function() {
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/get-org-prescriptions/" + orgId,
                success: function(answer) {
                    //console.log(answer);
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        $(".btClose", dContent).click(function(){
                            $(".dialog, .dialog-prescriptions").remove();
                        });
                    });
                }
            });
        }
    });
}





