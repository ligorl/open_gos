/**
 * отправленные заявления 
 * 
 * path: /public/js/eo/license/reply.js
 * url:  /js/eo/license/reply.js
 * 
 */

    console.log('лицензии - правленные заявления - загружаем скрипт - sended.js');
 
    indexSendedHandlers();
    
    
    /**
     * тык по табличке с зачявлениями
     *
     * @param {type} answer
     * @returns {undefined}
     */
    function sendedRequestClickAction(answer){
        console.log('добавим обработчик тыка по табличке');
        $('table.intable.license_table.license-table-content tbody tr ').undelegate('td','click');
        $('table.intable.license_table.license-table-content tbody tr ').delegate('td','click', function(){
            console.log('был тыка по табличке');
            $('table.intable.license_table.license-table-content tbody tr').removeClass('select');
            var tr = jQuery(this).parents('tr')[0];
            var requetsid = jQuery(tr).attr('requetsid');
            if ( 'undefined' == typeof(requetsid) ){
                console.log('открытай - лицензироканий - тук в строку холостого');
                return 0;
            }                        
            jQuery(tr).addClass('select');
        });
        $('table.intable.license_table.license-table-content tbody tr ').undelegate('td','dblclick');
        $('table.intable.license_table.license-table-content tbody tr ').delegate('td','dblclick', function(){
            console.log('был  тык-тык тыка по табличке');
            jQuery(this).click();
            var tr = jQuery(this).parents('tr')[0];
            var requetsid = jQuery(tr).attr('requetsid');
            if ( 'undefined' == typeof(requetsid) ){
                console.log('открытай - лицензироканий - тук-тык не в тему');
                return 0;
            }                        
            newRequestOpenAction(this);
        });
    }
