/**
 * Всплывающие диалоговое окно с информацией по платежам.
 * Работае как в окне Лицензии так и в окне приложения (если передан параметр sap).
 *
 * @param id
 * @param sap
 */
function requisitesPaymentNotification(id, sap) {
    var date;
    if(sap){
        date = paymentNotificationSap[id];
    }else{
        date = paymentNotification[id];
    }


    //надо открыть диалоговое окно добавления платежки
    var notification = date.number + ' от ' + date.date;
    var paymentDialog
        = '<div class="payment-notification-dialog">'
        + '<table>'
        + '<tr>'
        + '<td>'
        + 'Номер документа'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-number-document">'+ date.number +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'Дата оплаты'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-date">'+ date.date +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'Сумма'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-sum">'+ date.sum +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'Расчетный счёт'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-edu-number">'+ date.numEduAccount +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'Банк плательщика'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-bank-name">'+ date.bankName +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'БИК банка'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-BIK-bank">'+ date.bankBIK +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'Номер счета'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-edu-bank-number">'+ date.corAccount +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'УИН'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-UIN">'+ date.uin +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'КБК'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-KBK">'+ date.kbk +'</span>'
        + '</td>'
        + '</tr>'
        + '<tr>'
        + '<td>'
        + 'Статус'
        + '</td>'
        + '<td>'
        + '<div class="relative_block">'
        + '<span id="field-status">'+ date.status +'</span>'
        + '</td>'
        + '</tr>'
        + '</table>'
        + '</div>';

    jQuery(paymentDialog).dialog({
        'title': 'УВЕДОМЛЕНИЕ №' + notification,
        'modal': true,
        width: 440,
        //'position': ['center', 'top'],
        'open': function () {
            $(".hasDate", this).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd.mm.yy"
            });
            //центровка окна
            var win = $(window);
            $(this).parent().css({
                position: 'absolute',
                left: (win.width() - $(this).parent().outerWidth()) / 2,
                top: (win.height() - $(this).parent().outerHeight()) / 2
            });
        },
        'close': function () {
            jQuery(this).remove();
        },
        'buttons': {
            'Закрыть': function () {
                jQuery(this).dialog('close');
            }
        }
    });
}