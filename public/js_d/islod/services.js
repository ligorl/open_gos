
/**
 * Простая надстройка над аяксом
 * возвращает объект с готовыми типами запросов
 * @param {type} setting
 * {
 *    checkLevel :  request||data
 *    это уровень проверки ошибок. При data  обязывает входящий js объект иметь параметр status("ok"-если все нормально),
 *    и параметр data как возвращаеме данные,
 *    и желательно error как код возвращаемой ошибки
 *
 *    rootPath : 'Корневой путь для экшенов',
 *    defaultErrorMessage : 'сообщение об ошибке по ум.'
 * }
 * @returns {_L8}
 */
function _REQUEST( setting){
    var setting = setting || {};

    return new function(){
       var  _setting = {
                checkLevel : 'request', // request||data
                rootPath : '/eo/index/',
                defaultErrorMessage : 'Ошибка! Перезагрузите страницу, и попробуйте еще раз...',
                successMessage : null,
                transformUrl : true
            },
            _status = null,
            dataContainer = {};
    
        settingSetter( _setting, setting)
        //конвертируем get в строку запроса
        function GetObjToStr( getObj){
            var counter = 0;
            getString = '';

            if( empty(getObj)){
                return '';
            }

            for( var key in getObj){
                var elem = getObj[ key];

                if( counter>1){
                    getString += '&';
                }else{
                    getString += '?';
                }
                 getString +=  elem;
            }
            return getString;
        }

        //преобразует имя экшена в часть url
        function actionNamePreProcessor( actionName){
            var actionUrl = '',
                strLen = actionName.length;

             for(var i=0; i<strLen; i++ ){
                 var currentChar = actionName[ i ];

                 if( isUpper(currentChar)){
                    actionUrl += '-'+currentChar.toLowerCase();
                 }else{
                     actionUrl += currentChar;
                 }
             }
             return actionUrl;
        }

        //действие при ошибке
        function whenError( errorMessageOrCallback, _status ,error){
            switch( typeof(errorMessageOrCallback)){
                    case 'function':
                        errorMessageOrCallback( _status, error);
                    break;
                    case 'string':
                        useTopHoverTitle( errorMessageOrCallback, 5000);
                    break;
                    default:
                       useTopHoverTitle( _setting['defaultErrorMessage'], 5000);
                }
        }
        //Действия если все хорошо
        function whenSuccess(){
            switch( typeof( _setting['successMessage'])){
               case 'function':
                   succesMessageOrCallback();
               break;
               case 'string':
                   useTopHoverTitle( _setting['successMessage'], 5000);
               break;
               default:

           }
        }

        //обработка после получения ответа
        function requestPostPostprocessing( errorMessageOrCallback){
            //проверяем на ошибку
            if( _status !=  "success" ){
                whenError( errorMessageOrCallback, _status ,null);
            }else{
                //проверяем нужна ли проверка на уровне данных
                if( _setting['checkLevel'] == 'data' ){
                    if(dataContainer['status'] != 'ok'){
                        whenError( errorMessageOrCallback, _status ,dataContainer['error']);
                    }else{
                        whenSuccess();
                    }

                    if( !empty( dataContainer['data'])){
                        dataContainer = dataContainer['data']
                    }else{
                        console.log('Warning: [data] no detecting in answer!');
                        dataContainer = {};
                    }
                    
                 }else{
                      whenSuccess();
                 }
            }
            return dataContainer;
        }

         /**
          * посылаем синхронный запрос
          * @param {type} actionName
          * имя экшена на который летит запрос(автоматически преобразуеться url виду)
          * @param {type} postData
          * объект данных
          * @param {type} errorMessageOrCallback
          * callback или сообщение об ошибке
          * @param {type} GetData
          * Get параметры
          * @returns {data}
          */
        this.noAsunc = function( actionName, postData,errorMessageOrCallback ,GetData ){
            //очищаем контейнеры
            _status = 'no post';
            dataContainer = {};
            //генерируем url
            var actionUrl = '';
            
            if( _setting['transformUrl'] == false){
                actionUrl = actionName;
            }else{
                actionUrl = actionNamePreProcessor( actionName);
            }

            var url = _setting['rootPath']+actionUrl+GetObjToStr( GetData);

            $.ajax({
                type: 'POST',
                url: url,
                data: postData,
                async:false,

                success:function (data,status){
                    _status = status;
                    dataContainer = data;
                },

                error:function(jqXHR, exception){
                    _status = jqXHR.status;
                    console.log(exception);
                }
            });

            return requestPostPostprocessing( errorMessageOrCallback);
        };
    }
}


/**
 *  запросы на получение страницы
 * @returns {_L89}
 */
function _getPage( setings ){
    var rSetings = setings;
    return new function( data ){
        var request = _REQUEST(rSetings);

        function checkPageReturned( page){
            if( !empty(page)){
                return page;
            }
            return false;
        }
        
        this.supplementInfoFromId = function( id){
            return checkPageReturned(
                        request.noAsunc( 'supplementInfoPage', {
                                id : id
                            })
                    );
        };

        this.eduProgramsTabWhereSupplemenId = function( id){

            return checkPageReturned(
                    request.noAsunc( 'educationalProgramsTab', {
                            id : id
                        })
            );
        };

        this.organizationInfoPageFromId = function( id){

            return checkPageReturned(
                    request.noAsunc( 'organizationInfoPage', {
                        id : id
                    })
            );
        };

        this.getFirstFoundersPage = function( organizationId){

            return checkPageReturned(
                    request.noAsunc( 'getFirstFounders',{ id : organizationId })
            );
        };
        
        this.getAllFoundersPage = function( organizationId){

            return checkPageReturned( 
                    request.noAsunc( 'getAllFounders',{ id : organizationId })
            );
        };

        this.getFoundersPagesTest = function( data){

            return checkPageReturned(
                    request.noAsunc( 'getFounders', data)
            );
        };
        
        this.newOrganizationPage = function(orgId){
            var postData = {};
            if(orgId)
                postData["orgId"] = orgId

            return checkPageReturned(
                    request.noAsunc( 'newOrganization', postData)
            );
        };

        this.licenseFromOrg = function( organizationId){

            return checkPageReturned(
                    request.noAsunc( 'getLicenseFromOrganization',{id:organizationId})
                );
        };

        this.supplementFromOrg = function( organizationId, supplementId){

            return checkPageReturned( request.noAsunc( 'getSupplementFromOrganization',{
                            organizationId:organizationId,
                            supplementId:supplementId
                        })
                );
        };

        this.branchFromOrg = function( organizationId){

            return checkPageReturned(
                    request.noAsunc( 'getBranchFromOrganization',{
                            id : organizationId,
                        })
                );
        };

        this.documentFromOrg = function( organizationId){

            return checkPageReturned(
                    request.noAsunc( 'getDocumentFromOrganization',{
                            id : organizationId,
                        })
                );
        };

        this.createFoundersPage = function( organizationId){

            return checkPageReturned(
                    request.noAsunc( 'createFoundersPage')
                );
        };

        this.notificationFromId = function( docId){

            return checkPageReturned(
                    request.noAsunc( 'getNotificationDocument',{ id : docId })
                );
        };

        this.dealList = function( orgId, filters, order , page){
            return checkPageReturned(
                    request.noAsunc( 'getDealListFromOrg',{ id : orgId , filters: filters, order:order, page:page})
                );
        }

        this.getHeadOrgList = function(data){
            return checkPageReturned(
                    request.noAsunc( 'getHeadOrganizationList',data)
                );
        }

        this.prescriptionsDialog = function(orgId){
            var tempRequest = _REQUEST({transformUrl:false});
            return checkPageReturned(
                    tempRequest.noAsunc( 'get-organization-prescriptions/'+orgId)
                );
        }
        
        this.userList = function( data){
             return checkPageReturned(
                    request.noAsunc( 'users',data)
                );
        }

        this.getUser = function( id){
             return checkPageReturned(
                    request.noAsunc( 'getUser', { id:id })
                );
        }

        this.newUser = function( id){
             return checkPageReturned(
                    request.noAsunc( 'newUser')
                );
        }

        this.roleMatrix = function( id){
             return checkPageReturned(
                    request.noAsunc( 'getRoleMatrix')
                );
        }

        this.programs = function( data){
             return checkPageReturned(
                    request.noAsunc( 'programsPage', data)
                );
        }

        this.educationLocationList = function( suppId){
             return checkPageReturned(
                    request.noAsunc( 'getEducationLocationList', { supplementId : suppId, rootPath : '/eo/license/' } )
                );
        }

        this.administrativeDocument = function( orgId){
             return checkPageReturned(
                    request.noAsunc( 'administrativeDocument', { organizationId : orgId } )
                );
        }

        this.getCityFromRegion = function( regionId){
             return checkPageReturned(
                    request.noAsunc( 'getCityFromRegion', { regionId : regionId } )
                );
        }

        this.inventory = function(){
            return checkPageReturned(
                    request.noAsunc( 'inventory' )
                );
        }

        this.inventoryAndUser = function(){
            return checkPageReturned(
                    request.noAsunc( 'inventoryAndUser' )
                );
        }
        
        this.requestFromOrg = function( orgId){
            return checkPageReturned(
                    request.noAsunc( 'requestFromOrg', {'id':orgId} )
                );
        }

        this.calendar = function( data){
            return checkPageReturned(
                    request.noAsunc( 'calendar', data )
                );
        }

        this.accreditationFromOrg = function( data){
            return checkPageReturned(
                    request.noAsunc( 'getAccreditationFromOrg', data )
                );
        }

        this.certificateSupplement = function( data){
            return checkPageReturned(
                    request.noAsunc( 'getCertificateSupplement', data )
                );
        }

        this.applicationInfo = function( data){
            return checkPageReturned(
                    request.noAsunc( 'getApplicationInfo', data )
                );
        }
        
        this.fullOrgFilter = function(){
            return checkPageReturned(
                    request.noAsunc( 'getOrganizationFullFilter')
                );
        }

        this.applyFullOrgFilter = function( data){
            return checkPageReturned(
                    request.noAsunc( 'orgFullFilter', data)
                );
        }
    };
}

/**
 *  запросы на получение данных
 * @returns {_L89}
 */
function _getData(){
    return new function(data){
        var request = _REQUEST({
            checkLevel : 'data',
            defaultErrorMessage : 'Ошибка: невозможно получить данные!'
        });

        
        this.foundersFromOrgnizationId = function( id){
            var data = request.noAsunc( 'getFoundersFromOrganizationId',
                {
                    id : id
                });

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };

        this.getAllFounders = function(){
            var data = request.noAsunc( 'organizationInfoPage');

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };

        this.generateLoginOrPassword = function( strToConvert){
            var data = request.noAsunc( 'generateUserData', { data:strToConvert});

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };

        this.morphyOrganization = function( orgName){
            var data = request.noAsunc( 'morphyOrganization', { orgName : orgName });

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };
        
        this.report = function( dt ){
            var data = null;
            var dt = dt || {};
            data = request.noAsunc( 'startGenerateReport', dt);
            

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };
        
        this.reportProgress = function( type){
            var data = request.noAsunc( 'reportProgress', { type : type });

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };

        this.reportFinish = function( type){
            var data = request.noAsunc( 'reportFinish', { type : type });

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };
        
        this.inventoryLicense = function( data){
            var data = request.noAsunc( 'getLicenseInventory', data );

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };
        
        this.changeLicAndSuppDate = function( data){
            var data = request.noAsunc( 'changeLicAndSuppDate', data );

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };
        this.changeDayStatus = function( data){
            var data = request.noAsunc( 'changeDayStatus', data );

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };
        this.morphyTest = function( str){
            var data = request.noAsunc( 'morphyTest', { str : str });

            if( !empty(data)){
                return data;
            }else{
                return false;
            }
        };
    };
}

/**
 *  запросы на получение данных
 * @returns {_L89}
 */
function _delete(){
    return new function(data){
        var request = _REQUEST({
            checkLevel : 'data'
        });


        this.foundersFromOrgnizationId = function( id){
            var resp = request.noAsunc( 'deleteFoundersFromOrganization',{
                            id : id
                        });
        };

        this.deleteEduProgramFromSupplement = function( programId, supplementId){
            return request.noAsunc( 'deleteEduProgramFromSupplement',{
                            programId : programId,
                            supplementId : supplementId
                        });
        };
        
        this.deleteLocationFromSupplement = function( locationid){
            return request.noAsunc( 'deleteLocationFromSupplement',{
                            locationid : locationid
                        });
        };
        
    }
}

/**
 *  запросы на сохранение
 * @returns {_L89}
 */
function _save(){
    return new function(){
        var requset = _REQUEST({
            checkLevel : 'data',
            successMessage : 'Данные успешно сохранены',
            defaultErrorMessage : 'Ошибка при сохранении данных'
        });

        this.supplementInfo = function( data){
            return requset.noAsunc( 'supplementInfoSave', data);
        }
        this.addFounderToOrg = function( data){
            return requset.noAsunc( 'addFoundersToOrganization', data);
        }
        this.organizationPageSave = function( data){
            return requset.noAsunc( 'organizationPageSave', data);
        }
        this.newOrganizationPageSave = function( data){
            return requset.noAsunc( 'newOrganizationPageSave', data);
        }
        this.saveNewFounders = function( data){
            return requset.noAsunc( 'saveNewFounders', data);
        }
        this.user = function( data){
            return requset.noAsunc( 'saveUser', data);
        }
        this.role = function( name){
            return requset.noAsunc( 'saveRole', { 'name':name},'Ошибка при сохранении данных!');
        }
        this.roleMatrix = function( data){
            return requset.noAsunc( 'saveRoleMatrix', data);
        }
    }
}

/**
 * Переключние вкладок с анимацией
 * @param {type} tabToUnActive
 * @param {type} tabToActive
 * @param {type} animationBlock
 * @param {type} callbck
 * @returns {undefined}
 */
function tabsThumbler( tabToUnActive, tabToActive, animationBlock, callbck){
    var animBlType =  typeof(animationBlock),
        showBlockClass = '.'+$(tabToActive).attr("tab-container"),
        hideBlockClass = '.'+$(tabToUnActive).attr("tab-container"),
        downloadAnimation = _getAnimation().dContent();
    
    $( tabToUnActive).removeClass('active');
    $( hideBlockClass).hide();

    if( empty( _storage('tabs').get('timer')) ){
        if( animBlType == 'string' ){
            if( typeof( downloadAnimation)=='object'){
                downloadAnimation.addClass('temp-animation-block-code-nadwi87r3');
            }
           $( animationBlock).append( downloadAnimation);
        }
        //Создаем поочередность чтобы не провисало добавление анимации при запросах
        var timer = setTimeout(function(){
            var cType = typeof(callbck);

            if( cType == 'function'){
                callbck();
            }
            setTimeout(function(){
                $('.temp-animation-block-code-nadwi87r3').remove();
                $( tabToActive ).addClass('active');
                $( showBlockClass).show();
                _storage('tabs').add('timer',null);
            });
            

        });
        _storage('tabs').add('timer',timer);
    }

}

/**
 * Хранилище в глобальной переменной
 * Возвращает объект со списком действий
 * @param {type} storage
 * @returns {_L355}
 */
function _storage( storage){
    window.LOD_APPLICATION_STORAGE = window.LOD_APPLICATION_STORAGE || {};
    
    var functionStorage = window.LOD_APPLICATION_STORAGE,
        storage = storage || 'defaut';

    functionStorage[ storage] =  functionStorage[ storage] || {};
    
    return new function(){
        this.addOnce = function( key, value){
            if( functionStorage[ storage] [ key] === undefined){
                functionStorage[ storage] [ key] = value;
            }
        };

        this.add = function( key, value){
            functionStorage[ storage] [ key] = value;
        };
        
        this.get = function( key){
            if( functionStorage[ storage] [ key] !== undefined){
                return  functionStorage[ storage] [ key];
            }else{
                return  undefined;
            }
        };
        this.clear = function( key){
            if( functionStorage[ storage] [ key] !== undefined){
                delete functionStorage[ storage] [ key];
            }
        };
    }
}

function getArrSubElementFromArr( arr, needSubEl){
    if( empty(arr)){
        return [];
    }
    var len = arr.length,
        retArr = [];
    for( var i=0; i<len; i++ ){
        var el = arr[i];
        retArr.push( el[needSubEl]);
    }
    return retArr;
}

function inputCurrencyValidator( input ){
    var val = $(input).val(),
        splitted = [],
        ret = '';

    val = val.replace(/[^0-9\.\,]+/gim,'');
    if(val.indexOf(',') == -1 && val.indexOf('.')  == -1){
        $(input).val(val);
        return val;
    }
    
    if( val.indexOf(',') != -1){
        splitted = val.split(',');
    }

    if( val.indexOf('.')  != -1){
        splitted = val.split('.');
    }

    if( splitted.length>0 ){
        ret = splitted[0];

        if( splitted[1] != undefined){
            ret += '.'+splitted[1].substr(0,2);
            $(input).val(ret);
            return ret;
        }
    }
    $(input).val(val);
    return val;
}

function _FILESENDER( setting){
    var setting = setting || {};

    return new function(){
        var  _setting = {
                url : '/islod/index/send-file',
                precent : 0,
                input : 'input[type="file"]',
                beforeSend : null,
                afterSend : null,
                startMessage : 'Подготовка к загрузке...',
                loadMessage : 'Идет загрузка...',
                finishMessage : 'Загружено!',
                ownerClassCode : $('input[type="file"]').attr('ownerClassCode'),
                ownerId : $('input[type="file"]').attr('ownerId'),
            };

        settingSetter( _setting, setting);

        this.send = function(){
            var fd = new FormData(),
                file = $( _setting.input)[0].files[0];

            if( typeof( _setting.beforeSend) == 'function'){
                        _setting.beforeSend();
                    }
            
            if( $(_setting.input) == "undefined" || $(_setting.input).length == 0 || $(_setting.input).val() == '' ){
                useTopHoverTitleError('нет файла');
                return false;
            }

            fd.append('file', file );
            fd.append('ownerClassCode', _setting.ownerClassCode );
            fd.append('ownerId', _setting.ownerId );
            
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: fd,
                processData: false,
                contentType: false,
                url: _setting.url,
                beforeSend: function(){
                    if( $("#hover-title").length==0){
                        $('body').prepend('<span id="hover-title"></span>');
                    }
                    
                    $("#hover-title").hide();
                    $("#hover-title").html( _setting.startMessage);
                    $("#hover-title").fadeIn("fast");
                },
                xhr: function(){
                    var xhr = $.ajaxSettings.xhr();
                    // Устанавливаем обработчик
                    if(xhr.upload){
                        xhr.upload.addEventListener(
                                'progress',
                                function(evt){
                                    if (evt.lengthComputable){
                                        _setting.precent = Math.ceil(evt.loaded / evt.total * 100);
                                        $("#hover-title").html( _setting.loadMessage + _setting.precent + '%');
                                    }
                                },
                                false
                        );
                    }else{
                        console.error('ошибка!');
                    }
                    return xhr;
                },
                success: function(answer) {
                    if(answer.status == 'Ok'){
                        if( typeof( _setting.afterSend) == 'function'){
                            _setting.afterSend();
                        }
                        $("#hover-title").html( _setting.finishMessage);
                    }else{
                        $("#hover-title").html( 'Ошибка!');
                    }
                    $("#hover-title").fadeOut("slow");
                    _setting.precent = 0;
                }
            });
            
            return true;
        };
    }
}