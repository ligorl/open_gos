/* 
 * Файл с Биндами
 * ПОЖАЛУЙСТА ПРИ СОЗДАНИИ БИНДОВ ДОБАВЛЯЙТЕ ИМ МЕТОДЫ *.bind() и *.unbind()
 */

/**
 * биндер для списка юзеров
 * @returns {_L262}
 */
function userBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                generatePasswordBt : '.user-dialog-block .generate-password',
                generateLoginBt : '.user-dialog-block .generate-login',
                name : '.user-dialog-block input[name="Name"]',
                loginInp : '.user-dialog-block input[name="Login"]',
                passwordInp : '.user-dialog-block input[name="Password"]',
                
            };

        //связывание
        this.bind = function(){
            
            $( 'body').on('click', selectors.generateLoginBt, function(){
                var fio = $( selectors.name).val();
                if( empty(fio)){
                    fio = '';
                }
                var login = _getData().generateLoginOrPassword( fio);
                $( selectors.loginInp).val( login);
            });
            
            $( 'body').on('click', selectors.generatePasswordBt, function( e){
                var password = _getData().generateLoginOrPassword( '');
                $( selectors.passwordInp).val( password);
            });
        }
        //отвязывание
        this.unbind = function(){
           $( 'body').off('click', selectors.generateLoginBt);
           $( 'body').off('click', selectors.generatePasswordBt);
        }
    }
}

/**
 * биндер для списка юзеров
 * @returns {_L262}
 */
function userListBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                container : '.users-data-container',
                searchBt : '.users-data-container .viewzone .search.bt',
                resetSearchBt : '.users-data-container .viewzone .reset-filter.bt',
                userRow : '.users-data-container .viewzone .users tbody tr',
                filterForm : '.users-data-container .viewzone .users-filter-block',
                table : '.users-data-container .viewzone table.users',
                page : '.users-data-container #pagination .page',
                addUser : '.users-button-block .add-user',
                roleMatrix : '.users-button-block .role-matrix',
               };
               
        this.getUsers =  function( page ){
            var data = $( selectors.filterForm).serializeArray();

            if( !empty(page)){
                data.push({
                    name: 'page',
                    value: page
                });
            }
            console.log(data);
            
            var page = _getPage().userList( data),
                tr = $( page).find('table.users tbody tr'),
                paginator = $( page).find('#pagination').children();

           

            $( selectors.container+' #pagination').html('').append( paginator);
            $( selectors.table+' tbody').html('').append(tr);
        }

        //связывание
        this.bind = function(){
            var getUsers = this.getUsers;
            
            $( 'body').on('click', selectors.roleMatrix, function(){
                users.getRoleMatrix();
            });
            
            $( 'body').on('click', selectors.addUser, function(){
                users.getNewDialog();
            });
            
            $( 'body').on('click', selectors.page, function(){
                getUsers( $( this).attr('page'));
            });

            $( 'body').on('keyup', selectors.filterForm, function( e){
                if( e.keyCode == 13){
                    getUsers();
                }
            });

            $( 'body').on('click', selectors.searchBt, function(){
                getUsers();
            });

            $( 'body').on('click', selectors.resetSearchBt, function(){
                $( selectors.filterForm+' select,'+selectors.filterForm +' input').each(function( i, el){
                    $( el).val('');
                });
                getUsers();
            });
            
            $( 'body').on('click', selectors.userRow, function(){
                 var userId = $(this).attr('id');

                if( !$(this).hasClass('active')){

                    if( $(selectors.table+' tbody tr.active').length !== 0){
                        $(selectors.table+' tbody tr.active').removeClass('active');
                    }
                    $(this).addClass('active');
                }else{
                    users.getUserDialog( userId);
                }
            });
        }
        //отвязывание
        this.unbind = function(){
            $( 'body').off('click', selectors.searchBt);
            $( 'body').off('click', selectors.resetSearchBt);
            $( 'body').off('click', selectors.userRow);
            $( 'body').off('keyup', selectors.filterForm);
            $( 'body').off('click', selectors.page);
            $( 'body').off('click', selectors.addUser);
            $( 'body').off('click', selectors.roleMatrix);
        }
    }
}


/**
 * биндер для organization list
 * @returns {_L262}
 */
function OrganizationInfoBinder( organizationId){
    return new function(){
        //массив селекторов
        var selectors = {
            tabButtons          : '.dialog-organization-info-container .tabs-container .tab',
            tabButtonActive     : '.dialog-organization-info-container .tabs-container .tab.active',
            removeFounders      : '.remove-founders',
            addFounders         : '.add-founders',
            contantContainer    : '.dialog-organization-info-container .content-container',
            licenseFromOrg      : '.choose-license',
            supplementFromOrgTr : '.license-info-block .supplement-info tbody tr[supplementid], .license-info-block .supplement-info tbody tr[supplementId]',
            branchOrgTr         : '.dialog-branch-info-container .ron-branch-orgs tbody tr',
            countriesSelect     : '.choose-country',
            regionSelect        : '.choose-region',
            notificationTr      : '#notification-info-form table tbody tr:not(.level)',
            searchRegNumber     : '.search-reg-number',
            applyDealsFilter    : '.apply-deals-filter',
            dealFilterBlock     : '.deal-filter-block',
            refreshDealsFilter  : '.refresh-deals-filter',
            sortableTh          : '.deals-from-org .sortable',
            dealsTableBody      : '.dialog-deals-info-container table tbody',
            deals               : '.dialog-deals-info-container',
            dealsPage           : '.dialog-deals-info-container #pagination .page',
            refreshDealFilter   : '.refresh-deals-filter',
            precissionButton    : '.btShowPrescriptions',
            dealsTr             : '.deals-from-org tbody tr',
            searchBranch        : 'form.serach-filter-block input[name="search"]',
            toLicense           : '.license-info-block .toLicense',
            caseBlock           : ".caseBlock",
            caseBlockBt         : ".showCaseValue",
            inputOrgName        : '#organizationInfoForm textarea[name="FullName"]',
            getCaseBt           : ".getCaseValue",
            getDocumentFromLink : ".get-document",
            getDealFromLink     : ".get-deal-from-document",
            LicenseSuppFilter   : 'input[name="search-license-supplement-from-org"]',
            LicenseSuppTr       : '.supplement-info table tbody tr',
            removeDocument      : '#notification-info-form .document-unselected-row .removeFileType',
            addDocument         : '#notification-info-form .document-unselected-row .newFileType',
            citySelect          : '.dialog-organization-info-container select[name="LawCity"]',
            applicationFilter   : '.tab-request input[name="filter-application"]',
            applicationTr       : '.dialog-application-info-container .application-orgs tbody tr[applicationid]',
            accreditationSelector   : '.accreditation-block select[name="certificate"]',
            accreditationDataBlock  : '.accreditation-block .data-block div',
            certificateSupplementTr : '.accreditation-block .certificate-supplement-info table tbody tr',
            inputSearchCertificate  : '.accreditation-block input[name="search-certificate-supplement-from-org"]'
        };

        $('body').on('click', selectors.applicationTr, function(){
             var appId = $(this).attr('applicationid');

                if( !$(this).hasClass('active') ){

                    if( !empty( $(selectors.applicationTr+'.active'))){
                        $(selectors.applicationTr+'.active').removeClass('active');
                    }
                    $(this).addClass('active');
                }else{
                    application.info(appId);
                }
        });

        

        function getDealFilterData(){
            var dealFilter = {};
            return $('.deal-filter-block').serializeArray();;
        }
        function getDealOrderStr(){
            var order = '',
             currentSortSelector = selectors.sortableTh+'.current';
        
            if( $(currentSortSelector).length >0){
                order = $(currentSortSelector).attr('field-name')+' '+$(currentSortSelector).attr('sort-type');
            }
            return order;
        }
        $('body').undelegate('.document-unselected-row .removeFileType', 'click');
        $('body').undelegate('.document-unselected-row .newFileType', 'click');
        
        //связывание
        this.bind = function(){

            $('body').on('click', selectors.certificateSupplementTr, function(){
                 var certificateId = $(this).attr('supplement-id');

                    if( !$(this).hasClass('active') ){

                        if( !empty( $(selectors.certificateSupplementTr+'.active'))){
                            $(selectors.certificateSupplementTr+'.active').removeClass('active');
                        }
                        $(this).addClass('active');
                    }else{
                        certificate.supplementDialog(certificateId);
                    }
            });

            $('body').on('keyup', selectors.inputSearchCertificate, function(){
                var idCert = $(selectors.accreditationSelector).val();
                $(selectors.certificateSupplementTr).hide();

                tableSearchFilter( $(this).val(),  selectors.certificateSupplementTr+'[certificate-id="'+idCert+'"]' , [0,3,4,5]);
            });

            $('body').on('change', selectors.regionSelect, function(){
                var regId = $(this).val();
                var cities = _getPage().getCityFromRegion(regId);
                $(selectors.citySelect).html(cities);
            });

            $('body').on('change', selectors.accreditationSelector, function(){
                var idCert = $(this).val();
                $(selectors.accreditationDataBlock).hide();
                $(selectors.certificateSupplementTr).hide();

                $( selectors.accreditationDataBlock+'[data-id="'+idCert+'"]').each(function( i , el){
                    $(el).show();
                });

                $( selectors.certificateSupplementTr+'[certificate-id="'+idCert+'"]').each(function( i , el){
                    $(el).show();
                });
            });

            $('body').on('click', selectors.removeDocument, function(){
              affairs.removeDocumentFile($(this).attr("type-file"), $(this).attr("document-id"), false, true);
            });

            $('body').on('click', selectors.addDocument, function(){
              activateFileUpload('/islod/affairs/save-files/' + $(this).attr("document-id"), '.multi', false, true);
              $("input[name=" + $(this).attr("real-type-name") + "]").click();
            });

            $('body').undelegate('.buttonDocument_2', 'click');
            $('body').delegate('.buttonDocument_2',  'click', function () {
                                    affairs.changeStatusDocumentDialog(
                                            $(this).attr("data-next-status"),
                                            $(this).attr("data-message"),
                                            $(this).attr("data-document-id"),
                                            $(this).attr("data-require"),
                                            $(this).attr("data-deal-number"),
                                            $(this).attr("data-deal-id")
                                    );
                                });

            $('select[name="propertyFormId"]').combobox();
            $('select[name="fk_eiisEducationalOrganizationType"]').combobox();
            $('select[name="fk_eiisEducationalOrganizationProperties"]').combobox();
            $( selectors.caseBlock).hide();

            if($('.is-branch').val() == '1'){
                $('.newRequest').remove();
            }

            $('body').on('click', selectors.getDocumentFromLink, function(){
                var id = $(this).attr('documentid');
                organization._createCreateNotificationDialog(id);
            });
            
            $('body').on('click', selectors.getDealFromLink, function(){
                var id = $(this).attr('dealid'),
                    delalNumber = $(this).html(),
                    orgName = $(this).attr('orgname');
                    
                affairs.openEditAffair(id,'Дело №'+delalNumber+' '+orgName);
            });

            $('body').on('click', selectors.getCaseBt, function(){
                organization.getNameForm( $( selectors.inputOrgName).val());
            });
            
            $( 'body').on('click', selectors.caseBlockBt, function(){
                console.log('wrk');
                if( $( selectors.caseBlock ).is( ":hidden" )){
                    $( selectors.caseBlock ).slideDown('slow');
                    $(this).html('Скрыть склонения');
                }else{
                    $(this).html('Показать склонения');
                    $( selectors.caseBlock ).slideUp('slow');
                }
            });

            $( 'body').on('click', selectors.toLicense, function(){
                var id = $(this).attr('id'),
                    title = $(this).attr('dialog-title');
                    
                license._get_license_info( id , title);
            });
            
            $( 'body').on('keyup', selectors.searchBranch, function(){
                tableSearchFilter( 
                    $(this).val(),
                    '.dialog-branch-info-container table tbody tr' ,
                    [0,1,2],
                    function(tr,status, trNumber){
                        if(trNumber==0){
                            $('.current-show-branch').html(0);
                        }
                        if(status){
                            var counter = parseInt($('.current-show-branch').html());
                            $('.current-show-branch').html(counter+1);
                        }
                        
                    }
                );
            });

            $( 'body' ).on('click', selectors.dealsTr, function(){
                var dealId = $(this).attr('id'),
                    orgName = $('.current-organization-short-name').val() || '',
                    title = $(this).attr('title')+' '+orgName ;

                if( !$(this).hasClass('active')){

                    if( !empty( $( selectors.dealsTr+'.active'))){
                        $( selectors.dealsTr+'.active').removeClass('active');
                    }
                    $(this).addClass('active');
                }else{
                    affairs.openEditAffair( dealId ,title);
                }
            });
            
            $( selectors.precissionButton ).on('click', function(){
                organization.getPrescriptionsDialog();
            });
            
            $( 'body' ).on('click', selectors.refreshDealFilter, function(){
                $('.deal-filter-block select,.deal-filter-block input').each(function(i,el){
                    $(el).val('');
                });
                organization._getDealListFromOrg('.tab-deal');
            });
            $( 'body' ).on('click', selectors.dealsPage, function(){
                var orgId = $('.current-organization-dialog-id').val(),
                    page = $(this).attr('page');
                organization._getDealRowsFromOrgFiltered(
                    selectors.deals,
                    orgId,
                    getDealFilterData(),
                    getDealOrderStr,
                    page
                );
            });
            
            $( 'body' ).on('keyup', selectors.dealFilterBlock, function( eventObject){
                if( eventObject.keyCode == 13){
                    var orgId = $('.current-organization-dialog-id').val();
                    organization._getDealRowsFromOrgFiltered(selectors.deals,orgId, getDealFilterData(),getDealOrderStr);
                }
            });

            $( 'body' ).on('click', selectors.applyDealsFilter, function(){
                var orgId = $('.current-organization-dialog-id').val();
                organization._getDealRowsFromOrgFiltered(selectors.deals,orgId, getDealFilterData(),getDealOrderStr);
            });

            $( 'body' ).on('click', selectors.sortableTh, function(){
                var currentSortSelector = selectors.sortableTh+'.current',
                    orgId = $('.current-organization-dialog-id').val();
                    
                if( !$(this).hasClass('current')){
                    $( currentSortSelector).each(function(i,el){
                        $(el).attr('sort-type','ASC');
                        $(el).removeClass('current');
                    });
                    $(this).addClass('current');
                }else{
                    if($(this).attr('sort-type') == 'ASC'){
                        $(this).attr('sort-type','DESC');
                    }else{
                        $(this).attr('sort-type','ASC');
                    }
                    
                }
                organization._getDealRowsFromOrgFiltered(selectors.deals,orgId, getDealFilterData(),getDealOrderStr);
            });
            
            $( 'body' ).on('keyup', selectors.searchRegNumber, function(){
                 tableSearchFilter( $(this).val(),  selectors.notificationTr, [2]);
             });

            $( 'body' ).on('keyup', selectors.applicationFilter, function(){
                tableSearchFilter( $(this).val(),  selectors.applicationTr, [0,2,3,4,5,6]);
            });
             
//            $( 'body' ).on('click', selectors.notificationTr, function(){
//                var notificationDocId = $(this).attr('data-documentid');
//
//                if( !$(this).hasClass('active')){
//
//                    if( !empty( $( selectors.notificationTr+'.active'))){
//                        $( selectors.notificationTr+'.active').removeClass('active');
//                    }
//                    $(this).addClass('active');
//                }else{
//                    organization._createCreateNotificationDialog( notificationDocId);
//                }
//            });
//Блокировка регионов для не России ибо для других стран тип региона только один
//            $(selectors.countriesSelect).on('change', function(){
//                var countryNum =  $(this).val(),
//                    worldOrgRegionCode = '03322C371096D282F7107F6E90FE9780',
//                    option  = 'option[value="'+worldOrgRegionCode+'"]';
//
//                if( countryNum == '643'){
//                    $(selectors.regionSelect).prop( "disabled", false );
//                    $(selectors.regionSelect).val('');
//                    $(selectors.regionSelect+' '+option).hide();
//                }else{
//                     $(selectors.regionSelect+' '+option).show();
//                     $(selectors.regionSelect).val( worldOrgRegionCode);
//                     $(selectors.regionSelect).prop( "disabled", true );
//                }
//            });
//
//            $(selectors.countriesSelect).change();

            $( 'body' ).on('click', selectors.branchOrgTr, function(){
                var organizationId = $(this).attr('id');

                if( !$(this).hasClass('active')){

                    if( !empty( $('.ron-branch-orgs .active'))){
                        $('.ron-branch-orgs .active').removeClass('active');
                    }
                    $(this).addClass('active');
                }else{
                    var dialog = $('.ui-dialog.ui-widget').detach();
                    _storage('reopen').clear('mainPage');
                    _storage('reopen').add('mainOrg', {
                        dialog: dialog,
                        id: organizationId,
                    });
                    OrganizationInfoBinder().unbind();
                    organization.createOrganizationInfoDialog( organizationId);
                }
           });
                    
            $( 'body').on('click', selectors.supplementFromOrgTr , function(){
                var supplementId = $(this).attr('supplementId'),
                    organizationId = $('#organizationInfoForm input[name="Id"]').val();

                if( !$(this).hasClass('active') ){

                    if( !empty( $('.license-info-block .supplement-info .active'))){
                        $('.license-info-block .supplement-info .active').removeClass('active');
                    }
                    $(this).addClass('active');
                }else{
                    licenseSupplement.createSupplementInfoDialog( supplementId);
                }
            });

            $('body').on('click', selectors.removeFounders , function(){
                var linkId = $(this).attr('linkid'),
                    parent = $(this).parent(),
                    name = $( parent).find('.founders-name').html(),
                    isNew = $(parent).attr('isnew'),
                    message = 'Вы действительно хотите удалить учредителя? ('+name+')',
                    deleteArr = _storage('foundersFromOrganization').get('delete') || [];

                if( confirm( message)){
                    console.log(isNew);
                    if( isNew == 'false'){
                        deleteArr.push( linkId);
                        _storage('foundersFromOrganization').add('delete',deleteArr);
                    }else{
                        var foundersArr = _storage('foundersFromOrganization').get('add'),
                            len = foundersArr.length,
                            newArr = [];
                            
                        for(var i=0; i<len; i++){
                            var el = foundersArr[i];
                            if( el.id != linkId){
                                newArr.push( el);
                            }
                            _storage('foundersFromOrganization').add('add',newArr);
                        }
                    }

                     $(parent).remove();
                }
            });

            
            $(selectors.tabButtons).on('click', function(){
                var that = this;
                if( !$(that).hasClass('active')){
                    tabsThumbler(
                            selectors.tabButtonActive,
                            $(that),
                            selectors.contantContainer,
                            function(){
                                organization.organizationInfoTabActionCaller( that);
                            }
                    );
                }
            });
            

            $( selectors.addFounders).on('click', function(){
                organization.createFoundersDialog( organizationId)
            });
            
            $( 'body').on('change', selectors.licenseFromOrg, function(){
                var licenseId = $(this).val();
                $(selectors.LicenseSuppFilter).val('');
                if( empty(licenseId)){
                    $('.supplement-info thead').hide();
                    $(selectors.LicenseSuppFilter).hide();
                }else{
                    $(selectors.LicenseSuppFilter).show();
                    $('.supplement-info thead').show();
                }
                
                $('.license-rows').hide();
                $('.supplement-rows').hide();

                $('.license-rows[licenseId="'+licenseId+'"]').show();
                $('.supplement-rows[licenseId="'+licenseId+'"]').show();
            });

            $('body').on('keyup', selectors.LicenseSuppFilter, function(){
                var licId = $(selectors.licenseFromOrg).val();
                tableSearchFilter( $(this).val(),  selectors.LicenseSuppTr+'[licenseId="'+licId+'"]' , [0,3,4,5]);
            });
            
            
        }
        //отвязывание
        this.unbind = function(){
            $('body').off('click', selectors.applicationTr);
            $('body').off('click', selectors.certificateSupplementTr);
            $('body').off('keyup', selectors.inputSearchCertificate);
            $('body').off('change', selectors.accreditationSelector);
            $('body').undelegate('.buttonDocument', 'click');
            $('body').off('click', selectors.getDocumentFromLink);
            $('body').off('click', selectors.getDealFromLink);
            $('body').off('click', selectors.getCaseBt);
            $( selectors.precissionButton ).off('click');
            $( selectors.tabButtons ).off('click');
            $( selectors.addFounders ).off('click');
            $( 'body' ).off('change',selectors.licenseFromOrg);
            $( 'body' ).off('click',selectors.removeFounders);
            $( 'body').off('click', selectors.supplementFromOrgTr);
            $( 'body').off('click', selectors.branchOrgTr);
            $( 'body' ).off('click', selectors.notificationTr);
            $( 'body' ).off('keyup', selectors.searchRegNumber);
            $( selectors.countriesSelect).off('change');
            $( 'body' ).off('click', selectors.applyDealsFilter);
            $( 'body' ).off('click', selectors.dealsPage);
            $( 'body' ).off('click', selectors.refreshDealFilter);
            $( 'body' ).off('keyup', selectors.dealFilterBlock);
            $( 'body').off('keyup', selectors.searchBranch);
            $( 'body').off('click', selectors.toLicense);
            $( 'body').off('click', selectors.caseBlockBt);
            $( 'body').off('keyup', selectors.LicenseSuppFilter);
            $( 'body').off('click', selectors.removeDocument);
            $( 'body').off('click', selectors.addDocument);
            $( 'body' ).off('keyup', selectors.applicationFilter);
            $( 'body' ).off('click', selectors.dealsTr);

        }
    }
}


/**
 * биндер для organization list
 * @returns {_L262}
 */
function OrganizationListBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
            organizationTr : '.ron-orgs tbody tr',
            addOrganization : '.add-new-organization',
            reset : '.bt-refresh-filters',
            searchFilter : '.org-filter .Search',
            applyBt : ".org-filter .btApply",
            fullFilter : ".org-filter .bt-full-filter",
        };
        sort = '';
        //связывание
        this.bind = function(){
            $( selectors.fullFilter ).on('click', function(){
                organization.getFullFilter();
            });

            $( selectors.organizationTr ).on('click', function(){
                var organizationId = $(this).attr('id');

                if( !$(this).hasClass('active')){

                    if( !empty( $('.ron-orgs .active'))){
                        $('.ron-orgs .active').removeClass('active');
                    }
                    $(this).addClass('active');
                }else{
                    organization.createOrganizationInfoDialog( organizationId);
                }
            });
            
            //Вешаем обработчик на нажатие Enter на поиск
            $( 'body').on( 'keyup', selectors.searchFilter , function (e) {
                if (e.keyCode == 13) {
                    $( selectors.applyBt).click();
                }
            });

            $( 'body').on( 'click', selectors.applyBt ,function () {
                //Заполняем фильтр
                filter = {
                    "Search": $(".Search", "#filter").val(),
                    "Region": $(".Region", "#filter").val(),
                    "Branch": $("select.Branch").val(),
                    "lod_stateCode" : $("#filter .lod_stateCode").val(),
                };
                //Отсылаем запрос на сервер
                _serverQuery(1, function (answer) {
                        var count = $(answer).find('.organiztion-list-count').html();
                        $('.organiztion-list-count').html(count);
                    });
            });
    
            $( selectors.addOrganization ).on('click', function(){
                organization.createNewOrganizationInfoDialog();
            });

            $( selectors.reset ).on('click', function(){
                $( '.Search').val('');
                $( '.Region').val('');
                $( '.Branch').val('0');
                $( '.btApply').click();
                $("#filter .lod_stateCode").val('');
                
            });

            


        }
        //отвязывание
        this.unbind = function(){
            $( selectors.fullFilter ).off('click');
            $( selectors.organizationTr ).off('click');
            $( selectors.reset ).off('click');
            $( 'body').off( 'keypress', selectors.searchFilter);
            $( 'body').off( 'click', selectors.applyBt );
        }
    }
}


/**
 * биндер для Supplement info дилога
 * @returns {_L262}
 */
function supplementInfoBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
            supplementTabButton : '.dialog-supplement-info-container .tabs-container .tab',
            supplementTabButtonActive : '.dialog-supplement-info-container .tabs-container .tab.active',
            supplementContent : '.dialog-supplement-info-container .content-container',
            educationlevelbutton : '.education-super-level-button',
            serachEduProg : '.serach-edu-program',
            searchLocation : '.search-education-location',
            educaionLocationTr : '.education-location-container table tbody tr',
            removeEducationProgram : '.remove-program',
            removeEducationLocation : '.remove-education-location',
            getDeal : 'a[name="dealNumber"]',
            scanCopyInput : '.supplement-scan-input',
            scanCopyPencil : '.supplement-scancopy-send'
        };
        //связывание
        this.bind = function(){

            $('body').undelegate('#supplementInfoForm .buttonReestr ', 'click');
            $('body').delegate('#supplementInfoForm .buttonReestr ', 'click', function () {
                var dialog = $(this).parent('.ui-dialog'),
                    close = $(dialog).find('.ui-icon-closethick'),
                    supId = $(this).attr("data-id"),
                    that = $(this),
                    afterCloseCallbck = function(){
                                $('body').off('click','.saveButLicense');
                                $(close).click();
                                licenseSupplement.createSupplementInfoDialog( supId);
                            },
                    defMessage = $(this).attr("data-message");
                 
                //если прописано событие то курим по нетму а не обработчик
                if( jQuery(this).is('[onClick]') ){
                    return false;
                }

                switch( $(this).attr("data-next-status") ){
                    case 'SEND_BY_MAIL':
                       defMessage = {
                                title : $(this).attr("data-message"),
                                message : $(this).attr("data-message")+'<table class="fields-table"><tr><td>Дата отправки по почте</td><td><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                                handler : function(dialog){
                                    $('input[name="date-attached-to-action"]').datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        dateFormat: "dd.mm.yy"
                                    });
                                    $('body').on('click','.saveButLicense', function(){
                                        _getData().changeLicAndSuppDate({
                                            id : $(that).attr("data-id"),
                                            type : $(that).attr("data-element-type"),
                                            input : 'DateSendingMail',
                                            date : $('input[name="date-attached-to-action"]').val()
                                        });
                                    });
                                }
                            };
                    break;
                    case 'RECEIVED_BY_APPLICANT':
                        defMessage = {
                                title : $(this).attr("data-message"),
                                message : $(this).attr("data-message")+'<table class="fields-table"><tr><td>Дата вручения</td><td><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                                handler : function(dialog){
                                    $('input[name="date-attached-to-action"]').datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        dateFormat: "dd.mm.yy"
                                    });
                                    $('body').on('click','.saveButLicense', function(){
                                        _getData().changeLicAndSuppDate({
                                            id : $(that).attr("data-id"),
                                            type : $(that).attr("data-element-type"),
                                            input : 'DateOnHands',
                                            date : $('input[name="date-attached-to-action"]').val()
                                        });
                                    });
                                }
                            };
                    break;
                }

                affairs.changeStatusReestrDialog(
                            $(this).attr("data-id"),
                            $(this).attr("data-element-type"),
                            $(this).attr("data-type-code"),
                            $(this).attr("data-next-status"),
                            defMessage,
                            $(this).attr("data-comment"),
                            afterCloseCallbck
                );                

        });
            
            $('body').on('click', selectors.removeEducationLocation ,function(){
                var locationid = $(this).attr('locationid');
                    if(confirm('Вы действительно хотите удалить адрес из приложения?')){
                        var data = _delete().deleteLocationFromSupplement( locationid);
                        if( data['isDelete']){
                            $(this).parent().parent().remove();
                        }
                    }

            });

            $('body').on('click', selectors.removeEducationProgram ,function(){
                var programid = $(this).attr('programid'),
                    supplementid = $('.hidden-supplement-data .current-supplement-dialog-id').val();
                    if(confirm('Вы действительно хотите удалить образовательную программу?')){
                        var data = _delete().deleteEduProgramFromSupplement( programid, supplementid);
                        if( data['isDelete']){
                            $(this).parent().parent().remove();
                        }
                    }
                    
            });

            $('body').on('keyup', selectors.searchLocation ,function(){
                var searchText = $(this).val(),
                     tr = selectors.educaionLocationTr,
                     tdArr = [ 2, 1];
                tableSearchFilter( searchText, tr , tdArr);
            });
             
            $('body').on('keyup', selectors.serachEduProg ,function(){
                 var searchText = $(this).val(),
                     tr = '.education-programs .right-education-programs-level-menu table tbody tr',
                     tdArr = [ 1, 2];
                     
                if( $( selectors.educationlevelbutton).hasClass('choosen-super-level')){
                  $( selectors.educationlevelbutton).removeClass('choosen-super-level');
                }
                var counter = 1;
                tableSearchFilter( 
                    searchText,
                    tr ,
                    tdArr,
                    function( rowNode, isValidTr){
                        if(isValidTr){
                            if($(rowNode).hasClass('program-title-block')){
                                counter = 1;
                            }else{
                                $(rowNode).find('td').first().html(counter);
                                counter++;
                            }
                        }
                });
             });

            //вкладки
            $( selectors.supplementTabButton ).on('click',function(){
                var tabButton = this;

                if( !$(tabButton).hasClass('active')){
                    tabsThumbler(
                            selectors.supplementTabButtonActive,
                            $(this),
                            selectors.supplementContent,
                            function(){
                                licenseSupplement.supplementInfoTabActionCaller(tabButton);
                            }
                    );
                }
            });

            $('body').on('click', selectors.educationlevelbutton ,function(){
                var selectorTr = '.right-education-programs-level-menu table tr[parent-level]';

                if( $(this).hasClass('choosen-super-level')){
                    $(this).removeClass('choosen-super-level');
                }else{
                    $('.left-education-programs-level-menu li a.choosen-super-level').each( function( i, el){
                        $(el).removeClass('choosen-super-level');
                    });
                    $(this).addClass('choosen-super-level');
                }

                var levelArr = [];
                $('.left-education-programs-level-menu li a.choosen-super-level').each( function( i, el){
                    var levelId = $(el).attr('super-level-id');
                    if( levelId != 'ALL'){
                        levelArr.push( levelId );
                    }
                    
                });

                var counter = 1;

                $( selectorTr).each(function( i, el){
                        var parentLevel = $(el).attr('parent-level');

                        if($(el).hasClass('program-title-block')){
                                counter = 1;
                        }else{
                            $(el).find('td').first().html(counter);
                            counter++;
                        }
                        
                        if( levelArr.indexOf( parentLevel) !== -1){
                            $(el).show();
                        }else{
                            if( levelArr.length === 0){
                                $(el).show();
                            }else{
                                $(el).hide();
                            }
                            
                        }

                });

            });

            $('body').on('click', selectors.getDeal, function(){
                var id = $(this).attr('dealid'),
                    delalNumber = $(this).html();

                affairs.openEditAffair(id,'Дело №'+delalNumber);
            });

        }
        //отвязывание
        this.unbind = function(){
            $('body').undelegate('#supplementInfoForm .buttonReestr ', 'click');
            $('body').off('click', selectors.removeEducationLocation );
            $('body').off('click', selectors.removeEducationProgram);
            $('body').off('keyup', selectors.serachEduProg);
            $( selectors.supplementTabButton ).off('click');
            $('body').off('click', selectors.educationlevelbutton);
            $('body').off('click', selectors.getDeal);
        }
    }
}


/**
 * биндер для license info дилога
 * @returns {_L262}
 */
function licenseInfoBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
            supplementTr : '.license_supplement_table tbody tr',
            dateEnd : 'input[name="DateEnd"]',
            dateEndCheckbox : '.forever-date-end',
            getDeal : 'a[name="dealNumber"]',
            searchSupplement : '.search-supplement-from-organization',
        };
        
        if( $( selectors.dateEnd).val() == '-'){
            $( selectors.dateEnd).prop('disabled',true);
            $( selectors.dateEndCheckbox).prop('checked',true);
            $( selectors.dateEnd).parent().find('.date-icon').hide();
        }
        if( $('.rights-edit-license').val() != '1'){
            $( selectors.dateEnd).prop('disabled',true);
            $( selectors.dateEnd).parent().find('.date-icon').hide();
        }
        //связывание
        this.bind = function(){
            $('body').undelegate('#licenseForm .buttonReestr ', 'click');
            $('body').delegate('#licenseForm .buttonReestr ', 'click', function () {
                var dialog = $(this).parent('.ui-dialog'),
                    title = $(dialog).find('.ui-dialog-title'),
                    licId = $(this).attr("data-id"),
                    that = $(this),
                    afterCloseCallbck = function(){
                                $('body').off('click','.saveButLicense');
                                $(dialog).find('.ui-icon-closethick').click();
                                $('.btApply').click();
                                license._get_license_info( licId, title);
                            },
                    defMessage = $(this).attr("data-message");


                switch( $(this).attr("data-next-status") ){
                    case 'SEND_BY_MAIL':
                       defMessage = {
                                title : $(this).attr("data-message"),
                                message : $(this).attr("data-message")+'<table class="fields-table"><tr><td>Дата отправки по почте</td><td><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                                handler : function(dialog,function_data){
                                    $('input[name="date-attached-to-action"]').datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        dateFormat: "dd.mm.yy"
                                    });
                                    function_data['beforeSaveCallBck'] = function(){
                                        _getData().changeLicAndSuppDate({
                                            id : $(that).attr("data-id"),
                                            type : $(that).attr("data-element-type"),
                                            input : 'DateSending',
                                            date : $('input[name="date-attached-to-action"]').val()
                                        });
                                    };
                                }
                            };
                    break;
                    case 'RECEIVED_BY_APPLICANT':
                        defMessage = {
                                title : $(this).attr("data-message"),
                                message : $(this).attr("data-message")+'<table class="fields-table"><tr><td>Дата вручения</td><td><div class="relative_block"><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                                handler : function(dialog,function_data){
                                    $('input[name="date-attached-to-action"]').datepicker({
                                        changeMonth: true,
                                        changeYear: true,
                                        dateFormat: "dd.mm.yy"
                                    });
                                    function_data['beforeSaveCallBck'] = function(){
                                        _getData().changeLicAndSuppDate({
                                            id : $(that).attr("data-id"),
                                            type : $(that).attr("data-element-type"),
                                            input : 'DateOnHands',
                                            date : $('input[name="date-attached-to-action"]').val()
                                        });
                                    };
                                }
                            };
                    break;
                }
                
                //если на кнопку прикручен свой обработчик,
                //ну так выкиним родную обработку
                //нужно чтоб завести отдельные окоши на некоторые кнопки
                var hasOnClick = jQuery(this).is('[onclick]');
                if (!hasOnClick){    
                    affairs.changeStatusReestrDialog(
                            $(this).attr("data-id"),
                            $(this).attr("data-element-type"),
                            $(this).attr("data-type-code"),
                            $(this).attr("data-next-status"),
                            defMessage,
                            $(this).attr("data-comment"),
                            afterCloseCallbck
                        );
                }                                
            });

            $('body').on('keyup', selectors.searchSupplement ,function(){
                 var searchText = $(this).val(),
                     tr = selectors.supplementTr,
                     tdArr = [ 0, 1];

                tableSearchFilter( searchText, tr , tdArr);
                countingRecords(tr);
             });

            $('body').on('click', selectors.getDeal, function(){
                var id = $(this).attr('dealid'),
                    delalNumber = $(this).html();

                affairs.openEditAffair(id,'Дело №'+delalNumber);
            });
            
            $( 'body').on('change', selectors.dateEndCheckbox , function(){
                var status =  $( selectors.dateEndCheckbox).prop('checked');

                if( status){
                    $( selectors.dateEnd).val('-');
                    $( selectors.dateEnd).prop('disabled',true);
                    $( selectors.dateEnd).parent().find('.date-icon').hide();
                }else{
                    $( selectors.dateEnd).prop('disabled',false);
                    $( selectors.dateEnd).parent().find('.date-icon').show();
                }
            });

            $( 'body').on('click', selectors.supplementTr , function(){
                var supplementId = $(this).attr('supplementId');

                if( !$(this).hasClass('active')){

                    if( $('.license_supplement_table tr.active').length !== 0){
                        $('.license_supplement_table tr.active').removeClass('active');
                    }
                    $(this).addClass('active');
                }else{
                    licenseSupplement.createSupplementInfoDialog( supplementId );
                }
            });
        }
        //отвязывание
        this.unbind = function(){
            $('body').undelegate('#licenseForm .buttonReestr ', 'click');
            $( 'body').off('change', selectors.dateEndCheckbox);
            $('body').off('click',selectors.supplementTr );
            $('body').off('click', selectors.getDeal);
        }
    }
}

/**
 * Подсчёт скрытых записей.
 * Если все записи скрыты выводим оповещение что записей нет.
 *
 * @param tag
 */
function countingRecords(tag)
{
    $(tag + '.td_level').hide();
    if($(tag + '[style="display: none;"]').length === $(tag).length){
        $(tag + '.td_level').show();
    }
}

/**
 * биндер для founders list
 * @returns {_L262}
 */
function foundersChooseListBinder( ){
    return new function(){
        //массив селекторов
        var selectors = {
                tbody  : '.dialog-founders-choose-list-container .founders-table tbody',
                filter : '.dialog-founders-choose-list-container .filter',
                searchButton : '.dialog-founders-choose-list-container .search-founders-button',
                foundersTr : '.founders-table tbody tr',
                refreshFilter : '.refresh-founders-filter',
                search : '.dialog-founders-choose-list-container .founders-search'
//                
            };

        function foundersPageProc( page , replace){
            var tr = $( page).find('.founders-table tbody tr'),
                replace = replace || false,
                currenPage = parseInt( $( page).find('.curent-page').val()),
                arrRemoveFromAnswer = getArrSubElementFromArr( _storage('foundersFromOrganization').get('add'), 'id');
        
            

            if( replace && !empty(tr)){
                $( selectors.tbody).scrollTop(0);
                $( selectors.tbody).html( tr);
            }else{
                $( selectors.tbody).append( tr);
            }
            if( $( selectors.tbody+' tr').length <= 0 ){
                $( selectors.tbody).html('<tr class="level"><td style="text-align: center">Записи не найдены</td></tr>');
            }

            $(selectors.tbody+' tr').each(function(){
                console.log($(this).attr('founderid'));
                console.log(arrRemoveFromAnswer);
                console.log(( arrRemoveFromAnswer.indexOf($(this).attr('founderid')) != (-1) ));
                if( arrRemoveFromAnswer.indexOf($(this).attr('founderid')) != (-1) ){
                    $(this).remove();
                }
            });
            
            _storage('founders').add('currentPage', currenPage);
        }

        _storage('founders').add('currentPage', 1);
        _storage('founders').add('scroll', 12);
        
        //связывание
        this.bind = function(){
            $(selectors.tbody).on( 'scroll', function(){
                if( _storage('founders').get('scroll') == 0){
                    var filter = $( selectors.filter).serializeArray();
                    filter.push({name:'page',value:_storage('founders').get('currentPage')+1});

                    var page = _getPage().getFoundersPagesTest( filter);
                    foundersPageProc( page);
                    _storage('founders').add('scroll', 12);
                }else{
                    _storage('founders').add( 'scroll', _storage('founders').get('scroll') - 1 );
                }
            });

            $( selectors.searchButton).on( 'click', function(){
                var filter = $( selectors.filter).serializeArray();
                    filter.push({name:'page',value:1});
                    filter.push({name:'id',value:$('#organizationInfoForm input[name="Id"]').val()});
                var page = _getPage().getFoundersPagesTest( filter);
                foundersPageProc( page, true);
                _storage('founders').add('scroll', 12);
            });

            $('body').on('keyup', selectors.search, function( eventObject){
                if(eventObject.keyCode == 13){
                    var filter = $( selectors.filter).serializeArray();
                    filter.push({name:'page',value:1});
                    filter.push({name:'id',value:$('#organizationInfoForm input[name="Id"]').val()});
                    var page = _getPage().getFoundersPagesTest( filter);
                    foundersPageProc( page, true);
                    _storage('founders').add('scroll', 12);
                }
            });

            $(selectors.refreshFilter).on('click', function(){
                $( selectors.filter+' select,'+selectors.filter+' input').val('');
                $( selectors.searchButton).click();
            });
        }
        //отвязывание
        this.unbind = function(){
            $( selectors.search).off('keyup');
            $(selectors.refreshFilter).off('click');
            $(selectors.tbody).off( 'scroll');
        }
    }
}

/**
 * биндер для new organization
 * @returns {_L262}
 */
function newOrganizationBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                branchSelector  : '#organizationInfoForm select[name="Branch"]',
                chooseHeadOrg   : '#organizationInfoForm .choose-head-organization',
                inputIdHeadOrg  : '#organizationInfoForm input[name="fk_eiisParentEducationalOrganization"]',
                selectRegion    : 'select[name="fk_eiisLawRegion"]',
                selectCity      : 'select[name="LawCity"]',
                inputAdress     : 'input[name="LawAddress"]'
            },
            //нечто вроде расширение бинда выходит, используем бинд для просмотра
            orgBind = OrganizationInfoBinder();
            selectCityOrRegion = function(){
                var region = $( selectors.selectRegion+' option:selected').text(),
                    city = $( selectors.selectCity+' option:selected').text(),
                    ret = '';

                if( $( selectors.selectCity).val() != '' ){
                    
                }
                if(!empty(region) && $( selectors.selectRegion).val() != '' ){
                    ret = region+', '+city;
                }
                $( selectors.inputAdress).val(ret);
            }
        //связывание
        this.bind = function(){
            orgBind.unbind();
            orgBind.bind();

            $('body').on('change', selectors.selectRegion, function(){
                 selectCityOrRegion();
            });

            $('body').on('change', selectors.selectCity, function(){
                 selectCityOrRegion();
            });

            $('body').on('change', selectors.branchSelector, function(){
                var branch = $( this).val();
                
                if( branch == '1'){
                    $( selectors.chooseHeadOrg).parent().parent().show();
                }else{
                    $( selectors.inputIdHeadOrg).val('');
                    $( selectors.chooseHeadOrg).val('');
                    $( selectors.chooseHeadOrg).parent().parent().hide();
                }
            });
            $('body').off('click', selectors.chooseHeadOrg);
            $('body').on('click', selectors.chooseHeadOrg, function(){
                //если на объекте есть класс noHandler - обработчик не будет обрабатываться
                //сделано для случая, когда создаем филиал заданной организации
                //но disbled не подходит, так как надо дать возможность копировать текст
                if($(this).hasClass("noHandler"))
                    return;

                $( selectors.inputIdHeadOrg).val('');
                $( selectors.chooseHeadOrg).val('');
                organization._createHeadOrgDialog();
            });

            $( selectors.branchSelector).change();
        }
        //отвязывание
        this.unbind = function(){
            $('body').off('change', selectors.branchSelector);
            $('body').off('click', selectors.chooseHeadOrg);
            orgBind.unbind();
        }

    }
}

function createFoundersBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                foundersSelect:'.create-founders .founders-data select',
                personInput : '.create-founders .founders-data input:has [name="spike-to-bug-nodelete"],[name="LastName"],[name="FirstName"],[name="Patronymic"],[name="PAddress"]',
                otherInput : '.create-founders .founders-data input:has [name="spike-to-bug-nodelete"],[name="OrganizationFullName"],[name="OrganizationShortName"],[name="LawAddress"]',
            };
        //связывание
        this.bind = function(){
            $( selectors.foundersSelect).on('change',function(){
                var type = $( this).val();
                if( 
                        type == '93a09094-8d4d-4c88-8c30-f9d2c89e078d' ||
                        type == 'cf87aa72-a0d1-43cb-94ba-e09ef2559ca2'
                    ){
                    $( selectors.personInput).parent().parent().show();
                    $( selectors.otherInput).each(function(i,el){
                        $(el).val('');
                    });
                    $( selectors.otherInput).parent().parent().hide();
                }else{
                    $( selectors.otherInput).parent().parent().show();
                    $( selectors.personInput).each(function(i,el){
                        $(el).val('');
                    });
                    $( selectors.personInput).parent().parent().hide();
                }
            });
        }
        //отвязывание
        this.unbind = function(){
            $( selectors.foundersSelect).off('change');
        }
    }
}

function headOrganizationBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                    dialogDiv : '.head-org-to-new-org',
                    viewzone  : '.head-org-to-new-org .viewzone',
                    tbody : '.head-org-to-new-org .viewzone table tbody',
                    search : '.search-org-name-or-inn'
                };
        //связывание
        this.bind = function(){
            $( selectors.dialogDiv).on('scroll',function(){
                var dialogDivTop = $( selectors.dialogDiv).position().top,
                winHeigh = $(window).height(),
                viewTop = $(window).scrollTop(),
                viewZoneTop = $( selectors.viewzone).position().top,
                viewZoneHeight = $( selectors.viewzone).height(),
                viewZoneToScroll = viewZoneHeight - ( winHeigh-50 ),
                currentTop = viewZoneTop + dialogDivTop;
                
                if(
                    0 > (viewTop+viewZoneToScroll+currentTop)
                ){
                    var pageNum = parseInt( _storage('organization-head-choose').get('page'))+1,
                        data = {page : pageNum },
                        searchVal = $( selectors.search).val();

                    if( !empty( searchVal)){
                        data['search'] = searchVal;
                    }
                    var    page = _getPage().getHeadOrgList( data);
                        
                    pageNum = $( page).find('.hidden-data input[name="page"]').val();
                    _storage('organization-head-choose').add('page', pageNum );

                    var tr = $( page).find('tbody tr');
                    $( selectors.tbody).append( tr);
                }
            });
            
            $( selectors.search).on('keyup',function( eventObject){
                if( eventObject.keyCode == 13){
                    var pageNum = 1,
                        data = {page : pageNum },
                        searchVal = $( selectors.search).val();

                    if( !empty( searchVal)){
                        data['search'] = searchVal;
                    }
                    var    page = _getPage().getHeadOrgList( data);
                    
                    _storage('organization-head-choose').add('page', pageNum );

                    var tr = $( page).find('tbody tr');
                    $( selectors.tbody).html( tr);
                }
            });
        }
        //отвязывание
        this.unbind = function(){
           $( selectors.dialogDiv).off('scroll');
        }
    }
}

function programsBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                    page : ".programs-container .page",
                    search : '.programs-container #filter .search',
                    allFilter : '.programs-container #filter input, #filter select',
                    btApply : '.programs-container #filter .btApply',
                    filter : '.programs-container #filter',
                    pagination : '.programs-container #pagination',
                    tbody : '.programs-container .viewzone table tbody',
                };

        $("body").undelegate(".route-table .content .page", "click");

        function search(page){
            var filterData = $( selectors.filter).serializeArray(),
                    pageNumber = page || 1,
                    filter = {};

                for( var key in filterData){
                    var name = filterData[key]['name'],
                        value = filterData[key]['value'];

                    filter[name] = value;
                }
                var data = { filter : filter, page : pageNumber},
                    page = _getPage().programs(data);


                if( page){
                    var tr = '' ,
                        pagination = '';

                    tr = $( page).find('tr'),
                    pagination = $( page).find('#pagination').children();

                    $( selectors.pagination ).html( pagination);
                    $( selectors.tbody ).html( tr);
                }
        }
        //связывание
        this.bind = function(){
            $( 'body').on('click', selectors.btApply, function(){
                var filterData = $( selectors.filter).serializeArray(),
                    filter = {};

                for( var key in filterData){
                    var name = filterData[key]['name'],
                        value = filterData[key]['value'];

                    filter[name] = value;
                }
                var data = { filter : filter},
                    page = _getPage().programs(data);


                if( page){
                    var tr = '' ,
                        pagination = '';
                        
                    tr = $( page).find('tr'),
                    pagination = $( page).find('#pagination').children();

                    $( selectors.pagination ).html( pagination);
                    $( selectors.tbody ).html( tr);
                }
            });
            
            $( 'body').on('click', selectors.page,function(){
                var page = $(this).attr('page');
                search(page);
            });

            $( 'body').on('keyup', selectors.filter,function( eventObject){
                if( eventObject.keyCode == 13){
                    search();
                }
                
            });
        }
        //отвязывание
        this.unbind = function(){
           $('body').off('click', selectors.btApply);
           $( 'body').off('keyup', selectors.filter);
           $( 'body').off('click', selectors.page);
        }
    }
}

function reportsBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                    report : ".report-data-container .generate-report",
                    dataLimit : 'form.report-eiis-license'
                };
        //связывание
        this.bind = function(){
           $(selectors.dataLimit+' input:not([readonly])').each(function(i,el){
            $(el).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd.mm.yy"
            });
        });

            $( 'body').on('click', selectors.report, function(){
                var type = $(this).data('type');
                if( type == 'EIIS_LICENSES'){
                    var data = $(selectors.dataLimit).serializeArray();
                    data.push({name:'type',value:type});
                    report.reportGenerate( data,'Лицензии в ФНС');
                }
                if(type == 'LICENSED_ORGANIZATION'){
                    var data = [];
                    data.push({name:'type',value:type});
                    report.reportGenerate(data,'Лицензированные организации');
                }
                if(type == 'LICENSED_PROGRAMMS'){
                    var data = [];
                    data.push({name:'type',value:type});
                    report.reportGenerate(data,'Лицензированные программы');
                }
            });

        }
        //отвязывание
        this.unbind = function(){
            $( 'body').off('click', selectors.report);

        }
        }
}

function inventoryBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                    generateLicInv : ".generate-license-inventory",
                    generateLicInvAndUser : ".generate-license-inventory-and-user",
                    form : "#license-inventory",
                    link : ".get-license-inventory"
                };
        //связывание
        this.bind = function(){
            $( 'body').on('click', selectors.generateLicInv, function(){
                var href = '/islod/index/get-license-inventory';
                if(!empty( $('input[name="dateStart"]').val())){
                    href += '/'+$('input[name="dateStart"]').val();
                }else{
                    href += '/'+'null';
                }
                if(!empty( $('input[name="dateEnd"]').val())){
                    href += '/'+$('input[name="dateEnd"]').val();
                }else{
                    href += '/'+'null';
                }
                $( selectors.link ).attr('href',href);
                window.location.href = href;
            });

            $( 'body').on('click', selectors.generateLicInvAndUser, function(){
                var href = '/islod/index/get-license-inventory-and-user';
                if(!empty( $('input[name="dateStart"]').val())){
                    href += '/'+$('input[name="dateStart"]').val();
                }else{
                    href += '/'+'null';
                }
                if(!empty( $('input[name="dateEnd"]').val())){
                    href += '/'+$('input[name="dateEnd"]').val();
                }else{
                    href += '/'+'null';
                }
                $( selectors.link ).attr('href',href);
                window.location.href = href;
            });
        }
        //отвязывание
        this.unbind = function(){
            $( 'body').off('click', selectors.generateLicInv);
            $( 'body').off('click', selectors.generateLicInvAndUser);
        }
        
        $(".hasDataPicker:not([readonly])").each(function(i,el){
                $(el).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy"
                });
        });
    }
}

function calendarBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
                    next : ".nextYear",
                    previos : ".previosYear",
                    year : ".year-value",
                    monthBlock : '.month-block',
                    day : '.day',
                    radio : '.day-data input[name="status"]'
                };

        this.year = function(yearOrNull){
            var yearOrNull = yearOrNull || null;
            if( empty( yearOrNull)){
               return parseInt($(selectors.year).html());
            }else{
               $(selectors.year).html(yearOrNull);
               return parseInt($(selectors.year).html());
            }
        }
        var that = this;

        //связывание
        this.bind = function(){
            $( 'body').on('click', selectors.next, function(){
                var year = that.year( that.year()+1),
                    page = _getPage().calendar({year:year}),
                    monthBlock = $(page).find('.month-block');

                $(selectors.monthBlock).html(monthBlock);
            });
            $( 'body').on('click', selectors.previos, function(){
                var year = that.year( that.year()-1),
                    page = _getPage().calendar({year:year}),
                    monthBlock = $(page).find('.month-block');

                $(selectors.monthBlock).html(monthBlock);
            });
            $( 'body').on('click', selectors.day, function(event){
                switch(true){
                    case (event.ctrlKey):
                        if( !$(this).hasClass('active')){
                            $(this).addClass('active');
                            $('[name="status"]').each(function(i,el){
                                $(el).prop('checked',false);
                            });
                        }
                    break;
                    default:
                        if( !$(this).hasClass('active')){
                            if( $('.day.active').length !== 0){
                                $('.day.active').removeClass('active');
                            }
                            $('[name="status"]').each(function(i,el){
                                $(el).prop('checked',false);
                            });
                            $(this).addClass('active');
                            if($(this).hasClass('red')){
                                $('.calendar-block input[value="free"]').prop('checked',true);
                                $('.calendar-block input[value="ferial"]').prop('checked',false);
                            }else{
                                $('.calendar-block input[value="free"]').prop('checked',false);
                                $('.calendar-block input[value="ferial"]').prop('checked',true);
                            }
                        }else{
                            $('[name="status"]').each(function(i,el){
                                $(el).prop('checked',false);
                            });
                            if( $('.day.active').length > 1){
                                $('.day.active').removeClass('active');
                                $(this).addClass('active');

                                if($(this).hasClass('red')){
                                    $('.calendar-block input[value="free"]').prop('checked',true);
                                    $('.calendar-block input[value="ferial"]').prop('checked',false);
                                }else{
                                    $('.calendar-block input[value="free"]').prop('checked',false);
                                    $('.calendar-block input[value="ferial"]').prop('checked',true);
                                }
                            }else{
                                 $('.day.active').removeClass('active');
                            }
                        }
                    break;
                }
            });
            
            $( 'body').on('change', selectors.radio, function(){
                var data = {},
                    dateArr = [],
                    radio = this;
                
                $('.day.active').each(function(i,el){
                    var day = parseInt( $(el).html() ),
                        month = parseInt( $(el).parent().parent().parent().attr('num')),
                        year = that.year();
                    dateArr.push({
                        date : year+'-'+month+'-'+day,
                        status : $(radio).val()
                    });

                    if($(radio).val()=='free'){
                        if(!$(el).hasClass('red')){
                            $(el).addClass('red')
                        }
                    }
                    
                    if($(radio).val()=='ferial'){
                        if($(el).hasClass('red')){
                            $(el).removeClass('red')
                        }
                    }
                });
                data = {dates:dateArr};
                _getData().changeDayStatus(data);
            });

        }
        //отвязывание
        this.unbind = function(){
            $( 'body').off('click', selectors.next);
            $( 'body').off('click', selectors.previos);
        }

        $(".hasDataPicker:not([readonly])").each(function(i,el){
                $(el).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy"
                });
        });
    }
}

function certificateSuppementBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
            tabsButtons : '.certificate-supplement .tabs-container .tab',
            inputSearch : '.search-from-ugs',
            programTr   : '.tab-education-program table tbody tr'
        };
        //связывание
        this.bind = function(){
            $( selectors.tabsButtons).on('click', function(){
                var that = this;
                if( !$(this).hasClass('active')){
                    console.log($(this));
                    tabsThumbler(
                            '.certificate-supplement .tabs-container .tab.active',
                            $(that),
                            '.certificate-supplements'
                    );
                }
            });

            $('.tab-education-program').hide();

             $( selectors.inputSearch).on('keyup', function( eventObj){
                  var searchText = $(this).val(),
                     tr = selectors.programTr,
                     tdArr = [ 1, 2, 3, 4];

                tableSearchFilter( searchText, tr , tdArr);
             });
        }
        //отвязывание
        this.unbind = function(){
            $('body').off('click', selectors.tabsButtons);
        }
    }
}

function applicationBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
            tabsButtons     : '.application-info .tabs-container .tab',
            branchTitle     : '.application-info .branch-title',
            precissionButton: '.btShowPrescriptions',
        };
        //связывание
        this.bind = function(){
            $( selectors.tabsButtons).on('click', function(){
                var that = this;
                if( !$(this).hasClass('active')){
                    console.log($(this));
                    tabsThumbler(
                            selectors.tabsButtons+'.active',
                            $(that),
                            '.application-info'
                    );
                }
            });

            $( selectors.branchTitle).on('click', function(){
                var branchId = $(this).data('branch'),//[data-branch="'+branchId+'"]
                    dataTr = $('.application-info table tr[data-branch]').not('.branch-title');

                $( dataTr).each(function(i,el){
                    if( $(el).data('branch') == branchId ){
                        if( $(el).is(":visible") ){
                            $(el).slideUp();
                        }else{
                            $(el).slideDown();
                        }
                    }else{
                        $(el).slideUp();
                    }
                });

                
            });

            $( selectors.precissionButton ).on('click', function(){
                organization.getPrescriptionsDialog();
            });

            $('.tab-history-application').hide();
            var tr = $('.application-info table tr[data-branch]').not('.branch-title');
            if( $(tr).length>9){
                $(tr).hide();
            }
        }
        //отвязывание
        this.unbind = function(){
            $('body').off( 'click', selectors.tabsButtons );
            $('body').off( 'click', selectors.precissionButton );
        }
    }
}

function fullOrgBinder(){
    return new function(){
        //массив селекторов
        var selectors = {
            tabsButtons     : '.application-info .tabs-container .tab',
            branchTitle     : '.application-info .branch-title',
            precissionButton: '.btShowPrescriptions',
        };
        //связывание
        this.bind = function(){
            $( selectors.tabsButtons).on('click', function(){
                var that = this;
                if( !$(this).hasClass('active')){
                    console.log($(this));
                    tabsThumbler(
                            selectors.tabsButtons+'.active',
                            $(that),
                            '.application-info'
                    );
                }
            });

            $( selectors.branchTitle).on('click', function(){
                var branchId = $(this).data('branch'),//[data-branch="'+branchId+'"]
                    dataTr = $('.application-info table tr[data-branch]').not('.branch-title');

                $( dataTr).each(function(i,el){
                    if( $(el).data('branch') == branchId ){
                        if( $(el).is(":visible") ){
                            $(el).slideUp();
                        }else{
                            $(el).slideDown();
                        }
                    }else{
                        $(el).slideUp();
                    }
                });


            });

            $( selectors.precissionButton ).on('click', function(){
                organization.getPrescriptionsDialog();
            });

            $('.tab-history-application').hide();
            var tr = $('.application-info table tr[data-branch]').not('.branch-title');
            if( $(tr).length>9){
                $(tr).hide();
            }
        }
        //отвязывание
        this.unbind = function(){
            $('body').off( 'click', selectors.tabsButtons );
            $('body').off( 'click', selectors.precissionButton );
        }
    }
}