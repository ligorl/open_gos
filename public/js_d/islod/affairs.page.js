/**
 * Функция вешает обработчики на пагинацию
 * @private
 */
_paginationHandler = function () {
    $("body").undelegate(".page", "click");
    $("body").delegate(".page", "click", function () {
        searchApplication(this);
    });
};
$(document).ready(function () {
    _paginationHandler();
    $("body").undelegate(".btApply", "click");
    $("body").delegate(".btApply", "click", function () {
        searchApplication();
    });
    
    $("body").delegate("input.Search", "keyup", function (eventObject) {
        if (eventObject.keyCode == 13) {
            searchApplication();
        }
    });
    $("body").undelegate(".row_appl", "click")
    $('body').delegate('.row_appl', 'click', function () {
        $('.row_appl').removeClass('active_appl');
        $(this).addClass('active_appl');
    });
    $("body").undelegate(".row_deal", "click")
    $('body').delegate('.row_deal', 'click', function () {
        $('.row_deal').removeClass('active_deal');
        $(this).addClass('active_deal');
    });
           
    $('body').undelegate('.active_deal', 'click');
    $('body').delegate('.active_deal', 'click', function () {
       affairs.openEditAffair();
    });
    
    $('body').undelegate('#procedureSelect', 'change');
    $('body').delegate('#procedureSelect', 'change', function () {
       affairs.getReasonForProcedure($(this).val());
    });
    $('body').undelegate("#filterForm.dealFilterWrap .btCancel","click");
    $('body').delegate("#filterForm.dealFilterWrap .btCancel","click",function(){
        $("#filterForm .Search").val("");
    });

    initFilterHandlers();
});
function showPop(url) {
    url = encodeURI(url);//персонально для ИЕ
    $.ajax({
        type: 'POST',
        url: url,
        success: function (answer) {
            $("#popupWindow").html(answer);
            $("#popupWindow").show();
        }
    });
}
function searchApplication(_this) {

    var page = $(_this).attr('page');

    if (page === undefined) {
        page = 1;
    }
    var event = $(".btn-warning").attr('event') + '/' + page;
    var keep_selector = $(".btn-warning").attr('controller');
    var destination_link = '/islod/' + keep_selector + '/' + event;
    $.ajax({
        type: 'POST',
        url: destination_link,
        data: $("#filterForm").serialize(),
        beforeSend: function () {
            $(".content_td .intable_content").html("").append($("<tr><td colspan='6'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")).fadeIn(500);
        },
        success: function (answer) {
            $(".content_td .intable_content").fadeOut(500, function () {
                answer = $(answer);
                $(".content_td .content").html("").append(answer);
                resizeContent($(".container .viewzone"));
                specialTableResize();
                _paginationHandler();
                //Запускаем обработчики
                var handlerName = keep_selector + event.charAt(0).toUpperCase() + event.substr(1) + "Handlers";
                if (window[handlerName])
                    window[handlerName]();
                affairs.getReasonForProcedure($("#procedureSelect").val(),$("#reasonHiddenInput").val());
            });
        }
    });
}

/**
 * Обработчики связанные с фильтром поиска по таблице
 */
function initFilterHandlers(container) {
    //Обработчик смены типа процедуры
    $("select.SearchType", container).change(function(){
        var value = $(this).val();
        $(".SearchReasons", container).val("0");
        //Если выбрали "Не выбрано" отображаем все причины
        if(value == "0"){
            $(".SearchReasons option", container).show();
        } else {
            //Прячем причины в списке
            $(".SearchReasons option", container).hide();
            $(".SearchReasons option[value=0]", container).show();
            $(".SearchReasons option[parentId="+value+"]", container).show();
        }
    });

    //Обработчик кнопки Сбросить
    $(".btClear", container).click(function(){
        $(".SearchOrg",container).val("");
        $(".dtDateStart",container).val("");
        $(".dtDateEnd",container).val("");
        $(".SearchRegNumber",container).val("");
        $(".SearchType",container).val("0");
        $(".SearchReasons",container).val("0");
    });

    //обработчик нажатия на кнопку ентер
    $(".SearchOrg, .SearchRegNumber",container).keypress(function(e){
        if(e.keyCode==13){
            $(".btApply", container).click();
        }
    });


}