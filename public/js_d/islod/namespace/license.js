var license = {};

/**
 * сохранение инфы о лицензии 
 */
license.saveLicenseForm = function () {
    $('input[name="DateEnd"]').prop('disabled',false);
    $.ajax({
        type: 'POST',
        dataType: "html",
        data: $("form#licenseForm").serialize(),
        url: "/islod/index/save-license-info",
        success: function (answer) {
            if (answer == "ok") {
                useTopHoverTitle("Данные успешно сохранены", 5000);
            } else {
                useTopHoverTitle("Произошла ошибка во время сохранения", 5000);
            }
        },
        error: function () {
            useTopHoverTitle("Произошла ошибка во время сохранения", 5000);
        }
    });
    $('input[name="DateEnd"]').prop('disabled',true);
}

/**
 * открываем окно лицензии
 * 
 * @param string license_id
 * @param string title
 * @returns open-dialog
 */
license._get_license_info = function (license_id, title) {
    dialog = {
        width: Math.round((screen.width / 100) * 87),
        height: Math.round((screen.height / 100) * 77)
    };
    specialResizeTableSupplement = function () {
        var viewzonHeight = $(".dialog-license-info").height();
        var headHeight = $(".dialog-license-info .intable thead").height() + $(".dialog-license-info .tabs-container").height();
        var needHeight = viewzonHeight - headHeight - 80;
        $(".dialog-license-info .intable tbody").height(needHeight);
        
        //пытался выровнять ширину колонок путем перетруски ширины колонок
        var parent_el = $(".dialog-license-info .intable.license_supplement_table").parent();
        if( !jQuery(parent_el).is(":visible")){
            return 0;
        }
        var tbodyWidth = $("  .intable tbody.content", parent_el).outerWidth();
        var trWidth = $("  .intable tbody.content tr", parent_el).outerWidth();
        var realwidth = tbodyWidth - trWidth;
        $("  .intable thead", parent_el).css("width", "calc( 100% - " + realwidth + "px )");
        var trFirst = jQuery(parent_el).find('tbody.content tr:first');
        if( trFirst.length ){
            var tdAll = jQuery(trFirst).find('td');
            var thAll = jQuery(parent_el).find('thead th');
            jQuery(tdAll).each( function(key,elem){            
                //var width = jQuery( tdAll[key]).css('width' );
                //jQuery( thAll[key] ).css( 'width', width );

            });
        }
        
    }
    var dContent = $(
            '<div class="dialog-license-info">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: title,
        width: dialog.width,
        height: dialog.height,
        modal: true,
        close: function () {
            licenseInfoBinder().unbind();
            $(".dialog, .dialog-license-info").remove();
        },
        buttons: [
            /*
            {
                text: "Открыть карточку ОО",
                "class": 'left openOrganizationButton',
                click: function () {
                    organization.createOrganizationInfoDialog($('#organizationId').val());
                }
            },
            */
           /*
            {
                text: "Сохранить",
                "class": 'right saveButLicense',
                click: function () {
                    license.saveLicenseForm();
                }
            },
            */
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        open: function () {
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/license/get-license-info/" + license_id,
                success: function (answer) {
                    licenseInfoBinder().unbind();
                    $(dContent).html("").append(answer);

                    if ($(answer).find('.license-dialog-title').length > 0) {
                        var title = $(answer).find('.license-dialog-title').val();
                        $(dContent).dialog('option', 'title', title);
                    }
                    $(".input-date:not([readonly])").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "dd.mm.yy"
                    });
                    $(".input-date").each(function (i, el) {
                        if ($(el).val() == '01.01.1970') {
                            $(el).val('-');
                        }
                    });
                    $(".tab", dContent).click(function () {
                        $(".tab", dContent).removeClass("active");
                        $(this).addClass("active");

                        $(".content-block", dContent).hide();
                        $(".content-block." + $(this).attr("tab-container"), dContent).show();

                        $(".buttons-block", dContent).hide();
                        $(".buttons-block." + $(this).attr("tab-container"), dContent).show();
                    });
                    changeSortField(".dialog-license-info .content-block.supplements", "license.licenseSupplementSort");
                    specialResizeTableSupplement();
                    licenseInfoBinder().bind();
                    $(function () {

                        var client = new ZeroClipboard($(".copy"), {
                            moviePath: "/js/zclip/ZeroClipboard.swf",
                            debug: false
                        });
                        client.on("dataRequested", function (client, args) {
                            client.setText($('.copy').text());
                            $('.dialog_info').remove();
                            /*$("<div class='dialog_info'></div>").html("ИНН скопирован в буфер обмена")
                             .dialog({
                             autoOpen:false,
                             title: "Уведомление",
                             modal:true,
                             buttons: { "Ok": function() { $(this).dialog("close"); } }
                             }).dialog("open")*/
                        });
                    });
                    if ($('.rights-view-org').val() != '1') {
                        $('.openOrganizationButton').remove();
                    }
                    if (
                            $('.rights-edit-license').val() != '1' &&
                            $('.rights-edit-license-post-date').val() != '1' &&
                            $('.rights-edit-license-hand-dat').val() != '1'
                            ) {
                        $('.saveButLicense').remove();
                    }
                }
            });
        }
    });

};
/**
 * Применяем стили к состояниям лицензий
 * @returns {undefined}
 */
license.applyStateStyle = function () {
    $('.license-state').each(function (k, v) {
        var item = $(v);
        var stateName = item.text();
        switch (stateName) {
            case 'Не действует':
                item.addClass('state-inactive');
                break;
            case 'Действует':
                item.addClass('state-active');
                break;
            case 'Приостановлена частично':
                item.addClass('state-suspended-partial');
                break;
            case 'Приостановлена':
                item.addClass('state-suspended');
                break;
            case 'Аннулирована':
                item.addClass('state-annuled');
                break;
            case 'Проект':
                item.addClass('state-project');
                break;
            default:
                break;
        }
    });
};
/**
 * Сортировка приложений в окне информации о лицензиях
 */
license.licenseSupplementSort = function () {
    $.ajax({
        type: 'POST',
        dataType: "html",
        data: {
            sort: $("#sort_field").val(),
            id: $("#LicenseId").val()
        },
        url: "/eo/license/get-license-supplement-sort",
        success: function (answer) {
            $(".supplements .license_supplement_table .content").html(answer);
            specialResizeTableSupplement();
        },
        error: function () {
            useTopHoverTitle("Произошла ошибка во время получения данных", 5000);
        }
    });
};

/**
 * Диалог выбора лицензий для отправки в реестр
 * @param {string} title
 * @returns {undefined}
 */
license.showSendToReestrDialog = function (title) {
    var width = Math.round(screen.width * 0.87);
    var height = Math.round(screen.height * 0.87);

    var url = '/islod/index/getSendToReestrDialog';
    var exportUrl = '/islod/index/exportLicensesToXml';
    var filter = {};
    var sort = {};


    var loadingHtml = "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>";

    var dialogContent = $(
            '<div class="dialog-content">' +
            loadingHtml +
            '</div>');

    var resizeContent = function () {
        var tbody = dialogContent.find('.intable tbody');
        var contentHeight = dialogContent.height();
        var filterTopHeight = dialogContent.find('.dialog-filter').height();
        var tableHeadHeight = dialogContent.find('.intable thead').height();
        var paginatorHeight = dialogContent.find('.dialog-paginator').outerHeight();
        var height = contentHeight - filterTopHeight - tableHeadHeight - paginatorHeight;
        tbody.height(height);
    };

    var customAjax = function (page) {
        $.ajax({
            type: 'POST',
            dataType: 'html',
            url: url,
            data: {
                page: page,
                filter: filter,
                sort: sort
            },
            beforeSend: function () {
                dialogContent.find('[data-action="selectAll"]').prop('checked', false);
                dialogContent.find('tbody.content').html('').append(loadingHtml).fadeIn(500);
            },
            success: function (response) {
                dialogContent.find('tbody.content .loading-img').fadeOut(500, function () {
                    response = $(response);
                    dialogContent
                            .find('tbody.content')
                            .html('')
                            .append(response.find('tbody.content > tr'));
                    dialogContent
                            .find('[data-item="pagination"]')
                            .html('')
                            .append(response.find('[data-item="pagination"] span'));
                });
            }
        });
    };

    var handlePagination = function () {
        dialogContent.find('[data-item="pagination"]').delegate('.page', 'click', function () {
            var page = parseInt($(this).attr('page'));
            customAjax(page);
            return false;
        });

    };

    var handleActions = function () {
        //чекбокс выбрать все
        dialogContent.delegate('[data-action="selectAll"]', 'click', function () {
            var checked = $(this).prop('checked');
            dialogContent.find('tbody.content [type="checkbox"]').prop('checked', checked);
        });
        //клик по table row = toggle checkbox
        dialogContent.delegate('tbody.content > tr', 'click', function () {
            var checkbox = $(this).find('[type="checkbox"]');
            checkbox.trigger('click');
            checkbox.prop('checked', !checkbox.prop('checked'));
        });
        //поиск
        dialogContent.delegate('[data-action="search"]', 'keypress', function (e) {
            if (e.keyCode === 13) {
                var search = $(this).val().trim();
                filter = search === '' ? {} : {search: search};
                customAjax(1);
            }
        });
        //сортировка
        dialogContent.delegate('[data-action="sort"]', 'click', function () {
            var column = $(this).attr('data-column');
            var type = $(this).attr('data-type') === 'asc' ? 'desc' : 'asc';
            $(this).attr('data-type', type);
            sort = column + ' ' + type;
            customAjax(1);
            return false;
        });

    };


    dialogContent.dialog({
        title: title,
        width: width,
        height: height - 80,
        modal: true,
        close: function () {
            $(".dialog-content").remove();
        },
        buttons: [
            {
                text: 'Отправить в реестр',
                class: 'dialog-button',
                'data-action': 'generateXml',
                click: function () {

                    var selected = [];
                    dialogContent.find('tbody.content tr :checked').each(function (k, v) {
                        var id = $(v).closest('tr').attr('data-id');
                        selected.push(id);
                    });

                    $.ajax({
                        type: 'POST',
                        dataType: 'html',
                        url: exportUrl,
                        data: {
                            'id[]': selected
                        },
                        beforeSend: function () {
                            dialogContent.html('').append(loadingHtml).fadeIn(500);
                            //$('[data-action="generateXml"] > .ui-button-text').text('Закрыть');
                            //$('[data-action="generateXml"]').prop('disabled', true);
                            $('[data-action="generateXml"]').addClass('hidden');
                        },
                        success: function (response) {
                            dialogContent.find('.loading-img').fadeOut(500, function () {
                                dialogContent.html('').append(response);
                                resizeContent();
//                                $(document).undelegate('[data-action="generateXml"]', 'click');
//                                $(document).delegate('[data-action="generateXml"]', 'click', function () {
//                                    $(".dialog-content").remove();
//                                });
                            });
                        },
                        error: function (response) {
                            console.log(response);
                            dialogContent.find('.loading-img').fadeOut(500, function () {
                                dialogContent.html('').append('Произошла ошибка');
                                resizeContent();
//                                $(document).undelegate('[data-action="generateXml"]', 'click');
//                                $(document).delegate('[data-action="generateXml"]', 'click', function () {
//                                    $(".dialog-content").remove();
//                                });
                            });
                        }
                    });
                }
            }
        ],
        open: function () {
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: url,
                success: function (answer) {
                    dialogContent.html('').append(answer);
                    resizeContent();
                    handlePagination();
                    handleActions();
                }
            });
        }
    });
};
license.licenseSaveLicNumber = function (id) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: "/eo/affairs/add-license-number/" + id,
        async: false,
        success: function (answer) {
            if (answer == '1') {
                return true;
            } else {
                return false;
            }
        }
    });
};
/**
 * Отправляем лицензии в реестр
 * @param array|string guid гуиды лицензий которые хотим отправить в реестр
 * @param callable success onSuccess
 * @param callable error onError
 * @returns undefined
 */
license.sendLicensesToReestr = function (guid, success, error) {
    if (!Array.isArray(guid)) {
        return license.sendLicensesToReestr([guid], success, error);
    }
    $.ajax({
        type: 'POST',
        dataType: 'html',
        url: '/eo/index/exportLicensesToXml',
        data: {
            'id[]': guid
        },
        success: function (response) {
            if (typeof (success) === 'function') {
                success(response);
            }
        },
        error: function (response) {
            if (typeof (error) === 'function') {
                error(response);
            }
        }
    });
};