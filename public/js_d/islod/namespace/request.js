/**
 * file  request.js
 * url:  /js/islod/namespace/request.js
 * path: /public/js/islod/namespace/request.js
 * 
 * @type type
 */

var request = {};

/**
 * Открывает окно создание заявления
 * @param reqId - если задано то форма корректируется под форму редактирования
 * @param organizationId - если задано, то в качестве выбранной организации выбирается переданная организация
 */
request.openAdd = function (reqId, organizationId, dealId) {
    var dialog = {
        width: Math.round((screen.width / 100) * 87),
        height: Math.round((screen.height / 100) * 87) - 100
    };

    var dContent = $(
            '<div class="dialog-request-edit">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

    var data = {};
    if(reqId)
        data.requestId = reqId;
    if(organizationId)
        data.organizationId = organizationId;
    if(dealId)
        data.dealId = dealId;

    $(dContent).dialog({
        title: (reqId)?"Изменение заявления":"Новое заявление",
        width: dialog.width,
        height: dialog.height,
        modal: true,
        close: function(event, ui) {
            $(this).dialog("destroy");
        },
        open: function () {
            //Отсылаем запрос на получение диалога
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/add",
                data: data,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));
                    });
                }
            });
        }
    });
};

/**
 * Открывает окно редактирования
 * @param {bool} isTest
 */
request.openEdit = function (isTest) {
    var dialog = {
        width: Math.round(jQuery(window).width() - 40),
        height: Math.round(jQuery(window).height() - 20)
    };

    jQuery('.dialog-request-edit').remove();
    var dContent = $(
            '<div class="dialog-request-edit">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');

    console.log(isTest);
    var requestId = "test";
    if (!isTest)
        //requestId = $(".row_appl.active_appl").attr("id").split("_")[1];
        requestId = $(".license-table-content tr.select").attr("requetsid");
    if (isTest && isTest !== true) {
        requestId = isTest;
    }
    console.info('преред дерг 1');
    console.log(requestId);
        
    $(dContent).dialog({
        title: "Редактирование заявления",
        width: dialog.width,
        height: dialog.height ,
        modal: true,
        close: function () {
            //RequestFrames.save(true);
            $(".dialog-request-edit").remove();
            if(RequestFrames.needRefreshDeal()){
                //рефрешим диалог с делом
                //закрыть дело
                jQuery('.dialog-deal-edit').remove();
                //открыть дело
                affairs.openEditAffair();
                //рефрешим списки в таблице
                $(".dealFilterWrap .btApply").click();
            }
        },
        open: function () {
            //Отсылаем запрос на получение диалога
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/edit/" + requestId,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));
                        RequestFrames.init();
                    });
                }
            });
        }
    });
};

request.openEditFrames = function () {
    request.openEdit(true);
};

request.searchOrg = function(){
    url = encodeURI(url);//персонально для ИЕ
    $.ajax({
        type: 'POST',
        url: url,
        success: function (answer) {
            $("#popupWindow").html(answer);
            $("#popupWindow").show();
        }
    });
}

/**
 * творим случ число
 *
 * @param {type} id
 * @returns {Number}
 */
var getRandomInt = function getRandomInt(min, max) {
                      return Math.floor(Math.random() * (max - min)) + min;
                    }

/**
 * делаем запрос  на этот вуз запись на прием в у вуза,
 * если есть то получим фио записавшегося и впихиваем это в форму
 *
 * @param {type} id - ид вуза
 * @returns {Number}
 */
request.getFioFromEqueue = function (id){
        if( 'undefinsed' == typeof (id) || '' == id ){
            return 0;
        }
        $("#prescriptions-container").html("Запрос информации...");
        $.ajax({
            url: '/eo/requests/get-fio-from-equeue/' + id,
            dataType: 'json',
            type: 'POST',
            success: function (answer) {
                console.log('дела - новое заявление - выбор организации - запрос в очередь - ответ получен');
                //результирующий отраблтчик
                var doResult = function(i){
                            if ( 'undefined' == typeof (i) ){
                                i = 0;
                            }
                            i = parseInt(i);
                            if ( 'undefined' != typeof (answer['data']) ){
                                //answer = jQuery.parseJSON(answer);
                                if ( "feb3633090b54d709c7a476f0982c3d6" == jQuery('#admissionType.dataField[name="admissionType"]').val() ){
                                    if ( 'undefined' != typeof (answer['data'][i]) ){
                                        if ( 'undefined' != typeof (answer['data'][i]['lastName'])
                                            &&
                                            '' == jQuery('.dataField[name="requestRepresentativeLastName"]').val()
                                            ){
                                            jQuery('.dataField[name="requestRepresentativeLastName"]').val(answer['data'][i]['lastName']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['firstName'])
                                            &&
                                            '' == jQuery('.dataField[name="requestRepresentativeFirstName"]').val()
                                            ){
                                            jQuery('.dataField[name="requestRepresentativeFirstName"]').val(answer['data'][i]['firstName']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['middleName'])
                                            &&
                                            '' == jQuery('.dataField[name="requestRepresentativeMiddleName"]').val()
                                            ){
                                            jQuery('.dataField[name="requestRepresentativeMiddleName"]').val(answer['data'][i]['middleName']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['phone'])
                                            &&
                                            '' == jQuery('.dataField[name="mobilePhoneNumber"]').val()
                                            ){
                                            jQuery('.dataField[name="mobilePhoneNumber"]').val(answer['data'][i]['phone']);
                                        }
                                        if ( 'undefined' != typeof (answer['data'][i]['email'])
                                            &&
                                            '' == jQuery('.dataField[name="eMail"]').val()
                                            ){
                                            jQuery('.dataField[name="eMail"]').val(answer['data'][i]['email']);
                                        }
                                    }
                                }
                                // если из очереди приехала процедура, по выберем ее
                                if ( 'undefined' != typeof (answer['data'][i]) ){
                                    if ( 'undefined' != typeof (answer['data'][i]['lisensingProcedureId']) ){
                                        if( jQuery('.dataField[name="licensingProcedureId"] option[value="'+answer['data'][i]['lisensingProcedureId']+'"]').length){
                                            jQuery('.dataField[name="licensingProcedureId"] option[value="'+answer['data'][i]['lisensingProcedureId']+'"]').prop('selected','selected');
                                            jQuery('.dataField[name="licensingProcedureId"] option[value="'+answer['data'][i]['lisensingProcedureId']+'"]').change();
                                            //проставим галочки по причинам, если есть
                                            if ( 'undefined' != typeof (answer['data'][i]['licenseReasonAllId']) ){
                                                for (var i in answer['data'][i]['licenseReasonAllId']){
                                                    if( jQuery('.cbField[type="checkbox"][value="'+answer['data'][i]['licenseReasonAllId'][i]+'"]').length){
                                                        jQuery('.cbField[type="checkbox"][value="'+answer['data'][i]['licenseReasonAllId'][i]+'"]').prop('checked','checked');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                //разбираем ответ
                request.getFioFromEqueueParse(answer, doResult);
                /*
                if ( 'undefined' != typeof (answer['data']) ){
                    //определим количесто приехавших зашисей очередей
                    var colEque = answer['data'].length;
                    if(colEque>1){
                        //если много записей
                        jQuery('<div class="select-eque-dialog"></div>').dialog({
                           'title': 'Выбор записи из очереди',
                           'modal': true,
                           'width':440,
                           'open': function(){
                                jQuery(this).append('<form><table class="select-eque-dialog-table" ></table></form>');
                                for (var i in answer['data']){
                                    var title = '';
                                    if ( 'undefined' != typeof (answer['data'][i]['lisensingProcedureTitle']) ){
                                        title = answer['data'][i]['lisensingProcedureTitle'];
                                    }
                                    else{
                                        title = 'не определена причина';
                                    }
                                    var id = 'select-eque-dialog-input-id-'+getRandomInt(1000,9999)+getRandomInt(1000,9999)+getRandomInt(1000,9999);
                                    var label = jQuery('<label></label>').append(title);
                                    jQuery(label).attr('for', id);
                                    var input = jQuery('<input >');
                                    jQuery(input).attr('type', 'radio');
                                    jQuery(input).attr('id', id);
                                    jQuery(input).attr('name', 'radioName');
                                    jQuery(input).attr('value',i);
                                    jQuery(input).attr('class', 'select-eque-dialog-input');
                                    var td1 = jQuery('<td></td>').append(label);
                                    var td2 = jQuery('<td></td>').append(input);
                                    var tr  = jQuery('<tr></tr>').append(td1);
                                    tr.append(td2);
                                    jQuery(this).find('.select-eque-dialog-table').append(tr);
                                }
                           },
                           'close': function (){
                                jQuery(this).remove();
                            },
                            'buttons':{
                                'Сохранить': function(){
                                    if(jQuery(this).find('input.select-eque-dialog-input:checked').length){
                                        doResult( jQuery(this).find('input.select-eque-dialog-input:checked').val() );
                                        jQuery(this).dialog('close');
                                    }
                                    else{
                                        useTopHoverTitleError('выберите запись из очереди');
                                    }
                                },
                                'Отмена': function (){
                                    jQuery(this).dialog('close');
                                }
                            }

                        });
                    }
                    else{
                        //усли одна запись
                        doResult( );
                    }
                }
                */
            },
            error:function(e){
                //сбросим сообщения
                useTopHoverTitle("",1);
                useTopHoverTitleError("Ошибка связи");
                console.error('дела - новое заявление - выбор организации - ошибка связи при дергании данных очереди');
                console.log(e);
                console.log(e.responseText);
            },

        });
    }

/**
 * разборка
 * @param {type} answer
 * @returns {undefined}
 */
request.getFioFromEqueueParse = function (answer, doResult){
                if ( 'undefined' != typeof (answer['data']) ){
                    //определим количесто приехавших зашисей очередей
                    var colEque = answer['data'].length;
                    if(colEque>1){
                        //если много записей
                        jQuery('<div class="select-eque-dialog"></div>').dialog({
                           'title': 'Выбор записи из очереди',
                           'modal': true,
                           'width':440,
                           'open': function(){
                                jQuery(this).append('<form><table class="select-eque-dialog-table" ></table></form>');
                                for (var i in answer['data']){
                                    var title = '';
                                    if ( 'undefined' != typeof (answer['data'][i]['lisensingProcedureTitle']) ){
                                        title = answer['data'][i]['lisensingProcedureTitle'];
                                    }
                                    else{
                                        title = 'не определена причина';
                                    }
                                    var id = 'select-eque-dialog-input-id-'+getRandomInt(1000,9999)+getRandomInt(1000,9999)+getRandomInt(1000,9999);
                                    var label = jQuery('<label></label>').append(title);
                                    jQuery(label).attr('for', id);
                                    var input = jQuery('<input >');
                                    jQuery(input).attr('type', 'radio');
                                    jQuery(input).attr('id', id);
                                    jQuery(input).attr('name', 'radioName');
                                    jQuery(input).attr('value',i);
                                    jQuery(input).attr('class', 'select-eque-dialog-input');
                                    var td1 = jQuery('<td></td>').append(label);
                                    var td2 = jQuery('<td></td>').append(input);
                                    var tr  = jQuery('<tr></tr>').append(td1);
                                    tr.append(td2);
                                    jQuery(this).find('.select-eque-dialog-table').append(tr);
                                }
                           },
                           'close': function (){
                                jQuery(this).remove();
                            },
                            'buttons':{
                                'Сохранить': function(){
                                    if(jQuery(this).find('input.select-eque-dialog-input:checked').length){
                                        doResult( jQuery(this).find('input.select-eque-dialog-input:checked').val() );
                                        jQuery(this).dialog('close');
                                    }
                                    else{
                                        useTopHoverTitleError('выберите запись из очереди');
                                    }
                                },
                                'Отмена': function (){
                                    jQuery(this).dialog('close');
                                }
                            }

                        });
                    }
                    else{
                        //усли одна запись
                        doResult( );
                    }
                }

}


/**
 * Метод обрабатывает диалог выбора организации
 * @param callback - функция обработки результатов выбора
 */
request.selectOrgDialog = function(callback){

    function getPrescriptions(id){
        $("#prescriptions-container").html("Запрос информации...");
        $.ajax({
            url: '/eo/requests/get-prescriptions-by-org/' + id,
            success: function (answer) {
                $("#prescriptions-container").html($(answer));
                $("#prescriptions-container")
            },
            type: 'POST',
            dataType: "html"
        });
    }



    function searchOrgAdd(page, needClear, type){
        if(page == null || page != 0){
            var nextPage=$("#nextPage").val();
            nextPage=parseInt(nextPage);
            var maxPage=$("#maxPage").val();
            maxPage=parseInt(maxPage);
            page= nextPage;
        }

        if(/* $("#filteredTable tbody").height()-$("#filteredTable").scrollTop() == 359
             && */page==0 || nextPage<= maxPage ){
            if($(".searchOrganization").val()==''){
                var url='/eo/requests/get-organizations-select/'+page;
            }else{
                var url='/eo/requests/get-organizations-select/'+page+'/'+$(".searchOrganization").val();
            }
            $.ajax({
                url: url,
                beforeSend: function (xhr) {
                    $(".loading-overlay").show();
                    $("#getNextResult").parent().parent().remove();
                    $("#nextPage").remove();
                    $("#maxPage").remove();
                },
                success: function(answer){
                    console.info("дела - новое дело - заявление - получение еще организаций - ответ получен");
                    //почемуто рисовалка дергается 2 раза, вставим клин
                    var isAlredyShow = false;
                    $(".loading-overlay").fadeOut(250, function(){
                        console.info("дела - новое дело - заявление - получение еще организаций - вставим ответ");
                        if( isAlredyShow ){
                            return 0;
                        }
                        if(needClear){
                            $(".dialog-org-select").html("").append(answer);
                        } else {
                            $("#filteredTable tbody").append(answer);
                        }
                        isAlredyShow = true;
                    })
                },
                error:function(e){
                    useTopHoverTitleError("Возникла ошибка");
                    $(".loading-overlay").fadeOut(250);
                    console.log("Ошибка");
                    console.log(e);
                }
            });
        }
    }

    //Функция

    var dialog = {
        width: 646,
        height: 446
    };

    var dContent = $(
        '<div class="dialog-org-select">' +
            "<div style='text-align: center;margin-top:50px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
        '</div>');

    $(dContent).dialog({
        title: "Выбор организации",
        width: dialog.width,
        height: dialog.height,
        modal: true,
        close: function(event, ui) {
            $(this).dialog("destroy");
        },
        open: function () {
            //Отсылаем запрос на получение диалога
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/get-organizations-select/0",
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));

                        //фокус в сирчинпут
                        var el=document.getElementById('filt_fillials');
                        el.focus();
                        el.setSelectionRange(el.value.length,el.value.length);

                        //обработчики
                        dContent.undelegate("#getNextResult");
                        dContent.delegate("#getNextResult","click",function(){
                            searchOrgAdd();
                        });
                        dContent.undelegate(".searchOrganization","change");
                        dContent.delegate(".searchOrganization","change",function(){
                            /*
                            if($("#add_reorg_struct").hasClass('clicked')){
                                showPop('/ron/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
                            }else{
                                showPop('/ron/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
                            }*/
                            searchOrgAdd(0, true);
                        });
                        $(dContent).delegate("#add_org","click", function(){
                            if(callback) {
                                var idOrg = $('.selected_org:checked').val();
                                var orgName = $('.selected_org:checked').attr('org_name');
                                var lawAddress = $('.selected_org:checked').attr('lawaddress');
                                callback(idOrg, orgName, lawAddress);
                                $(".dialog-org-select").remove();
                            } else {
                                $("#Id_organization").val($('.selected_org:checked').val());
                                $("#nameOrganization").val($('.selected_org:checked').attr('org_name'));
                                getPrescriptions($('.selected_org:checked').val());
                                request.getFioFromEqueue($('.selected_org:checked').val());
                                $(".dialog-org-select").remove();
                            }
                        });
                        //
                        $("#filteredTable", dContent).undelegate(".reorg_string","click");
                        $("#filteredTable", dContent).delegate(".reorg_string","click",function(){
                            console.log('тык по строчке');
                            console.log(this);
                            jQuery("input",this).prop("checked",true);
                        });

                        $(dContent).delegate(".btClose", "click", function(){
                            $(".dialog-org-select").remove();
                        });
                    });
                }
            });
        }
    });
}

/**
 * Функция отображает дилог с списком предписаний
 * @param orgId
 */
dialog_showPrescriptions = function(orgId){
    var dContent = $(
        '<div class="dialog-prescriptions">' +
        "<div style='text-align: center;margin-top:40px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "Неисполненные предписания организации",
        width: 800,
        height: 600,
        modal: true,
        close: function(event, ui) {
            $(this).dialog("destroy");
        },
        open: function() {
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/requests/get-org-prescriptions/" + orgId,
                success: function(answer) {
                    //console.log(answer);
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        $(".btClose", dContent).click(function(){
                            $(".dialog, .dialog-prescriptions").remove();
                        });
                    });
                }
            });
        }
    });
}



    /**
     * запускаем вывод выбора файла скан копии
     *
     * @param {type} fileId
     * @param {type} options
     * @returns {Number}
     */
    request.sendScanInput = function(licenseId){
        var prefix = 'лицензирование - карточка заявления - загрузка скана: ';
        console.info(prefix + 'начинаем');
        if('string'  != typeof(licenseId) || '' == licenseId){
            console.log(prefix + 'нет лицензии');
            useTopHoverTitleError("Не указано заявления");
            return 0;
        }
        if( $('#progressbar').length ){
             useTopHoverTitleError("Документ в загрузке");
             return 0;
        }
        //пока не надо
        //if(!confirm("Хотите загрузить макет")){
        //    return 0;
        //}
        console.log(prefix + 'рисуемм полее ввода файла');
        jQuery('.request-scan-input').remove();
        jQuery('body').append(
                '<input '
              + 'name="maket" '
              + 'type="file" '
              + 'class="request-scan-input" '
              + 'style="display: none" '
              + 'onchange="licenseSendFileAction('
                    +'\''+licenseId+'\','
                    +'\'.request-scan-input\', '
                    +'\'RequestScan\', '
                    +'\'a.request-scan-link\''
              + ')" '
              + '>');
        jQuery('.request-scan-input').click();
    }



    /**
     * запускаем вывод выбора файла подписаного документа
     *
     * @param {type} fileId
     * @param {type} options
     * @returns {Number}
     */
    request.sendDocumentInput = function(licenseId, element){
        var prefix = 'лицензирование - карточка заявления - загрузка документа подписаного эл подписью: ';
        console.info(prefix + 'начинаем');
        if('string'  != typeof(licenseId) || '' == licenseId){
            console.log(prefix + 'нет лицензии');
            useTopHoverTitleError("Не указано заявления");
            return 0;
        }
        if( $('#progressbar').length ){
             useTopHoverTitleError("Документ в загрузке");
             return 0;
        }
        //пока не надо
        //if(!confirm("Хотите загрузить макет")){
        //    return 0;
        //}
        request.sendDocumentInputOkFunction = function(){}
        var ownerClassCode = 'RequestDocument';
        var okFunction = '\'\'' ;
        var elementToChange = '\'a.request-document-link\'';
        var docExtention = '';
        var docTypeId = '';
        if( 'undefined' != typeof(element) ){            
            var temp = jQuery(element).attr('elementToChange');
            if( 'undefined' != typeof(temp) ){
                elementToChange = '\''+temp+'\'';
            }
            var temp = jQuery(element).attr('ownerClassCode');
            if( 'undefined' != typeof(temp) ){
                ownerClassCode = temp;
            }
            var temp = jQuery(element).attr('doAftert');
            if( 'undefined' != typeof(temp) ){
                if( 1 == temp ){
                    var parent = jQuery( element ).parent();
                    var vrap = jQuery( parent ).find('.FileListWrap');
                    var max = 9999999999;
                    var min = 1000000000;
                    var sluch =  Math.floor(Math.random() * (max - min + 1)) + min;
                    var id = 'doc-file-'+sluch;
                    var idRow = 'doc-file-row-'+sluch;
                    jQuery( vrap ).append('<div id="'+ idRow +'"><a id="'+id+'" class="hiden"></a></div>')
                    elementToChange = '\'a#'+id+'\'';

                    var idReload = 'doc-file-del-'+sluch;
                    var d = document.createElement('img');
                    var reloadElement = jQuery(d);
                    
                    jQuery(reloadElement).attr( 'id', idReload );
                    jQuery(reloadElement).attr( 'alt',"Заменить документ");
                    jQuery(reloadElement).attr( 'title',"Заменить документ");
                    jQuery(reloadElement).attr( 'src' , "/img/islod/pencil.png");
                    jQuery(reloadElement).attr( 'onmouseover' , "this.src='/img/islod/pencil_color.png';" );
                    jQuery(reloadElement).attr( 'onmouseout', "this.src='/img/islod/pencil.png';" );
                    jQuery(reloadElement).attr( 'class' , "iconFile request-document-send" );
                    jQuery(reloadElement).attr( 'onclick' , 'request.sendDocumentInput( jQuery(\'#requestId\').val() , this );' );
                    jQuery(reloadElement).attr( 'fileId' , "" );
                    jQuery(reloadElement).attr( 'doAftert' , "delete" );
                    jQuery(reloadElement).attr( 'elementToChange' , "a#"+id );
                    jQuery(reloadElement).attr( 'ownerClassCode' , "RequestFileChange" );
                    jQuery(reloadElement).attr( 'docExtention' , jQuery(element).attr('docExtention') );
                    //jQuery(reloadElement).attr( 'checkSign' , "1" );
                    
                    var signChek = false;
                    var temp2 = jQuery(element).attr('checkSign');
                    if( 'undefined' != typeof(temp2) && parseInt(temp2) ){
                        signChek = true;
                        jQuery(reloadElement).attr( 'checkSign' , temp2 );
                    }
                    
                    jQuery( '#'+idRow ).append(reloadElement);
                                        
                    var idDel = 'doc-file-del-'+sluch;
                    var d = document.createElement('img');
                    var delElement = jQuery(d);
                    jQuery(delElement).attr( 'id', idDel );
                    jQuery(delElement).attr( 'alt',"Удалить документ");
                    jQuery(delElement).attr( 'title',"Удалить документ");
                    jQuery(delElement).attr( 'src' , "/img/islod/delete.png");
                    jQuery(delElement).attr( 'onmouseover' , "this.src='/img/islod/delete_color.png';" );
                    jQuery(delElement).attr( 'onmouseout', "this.src='/img/islod/delete.png';" );
                    jQuery(delElement).attr( 'class' , "iconFile request-document-delete" );
                    jQuery(delElement).attr( 'onclick' , "licenseDeleteFileAsk( this );" );
                    jQuery(delElement).attr( 'fileId' , "" );
                    jQuery(delElement).attr( 'doAftert' , "delete" );
                    jQuery(delElement).attr( 'elementToChange' , "noneIt" );
                    jQuery(delElement).attr( 'elementToDelete' , '#'+idRow );                    
                    jQuery( '#'+idRow ).append(delElement);
                    
                    if( signChek ){
                        var idSign = 'doc-file-sign-'+sluch;
                        var d = document.createElement('img');
                        var signElement = jQuery(d);
                        jQuery(signElement).attr( 'id', idSign );
                        jQuery(signElement).attr( 'alt',"проверка подписи");
                        jQuery(signElement).attr( 'title',"проверка подписи");
                        jQuery(signElement).attr( 'src' , "/img/grey-icon.png");
                        jQuery(signElement).attr( 'class' , "request-sign-check" );
                        jQuery(signElement).attr( 'onclick' , "request.chekSignFileAction('fileId', this);" );
                        jQuery(signElement).attr( 'fileId' , "" );
                        jQuery(signElement).attr( 'style' , "display:none;" );
                        jQuery( '#'+idRow ).append(signElement);
                    }

                    
                    
                    request.sendDocumentInputOkFunction = function(){
                        jQuery(delElement)      .attr( 'fileId' , jQuery('a#'+id).attr('fileId') );
                        jQuery(reloadElement)   .attr( 'fileId' , jQuery('a#'+id).attr('fileId') );
                        jQuery(signElement)     .attr( 'fileId' , jQuery('a#'+id).attr('fileId') );
                        jQuery(signElement).click();
                        //alert ('ggg');
                    }
                    
                    okFunction = 'request.sendDocumentInputOkFunction'; 
                    
                }
                if( 'change' == temp ){
                    request.sendDocumentInputOkFunction = function(){
                        var parent          = jQuery(element).parent();
                        var aElement        = jQuery(parent).find('a');
                        var delElement      = jQuery(parent).find('.request-document-delet');
                        var reloadElement   = jQuery(parent).find('.request-document-send');
                        var signElement     = jQuery(parent).find('.request-sign-check');
                        jQuery(delElement)      .attr( 'fileId' , jQuery(aElement).attr('fileId') );
                        jQuery(reloadElement)   .attr( 'fileId' , jQuery(aElement).attr('fileId') );
                        jQuery(signElement)     .attr( 'fileId' , jQuery(aElement).attr('fileId') );
                        jQuery(signElement).click();
                        //alert ('ggg');
                    }
                    
                    okFunction = 'request.sendDocumentInputOkFunction';                     
                }
                
            }            
            var temp = jQuery(element).attr('docExtention');
            if( 'undefined' != typeof(temp) ){
                docExtention = ' accept="'+temp+'" ';
            }
            //отсылаем тип документа
            var temp = jQuery(element).attr('docTypeId');
            if( 'undefined' != typeof(temp) ){
                docTypeId = temp;
            }
            //отсылаем ид файла
            var temp = jQuery(element).attr('fileId');
            if( 'undefined' != typeof(temp) ){
                docTypeId = temp;
            }
            
        }
        
        console.log(prefix + 'рисуемм полее ввода файла');
        jQuery('.request-scan-input').remove();
        var s =  '<input '
              + 'name="maket" '
              + 'type="file" '
              + 'class="request-scan-input" '
              + 'style="display: none" '
              //+ (signChek)?' acept=".sig" ': ' '
              + 'onchange="licenseSendFileAction('
                    +'\''+licenseId+'\','
                    +'\'.request-scan-input\', '
                    +'\''+ownerClassCode + '\', '
                    + elementToChange + ', '
                    + okFunction + ', '
                    + '\'' + docTypeId + '\''
              + ')" '
              + docExtention
              + '>'
            
            
        jQuery('body').append( s );
        if( signChek ){
            jQuery('.request-scan-input').attr("acept" , ".sig");
        }
        jQuery('.request-scan-input').click();
    }

    /**
     * запускаем вывод выбора файла подписи
     *
     * @param {type} element
     * @param {type} options
     * @returns {Number}
     */
    request.sendSignInput = function(licenseId, element){
        var prefix = 'лицензирование - карточка заявления - загрузка документа подписаного эл подписью: ';
        console.info(prefix + 'начинаем');
        if('string'  != typeof(licenseId) || '' == licenseId){
            console.log(prefix + 'нет лицензии');
            useTopHoverTitleError("Не указано заявления");
            return 0;
        }
        var fileId;
        fileId = jQuery(element).attr('fileId');
        if('string'  != typeof(fileId) ){
            fileId = '';
        }
        if( $('#progressbar').length ){
             useTopHoverTitleError("Документ в загрузке");
             return 0;
        }
        request.sendSignInputAfter = function(){
            jQuery('.request-sign-check').show();
            jQuery('.request-sign-check ').click();            
        }
        //пока не надо
        //if(!confirm("Хотите загрузить макет")){
        //    return 0;
        //}
        var answerElement = jQuery(element).parent().find('a.request-sign-link');
        var answerId = 'a.request-sign-link[uniqId=\\\''+jQuery(answerElement).attr('uniqId')+'\\\']';
         
        console.log(prefix + 'рисуемм полее ввода файла');
        jQuery('.request-scan-input').remove();
        jQuery('body').append(
                '<input '
              + 'name="maket" '
              + 'type="file" '
              + 'class="request-scan-input" '
              + 'style="display: none" '
              + 'accept=".pdf.sig" '
              + 'onchange="licenseSendFileAction('
                    +'\''+licenseId+'\','
                    +'\'.request-scan-input\', '
                    +'\'RequestSign\', '
                    +'\''+answerId+'\', '
                    +'request.sendSignInputAfter, '
                    +'\''+fileId+'\''
              + ')" '
              + '>');
        jQuery('.request-scan-input').click();
    }

    /**
     * заготовка
     * выполняетс посл е того ака загрузился файцл  с подписаным документом
     * 
     * @returns {undefined}
     */
    request.sendSignInputAfter = function(){
        console.log( 'запуск после чего-то' );
    }

    /**
     * отправлеяем файл файлохранилще
     * @param {type} licenseId       - ид лицензии или записи
     * @param {type} inputFrom       - элемент с которого брать файл - инпут файл
     * @param {type} ownerClassCode  - код владельца куда пихать
     * @param {type} elementToChenge - элемент куда прописать ответ - анкер
     * @returns {Number|Boolean}
     */
    function licenseSendFileAction(
            licenseId,          inputFrom,      ownerClassCode,
            elementToChenge,    okFunction,     fileId){
        if( $('#progressbar').length ){
             useTopHoverTitleError("Документ в загрузке");
             return;
        }
        if(typeof (licenseId) == 'undefined' || licenseId == ''){
            useTopHoverTitleError('не задана лицензия');
            return 0;
        }
        if(typeof (ownerClassCode) == 'undefined' || ownerClassCode == ''){
            ownerClassCode = 'License';
        }
        if(typeof (elementToChenge) == 'undefined' || elementToChenge == ''){
            elementToChenge = 'a.license-scan-link';
        }
        if(typeof (inputFrom) == 'undefined' || inputFrom == ''){
            inputFrom = '.license-scan-input';
        }
        var fileInput = jQuery(inputFrom);
        if( typeof (fileInput)  == 'undefined' || jQuery(fileInput).length == 0 || jQuery(fileInput).val() == ''){
            useTopHoverTitleError('нет файла');
            return 0;
        }
        var fd = new FormData();
        console.log($(fileInput)[0].files[0]);
        var fileData =  $(fileInput)[0].files[0];
        if(typeof fileData == "undefined"){
            useTopHoverTitleError('нет файла');
            return 0;
        }
        fd.append('file', fileData);
        fd.append('ownerClassCode', ownerClassCode);
        fd.append('ownerId', licenseId);
        if('string'  == typeof(fileId) && '' != fileId ){
            fd.append('fileId', fileId);
        }

        var prefix = 'ЛОД - формирование переформирование лицензии: ';
        console.info(prefix + 'начинаем');
        $.ajax({
            type: 'POST',
            //timeout: 5000,
            dataType: "json",
            data: fd,
            processData: false,
            contentType: false,
            url: '/eo/license/send-file',
            beforeSend: function(){
                console.log('карточка лицензии - отправка файла на сервер - начинается отправка');
                $(".loading-overlay").show();
                jQuery('body').prepend('<progress id="progressbar" value="0" max="100"></progress>');
                useTopHoverTitle("Отправляем файл на сервер", 100);
            },
            xhr: function(){
                var xhr = $.ajaxSettings.xhr();
                // Устанавливаем обработчик отправлялки
                if(xhr.upload){
                    xhr.upload.addEventListener('progress', function(evt){
                      if (evt.lengthComputable) {
                        var percentComplete = Math.ceil(evt.loaded / evt.total * 100);
                        var progressBar = $('#progressbar');
                        progressBar.val(percentComplete);
                        useTopHoverTitle("Загружено "+percentComplete+"%", 1000);
                        if( 100 == percentComplete  ){
                            useTopHoverTitle("Файл загружен, идет обработка",50000);
                        }
                      }
                    }, false);
                }else{
                    console.error('карточка лиценизии - оправка файла - какаято непонятка с отправлялкой');
                }
                return xhr;
            },
            success: function(answer) {
                //$("#hover-title").fadeOut('fast');
                var message = '';
                if(typeof (answer.message) != 'undefined' && answer.message != ''){
                    message = '' + answer.message;
                }
                var consoleMsg = '';
                if(typeof (answer.console) != 'undefined' && answer.console != ''){
                    consoleMsg = ' ' + answer.console;
                }
                if(answer.status == 'Error'){
                    useTopHoverTitle("", 1);
                    useTopHoverTitleError("Ошибка загрузки документа ("+message+")", 2001);
                    console.error(prefix+'Ошибка загрузки документа');
                    console.log(prefix + consoleMsg);
                    return ;
                }
                if(answer.status == 'Ok'){                    
                    if(typeof (answer.message) == 'undefined' && answer.message == ''){
                        message = 'Сохранено ' ;
                    }
                    useTopHoverTitle( message, 2001);
                    //обновим ссылку
                    var link = '#';
                    if(typeof (answer.link) != 'undefined' && answer.link != ''){
                        link =  answer.link;
                    }
                    var fileName = 'Лиценния.docx';
                    if(typeof (answer.fileName) != 'undefined' && answer.fileName != ''){
                        fileName =  answer.fileName;
                    }
                    jQuery(elementToChenge).removeClass('hiden');
                    jQuery(elementToChenge).attr('href','/uploads/'+link);
                    jQuery(elementToChenge).attr('download',''+fileName);
                    jQuery(elementToChenge).attr('href','/uploads/'+link);
                    jQuery(elementToChenge).text(fileName);
                    jQuery(elementToChenge).attr('fileId',answer.fileId);
                    if( 'RequestSign' == ownerClassCode ){
                        jQuery(elementToChenge).parent().find('img.request-sign-send').attr('fileId',answer.fileId);   
                        jQuery(elementToChenge).parent().find('img.request-sign-check').attr('onclick' , 'request.chekSignFileAction(\''+answer.fileId+'\', this);');
                        jQuery(elementToChenge).parent().find('img.request-sign-check').attr('src' , '/img/ok-green.png');
                        //впихивание сканов заялвения
                        if( 'undefined' == typeof(answer.scanName) ){
                            answer.scanName = '';
                        }
                        if( 'undefined' == typeof(answer.scanLink) ){
                            answer.scanLink = '';
                            answer.scanName = '';
                        }
                        jQuery('.request-scan-link').attr( 'href' ,  answer.scanLink );
                        jQuery('.request-scan-link').text( answer.scanName );
                    }
                    //jQuery('a.license-scan-remake').text('Переформировать макет');
                    console.info(prefix + 'файл впихан');
                    //отработать позитив
                    if( 'function' == typeof(okFunction) ){
                        okFunction();
                    }
                    return ;
                }
                useTopHoverTitleError("Ошибка при формирвоании\n", 2001);
                console.info(prefix + 'Непонятно чегото');
                console.log(answer);
            },
            error:function(e){
                //сбросим сообщения
                useTopHoverTitle("",1);
                useTopHoverTitleError("Ошибка связи");
                console.error(prefix + 'ошибка связи при формированиии');
                console.log(e);
                console.log(e.responseText);
            },
            complete:function(){
                $('#progressbar').remove();
                $(".loading-overlay").fadeOut(250);
                console.info(prefix + 'впихивание окончено');
            }
        });
        return false;
    }
    
    function licenseDeleteFileAsk( element ,  okFunction){
        if( confirm('Удалить файл') ){
            var fileId = jQuery(element).attr( 'fileId');
            var elementToDelete = jQuery(element).attr( 'elementToDelete');
            licenseDeleteFileAction( fileId  , elementToDelete  , okFunction);
        }        
    }
    
    function licenseDeleteFileAction( idFile , elementToRemove , okFunction){
        console.log( '  удаление файлов - хотим удалить файл');
        $.ajax({
            type: 'POST',
            //timeout: 5000,
            dataType: "json",
            data: {
                ownerClassCode: "deleteRequestDocumentFile",
                ownerId: idFile
            },
            url: '/eo/license/send-file',
            beforeSend: function(){ 
                useTopHoverTitle("Отправляем данные на сервер", 10000);
            },
            success: function(answer) {
                //$("#hover-title").fadeOut('fast');
                var message = '';
                if(typeof (answer.message) != 'undefined' && answer.message != ''){
                    message = '' + answer.message;
                }
                var consoleMsg = '';
                if(typeof (answer.console) != 'undefined' && answer.console != ''){
                    consoleMsg = ' ' + answer.console;
                }
                if(answer.status == 'Error'){
                    useTopHoverTitle("", 1);
                    useTopHoverTitleError("Ошибка загрузки документа ("+message+")", 2001);
                    console.error(' удаление файла  - Ошибка загрузки документа');
                    console.log( 'удаление файла  - ' + consoleMsg);
                    return ;
                }
                if(answer.status == 'Ok'){                    
                    if(typeof (answer.message) == 'undefined' && answer.message == ''){
                        message = 'Удалено ' ;
                    }
                    if( 'undefined' != typeof(elementToRemove) ){
                        jQuery( elementToRemove ).remove();
                    }
                    useTopHoverTitle( message, 2003);
                    //обновим ссылку
                    console.info('удаление файла  - файл впихан');
                    //отработать позитив
                    if( 'function' == typeof(okFunction) ){
                        okFunction();
                    }
                    return ;
                }
                useTopHoverTitleError("Ошибка при удалении\n", 2001);
                console.info(prefix + 'Непонятно чегото');
                console.log(answer);
            },
            error:function(e){
                //сбросим сообщения
                useTopHoverTitle("",1);
                useTopHoverTitleError("Ошибка связи");
                console.error(prefix + 'ошибка связи при формированиии');
                console.log(e);
                console.log(e.responseText);
            },
            complete:function(){
                $('#progressbar').remove();
                $(".loading-overlay").fadeOut(250);
                console.info( 'удаелние файла - впихивание окончено');
            }
        });
        return false;
    }

    /**
     * проверяем подпись на сервере
     *
     * @param {type} requestId      - ид файла в фацлохнанилиде
     * @param {type} statusElement  - элемент картинки, который меняем
     * @returns {undefined}
     */
    request.chekSignFileAction = function(requestId, statusElement){
        console.log('лицензирование - карточка заявления - проверка подписи');
        jQuery(statusElement).show();
        if( 'undefined' == typeof(requestId) || '' == requestId){
            console.info('карточка заявления - проверка подписи - не указан ид  файл');
            return 0;
        }
        if( 'fileId' == requestId || 'idFile' == requestId){
             requestId = jQuery(statusElement).attr(requestId);
        }
        if( 'undefined' == typeof(requestId) || '' == requestId){
            console.info('карточка заявления - проверка подписи - не до указан ид  файл');
            return 0;
        }
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '/eo/license/request-chek-sign/'+requestId,
            success: function(answer) {
                var message = '';
                if(typeof (answer.message) != 'undefined' && answer.message != ''){
                    message = ' ' + answer.message;
                }
                var consoleMsg = '';
                if(typeof (answer.console) != 'undefined' && answer.console != ''){
                    consoleMsg = ' ' + answer.console;
                }
                if(answer.status == 'Error'){
                    if('' == message ){
                        message = 'Подпись не прошла проверку';
                    }
                    //useTopHoverTitleError("Ошибка заливки\n"+message, 2001);
                    console.error('лицензирование - карточка заявления - проверка подписи - ' + message);
                    console.log(consoleMsg);
                    jQuery(statusElement).attr('src','/img/red-icon.png');
                    jQuery(statusElement).attr('title',message);
                    return ;
                }
                if(answer.status == 'Ok'){
                    console.info('лицензирование - карточка заявления - проверка подписи - положительно');
                    //jQuery(statusElement).attr('src','/img/accept-icon2.png');
                    jQuery(statusElement).attr('src','/img/ok-green.png');
                    jQuery(statusElement).attr('title','подпись прошла проверку');
                    if( 'undefined' != typeof(answer.signData) ){
                        var title = '';
                        var signData = answer.signData;
                        if( 'undefined' != typeof(answer.signData.SubjectName) ){
                            for(var i in answer.signData.SubjectName){
                                title += '[' + i + ']=' + answer.signData.SubjectName[i] + "\n";
                            }                            
                        }
                        if( 'undefined' != typeof(answer.signData.dateBegin) ){
                            title += 'годен с ' +  answer.signData.dateBegin + "\n";
                        }
                        if( 'undefined' != typeof(answer.signData.dateEnd) ){
                            title += 'годен до ' + answer.signData.dateEnd + "\n";
                        }
                        jQuery(statusElement).attr('title',title);
                    }
                    
                    return 0;
                }
                //useTopHoverTitleError("Ошибка при формирвоании\n", 2001);
                console.error('лицензирование - карточка заявления - проверка подписи - Непонятно чегото');
                console.log(answer);
                jQuery(statusElement).attr('src','/img/red-icon.png');
                jQuery(statusElement).attr('title','подпись не прошла проверку');
            },
            error:function(e){
                //сбросим сообщения
                //useTopHoverTitle("",1);
                //useTopHoverTitleError("Ошибка связи");
                console.error( 'лицензирование - карточка заявления - проверка подписи - ошибка связи');
                console.log(e);
                console.log(e.responseText);
            },            
        });
    }


    /**
     * карточка заявления - филиалы -слайдер / аккордион - скрывалка открывалка
     * 
     * @param {type} element
     * @returns {undefined}
     */
    request.showHideData = function(element){
        var batanHiden = jQuery(jQuery(element).parents('.BayanPerechen')[0]).find('.BayanHiden');
        var batanHidenIsHiden = jQuery(batanHiden).is(':hidden');
        if( batanHidenIsHiden ){
            jQuery('.BayanHiden').hide();
            jQuery(batanHiden).show();
        }
        else{
            jQuery(batanHiden).hide();
        }        
    }
    
    /**
     * открытая - лицензирование - карточка заявления - документы -  дополнительные файлы - рисуем диалог
     * 
     * @type type
     */
    request.documentDopAdd = function(element){
        console.log( ' ту-ту тык' );
        var s = '<div class="dialog-request-dicument-file-dop-add">'
                +'<table>'
                    +'<tr>'
                        +'<td width="100">Тип документа</td>'
                        +'<td><select>'
            for(var i in documentTypeAll){
                var ss = '<option value="'+i+'">'+ documentTypeAll[i]+'</option>'
                s += ss;
            }
                    s +=   '</select></td>'
                    +'</tr>'
                    //+'<tr>'
                    //    +'<td>Файл</td>'
                    //    +'<td><input type="file"></td>'
                    //+'</tr>'
                +'</table>'
            +'</div>'


        var dContent = $( s );
        $(dContent).dialog({
            title: "Загрузка документа",
            width: '600',
            //height: 600,
            modal: true,
            close: function(event, ui) {
                $(this).dialog("destroy");
            },
            open: function() {
                var content = jQuery(this).find('.dialog-request-dicument-file-dop-add');
                jQuery( content ).append('<div>ddddd</div>');
            },
            buttons: [{
                text: 'Загрузить',
                click: function() {
                    console.log( ' открытая - лицензироване - карточка заявлени - дополнительный файл - загрузить' );
                    var selectedTypeId=jQuery(this).find('option:selected').val();
                    var selectedTypeName=jQuery(this).find('option:selected').text();
                    if( !jQuery('.document-type-table tr[DocumentTypeId="'+selectedTypeId+'"]').length ){
                         console.log( ' открытая - лицензироване - карточка заявлени - дополнительный файл - загрузить - такого типа документов еще нет' );
                        jQuery('.document-type-table').append(
                            '<tr DocumentTypeId="'+selectedTypeId+'">'
                            +'<td>'+selectedTypeName
                            +'</td><td>'
                                +'<div class="containerWrapFile">'
                                    +'<img'
                                        +' alt="Загрузить документ"'
                                        +' title="Загрузить документ"'
                                        +' onclick="request.sendDocumentInput( jQuery(\'#requestId\').val() ,this);"'
                                        +' class="request-document-send request-document-send-add"'
                                        +' src="/img/islod/pencil.png"'
                                        +' onmouseover="this.src=\'/img/islod/pencil_color.png\';"'
                                        +' onmouseout ="this.src=\'/img/islod/pencil.png\';"'
                                        +' disabled="disabled"'
                                        +' ownerClassCode="RequestFile"'
                                        +' doAftert="1"'
                                        +' docTypeId="'+selectedTypeId+'"'
                                        +' checkSign="1" '                                        
                                    +'>'
                                    +'<div class="FileListWrap " >'
                                    +'</div>'                                
                            +'</div>'
                            +'</td></tr>')
                    }
                    jQuery('.document-type-table tr[DocumentTypeId="'+selectedTypeId+'"]').find('.request-document-send-add').click();
                    jQuery(this).dialog('close');

                }
            }, {
                text: 'Закрыть',
                click: function() {
                    $(this).dialog('close');
                }
            }]

        });
        
    }
