var organization = {};

/** 
 * Работа с вкладками в окне организации
 */
organization.organizationInfoTabActionCaller = function (tabButton) {
    var tabName = $(tabButton).attr("tab-container"),
            containerClass = '.' + tabName;
    console.log(tabButton);
    switch (tabName) {
        case 'tab-document':
            if (empty($(containerClass).html())) {
                // _getEducationProgramsTab( containerClass);
            }
            break;
        case 'tab-license':
            if (empty($(containerClass).html())) {
                organization._getLicenseFromOrganizationTab(containerClass);
                $('.supplement-info thead').hide();
                $('.license-rows').hide();
                $('.supplement-rows').hide();
                $('.choose-license').change();
            }
            break;
        case 'tab-branch':
            if (empty($(containerClass).html())) {
               organization._getBranchfromOrgDialog(containerClass);
            }
            break;
        case 'tab-deal':
            if (empty($(containerClass).html())) {
                  organization._getDealListFromOrg( containerClass);
            }
            break;
            //
        case 'tab-request':
            if (empty($(containerClass).html())) {
                  organization._getRequestFromOrg( containerClass);
            }
            break;
        case 'tab-notice':
            if (empty($(containerClass).html())) {
                organization._getDocumentInfoFromOrg(containerClass);
            }
            break;
        case 'tab-distribution-documents':
            if (empty($(containerClass).html())) {
                organization.administrativeDocumentFromOrg( containerClass);
            }
            break;
        case 'tab-certificate':
            if (empty($(containerClass).html())) {
                organization.getCertificates( containerClass);
            }
        break;
        default:
            break;
    }
}

/**
 * получение документов организацтт
 */
organization._getDocumentInfoFromOrg = function (containerClass) {
    var orgId = $('.current-organization-dialog-id').val(),
        tabPage = _getPage().documentFromOrg(orgId);

    if (tabPage) {
        $(containerClass).html(tabPage);

        resizeContent( $(".document-org-info-block .content-container .viewzone") );
        specialTableResize(".document-org-info-block .content-container");
    }
}

/**
 * Получение вкладки с образовательными программами
 */
organization._getEducationProgramsTab = function (containerClass) {
    var supId = $('.current-supplement-dialog-id').val(),
            tabPage = _getPage({ rootPath : '/eo/license/' }).eduProgramsTabWhereSupplemenId(supId);

    if (tabPage) {
        $(containerClass).html(tabPage);
    }
}

organization._getLicenseFromOrganizationTab = function( containerClass){
    var orgId = $('.current-organization-dialog-id').val(),
                    tabPage = _getPage().licenseFromOrg(orgId);

                if( tabPage){
                    $( containerClass).html( tabPage);
                }
                resizeContent(".viewzone.supplement-info");
                specialTableResize(".organization-info-dialog .tab-license");
                if($('.license-info-block .supplement-info table tbody').height()<190){
                    $('.license-info-block .supplement-info table tbody').height(190)
                }
}

organization.createOrganizationInfoDialog = function( organizationId, clone){

    var selectorName = 'organization-info-dialog',
        page = _getPage().organizationInfoPageFromId( organizationId),
        title = $(page).find('.organization-dialog-title').val(),
        clone = clone || false;
	    dialogToW = percentageOfTheScreen( 90 ),
        dialogToH = percentageOfTheScreen( 78 );

    _storage('foundersFromOrganization').clear('add');
    _storage('foundersFromOrganization').clear('delete');
    
    if( page){
        createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
		   height: dialogToH.height,
           width: dialogToW.width,
           binder : OrganizationInfoBinder( organizationId),
           preCreated:function(settingData){
               var rights = $(settingData['data']).find('.current-rights').val();
               if( rights != '1'){
                   settingData['buttons'].shift();
                   settingData['buttons'].shift();
               }
           },
           buttons: [
            {
                text: "Новое заявление",
                "class": 'newRequest full-left-button',
                click: function () {
                   request.openAdd(null,organizationId);
                }
            },
            {
                text: "Копировать",
                "class": 'copyOrganization full-left-button',
                click: function () {
                   organization.copyCurrentOrganization( organizationId);
                }
            },
            {
                text: "Справка",
                "class": 'spravka full-left-button',
                click: function () {
                   var newWin = window.open('/islod/index/export-spravka/'+organizationId,'_blank'); 
                   console.log(newWin);
                   if(jQuery('#error',newWin).length){
                        useTopHoverTitleError('Возникла ошибка');
                        console.log(jQuery('#error',newWin).text());    
                   }
                }
            },
            /*
            {
                text: "Сохранить",
                "class": 'left saveLicenseSupplement',
                click: function () {
                    //отключена проверка на обязательные поля
//                    if( !organization.orgReqPageFieldIsHaveEpmty()){
                        organization.saveOrganizationPage( organizationId);
//                        $(this).dialog('close');
//                    }else{
//                        useTopHoverTitle('Обязательные поля не заполнены', 2000);
//                    }
                }
            },
            */
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            },

            ],
            afterClose: function(){
                var mainOrg =  _storage('reopen').get('mainOrg');
                
                if(typeof(mainOrg)!=="undefined" && !empty( mainOrg.dialog)){
                    $('body').append( mainOrg.dialog);
                    OrganizationInfoBinder( mainOrg.id).bind();
                    _storage('foundersFromOrganization').clear('add');
                    _storage('foundersFromOrganization').clear('delete');
                    organization.getFoundersWhenOrganizationInfoLoad( organizationId);
                }
            }
        });

        organization.getFoundersWhenOrganizationInfoLoad( organizationId);

        $(".orgs-date:not([readonly])").each(function(i,el){
            $(el).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd.mm.yy"
            });
        });
    }
}

organization.getFoundersWhenOrganizationInfoLoad = function(organizationId){
    var data = _getData().foundersFromOrgnizationId( organizationId),
        str = '',
        foundersRows = '';

    for( var key in data){
        foundersRows += organization.createFounderListFromOrgElement( key, data[ key]);
    }
    var str = createDualHtmlElement('div', foundersRows,{ class:'founders-container',});
    $('.founders-place').html('').append( str);
}


 organization.createFounderListFromOrgElement = function( id, value , isNew){
    var isNew = isNew || 'false',
        elem = value,
        founders = createDualHtmlElement('span', elem ,{ class:'founders-name'}),
        removeButton = createDualHtmlElement(
                                                'span',
                                                'X' ,
                                                {
                                                    class:'remove-founders x-button',
                                                    linkid: id
                                                }
                                            ),
        appendStr = founders+' '+removeButton;

        return createDualHtmlElement('div', appendStr ,{ class:'founders-row', isnew: isNew});
}

/**
 * создает диалог для выбора учередителей
 * @param {type} organizationId
 * @returns {undefined}
 */
 organization.createFoundersDialog=function( organizationId){
    var selectorName = 'founders-list-dialog',
        page = _getPage().getFoundersPagesTest({ id:organizationId, page : 1} ),
        title = 'Учредители',
        dialogWindow = '';
		dialog = {
        height: Math.round((screen.height / 100) * 82) - 20
		};
    if( page){
        dialogWindow = createStandartDialog({
           className : selectorName,
           data : page,
		   height: dialog.height,
           title : title,
           binder : foundersChooseListBinder(),
           buttons: [
               /*
            {
                text: "Добавить",
                class: 'left saveLicenseSupplement',
                click: function () {
                    var arrFounders = [];

                    if( !empty( _storage('foundersFromOrganization').get('add'))){
                        arrFounders = _storage('foundersFromOrganization').get('add');
                    }
                    
                    $(".dialog-founders-choose-list-container table tr").each(function(i,el){
                        var chk =  $(el).find('.add-current-founder'),
                            title = $(el).find('.founders-name').html();
                        
                        if( $(chk).prop('checked')){
                            arrFounders.push({
                                id : $(el).attr('founderid'),
                                title : title
                            });
                            $('.founders-place').append( organization.createFounderListFromOrgElement(
                                                                $(el).attr('founderid'),
                                                                title,
                                                                'true'
                                            ));
                        }
                    });
                     _storage('foundersFromOrganization').add('add',arrFounders);
                     $(this).dialog('close');
                }
            },
            */
            {
                text: "Создать",
                "class": 'right active-button',
                click: function () {
                    organization._createCreateFoundersDialog();
                }
            },
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
            ]
        });

        
        
        resizeContent($(".dialog-founders-choose-list-container .container .viewzone"));
        specialTableResize(".dialog-founders-choose-list-container");
        organization.removeAddedFounders();
    }
}

organization.removeAddedFounders = function(){
    $(  _storage('foundersFromOrganization').get('add') ).each(function(i,el){
            $('.dialog-founders-choose-list-container .founders-table tr[founderid="'+el.id+'"]').remove();
        
    });
}

organization.saveOrganizationPage = function(){
    var data = $("form#organizationInfoForm").serialize(),
        param = {
            add: getArrSubElementFromArr( _storage('foundersFromOrganization').get('add'), 'id'),
            delete: _storage('foundersFromOrganization').get('delete') || []
        };
    
    data += '&'+ $.param({founders : param });
    _save().organizationPageSave( data);
}

/**
 * Если передан orgId, то диалог переключается в режим добавления филиала
 * в организацию, айди которой передан
 * @param orgId
 */
organization.createNewOrganizationInfoDialog = function(orgId, callback){

    var selectorName = 'organization-info-dialog',
        page = _getPage().newOrganizationPage(orgId),
        title = $(page).find('.organization-dialog-title').val(),
	    dialog = {
			height: Math.round((screen.height / 100) * 82) - 50
        },
        newDialogObj = null;

    _storage('foundersFromOrganization').clear('add');
    _storage('foundersFromOrganization').clear('delete');

    if( page){
        newDialogObj = createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
		   height: dialog.height,
           binder : newOrganizationBinder(),
           buttons: [
               /*
            {
                text: "Сохранить",
                "class": 'left saveLicenseSupplement',
                click: function () {
//                    if( !orgReqPageFieldIsHaveEpmty()){
                        organization.saveNewOrganizationPage();
                        $(this).dialog('close');
//                    }else{
//                        useTopHoverTitle('Обязательные поля не заполнены', 2000);
//                    }
                    
                }
            },
            */
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
            ],
           afterClose: callback
        });
    }

    $(".orgs-date").each(function(i,el){
        $(el).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd.mm.yy"
        });
    });
    
    return newDialogObj;
}

organization.saveNewOrganizationPage =function(){
    var data = $("form#organizationInfoForm").serialize(),
        param = {
            add: getArrSubElementFromArr( _storage('foundersFromOrganization').get('add'), 'id')
        };
       
    data += '&'+ $.param({founders : param });
    _save().newOrganizationPageSave( data);
}

organization._getBranchfromOrgDialog =function( containerClass){
    var orgId = $('.current-organization-dialog-id').val(),
                    tabPage = _getPage().branchFromOrg(orgId);

                if( tabPage){
                    $( containerClass).html( tabPage);
                    specialTableResize();
                }
}

organization.orgReqPageFieldIsHaveEpmty = function (){
    return _requiredFieldsHasEmpty(
            $('#organizationInfoForm input:not([name="acronym"],[name="GosRegNum"],[name="stateRegistrationCertificateNumber"],[name="stateRegistrationCertificateDate"],[name="stateRegistrationCertificateAuthorityAddress"],[name="Phones"],[name="Mails"],[name="Www"])')
         ,
        function(el){
            $(el).css({'border-color':'red'});
            var parent = $(el).parent(),
                errorText = $(parent).find('.input-error-text');
        
            if( errorText.length ==0){
                $(parent).prepend('<div class="input-error-text">Поле не может быть пустым!</div>');
            };
        },
        function(el){
            $(el).css({'border-color':'#c2c2c2'});
            var parent = $(el).parent(),
                errorText = $(parent).find('.input-error-text');

            if( !(errorText.length ==0)){
                $(errorText).remove();
            }
        }
    );
}

organization._createCreateFoundersDialog =function (){
    var selectorName = 'create-founder-dialog',
        page = _getPage().createFoundersPage(),
        title = 'Создание учредителя';

    if( page){
        createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
		   width: 700,
		   height: 450,
           binder : createFoundersBinder(),
           buttons: [
               /*
            {
                text: "Сохранить",
                "class": 'left saveLicenseSupplement',
                click: function () {
                    organization.saveNewFounders( );
                    $(this).dialog('close');
                    $('.founders-list-dialog').dialog('close');
                    var organizationId = $('.current-organization-dialog-id').val() || '';
                    organization.createFoundersDialog( organizationId);
                }
            },
            */
            {
                text: "Отменить",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
            ]
        });
        $('.create-founders .founders-data select').change();
    }
}

organization.saveNewFounders = function(){
    var data = $(".founders-page-block .founders-data").serialize();
    _save().saveNewFounders( data);
    $('ui-dialog-titlebar-close');
}

organization._createCreateNotificationDialog =function ( docId){
     var selectorName = 'org-notification-dialog',
        page = _getPage().notificationFromId( docId),
        title = $(page).find('.notification-dialog-title').val();
	 var dialog = {
        width: Math.round((screen.width / 100) * 75),
        height: Math.round((screen.height / 100) * 78),
    };
    if( page){
        createStandartDialog({
           className : selectorName,
           data : page,
		   width: dialog.width,
		   height: dialog.height,
           title : title,
//           binder : ,
           buttons: [
                {
                    text: "Отменить",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });
        $('.dateInput').datepicker();
    }
}

organization._getDealListFromOrg = function( containerClass ){
    var orgId = $('.current-organization-dialog-id').val(),
                    tabPage = _getPage().dealList( orgId);

                if( tabPage){
                    $( containerClass).html( tabPage);
                    resizeContent(containerClass+ " .viewzone");
                    specialTableResize(containerClass);
                }
}

organization._getDealRowsFromOrgFiltered = function( dealBlock, orgId, filters, order , page){
    var animation = _getAnimation().dContent();
    $( dealBlock+' .deals-from-org tbody').html('').append(animation);
    
    var page = _getPage().dealList( orgId, filters, order, page),
        rows = $( page).find('.deals-from-org tbody tr'),
        pages = $( page).find('#pagination').children();

    if( rows){
        $( dealBlock+' .deals-from-org tbody').html('').append( rows);
        $( dealBlock+' #pagination').html('').append( pages);
    }
}

organization._createHeadOrgDialog =function ( ){
     var selectorName = 'head-org-to-new-org',
        page = _getPage().getHeadOrgList( ),
        title = 'Выберите головную организацию';

    if( page){
        createStandartDialog({
           className : selectorName,
           data : page,
           title : title,
		   width: 600,
		   height: 550,
           binder : headOrganizationBinder(),
           buttons: [
                {
                    text: "Выбрать",
                    "class": 'right',
                    click: function () {
                         var selectors = {
                                inputIdHeadOrg : '#organizationInfoForm input[name="fk_eiisParentEducationalOrganization"]',
                                chooseHeadOrg : '#organizationInfoForm .choose-head-organization'
                            },
                            selectedRadio = ('.head-organization-table input[type="radio"]:checked');

                        if( $(selectedRadio).length > 0){
                            var idOrg = $(selectedRadio).val(),
                                nameOrg = $(selectedRadio).parent().parent().find('.organization-name').html();
                            $( selectors.inputIdHeadOrg).val( idOrg);
                            $( selectors.chooseHeadOrg).val( nameOrg);
                        }
                        $(this).dialog('close');
                    }
                },
                {
                    text: "Отменить",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });
        var curPage = $('.hidden-data input[name="page"]').val();
        _storage('organization-head-choose').add('page', curPage );
    }
}

/**
 * Функция отображает дилог с списком предписаний
 * @param orgId
 */
organization.getPrescriptionsDialog = function(){
    var selectorName = 'prescriptions-dialog',
        orgId = $('.current-organization-dialog-id').val(),
        page = _getPage().prescriptionsDialog( orgId),
        prescDocUrl = $(page).find('.url-get-prscription').val(),
        title = 'Неисполненные предписания организации';

    if( page){
        createStandartDialog({
           className : selectorName,
           data : page,
		   width: 800,
		   height: 600,
           title : title,
//           binder : ,
           buttons: [
                {
                    text: "Печать",
                    "class": 'right print-prescription-bt',
                    click: function () {
                        $('body').append('<a class="temporarityDownloadLink" href="'+prescDocUrl+'"></a>');
                        $('.temporarityDownloadLink').click();
                        window.open(prescDocUrl);
                    }
                },
                {
                    text: "Отменить",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        });
    }
}

organization.getNameForm = function( name){
    var nameForms = _getData().morphyOrganization( name);

    if( $('.tempCaseData').length>0){
        $('.tempCaseData').remove();
    }

    $('textarea[name="NameGenCase"]').val( nameForms['genitive']);
    $('textarea[name="NameDatCase"]').val( nameForms['dative']);
    $('textarea[name="NameInstrCase"]').val( nameForms['ablative']);
    $('textarea[name="NameAccCase"]').val( nameForms['accusative']);
};

organization.administrativeDocumentFromOrg = function( containerClass){
    var orgId = $('.current-organization-dialog-id').val(),
                tabPage = _getPage().administrativeDocument( orgId);

                if( tabPage){
                    $( containerClass).html( tabPage);
                    resizeContent(".administrative-document-info-block.viewzone");
                    specialTableResize(".tab-distribution-documents");
                }
};

organization._getRequestFromOrg = function( containerClass){
    var orgId = $('.current-organization-dialog-id').val(),
                tabPage = _getPage().requestFromOrg( orgId);

                if( tabPage){
                    $( containerClass).html( tabPage);
                    resizeContent(".dialog-application-info-container.viewzone");
                    specialTableResize(".tab-request");
                }
};

organization.copyCurrentOrganization = function( organizationId){
    var currentPage = _getPage().organizationInfoPageFromId( organizationId),
        dialog = $('.ui-dialog.ui-widget').detach(),
        data = [];

    $(currentPage).find('[name]:not([name="lod_stateCode"])').each(function(i,el){
        if($(el).attr('checked') == 'checked'){
            data[ $(el).attr('name')] = 'checked';
        }else{
            data[ $(el).attr('name')] = $(el).val();
        }
    });

    var reopenStorage = _storage('reopen');
    reopenStorage.clear( 'mainPage' );
    reopenStorage.add( 'mainOrgFromCopy', {
                        dialog: dialog,
                        id: organizationId,
                    });
    OrganizationInfoBinder().unbind();

    var newOrg = organization.createNewOrganizationInfoDialog( null, function(){
                var mainOrg =  _storage('reopen').get('mainOrgFromCopy');

                if(typeof(mainOrg)!=="undefined" && !empty( mainOrg.dialog)){
                    $('body').append( mainOrg.dialog);
                    OrganizationInfoBinder( mainOrg.id).bind();
                    var foundersStorage = _storage('foundersFromOrganization');
                    foundersStorage.clear('add');
                    foundersStorage.clear('delete');
                    organization.getFoundersWhenOrganizationInfoLoad( organizationId);
                }
            });

    $(newOrg).find('[name]').each(function(i,el){
        var name = $(el).attr('name');

        if( data[name] !== undefined){
            if( data[name] == 'checked'){
                $(el).prop('checked', 'checked');
            }else{
                $(el).val(data[name]);
            }
            
        }
        
    });

    $('select[name="propertyFormId"]').combobox();
    $('select[name="fk_eiisEducationalOrganizationType"]').combobox();
    $('select[name="fk_eiisEducationalOrganizationProperties"]').combobox();
    $('select[name="Branch"]').change();
}

organization.getCertificates = function( containerClass){
     var orgId = $('.current-organization-dialog-id').val(),
        tabPage = _getPage().accreditationFromOrg({ 'orgId' : orgId });

    if( tabPage){
        $( containerClass).html( tabPage);
        $('.accreditation-block select[name="certificate"]').change();
    }
}

organization.getFullFilter = function( ){
     var selectorName = 'full-org-filter-dialog',
        page = _getPage().fullOrgFilter( );

    if( page){
        createStandartDialog({
           className : selectorName,
           data : page,
		   width: 400,
		   height: 720,
           modal: false,
           preCreated: function(){
                var heig = $(".org-filter").height()+$('.viewzone').height();
                $('.viewzone').height(heig);
                $(".org-filter").slideUp('slow');
           },
           afterClose: function(){
                var heig = $('.viewzone').height() - $(".org-filter").height();
                $('.viewzone').height(heig);
                $(".org-filter").slideDown('slow');
           },
           title : 'Полный фильтр организаций',
//           binder : ,
           buttons: [
               {
                    text: "Закрыть",
                    "class": 'right',
                    click: function () {
                        $(this).dialog('close');
                    }
                },
                {
                    text: "Применить",
                    "class": 'right',
                    click: function () {
                        organization.applyFullFilter();
                    }
                }
            ]
        });
    }
}

organization.applyFullFilter = function(){
    var form = $('#organization-full-filter-form'),
        data = $( form).serializeArray(),
        page = _getPage().applyFullOrgFilter(data);
    
    $(".content_td tbody.content").html("").append($(page).find(".content > tr"));
    $(".organiztion-list-count").html("").append($(page).find(".organiztion-list-count").html());
    $("#pagination").html("").append( $(page).find("#pagination").html());
    _paginationHandler();
    afterServerQueryFunction();
}