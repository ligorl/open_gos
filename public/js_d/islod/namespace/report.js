var report = report || {};

report.generateDialog = function( count,title){
    var dialogClassName = 'generate-report-diaog',
        dialogClass = '.'+dialogClassName;
    $('body').append('<div class="'+dialogClassName+'"></div>');

    return $( dialogClass).dialog({
        title: 'Поиск данных для генерации отчета',
        width: 600,
        height: 430,
        modal: true,
		buttons: [            
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ],
        open: function () {
            $( dialogClass).append(
                    '<div>'+
                    '<h2>'+title+'</h2>'+
					'<div class="report-generate-count"> Всего найдено записей: '+count+'</div>'+
                    '<div class="report-generate-processed"></div>'+
                    '<div class="report-pre-finish"></div>'+
                    '<div class="report-generate-body"></div>'+
                    '</div>'
            );
            $(".report-generate-body").append(_getAnimation().dContent());
            $(".ui-dialog-titlebar-close").hide();
        },
        close: function () {
                $( dialogClass).remove();
        }
    });
}

report.checkAndConfirmation = function( ){

}

report.reportGenerate = function( dt ,title){
    var data = _getData().report( dt );
    var reportType = '';
    for( var key in dt){
        var el = dt[key];
        if(el.name == 'type'){
            reportType = el.value;
        }
    }

    if( data['reportStatus'] == 'started'){
        var dialog = report.generateDialog( data['count'],title),
            generateIsProcess = true;
    
        setTimeout(function(){
            var reportData = _getData().reportProgress( reportType);
            report.check( reportData, reportType);
        });
        
    }
}

report.check = function( reportData , reportType){

     if( reportData['next_step'] == 'FINISH'){
       var proc = 'Обработано: '+reportData['processed'];
       $('.report-generate-processed').html(proc);
       $('.report-pre-finish').html('Генерация xml файла...');
       setTimeout(function(){
            reportData = _getData().reportFinish( reportType , reportType);///
            $('.report-generate-body').html('<a class="add_report" href="/public/reports/'+reportData['file']+'">Скачать отчет</link>');
            $(".ui-dialog-titlebar-close").show();
       });
    }
    
    if( reportData['next_step'] == 'CHECK'){
        var proc = 'Обработано: '+reportData['processed'];
        $('.report-generate-processed').html(proc);
        setTimeout(function(){
            var reportData = _getData().reportProgress( reportType);
            report.check( reportData , reportType);
        });
        
    }
   
}



