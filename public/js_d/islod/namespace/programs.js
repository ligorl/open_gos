/**
 * обработчик  на главныые - программы - обработчеки
 *  url:  /islod/index/programs
 *  path: /trunk/public/js/islod/namespace/programs.js
 */

console.log('обработчик  на главныые - программы - скрипты');

var programs = { edtDialog: { dContent:'' }, searchDialog: { dContent:'' }, newQualification: { dContent:'' } };


//programs.edtDialog.dContent = '';
/**
 * лод - главная - рограммы - кнопка открыть
 *
 * @returns {undefined}
 */
programs.edtDialog.openProgramButonAction = function(){
    if( jQuery('table.edu-programs-table tbody tr.active ').length ){
        jQuery('table.edu-programs-table tbody tr.active td:first ').dblclick();
    }
    else{
        useTopHoverTitleError("Выберите запись");
    }

}

programs.edtDialog.newProgramButonAction = function(){

    this.openProgramDialog('new');
}

/**
 *  лод - главная - рограммы - рисуем и дергаем карточку программы
 *
 * @param {type} programId - собственно ид программы
 * @returns {Number}
 */
programs.edtDialog.openProgramDialog = function( programId ){

    console.log(this);

    if( 'string' != typeof(programId) || '' == programId ){
        return 0;
    }
        var dialog = {
            width : Math.round( (jQuery(window).width()/100)*87  ),
            height: Math.round( (jQuery(window).height()/100)*77 )
        };
        this.dContent = $(
            '<div class="dialog-programs-edit">' +
                "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading_islod.gif' /></div></div>" +
            '</div>');
        var dContent = this.dContent;
        $(this.dContent).dialog({
            title: 'Образовательная программа',
            width: dialog.width,
            height: dialog.height,
            modal: true,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            close: function () {
                $(this).remove();
            },
            open: function () {

                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/islod/index/get-programs-edit-dialog/"+programId,
                    //data: {
                    //    orgId: orgId,
                    //    requestId: requestId,
                    //    type: type
                    //},
                    success: function (answer) {
                        $(".loading-img", dContent).fadeOut(500, function () {
                            $(dContent).html("").append(answer);
                            //тык по уровню, чтоб типо выбиралка правильно подогрузился
                            jQuery(dContent).find('select[name="programEditDialog[fk_eiisEduLevels]"]').change();
                            //убираем импут при тыке вне инпута
                            $(dContent).mouseup(function (e){
                                var element =  $(".qualification-table-name input:visible");
                                var div = jQuery(element).parents(".qualification-table-name").first();
                                //var div = $(".qualification-table-name");
                                var okElement = jQuery('.qualification-table-edit-ok');
                                if(
                                    !div.is(e.target) 
                                 && div.has(e.target).length === 0
                                 && !okElement.is(e.target)
                                ){
                                    programs.edtDialog.onQualificationEditUnClick();
                                }
                                console.log('убираем видимость инпута');
                                //if( jQuery(el.currentTarget).is() )
                                //programs.edtDialog.onQualificationEditUnClick();
                            });
                        });
                    },
                    error:function(e){
                        //сбросим сообщения
                        useTopHoverTitle("",1);
                        useTopHoverTitleError("Ошибка связи");
                        console.error( 'ошибка связи при формированиии');
                        console.log(e);
                        console.log(e.responseText);
                        //$(dContent).dialog("close");
                        $(".loading-img", dContent).fadeOut(500, function () {
                           jQuery(dContent).dialog('close');
                        });
                    },
                    complete: function () {

                    }
                });

            },
                buttons: [
                    {
                        text: 'Сохранить',
                        click: function() {
                            if($('input[name="programEditDialog[Code]"]').val() == "" || $('input[name="programEditDialog[Name]"]').val() == ""){
                                useTopHoverTitleError("Заполните обязательные поля");
                                if($('input[name="programEditDialog[Code]"]').val() == ""){
                                    $('input[name="programEditDialog[Code]"]').css({'border':'1px solid red'});
                                }else{
                                    $('input[name="programEditDialog[Code]"]').css({'border':'1px solid #c2c2c2'});
                                }

                                if($('input[name="programEditDialog[Name]"]').val() == ""){
                                    $('input[name="programEditDialog[Name]"]').css({'border':'1px solid red'});
                                }else{
                                    $('input[name="programEditDialog[Name]"]').css({'border':'1px solid #c2c2c2'});
                                }
                            }else{
                                programs.edtDialog.onSaveAction();
                            }
                        }
                    },
                    {
                        text: 'Закрыть',
                        click: function() {
                            $(this).dialog('close');
                        }
                    }
                ]
        });
}

//флаг на наличие права редактирование программ
programs.hasAccesEdit = false;

/**
 * инициация обработчиков тыков по табличкае с программати програм
 *
 * @returns {undefined}
 */
programs.initAction = function(){
    //родные обрботчики
    programsBinder().unbind();
    programsBinder().bind();


    if( this.hasAccesEdit ){
        /**
         * лод - главная - рограммы - вешаем обработчик на клик по табличке
         *
         * @param {type} param
         */
        jQuery('table.edu-programs-table tbody tr td').unbind('click');
        jQuery('table.edu-programs-table tbody tr td').bind('click',function(){
            jQuery('.programs-container .active').removeClass('active');
            jQuery(this).parent().addClass('active');
        });


        /**
         * лод - главная - рограммы - вешаем обработчи на дубль клик
         */
        jQuery('table.edu-programs-table tbody tr td').unbind('dblclick');
        jQuery('table.edu-programs-table tbody tr td').bind('dblclick',function(){
            //пометим как выбраное
            jQuery(this).click();

            var elemenIdtAttr = jQuery(this).parent().attr('id');
            if( 'string' != typeof(elemenIdtAttr) || '' == elemenIdtAttr ){
                return 0;
            }
            var elemenIdArray = elemenIdtAttr.split('program_');
            console.log(elemenId);
            console.log(typeof(elemenId));
            if( 'string' != typeof(elemenIdArray[1]) || '' == elemenIdArray[1] ){
                return 0;
            }
            var elemenId = elemenIdArray[1];
            programs.edtDialog.openProgramDialog( elemenId );
        });
    }
    console.log('обработчики тыка по программ повешены');
}

/**
 * лод - главная - рограммы - карточка программы - обработчик изменения уровня программы
 *
 * @param {type} element
 * @returns {undefined}
 */
programs.edtDialog.onChangeLevelAction = function(element){
    console.log('программы - редакт программы - изменяется уровень программы');
    var levelId = jQuery(element).val();
    var typeSelect = jQuery(this.dContent).find('select[name="programEditDialog[fk_eiisEduProgramType]"]');
    jQuery(typeSelect ).find('option').hide();
    if( jQuery(typeSelect ).find('option[levelId="'+levelId+'"]').length ){
        jQuery(typeSelect ).find('option[levelId="'+levelId+'"]').show();
    }
    else{
        jQuery(typeSelect ).append('<option levelId="'+levelId+'" value="">Отсутствует</option>');
    }
    if( !jQuery(typeSelect).find('option[levelId="'+levelId+'"]:selected').length ){
        jQuery(typeSelect).find('option[levelId="'+levelId+'"]:first').prop('selected', 'selected');
    }
}

/**
 * лод - главная - рограммы - карточка программы - тык по редактировать наименование квалиффикации - выводит поле ввода
 *
 * @param {type} element
 * @returns {undefined}
 */
programs.edtDialog.onQualificationEditClick = function(element){
    //уберем вводимые инпуты
    programs.edtDialog.onQualificationEditUnClick();
    
    jQuery(this.dContent).find('td.qualification-table-name input').hide();
    var parent = jQuery( jQuery(element).parents('.qualification-table-row')[0] );
    jQuery(parent).find('input').show();
    jQuery(parent).find('span').hide();
    jQuery(parent).find('.qualification-table-edit-ok').show();
    jQuery(parent).find('.qualification-table-to-edit').hide();
}

/**
 * лод - главная - рограммы - карточка программы - тык по редактировать наименование квалиффикации - скрывает поля ввода программ
 *
 * @param {type} element
 * @returns {undefined}
 */
programs.edtDialog.onQualificationEditUnClick = function(element){
    //обновить данны в инпуте, чтоб совпадало
    var rows = jQuery(this.dContent).find('td.qualification-table-name');    
    jQuery.each(rows,  function(ind, ele){
        var name = jQuery(ele).find('span').text();
        jQuery(ele).find('input').val(name);
    } );
    
    jQuery(this.dContent).find('td.qualification-table-name input').hide();
    jQuery(this.dContent).find('td.qualification-table-name span').show();
    jQuery(this.dContent).find('.qualification-table-edit-ok').hide();
    jQuery(this.dContent).find('.qualification-table-to-edit').show();
}

/**
 * лод - главная - рограммы - карточка программы - удаляем запись о квалификации
 *
 * @param {type} element
 * @returns {undefined}
 */
programs.edtDialog.onQualificationDeleteClick = function(element){
    var onQualificationDeleteClickDo = function(){
        console.log('удаляем элемент');
        jQuery(jQuery(element).parents('.qualification-table-row')[0]).remove();
    }
    confirmLod( 'Удалить квалификацию', onQualificationDeleteClickDo );
}

/**
 * лод - главная - рограммы - карточка программы -обработчи по тык на добавить кввалификацию
 *
 * @param {type} element
 * @returns {undefined}
 */
programs.edtDialog.openSearchQulificationAction = function(element){
   programs.searchDialog.openDialog();
}

/**
 * лод - главная - рограммы - карточка программы -тык по принять правки названия квалификации
 *
 * @param {type} element
 * @returns {undefined}
 */
programs.edtDialog.onQualificationEditOkAction = function(element){
    console.log('принять правки наименования квалиффикации');
    var val = jQuery( jQuery(element).parents('.qualification-table-row')[0] ).find('.qualification-table-name input').val();
    jQuery( jQuery(element).parents('.qualification-table-row')[0] ).find('.qualification-table-name input').attr('isedit','isedit');
    jQuery( jQuery(element).parents('.qualification-table-row')[0] ).find('.qualification-table-name span').text(val);
    programs.edtDialog.onQualificationEditUnClick();
}

/**
 * лод - главная - рограммы - карточка программы - редакт в поле квалификации
 *
 * @param {type} element
 * @param {type} event
 * @returns {undefined}
 */
programs.edtDialog.onQualificationEditKeyAction = function(element, event){
    if( 13 == event.keyCode ){
        programs.edtDialog.onQualificationEditOkAction(element);
    }
}

/**
 * лод - главная - рограммы - карточка программы - сохраняем данные
 *
 * @returns {undefined}
 */
programs.edtDialog.onSaveAction = function(){
    
    jQuery(this.dContent).find('.qualification-table-name input').each(function(ind, elem){
        if( !jQuery(elem).is('[isEdit]') ){
            jQuery(elem).val('');
        }
    });

    var data = jQuery(this.dContent).find('.dialog-programs-edit-form').serialize();
    programs.edtDialog.onQualificationEditUnClick();
    
    //посылаем на сервер
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: "/islod/index/get-programs-edit-save",
                    data: data,
                    processData: false,
                    beforeSend: function (xhr) {
                        if( jQuery(programs.edtDialog.dContent).find('.dialog-programs-edit-parent').is(':hidden') ){
                            useTopHoverTitle("Запрос в обработке");
                            return false;
                        };
                        jQuery(programs.edtDialog.dContent).find('.dialog-programs-edit-parent').hide();
                        jQuery(programs.edtDialog.dContent).append("<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading_islod.gif' /></div></div>");
                    },
                    success: function (answer) {
                        $(".loading-img", programs.edtDialog.dContent).fadeOut(500, function () {
                            console.log( 'сохранено , рез получен' );

                            if( answer.status =='Ok'){
                                var message = 'Сохранено';
                                if( 'undefined' != typeof(answer.message) && '' != answer.message ){
                                    message = answer.message;
                                }
                                if( 'string' == typeof(answer.newProgramId) && '' != answer.newProgramId){
                                    jQuery(programs.edtDialog.dContent).find('input[name="programEditDialog[id]"]').val(answer.newProgramId);
                                }
                                $('input[name="programEditDialog[Name]"]').css({'border':'1px solid #c2c2c2'});
                                $('input[name="programEditDialog[Code]"]').css({'border':'1px solid #c2c2c2'});
                                useTopHoverTitle(message);
                            }
                            else{
                                var message = 'ошибка при записи';
                                if( 'undefined' != typeof(answer.message) && '' != answer.message ){
                                    message = answer.message;
                                }
                                useTopHoverTitleError(message);
                                if( 'undefined' != typeof(answer.console) && '' != answer.console ){
                                    console.error('ошибка при сохранении');
                                    console.log(answer.console);
                                }
                            }
                            $(".loading-img", programs.edtDialog.dContent).remove();
                        });
                    },
                    error:function(e){
                        //сбросим сообщения
                        useTopHoverTitle("",1);
                        useTopHoverTitleError("Ошибка связи");
                        console.error( 'ошибка связи при формированиии');
                        console.log(e);
                        console.log(e.responseText);
                        //$(dContent).dialog("close");
                        $(".loading-img", programs.edtDialog.dContent).fadeOut(500, function () {
                           //jQuery(dContent).dialog('close');
                           $(".loading-img", programs.edtDialog.dContent).remove();
                        });
                    },
                    complete: function () {
                        jQuery(programs.edtDialog.dContent).find('.dialog-programs-edit-parent').show();
                    }
                });
}

/**
 * лод - главная - рограммы - карточка программы - диалог поиска квалификаци - рисуем диалог
 *
 * @returns {programs.searchDialog.openDialog}
 */
programs.searchDialog.openDialog = function(){
        this.dContent = $(
            '<div class="dialog-programs-search">' +
                "<div style='text-align: center;margin-top:60px'><div class='loading-img'><img src='/img/loading_islod.gif' /></div></div>" +
            '</div>');
        var dialog = {
            width : Math.round( (jQuery(window).width()/100)*87  ),
            height: Math.round( (jQuery(window).height()/100)*77 )
        };
        var dContent = this.dContent;
        $(this.dContent).dialog({
            title: 'Квалификации',
            width: dialog.width,
            height: dialog.height,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            modal: true,
            close: function () {
                $(this).remove();
            },
            open: function () {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/islod/index/get-programs-edit-search-dialog",
                    success: function (answer) {
                        $(".loading-img", dContent).fadeOut(500, function () {
                            $(dContent).html("").append(answer);
                        });
                    },
                    error:function(e){
                        //сбросим сообщения
                        useTopHoverTitle("",1);
                        useTopHoverTitleError("Ошибка связи");
                        console.error( 'ошибка связи при формированиии');
                        console.log(e);
                        console.log(e.responseText);
                        //$(dContent).dialog("close");
                        $(".loading-img", dContent).fadeOut(500, function () {
                           jQuery(dContent).dialog('close');
                        });
                    },
                    complete: function () {

                    }
                });

            },
            buttons: [
                {
                    text: 'Добавить',
                    click: function() {
                        programs.searchDialog.okAction();

                    }
                },
                {
                    text: 'Закрыть',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            ]
        });

}

/**
 * лод - главная - рограммы - карточка программы - диалог поиска квалификаци - делаем запрос на поиск квалификаций
 *
 * @returns {programs.searchDialog.findQustion}
 */
programs.searchDialog.findQustion = function(){
                var dContent = this.dContent;
                //console.log(jQuery(this.dContent).find(".dialog-programs-search-form").serialize());
                //console.log(jQuery(this.dContent).find(".dialog-programs-search-form").serializeArray());
                jQuery('.dialog-programs-search-find-table-more').remove();
                $.ajax({
                    //type: 'POST',
                    dataType: "html",
                    url: "/islod/index/get-programs-edit-search-find",
                    data: jQuery(this.dContent).find(".dialog-programs-search-form").serialize(),
                    processData: false,
                    before: function(){

                    },
                    success: function (answer) {
                        jQuery(dContent).find('.dialog-programs-search-find-table').append( jQuery(answer).find('.dialog-programs-search-find-table tr') );
                        jQuery(dContent).find('input[name="programs-search[skip]"]').val( jQuery(answer).find('input[name="programs-search[skip]"]').val() );
                    },
                    error:function(e){
                        //сбросим сообщения
                        useTopHoverTitle("",1);
                        useTopHoverTitleError("Ошибка связи");
                        console.error( 'ошибка связи при запросе');
                        console.log(e);
                        console.log(e.responseText);
                        //$(dContent).dialog("close");
                    },
                    complete: function () {

                    }
                });
}

/**
 * лод - главная - рограммы - карточка программы - диалог поиска квалификаци -ввод в строку поиска
 *
 * @param {type} event
 * @returns {undefined}
 */
programs.searchDialog.inputFindAction = function(event){
    console.log(event);
    if( 13 == event.keyCode ){
        jQuery(this.dContent).find('.dialog-programs-search-find-table').html('');
        jQuery(this.dContent).find('input[name="programs-search[skip]"]').val(0);
        this.findQustion();
    }
}

/**
 * лод - главная - рограммы - карточка программы - диалог поиска квалификаци -тык по добро
     *
 * @returns {undefined}
 */
programs.searchDialog.okAction = function(){
    console.log('добави квалификацию в перечень квалификаций');
    var selectetQualification = jQuery(programs.searchDialog.dContent).find('.dialog-programs-search-find-table input:checked');

    var added = function(){
        console.log('добавляем');
        console.log(selectetQualification);
        jQuery(selectetQualification).each(function(ind,el){
            var sampleRow = jQuery(programs.edtDialog.dContent).find('.qualification-new-sample .qualification-table-row').clone();
            var elVal = jQuery(el).val();
            //проверка на уже есть
            if( jQuery(programs.edtDialog.dContent).find('input[name="qualification['+elVal+']"]').length ){
                return 0;
            }
            var elLabel = jQuery(el).parent().parent().find('.dialog-programs-search-find-table-name').text();
            jQuery(sampleRow).find('input').attr('name','qualification['+elVal+']')
            jQuery(sampleRow).find('input').val(elLabel );
            jQuery(sampleRow).find('span').text(elLabel );
            jQuery(programs.edtDialog.dContent).find('table.qualification-table').append(sampleRow);
        });
        jQuery(programs.searchDialog.dContent).dialog('close');
    }

    if( selectetQualification.length ){
        added()
        //confirmLod('добавить квалиффикации',added );
    }
    else{
        useTopHoverTitleError('Не выбрана квалификация');
    }

    //последняя строка
}


/**
 * лод - главная - рпограммы - рисуем диалог новой квалиффикации
 * 
 * @returns {programs.newQualification.openDialog}
 */
programs.newQualification.openDialog = function(){
        this.dContent = $(
            '<div class="dialog-qualification-edit">'
                +'<div class="dialog-qualification-edit-parent">'
                    + '<form>'
                        + '<table width="100%">'
                            + '<tr>'
                                + '<td style="width: 120px;">'
                                    +'Квалификация'
                                + '</td>'
                                + '<td>'
                                    + '<input name="qualification[id]" type="hidden">'
                                    + '<textarea name="qualification[name]" rows="4" onkeypress="programs.newQualification.onNameKeyPress(event);" style="width: 100%;padding: 0 5px;">'
                                    + '</textarea>'
                                + '</td>'
                            + '</tr>'
                        + '</table>'
                    + '</form>'
                +'</div">'
            + '</div>'
        );
        var dialog = {
            width : Math.round( (jQuery(window).width()/100)*87  ),
            height: Math.round( (jQuery(window).height()/100)*77 )
        };
        var dContent = this.dContent;
        $(this.dContent).dialog({
            title: 'Добавить квалификацию',
            width: 600,
            //height: dialog.height,
            position: {
                my: "center center",
                at: "center center",
                of: window
            },
            modal: true,
            close: function () {
                $(this).remove();
            },
            open: function () {
            },
            buttons: [
                {
                    text: 'Сохранить',
                    click: function() {
                        programs.newQualification.onSaveAction();
                    }
                },
                {
                    text: 'Закрыть',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            ]
        });
}

/**
 * лод - главная - рпограммы - новые квалификации - отправляем данные на сервер
 * 
 * @returns {Number}
 */
programs.newQualification.onSaveAction = function(){
    if( '' == jQuery(this.dContent).find('textarea').val() ){
        useTopHoverTitleError("Данное поле не заполнено");
        return 0;
    }
    var data = jQuery(this.dContent).find('form').serialize();
    
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "/islod/index/get-programs-edit-save-qualification",
        data: data,
        processData: false,
        beforeSend: function (xhr) {
            if( jQuery(programs.newQualification.dContent).find('.dialog-qualification-edit-parent').is(':hidden') ){
                useTopHoverTitle("Запрос в обработке");
                return false;
            };
            //$(".dialog-qualification-edit-parent", programs.newQualification.dContent).fadeOut(500, function () {
                jQuery(programs.newQualification.dContent).find('.dialog-qualification-edit-parent').hide();
                jQuery(programs.newQualification.dContent).append("<div style='text-align: center;'><div class='loading-img'><img src='/img/loading_islod.gif' /></div></div>");
            //});
        },
        success: function (answer) {
            //$(".loading-img", programs.newQualification.dContent).fadeOut(500, function () {
                console.log( 'сохранено , рез получен' );
                if( answer.status =='Ok'){
                    var message = 'Сохранено';
                    if( 'undefined' != typeof(answer.message) && '' != answer.message ){
                        message = answer.message;
                    }
                    if( 'string' == typeof(answer.newId) && '' != answer.newId){
                        jQuery(programs.newQualification.dContent).find('input[name="qualification[id]"]').val(answer.newId);
                    }
                    useTopHoverTitle(message);
                    jQuery(programs.newQualification.dContent).dialog('close');
                }
                else{
                    var message = 'ошибка при записи';
                    if( 'undefined' != typeof(answer.message) && '' != answer.message ){
                        message = answer.message;
                    }
                    useTopHoverTitleError(message);
                    if( 'undefined' != typeof(answer.console) && '' != answer.console ){
                        console.error('ошибка при сохранении');
                        console.log(answer.console);
                    }
                }
            //});
        },
        error:function(e){
            //сбросим сообщения
            useTopHoverTitle("",1);
            useTopHoverTitleError("Ошибка связи");
            console.error( 'ошибка связи при формированиии');
            console.log(e);
            console.log(e.responseText);
            //$(dContent).dialog("close");
        },
        complete: function () {
            $(".loading-img", programs.newQualification.dContent).fadeOut(500, function () {
                jQuery(programs.newQualification.dContent).find('.dialog-qualification-edit-parent').show();
                jQuery(programs.newQualification.dContent).find('.loading-img').parent().remove();
            });
        }
    });
}

/**
 * лод - главная - рпограммы - новые квалификации - тыки по полю вводу
 * 
 * @param {type} event
 * @returns {undefined}
 */
programs.newQualification.onNameKeyPress = function(event){
    if( 13 == event.keyCode ){
        programs.newQualification.onSaveAction();
    }    
}


