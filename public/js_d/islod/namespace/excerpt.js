var excerpt = excerpt || {};

excerpt.getLicenses = function( page, search, sign){
    var animation = _getAnimation().dContent();
    $('.excerpt-content .excerpt-table tbody').html(animation).after(function(){
        var data = _getPage().licenseExcerpt( page, search, sign);
        animation.fadeOut("slow", function() {
           var
               tr = $(data).find('.excerpt-table tbody').html(),
               pages = $(data).find('.pagination').html(),
               count  = $(data).find('.excert-count').html();

           $('.excerpt-content .excerpt-table tbody').html(tr);
           $('.excerpt-list-data .pagination').html(pages);
           $('.excerpt-list-data .excert-count').html(count);
       });
    });
};
