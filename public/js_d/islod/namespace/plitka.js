/**   
 * лод = главная - на подписание обработка плиток
 * path: /trunk/public/js/islod/namespace/plitka.js
 * url:  /js/islod/namespace/plitka.js
 * 
 * @param {type} param
 */
    // украдено /trunk/public/js/islod/binds.js
    //function licenseInfoBinder() ... this.bind = function()
    console.log('/trunk/public/js/islod/namespace/plitka.js');
    $('body').undelegate('.plitka-butons .buttonReestr ', 'click');
    $('body').delegate('.plitka-butons .buttonReestr ', 'click', function () {
        console.log('плитки - отравка в реестр');
        var dialog = $(this).parent('.ui-dialog'),
            title = $(dialog).find('.ui-dialog-title'),
            licId = $(this).attr("data-id"),
            that = $(this),
            //добавлено и в license.js   license.Common_SignedFileOk
            afterCloseCallbck = function(){
                //тут изменяем
                        $('body').off('click','.saveButLicense');
                        $(dialog).find('.ui-icon-closethick').click();
                //        $('.btApply').click();
                // перегрузим перечень на подписание
                jQuery('.btn.btn-warning').click();
                        //license._get_license_info( licId, title);
                        
                jQuery('.dialog-reestr-change').dialog('close');
            },
                    
            defMessage = $(this).attr("data-message");

        /*
        switch( $(this).attr("data-next-status") ){
            case 'SEND_BY_MAIL':
               defMessage = {
                        title : $(this).attr("data-message"),
                        message : $(this).attr("data-message")+'<table class="fields-table"><tr><td>Дата отправки по почте</td><td><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                        handler : function(dialog,function_data){
                            $('input[name="date-attached-to-action"]').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd.mm.yy"
                            });
                            function_data['beforeSaveCallBck'] = function(){
                                _getData().changeLicAndSuppDate({
                                    id : $(that).attr("data-id"),
                                    type : $(that).attr("data-element-type"),
                                    input : 'DateSending',
                                    date : $('input[name="date-attached-to-action"]').val()
                                });
                            };
                        }
                    };
            break;
            case 'RECEIVED_BY_APPLICANT':
                defMessage = {
                        title : $(this).attr("data-message"),
                        message : $(this).attr("data-message")+'<table class="fields-table"><tr><td>Дата вручения</td><td><div class="relative_block"><div class="relative_block"><input placeholder="-" type="text" name="date-attached-to-action" /><span class="date-icon"></span></div></td></tr></table>',
                        handler : function(dialog,function_data){
                            $('input[name="date-attached-to-action"]').datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd.mm.yy"
                            });
                            function_data['beforeSaveCallBck'] = function(){
                                _getData().changeLicAndSuppDate({
                                    id : $(that).attr("data-id"),
                                    type : $(that).attr("data-element-type"),
                                    input : 'DateOnHands',
                                    date : $('input[name="date-attached-to-action"]').val()
                                });
                            };
                        }
                    };
            break;
        }
        */

        //если на кнопку прикручен свой обработчик,
        //ну так выкиним родную обработку
        //нужно чтоб завести отдельные окоши на некоторые кнопки
        var hasOnClick = jQuery(this).is('[onclick]');
        if (!hasOnClick){
            affairs.changeStatusReestrDialog(
                    $(this).attr("data-id"),
                    $(this).attr("data-element-type"),
                    $(this).attr("data-type-code"),
                    $(this).attr("data-next-status"),
                    defMessage,
                    $(this).attr("data-comment"),
                    afterCloseCallbck
                );
        }
    });


    /**
     * ичистка управляющих стилей на плитке
     * 
     * @param {type} element
     * @returns {undefined}
     */
    function lisenseAndSupplementResetStylePlitka(element){
        licenseResetStylePlitka(element);
        supplementResetStylePlitka(element);
    }
    

    /**
     * сбрасует стили элементов подписи у всех и прописывает только у данной плитки
     *
     * @param {type} element
     * @returns {undefined}
     */
    function licenseResetStylePlitka(element){        
        jQuery('.license-maket-link').removeClass('license-maket-link');
        jQuery('.license-document-link').removeClass('license-document-link');
        jQuery('.license-sign-link').removeClass('license-sign-link');
        jQuery('.maket-sign-link').removeClass('maket-sign-link');
        jQuery('.license-maket-link').removeClass('license-maket-link');
        var parent = jQuery(element).parents('.plitka-border:first');
        jQuery(parent).find('.plitka-license-maket-link').addClass('license-maket-link');
        jQuery(parent).find('.plitka-license-document-link').addClass('license-document-link');
        jQuery(parent).find('.plitka-license-sign-link').addClass('license-sign-link');
        jQuery(parent).find('.plitka-maket-sign-link').addClass('maket-sign-link');
        jQuery(parent).find('.plitka-license-maket-link').addClass('license-maket-link');
    }

    /**
     * открывает карточку лицензии
     * 
     * @param {type} element
     * @param {type} licenseId
     * @returns {undefined}
     */
    function licenseOpenDialogPlitka(element, licenseId){
        console.log('плитка -  хотим открыть лицензияю');
        lisenseAndSupplementResetStylePlitka(element);
        license._get_license_info(licenseId);
    }

    /**
     * открывает диалог подписания лаценнзии
     * 
     * @param {type} element
     * @param {type} fileId
     * @param {type} options
     * @returns {undefined}
     */
    function licenseSignDialogActionPlitka(element, fileId, options){
        console.log('плитка -  хотим подписать чего-то ');
        lisenseAndSupplementResetStylePlitka(element);
        licenseSignDialogAction(fileId, options);
    }

    //обработчик удачного подписания лицензии
    Common_SignedFileOk = license.Common_SignedFileOk;

    /**
     * переформировать макет лицензии
     * 
     * @param {type} element
     * @param {type} licenseId
     * @param {type} licenseType
     * @returns {undefined}
     */
    function  licenseMaketReMakeActionPlitka(element, licenseId, licenseType){
        console.log('хотим переформировать лицензию');
        lisenseAndSupplementResetStylePlitka(element)
        licenseMaketReMakeAction(licenseId, licenseType);

    }


    /**
     * озагрузить макет лицензии
     * 
     * @param {type} element
     * @param {type} licenseId
     * @returns {undefined}
     */
    function licenseSendMaketMaketInputPlitka(element, licenseId ){
        console.log('плитка -  хотим подписать лицензию');
        lisenseAndSupplementResetStylePlitka(element)
        licenseSendMaketMaketInput(licenseId);
    }


    /**
     * сбрасует стили элементов подписи у всех и прописывает только у данной плитки
     * приложения
     *
     * @param {type} element
     * @returns {undefined}
     */
    function supplementResetStylePlitka(element){
        jQuery('.supplement-sign-maket-link').removeClass('supplement-sign-maket-link');
        jQuery('.supplement-sign-remake').removeClass('supplement-sign-remake');
        jQuery('.supplement-maket-send').removeClass('supplement-maket-send');
        jQuery('.maket-supplement-sign-link').removeClass('maket-supplement-sign-link');
        jQuery('.supplement-document-link').removeClass('supplement-document-link');
        jQuery('.supplement-sign-link').removeClass('supplement-sign-link');
        
        jQuery('.supplement-document-link').removeClass('supplement-document-link');
        var parent = jQuery(element).parents('.plitka-border:first');
        jQuery(parent).find('.plitka-supplement-sign-maket-link').addClass('supplement-sign-maket-link');
        jQuery(parent).find('.plitka-supplement-sign-remake').addClass('supplement-sign-remake');
        jQuery(parent).find('.plitka-supplement-maket-send').addClass('supplement-maket-send');
        jQuery(parent).find('.plitka-maket-supplement-sign-link').addClass('maket-supplement-sign-link');
        jQuery(parent).find('.plitka-supplement-document-link').addClass('supplement-document-link');
        jQuery(parent).find('.plitka-supplement-sign-link').addClass('supplement-sign-link');
        
        jQuery(parent).find('.plitka-lsupplement-document-link').addClass('supplement-document-link');
    }

    /**
     * открыть диалог приложения
     *
     * @param {type} element
     * @param {type} suplementId
     * @param {type} code
     * @returns {undefined}
     */
    function supplementOpenDialogPlitka(element, suplementId, code ){
        console.log('плитка -  хотим открыть приложение');
        lisenseAndSupplementResetStylePlitka(element);
        licenseSupplement.createSupplementInfoDialog(suplementId, code);
    }
    
    /**
     * переформировать макет приложжения
     * @param {type} element
     * @param {type} supplementId
     * @param {type} code
     * @returns {undefined}
     */
    function supplementMaketReMakeActionPlitka(element, supplementId, code){
        console.log('плитка -  хотим переформировать макет приложения');
        lisenseAndSupplementResetStylePlitka(element);
        supplementMaketReMakeAction(supplementId, code);
    }
    
    /**
     * загрузить макет приложения
     * 
     * @param {type} element
     * @param {type} supplementId
     * @param {type} code
     * @returns {undefined}
     */
    function supplementSendMaketInputPlitka(element, supplementId, code){
        console.log('плитка -  хотим загрузки макет приложения');
        lisenseAndSupplementResetStylePlitka(element);
        supplementSendMaketInput( supplementId, code);
    }
    
