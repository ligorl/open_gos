/**
 * Подтягиваем данные с платёжки.
 * Не срабатывает при создании платёжки.
 *
 * @param id
 */
function getPaymentNotification(id) {
    $('#input-sum').show();
    $('#tr-uin').hide();
    $('#tr-sum').hide();
    if(!id){
        return false;
    }

    $.ajax({
        url: '/islod/requests/get-payment-notification/' + id,
        success: function (answer) {
            if(answer.status == 'ok'){
                $('#field-sum').text(answer.sum);
                $('#field-uin').text(answer.uin);
                $('#tr-uin').show();
                $('#tr-sum').show();
                $('#input-sum').hide();

                return true;
            }
        }
    });
}
