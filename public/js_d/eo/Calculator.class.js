var Calculator = function(){
    var _calculator = this;
    var _private = {
        /**=====================================================
         * Приватные данные
         *====================================================*/

        // блоки с которыми работает калькулятор
        blocks: {
            main: null,
            mid: null,
            rows: null
        },
        data: {
            mode: 0
        },
        count: {
        },

        /**=====================================================
         * Приватные методы
         *====================================================*/

        /**
         * Метод инициирует данные для работы
         */
        initData: function(){
            _private.blocks.main = $(".viewzone.calculator");
            _private.blocks.mid = $(".mid-row", _private.blocks.main);
            _private.blocks.rows = $(".calculator-rows", _private.blocks.main);
            /*

            _private.blocks.options = $(".calculator-options",_private.blocks.main);
            _private.blocks.optionsRow = $("tr",_private.blocks.options).eq(0).clone();
            _private.blocks.infoFull = $(".calculator-info",_private.blocks.main);
            _private.blocks.info = $(".info-content",_private.blocks.main);
            _private.blocks.sum = $(".sumField .sum",".buttons-container");
            _private.blocks.infoText = $(".infoText",_private.blocks.main);
            _private.blocks.base = $(".baseSelectRow", _private.blocks.main);
            //собираем данные для активного типа обр программ
            var eduProgramTypeId = $(".selectEduProgramTypes",_private.blocks.optionsRow).val();
            _private.data.EduProgramTypes[eduProgramTypeId] = [];

            $(".selectEduPrograms",_private.blocks.optionsRow).find("option").each(function(i,item){
                var eduProgram = {
                    Id: $(item).attr("value"),
                    Name: $(item).attr("data-Name"),
                    Code: $(item).attr("data-Code"),
                    ParentEGSId: $(item).attr("data-ParentEGSId")
                }
                _private.data.EduProgramTypes[eduProgramTypeId].push(eduProgram);
            });
            */
        },

        /**
         * Метод отвечает за установку обработчиков
         */
        initHandlers: function(){
            $( ".spinner", _private.blocks.main).spinner({
                min: 0,
                numberFormat: "n"
            });
            $( ".spinner", _private.blocks.main).spinner( "disable" );

            //Обработчик проставления галочек
            $(".cb-type", _private.blocks.main).change(function(){
                if($(this).is(":checked")){
                    $(this).parents("tr").eq(0).find(".spinner").spinner("enable");
                } else {
                    $(this).parents("tr").eq(0).find(".spinner").spinner("disable");
                }
            });

            //Рассчитать
            $(".btCount", _private.blocks.mid).click(function(){
                //убираем кнопку печать
                $('.btPaymentPrint').css('display', 'none');
                $('.btPaymentPrint').find('a').removeAttr('href');
                var sum = 0;
                if($("#cb-midNum",_private.blocks.main).is(":checked")){
                    sum += $("#midNum", _private.blocks.main).val() * 35000;
                }
                if($("#cb-highNum",_private.blocks.main).is(":checked")){
                    sum += $("#highNum", _private.blocks.main).val() * 100000;
                }
                if($("#cb-lowNum",_private.blocks.main).is(":checked") && $("#lowNum", _private.blocks.main).val() != "0"){
                    sum += 15000;
                }
                sum += "";
                $(".sum",_private.blocks.mid).text(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
            });

            //Очистить
            $(".btClear", _private.blocks.mid).click(function(){
                $(".cb-type", _private.blocks.main).prop("checked",false);
                $(".spinner" ,_private.blocks.main).val("0").spinner("disable");
                $(".sum",_private.blocks.mid).text("0");
            });

            //Сохранить
            $(".btSave", _private.blocks.mid).click(function(){
                //console.log(_private.data.mode);
                var appTypeId = $(".appType",_private.blocks.main).val(),
                    actionId = $('#reason_711776cf-7871-4c90-9ed7-9669c0e0356f').val(),
                    actionIdFromCalcArr = [];

                    

                    $('.calculator-rows tr[actionId]').each(
                        function(i,el){
                            var len = actionIdFromCalcArr.length;
                            actionIdFromCalcArr[ len ] = $(el).attr('actionId');
                    });
                    //console.log(actionIdFromCalcArr);

                switch(_private.data.mode){
                    case 0:
                        $(".btCount", _private.blocks.mid).click();

                        if(
                            $(".sum",_private.blocks.mid).text() == "0" ||
                            _private.checkToConcurrence('67f97827-57f9-4a0c-b2c7-d9806969bef6',actionIdFromCalcArr)
                        ){
                             return;
                        }

                        _private.blocks.rows.append(
                            $("<tr actionId='"+actionId+"'>")
                                .append($("<td>"))
                                .append($("<td>").text($("#shortName :selected", _private.blocks.main).text()))
                                .append($("<td>").text($(".sum",_private.blocks.mid).text()))
                                .append($("<td>")
                                    .css("width","30px")
                                    .append($("<span>").addClass("remove").addClass("ui-icon-closethick")))
                        );
                        if(
                            appTypeId != "711776cf-7871-4c90-9ed7-9669c0e0356f"
                        ){
                             $(".btClear", _private.blocks.mid).click();
                        }
                        break;
                    case 1:

                        if( 
                                (   $(".mode1",_private.blocks.rows).length == 0 ||
                                    appTypeId == "711776cf-7871-4c90-9ed7-9669c0e0356f"
                                ) && !_private.checkToConcurrence( actionId, actionIdFromCalcArr)
                        ){
                             _private.blocks.rows.append(
                                $("<tr class='mode1' actionId='"+actionId+"'>")
                                    .append($("<td>"))
                                    .append($("<td>").text($(".appType option:selected",_private.blocks.main).text()+" ("+$(".appReasons:visible option:selected", _private.blocks.main).text() + ")"))
                                    .append($("<td>").text($(".sum",_private.blocks.mid).text()))
                                    .append($("<td>")
                                        .css("width","30px")
                                        .append($("<span>").addClass("remove").addClass("ui-icon-closethick")))
                            );
                        }
                        break;
                    case 2:
                        if($(".mode2",_private.blocks.rows).length == 0)
                            _private.blocks.rows.append(
                                $("<tr class='mode2'>")
                                    .append($("<td>"))
                                    .append($("<td>").text($(".appType option:selected",_private.blocks.main).text()+" ("+$(".appReasons:visible option:selected", _private.blocks.main).text() + ")"))
                                    .append($("<td>").text($(".sum",_private.blocks.mid).text()))
                                    .append($("<td>")
                                        .css("width","30px")
                                        .append($("<span>").addClass("remove").addClass("ui-icon-closethick")))
                            );
                        break;
                }
                _private.refreshNumbers();
                _private.refeshFullSum();
            });

            // Кнопка удаляющая ряд
            _private.blocks.rows.delegate(".remove","click", function(){
                $(this).parents("tr").eq(0).remove();
                _private.refreshNumbers();
                _private.refeshFullSum();
            });

            //Очистить все
            $(".btClearAll",".buttons-container").click(function(){
                _private.clearAll();
            });

            //Подгрузка из заявлений
            $(".btFromApplication", ".buttons-container").click(function(){
                _private.dialog_LoadFromApplications();
            });

            //Обработчик смены селекта с типами заявок
            $(".appType", _private.blocks.main).change(function(){
                var typeId = $(this).val();
                $(".reasons-block .appReasons", _private.blocks.main).hide();
                $(".reasons-block .appReasons[id=reason_"+typeId+"]", _private.blocks.main).show();
                //
                _private.clearAll();
                //Проверяем значения
                _private._checkSelectValues();
            });

            //Обработчик смены селекта с основаниями заявок
            $(".appReasons", _private.blocks.main).change(function(){
                //Проверяем значения
                _private._checkSelectValues();
            });
        },

        /**
         * Метод проверяет текущие значения селектов и обрабатывает что нужно сделать
         * @private
         */
        _checkSelectValues : function(){
            var appTypeId = $(".appType",_private.blocks.main).val();
            var appReasonId = $(".appReasons:visible", _private.blocks.main).val();
            var actionId = $('#reason_711776cf-7871-4c90-9ed7-9669c0e0356f').val();
            var actionIdFromCalcArr = [];

            $('.calculator-rows tr[actionId]').each(
                function(i,el){
                    var len = actionIdFromCalcArr.length;
                    actionIdFromCalcArr[ len ] = $(el).attr('actionId');
            });

            if(
                    appTypeId != "711776cf-7871-4c90-9ed7-9669c0e0356f" ||
                    actionId == '67f97827-57f9-4a0c-b2c7-d9806969bef6' ||
                    _private.checkToConcurrence('67f97827-57f9-4a0c-b2c7-d9806969bef6',actionIdFromCalcArr)
            ){
                _private.clearAll();
            }

            //Если выбран тип заявления «Заявление о государственной аккредитации» и «Заявление о
            //переоформлении свидетельства» по выбранному основанию «государственная аккредитация
            //в отношении ранее не аккредитованных образовательных программ»
            if(
                appTypeId == "b30d724a-17d1-4792-8c75-8f9db2a6ce57" ||
                (appTypeId == "711776cf-7871-4c90-9ed7-9669c0e0356f" && appReasonId == "67f97827-57f9-4a0c-b2c7-d9806969bef6")
            ) {
                _private.data.mode = 0;
            } else
            // «Заявление о выдаче временного свидетельства» и «Заявление о переоформлении свидетельства»
            // по остальным основаниям кроме вышеуказанного
            if(
                appTypeId == "550b5c08-99e0-4449-a242-b448e9b1b422" ||
                (appTypeId == "711776cf-7871-4c90-9ed7-9669c0e0356f" && appReasonId != "67f97827-57f9-4a0c-b2c7-d9806969bef6")) {
                _private.data.mode = 1;
            //«Заявление о предоставлении дубликата свидетельства
            } else if(appTypeId == "41152d9a-3e6a-4c6b-9a6b-53e246eb8638") {
                _private.data.mode = 2;
            }
            
            $(".sum",_private.blocks.mid).text("0");
            switch(_private.data.mode){
                case 0:
                    $(".hide-block",_private.blocks.main).show();
                    var sum = "0";
                    $(".sum",_private.blocks.mid).text(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
                    break;
                case 1:
                    $(".hide-block",_private.blocks.main).hide();
                    var sum = "3000";
                    $(".sum",_private.blocks.mid).text(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
                    break;
                case 2:
                    $(".hide-block",_private.blocks.main).hide();
                    var sum = "350";
                    $(".sum",_private.blocks.mid).text(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
                    break;
            }
        },

        /**
         * Метод заново проставляет номера в таблице
         */
        refreshNumbers : function(){
            var num = 1;
            _private.blocks.rows.find("tr").each(function(i,item){
                $(item).find("td").eq(0).text(num++);
            });
        },

        /**
         * Метод пересчитывает итоговую сумму
         */
        refeshFullSum : function(){
            //Обработчик для варианта переоформления
            var appTypeId = $(".appType",_private.blocks.main).val();
            var appReasonId = $(".appReasons:visible", _private.blocks.main).val();
            if(appTypeId == "711776cf-7871-4c90-9ed7-9669c0e0356f" && appReasonId != "67f97827-57f9-4a0c-b2c7-d9806969bef6"){
                var first = true;
                _private.blocks.rows.find("tr").each(function(i,item){
                    if(first){
                        $(item).find("td").eq(2).text("3 000");
                        first = false;
                    } else {
                        $(item).find("td").eq(2).text("-");
                    }
                });
            }


            var sum = 0;
            _private.blocks.rows.find("tr").each(function(i,item){
                sum += parseInt($(item).find("td").eq(2).text().replace(" ","").replace("-","0"));
            });
            sum += "";
            $(".sum",".buttons-container").html(sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        },

        /**
         * Метод очищает все
         */
        clearAll : function(){
            $(".btClear", _private.blocks.mid).click();
            _private.blocks.rows.html("");
            $(".sum",".buttons-container").text("0");
            //убираем кнопку печать
            $('.btPaymentPrint').css('display', 'none');
            $('.btPaymentPrint').find('a').removeAttr('href');

        },

        /**
         * Метод заполняет таблицу данными
         */
        drawTable : function(data){
            _private.clearAll();
            //Меняем сеоекты с типом и основанием
            $(".appType",_private.blocks.main).val(data.type).change();
            $(".appReasons:visible", _private.blocks.main).val(data.reason).change();
            //
            for(var i = 0; i < data.tableData.length; i++){
                var rowData = data.tableData[i];
                var tr = $("<tr>")
                    .append($("<td>"))
                    .append($("<td>").text(rowData["ShortName"]))
                    .append($("<td>").text(rowData["Sum"]))
                    .append($("<td>")
                        .css("width","30px")
                        .append($("<span>").addClass("remove").addClass("ui-icon-closethick")));
                if(_private.data.mode == 1 || _private.data.mode == 2)
                    tr.addClass("mode"+_private.data.mode);
                _private.blocks.rows.append(tr);
            }
            if(_private.data.mode == 1 || _private.data.mode == 2)
                $(".sum",_private.blocks.mid).text(0);

            _private.refreshNumbers();
            _private.refeshFullSum();
            $(".dialog, .dialog-progress").remove();
        },

        /**
         * Метод отображает диалог выбора из заявок
         */
        dialog_LoadFromApplications : function(){
            $('body').append('<div class="ui-widget-overlay ui-front" style="z-index: 101;"></div>');
            var dContent = $('<div class="dialog-applications">'+
                    '<div class="info-text"> Выберите заявление </div>'+
                    '<div class="applications-list">' +
                        '<div class="windows8"><div class="wBall" id="wBall_1" style="width: 32px; height: 32px;"><div class="wInnerBall" style="width: 4.71428571428571px; height: 4.71428571428571px;"></div></div><div class="wBall" id="wBall_2" style="width: 32px; height: 32px;"><div class="wInnerBall" style="width: 4.71428571428571px; height: 4.71428571428571px;"></div></div><div class="wBall" id="wBall_3" style="width: 32px; height: 32px;"><div class="wInnerBall" style="width: 4.71428571428571px; height: 4.71428571428571px;"></div></div><div class="wBall" id="wBall_4" style="width: 32px; height: 32px;"><div class="wInnerBall" style="width: 4.71428571428571px; height: 4.71428571428571px;"></div></div>   <div class="wBall" id="wBall_5" style="width: 32px; height: 32px;"><div class="wInnerBall" style="width: 4.71428571428571px; height: 4.71428571428571px;"></div></div></div>'+
                    '</div>'+
                    '<div class="buttons">'+
                        '<div class="bt btChoose" style="margin-right: 5px!important;">Выбрать</div>'+
                        '<div class="bt btCancel">Отмена</div>'+
                    '</div>'+
                '</div>');
            $(dContent).dialog({
                title: "Список заявлений",
                width: 600,
                height: 300,
                close: function() {
                    $(".dialog, .dialog-applications").remove();
                    $(".ui-front").remove();
                },
                open: function() {

                    //Отсылаем запрос на получение списка заявок
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: "/eo/index/get-applications",
                        success: function(answer) {                          
                            $(".applications-list",dContent).html("");
                            if(answer.length == 0){
                                $(".applications-list",dContent).append(
                                    $("<div>").text("Нет заявлений")
                                );
                            } else
                                for(var i = 0; i < answer.length; i++){
                                    var app = answer[i];
                                    $(".applications-list",dContent).append(
                                        $("<div>").addClass("application").attr("id","app_"+app.Id).text(app.DateCreate + " " + app.TypeName)
                                    );
                                }
                        },
                        error:function(e){
                            console.log('ошибка при запросе перечня заявлений');
                            console.log(e);
                        }
                    });

                    //Обработчик клика на заявке
                    $(".applications-list",dContent).delegate(".application","click", function(){
                        $(".applications-list .application",dContent).removeClass("selected");
                        $(this).addClass("selected");
                    });

                    //Клик по кнопке Выбрать
                    $(".btChoose", dContent).click(function(){
                        //Проверяем если было выбрано заявление
                        if($(".applications-list .application.selected",dContent).length == 0){
                            useTopHoverTitleError("Не выбрано ни одного заявления");
                            return;
                        }
                        //==== Если заявление выбрано
                        //Запоминаем айди
                        var appId = $(".applications-list .application.selected",dContent).attr("id").split("_")[1];
                        //Закрываем диалог
                        $(".dialog, .dialog-applications").remove();
                        //Показываем сообщение об обработке, запрашиваем данные для построения
                        var content = $("<div class='dialog-progress'><div class='progress'></div></div>");
                        $(content).dialog({
                            width: 200,
                            title: "Обработка",
                            resizable: false,
                            draggable: false,
                            height: 140,
                            open: function(){
                                $(".progress",content).css("margin-top","20px").progressbar({
                                    value: false
                                });
                            }
                        });
                        //Отсылаем приватному методу данные для заполнения калькулятора
                        $.ajax({
                            type: 'POST',
                            dataType: "json",
                            url: "/eo/index/get-application-calculator-data/"+appId,
                            success: function (answer) {
                                //Распихиваем результат, в таблицу
                                $(".dialog, .dialog-progress").remove();
                                $(".ui-front").remove();

                                // $(".ui-front").remove();
                                if(answer.error==='true'){
                                    console.log(answer);
                                    console.log(answer.message);
                                    useTopHoverTitleError('Ошибка расчета стоимости'+'\n'+answer.message);
                                }
                                _private.drawTable(answer);
                                if (answer.tableData.length > 0) {
                                    //Светим кнопку печатать
                                    $('.btPaymentPrint').css('display', 'inline-block');
                                    $('.btPaymentPrint').find('a').attr('href', '/eo/index/payment-export/' + appId);
                                }
                            },

                            error:function(e){
                                $(".dialog, .dialog-progress").remove();
                                $(".ui-front").remove();
                                console.log('ошибка при получнии данных калькулятора');
                                console.log(e);
                            },
                            complete:function(){
                                $(".ui-front").remove();
                                $(".dialog, .dialog-progress").remove();
                            }

                        });
                    });

                    //Клик по кнопке отмена
                    $(".btCancel", dContent).click(function(){
                        $(".dialog, .dialog-applications").remove();
                        $(".ui-front").remove();
                    });
                }
            });
        },

        /**
         * проверяет встречаеться ли val в массив arr возвращает true если встречаеться и false если нет
         * @param {type} val
         * @param {type} arr
         * @returns {true||false}
         */
        checkToConcurrence: function (val,arr){
            if( val === undefined ){ console.log('Warning: val is undefined!')}

            if( typeof(arr) != 'object'){
                console.log('Error()Unexpext arr type('+typeof(arr)+') expect Array');
            }else{
                if( arr.length == 0){ return false;}
                for( var key in arr){
                    var elem = arr[key];

                    if( elem == val){
                        return true;
                    }
                }
            }
            return false;

        }
    }

    /**=====================================================
     * Публичные методы
     *====================================================*/

    /**
     * Метод инициализирует калькулятор, вешает обработчики и т.д.
     */
    this.init = function(){
        //Подготавливаем данныедля работы
        _private.initData();
        //Вешаем обработчики
        _private.initHandlers();
    }
}
var Calculator = new Calculator();


