/*
 * Окно редактирования/создания заявки
 */

$(document).ready(function(){
    getInfoEqueue();
});

function NewQueueClick(){
        $.ajax({
            url: '/eo/index/newQueue',
            type: 'POST',
            data: {type:'new'},
            success: function(answer){
                var popuoWin = $("#popup_win");
                popuoWin.html(answer);
                popuoWin.show();
            }
        });
};




$('.btReset').click(function () {
    $('#filter select').each(function () {
        $(this).val('');
    });
});


$('.btApply').click(function () {
    var search = {
        "queType": $('select[name="queType"]').val()  || '',
        "licenseReasonCatalogItem": $('select[name="licenseReasonCatalogItem"]').val()  || '',
        "typeTreatment": $('select[name="typeTreatment"]').val()  || '',
        "basisTreatment": $('select[name="basisTreatment"]').val()  || ''
    };

    $.ajax({
        url: '/eo/index/equeue',
        type: 'POST',
        data: search,
        success: function(answer) {
            answer = $(answer);
            $(".content_td .content .viewzone .content").html("").append(answer.find(".content").html());
            getInfoEqueue();
        }
    });

});

/**
 * Подробная информация по эксп. очереди
 */
function getInfoEqueue() {
    $('.equeue_row').dblclick(function () {
        if ($(this).attr('equeueid')) {
            var equeueId = $(this).attr('equeueid');
            $.ajax({
                type: 'POST',
                url: '/eo/index/info-equeue',
                data: {
                    equeueId: equeueId
                },
                success: function (answer) {
                    equeueInfo(answer);
                }
            });
        }
    });
}
/**
 * Формирование диал.окна для подробной инф. по эксп. очереди
 *
 * @param answer
 */
function equeueInfo(answer) {
    var dContent = $('<div class="equeue_info"></div>');

    $(dContent).dialog({
        title: 'Заявка в электронную очередь',
        width: 1000,
        height: 620,
        position: { my: "center center", at: "center center", of: window },
        modal: true,
        close: function(event, ui) {
            $(this).remove();
        },
        open: function() {
            $(dContent).append(answer.content);
        },
        buttons:[
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ]
    });
}