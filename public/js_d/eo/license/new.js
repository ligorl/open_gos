/**
 * лицензии - новые заявления
 * path: /public/js/eo/license/new.js
 * url:  /js/eo/license/new.js
 *
 * @param {type} param
 */
    console.log('лицензии - новые заявления - загружаем скрипт');


    function licenseProcedureChange(element){
        var licProcId = jQuery(element).val();
        if( 'undefined' == typeof(licProcId) || '' ==  licProcId ){
            jQuery('.license-reason option').show();
        }
        else {
            jQuery('.license-reason option').removeProp('selected');
            jQuery('.license-reason option').hide();
            jQuery('.license-reason option[licensingProcedureId="'+licProcId+'"]').show();
            jQuery('.license-reason option[value=""]').show();
        }

    }


    function licenseNewRequestClick(element){
        var dContent = $('<div class="request-new"></div>');
        console.log('заявления - добавления заявления - готовим')
        $(dContent).dialog({
                title: "Новое заявление",
                width: '95%',
                height: '530',
                //position: 'center',
                position: { my: "center center", at: "center center", of: window },
                modal: true,
                close: function(event, ui) {
                    console.log('заявления - добавления заявления - закрываем диалог');
                    $(this).remove();
                },
                open: function() {
                    licenseNewRequestLoad(dContent)

                },
                buttons:[
                    {
                        text: "Добавить",
                        "class": 'right ',
                        click: function () {
                            licRequest.OkAction(this);

                        }
                    },
                    {
                        text: "Отмена",
                        "class": 'right',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                ]
        });
    }

    function licenseNewRequestLoad(dContent){
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/eo/license/add",
                //data: data,
                beforeSend: function(){
                    $(dContent).append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
                },
                success: function (answer) {
                    console.log('ответ получен - выводим ответ');
                    $(".loading-img", dContent).fadeOut(500, function () {

                        jQuery(dContent).append(answer);
                    });
                },
                error:function(e){
                    //сбросим сообщения
                    useTopHoverTitle("",1);
                    useTopHoverTitleError("Ошибка связи");
                    $(".loading-img", dContent).fadeOut(500, function(){

                    });
                    console.error('ошибка связи при переходе по меню');
                    console.log(e);
                    console.log(e.responseText);
                }
            });
    }

    //если   это лицензирование новые то инициализируем иначе не надо
    if( jQuery('li.btn.btn-warning').is('[controller="license"][event="new"]') ){
        indexNewHandlers();
    }

    /**
     * тык по удалить заявление, откроем диалок удаления
     *
     * @returns {undefined}
     */
    function newRequestDeleteAction(){
        console.log('хотим удалить заявление');
        if($('table.intable.license_table.license-table-content tbody tr.select').length){
            var reqId = $('table.intable.license_table.license-table-content tbody tr.select').attr('requetsId');
            if ( '' == reqId){
                useTopHoverTitleError('Выберите заявление');
            }
            else{

                var dContent = $(
                    '<div >' +
                    '</div>');
                $(dContent).dialog({
                    title: "Удалить заявление",
					width: 500,
                    modal: true,
                    open: function () {
                        jQuery(this).append('<p>Выбранное заявление будет удалено.</p>');
                        jQuery(this).append('<p>Для продолжения – нажмите «Да», для отмены действия и возврата в предыдущее меню – нажмите «Нет»</p>');
                    },
                    close: function () {
                        jQuery(this).remove();
                    },
                    buttons: [
                        {
                            text: 'Да',
                            class: '',
                            click: function() {
                                newRequestDeleteSend(reqId,this)
                            }
                        },
                        {
                            text: 'Нет',
                            class: '',
                            click: function() {
                                jQuery(this).dialog('close');
                            }
                        }
                    ]

                });
            }
        }
        else {
            useTopHoverTitleError('Выберите заявление');
        }
    }

    /**
     * запрос на удалени заявления
     *
     * @param {type} reqId      - ид заявление которое удалить
     * @param {type} dialog     - диалог от которого пришел запрос
     * @returns {Number}
     */
    function newRequestDeleteSend(reqId,dialog){
        console.log('удаление заявлений - хотим послать');
            if( undefined == typeof (reqId) || '' == reqId ){
                console.log('удаление заявлений - нет ид');
                useTopHoverTitleError('не выбрано заявление');
                return 0;
            }
            $.ajax({
                type: 'DELETE',
                dataType: "json",
                url: "/eo/license/delete/"+reqId,
                //data: data,
                beforeSend: function(){
                    $(dialog).append($("<div class='loading-img-full'><img src='/img/loading.gif' /></div>")).fadeIn(500);
                },
                success: function (answer) {
                    if( 'undefined' == typeof(answer.console) ){
                        answer.console = '';
                    }
                    if( 'undefined' == typeof(answer.message) ){
                        answer.message = '';
                    }
                    console.log('удаление заявления - ответ получен - отработаем ответ');
                    $(".loading-img-full", dialog).fadeOut(500, function () {
                        if( 'undefined' == typeof(answer.result) || 'Ok' != (answer.result) ){
                            if( '' == answer.message ){
                                useTopHoverTitleError("Ошибка при удалении");
                            }
                            else{
                                useTopHoverTitleError(answer.message);
                            }
                            if( '' == answer.console ){

                            }
                            else{
                                console.error(answer.console);
                            }
                        }
                        else{
                            if( '' == answer.message ){
                                useTopHoverTitle("Заявление удалено");
                            }
                            else{
                                useTopHoverTitle(answer.message);
                            }
                            // закроем диалок
                            jQuery(dialog).dialog('close');
                            //перегрузить пагинацию
                            var itPage = jQuery('#pagination .current-page').text();
                            _serverQuery(itPage);
                        }

                        //jQuery(dContent).append(answer);
                    });
                },
                error:function(e){
                    //сбросим сообщения
                    useTopHoverTitle("",1);
                    useTopHoverTitleError("Ошибка связи");
                    $(".loading-img-full", dialog).fadeOut(500, function(){

                    });
                    console.error('ошибка связи при переходе при удалении');
                    console.log(e);
                    console.log(e.responseText);
                }
            });
    }


    /**
     * тык по табличке с зачявлениями
     *
     * @param {type} answer
     * @returns {undefined}
     */
    function newRequestClickAction(answer){
        console.log('добавим обработчик тыка по табличке');
        $('table.intable.license_table.license-table-content tbody tr ').undelegate('td','click');
        $('table.intable.license_table.license-table-content tbody tr ').delegate('td','click', function(){
            console.log('был тыка по табличке');
            $('table.intable.license_table.license-table-content tbody tr').removeClass('select');
            var tr = jQuery(this).parents('tr')[0];
            jQuery(tr).addClass('select');
        });
        $('table.intable.license_table.license-table-content tbody tr ').undelegate('td','dblclick');
        $('table.intable.license_table.license-table-content tbody tr ').delegate('td','dblclick', function(){
            console.log('был  тык-тык тыка по табличке');
            $('table.intable.license_table.license-table-content tbody tr').removeClass('select');
            var tr = jQuery(this).parents('tr')[0];
            jQuery(tr).addClass('select');
            newRequestOpenAction(this);
        });
    }


    /**
     * лицензирование - карточка заявления - отправить - 
     * @returns {undefined}
     */
    function newRequestSendAction(requestId){
        console.log('хотим отправить заявление в рон');
        if( 'undefined' == typeof(requestId) || '' == requestId){
            useTopHoverTitleError('Не выбрано заявление');
            return 0;
        }
                var dContent = $(
                    '<div class="request-send-to-ron-dialog">' +
                    '</div>');
                $(dContent).dialog({
                    title: "Отправка заявления",
                    width: 750,
                    position: { my: "center center", at: "center center", of: window },
                    modal: true,
                    open: function () {
                        console.log('диалог заявления - диалог отправки на сервер рон');
                        var requestFileDiv = jQuery('img.request-sign-send').parent();
                        var link = jQuery(requestFileDiv).find('.request-sign-link').clone();
                        var ref = jQuery(requestFileDiv).find('.request-sign-send').clone();
                        var check = jQuery(requestFileDiv).find('.request-sign-check').clone();
                        
                        var scanSignLink = jQuery('.request-scan-link').clone();
                        
                        //проверка на новое заявление или исправление
                        var isReRequest = false;
                        if( jQuery( '.requestData .helperField[name="isReRequest"]' ).length ){
                            if( jQuery( '.requestData .helperField[name="isReRequest"]' ).val() ){
                                isReRequest = true;
                            }
                        }
                        if( isReRequest ){
                            //это при исправлении
                            //jQuery(this).append('<p><b>исправление</b></p>');
                            jQuery(this).append('<p>В случае если в документ заявления были внесены изменения, необходимо обновить файл подписанного эл. подписью заявления. Для выгрузки файла заявления нажмите на '
                                    +'<a href="/eo/license/make-document/requestmaket/'+requestId+'"'
                                    +'target="_blank" >ссылку</a></p>');
                            jQuery(this).append('<p >Файл подписанного эл. подписью заявления: <span class="dialog-ref"></span></p>');
                            jQuery(this).append('<p>При нажатии на кнопку «Отправить» ваш комплект документов будет отправлен в Рособрнадзор</p>');
                            jQuery(this).append('<p>Для отмены действия и возврата к редактированию заявления – нажмите на кнопку «Закрыть»</p>');
                            jQuery(this).append('<p align="center"><b>Внимание!</b></p>');
                            jQuery(this).append('<p>После отправки документа в Рособрнадзор их дальнейшее редактирование невозможно</p>');
                        }
                        else{
                            //это при новом заявлении
                            /*jQuery(this).append('<p>Для завершения отправки заявления, требуется загрузить в систему файл подписанного эл. подписью заявления</p>');*/
                            //jQuery(this).append('<p><b>новое</b></p>');
                            jQuery(this).append('<p>Для завершения отправки заявления, требуется загрузить в систему файл подписанного эл. подписью заявления.</p><p>Макет заявления формируется системой автоматически. Для выгрузки файла заявления нажмите на '
                                    +'<a href="/eo/license/make-document/requestmaket/'+requestId+'"'
                                    +'target="_blank" >ссылку</a></p>');
                            jQuery(this).append('<p >Файл подписанного эл. подписью заявления: <span class="dialog-ref"></span></p>');
                            jQuery(this).append('<p >pdf-файл подписанного заявления: <span class="dialog-scan"></span></p>');
                            jQuery(this).append('<p>При нажатии на кнопку «Отправить» ваш комплект документов будет отправлен в Рособрнадзор</p>');
                            jQuery(this).append('<p>Для отмены действия и возврата к редактированию заявления – нажмите на кнопку «Закрыть»</p>');
                            jQuery(this).append('<p align="center"><b>Внимание!</b></p>');
                            jQuery(this).append('<p>После отправки документа в Рособрнадзор их дальнейшее редактирование невозможно</p>');                            
                        }
                        jQuery(this).find('.dialog-ref').append(ref);
                        jQuery(this).find('.dialog-ref').append(link);
                        jQuery(this).find('.dialog-ref').append(check);
                        jQuery(this).find('.dialog-scan').append(scanSignLink);
                        
                        jQuery(this).dialog('option', 'position', { my: "center top", at: "center top", of: jQuery(jQuery('.dialog-request-edit.ui-dialog-content')[0]), collision: "fit"  });
                        jQuery(this).dialog('option', 'position', { my: "center top", of: jQuery(jQuery('.dialog-request-edit.ui-dialog-content')[0]), collision: "flip"  });
                        jQuery('.request-send-to-ron-dialog').dialog('option','position',{ my: "center top", at: "center top", of: jQuery(jQuery('.dialog-request-edit.ui-dialog-content')[0]), collision: "fit" });
                    },
                    close: function () {
                        jQuery(this).remove();
                    },
                    buttons: [
                        {
                            text: 'Отправить',
                            class: '',
                            click: function() {
                                newRequestSendSend(requestId);
                                //jQuery(this).dialog('close');
                            }
                        },
                        {
                            text: 'Закрыть',
                            class: '',
                            click: function() {
                                jQuery(this).dialog('close');
                            }
                        }
                    ]

                });
    }



    /**
     * оправка заявления на открытую часть
     *
     * @param {type} requestId
     * @returns {undefined}
     */
    function newRequestSendSend(requestId){
        console.log('редактирование заявления  - хотим послать передать заявления на отккрытую часть');
        if( 'undefined' == typeof(requestId) || '' == requestId){
            useTopHoverTitleError('Не выбрано заявление');
            return 0;
        }
        if('boolean' == typeof(isSendedToOpenServerFlag) &&  isSendedToOpenServerFlag){
            useTopHoverTitleError('Запрос в обработке');
            return 0;            
        }

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: "/eo/license/send-to-open/"+requestId,
                //data: data,
                beforeSend: function(){
                    isSendedToOpenServerFlag = true;
                    useTopHoverTitle('Отправляем данные в Рособрнадзор',20000);
                    //$(dialog).append($("<div class='loading-img-full'><img src='/img/loading_islod.gif' /></div>")).fadeIn(500);
                    //useTopHoverTitle('Отправляем данные');
                },
                success: function (answer) {
                    console.log('отправка на открытую часть - ответ получен - отработаем ответ');
                    useTopHoverTitle('',1);
                    if( 'undefined' == typeof(answer.console) ){
                        answer.console = '';
                    }
                    if( 'undefined' == typeof(answer.message) ){
                        answer.message = '';
                    }
                    
                        if( 'undefined' == typeof(answer.result) || 'Ok' != (answer.result) ){
                            if( '' == answer.message ){
                                useTopHoverTitleError("Ошибка при удалении");
                            }
                            else{
                                useTopHoverTitleError(answer.message);
                            }
                            if( '' == answer.console ){

                            }
                            else{
                                console.error(answer.console);
                            }
                        }
                        else{
                            if( '' == answer.message ){                                                                

                            }
                            else{
                                useTopHoverTitle(answer.message);
                            }         
                            isSendedToOpenServerFlag = false;
                            //перегрузить пагинацию
                            var itPage = jQuery('#pagination .current-page').text();
                            _serverQuery(itPage);                           

                            var dContent = $(
                                '<div >' +
                                '</div>');
                            $(dContent).dialog({
                                title: "Документы отправлены",
                                width: 500,
                                modal: true,
                                open: function () {
                                    jQuery(this).append('<p>Документы успешно отправлены</p>');
                                    jQuery(this).append('<p>Информацию о ходе рассмотрения можно получить в разделе «Отправленные заявления»</p>');
                                },
                                close: function () {
                                    jQuery(this).remove();
                                },
                                buttons: [
                                    {
                                        text: 'Закрыть',
                                        class: '',
                                        click: function() {
                                            jQuery(this).dialog('close');
                                        }
                                    }
                                ]
                            });
                            //закроем этот диалог
                            jQuery('.request-send-to-ron-dialog').dialog('close');                            
                            //закроем диалог заявления
                            jQuery('.dialog-request-edit').dialog('close');
                        }                                           
                },
                error:function(e){
                    //сбросим сообщения
                    useTopHoverTitle("",1);
                    useTopHoverTitleError("Ошибка связи");
                    $(".loading-img-full").fadeOut(500, function(){

                    });
                    console.error('ошибка связи при переходе при удалении');
                    console.log(e);
                    console.log(e.responseText);
                },
                complete: function(){
                    isSendedToOpenServerFlag = false;
                }                
            });
    }

