var isHashChangerClick = false;
var intervalIdentificator = null;

$(document).ready(function(){
    if( 'undefined' != typeof(flagLastMainLoadjsEoLicense) ){
        return 0;
    }
    flagLastMainLoadjsEoLicense = true;    
    console.info('запускаем обработчики загрузки /js/eo/license/main.js');
        
    //Вешаем обработчик для кнопок шагов    
    $(".btn",".steps_div").click(function(){
        //меняем внешний вид
        $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
        $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
    });

    bindIndexEvents();
    
    $(window).unbind( 'hashchange' );
    $(window).bind('hashchange', function(){
        if(isHashChangerClick){
            isHashChangerClick = false;
            return;
        }
        if(window.location.hash){
            $("a[href^='"+window.location.hash+"']").parent().click();
        }else{
            window.location.hash = $(".btn-lg").attr('event');;
            var event=$(".btn>a",".steps_div").first().attr('event');
            var controller=$(".btn>a",".steps_div").first().attr('controller');
            indexEventsAjaxHandler($(".btn>a",".steps_div").first(), event, controller);
        }
    });
    if(window.location.hash){
        $("a[href^='"+window.location.hash+"']").parent().click();
    }else{
        window.location.hash = $(".btn-lg").attr('event');
        var event=$(".btn>a",".steps_div").first().attr('event');
        var controller=$(".btn>a",".steps_div").first().attr('controller');
        indexEventsAjaxHandler($(".btn>a",".steps_div").first(), event,controller);
    }


    $(window).resize(function(){
        resizeContent($(".container .viewzone"));
        resizeContent($(".content_td .content"));
    });

    console.log('Обработчик скрытия левого меню /public/js/eo/license/main.js');
    $(".steps_td").on('click','.toggleButton',function(){
        $('body').toggleClass('icons');
        $('#head').toggleClass('icons');
        $('.route .steps_td').toggleClass('icons');
        $('.steps_td .nav li').toggleClass('icons');
        $('.steps_td .nav li a').toggleClass('icons');
        $('.steps_td .nav li a span.num').toggleClass('icons');
        $(this).toggleClass('icons');
        if($(this).hasClass('icons')){
            setCookie('menu','icons');
        }else{
            setCookie('menu','');
        }
    });

});



function showPop(url){
    $.ajax({
        type: 'POST',
        url: url,
        success: function(answer) {
           $("#popupWindow").html(answer);
           $("#popupWindow").show();
        }
    });
}

function bindIndexEvents() {
    //проблема - этот обработчик вешается много раз, а потом запускается множество раз
    // если его сбрасывать, то он убивается и во время обработки, решиб пооставить влаг
    if( 'undefined' != typeof(bindIndexFlagjsEoLicenseMain) ){
        return 0;
    }
    bindIndexFlagjsEoLicenseMain = true;
    $('.btn').unbind('click');
    //Вешаем обработчик на переключение вкладок
    $('.btn').click(function() {
        isHashChangerClick = true;
        var event = $(this).attr('event');
        var _this = this;
        var controller = $(this).attr('controller');
        if (event) {
            $(".btn",".steps_div").removeClass("btn-warning").removeClass("btn-lg").addClass("btn-success");
            $(this).removeClass("btn-success").addClass("btn-warning").addClass("btn-lg");
            indexEventsAjaxHandler(_this, event,controller);
        }
    });

    //changeAffilatesSelect();
    //changeLicenseSupplementsSelect();
    //changeCertificateSupplementsSelect();
    //changeCertificateSelect();
    //btPubAccrHandlers();
    //btSaveHandlers();
    //btSendErrorHandler();
    //searchHandlers();
}

/**
 * обработчик тыка по елементам меню слева
 * 
 * @param {type} _this
 * @param {type} event
 * @param {type} keep_selector
 * @returns {Boolean}
 */
function indexEventsAjaxHandler(_this, event ,keep_selector) {
    console.log('обработчик тыка по елементам меню слева 1');
    if (!event) return false;
        var destination_link = '/eo/'+keep_selector+'/' + event;

    $.ajax({
        type: 'POST',
        url: destination_link,
        beforeSend: function(){
            //ресайзимблок с контентом
			resizeContent($(".content_td .content"));
			$(".content_td .content").html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
            //$(".content_td .content").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);            //
            
            //если это не вкладка с общими сведениями
            //то прячем кнопку сохранения
            $(".button, .sumField",".buttons-container").hide();
            $(".btSendError",".buttons-container").show();

            $(".btSave",".buttons-container").removeClass("isAffiliates");
            if(event == "info"){
                $(".btSave",".buttons-container").show();
                $(".btPubAccr",".buttons-container").show();                
            }
            if(event == "affiliates"){
                $(".btSave",".buttons-container").addClass("isAffiliates");
                $(".btSave",".buttons-container").show();
            }
            if(event == "calculator"){
                $(".btSendError",".buttons-container").hide();
                $(".sumField",".buttons-container").show();
                $(".btClearAll",".buttons-container").show();
                $(".btFromApplication",".buttons-container").show();
            }
            if(keep_selector == "declaration"){
                getAllCount();
            }
        },
        success: function(answer) {
            $(".content_td .content .loading-img").fadeOut(500, function(){
                answer = $(answer)
                $(".content_td .content").html("").append(answer);
                resizeContent($(".viewzone", answer));
                
                $(".date-field",".viewzone").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy"
                });
                $(".date-icon",".viewzone").click(function(){
                    $(this).parent().find("input").focus()
                })

                //Если вкладка с калькулятором, инициируем калькулятор
                if(event=="calculator"){
                    Calculator.init();
                    //прячем поиск
                    $("#filter").hide();
                } else {
                    $("#filter").show();
                }

            })
        },
        error:function(e){
            //сбросим сообщения
            useTopHoverTitle("",1);
            useTopHoverTitleError("Ошибка связи");
            $(".content_td .content .loading-img").fadeOut(500, function(){

            });
            console.error('ошибка связи при переходе по меню');
            console.log(e);
            console.log(e.responseText);
        }            
    });
}

function resizeContent(content){
    var wHeight = $(window).height();
    var delta = 180;
    if($(content).attr("data-heightdelta"))
        delta -= parseInt($(content).attr("data-heightdelta"));

    var contentHeight = wHeight - delta;
    $(content).height(contentHeight);
}


function searchHandlers(){
    $.expr[":"].contains = $.expr.createPseudo(function(arg) {
        return function( elem ) {
            var strText = $(elem).text()//.toLowerCase();
            var valText = $(elem).val()//.toLowerCase();
            if(
                strText.indexOf(arg)//.toLowerCase()) >= 0
                || valText.indexOf(arg)//.toLowerCase()) >= 0
                )
                return true;
            else
                return false;
        };
    });

    //Обработчик ввода текста в поиск
    $('#filter').keyup(function(e) {
        // Добавил проверку что бы убрать обработку для вкладки "Заявления"
        if($(".nav li:first").hasClass('active')){
            if(e.which == 27){
                clearTimeout($.data(this, 'timer'));
                clearSearch();
                return;
            }
            clearTimeout($.data(this, 'timer'));
            var wait = setTimeout(search, 500);
            $(this).data('timer', wait);
        }
    });

    function search(){
        var searchVal = $('#filter').val();
        $("tr",".viewzone").hide();
        $(".left-title:contains("+searchVal+")",".viewzone").parents("tr").show();
        $(".right-value:contains("+searchVal+")",".viewzone").parents("tr").show();
        $("textarea:contains("+searchVal+")",".viewzone").parents("tr").show();
        $("input:contains("+searchVal+")",".viewzone").parents("tr").show();
    }

    function clearSearch(){
        $('#filter').val("");
        $("tr",".viewzone").show();
    }
}



//изменяем ширины полей шапки таблички
    function tableProgramsByUgsHeadering(){
        var header=jQuery('#edu_programs thead').html();
        jQuery('.edu_programs_table_header thead').html('');
        jQuery('.edu_programs_table_header thead').append(header);

        jQuery('.edu_programs_table_header table').width( jQuery('#edu_programs').width());

        var header2 = jQuery('.edu_programs_table_header thead  th');
        //console.log(header2);
        jQuery('#edu_programs thead th').each(function(i,el){
            var h = jQuery(el).height();
           jQuery(header2[i]).height(h);

            var w = jQuery(el).innerWidth();
            w=Math.ceil(w);

            w = jQuery(el).width();
           jQuery(header2[i]).width(w);
           jQuery(el).width(w);

           var fs=jQuery(el).css('font-size');
           jQuery(header2[i]).css('font-size',fs);

        });
    }

 /**
 * В специальной верхней зоне выводит текст из параметра text, 
 * и прячет его через время указанное в милисекундах в параметре time 
 * 
 * @param string text
 * @param int time
 */
function useTopHoverTitle(text, time) {
    $("#hover-title").text(text);
    $("#hover-title").fadeIn("fast");
    setTimeout(function () {
        $("#hover-title").fadeOut('fast');
    }, time);
}

/**
 * Вешаем событие на клик по всплывающему окну.
 */
function eventTopHoverClick() {
    $("body").undelegate("#hover-title", "click");
    $("body").delegate("#hover-title", "click", function () {
        $(this).toggle();
    });
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}






