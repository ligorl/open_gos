declaration = {};
$(document).ready(function(){
    $("body").delegate(".change_fill_deprivation","click",function(){
        $(".change_fill_deprivation").removeClass("active_fill_deprivation");
        $(this).addClass("active_fill_deprivation");
        declaration.deprivation.getEduLevel();
    })
    $("body").delegate(".leftSideBarDeprivation .level-elem-deprivation","click",function(){
        $(".level-elem-deprivation").removeClass("active-level-deprivation");
        $(this).addClass("active-level-deprivation");
        declaration.deprivation.getUgsList();
    })
    $("body").delegate(".rightSideBarDeprivation .ugs_row_deprivation ","click",function(){
        $(".ugs_row_deprivation").removeClass("ugs_active_row_deprivation");
        $(this).addClass("ugs_active_row_deprivation");
        console.log(1);
        declaration.deprivation.getProgramsByUgs();
    })
})
$(window).resize(function () {
    tableProgramsByUgsHeadering();
});

declaration.getNextTab = function(){
    var next = 0;
    var curent = jQuery('#list_tabs .active.show_tab').attr('tab');
    var prev = 0;
    jQuery('#list_tabs a[tab]:visible').each( function(ind,element){
        var it = jQuery(element).attr('tab');
        if( prev == curent ){
            next = it;            
        }          
        prev = it;
    });
    return next;
}

declaration.printApplicationDialog = function(url){
    var className = 'print-application-dialog',
        selClass = '.'+className;

    $('body').append('<div class="'+className+'"></div>');
    
    $( selClass).dialog({
            title: 'Формирование заявления',
            width: 400,
            height: 300,
            modal: true,
            close: function () {
                $( selClass).remove();
            },
            buttons: [
                {
                    text: "Сформировать",
                    click: function() {
                        var dt =  $('.print-application-date').val(),
                            newUrl = encodeURI(url+'/'+dt);
                    
                        if( dt == ''){
                            useTopHoverTitleError('Задайте, пожалуйста, дату заявления',2000);
                            return 0;
                        }
                        console.log(newUrl);
                        window.location.href = newUrl;
                        $( this ).dialog( "close" );
                    }
                },
                {
                    text: "Отмена",
                    click: function() {
                        $( this ).dialog( "close" );
                    }
                },
            ],
            open: function () {
                $( selClass).append('<p style="margin-top: 50px">Для формирования заявления выберите дату заявления</p><div class="relative_block"><input class="print-application-date" style="width: 100%;padding-left: 10px;" /><span class="date-icon"x;"></span></div>');
                $( selClass+' .print-application-date').datepicker({ dateFormat: "dd.mm.yy", changeYear: true}).datepicker('setDate',new Date());
            }
        });
}
