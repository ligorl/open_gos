$(document).ready(function(){
    var filter = $("#filter");
    // Датапикер
    max_show_tab = 0;
    $(".date-field").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy",
    });
    jQuery(".date-field").datepicker("option", "maxDate", '');
    jQuery(".date-field").datepicker("option", "minDate", '');
    /**
     * ограничем по дате окончания
     */
    jQuery('input.SearchDate[name="dateIssueBegin"]').datepicker(
        'option',
        'beforeShow',
        function ( input, inst ){
            var elementFromChange = jQuery('input.SearchDate[name="dateIssueEnd"]');
            var elementFromValue  = jQuery(elementFromChange).val();
            jQuery(input).datepicker("option", "maxDate", elementFromValue);
        }
    );
    /**
     * ограничем по дате начала
     */
    jQuery('input.SearchDate[name="dateIssueEnd"]').datepicker(
        'option',
        'beforeShow',
        function ( input, inst ){
            var elementFromChange = jQuery('input.SearchDate[name="dateIssueBegin"]');
            var elementFromValue  = jQuery(elementFromChange).val();
            jQuery(input).datepicker("option", "minDate", elementFromValue);
        }
    );

    // Обработчик клика по кнопке.
    filter.delegate('.btApply','click',function(){
        var search = {
            "regNumber" : $(".regNumber", "#filter").val() || '',
            "status"    : $(".status", "#filter").val() || '',
            "dateBegin" : $(".createDateBegin", "#filter").val() || '',
            "dateEnd"   : $(".createDateEnd", "#filter").val() || ''

        };
        if (event) {
            searchNotification(search);
        }
    });

    // Обработчик клавиши Enter.
    filter.keydown(function(e){
        if (e.keyCode == 13)
        {
            $('.btApply').click();
            return false;
        }
    });

    // Обработчик вдля ввода в поле только цыфр
    $(".regNumber").keydown(function(event) {
        // Разрешаем: backspace, delete, tab и escape
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            // Разрешаем: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Разрешаем: home, end, влево, вправо
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // Ничего не делаем
            return;
        }
        else {
            // Обеждаемся, что это цифра, и останавливаем событие keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

});

/**
 * Поиск.
 *
 * @param search
 * @returns {boolean}
 */
function searchNotification(search){
    $.ajax({
        type: 'POST',
        data: search,
        url: '/eo/declaration/notification/1',
        beforeSend: function(){
            $(".supplements").html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
        },
        success: function(answer) {
            $(".supplements .loading-img").fadeOut(500, function(){
                $(".supplements").html("").append(answer);
                resizeContent($(".viewzone", answer));
                updateCountAllRecord();
            })
        }
    });
}

/**
 * Обновление количества найденых записей.
 */
function updateCountAllRecord() {
    $("#report-generate-count-span").text($(".supplements .row_appl").length);
}




