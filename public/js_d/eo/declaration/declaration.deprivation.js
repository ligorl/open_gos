
declaration.deprivation = {};

declaration.deprivation.openTabDeprivation = function ( tabName, type ) {
    if( 'undefined' == typeof(tabName) || '' == tabName){
        this.tabName = 'deprivation';
    }
    else{
        this.tabName = tabName;
    }
    if( 'undefined' == typeof(type) || '' == type){
        this.type = '';
    }
    else{
        this.type = type;
    }
    $(".show_tab").removeClass("active");
    $(".tabs-panel").removeClass("show-panel");
    if( 'removal' == this.tabName){
        $(".show_tab[tab='removal']").addClass("active");
        $("#tabremoval").addClass("show-panel").show();
    }
    else{
        $(".show_tab[tab='deprivation']").addClass("active");
        $("#tabdeprivation").addClass("show-panel").show();
    }
    declaration.deprivation.getFillialsPrograms();
    jQuery('#sendform1').attr('numb',this.tabName);
}


/**
 * дергаем филиалы
 * @returns {undefined}
 */
declaration.deprivation.getFillialsPrograms = function (type) {
    var data = {};
    if( 'removal' == this.tabName ){
        data['tabName'] = 'removal';
    }
    data['sent'] = this.type;
    $.ajax({
        url: '/eo/declaration/get-fillials-programs-deprivation/' + $("#id_application_val").val(),
        type: "POST",
        data: data,
        beforeSend: function () {
            $(".loading-overlay").show();
        },
        success: function (answer) {
            jQuery('.tabs-panel.show-panel').find(".leftSideBarDeprivation").html(answer);
            $(".loading-overlay").fadeOut(250);
            jQuery('.tabs-panel.show-panel').find(".change_fill_deprivation:first").click();
        },
        error: function (e) {
            useTopHoverTitleError("Возникла ошибка");
            $(".loading-overlay").fadeOut(250);
            console.log("Ошибка");
            console.log(e);
        }
    });
}

/**
 * дергаем типы программ у филиалов
 * @returns {undefined}
 */
declaration.deprivation.getEduLevel = function () {
    var data = {};
    if( 'removal' == this.tabName ){
       data['tabName'] = 'removal';
    }
    data['sent'] = this.type;
    $.ajax({
        url: '/eo/declaration/get-edu-program-type-deprivation/' 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_appl') 
                + '/' 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_org'),
        data: data,
        type: "POST",
        success: function (answer) {
            jQuery('.tabs-panel.show-panel').find('.leftSideBarDeprivation .level-list').addClass('old-level-list');
            jQuery('.tabs-panel.show-panel').find('.leftSideBarDeprivation .old-level-list').remove();
            jQuery('.tabs-panel.show-panel').find('.leftSideBarDeprivation .active_fill_deprivation').parent('li').append(answer);
            jQuery('.tabs-panel.show-panel').find('.leftSideBarDeprivation .level-list').show(250);
            jQuery('.tabs-panel.show-panel').find('.leftSideBarDeprivation .level-list:first').click();
            declaration.deprivation.getUgsList();
        }
    });
}

/**
 * Дергаем список угс у у ровня и рисуем заготовку таблички
 * @returns {undefined}
 */
declaration.deprivation.getUgsList = function () {
    var data = {};
    if( 'removal' == this.tabName ){
       data['tabName'] = 'removal';
    }
    data['sent'] = this.type;
    $.ajax({
        url: '/eo/declaration/get-ugs-by-level-deprivation/' 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_appl') 
                + '/' 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_org') 
                + '/' 
                + jQuery('.tabs-panel.show-panel').find(".leftSideBarDeprivation .active-level-deprivation").attr('id-level'),
        type: "POST",
        data: data,
        success: function (answer) {
            jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation').html(answer);
            jQuery('.tabs-panel.show-panel').find(".rightSideBarDeprivation .ugs_row_deprivation").click();
            declaration.deprivation.tableProgramsByUgsHeadering();
        }
    });
}

/**
 * тык по учгс и получи пролграмки
 * 
 * @returns {undefined}
 */
declaration.deprivation.getProgramsByUgs = function () {
    var data = {};
    if( 'removal' == this.tabName ){
       data['tabName'] = 'removal';
    }
    $.ajax({
        //type: 'POST',
        data: data,
        dataType: "html",
        async: false,
        url: '/eo/declaration/get-program-by-ugs-deprivation/' 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_appl') 
                + '/' 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_org') 
                + '/' 
                + jQuery('.tabs-panel.show-panel').find('.active-level-deprivation').attr('id-level') 
                + '/' 
                + jQuery('.tabs-panel.show-panel').find('.ugs_active_row_deprivation').attr('ugs_id'),
        success: function (answer) {
            if (!$("#show_all_program_deprivation").is(":checked")) {
                jQuery('.tabs-panel.show-panel').find('.ugs_block').removeClass('active_ugs_block');
            }
            if ($("#show_only_program_deprivation").is(":checked") && answer.indexOf("Нет выбранных программ") == -1) {
                console.log(1);
                jQuery('.tabs-panel.show-panel').find('.ugs_block[ugs_id="' + jQuery('.tabs-panel.show-panel').find('.ugs_active_row_deprivation').attr('ugs_id') + '"]').html(answer);
                jQuery('.tabs-panel.show-panel').find('.ugs_block[ugs_id="' + jQuery('.tabs-panel.show-panel').find('.ugs_active_row_deprivation').attr('ugs_id') + '"]').addClass('active_ugs_block');
            }
            if (!$("#show_only_program_deprivation").is(":checked")) {
                jQuery('.tabs-panel.show-panel').find('.ugs_block[ugs_id="' + jQuery('.tabs-panel.show-panel').find('.ugs_active_row_deprivation').attr('ugs_id') + '"]').html(answer);
                jQuery('.tabs-panel.show-panel').find('.ugs_block[ugs_id="' + jQuery('.tabs-panel.show-panel').find('.ugs_active_row_deprivation').attr('ugs_id') + '"]').addClass('active_ugs_block');
            }
            declaration.deprivation.tableProgramsByUgsHeadering();
        }
    });
}


/**-----------------
 * 
 * @param {type} id
 * @returns {undefined}
 */
declaration.deprivation.getDirectByUgs = function (id) {
    var data = {};
    if( 'removal' == this.tabName ){
       data['tabName'] = 'removal';
    }
    jQuery('.tabs-panel.show-panel').find('.ugs_row_deprivation').removeClass('ugs_active_row_deprivation');
    jQuery('.tabs-panel.show-panel').find('.ugs_row_deprivation[ugs_id="' + id + '"]').addClass('ugs_active_row_deprivation');
    var dContent = $(
            '<div class="dialog-directugs-info">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    $(dContent).dialog({
        title: "Направление подготовки",
        width: 600,
        modal: true,
        close: function () {
            $(".dialog, .dialog-directugs-info").remove();
        },
        open: function () {
            //Отсылаем запрос на получение инфо по заявлению
            $.ajax({
                //type: 'POST',
                dataType: "html",
                data: data,
                url: '/eo/declaration/get-direct-by-ugs-deprivation/' 
                        + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_appl') 
                        + '/' 
                        + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_org') 
                        + '/' 
                        + jQuery('.tabs-panel.show-panel').find('.leftSideBarDeprivation .active-level-deprivation').attr('id-level') 
                        + '/' 
                        + id,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));
                        //Вешаем обработчики
                        //Устанавливаем высоту
                        $(".dialog-content", dContent).height($(dContent).height() - 20);
                    });
                }
            });
        }
    });
}

/**
 * строим окошко выбора программ
 * 
 * @returns {undefined}
 */
declaration.deprivation.sendCheckDirect = function () {
    var prLish = 1;
    if( 'removal' == this.tabName ){
       prLish  = 2;
    }
    $.ajax({
        type: 'POST',
        data: $("#direct_form").serialize() 
                + "&appl=" 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_appl') 
                + "&org=" 
                + jQuery('.tabs-panel.show-panel').find(".active_fill_deprivation").attr('id_org') 
                + "&level=" 
                + jQuery('.tabs-panel.show-panel').find('.active-level-deprivation').attr('id-level') 
                + "&PrLish="+prLish,
        url: '/eo/declaration/save-checked-direct',
        success: function (answer) {
            $(".dialog, .dialog-directugs-info").remove();
            declaration.deprivation.getProgramsByUgs();
        }
    });
}


/**
 * изменяем ширины полей шапки таблички
 * 
 * @param {type} id
 * @param {type} title
 * @returns {undefined}
 */
declaration.deprivation.tableProgramsByUgsHeadering = function () {
    var header = jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation #edu_programs thead').html();
    jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation .edu_programs_table_header thead').html('');
    jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation .edu_programs_table_header thead').append(header);
    jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation .edu_programs_table_header table').width(jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation #edu_programs').width());

    var header2 = jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation .edu_programs_table_header thead  th');
    //console.log(header2);
    jQuery('.tabs-panel.show-panel').find('.rightSideBarDeprivation #edu_programs thead th').each(function (i, el) {
        var h = jQuery(el).height();
        jQuery(header2[i]).height(h);

        var w = jQuery(el).innerWidth();
        w = Math.ceil(w);

        w = jQuery(el).width();
        jQuery(header2[i]).width(w);
        jQuery(el).width(w);

        var fs = jQuery(el).css('font-size');
        jQuery(header2[i]).css('font-size', fs);

    });
}

/**
 * акредитация - заявления - карточка заявления - образовательные программы для лишения - удаление программ
 */
function deleteProgramApplDeprivation(id){
     $.ajax({
        type: 'POST',
        dataType: "html",
        url: '/eo/declaration/delete-program-application/'+id,
        success:function(){
            declaration.deprivation.getProgramsByUgs();
        }
    });
}
$(document).ready(function(){
    $('body').undelegate('#show_all_program_deprivation', 'click');
    $('body').delegate('#show_all_program_deprivation', 'click', function () {
        setTimeout(function () {
            if ($("#show_all_program_deprivation").is(":checked")) {
                $('.ugs_row').each(function (i) {
                    $(this).click();
                });
            } else {
                if ($("#show_only_program_deprivation").is(":checked")) {
                    $("#show_only_program_deprivation").click();
                }
                $('.ugs_row:first').click();
            }
        },100);
    });
    $('body').undelegate('#show_only_program_deprivation', 'click');
    $('body').delegate('#show_only_program_deprivation', 'click', function () {
        if ($("#show_only_program_deprivation").is(":checked")) {
            $('.ugs_row_tbody').hide();
            /*if (!$("#show_all_program_deprivation").is(":checked")) {
             $("#show_all_program_deprivation").click();
             }*/
            $(".ugs_block tr:not('.programRow')").parent("tbody.ugs_block").removeClass("active_ugs_block");
        }else{
            $('.ugs_row_tbody').show();
            $(".ugs_block tr:not('.programRow')").parent("tbody.ugs_block").addClass("active_ugs_block");
        }
    });
});



function addOrgPage() {
    $(document).ready(function(){
        $("#wrapReorg").undelegate("#getNextResult");
        $("#wrapReorg").delegate("#getNextResult","click",function(){
            searchOrgAdd();
        });

        var nextPage=$("#nextPage").val();
        nextPage=parseInt(nextPage);
        var maxPage=$("#maxPage").val();
        maxPage=parseInt(maxPage);
        if(nextPage<= maxPage ){
            if($(".searchOrganization").val()==''){
                var url='/eo/declaration/get-edu-org/'+$("#nextPage").val();
            }else{
                var url='/eo/declaration/get-edu-org/'+$("#nextPage").val()+'/'+$(".searchOrganization").val();
            }
            $.ajax({
                url: url,
                beforeSend: function (xhr) {
                    $("#getNextResult").parent('td').parent('tr').remove();
                    $("#nextPage").remove();
                    $("#maxPage").remove();
                },
                success: function(answer){
                    $("#filteredTable tbody").append(answer);
                },
                error:function(e){
                    useTopHoverTitleError("Возникла ошибка");
                    $(".loading-overlay").fadeOut(250);
                    console.log("Ошибка");
                    console.log(e);
                }
            });
        }
    });
}