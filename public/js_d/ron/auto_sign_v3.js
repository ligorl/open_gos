var start_auto_sign = function(CertificateId)
{
    console.log( 'хотим подписать' );
    //проверим браузер IE
    var is_ie = is_ie_navigator();
    is_ie = true;
    if(!is_ie)
    {
        var message = $('<p></p>').html("Функция доступна только в браузере Microsoft Internet Explorer").css("text-align","center");
        open_dialog("notification", message, true);
        return false;
    }
    
    //открываем окно с xml
    $.ajax({
        type: "POST",
        url: "/ron/index/get-xml-for-sign/"+CertificateId,
        dataType: "json",
        success: function(data){
            $('.xml').remove();
            $("<div class='xml'></div>").html("<p class='segoe'>"+data.xml+"</p>")
                .dialog({
                        autoOpen:false,
                        title: "Данные для подписи",
                        width:1200,
                        height: 700,
                        modal:true,
                        buttons: { "Подписать": function() { 
                                //тут открыть окно подписания
                                start_auto_sign_cades( CertificateId , data.archive_name );
                                //pre_sign(data.archive_name, CertificateId);
                                
                            } }
                }).dialog("open");
                $('.qr', '.xml').attr("src",'/auto_signed_files/'+CertificateId+'.png').attr("width",100);
                $('.gerb-rf', '.xml').attr("src",'/auto_signed_files/gerb-rf.png').attr("width",100);
                //$('.linkqr', '.xml').attr("href",'http://isga.obrnadzor.gov.ru/accredreestr/details/'+CertificateId+'/1/').attr("width",100);
                
            
        },
        error:function(e){
            //jQuery(dContent).html('Ошибка связи при загрузке');
            console.error( '890 - диалог подписания свидетельства - ошибка при загрузке диалога');
            console.log(e);
            console.log(e.responseText);
            useTopHoverTitleError("Ошибка связи")
        }
    });
    
}

var start_auto_sign_cades = function( CertificateId , signData )
{
    if( 'undefined' == typeof( signData ) ){
        signData = '';
    }
    CertificateIdGlobalVar = CertificateId;
    CertificateFileNameGlobalVar = signData;
    console.log( CertificateFileNameGlobalVar );
     
    console.info( 'хотим подписать свидетельство ' );
    
            var dContent = $('<div class="sign-dialog"></div>');
            console.log('карточка Свидетельства - диалог подписания - данные провереныы, рисуем')
            $(dContent).dialog({
                    //title: title,
                    width: '95%',
                    //height: heightWindow,
                    //position: 'center',
                    position: { my: "center center", at: "center center", of: window },
                    modal: true,
                    close: function(event, ui) {
                        console.log('карточка Свидетельства - диалог подписания - закрываем диалог');
                        $(this).remove();
                    },
                    open: function() {
                        var dialog = this;
                        console.log( dialog );
                        
                            $.ajax({
                                type: 'POST',

                                url: '/islod/index/sign-dialog/first/'+CertificateId,
                                beforeSend: function(){
                                    //ресайзимблок с контентом
                                    resizeContent($(dContent));
                                    $(dContent).html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
                                    //$(".content_td .content").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);            //
                                },
                                success: function(answer) {
                                    console.info( 'диалог подписания свидетельства - данные получены');
                                    jQuery(dContent).html('');
                                    answer = jQuery( answer );
                                    jQuery(dContent).append(answer);//.fadeIn(500);
                                    jQuery('.dialog-cancel',dContent).click(function(){                                        
                                        jQuery(dContent).dialog('close');
                                    });
                                    resizeContent(".license-stop-suplement-cheked-parent .viewzone");
                                    specialTableResize(".license-stop-suplement-cheked-parent");
                                    //выцентруем окошко по высоте
                                    var heithWindow = jQuery(window).height();
                                    var heithDialog = jQuery(dContent).parent().height();
                                    var top = (heithWindow - heithDialog) / 2;
                                    jQuery(dContent).parent().css('top',top);
                                    
                                    console.log( 'подтянет данные для подписи' );
                                    
                                    //jQuery( '#DataToSignTxtBox' ).text(signData);
                                    
                                    /*
                                    $.ajax({
                                        type: 'POST',
                                        url: '/ron/index/get-xml-for-sign/'+CertificateId,
                                        dataType: "json",
                                        success: function(answer) {
                                            console.log( 'хмл получен ');                                            
                                            jQuery( '#DataToSignTxtBox' ).text(answer.xml);
                                            console.log( 'диалог подписания свидетельства -подгрузка хмл - добавлено');                                             
                                        },
                                        error:function(e){
                                            jQuery(dContent).html('Ошибка связи при загрузке');
                                            console.error( 'диалог подписания свидетельства -подгрузка хмл - ошибка при загрузке диалога');
                                            console.log(e);
                                            console.log(e.responseText);
                                            useTopHoverTitleError("Ошибка связи")
                                        }
                                    });
                                    */
                                },
                                error:function(e){
                                    jQuery(dContent).html('Ошибка связи при загрузке');
                                    console.error( 'диалог подписания свидетельства - ошибка при загрузке диалога');
                                    console.log(e);
                                    console.log(e.responseText);
                                    useTopHoverTitleError("Ошибка связи")
                                }
                            });
                        
                        
                        
                        
                        //licenseSlopDoDialogLoad(
                        //    dContent,
                        //    fileId,
                        //    url,
                        //    addGet
                        //);
                    },
                    buttons:[
                        {
                            text: "Да",
                            "class": 'right ',
                            click: function () {
                                //signStepFirstOk();
                                certSinDo(this);

                            }
                        },
                        {
                            text: "Нет",
                            "class": 'right',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    ]
            });
    
    
}

var pre_sign = function(file_name, CertificateId)
{
  
    //открываем окно процесса подписи
    var process_data_element = "<div class='process_data'></div><p class='sel_cert'>Выберите сертификат <select class='list_cert'></select></p>";
    open_dialog("process",  process_data_element, true, "Процесс подписи");
    set_process_message("поиск установленых сертификатов");
    
    var certificateList = $.capicom.getCertificatesList();
   
     console.log(JSON.stringify(certificateList));
    var count_certificate = 0;
    
    $.each(certificateList, function(key, item) {
        count_certificate++;
        var value = item.thumbprint;
        $('.list_cert').append($('<option></option>').attr("value", value).text(item.displayName));
    });
    
    switch(true){
        case (count_certificate == 0):
        {
            set_process_message("В Вашем браузере не установлен сертификат");
            break;
        }
        case (count_certificate == 1):
        {
            set_process_message("сертификат найден");
            $('.sel_cert').hide();
            sign(  $('.list_cert').val() , file_name, CertificateId);
            break;
        }
         case (count_certificate > 1):
        {
            $('.sel_cert').show();
            
            $(".process").dialog({
                buttons: [{
                    text: "Подписать",
                    click: function(){
                        sign(  $('.list_cert').val() , file_name, CertificateId);
                    }
               }]
            })
           
            break;
        }
    }
}

/*
var sign = function( CertificateId, thumbprint )
{
    set_process_message("получение данных для подписи");
    //получение данных для подписи
     $.ajax({
        type: "POST",
        url: "/ron/index/get-xml-for-sign/"+CertificateId,
       // data: ({id : CertificateId}),
        success: function(data){
            var filename = saveFile(data);return false;
            set_process_message("бинарное чтение файла");
            //// бинарное чтение файла
            var content = $.adodb_stream.read_binary(filename);
            
            
            // подпись файла
            //var signature = $.capicom.sign(data, false, thumbprint);
              var signature = $.capicom.sign(content, false, thumbprint);
            
            set_process_message("отправка CMS-контейнера на сервер");
             //// отправка CMS-контейнера на сервер
            var xmlhttp = getXmlHttp()
            xmlhttp.open('POST', '/ron/index/save-signed-file', false);
            xmlhttp.setRequestHeader('Content-Type', 'text/plain; charset=x-user-defined')
            xmlhttp.send(signature);

            var file_name = xmlhttp.responseText;
            alert(file_name)
            //sendServerSign(document_packages_id, file_name);
        }
    });
}*/

var sign = function(thumbprint, file_name, CertificateId){
        var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
        objXMLHTTP.open("GET", "/ron/index/get-binary-file/" + file_name, false);
        
            objXMLHTTP.send();
            
            if (objXMLHTTP.Status == 200)
            {
                
                var signature = $.capicom.sign(objXMLHTTP.ResponseBody, false, thumbprint);
                var xmlhttp = getXmlHttp()
                xmlhttp.open('POST', '/ron/index/save-signed-file/'+file_name, false);
                xmlhttp.setRequestHeader('Content-Type', 'text/plain; charset=x-user-defined')
                xmlhttp.send(signature);

                var file_sign_name = xmlhttp.responseText; 
                $.ajax({
                    type: "POST",
                    url: "/ron/index/update-sign-data",
                    data: ({file_sign_name : file_sign_name, id:CertificateId}),
                    success: function(data){
                        $(".ui-dialog-content").dialog("close");
                        var process_data_element = "<div class='process_data'></div>";
                        open_dialog("process",  process_data_element, true, "Процесс подписи");
                        set_process_message("Свидетельство успешно подписано!");
                        //$('.xml').dialog("close");
                    }
                });
                
            }
}

 function getXmlHttp(){
  var xmlhttp;
  try {
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
    try {
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
      xmlhttp = false;
    }
  }
  if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
    xmlhttp = new XMLHttpRequest();
  }
  return xmlhttp;
}

/**
 * процесс подписи, сообщение
 */
var set_process_message = function( message )
{
    $('.process_data',".process").html(message);
}


/**
 * функция проверки броузера IE
 * @return Boolen
 */
var is_ie_navigator = function()
{
    if((navigator.appName == 'Microsoft Internet Explorer') || ((navigator.appName == 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[.0-9]{0,})").exec(navigator.userAgent) != null))){
        return true;
    }else{
        return false;
    }
}



/**
 * ф-я открытия диалогового окна
 * @param {String} name идентификатор окна
 * @param {String} html содержиимое окна
 * @param {Boolen} close_button надо ли кнопку закрытия
 */
var CertificateId = function(name, html, close_button, title_window)
{
    var title_window = title_window || "Уведомление";
    $('.' + name).remove();
    $("<div class='" + name + "'></div>")
        .html(html)
        .dialog({
            autoOpen:false,
            title: title_window,
            modal:true
        }).dialog("open");
        
    if(close_button){
       $('.' + name).dialog({
           buttons: [{
                text: "Закрыть",
                click: function(){$('.' + name).dialog("close");$('.' + name).remove();}
           }]
       }) 
    }
}


/**
 * сохранение текста в файл
 * @param String; text
 * @return path to file
 */
var saveFile = function(text){
    var objXMLHTTP = new ActiveXObject("MSXML2.XMLHTTP");
    objXMLHTTP.open("GET", "/ron/index/get-xml-for-sign", false);
            objXMLHTTP.send();
            if (objXMLHTTP.Status == 200)
            {
                var objADOStream = new ActiveXObject("ADODB.Stream");
                
                objADOStream.Type = 1
                objADOStream.Open;
                objADOStream.Write(text);
                objADOStream.Position = 0
 
                var fso = new ActiveXObject("Scripting.FileSystemObject");
                var name_file = makeHash();
                  //путь к папке временных файлов
                var temp_path = fso.GetSpecialFolder(2).Path;
                temp_path = temp_path +"\\" + name_file+'.zip';
                objADOStream.SaveToFile(temp_path, 2);
                objADOStream.Close();
                //var objADOStream = null;
            }
    
    return temp_path;
}


/**
 * сохранение текста в файл
 * @param String; text
 * @return path to file
 */
var saveFileold = function(text){
    var fso = new ActiveXObject("Scripting.FileSystemObject");
    var name_file = makeHash();
    //путь к папке временных файлов
    var temp_path = fso.GetSpecialFolder(2).Path;
    temp_path = temp_path +"\\" + name_file+'.zip';
    console.log(temp_path);
    /*var fso = new ActiveXObject("Scripting.FileSystemObject");
    var ts = fso.CreateTextFile(temp_path, true, false);
    ts.Write(text);
    ts.Close(); */
    var adTypeText = 2; // ADODB.Stream file I/O constant.
    //var charset = "Windows-1251";
    var charset = "UTF-8";

    var adSaveCreateOverWrite = 2;
    var stream;
    stream = new ActiveXObject("ADODB.Stream");
    stream.Open();
    stream.Type = adTypeText;
    stream.Position = 0;
    stream.Charset = charset;

    stream.WriteText(text);

    stream.SaveToFile(temp_path, adSaveCreateOverWrite);
    stream.Close();
    
    return temp_path;
}

/**
 * генерирует случайный хэш
 */
var makeHash = function(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 15; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


/**
 * ф-я открытия диалогового окна
 * @param {String} name идентификатор окна
 * @param {String} html содержиимое окна
 * @param {Boolen} close_button надо ли кнопку закрытия
 * @param {String} title_window Наименование окна
 */
function open_dialog(name, html, close_button, title_window)
{
    var title_window = title_window || "Уведомление";
    $('.' + name).remove();
    $("<div class='" + name + "'></div>")
        .html(html)
        .dialog({
            autoOpen:false,
            title: title_window,
            modal:true
        }).dialog("open");
        
    if(close_button){
       $('.' + name).dialog({
           buttons: [{
                text: "Закрыть",
                click: function(){$('.' + name).dialog("close");$('.' + name).remove();}
           }]
       }) 
    }
}