function editTemplate(id) {
    var dContent =
        $('<div class="edit-dialog">' +
            '<form id="editForm" enctype="multipart/form-data" method="post" action="/ron/template/edit-template">'+
                '<input name="id" type="hidden" value="'+ id +'"/>'+
                ' <div><input name="file" type="file"/></div>' +
                ' <div>' +
                    '<textarea placeholder="Коментарий..." rows="10" cols="65" name="comment"></textarea>' +
                '</div>' +
            '</form>'+
        '</div>');

    $(dContent).dialog({
        title: 'Загрузка шаблона',
        width: 500,
        height: 400,
        position: { my: "center center", at: "center center", of: window },
        modal: true,
        close: function(event, ui) {
            $(this).remove();
        },
        open: function() {
        },
        buttons:[
            {
                text: "Сохранить",
                "class": 'left',
                click: function () {
                    $('#editForm').submit();
                }
            },
            {
                text: "Закрыть",
                "class": 'right',
                click: function () {
                    $(this).dialog('close');
                }
            }
        ]
    });
}
function handleResponse(status){
    if(status == 'ok'){
        useTopHoverTitle('Данные успешно сохранены');
    }else{
        alert('ошибка');
    }
}