var filter = {};
var serverLink = null;
var contentColumns = 1;
var afterServerQueryFunction = function(){};

/**
 * Функция вешает обработчики на пагинацию
 * @private
 */
_paginationHandler = function(){
    $(".page","#pagination").click(function(){
        var page = $(this).attr("page");
        _serverQuery(page);
    });
};

/*
 //Вешаем обработчики фильтра
 $(".btApply","#filter").click(function(){
 //Заполняем фильтр
 filter = {
 "Status" : $(".sState","#filter").val(),
 "Search" : $(".Search","#filter").val()
 };
 if($(".dtDateStart","#filter").val() != "")
 filter["DateStart"] = moment($(".dtDateStart","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
 if($(".dtDateEnd","#filter").val() != "")
 filter["DateEnd"] = moment($(".dtDateEnd","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
 //Отсылаем запрос на сервер
 _serverQuery(1);
 });
 */


/**
 * Функция обрабатывает запросы к серверу
 * @param page - страница
 * @private
 */
_serverQuery = function(page){
    $.ajax({
        type: 'POST',
        url: serverLink,
        data: {
            filter: filter,
            page: page
        },
        beforeSend: function(){
            //отображаем крутилку
            $("tbody.content").html("").append(
                $("<tr><td colspan='"+contentColumns+"'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
            ).fadeIn(500);
        },
        success: function(answer) {
            $("tbody.content .loading-img").fadeOut(500, function(){
                answer = $(answer)
                $("tbody.content").html("").append($(answer).find(".content > tr"));
                $("#pagination").html("").append($(answer).find(".pagination > span"));
                _paginationHandler();
                afterServerQueryFunction();
            })
        }
    });
};


/**
 * Обработчики фильтра и пагинации для /#backs
 */


///диалоговая форма "Принять заявление"
function DeclarationReviewDialog() {
   
    var dContent = $(
            '<div class="dialog-back-info">' +
            "<div style='text-align: center;margin-top:20px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>" +
            '</div>');
    //answer='<div>kjl jkjkj k</div>';
    var id = $('.active_appl');
    if (id.size()!=1){
        useTopHoverTitleError("Пожалуйста, выберите запись");
        return 0;
    }
    id = $('.active_appl').attr("id").split("_")[1];
    $(dContent).dialog({
        title: 'Заявление об отзыве',
        width: 640,
        height: 320,
        modal: true,
        close: function () {
            $(".dialog, .dialog-back-info").remove();
        },
        open: function () {
            //Отсылаем запрос на получение инфо по сертификату
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/declaration/declaration-review-dialog/" + id,
                success: function (answer) {
                    $(".loading-img", dContent).fadeOut(500, function () {
                        dContent.html("").append($(answer));
                        /*
                         //Вешаем обработчик на переключение вкладок
                         $(".tab", dContent).click(function(){
                         $(".tab", dContent).removeClass("active");
                         $(this).addClass("active");

                         $(".content-block",dContent).hide();
                         $(".content-block."+$(this).attr("tab-container"),dContent).show();
                         });
                         */
                        //Вешаем обработчик на открытие диалога с инфо по приложению
                        /*
                         $(".supplements tr", dContent).click(function(){
                         var id = $(this).attr("id").split("_")[1];
                         var title = $(this).attr("dialog-title");
                         _supplementDialog(id, title);
                         });
                         */
                    });
                },
                error: function () {
                    useTopHoverTitleError('Ошибка связи');
                    $(".dialog, .dialog-back-info").remove();
                }

            });
        }
    });

}
//обработчик тыка по табличке
function DeclarationReviewEvent() {    
    //вешаем обработчик открытия подробной инфо о сертификатах
    $("tr", ".ron-backs").click(function () {
        
        var id = $(this).attr("id").split("_")[1];        
        //var reason = $(this).find('.back-reason').text();
        //var organization = $(this).find('.back-organization').text();
        //    var title = $(this).attr("dialog-title");
        //var reason = $(this).find('.back-reason').value();
       // var fileName = $(this).find('a').attr('download');
        //var filePath = $(this).find('a').attr('href');
        //var appId = $(this).attr("appid");
        //var dialogData = {
        //    id: id,
        //    reason: reason,
        //    fileName: fileName,
        //    filePath: filePath,
        //    appId: appId
        //};
        DeclarationReviewDialog(id);
    });
}
//применяется обработчик тыка по табличке
//DeclarationReviewEvent();

//обработчик филтров и пагинации
handleResponse = function () {
    //Инициализируем фильтр
    filter = {
        "Type": $(".sType", "#filter").val(),
        "Status": $(".sState", "#filter").val(),
        "Search": $(".Search", "#filter").val()
    };

    if ($(".dtDateStart", "#filter").val() != "")
        filter["DateStart"] = moment($(".dtDateStart", "#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
    if ($(".dtDateEnd", "#filter").val() != "")
        filter["DateEnd"] = moment($(".dtDateEnd", "#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
    //Ссылка на скрипт сервера
    serverLink = "/ron/declaration/declaration-review-page";
    //Количество колонок в таблице
    contentColumns = 7;
    //Функция что будет запускаться после обновления контента

    afterServerQueryFunction = DeclarationReviewEvent;

    //Вешаем обработчики на календари
    $(".date-field", "#filter").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy",
    });
    $(".date-icon", "#filter").click(function () {
        $(this).parent().find("input").focus()
    })

    //Вешаем обработчики фильтра
    $(".btApply", "#filter").click(function () {
        //Заполняем фильтр
        filter = {
            "Type": $(".sType", "#filter").val(),
            "Status": $(".sState", "#filter").val(),
            "Search": $(".Search", "#filter").val()
        };
        if ($(".dtDateStart", "#filter").val() != "")
            filter["DateStart"] = moment($(".dtDateStart", "#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        if ($(".dtDateEnd", "#filter").val() != "")
            filter["DateEnd"] = moment($(".dtDateEnd", "#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });
    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаем обработчик открытия подробной инфо о сертификатах
    //$("tr",".ron-certificates").click(function(){
    //    var id = $(this).attr("id").split("_")[1];
    //    var title = $(this).attr("dialog-title");
    //    _certificateDialog(id, title);
    //});
}

//indexDeclarationReviewHandlers();
//
declarationReviewSend = function (id) {
    if (!confirm('Вы уверены, что хотите подтвердить заявление об отзыве?')) {
        return 0;
    }

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "/ron/declaration/declaration-review-send/" + id,
        success: function (answer) {
            if(answer.status!='Ok'){
                useTopHoverTitle(answer.status);
                return 0;
            }
            useTopHoverTitle('Данные записаны');
            $('#back_'+id).find('.back-conirm').text('Подтверждено');
            $('#back_'+id).find('.back-regnumber').text(answer['regnumber']);
            $(".dialog, .dialog-back-info").remove();

        },
        error: function () {
            useTopHoverTitleError('Ошибка связи');
            //$(".dialog, .dialog-back-info").remove();
        }

    });

};


$(document).ready(function () {
    //$("body").delegate("#filterForm .btApply", "click", function () {
    //    searchApplication(this);
   // });
    $('body').delegate('.row_appl', 'click', function () {
        $('.row_appl').removeClass('active_appl');
        $(this).addClass('active_appl');
    });
    $("body").delegate(".show_tab", "click", function () {
        var mes = $(this).attr('tab') - 1;
        $(".show_tab").removeClass('active');
        handleResponse(mes, 1);
    });
    $("body").delegate("#sendform1", "click", function () {
        if ($(this).attr('numb') == 1) {
            $(".loading-overlay").show();
            $("#submitFirstForm").click();
            //$("#edit_declaration"+$(this).attr('numb')).submit();
        } else {
            handleResponse($(this).attr('numb'), 1);
        }
    });
});
