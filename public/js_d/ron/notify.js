console.log('начало загрузки скриптов уведомлений');

/*
 *  блок скриптов кнопок уведомлений
 */

function notifyButtonClear(){
    console.log('Убираем все кнопки уведомлений');
    $('#btActions-Noticie').children().remove();
    jQuery('.notify-negativ-scan-add').hide();
    jQuery('.notify-denied-scan-add').hide();
    jQuery('.notify-positiv-scan-add').hide();
}

/**
 * Добавляем кнопку о движении учедомления
 *
 * @param {type} id
 * @param {type} caption
 * @param {type} functionName
 * @returns {undefined}
 */
function notifyButtonCreate(id,caption,functionName){
    $('#btActions-Noticie').append('\
                    <li class="filelink" id="'+id+'" >\
                        <div>\
                            <a class="" type="button" onclick="'+functionName+'();"  >\
                                <span class="text" style="width:100% ;max-width: 200px;">'+caption+'</span>\
                            </a>\
                        </div>\
                    </li>');
}

/**
 *
 * @returns {undefined}
 */
function notifyButtonSignPositivAdd() {

    //notifyButtonSignPositivDel();
    console.log('Добавляем кнопку подписаать уведобление о принятии');
    notifyButtonClear();
    notifyButtonCreateAdd();
     return 0;//временно
    notifyButtonCreate("notify-button-sign-positiv"
    ,'Уведомление подписано'
    ,'notifySign') ;
}
function notifyButtonSignPositivDel() {
    $('#notify-button-sign-positiv').remove();
}

function notifyButtonSignNegativAdd() {
    //notifyButtonSignNegativDel();
    notifyButtonClear();
    notifyButtonCreateAdd();
     return 0;//временно
    console.log('Добавляем кнопку подписаать уведобление о несоответстивии');
    notifyButtonCreate("notify-button-sign-negativ"
    ,'Уведомление подписано'
    ,'notifySign') ;
}

function notifyButtonSignNegativDel() {
    $('#notify-button-sign-negativ').remove();
}


function notifyButtonSignDeniedAdd() {

    //notifyButtonSignNegativDel();
    //notifyButtonSignPositivDel();
    notifyButtonClear();
    notifyButtonCreateAdd();
    return 0;//временно
    console.log('Добавляем кнопку подписаать уведобление об отказе');
    notifyButtonCreate("notify-button-sign-denied"
    ,'Уведомление подписано'
    ,'notifySign') ;
}

function notifyButtonSignDeniedADel() {
    $('#notify-button-sign-denied').remove();
}


/**
 * Добавляет кнопку "уведобленя" и вы падающую панель
 * отказать/несоответствия/принять
 * @returns {Number}
 */
function notifyButtonCreateAdd() {
    var step=jQuery('#now-step').val();
    var notifyCreateIs;
    if (step=='2'||step=='11'){
        if(jQuery('#notify-tab').is('.hide')){
                $('#btActions-Noticie').prepend('\
                    <li class="filelink" id="noticie_button">\
                        <div class="dropup">\
                            <a class="" type="button" data-toggle="dropdown">\
                                <?php //btn btn-primary dropdown-toggle\
                                ?>\
                                <span class="text">Уведомление о несоотв./отказ</span>\
                                <i class="caret"></i>\
                            </a>\
                            <ul class="dropdown-menu" style="display: none;    left: 150px;">\
                            <li><a class="noticie-item" href="javascript:;" onclick="createNotifyDenied();">Уведомление об отказе</a></li>\
                        <li><a class="noticie-item" href="javascript:;" onclick="createNotifyNegativ();">Уведомление о несоответствии</a></li>\
                            </ul>\
                        </div>\
                    </li>\
                    ')  ;
        }
            $('#noticie_button').click(function () {
                $(this).find('.dropdown-menu').show();
                //$(this).find('.noticie-item').addClass('noticie-item');
                //var widthItem = $(this).find('.dropdown-menu').outerWidth();
                var widthItem=250;
                $('.noticie-item').outerWidth(widthItem);
                $(this).find('.dropdown-menu').innerWidth(widthItem+20);
            });                   
                    //    jQuery('#btActions-Noticie').hide();
                    //}
    }else if (step=='3'||step=='11'){
        if(!jQuery('#noticie_button').length){                    
            $('#btActions-Noticie').append('\
                <li class="filelink" id="noticie_button">\
                    <div class="dropup">\
                        <a class="" type="button" onclick="createNotifyPositiv();"  href="javascript:;">\
                            <?php //btn btn-primary dropdown-toggle\
                            ?>\
                            <span class="text">Уведомление</span>\
                        </a>\
                    </div>\
                </li>\
                ')  ;
        }
    }else if (step=='7'||step=='11'){
    $('#btActions-Noticie').append('\
                    <li class="filelink" id="noticie_button">\
                        <div class="dropup">\
                            <a class="" type="button" onclick="createNotifyReturn();"  href="javascript:;">\
                                <?php //btn btn-primary dropdown-toggle\
                                ?>\
                                <span class="text">Уведомление</span>\
                            </a>\
                        </div>\
                    </li>\
                    ')  ;
    }else{
    return 0;
    }
}
function notifyButtonCreateDel() {
    $('#noticie_button').remove();
}

/**
 * Создаем новое уведомление
 * @param notifyType тип уведомления
 * positiv - о полложительно
 * negativ - отрицательно
 * @param doFunction - функция, которая будет выполнена в случае успешного внесения вы бд
 */
function createNotify(notifyType, doFunction) {
    if (!confirm('Вы уверен, что хотите создать новое уведомление?')) {
        return 0;
    }
    var url = "/ron/declaration/notify-create/" + notifyType + "/" + $('.active_appl').attr('id_appl');
    $.ajax({
        //async:false,
        url: url,
        dataType:'json',
        success: function (answer) {
            if (answer.result === "Ok") {
                //прописываем рег номер
                jQuery('.form-notify-number').val(answer.notifyNumber);
                doFunction();
            }
        },
        error: function () {
            useTopHoverTitleError("Ошибка связи");
        }
    });
}


createNotifyDeniedFunction = function () {
    //$('#notify-tab').addClass('hide');
    //showNotifyDeniedBags();
    $('#notify-tab').text('Причины отказа');
    showNotifyBags();
    //document.location = "/ron/declaration/notify-document/denied/" + $('.active_appl').attr('id_appl');
    notifyButtonClear();
    notifyButtonSignDeniedAdd();
    jQuery('.considiration').hide();
    jQuery(jQuery('.active_appl').children('td')[7]).text('Подготовка уведомления об отказе');
    // пригрузим кнопки
    getButtonByStep();
    notifyLoadReasons();
    $('#tab7 .btn-tabs-notyfy-print a').unbind('click');
    $('#tab7 .btn-tabs-notyfy-print a').click(notifyDeniedPrint);
    $('#tab7 .notify-reason-label').text("Причины отказа");    
    clearReason();
    //var win = window.open("/ron/declaration/export-data/positiv/" + $('.active_appl').attr('id_appl'));
};

function createNotifyDenied() {
    createNotify('denied', createNotifyDeniedFunction);
}

/*
 * Для позитивных уведомлений
 * Выполнится если данные внесены в базу данных
 */
createNotifyPositivFunction = function () {
    $('#notify-tab').addClass('hide');
    document.location = "/ron/declaration/notify-document/positiv/" + $('.active_appl').attr('id_appl');
    //notifyButtonSignNegativDel();
    //notifyButtonSignPositivAdd();
    //var win = window.open("/ron/declaration/export-data/positiv/" + $('.active_appl').attr('id_appl'));
    jQuery('.considiration').hide();
    jQuery(jQuery('.active_appl').children('td')[7]).text('Подготовка уведомления о приеме');
    // пригрузим кнопки
    getButtonByStep();

};

function createNotifyPositiv() {
    createNotify('positiv', createNotifyPositivFunction);
}

/*
 * Запускается, после создания уведомления об неправильностях
 */
createNotifyNegativFunction = function () {
    $('#notify-tab').text('Несоответствия');
    showNotifyBags();
    notifyButtonClear();
    notifyButtonCreateAdd();
    //notifyButtonSignPositivDel();
    notifyButtonSignNegativAdd();
    jQuery('.considiration').hide();
    $('#tab7 .btn-tabs-notyfy-print a').unbind('click');
    jQuery(jQuery('.active_appl').children('td')[7]).text('Подготовка уведомления о не соответствии');
    //console.log(jQuery(element).text());
    //element.append();
    $('#tab7 .btn-tabs-notyfy-print a').click(notifyNegativPrint);
    $('#tab7 .notify-reason-label').text("Причины несоответствия");
    getButtonByStep();
    notifyLoadReasons();
    clearReason();
};

function createNotifyNegativ() {
    createNotify('negativ', createNotifyNegativFunction);
}

createNotifyReturnFunction = function () {
    //showNotifyBags();
    console.log('уведомление о возрате создано');
    notifyButtonClear();
    notifyButtonCreateAdd();
    jQuery('.considiration').hide();
    jQuery(jQuery('.active_appl').children('td')[7]).text('Подготовка уведомления о возрате');
    document.location = "/ron/declaration/notify-document/return/" + $('.active_appl').attr('id_appl');
   // пригрузим кнопки
    getButtonByStep();
};

function createNotifyReturn() {
    console.log('начинаем создавать уведомление о возрате');
    createNotify('return', createNotifyReturnFunction);
    getButtonByStep()
}
/**
 *  метод вносит метку о подписи уведомления в базу
 *
 * @returns {Number} ничего
 */
function notifySign() {
    if (!confirm('Вы хотите внести данные о подписании уведомления?')) {
        return 0;
    }
    $.ajax({
        url: "/ron/declaration/notify-sign/" + $('.active_appl').attr('id_appl'),
        success: function (answer) {
            if (answer === 'Ok') {
                useTopHoverTitle('Данные внесены');
                $('#cboxClose').click();
            } else {
                useTopHoverTitleError('Возникла ошибка');
            }
        }
    });
}

/**
 * Обработчик при нажатии на кнопку таба "несоответствия"
 * показываем панельку "несоответствия"
 */
function showNotifyBags() {
    $(".show-panel").removeClass("show-panel");
    $("#tab" + $('#notify-tab').attr('tab') + "").addClass('show-panel');
    $('#notify-tab').removeClass('hide');
    $('#notify-tab').parent().parent().find('.active').removeClass('active');
    $('#notify-tab').addClass('active');
    $('#sendform1').attr('numb',7);//правим кнопку сохранить
    //$.ajax({
    //    url: "/ron/declaration/notify-comments/" + $('.active_appl').attr('id_appl'),
    //   success: function (answer) {
    //        $('#list_tabs').parent().append('<div id="tab' + count + '" class="tabs-panel notify-panel" >' + answer + '</div>');
    //   }
    //});
}


/**
 * Обработчик при нажатии на кнопку таба "отказано"
 * показываем панельку "отказано"
 */
function showNotifyDeniedBags() {
    $(".show-panel").removeClass("show-panel");
    $("#tab" + $('#notify-tab-denied').attr('tab') + "").addClass('show-panel');
    $('#notify-tab-denied').removeClass('hide');
    $('#notify-tab-denied').parent().parent().find('.active').removeClass('active');
    $('#notify-tab-denied').addClass('active');
    $('#sendform1').attr('numb',9);//правим кнопку сохранить
    //$.ajax({
    //    url: "/ron/declaration/notify-comments/" + $('.active_appl').attr('id_appl'),
    //   success: function (answer) {
    //        $('#list_tabs').parent().append('<div id="tab' + count + '" class="tabs-panel notify-panel" >' + answer + '</div>');
    //   }
    //});
}

/**
 *  конец блока скриптов кнопок уведомлений
 */

/**
 *   блок скриптов панели причин не соответствий
 */

/*
 * просто счетчик причин несоответствий
 * (чтоб ид отчичались)
 * @type Number
 */
var notifyReasonCount = 0;

/**
 *  Добавляем текстовое  поле причины
 *
 *   @param reasonNew - текст причины, если есть
 */
function notifyReasonAdd(reasonNew) {
    if (reasonNew === undefined) {
        reasonNew = '';
    }
    ++notifyReasonCount;
    var collection = $('.notify-reason');
    var notifyNum = collection.length + 1;
    $('#notify-form tbody').append('<tr class="notify-reason">\n\
                    <td clacc="title_form">\
                        Причина ' + notifyNum + '\
                    </td>\
                    <td style="padding-right: 0px;">\n\
                            <textarea  class="form-control notify-reason-item" type="text" name="reasons[' + notifyReasonCount + '][comment]"  placeholder="Введите причину" onchange="notifyReasonChange(this);" ntype="input"  rows="3">' + reasonNew + '</textarea>\
                    </td>\
                    <td width=30px style="padding: 0px;vertical-align: middle;text-align: center;width: 30px;">\
                            <span onclick="notifyReasonDel(this);" class="delete_direct_butt">\n\
                            </span>\
                    </td>\
                </tr>'
            );
    $('#notify-form tbody').children().last().find('input').focus();
}

/**
 *Тык по красному крестику возле поля причины
 *удаляем причину из списка
 *
 * @param {type} element -указательна элемент
 * @returns {Number} ничего
 */
function notifyReasonDel(element,isYes) {
    if (isYes===undefined){
        isYes=false;
    }
    if (!isYes && !confirm("Удалить причину?")) {
        return 0;
    }
    $(element).parents('.notify-reason').remove();
    //переименовываем заголовки причин
    var collection = $('.notify-reason');
    var l = collection.length;
    for (var i = 0; i < l; i++) {
        var td = $(collection[i]).find('td');
        $(td[0]).text('Причина ' + (i + 1));
    }
    //var $('notify-reason');
}


/**
 * хэш справочника причин на вид
 * Ид : Описоние
 *
 * @type type
 */
var notifyReasonReference = {};

/**
 * Добавляем причину из справочинка в виде выпадающего меню
 *
 * @param {type} reasonSelected - Ид причины
 * @returns {undefined}
 */
function notifyReasonAddFromReference(reasonSelected) {
    if (reasonSelected === undefined) {
        reasonSelected = '';
    }
    ++notifyReasonCount;
    var collection = $('.notify-reason');
    var notifyNum = collection.length + 1;
    var references;
    var notifyReasonReferencePut = '';
    //обозначаем выделеную, если она (предпологалось гасить, уже выбраную)
    for (var key in notifyReasonReference) {
        var selectIt = '';
        if (reasonSelected === key) {
            selectIt = 'selected';
        }
        notifyReasonReferencePut = notifyReasonReferencePut
                + '<option value="'
                + key
                + '" '
                + selectIt
                + ' >'
                + notifyReasonReference[key]
                + '</option>';
    }
    //добавляем элемент
    $('#notify-form tbody').append('<tr class="notify-reason">\n\
                    <td clacc="title_form">\
                        Причина ' + notifyNum +
            '</td>'
            + '<td style="padding-right: 0px;">'
            + '<select class="form-control notify-reason-item" name="reasons[' + notifyReasonCount + '][reference]" value="" placeholder="Введите причину" onchange="notifyReasonChange(this);" ntype="select">'
            + '<option value="">Выберите причину</option>'
            + notifyReasonReferencePut
            + '</select>\
                    </td>\
                    <td width=30px style="padding: 0px;vertical-align: middle;text-align: center;width: 30px;">\
                            <span onclick="notifyReasonDel(this);" class="delete_direct_butt">\n\
                            </span>\
                    </td>\
                </tr>'
            );
    //ставим фокус
    $('#notify-form tbody').children().last().find('select').focus();
}


/**
 * если в базе есть причины, покажем их
 * @returns {Number}
 */
function notifyReasonsPut(type) {
    //if (type == undefined){
    //    type = 'negativ';
    //}
    console.log("хотим вставить сохраненные причины ");
    setMaxShowTab(6);//разрешаем прыгать на табку
    var url = "/ron/declaration/notify-comments-get/"+ $('.active_appl').attr('id_appl');
    $.ajax({
        url: url,
        dataType: "json",
        success: function (answer) {
            console.log("причины загружены ");
            console.log(answer);
            for (var key in answer) {
                var element = answer[key];
                if (element['comment'] !== undefined) {
                    notifyReasonAdd(element['comment']);
                }
                if (element['reference'] !== undefined) {
                    notifyReasonAddFromReference(element['reference']);
                }
            }
            //заблокировать причины
            console.log("заблокировать причины");
            console.log(blockedNotifyComents);
            if (blockedNotifyComents!==0){
                jQuery(".notify-reason input").attr('readonly','readonly');
                jQuery(".notify-reason select").attr('disabled','disabled');
                jQuery(".notify-reason .del-button").parent().remove();
            }
            noyifyReasonAddIfEmpty();
        },
        error:function(e){
            console.log('Ошибка при получении причин из базы');
            console.log(e);
            noyifyReasonAddIfEmpty();
        }
    });

}

function noyifyReasonAddIfEmpty(){
    //если причин нет то добавим пустую
    ///отменяем
    //var len=$('.notify-reason').length;
    //if(len===0){
    //    notifyReasonAdd();
    //}
}

/**
 * загружаем справочник причин
 *
 */

function notifyLoadReasons() {
    $.ajax({
        url: "/ron/declaration/notify-comments-reference/" + $('.active_appl').attr('id_appl'),
        dataType: "json",
        success: function (answer) {
            console.log("стандартные причины");
            console.log(answer);
            notifyReasonReference = {};
            for (var key in answer) {
                notifyReasonReference[answer[key]['Id']] = answer[key]['Name'];
            }
            notifyReasonsPut();
        },
        error: function (e) {
            console.log('Ошибка при получини справочника причин из базы');
            console.log(e);
            //и всеравно выводим причины
            notifyReasonsPut();
        }
    });
}

/**
 * убираем метку ошибки с поля при его изменении
 *
 * @param {type} element - выбраный элемент
 * @returns {undefined}
 */
function notifyReasonChange(element) {
    if ($(element).val() !== '') {
        $(element).parent().removeClass('has-error');
    }

}

/**
 * Отправляем на сервер причины не соответствия и печатаем
 * @param doAfterSave - Функция, которая будет выполнена после сохранения
 * @returns {Number}
 */
function notifyCommentsSend(doAfterSave) {
    console.log('Хотим сохранит причины');
    //выборка
    var collection = $('.notify-reason-item');
    console.log('коллекция причин');
    console.log(collection);
    var sendData = {};
    var l = collection.length;
    //проверка на количество     
    if (l === 0) {
        if (!confirm('Не указано ни одной причины.\nПродолжить?')){
            return 0;
        }
    }
    // Проверка на пустые и формируем отправочный хешш
    var haveEmpty = 0;
    for (var i = 0; i < l; i++) {
        var element = $(collection[i]);
        console.log("смотрим элемент");
        console.log(element);
        var elementVal = element.val();
        console.log("значение элемента");
        console.log(elementVal);
        var itEmpty = false;
        //            element.removeClass('ui-state-error')
        element.parent().removeClass('has-error');
        if (elementVal === '') {
            //                element.addClass('ui-state-error');
            element.parent().addClass('has-error');
            haveEmpty = 1;
            itEmpty = true;
        }
        var elementName = element.attr('name');
        elementName = String(elementName);
        var elementNameSplit = elementName.split('[');
        var obj = {};
        var s2 = elementNameSplit[2];
        s2 = s2.substring(0, s2.length - 1);
        obj[s2] = elementVal;
        if(!itEmpty){
            sendData[i] = obj;
        }
    }
    
    if(l===0){
        //если нет причин то меточка
        sendData='No Reasons';
    }
    if (haveEmpty > 0) {
        if(!confirm('Некоторые поля с причинами пустые.\nПродолжить')){
            return 0;
        }
    }
    var leng = 0;
    for (var s in sendData) {
           leng++;
    }
    if(typeof(sendData)==='object' && leng===0){
        //если нет причин то меточка
        sendData='No Reasons';
    }
    console.log('Отсылаемые причины');
    console.log(sendData);

    var notifyNumber = jQuery('.form-notify-number').val();
    
    //упаковка
    var url = "/ron/declaration/notify-comments-put/" + $('.active_appl').attr('id_appl');   
    var data = {'reasons': sendData};    
    if(notifyNumber!==undefined){
        data['notifyNumber']=notifyNumber;
    }
    $.ajax({
        url: url,
        data: data,
        dataType: 'text',
        type: 'post',
        success: function (answer) {
            //console.log("ответ " + answer);
            if (answer == "Ok") {
                if (typeof(doAfterSave)==="function") {
                    doAfterSave();
                }
                //var win = window.open("/ron/declaration/export-data/negativ/" + $('.active_appl').attr('id_appl'));
            }else{
                useTopHoverTitleError("Ошибка при сохранении");
                console.log("Чего то не то при сохранении причин несоответствия ");
                console.log(answer);
            }
        },
        error: function (error) {
            console.log("Ошибка связи при " + url);
            console.log(error);
            useTopHoverTitleError("Ошибка связи");
        }
    });
}

notifyNegativPrint = function (){
    console.log("Хотим распечатать уведомление о несоответствие");
    var doPrintAfterSave=function(){
        document.location = "/ron/declaration/notify-document/negativ/" + $('.active_appl').attr('id_appl');
    };
     if (blockedNotifyComents!==0){
         doPrintAfterSave();
     } else {
           notifyCommentsSend(doPrintAfterSave);
     }
}


notifyDeniedPrint = function (){
    console.log("Хотим распечатать уведомление о откзе");    
    var doPrintAfterSave=function(){
        document.location = "/ron/declaration/notify-document/denied/" + $('.active_appl').attr('id_appl');
    };
     if (blockedNotifyComents!==0){
         doPrintAfterSave();
     } else {
           notifyCommentsSend(doPrintAfterSave);
     }
}

function clearReason(){
    jQuery('#tab7 .del-button').each(function(){
        notifyReasonDel(this,true);
    });
}
/**
 *   конец блока скриптов панели причин не соответствий
 */

console.log('конец загрузки скриптов уведомлений');



