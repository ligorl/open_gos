var filter = {};
var serverLink = null;
var contentColumns = 1;
var afterServerQueryFunction = function(){};

/**
 * Функция вешает обработчики на пагинацию
 * @private
 */
_paginationHandler = function(){
    $(".page","#pagination").unbind('click');
    $(".page","#pagination").click(function(){
        var page = $(this).attr("page");
        _serverQuery(page);
       //searchApplication(this);
    });
};

_paginationHandlerNoPage = function(){
    $(".page","#pagination").unbind('click');
    $(".page","#pagination").click(function(){
        var page = $(this).attr("page");
       // _serverQuery(page);
       searchApplication(this);
    });
};

/**
 * Функция обрабатывает запросы к серверу
 * @param page - страница
 * @private
 */
_serverQuery = function(page,active_id,order,sort){
    var filter ={};
    $("#filter #filterForm").serializeArray().map(function(x){filter[x.name] = x.value;});
    console.log('Отправляем фильтр');
    console.log(filter);
    $.ajax({
        type: 'POST',
        url: serverLink,
        data: {
            filter: filter,
            order: order,
            sort: sort,
            page: page
        },
        beforeSend: function(){
            //отображаем крутилку
            $("tbody.content").html("").append(
                $("<tr><td colspan='"+contentColumns+"'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
            ).fadeIn(500);
        },
        success: function(answer) {
            $("tbody.content .loading-img").fadeOut(500, function(){
                answer = $(answer);
                $("tbody.content").html("").append($(answer).find(".content > tr"));
                $("#pagination").html("").append($(answer).find(".pagination  > span"));
                if($(answer).find("#pagination  > span").length>0){
                    $("#pagination").html("").append($(answer).find("#pagination  > span"));
                }
                _paginationHandler();
                afterServerQueryFunction();
                if(active_id){
                    $('#cert_'+active_id).addClass('active_appl');
                }
            })
        }
    });
};

/**
 * Обработчики фильтра и пагинации для /expertise#expertise
 */
expertiseExpertiseHandlers = function(){
    //Инициализируем фильтр
    filter = {
       // "Status" : $(".sState","#filter").val(),
       // "Search" : $(".Search","#filter").val()
    };

    //Ссылка на скрипт сервера
    serverLink = "/ron/expertise/expertise-page";
    //Количество колонок в таблице
    contentColumns = 6;

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){

        //Отсылаем запрос на сервер
        //_serverQuery(1);
        searchApplication();
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    $('body').delegate('.row_appl', 'click', function () {
        $('.row_appl').removeClass('active_appl');
        $(this).addClass('active_appl');
    });
    $('body').delegate('.active_appl','click',function(){
        view_expertise();
    });
    $("#popup_win").delegate("#cboxClose","click",function(){
        console.log("Закрываем всплывающее окошко");
        $("#popup_win").hide();
    });

}

/**
 * Метод отрабатывает диалог с подробной информацие о сертификате
 * @param id
 * @param title
 * @private
 */
_certificateDialog =function(id, title){
    var dContent = $(
        '<div class="dialog-certificate-info">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "Свидетельство "+title,
        width: 800,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-certificate-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по сертификату
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/index/certificate-info/"+id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Вешаем обработчик на переключение вкладок
                        $(".tab", dContent).click(function(){
                            $(".tab", dContent).removeClass("active");
                            $(this).addClass("active");

                            $(".content-block",dContent).hide();
                            $(".content-block."+$(this).attr("tab-container"),dContent).show();
                        });
                        //Вешаем обработчик на открытие диалога с инфо по приложению
                        $(".supplements tr", dContent).click(function(){
                            var id = $(this).attr("id").split("_")[1];
                            var title = $(this).attr("dialog-title");
                            _supplementDialog(id, title);
                        });
                        //jQuery('#info-certificate-id').val(id);
                    });
                }
            });
        }
    });
}

/**
 * Метод отрабатывает диалог с подробной информацие о приложении сертификата сертификате
 * @param id
 * @param title
 * @private
 */
_supplementDialog = function(id, title){
    var dContent = $(
        '<div class="dialog-certsupplement-info">' +
            "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "Приложение "+title,
        width: 800,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-certsupplement-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по приложению
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/index/certificate-supplement-info/"+id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Вешаем обработчик на переключение вкладок
                        $(".tab", dContent).click(function(){
                            $(".tab", dContent).removeClass("active");
                            $(this).addClass("active");

                            $(".content-block",dContent).hide();
                            $(".content-block."+$(this).attr("tab-container"),dContent).show();
                        });
                    });
                }
            });
        }
    });
}

/**
 * Метод отрабатывает диалог с подробной информацие об организации
 * @param id
 * @param title
 * @private
 */
_orgDialog =function(id, title){
    var dContent = $(
        '<div class="dialog-org-info">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: title,
        width: 1100,
        height: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-org-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по сертификату
            $.ajax({
                type: 'POST',
                dataType: "html",
                url: "/ron/index/org-info/"+id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Вешаем обработчик на переключение вкладок
                        $(".tab", dContent).click(function(){
                            $(".tab", dContent).removeClass("active");
                            $(this).addClass("active");

                            $(".content-block",dContent).hide();
                            $(".content-block."+$(this).attr("tab-container"),dContent).show();

                            $(".buttons-block",dContent).hide();
                            $(".buttons-block."+$(this).attr("tab-container"),dContent).show();
                        });
                        //Вешаем обработчик на смену селекта с выбранным сертификатом
                        $(".CertificatesSelect", dContent).change(function(){
                            var idCert = $(".CertificatesSelect", dContent).val();
                            //Чистим контент, рисуем крутилку
                            $(".certificateInfo",dContent).html("").append(
                                $("<tr><td colspan='"+contentColumns+"'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
                            ).fadeIn(500);;
                            //Отсылаем запрос на отрисовку контента по другому сертификату
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/index/org-certificate-info/"+idCert,
                                success: function(answer) {
                                    $(".loading-img", dContent).fadeOut(500, function() {
                                        $(".certificateInfo",dContent).html("").append($(answer).find(".content > tr"));
                                    });
                                }
                            });
                        });
                        //Вешаем обработчик на смену селекта с выбранной лицензией
                        $(".licensesSelect", dContent).change(function(){
                            var idCert = $(".licensesSelect", dContent).val();
                            //Чистим контент, рисуем крутилку
                            $(".licenseInfo",dContent).html("").append(
                                $("<tr><td colspan='"+contentColumns+"'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")
                            ).fadeIn(500);;
                            //Отсылаем запрос на отрисовку контента по другому сертификату
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/index/org-license-info/"+idCert,
                                success: function(answer) {
                                    $(".loading-img", dContent).fadeOut(500, function() {
                                        $(".licenseInfo",dContent).html("").append($(answer).find(".content > tr"));
                                    });
                                }
                            });
                        });
                        //Вешаем обработчик на удаление созданного учредителя
                        $(".founders-list", dContent).delegate(".deleteFounder.added", "click", function(){
                            $(this).parent().remove();
                        });
                        //Вешаем обработчик на удаление существующих учредителей
                        $(".founders-list", dContent).delegate(".deleteFounder.current", "click", function(){
                            var parent = $(this).parent();
                            parent.find(".orgInfo").attr("name","founders.removed[]");
                            parent.removeClass("current").addClass("removed");
                            $(this).removeClass("current").addClass("removed");
                        });
                        //Вешаем обработчик на возврат помеченных на удаление учредителей
                        $(".founders-list", dContent).delegate(".deleteFounder.removed", "click", function(){
                            var parent = $(this).parent();
                            parent.find(".orgInfo").attr("name","founders.current[]");
                            parent.removeClass("removed").addClass("current");
                            $(this).removeClass("removed").addClass("current");
                        });

                        //Вешаем обработчик на добавление учредителя
                        $(".add-founder", dContent).click(function(){
                            //Показываем диалог
                            var fContent = ""+
                                '<div class="founders-dialog">'+
                                    '<div class="search-block">' +
                                        '<input class="search" placeholder="Поиск">'+
                                    '</div>'+
                                    '<div class="founders-list">'+
                                        "<div style='text-align: center;margin-top:20px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
                                    '</div>'+
                                    '<div class="buttons-block">'+
                                        '<div class="bt btOk">ОК</div>'+
                                        '<div class="bt btCancel">Отмена</div>'+
                                    '</div>'+
                                '</div>';
                            fContent = $(fContent);
                            fContent.dialog({
                                title: "Добавление учредителя",
                                width: 650,
                                height: 520,
                                modal: true,
                                close: function() {
                                    $(".founders-dialog").remove();
                                },
                                open: function(){
                                    $.ajax({
                                        type: 'POST',
                                        dataType: "html",
                                        url: "/ron/index/get-founders",
                                        success: function (answer) {
                                            $(".loading-img", fContent).fadeOut(500, function() {
                                                $(".founders-list",fContent).html("").append($(".founder-row",answer));
                                                //=============== Обработка клика по пункту
                                                $(".founder-row input[type=checkbox]", fContent).change(function(){
                                                    var parent = $(this).parent();
                                                    if(parent.hasClass("active"))
                                                        parent.removeClass("active");
                                                    else
                                                        parent.addClass("active");
                                                });
                                            });
                                            //=============== Обработка кнопки Отмена
                                            $(".btCancel", fContent).click(function(){
                                                $(".founders-dialog").remove();
                                            });
                                            //=============== Обработка поиска
                                            //добавляем в jquery регистронезависимый contains
                                            $.expr[":"].contains = $.expr.createPseudo(function(arg) {
                                                return function( elem ) {
                                                    arg = arg.toLowerCase();
                                                    var strText = $(elem).text().toLowerCase();
                                                    var valText = $(elem).val().toLowerCase();
                                                    if(
                                                        strText.toLowerCase().indexOf(arg) >=  0
                                                        || valText.toLowerCase().indexOf(arg) >= 0
                                                    )
                                                        return true;
                                                    else
                                                        return false;
                                                };
                                            });
                                            //Обработчик ввода текста в поиск
                                            $("input.search", fContent).keyup(function(e) {
                                                if(e.which == 27){
                                                    clearTimeout($.data(this, 'timer'));
                                                    clearSearch();
                                                    return;
                                                }
                                                clearTimeout($.data(this, 'timer'));
                                                var wait = setTimeout(search, 500);
                                                $(this).data('timer', wait);
                                            });
                                            //Функция поиска
                                            function search(){
                                                var searchVal = $("input.search", fContent).val();
                                                $(".founder-row", fContent).hide();
                                                $(".founder-row > label:contains("+searchVal+")",fContent).parents(".founder-row").show();
                                            }
                                            //функция отмены обработки поиска
                                            function clearSearch(){
                                                $("input.search", fContent).val("");
                                                $(".founder-row", fContent).show();
                                            }
                                            //обработчик кнопки ОК
                                            $(".btOk", fContent).click(function(){
                                                //собираем данные
                                                var founders = [];
                                                $(".founder-row.active", fContent).each(function(i, item){
                                                    //id
                                                    var id = $(item).find("input").attr("id").split("_")[1];
                                                    var text = $(item).find("label").text();
                                                    founders[id] = text;
                                                });
                                                //Дорисовываем строки в список учредителей
                                                var foundersList = $(".founders-list",dContent);
                                                for(var id in founders){
                                                    var fBlock = '<div class="founderBlock added" id="'+id+'">' +
                                                        '<input class="orgInfo" type="hidden" name="founders.added[]" value="'+id+'">'+
                                                        founders[id] +
                                                        '<span class="deleteFounder added ui-icon ui-icon-minusthick"></span>'+
                                                    '</div>';
                                                    foundersList.append(fBlock);
                                                }
                                                //======
                                                $(".founders-dialog").remove();
                                            });
                                        }
                                    })
                                }
                            })
                        });

                        //Вешаем обработчики на поля даты для публичных аккредитаций
                        $(".date-field", dContent).datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd.mm.yy"
                        });
                        $(".date-icon", dContent).click(function(){
                            $(this).parent().find("input").focus()
                        })

                        //Вешаем обработчик добавления нового блока публичной аккредитации
                        $(".pubaccr-block span",dContent).click(function(){
                            console.log( 'обработчик добавления нового блока публичной аккредитации 2' );
                            var dataType = $(this).attr("data-type");
                            console.log(dataType );
                            var tbodyClass = "";
                            switch(dataType){
                                case "addPubAccr":
                                    tbodyClass = "PubAccr";
                                    break;
                                case "addProfPubAccr":
                                    tbodyClass = "ProfPubAccr";
                                    break;
                                case "addIntPubAccr":
                                    tbodyClass = "IntPubAccr";
                                    break;
                            }
                            //Получаем набор блоков нужного нам класса
                            var tbodyBlocks = $("."+tbodyClass, dContent);
                            //клонируем первый
                            var clonedBlock = tbodyBlocks.first().clone();
                            clonedBlock.removeClass("firstTbody");
                            $("input, textarea",clonedBlock).val("");
                            //добавляем клонируемый блок в список
                            tbodyBlocks.last().after(clonedBlock);
                            //вешаем обработчик для выбора даты

                            $(".date-field", clonedBlock).removeClass("hasDatepicker");
                            $(".date-field", clonedBlock).removeAttr("id");
                            $(".date-field", clonedBlock).datepicker({
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: "dd.mm.yy"
                            });
                            $(".date-icon", clonedBlock).click(function(){
                                $(this).parent().find("input").focus();
                            })
                            //ставим фокус на новый блок
                            $("textarea",clonedBlock).focus();
                        });

                        //Вешаем обработчик на кнопку сохранения организации
                        $(".buttons-container .btSave", dContent).click(function(){
                            var button = $(this);
                            //Если идет отправка, не отрабатываем
                            if(button.prop("disabled"))
                                return;

                            //Чистим пустые блоки аккредитаций
                            removeEmptyBlocks("PubAccr");
                            removeEmptyBlocks("ProfPubAccr");
                            removeEmptyBlocks("IntPubAccr");

                            //Собираем данные с полей
                            var fieldsData = {};
                            $(".content-block.org .orgInfo", dContent).each(function(i, item){
                                var name = $(item).attr("name");
                                if(name.split(".").length>1){
                                    var parts = name.split(".");
                                    var level = fieldsData;
                                    for(var i = 0; i < parts.length-1; i++){
                                        part = parts[i];
                                        if(!level[part])
                                            level[part] = {};
                                        level = level[part];
                                    }
                                    var lastPart = parts[parts.length-1];
                                    if(lastPart.length>2 && lastPart.substring(lastPart.length-2,lastPart.length) == "[]"){
                                        lastPart = lastPart.substring(0,lastPart.length-2);;
                                        if(!level[lastPart])
                                            level[lastPart] = []
                                        level[lastPart].push($(item).val());
                                    } else {
                                        level[lastPart] = $(item).val();
                                    }
                                } else {
                                    fieldsData[name] = $(item).val();
                                }
                            });

                            fieldsData["orgData"] = {};
                            fieldsData["orgData"]["PubAccr"] = getAccrData("PubAccr");
                            fieldsData["orgData"]["ProfPubAccr"] = getAccrData("ProfPubAccr");
                            fieldsData["orgData"]["IntPubAccr"] = getAccrData("IntPubAccr");

                            //Чтобы не было повторной отправки
                            button.prop("disabled",true);
                            button.addClass("disabled");

                            var sendData = {
                                fieldsData : fieldsData
                            };
                            console.log(sendData);


                            $.post(
                                "/ron/index/save-org-info",
                                sendData,
                                function(data){
                                    button.prop("disabled",false);
                                    button.removeClass("disabled");
                                    //Отрабатываем добавление и удаление учредителей
                                    $(".founderBlock.removed", dContent).remove();
                                    $(".founderBlock.added .deleteFounder.added", dContent).removeClass("added").addClass("current");
                                    $(".founderBlock.added", dContent).removeClass("added").addClass("current");
                                }
                            )
                        });

                        //функция, что удаляет пустые блоки для ввода данных
                        //по общественным аккредитациям
                        var removeEmptyBlocks = function(className){
                            //Получаем набор блоков нужного нам класса
                            var tbodyBlocks = $("."+className,dContent);
                            //Проходим по всем найденным блокам
                            tbodyBlocks.each(function(i,item){
                                //Проверяем если это первый блок
                                //то пропускаем
                                if($(item).hasClass("firstTbody"))
                                    return true;
                                //Проверяем, если значения полей пустые
                                if( $("textarea",item).val() == "" && $("input",item).val()=="")
                                    $(item).remove();
                            })
                        }

                        //Функция проходит по всем блокам и собирает массив данных
                        //для возврата
                        var getAccrData = function(className){
                            var returnArray = [];
                            //Получаем набор блоков нужного нам класса
                            var tbodyBlocks = $("."+className, dContent);
                            //Проходим по всем найденным блокам
                            tbodyBlocks.each(function(i,item){
                                returnArray.push({
                                    organization: $.trim($("textarea",item).val()),
                                    date: $.trim($("input.date-field",item).val()),
                                    requisite: $.trim($("input.requisite-field",item).val())
                                })
                            });
                            return returnArray;
                        }
                    });
                }
            });
        }
    });
}

/**
 * Обработчики фильтра и пагинации для /#programs
 */
indexProgramsHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "ugsSelect" : $(".ugsSelect","#filter").val(),
        "eduLevelSelect": $(".eduLevelSelect","#filter").val(),
        "Search" : $(".Search","#filter").val()
    };
    //Ссылка на скрипт сервера
    serverLink = "/ron/index/programs-page";
    //Количество колонок в таблице
    contentColumns = 7;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){};

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "ugsSelect" : $(".ugsSelect","#filter").val(),
            "eduLevelSelect": $(".eduLevelSelect","#filter").val(),
            "Search" : $(".Search","#filter").val()
        };
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //Вешаем обработчик на нажатие Enter на поиск
    $(".Search","#filter").keypress(function(e){
        if(e.keyCode==13){
            $(".btApply","#filter").click();
        }
    });
}

/**
 * Обработчики фильтра и пагинации для /#orders
 */
indexOrdersHandlers = function(){
    //Инициализируем фильтр
    filter = {
        "Type" : $(".Type","#filter").val(),
        "Kind" : $(".Kind","#filter").val()
    };
    if($(".dtDateStart","#filter").val() != "")
        filter["DateStart"] = moment($(".dtDateStart","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
    if($(".dtDateEnd","#filter").val() != "")
        filter["DateEnd"] = moment($(".dtDateEnd","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");

    //Ссылка на скрипт сервера
    serverLink = "/ron/index/orders-page";
    //Количество колонок в таблице
    contentColumns = 5;
    //Функция что будет запускаться после обновления контента
    afterServerQueryFunction = function(){

    }

    //Вешаем обработчики на календари
    $(".date-field", "#filter").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy"
    });
    $(".date-icon", "#filter").click(function(){
        $(this).parent().find("input").focus()
    })

    //Вешаем обработчики фильтра
    $(".btApply","#filter").click(function(){
        //Заполняем фильтр
        filter = {
            "Type" : $(".Type","#filter").val(),
            "Kind" : $(".Kind","#filter").val()
        };
        if($(".dtDateStart","#filter").val() != "")
            filter["DateStart"] = moment($(".dtDateStart","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        if($(".dtDateEnd","#filter").val() != "")
            filter["DateEnd"] = moment($(".dtDateEnd","#filter").val(), "DD.MM.YYYY").format("YYYY-MM-DD");
        //Отсылаем запрос на сервер
        _serverQuery(1);
    });

    //вешаем обработчики пагинации
    _paginationHandler();

    //вешаемо обработчик на смену типа
    $(".Type","#filter").change(function(){
        if($(this).val()!=""){
            $(".Kind option","#filter").hide();
            $(".Kind option[typeid='"+$(this).val()+"']","#filter").show();
            $(".Kind option[value='']","#filter").show();
            $(".Kind","#filter").val('');
        } else {
            $(".Kind option","#filter").show();
        }
    });
}

function view_expertise() {
    if ($('.row_appl').is('.active_appl')) {
        $.ajax({
            url: "/ron/expertise/view-expertise/"+ $('.row_appl.active_appl').attr("id").split("_")[1],
            success: function (answer) {
                $("#popup_win").html(answer);
                $("#popup_win").show();
                resizeContent($(".container .viewzone"));
                $("#but_wrap_child_left").hide();
                $(/*'#but_wrap_child_left*/ '#btActions-Noticie ').parent().parent().show();
                $("#fields_form input.disabled").attr('disabled', "disabled");
                $(".input-date").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy",
                });
            }
        });
    } else {
        useTopHoverTitleError("Пожалуйста, выберите запись");
    }
}


function add_comment_expert(expert_id){
    var dContent = $(
        '<div class="dialog-add-comment">' +
            "<div style='text-align: center;'><div class='loading-img'><img src='/img/loading.gif' /></div><div class='leftSideBar'></div><div class='rightSideBar'></div></div>"+
        '</div>');

         $(dContent).dialog({
            title: "Новое примечание",
            width: 600,
            height: 400,
            modal: true,
            close: function() {
                $(".dialog-add-comment").remove();
            },
            open: function() {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/ron/expertise/get-comment-form/"+ expert_id,
                    success: function(answer) {

                        $(".loading-img", dContent).fadeOut(500, function() {
                            $(dContent).html(answer);
                        });

                    },
                    error:function(e){
                        console.log('ошибка при загрузке панели добавления распорядительного документа');
                        console.log(e);
                    }

                });
            }
    });
}

function save_expert_comment(){
    var form = "#form_expert_comment",
        comment = $('#comment-expert', form).val();
        comment = $.trim(comment);
    if(comment == "") {
       $('#comment-expert', form).addClass("color_pink");
    }else{
        $.ajax({
            type: 'POST',
            data: $(form).serialize(),
            url: "/ron/expertise/set-comment-form",
            success: function(answer) {

               $('.comment-expert').show();
               $('.comment-text').html(comment);
               $('.dialog-add-comment').dialog("close");
            },
            error:function(e){
                console.log('ошибка при загрузке панели добавления распорядительного документа');
                console.log(e);
            }

        });
    }

}
function changeTab(elem){
    $('.tab_exp').hide();
    $('.show_tab').removeClass('active');
    $(elem).addClass('active');
    $('.addCertButton').hide();
    $(".expertButton").hide();
    if($(elem).attr('tab')=='wrap_accreditation'){
        var idcert = $('.active_appl').attr('id');
        $.ajax({
            url: "/ron/expertise/get-certificate-expert/"+idcert.split("_")[1],
            success: function(data){
               $("#wrap_accreditation").html(data);
               $("#wrap_accreditation").show();
                $('.addCertButton').show();
                $(".saveButExpert").hide();
            }
        });
    }else if($(elem).attr('tab')=='wrap_expertise'){
        var idexp = $('.active_appl').attr('id');
        $.ajax({
            url: "/ron/expertise/get-expertise-expert/"+idexp.split("_")[1],
            success: function(data){
               $("#wrap_expertise").html(data);
               $("#wrap_expertise").show();
                $(".saveButExpert").hide();
            }
        });
    }else if($(elem).attr('tab')=='wrap_child_parent'){
        var idorg = $('.active_appl').attr('id');
        $.ajax({
            url: "/ron/expertise/get-expert-from-organization/"+idorg.split("_")[1],
            success: function(data){
               $("#wrap_child_parent").html(data);
               $("#wrap_child_parent").show();
               $(".expertButton").show();
               $(".saveButExpert").hide();
            }
        });
    }else{
        var idexp = $('.active_appl').attr('id');
        $.ajax({
            url: "/ron/expertise/get-card-expert/"+idexp.split("_")[1],
            data:{first:1},
            success: function(data){
                $("#information-form").html(data);
                $("#information-form").show();
                $(".saveButExpert").show();
                $('.choos').chosen();
            }
        });
    }

}
function changeDate(el){
    if($('.order_type').val()=='1'){
        var date = new Date(el.datepicker('getDate'));
        date.setFullYear(date.getFullYear() + 3);
        $('input[name="date_end"]').datepicker('setDate',date);
    }
}
function changeType(){
    if($('.order_type').val()=='1'){
        $('.orders,.spec_but').attr('disabled',false);
        $('select.orders').attr('disabled', false).trigger('chosen:updated');
    }else{
        $('.orders,.spec_but').attr('disabled',true);
        $('select.orders').attr('disabled', true).trigger('chosen:updated');
    }
}
function expert_org(el){
    $('.addExpert').hide();
    $('.addOrg').hide();
    if(el.is(':checked')){
        $('.addOrg').show();
    }else{
        $('.addExpert').show();
    }
    $('.lvl_access').hide();
}
function changeSel(){
    var level_id = $('.lvl').val();
    if(level_id != '')
        $.post('/ron/expertise/get-ugs',{level_id:level_id},function(data){
            $('.ugs').parent().empty().append(data);
            $(".ugs").chosen();
        })

}
function changeSel2(){
    var level_id = $('.lvl').val();
    var group_id = $('.ugs').val();
    if(level_id != '' && group_id != '')
        $.post('/ron/expertise/get-specialty-dictionary',{level_id:level_id,group_id:group_id},function(data){
            $('.programm').parent().empty().append(data);
            $(".programm").chosen();
        })

}

function addCertificate(){
     var dContent = $(
        '<div class="dialog-view-addcert">' +
            "<div style='text-align: center;'><div class='loading-img'><img src='/img/loading.gif' /></div><div class='leftSideBar'></div><div class='rightSideBar'></div></div>"+
        '</div>');

         $(dContent).dialog({
            title: "Аккредитация эксперта",
            width: 1000,
            height: 600,
            modal: true,
            close: function() {
                $(".dialog, .dialog-view-addcert").remove();
            },
             dialogClass: "scroll_off",
             buttons: [
                 {
                     text: "Сохранить",
                     click: function() {
                         sendCertificate();
                     }
                 }
             ],
            open: function() {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/ron/expertise/add-certificate-expert/"+ $('.row_appl.active_appl').attr("id").split("_")[1],
                    success: function(answer) {

                        $(".loading-img", dContent).fadeOut(500, function() {
                            $(dContent).html(answer);
                            $(".lvl").chosen();
                            $(".ugs").chosen();
                            $(".programm").chosen();
                        });


                    },
                    error:function(e){
                        console.log('ошибка при выполнении скрипта');
                        console.log(e);
                    }

                });
            }
    });
}

function addSpeciality(){
    var lvl = $('.lvl').val();
    var ugs = $('.ugs').val();
    /*var programm = $('.programm').val();*/
    if(lvl!=''&&ugs!=''){
        /*if(programm!=''){
            if($('.dictionary table tbody tr#'+programm).length){
         useTopHoverTitleError('Вы уже добавили данную программу!');
                return false;
            }
        }*/
        if(ugs!=''){
            if($('.dictionary table tbody tr#'+ugs+'_'+lvl).length){
                useTopHoverTitleError('Вы уже добавили данный УГСН!');
                return false;
            }
        }
        var code = $('.ugs option[value="'+ugs+'"]').attr('code');
        var search_code = code!=''?code+' - ':'';
        var str = '<tr id="'+ugs+'_'+lvl+'">' +/* id="'+programm+'"*/
            '<td><input type="hidden" name="Program['+counter+'][speciality_group_id]" value="'+ugs+'">'+code+'</td>' +
            '<td>'+$('.ugs option[value="'+ugs+'"]').text().replace(search_code,'')+'</td>' +
            '<td><input type="hidden" name="Program['+counter+'][education_level_id]" value="'+lvl+'">'+$('.lvl option[value="'+lvl+'"]').text()+'</td>';
        /*if(programm!=''){
            var str = str + '<td><input type="hidden" name="Program['+counter+'][speciality_id]" value="'+programm+'">'+$('.programm option[value="'+programm+'"]').text()+'</td>';
        }else{
            var str = str + '<td><input type="hidden" name="Program['+counter+'][speciality_id]" value="0">-</td>';
        }*/
        var str = str + '</tr>';
        $('.dictionary table tbody').append(str);
        counter++;
    }
    return false;
}
function sendCertificate(){
    var err=0;
    $(".requisite-field").each(function(indx, element){
       if($(element).val()=='' && !$(element).is(':disabled')){
           err=1;
       }
    });
    if(err==1){
        useTopHoverTitleError('Все поля обязательны для заполнения!');
        return false;
    }
    $.ajax({
        type: 'POST',
        data:$('#certForm').serialize(),
        url: "/ron/expertise/save-certificate-expert",
        success: function(answer) {
            $(".dialog, .dialog-view-addcert").remove();
            $("a[tab='wrap_accreditation']").trigger('click');
        }
    });
}

function showPop(url) {
    url=encodeURI(url);//персонально для ИЕ
    //$.ajax({
    //    type: 'POST',
    //    url: url,
    //    success: function (answer) {
    //        $("#popupWindow").html(answer);
    //        $("#popupWindow").show();
    //    }
    //});


     $(".dialog, .dialog-add-edu-org").remove();
     var dContent = $(
        '<div class="dialog-add-edu-org">' +
            "<div style='text-align: center;'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');

         $(dContent).dialog({
            title: "Введите название организации",
            width: 800,
            height: 510,
            modal: true,
            close: function() {
                $(".dialog, .dialog-add-edu-org").remove();
            },
            open: function() {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: url,
                    success: function(answer) {

                        $(".loading-img", dContent).fadeOut(500, function() {
                            $(dContent).html(answer);
                            $(".lvl").chosen();
                            $(".ugs").chosen();
                            $(".programm").chosen();
                        });
                    },
                    error:function(e){
                        console.log('ошибка при выполнении скрипта');
                        console.log(e);
                    }
                });
            }
    });

}

    function addOrgWork(area, now){
        if(!now){now = 0;}
        jQuery('.itOrgInDo').removeClass('itOrgInDo');
        var toAdd = jQuery(area).parents('.org-do-to-add').find('.org-do-to-add-items');
        jQuery(toAdd).addClass('itOrgInDo');
        jQuery(toAdd).append('');
        showPop('/ron/expertise/get-edu-org/0/0/'+now);
    }

    function delOrgWork(element, now){
        if (confirm("Хотите удалить эту образовательную организацию")){
            jQuery(element).parents('.org-do-to-add-item-one').remove();
            if (now == 1){
                $('.hide_add').show();
            }
        }
        return false;
    }


/**
 * Вешаем обработчик на кнопки поиска
 *
 * @param {type} param1
 * @param {type} param2
 */
    _filterHandler = function(){
        $("body").undelegate("#appSearchInput", "keyup");
        $("body").delegate("#appSearchInput", "keyup", function (e) {
            if(e.which==13){
                searchApplication();
                //if(e.stopPropagation) e.stopPropagation();
                //if(e.preventDefault) e.preventDefault();
            }
        });
        $(".Search","#filter").change(function(){
            if(jQuery(this).val()===''){
                searchApplication();
            }
        });
    }

    /**
     * делаем поиск и пагинацию
     *
     * @param {type} _this - элемент кнопки пагинации
     * @returns {undefined}
     */
        function searchApplication(_this) {
            var page = $(_this).attr('page');
            if (page === undefined) {
                page = 0;
            }
            var event = $(".btn-warning").attr('event') + '/' + page;
            var keep_selector = $(".btn-warning").attr('controller');
            var destination_link = '/ron/' + keep_selector + '/' + event;
            $.ajax({
                type: 'POST',
                url: destination_link,
                data: $("#filterForm").serialize(),
                beforeSend: function () {
                    $(".content_td .intable_content").html("").append($("<tr><td colspan='6'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")).fadeIn(500);
                },
                success: function (answer) {
                    //$(".content_td .intable_content").fadeOut(500, function () {
                        //answer = $(answer);
                        $(".content_td .content").html("").append(answer);
                        resizeContent($(".viewzone", answer));
                        //Запускаем обработчики
                        var handlerName = keep_selector + event.charAt(0).toUpperCase() + event.substr(1) + "Handlers";
                        if (window[handlerName])
                            window[handlerName]();
                    //});
                    //вешаем обработчики
                    _paginationHandlerNoPage();
                    _filterHandler();
                    //вешаем фокус
                    setTimeout(function() { $('#appSearchInput').focus(); }, 200);
                    var el=document.getElementById('appSearchInput');
                    el.focus();
                    el.setSelectionRange(el.value.length,el.value.length);
                    el.scrollIntoView();
                }
            });
        }



////-----------------
//Вкладка состав комисиии
//-----------------------


    function commissionFilterSelectedAction(){
        //Вешаем обработчик фильтрам
        jQuery('.commission-filter-selected').change(function(){
            var itId = jQuery(':selected', this).val();
            var itFilter = jQuery(':selected', this).parent().attr('filtred-by');
            var orgId = jQuery('.leftSideBar .branch-tab.selected').attr('branch-id');
            jQuery('.tab-tbody.tabcontent-'+orgId+' tr').addClass(itFilter+'-hide')
            if (itId == ''){
                //вывести все
                jQuery('.tab-tbody.tabcontent-'+orgId+' tr').removeClass(itFilter+'-hide');
            }else{
                //вывести только те что надо
                jQuery('.tab-tbody.tabcontent-'+orgId+' tr').find('['+itFilter+'='+itId+']').parents('.commission-one-record').removeClass(itFilter+'-hide');
            }
            jQuery('tr.commission-one-record').removeClass('select');
        });
    }
    //обработчик коментария
    //jQuery('.commission-table-comment-button').click(function(){
    function commissionButtonCommentAction(){
        jQuery('.commission-button-comment').click(function(){
            var expertiseId = jQuery('#edit_declaration1 input[name="id"]').val();

            var expertId = jQuery(this).parents('.commission-one-record').attr('expert_id');
            var commentValue = jQuery(this).parents('.commission-one-record').find('td .commission-table-comment-value').text();

            if( expertId === undefined){
                expertId = jQuery('.commission-one-record.select').attr('expert_id');
                commentValue = jQuery('.commission-one-record.select').find('td .commission-table-comment-value').text();
            }
            console.log(expertId);

            //console.log(jQuery(this).parents('.commission-one-record').find('td .commission-table-comment-value'));
            //return 0;

            if (expertId===undefined || expertId=='' || expertId==0){
                useTopHoverTitleError("Не выбрана программа");
                return 0;
            }

            var dContent = $(
                '<div class="commission-comment-dialog">'
                //"<div style='text-align: center;margin-top:40px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
                + '<textarea style="width:100%" autofocus="autofocus" rows="5">'+commentValue+'</textarea>'
                //+ '<input value="'+commentValue+'">'
                + '<div class="commission-comment-dialog-button-block">'
                + '<div class="commission-button ok-save-comment">Ок</div>'
                + '<div class="commission-button cancel">Отмена</div>'
                + '</div>'
                + '</div>'
                );

            $(dContent).dialog({
                    title: "Комментарий",
                    width: 640,
                    height: 240,
                    modal: true,
                    close: function() {
                        $(".dialog, .commission-comment-dialog").remove();
                    },
                    open: function() {

                        jQuery('.commission-comment-dialog .cancel').click(function(){
                            $(".dialog, .commission-comment-dialog").remove();
                        });
                        jQuery('.commission-comment-dialog .ok-save-comment').click(function(){
                            commentValue = jQuery('.commission-comment-dialog textarea').val();
                            saveComentToContacting(expertiseId,expertId,commentValue,undefined,"afterSaveComentToContacting(expertId, comment)"/*'afterSaveComentToContacting("'+expertId+'", "'+commentValue+'")'*/);
                        });

                    }
            });
        });
    };


    function saveComentToContacting(expertiseId,expertId,comment,contactDate,afterFunction){
        //var comment = jQuery('.commission-comment-dialog input').val();
         console.log('Сохраняем комментарии об результаты эксперта');
         console.log(expertiseId);
         console.log(expertId);
         console.log(comment);
         console.log(afterFunction);
         //return 0;
        var data={};
        data['expertise_id'] = expertiseId;
        data['expert_id'] = expertId;
        if(comment !== undefined){
            data['description'] = comment;
        }
        if(contactDate !== undefined){
            data['contact_timestamp'] = contactDate;
        }
                $.ajax({
                    type: 'POST',
                    //timeout: 5000,
                    //dataType: "html",
                    data: data,
                    url: "/ron/expertise/save-contacting",
                    success: function(answer) {
                        if(answer == 'Ok'){
                            useTopHoverTitle('Данные записаны');
                            if(typeof(afterFunction) == 'string'){
                                eval(afterFunction);
                            }
                            if(typeof(afterFunction) == 'function'){
                                afterFunction();
                            }
                        }else{
                            console.log('Ошибка при записи добавлении коментария');
                            console.log(answer);
                            useTopHoverTitleError('Ошибка при записи');
                        }
                       // console.log(answer);
                       //  $(".loading-img", dContent).fadeOut(500, function() {
                       //      dContent.html("").append($(answer));
                       //      $(".date-calendar").html('Дата документа');
                       //      //$("#noticie_button-add a").attr('onclick','addNewOrderDocumentFromDeclaration()');
                       //  });

                    },
                    error:function(e){
                        console.log('ошибка при загрузке панели добавления распорядительного документа');
                        console.log(e);
                        useTopHoverTitleError('Ошибка связи');
                    }
                });
     }

     function afterSaveComentToContacting(expertId,comment){
        var orgId = jQuery('.leftSideBar .branch-tab.selected').attr('branch-id');
        jQuery('.tab-tbody.tabcontent-'+orgId+' tr[expert_id="'+expertId+'"] td .commission-table-comment-value').text(comment);
        $(".dialog, .commission-comment-dialog").remove()
     }

    function expertAgreementAction(it){
        var expertiseId = jQuery('#edit_declaration1 input[name="id"]').val();
        var expertId = jQuery(it).parents('.commission-one-record').attr('expert_id');
        var orgId = jQuery('.leftSideBar .branch-tab.selected').attr('branch-id');
         console.log(expertiseId);
         console.log(expertId);
         console.log(orgId);
       // window.open('/ron/expertise/expertise-expert-consent-document/'+expertiseId+'/'+expertId+'/'+orgId, '_blank');
        commissionButtonSend(expertiseId,expertId) ;
    }

    /**
     * обработчик тык по табличке
     * @returns {undefined}
     */
    function commissionOneRecordAction(){
        jQuery('tr.commission-one-record td').click(function(){
            jQuery('tr.commission-one-record.select').removeClass('select');
            console.log(jQuery(this).parent());
            jQuery(this).parent().addClass('select');
            //Запомник куда тык
            selectedProgramIdMemory = jQuery(this).parent().attr('pogram_id');
            selectedExpertIdMemory = jQuery(this).parent().attr('expert_id');
            console.log(selectedProgramIdMemory);
            console.log(selectedExpertIdMemory);

        });
    }

    /**
     * Обработчик переключения вкладок
     * @param {type} param
     */
    function tabCommisionAction(){
        $("#tab-commision .branch-tab").click(function(){
            $(".branch-tab").removeClass("selected");
            $(this).addClass("selected");

            var branchId = $(this).attr("branch-id");
            $(".tab-tbody").hide();
            $(".tab-tbody.tabcontent-"+branchId).show();

            $(".headName").hide();
            $(".headName.b_"+branchId).show();

            //запоминаем где
            selectedOrganisationIdMemory = jQuery(this).attr('branch-id');
            selectedProgramIdMemory = undefined;
            selectedExpertIdMemory = undefined;

            jQuery('tr.commission-one-record.select').removeClass('select');
        });
    }

    /**
     * Тык по кнопке Формуляр / Отправлено
     *Подгружаем диалоговое окно Согласие
     * @param {type} param
     */
    function commissionButtonSendAction(){
        jQuery('.commission-button-send').click(function(){
            var expertiseId = jQuery('#edit_declaration1 input[name="id"]').val();
            var expertId = jQuery('.commission-one-record.select').attr('expert_id');
            commissionButtonSend(expertiseId,expertId) ;
        });
    }

    function commissionButtonSend(expertiseId,expertId){
        //var expertiseId = jQuery('#edit_declaration1 input[name="id"]').val();
        //var expertId = jQuery('.commission-one-record.select').attr('expert_id');
        //var commentValue = jQuery('.commission-one-record.select').find('td .commission-table-comment-value').text();
        var orgId = jQuery('.leftSideBar .branch-tab.selected').attr('branch-id');


        console.log('диалог комментарии об результаты эксперта');
        console.log(expertiseId);
        console.log(expertId);
        //console.log(jQuery(this).parents('.commission-one-record').find('td .commission-table-comment-value'));
        //return 0;

        if (expertId===undefined || expertId=='' || expertId==0){
            useTopHoverTitleError("Не выбрана программа");
            return 0;
        }

        var date = new Date();
        var mount=(date.getMonth()+1);
        if (mount<10){
            mount = '0'+mount;
        }
        var curDatePrint = date.getDate()+'.'+mount+'.'+date.getFullYear();

        var dContent = $(
                '<div class="commission-consent-dialog">'
                + '</div>'
            );

        $(dContent).dialog({
                title: "Согласование с экспертом",
                width: 640,
                height: 280,
                modal: true,
                close: function() {
                    $(".dialog, .commission-consent-dialog").remove();
                },
                open: function() {
                    $.ajax({
                        type: 'POST',
                        //timeout: 5000,
                        //dataType: "html",
                        //data: data,
                        url: "/ron/expertise/get-expertise-consent-contacting-dialog/"+expertiseId+'/'+expertId+'/'+orgId,
                        success: function(answer) {
                            jQuery(dContent).append(answer);
                            jQuery('',answer)
                        },
                        error:function(e){
                            console.log('ошибка при загрузке панели добавления  документа');
                            console.log(e);
                            useTopHoverTitleError('Ошибка связи');
                        }

                    });
                    //Вешаем обработчики на календари
                    $(".commission-comment-dialog-date", ".commission-comment-dialog").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: "dd.mm.yy",
                    });
                    $(".date-icon", ".dialog-certificate-edit").click(function () {
                        $(this).parent().find("input").focus()
                    })
                    jQuery('.commission-comment-dialog .cancel').click(function(){
                        $(".dialog, .commission-comment-dialog").remove();
                    });
                    jQuery('.commission-comment-dialog .ok-save-comment').click(function(){
                        var commentValue = jQuery('.commission-comment-dialog .commission-comment-dialog-comment').val();
                        var commentDate = jQuery('.commission-comment-dialog .commission-comment-dialog-date').val();

                        saveComentToContacting(expertiseId,expertId,commentValue,'afterSaveComentToContacting(expertId, comment")');
                    });
                }
        });
    };

    function commissionFilterOnlyConfirmAction(){
        //Вешаем обработчик фильтр только согласившихся
        jQuery('.commission-filter-only-confirm').change(function(){
            //var itId = jQuery(':selected', this).val();
            //var itFilter = jQuery(':selected', this).parent().attr('filtred-by');
            var itFilter = 'confirm-filter'
            var orgId = jQuery('.leftSideBar .branch-tab.selected').attr('branch-id');
            jQuery('.tab-tbody.tabcontent-'+orgId+' tr').addClass(itFilter+'-hide')
            var isCheck = jQuery(this).is(':checked');
            if (!isCheck){
                //вывести все
                jQuery('.tab-tbody.tabcontent-'+orgId+' tr').removeClass(itFilter+'-hide');
            }else{
                //вывести только те что надо
                jQuery('.tab-tbody.tabcontent-'+orgId+' tr[contacting-code="СonsentConfirm"]').removeClass(itFilter+'-hide');
            }
            jQuery('tr.commission-one-record').removeClass('select');
        });
    }

    /**
     * проверяем у всех ли програм получены положительные ответы
     * если да, то показываем кнопку печати распоряжения
     * переделка под просто наличие в программе
     * @returns {undefined}
     */
    function checkAllConfirm(){
        var result = true;
        console.log('проверка все ли эксперты согласились');
        //всего програм из заявы
        var programCount = 0;
        jQuery('#tab-commision .leftSideBar .branch-tab').each(function(ind,el){
            var haveProgram = jQuery(el).attr('program-count');
            if(!isNaN(haveProgram)){
                programCount += parseInt(haveProgram);
            }
        });
        //всего записей
        var allRaws = jQuery('.commission-one-record').length;
//        var allConfirm = jQuery('.commission-one-record[contacting-code="СonsentConfirm"]').length;
        // экспертов в таблице, назначено
        var allConfirm = jQuery('.commission-one-record[expert_id]').length;
        //есть программы без экспертов
        if(allRaws != allConfirm ){
            result = false;
            return false;
        }
        // эксперты без программ: руководители
        var allConfirmEmpty = jQuery('.commission-one-record[expert_id=""]').length;
        // эксперты отказавшиеся
        var allConfirmRefuse = jQuery('.commission-one-record[contacting-code="СonsentRefused"]').length;
        //эксперты с программами
        allConfirm = allConfirm - allConfirmEmpty - allConfirmRefuse;
        //только руководители
        var allOnlyCurator = jQuery('.commission-one-record[pogram_id="0"]').length;
        //выкиннем руководителей из общего списка
        allConfirm  = allConfirm - allOnlyCurator;
        //выкенем из назначеных отказников        

        //проверочка, чтоб эксперт не была контора
        jQuery('.commission-one-record .commission-button-find-expert').each(function(i,el){
            var parentId = jQuery(el).attr("parent_id");
            var expertId = jQuery(el).parents(".commission-one-record").attr("expert_id");
            if(parentId == expertId){
                result = false;
                return false;
            }
        });
        console.log(allRaws);
        console.log(allConfirm);
        if (programCount != allConfirm){
            result = false;
            return false;
            jQuery('.commission-button-commission-order').show();
            jQuery('.commission-button-order-include').show();
        }
        //проверяем руководителей
        jQuery('#tab-commision .rightSideBar .head-name').each(function(ind,el){
            var value = jQuery(el).text();
            if(value == 'Не задано'){
                result = false;
                return false;
            }
        });
        
        return result;
    }

    function commissionButtonCommissionOrder(){

        //Печатаем распорядительный документ распоряжения
        jQuery('.commission-button-commission-order').click(function(){
            var isAllConfirm=checkAllConfirm();
            if(!isAllConfirm){
                useTopHoverTitleError('Не все эксперты назначены');
                return 0;
            }
            var expertiseId = jQuery('#edit_declaration1 input[name="id"]').val();
            window.open('/ron/expertise/expertise-document/'+expertiseId, '_blank');
        });
    }


    function commissionDialogAction(){
        commissionFilterSelectedAction();
        commissionButtonCommentAction()
        commissionOneRecordAction();
        commissionButtonSendAction()
        tabCommisionAction();
        commissionFilterOnlyConfirmAction();
        checkAllConfirm();
        commissionButtonCommissionOrder();
        commissionButtonOrderInclude();
    }
    function view_shot_expert_card(){
    if ($('.row_appl').is('.active_appl')) {
        var dContent = $(
                '<div class="dialog-shot-view-card">' +
                    "<div style='text-align: center;'><div class='loading-img'><img src='/img/loading.gif' /></div><div class='leftSideBar'></div><div class='rightSideBar'></div></div>"+
                '</div>');

        $(dContent).dialog({
           title: "Информация об эксперте / экспертной организации",
           width: 1000,
           height: 600,
           modal: true,
           close: function() {
               $(".dialog, .dialog-shot-view-card").remove();
           },
           dialogClass: "scroll_off",
           buttons: [
                {
                    text: "Сохранить",
                    "class": 'right',
                    click: function() {
                        $('form.tab_exp#information-form').submit();
                    }
                },
                {
                    text: "Закрыть",
                    "class": 'right',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
           ],
           open: function() {
               $.ajax({
                   type: 'POST',
                   dataType: "html",
                   url: "/ron/expertise/get-card-expert/"+ $('.row_expert.active_expert').attr("id").split("_")[1],
                   success: function(answer) {

                       $(".loading-img", dContent).fadeOut(500, function() {
                       $(dContent).html(answer);
                           $('.choos').chosen();
                   });

               },
               error:function(e){
                   console.log('ошибка при загрузке панели добавления распорядительного документа');
                   console.log(e);
               }

                });
            }
        });
    }else{
        var dContent = $(
                '<div class="dialog-view-card">' +
                    "необходимо выбрать запись"+
                    '<ul class="nav_footer" id="footer_button" style="float:right; margin-top:20px;">' +
                    '<li class="filelink" id="filelink">'+
                    '<a id="help_button" onclick="$(\'.dialog-view-card\').dialog(\'close\');" href="javascript:;">'+
                        '<div></div>'+
                       ' <span class="text">Закрыть</span>'+
                    '</a>'+
                    ' </li>'+
                    '</ul>'+
                '</div>');

        $(dContent).dialog({
           title: "Карточка эксперта",
           width: 400,
           height: 140,
           modal: true,
           close: function() {
               $(".dialog, .dialog-view-card").remove();
           },
           autoOpen:true,


        });
    }
}
function showConfirmedDelete(){
        if ($('.row_expert').is('.active_expert')) {
            var dContent = $(
                '<div class="dialog-confirmed" style="height:85px;">' +
                    '<p >Вы уверены что хотите удалить данные об эксперте ?</p>'+
                '</div>');

            $(dContent).dialog({
               title: "Подтвердить удаление",
               width: 400,
               height: 220,
               modal: true,
               close: function() {
                   $(".dialog, .dialog-confirmed").remove();
               },
               autoOpen:true,
               buttons: [
                    {
                        text: "Да",
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/expertise/delete-all-expert-info/"+ $('.row_expert.active_expert').attr("id").split("_")[1],
                            });
                            $(".buttonParentClass  .show_tab.active").click();
                            $(this).dialog('close');
                        }
                    },
                    {
                        text: "Нет",
                        click: function() {
                            $(this).dialog('close');
                        }
                    }
               ],
            });
        }else{
             var dContent = $(
                '<div class="dialog-confirmed">' +
                    "необходимо выбрать запись"+
                    '<ul class="nav_footer" id="footer_button" style="float:right; margin-top:20px;">' +
                    '<li class="filelink" id="filelink">'+
                    '<a id="help_button" onclick="$(\'.dialog-confirmed\').dialog(\'close\');" href="javascript:;">'+
                        '<div></div>'+
                       ' <span class="text">Закрыть</span>'+
                    '</a>'+
                    ' </li>'+
                    '</ul>'+
                '</div>');

        $(dContent).dialog({
           title: "Карточка эксперта",
           width: 400,
           height: 140,
           modal: true,
           close: function() {
               $(".dialog, .dialog-confirmed").remove();
           },
           autoOpen:true,


        });
        }
    }
    function delete_expert_card(){
        if ($('.row_appl').is('.active_appl')) {
            var dContent = $(
                '<div class="dialog-confirmed" style="height:85px;">' +
                    '<p >Вы уверены что хотите удалить данные об эксперте ?</p>'+
                '</div>');

            $(dContent).dialog({
               title: "Подтвердить удаление",
               width: 400,
               height: 220,
               modal: true,
               close: function() {
                   $(".dialog, .dialog-confirmed").remove();
               },
               autoOpen:true,
               buttons: [
                    {
                        text: "Да",
                        click: function() {
                            $.ajax({
                                type: 'POST',
                                dataType: "html",
                                url: "/ron/expertise/delete-all-expert-info/"+ $('.row_appl.active_appl').attr("id").split("_")[1],
                            });
                            _serverQuery(1);
                            $(this).dialog('close');
                        }
                    },
                    {
                        text: "Нет",
                        click: function() {
                            $(this).dialog('close');
                        }
                    }
               ],
            });
        }else{
             var dContent = $(
                '<div class="dialog-confirmed">' +
                    "необходимо выбрать запись"+
                    '<ul class="nav_footer" id="footer_button" style="float:right; margin-top:20px;">' +
                    '<li class="filelink" id="filelink">'+
                    '<a id="help_button" onclick="$(\'.dialog-confirmed\').dialog(\'close\');" href="javascript:;">'+
                        '<div></div>'+
                       ' <span class="text">Закрыть</span>'+
                    '</a>'+
                    ' </li>'+
                    '</ul>'+
                '</div>');

            $(dContent).dialog({
               title: "Карточка эксперта",
               width: 400,
               height: 140,
               modal: true,
               close: function() {
                   $(".dialog, .dialog-confirmed").remove();
               },
               autoOpen:true,


            });
        }
    }


    function commissionButtonOrderInclude(){
        jQuery('.commission-button-order-include').click(function(){
            var isAllConfirm=checkAllConfirm();
            if(!isAllConfirm){
                useTopHoverTitleError('Не все эксперты назначены');
                return 0;
            }
            var expertiseId = jQuery('#edit_declaration1 input[name="id"]').val();

            var dContent = $(
                    '<div class="commission-order-include-dialog">'
                    + '</div>'
                );

            $(dContent).dialog({
                    title: "Согласование с экспертом",
                    width: 480,
                    height: 300,
                    modal: true,
                    close: function() {
                        $(".dialog, .commission-order-include-dialog").remove();
                    },
                    open: function() {
                        $.ajax({
                            type: 'POST',
                            //timeout: 5000,
                            //dataType: "html",
                            //data: data,
                            url: "/ron/expertise/get-expertise-order-document-include-dialog/"+expertiseId,
                            success: function(answer) {
                                jQuery(dContent).append(answer);
                                jQuery('',answer)
                            },
                            error:function(e){
                                console.log('ошибка при загрузке панели добавления  документа');
                                console.log(e);
                                useTopHoverTitleError('Ошибка связи');
                            }

                        });
                        //Вешаем обработчики на календари
                        $(".commission-comment-dialog-date", ".commission-comment-dialog").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd.mm.yy",
                        });
                        $(".date-icon", ".dialog-certificate-edit").click(function () {
                            $(this).parent().find("input").focus()
                        })
                        jQuery('.commission-comment-dialog .cancel').click(function(){
                            $(".dialog, .commission-comment-dialog").remove();
                        });
                        jQuery('.commission-comment-dialog .ok-save-comment').click(function(){
                            var commentValue = jQuery('.commission-comment-dialog .commission-comment-dialog-comment').val();
                            var commentDate = jQuery('.commission-comment-dialog .commission-comment-dialog-date').val();

                            saveComentToContacting(expertiseId,expertId,commentValue,'afterSaveComentToContacting(expertId, comment")');
                        });
                    }
            });
        });
    }
////---------------------
//Окончание вкладка состав комисиии
////-----------------------
