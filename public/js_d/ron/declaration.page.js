/*
 *
 * @param {type} _this
 * @returns {undefined}
 * Функция поиска по заявкам.
 */

function searchApplication(_this) {
    var page = $(_this).attr('page');
    if (page === undefined) {
        page = 0;
    }
    var event = $(".btn-warning").attr('event') + '/' + page;
    var keep_selector = $(".btn-warning").attr('controller');
    var destination_link = '/eo/' + keep_selector + '/' + event;
    $.ajax({
        type: 'POST',
        url: destination_link,
        data: $("#filterForm").serialize(),
        beforeSend: function () {
            $(".content_td .intable_content").html("").append($("<tr><td colspan='6'><div class='loading-img'><img src='/img/loading.gif' /></div></td></tr>")).fadeIn(500);
        },
        success: function (answer) {
            $(".content_td .intable_content").fadeOut(500, function () {
                answer = $(answer);
                $(".content_td .content").html("").append(answer);
                resizeContent($(".viewzone", answer));
                //Запускаем обработчики
                var handlerName = keep_selector + event.charAt(0).toUpperCase() + event.substr(1) + "Handlers";
                if (window[handlerName])
                    window[handlerName]();
            });
        }
    });
}

/*
 * Удаляет заявку и перегружает текущую вкладку
 */
function deleteApplication() {
    if ($('.row_appl').is('.active_appl')) {
        if (confirm("Вы уверены что хотите удалить заявку ?")) {
            $.ajax({
                url: '/eo/declaration/delete-declaration/' + $('.row_appl.active_appl').attr('id_appl'),
                success: function (answer) {
                    $('.btn-warning').click();
                }
            });
        }
    } else {
        useTopHoverTitleError("Пожалуйста, выберите запись");
    }
}

/*
 * Окно редактирования/создания заявки
 *//*
function openEdit(id_appl) {
    if (typeof(id_appl)=='undefined'||typeof(id_appl)=='object'){
        var id_appl = $('.row_appl.active_appl').attr('id_appl');
    }
    //Чтобы не обрабатывало при куче кликов
    if($('.row_appl.active_appl').attr("state")=="opening")
        return;
    //
    if (typeof(id_appl)!='undefined'||typeof(id_appl)!='object') {
        $('.row_appl.active_appl').attr("state","opening");
        $.ajax({
            url: '/eo/declaration/edit/' +id_appl ,
            success: function (answer) {
                $("#popup_win").html(answer);
                $("#popup_win").show();
                resizeContent($(".container .viewzone"));
                if ($("#type_declaration").val() == '41152d9a-3e6a-4c6b-9a6b-53e246eb8638') {
                    $('a[tab="2"]').parent('li').hide();
                    $('a[tab="3"]').parent('li').hide();
                }
                getButtonByStep();
                $(".input-date").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy",
                });
                $('.row_appl.active_appl').attr("state","none");
            }
        });
    } else {
 useTopHoverTitleError("Пожалуйста, выберите запись");
    }
};*/

function getLicenseByIdOrg(id) {
    $.ajax({
        url: '/eo/declaration/get-licenses-by-org/' + id,
        success: function (answer) {
            $("#licensedWrap").html(answer);
        }
    });
}
function getCertificateByIdOrg(id) {
    $.ajax({
        url: '/eo/declaration/get-certificates-by-org/' + id,
        success: function (answer) {
            $("#cetificateWrap").html(answer);
        }
    });
}
function searchOrgReorg(){
    if( $("#filteredTable tbody").height()-$("#filteredTable").scrollTop() == 359 &&  $("#nextPage").val()<= $("#maxPage").val() ){
                if($(".searchOrganization").val()==''){
                    var url='/eo/declaration/get-edu-org/'+$("#nextPage").val();
                }else{
                    var url='/eo/declaration/get-edu-org/'+$("#nextPage").val()+'/'+$(".searchOrganization").val();
                }
                $.ajax({
                    url: url,
                    beforeSend: function (xhr) {
                     $(".loading-overlay").show();
                       $("#getNextResult").parent('td').parent('tr').remove();
                       $("#nextPage").remove();
                       $("#maxPage").remove();
                    },
                    success: function(answer){
                        $(".loading-overlay").fadeOut(250, function(){
                            $("#filteredTable tbody").append(answer);
                        })
                    },
                    error:function(e){
                        useTopHoverTitleError("Возникла ошибка");
                        $(".loading-overlay").fadeOut(250);
                        console.log("Ошибка");
                        console.log(e);
                    }
                });
            }
}
function searchOrgAdd(){
    var nextPage=$("#nextPage").val();
    nextPage=parseInt(nextPage);
    var maxPage=$("#maxPage").val();
    maxPage=parseInt(maxPage);
    if(/* $("#filteredTable tbody").height()-$("#filteredTable").scrollTop() == 359
            && */  nextPage<= maxPage ){
                if($(".searchOrganization").val()==''){
                    var url='/eo/declaration/get-organizations-select/'+$("#nextPage").val();
                }else{
                    var url='/eo/declaration/get-organizations-select/'+$("#nextPage").val()+'/'+$(".searchOrganization").val();
                }
                $.ajax({
                    url: url,
                    beforeSend: function (xhr) {
                     $(".loading-overlay").show();
                       $("#getNextResult").parent('td').parent('tr').remove();
                       $("#nextPage").remove();
                       $("#maxPage").remove();
                    },
                    success: function(answer){
                        $(".loading-overlay").fadeOut(250, function(){
                            $("#filteredTable tbody").append(answer);
                        })
                    },
                    error:function(e){
                        useTopHoverTitleError("Возникла ошибка");
                        $(".loading-overlay").fadeOut(250);
                        console.log("Ошибка");
                        console.log(e);
                    }
                });
            }
}
function openAdd() {

    $.ajax({
        url: '/eo/declaration/edit',
        success: function (answer) {
            max_show_tab = 0;
            $("#popup_win").html(answer);
            $("#popup_win").show();
            resizeContent($(".container .viewzone"));
            $(".input-date").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: "dd.mm.yy",
            });
            $.ajax({
                url: '/eo/declaration/get-application-reason/' + $(this).val(),
                success: function (answer) {
                    $("#reason_td").html(answer);
                }
            });
        }
    });
}

/**
 * шаг 5
 * Диалог для кнопочки перехода по шагам
 * Добавляем к заявлению распорядительный документ
 * (когда документ будет подписан).
 *
 */
function applicationAddOrderDocument(){
    console.log('хотим добавит распорядительный документ к заявлению');
    var dContent = $(
        '<div class="dialog-orderDecument-add">' +
            "<div style='text-align: center;margin-top:40px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
            title: "Распорядительный акт",
            width: 640,
            height: 400,
            modal: true,
            close: function() {
                $(".dialog, .dialog-orderDecument-add").remove();
            },
            open: function() {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/declaration/application-document-add/" + $('.row_appl.active_appl').attr('id_appl'),
                    success: function(answer) {
                        console.log(answer);
                         $(".loading-img", dContent).fadeOut(500, function() {
                             dContent.html("").append($(answer));
                             $(".date-calendar").html('Дата документа');
                             //$("#noticie_button-add a").attr('onclick','addNewOrderDocumentFromDeclaration()');
                         });

                    },
                    error:function(e){
                        console.log('ошибка при загрузке панели добавления распорядительного документа');
                        console.log(e);
                    }

                });
            }
    });

}

function addError(selectorName,hasError){
    jQuery(selectorName).parent().removeClass('has-error');
    if (jQuery(selectorName).val()===''){
        jQuery(selectorName).parent().addClass('has-error');
        return true;
    }
    return hasError;
}

function addNewOrderDocumentFromDeclaration(){
    //проверяем на наличие значений
    var hasError=false;
    hasError = addError('.document-field-type',hasError);
    hasError = addError('.document-field-kind',hasError);
    hasError = addError('.document-field-number',hasError);
    hasError = addError('.document-field-date',hasError);

    if(hasError){
        useTopHoverTitleError("Есть не заполненые поля");
        return 0;
    }
console.log(jQuery('#CertificateValue').attr('idcert'));
    var sendData={
        "Kind":jQuery('.document-field-kind').val(),
        "Number":jQuery('.document-field-number').val(),
        "DateSign":jQuery('.document-field-date').val(),
        "CertificateId":jQuery('#CertificateValue').attr('idcert')
    };

    //отправляем на сервер
    $.ajax({
        type: 'POST',
        dataType: "text",
        url: "/eo/index/certificate-document-new",
        data:sendData,
        success: function(answer) {
            //if (answer==='Ok'){

                jQuery(".content-block.documents tbody").append(
                '<tr><td>'
                +jQuery('.document-field-type option:selected').text()
                +'</td><td>'
                +jQuery('.document-field-kind option:selected').text()
                +'</td><td>'
                +jQuery('.document-field-number').val()
                +'</td><td>'
                +jQuery('.document-field-date').val()
                +'</td></tr>'
                    );
                //console.log();
                $(".dialog, .dialog-orderDecument-add").remove();
                showEP()
            //}else{
           //     useTopHoverTitleError('Ошибка при записи');
           // }

        },
        error:function(e){
            useTopHoverTitleError('Ошибка при записи');
            console.log('ошибка при сохранении добавления распорядительного документа');
            console.log(e);
        }

    });

    //получаем результат

}
/*
 *Получение кнопок для окна на определеном шаге
 */
function getButtonByStep() {
    $("#popup_win #but_wrap_child_right #btActions-block li").each(function(indx, element){
        if(!jQuery(element).hasClass('btn_app_card_edit_default')){
            jQuery(element).remove();
        }
    });
    console.log($("#popup_win #but_wrap_child_right #btActions-block"));
    var idAppl=jQuery("#id_application_val_id").val();
    $.ajax({
        url: '/eo/declaration/get-button-by-step/' + $("#now-step").val()+'/'+idAppl,
        async:false,
        timeout:2000,
        success: function (answer) {
            $("#popup_win #but_wrap_child_right #btActions-block").append(answer);
        }
    });

}
/*
 * @param string new_stat Новый статус заявки
 */
function changeStatus(new_stat, id_appl, more_func, bt_click,isConfirm,after_func) {
    if (typeof(isConfirm)==='undefined'){
        isConfirm=1;
    }
    if(isConfirm){
        if (!confirm('Вы уверены что хотите изменить статус заявки ?')) {
            return 0;
        }
    }
    if (bt_click === undefined){
        bt_click=0;
    }
    more_func = more_func || 0;
    if (id_appl === undefined || id_appl === 0) {
        id_appl = $("#id_application_val").val();
    }
    $.ajax({
        type: "POST",
        url: '/eo/declaration/change-status-declaration',
        data: "newStatus=" + new_stat + "&idAppl=" + id_appl + "",
        success: function (answer) {
            if (answer == 'ok') {
                if (more_func != 0) {
                    eval(more_func);
                }
                useTopHoverTitle("Статус заявки изменен!");
                if (bt_click ) {
                    $(".btn-warning").click();
                    max_show_tab = 0;
                }
                if (typeof(after_func)=='function'){
                    after_func();
                }
                if(typeof(after_func)=='string'){
                    eval(after_func);
                }
            }
        }
    });
    $('.ui-dialog').hide();
    $('.ui-widget-overlay').hide();
}

/*
 * Окно редактирования/создания заявки
 */
function showDeclaration(appId) {
    if(appId==undefined){
        appId = $('.row_appl.active_appl').length;
        if(appId>0){
            appId = $('.row_appl.active_appl').attr('id_appl');
        }else{
            useTopHoverTitleError("Пожалуйста, выберите запись");
        }
    }
    if (appId) {
        $.ajax({
            url: '/eo/declaration/edit/' + appId,
            success: function (answer) {
                $("#popup_win").html(answer);
                $("#popup_win").show();
                resizeContent($(".container .viewzone"));
                if ($("#type_declaration").val() == '41152d9a-3e6a-4c6b-9a6b-53e246eb8638') {
                    $('a[tab="2"]').parent('li').hide();
                    $('a[tab="3"]').parent('li').hide();
                }
                $("#but_wrap_child_left").hide();
                $(/*'#but_wrap_child_left*/ '#btActions-Noticie ').parent().parent().show();
                $("#fields_form input").attr('disabled', "disabled");
                getButtonByStep();
                $(".input-date").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: "dd.mm.yy",
                });
            }
        });
    } else {
        useTopHoverTitleError("Пожалуйста, выберите запись");
    }
}
;

/*
 * переход по вкладкам на форме редактирования
 *//*
function showHideTabs(show, hide) {
    $(".show_tab[tab='" + hide + "']").removeClass('active');
    $(".show_tab[tab='" + show + "']").addClass("active");
    $(".tabs-panel").removeClass("show-panel");
    $("#tab" + show + "").addClass('show-panel');
    $("#sendform1").attr('numb', show);
    if(show != 4){$("#button_load_dop_docs").remove(); }
}*/

function getFillialsPrograms(){
            $.ajax({
                url: '/eo/declaration/get-fillials-programs/' + $("#id_application_val").val(),
                type: "POST",
                beforeSend: function () {
                    $(".loading-overlay").show();
                },
                success: function (answer) {
                    $(".leftSideBar").html(answer);
                    $(".loading-overlay").fadeOut(250);
                },
                error:function(e){
                    useTopHoverTitleError("Возникла ошибка");
                    $(".loading-overlay").fadeOut(250);
                    console.log("Ошибка");
                    console.log(e);
                }
            });
}



function handleResponse2(mes) {
    $(".loading-overlay").hide();
    if (mes == "error") {
        useTopHoverTitleError("Возникли ошибки при добавлении заявки!");
    } else {
        useTopHoverTitle('Заявка на экспертизу удачно создана');
        $('#popup_win2 #cboxClose').click();
        getButtonByExpertise();
    }
}

/*
 * получение списка загруженных файлов.
 */
/*
function getFilesAjax(){
    $.ajax({
        url: '/eo/declaration/application-files/'+$("#id_application_val").val(),
        beforeSend: function () {
                $(".loading-overlay").show();
        },
        success: function(answer){
            $("#tab4").html(answer);
            $('#button_load_dop_docs').remove();
            if($(".importantFiles .maySent").length==0 && ($(".active_appl").attr('id_status') === undefined || $(".active_appl").attr('id_status') == '38D871AC-BF1D-469E-AFC1-7A6CD55EF1B6') ){
                $("#sendform1").hide();
                $("#but_wrap_child_right #btActions-block").prepend('<li class="filelink sendApplication"><a onclick="sendToSend()" href="javascript:;"><div></div><span class="text">Отправить</span></a></li>');
            }else{
                $(".maySent").parents('.containerWrapFile').css('border','1px solid red');
                $(".maySent").focus();
                $("#but_wrap_child_right #btActions-block .sendApplication").remove();
            }
            $(".loading-overlay").fadeOut(250);
        },
        error:function(e){
 useTopHoverTitleError("Возникла ошибка");
            $(".loading-overlay").fadeOut(250);
            console.log("Ошибка");
            console.log(e);
        }
});
}*/

/**
 * ф-ция закрытия диалогового окна загрузки дополнительного док-та и вывода сообщщения
 */
function closeDialogAddDopDocs(){
    $(".loading-overlay").hide();
    var count_files = 1, ending;
    count_files = $(".MultiFile-label", "#dop_files_list_upload").length;
    ending = (count_files > 1) ? 'ы' : '' ;


    $('.dialog').remove();
    $("<div class='dialog'></div>").html("<p>Документ" + ending + " успешно добавлен" + ending + " в систему</p><div class='nav_footer' style='float:right'><ul><li><a href='javascript:;' onclick='$(\".dialog\").dialog(\"close\");$(\".dialog\").remove();'>Закрыть</a></li></ul></div>")
        .dialog({
            autoOpen:false,
            title: "Уведомление",
            width:500,
            modal:true
        }).dialog("open");
}

/**
 * загрузка дополнительного документа в систему
 */
function load_dop_file_in_system(){
    var form_id = "#dop_files_list_upload",
        count_files = 0;

    //проверка установлен обязательность полей
    count_files = $(".FileListWrap div", form_id).length;
    if(count_files == 0){
       $("input[type='file']", form_id).after('<span style="color:red" class="error_text">Необходимо выбрать файл</span>');
       return false;
    }

    //сохранение документа на сервере
    $(".loading-overlay").show();
    closeDialogAddDopDocs();
    getFilesAjax();
    //$(form_id).submit();

}

/**
 * окно удаление документа
 * @param {Integer} file_id ид удаляемого файла
 * @param {Integer} table_flag value = 1 - удаляем из таблицы isga_ApplicationFiles, 2 - из таблицы isga_ApplicationDocumentGroups
 */
function window_delete_application_file(file_id, table_flag){
    var buttons = '<div class="nav_footer" style="float:right"><ul><li><a href="javascript:;" onclick="delete_application_file(\'' + file_id +'\', ' + table_flag + ')">Удалить</a></li><li><a href="javascript:;" onclick="$(\'.dialog\').dialog(\'close\');$(\'.dialog\').remove();">Отмена</a></li></ul></div>';
    $('.dialog').remove();
    $("<div class='dialog'></div>").html("<p class='message_for_user'>Удалить документ?</p>" + buttons)
        .dialog({
            autoOpen:false,
            title: "Уведомление",
            modal:true
        }).dialog("open");
}

/**
 * удаление документа
 * @param {Integer} file_id ид удаляемого файла
 * @param {Integer} table_flag value = 1 - удаляем из таблицы isga_ApplicationFiles, 2 - из таблицы isga_ApplicationDocumentGroups
 */
function delete_application_file(file_id, table_flag){
    $.ajax({
        type: "POST",
        url: "/eo/declaration/delete-application-files",
        dataType: 'json',
        beforeSend: function () {
            $(".loading-overlay").show();
        },
        data: ({file_id : file_id, table_flag : table_flag}),
        success: function(data){
            $(".loading-overlay").hide();
            handleResponse(4);
            var message = "Документ успешно удален"
            if(data.response == 'error'){
               message = data.message;
            }

            $('.dialog').dialog("close");$('.dialog').remove();
            var buttons = '<div class="nav_footer" style="float:right"><ul><li><a href="javascript:;" onclick="$(\'.dialog\').dialog(\'close\');$(\'.dialog\').remove();">Закрыть</a></li></ul></div>';
            $("<div class='dialog'></div>").html("<p>" + message + buttons + "</p>")
                .dialog({
                        autoOpen:false,
                        title: "Уведомление",
                        modal:true
                }).dialog("open")

        }
    });
}

/**
 * функция открытия окна Загрузки дополнительных документов
 */
function loadDopDocs(){
    var id_application = $("#id_application_val").val();

    //подгрузим форму для загрузки
    $.ajax({
        url: '/eo/declaration/application-dop-files/' + id_application,
        beforeSend: function () {
            $(".loading-overlay").show();
        },
        success: function(response){
            $(".loading-overlay").hide();
            $('.dialog').remove();
            $("<div class='dialog'></div>").html("<p class='segoe'>" + response + "</p>")
                .dialog({
                    autoOpen: false,
                    title: "Загрузка документа",
                    modal: true,
                    width: 600
                }).dialog("open")
        }
    });



}

/*
 /*
  * ОТправка заявки на вкладку отправленные
  */
/*
 function sendToSend(){
     if($(".importantFiles .maySent").length>0){
 useTopHoverTitleError("Вы не загрузили один из обязательных документов.");
         $(".maySent").parents('.containerWrapFile').css('border','1px solid red');
         $(".maySent").focus();
     }else{
        $.ajax({
            url: '/eo/declaration/send-to-send/'+$("#id_application_val").val(),
            beforeSend: function () {
                $(".loading-overlay").show();
            },
            success: function(answer){
                console.log(answer);
                if ( answer == 'Ok' ) {
                    console.log(answer);
                    useTopHoverTitle('Ваше заявление направлено в Рособрнадзор',2000)
                    $('.btn-warning').click();
                    $(".loading-overlay").fadeOut(250);
                }
                if( answer == 'OkNo' ) {
                    console.log(answer);
                    //useTopHoverTitleError("Заявка отправлена");
                    $('.btn-warning').click();
                    $(".loading-overlay").fadeOut(250);
                }
                if ( answer == 'Unsigned') {
                    showPop('/eo/declaration/get-form-signed/'+$("#id_application_val").val());
                    $(".loading-overlay").show();
                }

            },
            error:function(e){
 useTopHoverTitleError("Возникла ошибка");
                $(".loading-overlay").fadeOut(250);
                console.log("Ошибка");
                console.log(e);
            }
        });
     }
 }*/
function showPop(url,showWaitWindow) {
    url=encodeURI(url);//персонально для ИЕ
    if(showWaitWindow == '1')
        $(".loading-overlay").show();
    $.ajax({
        type: 'POST',
        url: url,
        success: function (answer) {
            $("#popupWindow").html(answer);
            $("#popupWindow").show();
        }
    });
}
$(document).ready(function () {
    $("body").delegate(".btCancel","click",function(){
        $("#filterForm")[0].reset();
    });
    /*$("body").delegate("#filterForm .btApply", "click", function () {
        searchApplication(this);
    });*/
    $("body").delegate("#appSearchInput", "keyup", function (e) {
        if(e.which==13){
            searchApplication(this);
            if(e.stopPropagation) e.stopPropagation();
            if(e.preventDefault) e.preventDefault();
        }
    });
    $('body').undelegate('.row_appl', 'click');
    $('body').delegate('.row_appl', 'click', function () {
        $('.row_appl').removeClass('active_appl');
        $(this).addClass('active_appl');
    });
   /* $("body").delegate("#sendform1", "click", function () {
        if ($(this).attr('numb') == 1) {
            if(jQuery('#Id_organization').val()==''){
    useTopHoverTitleError('Не выбрана организация');
                return 0;
            }
            var step = jQuery("#now-step").val();
            var countSelected=jQuery("#reason_td .check_req:checked").length;
            if(countSelected<1 && step==1){
    useTopHoverTitleError('Не выбрано оснований заявления');
                return 0;
            }
            //$(".loading-overlay").show();
            $("#submitFirstForm").click();
            //$("#edit_declaration"+$(this).attr('numb')).submit();
        } else {
            handleResponse($(this).attr('numb'), 1);
        }
    });
    $("body").delegate("#sendform2", "click", function () {
        $("#popup_win2 #submitFirstForm").click();
    });*/
    $("body").delegate(".page","click",function(){
        searchApplication(this);
    });
    $("#wrapReorg").delegate("#getNextResult","click",function(){
            searchOrgAdd();
        });
        $("#popupWindow").delegate(".searchOrganization","change",function(){
            if($("#add_reorg_struct").hasClass('clicked')){
                showPop('/eo/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
            }else{
                showPop('/eo/declaration/get-organizations-select/0/'+$(".searchOrganization").val());
            }
        });
        //обработчик тыков по галчкам причин/оснований заявлений
        $("body").delegate(".check_req","click",function(){
            if($(".check_req:checked").length>0){
                var isset = true;
            }else{
                var isset = false;
            }
            if(isset === false){
                $(".check_req").prop('disabled',false);
            }else{
                $(".check_req").prop('disabled',true);
                $(".check_req[groupe_id='"+$(this).attr('groupe_id')+"']").prop('disabled',false);
            }
            //если забанена тип дупликата, то нельзя выбрать пирчины
            //(нет предыдущего свидетельства)
            //баним доокредитацию и переоформление
            var appDuplicatType=jQuery("#type_declaration option[code='DublicateCertificate']").attr('disabled');
            if(appDuplicatType !== undefined){
                jQuery('#reason_td .check_req[groupe_id="1"]').attr('disabled','disabled');
                //jQuery('#reason_td .check_req[groupe_id="3"]').attr('disabled','disabled');
            }

        });
        /*$("#popupWindow").undelegate("#add_org","click");
        $("#popupWindow").delegate("#add_org","click",function(){
            $("#Id_organization").val($('.selected_org:checked').val());
            $("#nameOrganization").val($('.selected_org:checked').attr('org_name'));
            getLicenseByIdOrg($('.selected_org:checked').val());
            getCertificateByIdOrg($('.selected_org:checked').val());
            $("#popupWindow").hide();
        });*/

        $('body').delegate('.level-elem', 'click', function () {
            $('.level-elem').removeClass('active-level');
            $(this).addClass('active-level');
            getUgsList();

        });
        $('body').delegate('.programCol', 'change', function () {
            saveProgramField(this);
        });
        $('body').delegate('.ugs_row', 'click', function () {
            $('.ugs_row').removeClass('active_ugs_row');
            $(this).addClass('active_ugs_row');
            getProgramsByUgs();
        });
        $('body').delegate('#show_all_program', 'click', function () {
            setTimeout(function () {
                if ($("#show_all_program").is(":checked")) {
                    $('.ugs_row').each(function (i) {
                        $(this).click();
                    });
                } else {
                    $('.ugs_row:first').click();
                }
            },500);
        });
        $("body").delegate(".remove_answer","click",function(){
            $(this).parent('.wrap_file_answer').remove();
        });
        $("body").delegate("#search_direct","keyup",function(){
            var search_str = $("#search_direct").val().toLowerCase();
            if(search_str==""){ $(".list_of_direct li").show(); return 1;}
            $(".list_of_direct li label").each(function(i){
                var text = $(this).text().toLowerCase();
                if(text.indexOf(search_str)===-1){
                    $(this).parent('li').hide();
                }else{
                    $(this).parent('li').show();
                }
            });
        });
        $("body").delegate(".select_all_direct","click",function(){
            if($(this).is(":checked")){
                $(".list_of_direct li input[type=checkbox]").prop('checked', true);
            }else{
                $(".list_of_direct li input[type=checkbox]").prop('checked', false);
            }
        });
});
function showEP(){
    var dContent = $(
        '<div class="dialog-orderDecument-add">' +
            "<div style='text-align: center;'><div class='loading-img'><img src='/img/loading.gif' /></div><div class='leftSideBar'></div><div class='rightSideBar'></div></div>"+
        '</div>');
    $(dContent).dialog({
            title: "Выбор образовательных программ",
            width: 1170,
            height: 600,
            modal: true,
            close: function() {
                $(".dialog, .dialog-orderDecument-add").remove();
            },
            open: function() {
                $.ajax({
                    type: 'POST',
                    dataType: "html",
                    url: "/eo/declaration/get-fillials-programs/"+$('.row_appl.active_appl').attr('id_appl')+'/1',
                    success: function(answer) {
                         $(".loading-img", dContent).fadeOut(500, function() {
                             $(".ui-dialog-titlebar-close").hide();
                             $(".dialog-orderDecument-add").prepend('<p>Выберите нужные образовательные программы и нажмите кнопку "Закрыть".</p>');
                             $(".dialog-orderDecument-add .leftSideBar").html("").append(answer);
                             $(".dialog-orderDecument-add .leftSideBar").append('<style>.allhidden{display:table-cell;}</style>');
                             $('.allhidden',dContent).css('display','table-cell');
                             //jQuery('.program-checked').css('display','table-cell');
                             //$(".dialog-orderDecument-add").append('<a class="right-close" type="button" onclick="changeStatus(\'AF475ADD-6ED4-43F1-9E66-C9C2BF4E2DB9\',0,\'changeBeforeAddOrderDocument()\',0,0,\'changeAfterAddOrderDocument()\');" href="javascript:;"><span class="text">Закрыть</span></a>');
                             //$(".dialog-orderDecument-add").append('<a class="right-close" type="button" onclick="changeBeforeAddOrderDocument();changeAfterAddOrderDocument();" href="javascript:;"><span class="text">Закрыть</span></a>');
                         });
                    },
                    error:function(e){
                        console.log('ошибка при загрузке панели добавления распорядительного документа');
                        console.log(e);
                    }
                });
            }
    });
}

var appIdMemory=0;
function changeBeforeAddOrderDocument(){
    appIdMemory=$('.row_appl.active_appl').attr('id_appl');
}

function changeAfterAddOrderDocument(){
                //после закрытия автоматом перепрыгиваем на след шаг
                //и открываем карточку заявления
                 $(".dialog, .dialog-orderDecument-add").remove();
                 jQuery('.btn.btn-success[event="step/6"]').click();
                //jQuery('.btn-warning').removeClass('btn-lg');
                //jQuery('.btn-warning').addClass('btn-success');
                //jQuery('.btn-warning').removeClass('btn-warning');
                //jQuery('.btn.btn-success[event="step/6"]').addClass('btn-warning');
                //jQuery('.btn-warning').addClass('btn-lg');
                //jQuery('.btn-warning').removeClass('btn-success');
                //jQuery('#now-step').val(6);
                //showDeclaration(appIdMemory);
                //jQuery('.btn-warning').click();

}

function sendEPChecked(elem){
    if($(elem).prop('checked')){
        var status = 1;
    }else{
        var status = 0;
    }
    $.ajax({
        type: 'POST',
        dataType: "html",
        url: "/eo/declaration/change-status-ep/"+$(elem).attr('attr_id')+'/'+status,
    });
//$(".dialog, .dialog-orderDecument-add").remove();
}
function openDialogLoadXML() {
    var content = $('#dialog-loadXML');
    content.dialog({
        title: 'Поиск заявления',
        width: '95%',
        height: 600,
        open: function () {
            $('#divFormLoadXml').css('display', 'block');
            $('#formLoadXml', '#dialog-loadXML')[0].reset();
            $('#divDiffData', '#dialog-loadXML').empty();
            $('#btInWork').css('display', 'none');
        },
        close: function () {
            //$(".btn-warning").click();
        }
    });
}

function setData() {
    $('#divFormLoadXml').toggle();
    $('#divDiffData').html($("#frameLoadXml").contents().find("#resultData").html());
}

function loadXMLResponse(data) {
    data = jQuery.parseJSON(data);
    if (data.status === 'OK') {
        $('#divFormLoadXml').toggle();
        $('#divDiffData').html(decodeURIComponent(data.html));
    } else {
        useTopHoverTitleError(data.message);
    }
}

function echoDivDiffData(app_id, file_name) {
    $.ajax({
        url: '/eo/declaration/get-diff-data/' + app_id + '/' + file_name,
        type: "POST",
        success: function (answer) {
            $('#divDiffData').html(answer);
        }
    });
}

function applyDiffData() {
    $.ajax({
        url: '/eo/declaration/apply-diff-data/' + app_edit_data_id,
        type: "POST",
        success: function (answer) {
            useTopHoverTitle(answer);
        }
    });
}

function notifyShow(){

}
/*
  * Ajax отправка файлов на сервер
  */
 function SendFile() {
    //отправка файла на сервер
    $(".loading-overlay").show();
    $("#files_list_upload").submit();
}

function btnBackToRegistre(){
    var id_appl = $(".supplements.intable_content tr.active_appl").attr('id_appl');
    if (id_appl === undefined || id_appl === 0) {
        useTopHoverTitleError("Выберите заявление") ;
        return 0;
    }
        changeStatus('0FE99A13-98BB-4235-97B2-B65819AB34AF',id_appl,0,1);
}

function btnBackToNotifyPositivSign(){
    var id_appl = $(".supplements.intable_content tr.active_appl").attr('id_appl');
    if (id_appl === undefined || id_appl === 0) {
        useTopHoverTitleError("Выберите заявление") ;
        return 0;
    }
        changeStatus('342fa3ba-27a5-421c-9faa-3784e3e7f7a9',id_appl,0,1);
}


/**
 * кнопка назад на шаге Подготовка распорядительного документа
 *
 * @returns {Number}
 */
function btnBackFromOrderDocStep(){
    var id_appl = $(".supplements.intable_content tr.active_appl").attr('id_appl');
    if (id_appl === undefined || id_appl === 0) {
        useTopHoverTitleError("Выберите заявление") ;
        return 0;
    }
    var id_status = $(".supplements.intable_content tr.active_appl").attr('id_status');
    id_status = id_status.toUpperCase();
    //короткое
    if(id_status == '26BDDB50-A743-489D-A543-377F024F11FE'){
        changeStatus('E4435D27-3A66-49B6-B1A6-8804AB318C01',id_appl,0,1);
        return 0;
    }
    //Длинное
    if(id_status == '502B4245-08F3-4CFF-BF51-907058B93AA9'){
        changeStatus('344F1AC9-CCE7-4A5F-A478-39ABDEDA5B21',id_appl,0,1);
        return 0;
    }
    useTopHoverTitleError('Не знаю куда идти');
}



function getEduLevel() {
    $.ajax({
        url: '/eo/declaration/get-edu-program-type/' + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org'),
        type: "POST",
        success: function (answer) {
            $('.level-list').addClass('old-level-list');
            $('.old-level-list').remove();
            $('.active_fil').parent('li').append(answer);
            $('.level-list').show(250);
            getUgsList();
        }
    });
}/*
function getUgsList() {
    $.ajax({
        url: '/eo/declaration/get-ugs-by-level/' + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org')+'/'+$(".level-elem.active-level").attr('id-level'),
        type: "POST",
        success: function (answer) {
            $('.rightSideBar').html(answer);
            getProgramsByUgs();
        }
    });
}*/
function getDirectByUgs(id){
    $('.ugs_row').removeClass('active_ugs_row');
    $('.ugs_row[ugs_id="'+id+'"]').addClass('active_ugs_row');
    var dContent = $(
        '<div class="dialog-directugs-info">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "Направление подготовки",
        width: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-directugs-info").remove();
        },
        open: function() {
            //Отсылаем запрос на получение инфо по заявлению
            $.ajax({
                type: 'POST',
                dataType: "html",

                url: '/eo/declaration/get-direct-by-ugs/'  + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org') + '/' + $('.active-level').attr('id-level') + '/' + id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Вешаем обработчики

                        //Устанавливаем высоту
                        $(".dialog-content",dContent).height($(dContent).height()-20);
                    });
                }
            });
        }
    });
}
function sendCheckDirect(){
    $.ajax({
        type: 'POST',
        data:$("#direct_form").serialize()+"&appl="+ $(".active_fil").attr('id_appl')+"&org="+$(".active_fil").attr('id_org')+"&level="+$('.active-level').attr('id-level'),
        url: '/eo/declaration/save-checked-direct',
        success: function (answer) {
             $(".dialog, .dialog-directugs-info").remove();
             getProgramsByUgs();
        }
    });
}
/*
function getProgramsByUgs() {
    $.ajax({
        type: 'POST',
        dataType: "html",
        async: false,
        url: '/eo/declaration/get-program-by-ugs/' + $(".active_fil").attr('id_appl') + '/' + $(".active_fil").attr('id_org') + '/' + $('.active-level').attr('id-level') + '/' + $('.active_ugs_row').attr('ugs_id'),
        success: function (answer) {
            if(!$("#show_all_program").is(":checked")){
                $('.ugs_block').removeClass('active_ugs_block');
            }
            $('.ugs_block[ugs_id="'+$('.active_ugs_row').attr('ugs_id')+'"]').html(answer);
            $('.ugs_block[ugs_id="'+$('.active_ugs_row').attr('ugs_id')+'"]').addClass('active_ugs_block');
            tableProgramsByUgsHeadering();
        }
    });
}*/
function saveProgramField(elem) {
    console.log('рон - акредитация - канрточка заявления - программы - тык по флажку - 3');
    var id = $(elem).parent().parent().attr('id_program');
    var fieldName = $(elem).attr('name');
    var fieldVal = $(elem).val();
    
    if( jQuery(elem).is('[type="checkbox"]') ){
        if( jQuery(elem).is(':checked') ){
            fieldVal = 1;
        }
        else{
            fieldVal = 0;
        }
    }
    
    $.ajax({
        type: 'POST',
        dataType: "html",
        data: fieldName+"="+fieldVal,
        url: '/eo/declaration/save-field-program-application/'+id,
    });
}
function deleteProgramAppl(id){
     $.ajax({
        type: 'POST',
        dataType: "html",
        url: '/eo/declaration/delete-program-application/'+id,
        success:function(){
             getProgramsByUgs()
        }
    });
}

function getEditComments(){
    var url = '/eo/declaration/get-edit-comment/' + $("#id_application_val").val();
    $.ajax({
        url: url ,
        type: "POST",
        success: function (answer) {
            $('#tab11').html(answer)
        },
        error:function(e){
            console.log('ошибка при получении комментариев');
            console.log(e);
        }

    });
}


/*пригружаем кнопки на шаге Акредитационная экспертиза
 * статус Формирование экспертной группы 968BE98A-A5B9-404D-B97B-233784AF7AA0
 *
 * если экспертиза не создана, то кнопку Назначить экспертизу
 * если создана, то кнопки Подготовка приказа, Подбор экспертов
 *
 * слиззано с getButtonByStep
 *
 * @returns {undefined}
 */
function getButtonByExpertise() {
    $("#popup_win #but_wrap_child_right #btActions-block li").each(function(indx, element){
        if(!jQuery(element).hasClass('btn_app_card_edit_default')){
            jQuery(element).remove();
        }
    });
    $('.btn_expertise_class').remove();
    var idAppl=jQuery("#id_application_val_id").val();
    $.ajax({
        url: '/eo/declaration/get-button-by-expertise/'+idAppl,
        async:false,
        timeout:2000,
        success: function (answer) {
            $("#popup_win #but_wrap_child_right #btActions-block").append(answer);
        }
    });
}
/*
 * подключение fileupload для различных загрузок
 */
/*
function activateFileUpload(url_out,class_elem){
    $(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = url_out;
    $(class_elem).fileupload({
        url: url+'/'+$("#id_application_val").val(),
        dataType: 'json',
        done: function (e, data) {
            var elem = this;
            $.each(data.result.files, function (index, file) {
                if(file.errors.length<1){
                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div id="'+file.fileId+'"><span class="MultiFile-remove" onclick="window_delete_application_file(\''+file.fileId+'\', 1)">x</span><a target="_blank" href="'+file.filePath+'" download="'+file.fileName+'">'+file.fileName+'</a></div>')     ;
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').width("0%");
                    $('.FileListWrap.'+$(elem).attr("codeType")).parent(".containerWrapFile").css('border','none');
                }else{
                    var error_string = data.result.files[0].errors.join(',');

                    $('.FileListWrap.'+$(elem).attr("codeType"))
                        .append('<div class="wrap_file_answer"><a class="MultiFile-remove remove_answer" >x</a> <span style="color:red;" class="red">'+data.originalFiles[0].name+'(Ошибки при загрузке файла: '+error_string+' )</span></div>');
                    $('.progress .progress-bar.'+$(elem).attr("codeType")+'').css('width','0%');
                }
                 $('.progress .progress-bar.'+$(elem).attr("codeType")).parent('.progress').hide();
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').parent('.progress').show();
            $('.progress .progress-bar.'+$(this).attr("codeType")+'').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
}*/
function recallApplication(id){
    var dContent = $(
        '<div class="dialog-recall-application">' +
        "<div style='text-align: center;margin-top:100px'><div class='loading-img'><img src='/img/loading.gif' /></div></div>"+
        '</div>');
    $(dContent).dialog({
        title: "Отзыв заявления",
        width: 600,
        modal: true,
        close: function() {
            $(".dialog, .dialog-recall-application").remove();
        },
        buttons: [
                               {
                    text: "Сохранить",
                    "class": 'right saveButExpert',
                    click: function() {
                       sendRecallForm(id)
                    }
                },
                {
                    text: "Закрыть",
                    "class": 'right closeDialogRecall',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
           ],
        open: function() {
            //Отсылаем запрос на получение инфо по заявлению
            $.ajax({
                dataType: "html",

                url: '/eo/declaration/recall-application/' + id,
                success: function(answer) {
                    $(".loading-img", dContent).fadeOut(500, function() {
                        dContent.html("").append($(answer));
                        //Вешаем обработчики

                        //Устанавливаем высоту
                        $(".dialog-content",dContent).height($(dContent).height()-20);
                        $(".input-date").datepicker({
                            changeMonth: true,
                            changeYear: true,
                            dateFormat: "dd.mm.yy"
                        });
                    });
                }
            });
        }
    });

}
function sendRecallForm(id){
    var form = document.forms.RecallForm;

    var formData = new FormData(form);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", '/eo/declaration/recall-application/' + id);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                data = $.parseJSON(xhr.responseText);
                if (typeof data.fileRecall != 'undefined') {
                    var file_errors = data.fileRecall;
                    for(var file_error in file_errors){
                       $("#error_fileRecall").append(file_errors[file_error]);
                    }

                }
                if (typeof data.numberRecall != 'undefined') {
                    $("#error_numberRecall").html(data.numberRecall);
                }
                console.log(data);
                if(data == '1'){
                   $('.btn-warning').click();
                   $(".closeDialogRecall").click();;
                }
            }
        }
    };

    xhr.send(formData);
}
