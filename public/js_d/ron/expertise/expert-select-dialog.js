var paramSearch = {
    sFedRegion: $(".sFedRegion").val(),
    sRegion: $(".sRegion").val()
};
/**
 * Сброс данных с формы поиска.
 */
function clearParam(){
    var parent = $('.fields-table').first();
    jQuery( parent ).find('.sOrg option:first ').prop( 'selected' , 'selected' ).change();
    jQuery( parent ).find(".sFedRegion [value='" + paramSearch.sFedRegion + "']").prop("selected", "selected");
    jQuery( parent ).find('.sType option:first ').prop( 'selected' , 'selected' );
    jQuery( parent ).find('.sExperience').val('0');
    jQuery( parent ).find(".sRegion [value='" + paramSearch.sRegion + "']").prop("selected", "selected");
    jQuery( parent ).find('.tPosition').val('');
    jQuery( parent ).find('.sAcademicDegree option:first ').prop( 'selected' , 'selected' );
    jQuery( parent ).find('.sAcademicStatus option:first ').prop( 'selected' , 'selected' );
    jQuery( parent ).find('.tFIO').val('');
    jQuery( parent ).find('.isStrong').prop('checked',false);
    jQuery( parent ).find('.isReligion').prop('checked',false);
    jQuery( parent ).find('.currentOrg').prop('checked',false);
    jQuery( parent ).find('.cbHasStateSecretAccess').prop('checked',false);
    jQuery( parent ).find('.cbFromStateOrg').prop('checked',true);
}