var idTable;
var attributesLines = [];

/**
 * Вешаем обработчики на фильтры.
 */
$(document).ready(function () {
    $('.search-document td .expertSelect').change(function () {
        documentFilterExpertCodeDirection();
    });
    $('.search-document td .codeSelect').change(function () {
        documentFilterExpertCodeDirection();
    });
    $('.search-document td .directionSelect').change(function () {
        documentFilterExpertCodeDirection();
    });


    $('.search-result td .expertSelect').change(function () {
        resultFilterExpertCodeDirectionResult();
    });
    $('.search-result td .codeSelect').change(function () {
        resultFilterExpertCodeDirectionResult();
    });
    $('.search-result td .directionSelect').change(function () {
        resultFilterExpertCodeDirectionResult();
    });
    $('.search-result td .resultSelect').change(function () {
        resultFilterExpertCodeDirectionResult();
    });
});

/**
 * Фильтр по полям на вкладке Документы от эксперта
 */
function documentFilterExpertCodeDirection() {
    var expert = $('.' + idTable + ' .expertSelect').val();
    var code = $('.' + idTable + ' .codeSelect').val();
    var direction = $('.' + idTable + ' .directionSelect').val();

    $('.container table #' + idTable + ' tr').hide();

    if (expert == '' && code == '' && direction == '') {
        $('.container table #' + idTable + ' tr').show();
    } else if (expert == '' && direction == '' && code != '') {
        $('.container table #' + idTable + ' tr[code="' + code + '"]').show();
    } else if (expert == '' && direction != '' && code == '') {
        $('.container table #' + idTable + ' tr[direction="' + direction + '"]').show();
    } else if (expert != '' && direction == '' && code == '') {
        $('.container table #' + idTable + ' tr[expert="' + expert + '"]').show();
    } else if (expert == '' && direction != '' && code != '') {
        $('.container table #' + idTable + ' tr[code="' + code + '"][direction="' + direction + '"]').show();
    } else if (expert != '' && direction != '' && code == '') {
        $('.container table #' + idTable + ' tr[expert="' + expert + '"][direction="' + direction + '"]').show();
    } else if (expert != '' && direction == '' && code != '') {
        $('.container table #' + idTable + ' tr[expert="' + expert + '"][code="' + code + '"]').show();
    } else {
        $('.container table #' + idTable + ' tr[expert="' + expert + '"][code="' + code + '"][direction="' + direction + '"]').show();
    }
}

/**
 * Фильтр по полям на вкладке Результаты экспертизы
 */
function resultFilterExpertCodeDirectionResult() {
    var expert = $('.search-result .expertSelect').val();
    var code = $('.search-result .codeSelect').val();
    var direction = $('.search-result .directionSelect').val();
    var result = $('.search-result .resultSelect').val();

    var searchResult = $('#search-result-tbody');

    searchResult.find('tr').hide();

    if (expert == '' && code == '' && direction == '' && result == '') {
        searchResult.find('tr').show();

    } else if (expert == '' && direction == '' && code == '' && result != '') { // Совпадение по одному показателю
        searchResult.find('tr[result="' + result + '"]').show();
    } else if (expert == '' && direction == '' && code != '' && result == '') {
        searchResult.find('tr[code="' + code + '"]').show();
    } else if (expert == '' && direction != '' && code == '' && result == '') {
        searchResult.find('tr[direction="' + direction + '"]').show();
    } else if (expert != '' && direction == '' && code == '' && result == '') {
        searchResult.find('tr[expert="' + expert + '"]').show();

    } else if (expert == '' && direction == '' && code != '' && result != '') { // Совпадение по двум показателям
        searchResult.find('tr[code="' + code + '"][result="' + result + '"]').show();
    } else if (expert == '' && direction != '' && code == '' && result != '') {
        searchResult.find('tr[direction="' + direction + '"][result="' + result + '"]').show();
    } else if (expert != '' && direction == '' && code == '' && result != '') {
        searchResult.find('tr[expert="' + expert + '"][result="' + result + '"]').show();
    } else if (expert == '' && direction != '' && code != '' && result == '') {
        searchResult.find('tr[code="' + code + '"][direction="' + direction + '"]').show();
    } else if (expert != '' && direction == '' && code != '' && result == '') {
        searchResult.find('tr[expert="' + expert + '"][code="' + code + '"]').show();
    } else if (expert != '' && direction != '' && code == '' && result == '') {
        searchResult.find('tr[expert="' + expert + '"][direction="' + direction + '"]').show();

    } else if (expert == '' && direction != '' && code != '' && result != '') {   // Совпадение по трём показателям
        searchResult.find('tr[direction="' + direction + '"][code="' + code + '"][result="' + result + '"]').show();
    } else if (expert != '' && direction != '' && code != '' && result == '') {
        searchResult.find('tr[expert="' + expert + '"][direction="' + direction + '"][code="' + code + '"]').show();
    } else if (expert != '' && direction != '' && code == '' && result != '') {
        searchResult.find('tr[expert="' + expert + '"][direction="' + direction + '"][result="' + result + '"]').show();
    } else if (expert != '' && direction == '' && code != '' && result != '') {
        searchResult.find('tr[expert="' + expert + '"][code="' + code + '"][result="' + result + '"]').show();
    } else {
        searchResult.find('tr[expert="' + expert + '"][code="' + code + '"][direction="' + direction + '"][result="' + result + '"]').show(); // Совпадение по четырём показателям
    }
}

/**
 * Обновление значения таблицы в которой мроисходит поиск для вкладки Документы от эксперта
 */
function activeTable(id) {
    idTable = id;
}
/**
 *Запоминаем строки в которых изменили select.
 *
 * @param data
 */
function rememberSelectLine(data) {
    attributesLines.push(data);
}

/**
 * Обновляем значения в атрибутов в строке и select.
 */
function updateAttributesLine() {
    var searchResult = $('#search-result-tbody');

    for (var key in attributesLines) {
        var attributes = attributesLines[key];

        // Вытаскиваем значение из select в строке
        var resultValue = searchResult.find(' tr[' +
            'expert="' + attributes.expert + '"]' +
            '[code="' + attributes.code + '"]' +
            '[direction="' + attributes.direction + '"]' +
            '[result="' + attributes.result + '"]'
        ).find('.result-line-select select').val();


        // Обновление атрибутов в строке
        searchResult.find(' tr[' +
            'expert="' + attributes.expert + '"]' +
            '[code="' + attributes.code + '"]' +
            '[direction="' + attributes.direction + '"]' +
            '[result="' + attributes.result + '"]'
        ).attr(
            'result', resultValue
        );

        //Обновление атрибутов onchange в select
        searchResult.find(' tr[' +
            'expert="' + attributes.expert + '"]' +
            '[code="' + attributes.code + '"]' +
            '[direction="' + attributes.direction + '"]' +
            '[result="' + resultValue + '"]'
        ).find('.result-line-select select').attr(
            'onchange', 'rememberSelectLine({' +
                'expert: "' + attributes.expert + '", ' +
                'code: "' + attributes.code + '", ' +
                'direction: "' + attributes.direction + '", ' +
                'result: "' + resultValue + '"' +
                '}); analiticsAcreditationDialogPassedAction(this);'
            );
    }
    attributesLines = [];

    // Обновим список отображающихся линий
    resultFilterExpertCodeDirectionResult();
}