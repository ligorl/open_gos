// проверка данных который пользователь пытается вставить через сочетание клавишь
$('.date_pick').on('paste', function(e) {
    var clipboardData = e.clipboardData || e.originalEvent.clipboardData || window.clipboardData;
    var pastedData = clipboardData.getData('text');
    var dt = new Date( pastedData.replace( /(\d{2}).(\d{2}).(\d{4})/, "$2/$1/$3") );
    if(isNaN(dt.getTime())){
        e.preventDefault();
    }
});