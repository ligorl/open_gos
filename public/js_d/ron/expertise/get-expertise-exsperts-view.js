/**
 * Обновление значения поля total-expert-number.
 * Количество экспертов.
 *
 * @param isCheck
 */
function updateTotalExpertNumber(isCheck) {
    var number = $('.total-expert-number'),
        block = $('.block-total-experts');

    switch(isCheck) {
        case '':
            block.show();
            number.text(totalExperts.total);

            break;
        case 'СonsentConfirm':
            block.show();
            number.text(totalExperts.СonsentConfirm);
            break;
        case 'AgreementSend':
            block.show();
            number.text(totalExperts.AgreementSend);
            break;
        default:
            // block.hide();
            block.show();
            number.text(totalExperts.total);
            break;
    }
}


$(document).ready(function () {

    var countFilial = 0;
    $('.leftTabs .branch-tab').each(function () {
        countFilial++;
    });

    /**
     * Получает активную шапку таблицы
     */
    $('.branch-tab').click(function () {
        var active_tab_id = $(this).attr('branch-id');
        $('.headName').removeClass('active_head_show');
        $('.b_'+active_tab_id).addClass('active_head_show');
        expertsInFilialByStatus(countFilial);

        var getNameStatus = [];
        var getNameByStatus = [];
        var status = $(".active_head_show .commission-filter-only-confirm option:selected").text();
        if(status.indexOf('Все') !== -1){
            $('.total-expert-number').css({'display':'inline-block'});

            $('.intable .content tr').each(function () {
                if($(this).hasClass('commission-one-record')) {
                    $(this).each(function () {
                        getNameStatus.push({
                            name: $('#get_commission_expert_name', this).text().replace(/\s{2,}/g, ' '),
                            status: $('#get_commission_status', this).text().replace(/\s{2,}/g, ' ')
                        });
                    });
                }
            });

            getNameStatus.forEach(function (value, index) {
                if(value.status.indexOf('Получено') !== -1 || value.status.indexOf('Отправлено') !== -1){
                    getNameByStatus.push(
                        value.name
                    );
                }
            });

            var countAllExperts = getUniqueElements(getNameByStatus);

            if(countFilial === 1){
                $('.total-expert-number').css({'display':'none'});
                $('.filial-expert-number').text('Общее количество экспертов: ' + countAllExperts);
            }else{
                $('.filial-expert-number').text('Общее количество экспертов: ' + countAllExperts + ' / ');
            }
        }
    });

});


/**
 * Выводит кол-во экспертов в филиале по статусу
 *
 * @param countFilial
 */
function expertsInFilialByStatus(countFilial) {

    $('.active_head_show').undelegate('.commission-filter-only-confirm', 'change');
    $('.active_head_show').delegate('.commission-filter-only-confirm', 'change', function () {

        var status = $(".active_head_show .commission-filter-only-confirm option:selected").text();
        var getName = [];
        var getCountByStatus = [];
        var getNameStatus = [];
        var getNameByStatus = [];
        var getWithOutExpert = [];

        if (status.indexOf('Получено') !== -1 || status.indexOf('Отказ') !== -1 || status.indexOf('Формуляр') !== -1 || status.indexOf('Отправлено') !== -1) {
            $('.intable .content tr').each(function () {
                if (!$(this).hasClass('confirm-filter-hide')) {
                    $('#get_commission_expert_name', this).each(function () {
                        getName.push($(this).text());
                    });
                }
            });


            $.each(getName, function (index, value) {
                getCountByStatus.push(value.replace(/\s{2,}/g, ' '));
            });

            // var filialExpertCount = jQuery.unique(getCountByStatus).length;
            var filialExpertCount = getUniqueElements(getCountByStatus);

            $('.total-expert-number').css({'display':'none'});

            if (filialExpertCount < 1) {
                $('.filial-expert-number').text('Количество экспертов со статусом ' + status + ':0');
            } else {
                $('.filial-expert-number').text('Количество экспертов со статусом ' + status + ': ' + filialExpertCount);
            }

        }
        else if(status.indexOf('Все') !== -1){
            $('.total-expert-number').css({'display':'inline-block'});
            $('.intable .content tr').each(function () {
                if($(this).hasClass('commission-one-record')) {
                    $(this).each(function () {
                        getNameStatus.push({
                            name: $('#get_commission_expert_name', this).text().replace(/\s{2,}/g, ' '),
                            status: $('#get_commission_status', this).text().replace(/\s{2,}/g, ' ')
                        });
                    });
                }
            });

            getNameStatus.forEach(function (value, index) {
                if(value.status.indexOf('Получено') !== -1 || value.status.indexOf('Отправлено') !== -1){
                    getNameByStatus.push(
                        value.name
                    );
                }
            });

            // var countAllExperts = jQuery.unique(getNameByStatus).length;
            var countAllExperts = getUniqueElements(getNameByStatus);

            if(countFilial === 1){
                $('.total-expert-number').css({'display':'none'});
                $('.filial-expert-number').text('Общее количество экспертов: ' + countAllExperts);
            }else{
                $('.filial-expert-number').text('Общее количество экспертов: ' + countAllExperts + ' / ');
            }
        }
        else if(status.indexOf('Без экспертов') !== -1){
            $('.intable .content tr').each(function () {
                if (!$(this).hasClass('confirm-filter-hide')) {
                    $('#get_commission_expert_name', this).each(function () {
                        getWithOutExpert.push($(this).text());
                    });
                }
            });

            var countWithoutExperts = getWithOutExpert.length;
            $('.total-expert-number').css({'display':'none'});


            if (countWithoutExperts < 1) {
                $('.filial-expert-number').text('Общее количество ОП без экспертов: 0' );
            } else {
                $('.filial-expert-number').text('Общее количество ОП без экспертов: ' + countWithoutExperts);
            }
        }
    });
}

/**
 * Выбираем уникальные элементы массива
 *
 * @param elementsArr - массив всех элементов
 * @returns {Number}
 */
function getUniqueElements(elementsArr){
    var newArray = [];
    for(var i=0, j=elementsArr.length; i<j; i++){
        if(newArray.indexOf(elementsArr[i]) == -1)
            newArray.push(elementsArr[i]);
    }
    return newArray.length;
}