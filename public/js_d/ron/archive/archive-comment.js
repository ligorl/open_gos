$('#commen-new textarea').keypress(function(e){
    var code = e.which;
    console.log(code);
    if(code == 13){
        saveComment();
        return false;
    }

    if(code == 10){
        var startPos = this.selectionStart;
        var endPos = this.selectionEnd;
        var myValue='\n';
        this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
        this.selectionStart=startPos +1;
        this.selectionEnd=this.selectionStart;
    }
});


/**
 * Сохранение комментраия
 */
function saveComment() {
    var commentText = $('#commen-new textarea').val();


    if(commentText[commentText.length-1]=='\n'){
        commentText = commentText.substring(0, commentText.length-1);
    }
    if(commentText == ''||commentText == '\n'){
        $('#commen-new textarea').val('');
        return 0;
    }

    $.ajax({
        url: '/ron/archive/save-comment',
        type: "POST",
        dataType: 'json',
        data: {
            comment : commentText,
            applicationId : "<?=$applicationId?>"
        },
        success: function (answer) {
            if(answer.result==='Ok'){
                var template = $('#comment-template').clone();
                $('.comment-item',template).attr('id_comment', answer.data.commentId);
                $('.comment-user',template).text(answer.data.commentUser);
                $('.comment-content',template).html( answer.data.commentContent);
                $('.comment-date',template).text( answer.data.commentDate);
                $('#comment-area').prepend($(template).html());
            }
            $('#commen-new textarea').val('');
        },
        error:function(e){
            console.log('ошибка связи при отправке комментариев');
            console.log(e);
            useTopHoverTitleError('Возникла ошибки связи');
        }
    });
}


/**
 * Удаление комментария
 *
 * @param item
 * @returns {number}
 */
function commentDelete(item){
    if (!confirm('Хотите удалить этот комментарий')){
        return 0;
    }
    var itItem = jQuery(item).parents('.comment-item');
    var commentId = $(itItem).attr('id_comment');

    $.ajax({
        url:  '/ron/archive/delete-comment',
        type: "POST",
        data: {
            commentId : commentId
        },
        success: function (answer) {
            if(answer === 'Ok'){
                $(itItem).remove();
            }
        },
        error:function(e){
            console.log('ошибка связи при удалении комментариев');
            console.log(e);
            useTopHoverTitleError('Возникла ошибки связи');
        }
    });
}