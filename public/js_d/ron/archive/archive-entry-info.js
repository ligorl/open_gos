
$(document).ready(function () {
    var expertiseId = $("#id_expertise").val(), applicationId = $("#id_application_val").val();
    // getDocumentForExpert(expertiseId);
    getAccreditationProp(expertiseId, applicationId);
    $('.page_archive_info').hide();
    $('.page_archive_info.active').show();
});


$("#list_tabs").undelegate(".show_tab","click");
$("#list_tabs").delegate(".show_tab", "click", function () {
    var activeTab = $(this).attr('tab'),  applicationId = $("#id_application_val").val(), expertiseId = $("#id_expertise").val();
    getActiveTab(activeTab);
    getDataToView(activeTab, applicationId, expertiseId);
});


/**
 * Переключение между вкладками
 */
function getActiveTab(activeTab) {
    $('.page_archive_info').hide();
    $('.page_archive_info.active').removeClass('active');
    $(".show_tab.active").removeClass('active');
    $(".show_tab[tab='" + activeTab + "']").addClass("active");
    $(".tabs-panel").removeClass("show-panel");
    $('#tab'+activeTab).show();
}

/**
 * Выполняет запрос в зависимости от того на какую вкладку перешли
 */
function getDataToView(tab, applicationId, expertiseId) {
    if(tab == '1') {
        getDocumentForExpert(expertiseId);
    }
    if(tab == '4'){
        getDocumentStatement(applicationId);
    }
    if(tab == '11'){
        getAccreditationProp(expertiseId, applicationId);
    }
    if(tab == '3'){
        getSupportingDocument(applicationId);
    }
    if(tab == '5'){
        getArchiveComment(applicationId);
    }
    if(tab == '2'){
        getExpertsDocument(expertiseId);
    }
}


/**
 * Запроос заполняет вкладку Документы заявлений
 *
 *
 *@returns {undefined}
 */
function getDocumentStatement(applicationId) {
    $.ajax({
        url: '/ron/declaration/application-files/'+applicationId,
        data: {
            applicationId:applicationId
        },
        success: function(answer){
            $("#tab4").html(answer);
            $('#button_load_dop_docs').remove();
            var button = '';
            $("#btActions-block","#but_wrap_child_right").append(button);
            // if($(".importantFiles .maySent").length==0 && ($(".active_appl").attr('id_status') === undefined || $(".active_appl").attr('id_status') == '38D871AC-BF1D-469E-AFC1-7A6CD55EF1B6') ){
            //     //$("#sendform1").hide();
            //     //$("#but_wrap_child_right #btActions-block").prepend('<li class="filelink sendApplication"><a onclick="sendToSend()" href="javascript:;"><div></div><span class="text">Сохранить</span></a></li>');
            // }else{
            //     //$(".maySent").parents('.containerWrapFile').css('border','1px solid red');
            //     //$(".maySent").focus();
            //     //$("#but_wrap_child_right #btActions-block .sendApplication").remove();
            // }
            $(".loading-overlay").fadeOut(250);
            //jQuery('.MultiFile-wrap ').hide();
            jQuery('.MultiFile-wrap input').remove();
            jQuery('.MultiFile-remove').remove();
            jQuery('.FileListWrap').addClass('FileListWrapExprtise');
            jQuery("#tab4 .labelText").parent().css('vertical-align','middle');
            //убираем строчки, если нет на них файлов
            jQuery('.FileListWrap').each(function(ind,el){
                var colInBlock = jQuery(el).find('a').length;
                if(colInBlock==0){
                    jQuery(el).parent().parent().parent().remove();
                }
            });
            jQuery("#tab4").find('input[type="file"]').remove();
            jQuery("#tab4").find('.MultiFile-remove').remove();
            jQuery("#tab4").find('.MultiFile-arhive').remove();
            jQuery("#tab4").find('.fileIsArhive ').remove();
            var table = jQuery("#tab4").find('.TableFile tr');
            jQuery.each(table,function(index, value){
                var countFile = jQuery(table[index]).find('.FileListWrap div').length;
                if(countFile == 0){
                    jQuery(table[index]).remove();
                }
            });
            var countDocument = jQuery("#tab4").find('.TableFile tr').length;
            if(countDocument == 0){
                jQuery(jQuery("#tab4").find('.TableFile')[0]).append('<tr class="level"><td colspan="2" style="text-align: center;"> нет данных</td></tr>');
            }
        },
    });
}


/**
 * Запроос заполняет вкладку Документы от экспертов
 *
 * @param expertiseId
 */
function getDocumentForExpert(expertiseId){
    var eContent = $("#tab1");
    $.ajax({
        type: 'POST',
        dataType: "html",
        data: {
            'expertise': expertiseId
        },
        url: "/ron/expertise/get-expertise-document-for-expert",
        success: function (answer) {
            eContent.html(answer);
            // $("input[type=radio]", eContent).change(function(){
            //     var parentTr = $(this).parents("tr").eq(0);
            //     $(".temp-block", eContent).hide();
            //     $(".start-block", eContent).show();
            //
            //     $(".temp-block", parentTr).text($('.results-content tr.selected .expert-name', ".dialog-selectexperts-info").text()).show();
            //     $(".start-block", parentTr).hide();
            // });
            // $(".btCancel",eContent).click(function(){
            //     $(".dialog-add-expert").remove();
            // });
        }
    })
}


/**
 *
 *
 * @param expertiseId
 * @param applicationId
 */
function getAccreditationProp(expertiseId, applicationId) {
    var eContent = $("#tab11");

    $.ajax({
        type: 'POST',
        dataType: "html",
        data: {
            'expertiseId': expertiseId,
            'applicationId': applicationId
        },
        url: "/ron/archive/archive-accreditation-properties",
        success: function (answer) {
            eContent.html(answer);
        }
    })
}

/**
 *
 *
 * @param applicationId
 */
function getSupportingDocument(applicationId) {
    var eContent = $("#tab3");

    $.ajax({
        type: 'POST',
        dataType: "html",
        data: {
            'applicationId': applicationId
        },
        url: "/ron/archive/archive-supporting-document",
        success: function (answer) {
            eContent.html(answer);
        }
    })
}

/**
 *
 * @param applicationId
 */
function getArchiveComment(applicationId) {
    var eContent = $("#tab5");

    $.ajax({
        type: 'POST',
        dataType: "html",
        data: {
            'applicationId': applicationId
        },
        url: "/ron/archive/archive-comment",
        success: function (answer) {
            eContent.html(answer);
        }
    })
}

/**
 * Запроос заполняет вкладку Документы от экспертов
 */
function getExpertsDocument(expertiseId) {
    var eContent = $("#tab2");

    $.ajax({
        type: 'POST',
        dataType: "html",
        data: {
            'expertiseId': expertiseId
        },
        url: "/ron/archive/archive-experts-documents",
        success: function (answer) {
            eContent.html(answer);
        }
    })
}