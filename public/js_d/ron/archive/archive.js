/**
 * Подключил datepicker к фильтру по дате.
 * Проинициализировал функции
 */
$(document).ready(function () {
    getArchiveInfo();
    paginationHandler();
    $(".date-field").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd.mm.yy",
        yearRange: "-100:+100"
    });
});

/**
 * Отправка запроса на сервер, данные для диалог. окна, для записи
 */
function getArchiveInfo() {
    $('.archive_row').dblclick(function () {
        var id = $(this).attr('id'), number = $(this).attr('number'), org_reg_name  = $(this).attr('org_reg_name'), expertise_id = $(this).attr('expertise_id');
        if (id) {
            $.ajax({
                type: 'POST',
                url: '/ron/archive/archive-entry-info',
                data: {
                    id: id,
                    expertiseId: expertise_id,
                    number: number,
                    org_reg_name: org_reg_name
                },
                success: function (answer) {
                    dialogArchiveInfo(answer);
                }
            });
        }
    });
}


/**
 * Генирация диалогового окна, для записи из архива
 *
 * @param answer
 */
function dialogArchiveInfo(answer) {
    var dContent = $('<div class="archive_info"></div>');

    $(dContent).dialog({
        title: 'Аккредитационное дело № 06-AK-' + answer.number + ' ' + answer.org_reg_name,
        width: 1290,
        height: 620,
        position: { my: "center center", at: "center center", of: window },
        modal: true,
        close: function(event, ui) {
            $(this).remove();
        },
        open: function() {
            $(dContent).append(answer.content);
        },
        buttons:{
            'Закрыть':function () {
                if(!window.checkAccredArchive.is(':checked')){
                    if(confirm('Поле "Экспертиза без выезда" не заполнено, Вы уверены, что хотите закрыть окно?')) {
                        $(this).dialog('close');
                    }
                }else {
                    $(this).dialog('close');
                }
            }
        }
    });
}


/**
 * Отправка запроса на сервер
 *
 * @param page - номер страницы пагинации
 *
 */
function sendFilter(page) {
    var pageNum;
    if(page === null){
        pageNum = 1;
    }else {
        pageNum = page;
    }

    var search = {
        'innOrName': $('input[name="stringFilter"]').val() || '',
        'appNumber': $('input[name="appNumberFilter"]').val() || '',
        'page': pageNum
    };
    $.ajax({
        type: 'POST',
        url: '/ron/archive/archive',
        data: search,
        success: function (answer) {
            $("tbody.content").html("").append($(answer).find(".content > tr"));
            $("#pagination").html("").append($(answer).find("#pagination > span"));
            if($(answer).find("#pagination > span").length>0){
                $("#pagination").html("").append($(answer).find("#pagination > span"));
            }
            getArchiveInfo();
            paginationHandler();
        }
    });
}


/**
 * Функция вешает обработчики на пагинацию
 *
 */
function paginationHandler(){
    $(".page","#pagination").unbind('click');
    $(".page","#pagination").click(function(){
        var page = $(this).attr("page");
        sendFilter(page);
    });
}

