
/**
 * Простая надстройка над аяксом
 * возвращает объект с готовыми типами запросов
 * @param {type} setting
 * {
 *    checkLevel :  request||data
 *    это уровень проверки ошибок. При data  обязывает входящий js объект иметь параметр status("ok"-если все нормально),
 *    и параметр data как возвращаеме данные,
 *    и желательно error как код возвращаемой ошибки
 *
 *    rootPath : 'Корневой путь для экшенов',
 *    defaultErrorMessage : 'сообщение об ошибке по ум.'
 * }
 * @returns {_L8}
 */
function _REQUEST( setting){
    var setting = setting || {};

    return new function(){
       var  _setting = {
                checkLevel : 'request', // request||data
                rootPath : '/ron/index/',
                defaultErrorMessage : 'Ошибка! Перезагрузите страницу, и попробуйте еще раз...',
                successMessage : null,
                transformUrl : true
            },
            _status = null,
            dataContainer = {};
    
        settingSetter( _setting, setting)
        //конвертируем get в строку запроса
        function GetObjToStr( getObj){
            var counter = 0;
            getString = '';

            if( empty(getObj)){
                return '';
            }

            for( var key in getObj){
                var elem = getObj[ key];

                if( counter>1){
                    getString += '&';
                }else{
                    getString += '?';
                }
                 getString +=  elem;
            }
            return getString;
        }

        //преобразует имя экшена в часть url
        function actionNamePreProcessor( actionName){
            var actionUrl = '',
                strLen = actionName.length;

             for(var i=0; i<strLen; i++ ){
                 var currentChar = actionName[ i ];

                 if( isUpper(currentChar)){
                    actionUrl += '-'+currentChar.toLowerCase();
                 }else{
                     actionUrl += currentChar;
                 }
             }
             return actionUrl;
        }

        //действие при ошибке
        function whenError( errorMessageOrCallback, _status ,error){
            switch( typeof(errorMessageOrCallback)){
                    case 'function':
                        errorMessageOrCallback( _status, error);
                    break;
                    case 'string':
                        useTopHoverTitle( errorMessageOrCallback, 5000);
                    break;
                    default:
                       useTopHoverTitle( _setting['defaultErrorMessage'], 5000);
                }
        }
        //Действия если все хорошо
        function whenSuccess(){
            switch( typeof( _setting['successMessage'])){
               case 'function':
                   succesMessageOrCallback();
               break;
               case 'string':
                   useTopHoverTitle( _setting['successMessage'], 5000);
               break;
               default:

           }
        }

        //обработка после получения ответа
        function requestPostPostprocessing( errorMessageOrCallback){
            //проверяем на ошибку
            if( _status !=  "success" ){
                whenError( errorMessageOrCallback, _status ,null);
            }else{
                //проверяем нужна ли проверка на уровне данных
                if( _setting['checkLevel'] == 'data' ){
                    if(dataContainer['status'] != 'ok'){
                        whenError( errorMessageOrCallback, _status ,dataContainer['error']);
                    }else{
                        whenSuccess();
                    }

                    if( !empty( dataContainer['data'])){
                        dataContainer = dataContainer['data']
                    }else{
                        console.log('Warning: [data] no detecting in answer!');
                        dataContainer = {};
                    }
                    
                 }else{
                      whenSuccess();
                 }
            }
            return dataContainer;
        }

         /**
          * посылаем синхронный запрос
          * @param {type} actionName
          * имя экшена на который летит запрос(автоматически преобразуеться url виду)
          * @param {type} postData
          * объект данных
          * @param {type} errorMessageOrCallback
          * callback или сообщение об ошибке
          * @param {type} GetData
          * Get параметры
          * @returns {data}
          */
        this.noAsunc = function( actionName, postData,errorMessageOrCallback ,GetData ){
            //очищаем контейнеры
            _status = 'no post';
            dataContainer = {};
            //генерируем url
            var actionUrl = '';
            
            if( _setting['transformUrl'] == false){
                actionUrl = actionName;
            }else{
                actionUrl = actionNamePreProcessor( actionName);
            }

            var url = _setting['rootPath']+actionUrl+GetObjToStr( GetData);

            $.ajax({
                type: 'POST',
                url: url,
                data: postData,
                async:false,

                success:function (data,status){
                    _status = status;
                    dataContainer = data;
                },

                error:function(jqXHR, exception){
                    _status = jqXHR.status;
                    console.log(exception);
                }
            });

            return requestPostPostprocessing( errorMessageOrCallback);
        };
    }
}


/**
 *  запросы на получение страницы
 * @returns {_L89}
 */
function _getPage(){
    return new function( data){
        var request = _REQUEST();

        function checkPageReturned( page){
            if( !empty(page)){
                return page;
            }
            return false;
        }
        
        this.licenseExcerpt = function( page, search, sign){
            return checkPageReturned(
                        request.noAsunc( 'excerpt', {
                                page : page,
                                search : search,
                                sign : sign 
                            })
                    );
        };
    };
}
