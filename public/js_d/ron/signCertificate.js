    /* 
     * url:  /js/ron/signCertificate.js
     * path: /trunk/public/js/ron/signCertificate.js
     */
    
    /**
     * обработка подписания
     *
     * @param {type} element
     * @param {type} documentCode
     * @returns {undefined}
     */
    function certificateSigningFirst( element , documentCode , addData ){
        if( 'undefined' == typeof(addData) || '' == addData ){
            addData ={};
        }

        var parent = jQuery(element).parents('.ui-dialog:first');

        var form=jQuery(parent).find('form');

        var certificateId=jQuery('input[name="Id"]',form).val();

        var url = '/islod/index/sign-dialog/first/';
        var data = {};



        console.log( 'проверка на наличие макета на подписание' );
        if( !jQuery( form ).find( '.SignMaketCertificate a').length ){
            useTopHoverTitleError("Не загружен макет для подписания");
            return 0;
        }

        if( 'undefined' == typeof(documentCode) || '' == documentCode ){
            documentCode = 'certificate';
        }
        var addGetStr = '?isCertificate=1';
        if( 'undefined' != typeof(addData.isSupplement) && addData.isSupplement || 'certificatesupplement' == documentCode.toLowerCase() ){
            addGetStr +='&isSupplement=1';
        }
        var dContent = $('<div class="sign-dialog"></div>');
        console.log('карточка свидетельства - диалог подписания - данные типо провереныы, рисуем')
            $(dContent).dialog({
                    //title: title,
                    width: '95%',
                    //height: heightWindow,
                    //position: 'center',
                    position: { my: "center center", at: "center center", of: window },
                    modal: true,
                    close: function(event, ui) {
                        console.log('карточка лицензии - диалог подписания - закрываем диалог 22');
                        $(this).remove();
                    },
                    open: function() {
                        $.ajax({
                            type: 'POST',
                            data: data,
                            url: url+certificateId+addGetStr,
                            beforeSend: function(){
                                //ресайзимблок с контентом
                                resizeContent($(dContent));
                                $(dContent).html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
                                //$(".content_td .content").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);            //
                            },
                            success: function(answer) {
                                console.info('данные получены');
                                jQuery(dContent).html('');
                                jQuery(dContent).append(answer).fadeIn(500);
                                jQuery('.dialog-cancel',dContent).click(function(){
                                    var uiDialog = jQuery(jQuery(this).parents('.ui-dialog-content')[0]);
                                    jQuery(uiDialog).dialog('close');
                                });
                                resizeContent(".license-stop-suplement-cheked-parent .viewzone");
                                specialTableResize(".license-stop-suplement-cheked-parent");
                                //выцентруем окошко по высоте
                                var heithWindow = jQuery(window).height();
                                var heithDialog = jQuery(dContent).parent().height();
                                var top = (heithWindow - heithDialog) / 2;
                                jQuery(dContent).parent().css('top',top);

                            },
                            error:function(e){
                                jQuery(dContent).html('Ошибка связи при загрузке');
                                console.error( 'ошибка при загрузке диалога');
                                console.log(e);
                                console.log(e.responseText);
                                useTopHoverTitleError("Ошибка связи")
                            }
                        });
                    },
                    buttons:[
                        {
                            text: "Да",
                            "class": 'right ',
                            click: function () {
                                signStepFirstOk();

                            }
                        },
                        {
                            text: "Нет",
                            "class": 'right',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    ]
            });



    }

    /**
     * обработка подписания
     *
     * @param {type} element
     * @param {type} documentCode
     * @returns {undefined}
     */
    function certificateSigningFirstExcertTable( element , documentCode , addData ){
        if( 'undefined' == typeof(addData) || '' == addData ){
            addData ={};
        }

        var form = jQuery(element).parents('form');

        var certificateId=jQuery('input[name="Id"]',form).val();

        var url = '/islod/index/sign-dialog/first/';
        var data = {};



        console.log( 'проверка на наличие макета на подписание' );
        if( !jQuery( form ).find( '.SignMaketCertificate a').length ){
            useTopHoverTitleError("Не загружен макет для подписания");
            return 0;
        }

        if( 'undefined' == typeof(documentCode) || '' == documentCode ){
            documentCode = 'certificate';
        }
        var addGetStr = '?isCertificate=1';
        if( 'undefined' != typeof(addData.isSupplement) && addData.isSupplement || 'certificatesupplement' == documentCode.toLowerCase() ){
            addGetStr +='&isSupplement=1';
        }
        var dContent = $('<div class="sign-dialog"></div>');
        console.log('карточка свидетельства - диалог подписания - данные типо провереныы, рисуем')
            $(dContent).dialog({
                    //title: title,
                    width: '95%',
                    //height: heightWindow,
                    //position: 'center',
                    position: { my: "center center", at: "center center", of: window },
                    modal: true,
                    close: function(event, ui) {
                        console.log('карточка лицензии - диалог подписания - закрываем диалог 22');
                        $(this).remove();
                    },
                    open: function() {
                        $.ajax({
                            type: 'POST',
                            data: data,
                            url: url+certificateId+addGetStr,
                            beforeSend: function(){
                                //ресайзимблок с контентом
                                resizeContent($(dContent));
                                $(dContent).html("").append($("<div class='loading-img'><img src='/img/loading.gif' /></div>")).fadeIn(500);
                                //$(".content_td .content").html("").append($("<div class='loading-overlay'><div class='windows8'><div class='wBall' id='wBall_1' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_2' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_3' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div><div class='wBall' id='wBall_4' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div>   <div class='wBall' id='wBall_5' style='width: 32px; height: 32px;'><div class='wInnerBall' style='width: 4.71428571428571px; height: 4.71428571428571px;'></div></div></div><div class='loading-overlay-message'>Загрузка...</div></div>")).fadeIn(500);            //
                            },
                            success: function(answer) {
                                console.info('данные получены');
                                jQuery(dContent).html('');
                                jQuery(dContent).append(answer).fadeIn(500);
                                jQuery('.dialog-cancel',dContent).click(function(){
                                    var uiDialog = jQuery(jQuery(this).parents('.ui-dialog-content')[0]);
                                    jQuery(uiDialog).dialog('close');
                                });
                                resizeContent(".license-stop-suplement-cheked-parent .viewzone");
                                specialTableResize(".license-stop-suplement-cheked-parent");
                                //выцентруем окошко по высоте
                                var heithWindow = jQuery(window).height();
                                var heithDialog = jQuery(dContent).parent().height();
                                var top = (heithWindow - heithDialog) / 2;
                                jQuery(dContent).parent().css('top',top);

                            },
                            error:function(e){
                                jQuery(dContent).html('Ошибка связи при загрузке');
                                console.error( 'ошибка при загрузке диалога');
                                console.log(e);
                                console.log(e.responseText);
                                useTopHoverTitleError("Ошибка связи")
                            }
                        });
                    },
                    buttons:[
                        {
                            text: "Да",
                            "class": 'right ',
                            click: function () {
                                signStepFirstOk();

                            }
                        },
                        {
                            text: "Нет",
                            "class": 'right',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    ]
            });



    }

    /**
    * украдено
    * /srv/www/work/f11ron_mon/trunk/public/js/islod/namespace/license.js
    *
     * тык по кнопке подписать
     * шаг 2
     * @returns {Number|undefined}
     */
    function signStepFirstOk(){
        console.log('диалог подписания - начинаем подписание документа');
        var positiv = function(){
            //сброси значения документа
            fileContent = undefined;
            //соберем анные сертификата
            var data = {};
            data['subject']         = jQuery('#subject b').text();
            data['issuer']          = jQuery('#issuer b').text();
            data['from']            = jQuery('#from b').text();
            data['till']            = jQuery('#till b').text();
            data['provname']        = jQuery('#provname b').text();
            data['algorithm']       = jQuery('#algorithm b').text();
            data['serialNumber']    = jQuery('#serialNumber b').text();

            console.log(data);
            var fileId = jQuery(".sign-first-dialog-parent input[name='fileId']").val();
            if ('undefined' == typeof(fileId) || '' == fileId){
                alert('нет ид');
                return;
            }
            //проверим не приложение ли
            var Addad = '';
            //проверим не приложение ли
            var isSupplement = jQuery(".sign-first-dialog-parent input[name='isSupplement']").val();
            if ('undefined' != typeof(isSupplement)  && '' != isSupplement){
                isSupplement = 'isSupplement=1';
            }
            else{
                isSupplement = '';
            }
            var isCertificate = jQuery(".sign-first-dialog-parent input[name='isCertificate']").val();
            if ('undefined' != typeof(isCertificate)  && '' != isCertificate){
                isCertificate = 'isCertificate=1';
            }
            else{
                isCertificate = '';
            }
            if( '' == isSupplement || '' == isCertificate ){
                if( '' == isSupplement && '' == isCertificate ){

                }
                else{
                    fileId += '?'+isSupplement+isCertificate;
                }
            }
            else{
                fileId += '?'+isSupplement+'&'+isCertificate;
            }

            $.ajax({
                type: 'POST',
                data: data,
                url: '/islod/index/sign-dialog/second/'+fileId,
                dataType: 'json',
                beforeSend: function(){
                    //повесить крутилку и блокировку
                },
                success: function(answer) {
                    console.log(answer);
                    if('Ok' == answer.result ){
                        fileContent =  answer.fileData;
                        console.info('подписываем');
                        Common_SignCadesBES_File('CertListBox');
                        console.info('подписано');


                        var elementToChenge;
                        var link;
                        var fileName;
                        //перебить ссылки на подпись и на подписанный документ
                        if( 'undefined' != typeof(answer.licenseXmlUrl) && '' != answer.licenseXmlUrl
                         && 'undefined' != typeof(answer.licenseXmlFile) && '' != answer.licenseXmlFile
                        ){
                            elementToChenge = jQuery('.license-xml-link');
                            link = answer.licenseXmlUrl;
                            fileName = answer.licenseXmlFile;
                            jQuery(elementToChenge).removeClass('hiden');
                            jQuery(elementToChenge).attr('href',link);
                            jQuery(elementToChenge).attr('download',''+fileName);
                            jQuery(elementToChenge).text(fileName);
                        }
                        if( 'undefined' != typeof(answer.supplementXmlUrl) && '' != answer.supplementXmlUrl
                         && 'undefined' != typeof(answer.supplementXmlFile) && '' != answer.supplementXmlFile
                        ){
                            elementToChenge = jQuery('.supplement-xml-link');
                            link = answer.supplementXmlUrl;
                            fileName = answer.supplementXmlFile;
                            jQuery(elementToChenge).removeClass('hiden');
                            jQuery(elementToChenge).attr('href',link);
                            jQuery(elementToChenge).attr('download',''+fileName);
                            jQuery(elementToChenge).text(fileName);
                        }


                    }else{
                        console.error(answer.console);
                        useTopHoverTitleError(answer.message);
                    }
                },
                error:function(e){
                    console.error( 'ошибка при загрузке диалога');
                    console.log(e);
                    console.log(e.responseText);
                    useTopHoverTitleError("Ошибка связи");
                }
            });
        }

        //проверим данные
        if(!jQuery('#CertListBox option').length){
            useTopHoverTitleError('Выберите сертификат');
            return 0;
        }
        if(!jQuery('#CertListBox option:selected').length){
            useTopHoverTitle('Выберите сертификат');
            return 0;
        }
        confirmLod( 'Хотите подписать документ?', positiv );
    }

    /**
     * запускаеться после удачного подписания
     * 
     * @returns {undefined
     */
    SignedFileOkFunctionOk = function(){}

    /**
    * украдено
    * /srv/www/work/f11ron_mon/trunk/public/js/islod/namespace/license.js
    *
     * запускаутся после успешного подписания данных
     * шаг 3й
     *
     * @returns {undefined}
     */
  Common_SignedFileOk = function(){
        //отправим подпись на сервер
        var data = {};
        data['sign'] = document.getElementById("SignatureTxtBox").innerHTML;
        if( 'undefined' == data['sign'] || '' == data['sign'] ){
            useTopHoverTitleError('нет подписии');
        }
        var fileId = jQuery(".sign-first-dialog-parent input[name='fileId']").val();
        if ('undefined' == fileId || '' == fileId){
            alert('нет файла');
            return 0;
        }
        //проверим не приложение ли
            var Addad = '';
            //проверим не приложение ли
            var isSupplement = jQuery(".sign-first-dialog-parent input[name='isSupplement']").val();
            if ('undefined' != typeof(isSupplement)  && '' != isSupplement){
                isSupplement = 'isSupplement=1';
            }
            else{
                isSupplement = '';
            }
            var isCertificate = jQuery(".sign-first-dialog-parent input[name='isCertificate']").val();
            if ('undefined' != typeof(isCertificate)  && '' != isCertificate){
                isCertificate = 'isCertificate=1';
            }
            else{
                isCertificate = '';
            }
            if( '' == isSupplement || '' == isCertificate ){
                if( '' == isSupplement && '' == isCertificate ){

                }
                else{
                    fileId += '?'+isSupplement+isCertificate;
                }
            }
            else{
                fileId += '?'+isSupplement+'&'+isCertificate;
            }
        $.ajax({
            type: 'POST',
            data: data,
            url: '/islod/index/sign-dialog/three/'+fileId,
            dataType: 'json',
            beforeSend: function(){
                //повесить крутилку и блокировку
            },
            success: function(answer) {
                console.log(answer);
                if('Ok' == answer.result ){
                    console.info('диалог подписания- подпись внесена 66');
                    console.log(answer);
                    var elementToChenge;
                    var link;
                    var fileName;
                    //перебить ссылки на подпись и на подписанный документ
                    if( 'undefined' != typeof(answer.documentUrl) && '' != answer.documentUrl
                     && 'undefined' != typeof(answer.documentFile) && '' != answer.documentFile
                    ){
                        elementToChenge = jQuery('.license-document-link');
                        link = answer.documentUrl;
                        fileName = answer.documentFile;
                        jQuery(elementToChenge).removeClass('hiden');
                        jQuery(elementToChenge).attr('href',link);
                        jQuery(elementToChenge).attr('download',''+fileName);
                        //jQuery(elementToChenge).text(fileName);
                    }
                    if( 'undefined' != typeof(answer.signUrl) && '' != answer.signUrl
                     && 'undefined' != typeof(answer.signFile) && '' != answer.signFile
                    ){
                        elementToChenge = jQuery('.license-sign-link');
                        link = answer.signUrl;
                        fileName = answer.signFile;
                        jQuery(elementToChenge).removeClass('hiden');
                        jQuery(elementToChenge).attr('href',link);
                        jQuery(elementToChenge).attr('download',''+fileName);
                        //jQuery(elementToChenge).text(fileName);
                    }
                    if( 'undefined' != typeof(answer.supplementDocumentUrl) && '' != answer.supplementDocumentUrl
                     && 'undefined' != typeof(answer.supplementDocumentFile) && '' != answer.supplementDocumentFile
                    ){
                        elementToChenge = jQuery('.supplement-document-link');
                        link = answer.supplementDocumentUrl;
                        fileName = answer.supplementDocumentFile;
                        jQuery(elementToChenge).removeClass('hiden');
                        jQuery(elementToChenge).attr('href',link);
                        jQuery(elementToChenge).attr('download',''+fileName);
                        //jQuery(elementToChenge).text(fileName);
                    }
                    if( 'undefined' != typeof(answer.supplementSignUrl) && '' != answer.supplementSignUrl
                     && 'undefined' != typeof(answer.supplementSignFile) && '' != answer.supplementSignFile
                    ){
                        elementToChenge = jQuery('.supplement-sign-link');
                        link = answer.supplementSignUrl;
                        fileName = answer.supplementSignFile;
                        jQuery(elementToChenge).removeClass('hiden');
                        jQuery(elementToChenge).attr('href',link);
                        jQuery(elementToChenge).attr('download',''+fileName);
                        //jQuery(elementToChenge).text(fileName);
                    }
                    //свидетельство
                    if( 'undefined' != typeof(answer.certificateDocumentUrl)  && '' != answer.certificateDocumentUrl
                     && 'undefined' != typeof(answer.certificateDocumentFile) && '' != answer.certificateDocumentFile
                    ){
                        elementToChenge = jQuery('.dialog-certificate-edit .content-block:visible .SignDcumentCertificate');
                        if( !(elementToChenge.length) ){
                            elementToChenge = jQuery('.dialog-certificate-info .content-block:visible .SignDcumentCertificate');
                        }
                        link = answer.certificateDocumentUrl;
                        fileName = answer.certificateDocumentFile;
                        jQuery(elementToChenge).find('a').remove();
                        jQuery(elementToChenge).append('<a target="_blank" href="'+link+'" >'+fileName+'</a>');
                        //jQuery(elementToChenge).text(fileName);
                    }
                    if( 'undefined' != typeof(answer.certificateSignUrl)  && '' != answer.certificateSignUrl
                     && 'undefined' != typeof(answer.certificateSignFile) && '' != answer.certificateSignFile
                    ){
                        elementToChenge = jQuery('.dialog-certificate-edit .content-block:visible .SignSignCertificate');
                        if( !(elementToChenge.length) ){
                            elementToChenge = jQuery('.dialog-certificate-info .content-block:visible .SignSignCertificate');
                        }
                        link = answer.certificateSignUrl;
                        fileName = answer.certificateSignFile;
                        jQuery(elementToChenge).find('a').remove();
                        jQuery(elementToChenge).append('<a target="_blank" href="'+link+'" >'+fileName+'</a>');
                        //jQuery(elementToChenge).text(fileName);
                    }
                    //приложения свидетельства
                    if( 'undefined' != typeof(answer.certificateSupplementDocumentUrl)  && '' != answer.certificateSupplementDocumentUrl
                     && 'undefined' != typeof(answer.certificateSupplementDocumentFile) && '' != answer.certificateSupplementDocumentFile
                    ){
                        elementToChenge = jQuery('.dialog-certsupplement-edit .SignDcumentCertificate');
                        if( !(elementToChenge.length) ){
                            elementToChenge = jQuery('.dialog-certsupplement-info  .SignDcumentCertificate');
                        }
                        link = answer.certificateSupplementDocumentUrl;
                        fileName = answer.certificateSupplementDocumentFile;
                        jQuery(elementToChenge).find('a').remove();
                        jQuery(elementToChenge).append('<a target="_blank" href="'+link+'" >'+fileName+'</a>');
                        //jQuery(elementToChenge).text(fileName);
                    }
                    if( 'undefined' != typeof(answer.certificateSupplementSignUrl)  && '' != answer.certificateSupplementSignUrl
                     && 'undefined' != typeof(answer.certificateSupplementSignFile) && '' != answer.certificateSupplementSignFile
                    ){
                        elementToChenge = jQuery('.dialog-certsupplement-edit .SignSignCertificate');
                        if( !(elementToChenge.length) ){
                            elementToChenge = jQuery('.dialog-certsupplement-info .SignSignCertificate');
                        }
                        link = answer.certificateSignUrl;
                        fileName = answer.certificateSupplementSignFile;
                        jQuery(elementToChenge).find('a').remove();
                        jQuery(elementToChenge).append('<a target="_blank" href="'+link+'" >'+fileName+'</a>');
                        //jQuery(elementToChenge).text(fileName);
                    }
                    //закрыть диалог
                    useTopHoverTitle("Данные успешно внесены 44");
                    if( 'function' == typeof( SignedFileOkFunctionOk ) ){
                        console.info( 'подписание  --  есть функция после подписания' );
                        SignedFileOkFunctionOk();
                    }
                    jQuery('.sign-first-dialog-parent').parent().dialog('close');
                    if($('.excerpt-content').length>0){
                        excerpt.getLicenses();
                    }
                }else{
                    console.error('диалог подписания - подписание - второй шаг - получен не корректный ответ');
                    console.log(answer.console);
                    useTopHoverTitleError(answer.message);
                }
            },
            error:function(e){
                console.error( 'диалог подписания - подписание - второй шаг - ошибка при загрузке');
                console.log(e);
                console.log(e.responseText);
                useTopHoverTitleError("Ошибка связи");
            },
            complete:function(){
                //очистим данные подписаного файла
                fileContent = undefined;
            }
        });
    }


    /**
     * сохранить и распечатать
     *
     * @param {type} element
     * @param {type} documentCode
     * @returns {certificateTemplatePrint}
     */
    function certificateTemplatePrint(element , documentCode ){
        if( 'undefined' == typeof(documentCode) || '' == documentCode ){
            documentCode = 'certificate';
        }
        var form=jQuery(".dialog-certificate-edit .content-block:visible").find('form');
        var certificateId=jQuery('input[name="Id"]',form).val();


        /*
        //решение тупо в лоб
            jQuery('.sldkfdhjmsdfjkhsdjhsdfsdffaksdjjdds').remove();
            jQuery( 'body' ).append(
                '<form '
                    +' class="sldkfdhjmsdfjkhsdjhsdfsdffaksdjjdds" '
                    //+' style="display:none"  '
                    +' target="_blank" '
                    +' action="/ron/declaration/certificate-document/'+documentCode+'/'+certificateId+'" '
                +' ><input class="sldkfdhjmsdfjkhsdjhsdfsdffaksdjjdds-input"  type="submit" ></form>'
            );
        setTimeout( function(){
            console.log('переход на ссылку на получени макета таймером');
            //вариант простой, но иногда отсекает блокировака всплывающих окон
            window.open('/ron/declaration/certificate-document/'+documentCode+'/'+certificateId, '_blank');
            //поэтому геморимся
            jQuery('.cert-maket-print-after-save-to-form-input').click();
            jQuery(jQuery('.cert-maket-print-after-save-to-form-input')[0]).click();
            jQuery('.cert-maket-print-after-save-to-form-input').trigger('click');
            jQuery(jQuery('.cert-maket-print-after-save-to-form-input')[0]).trigger('click');
        }, 2000  );
        //по идее отрабатывалка посл есохранения, но болт, не рботает открытвалка вкладки
        var doPrintAfterSave=function (){

            console.log(document)
            console.log(document.parent)
            console.log(jQuery('.cert-maket-print-after-save-to-form-input'))

            console.log('переход на ссылку на получени макета');
            window.open('/ron/declaration/certificate-document/'+documentCode+'/'+certificateId, '_blank');
            jQuery('.cert-maket-print-after-save-to-form-input').click();
            jQuery(jQuery('.cert-maket-print-after-save-to-form-input')[0]).click();
            jQuery('.cert-maket-print-after-save-to-form-input').trigger('click');
            jQuery(jQuery('.cert-maket-print-after-save-to-form-input')[0]).trigger('click');
        };
        */

        doPrintAfterSave=function(){
            document.location = "/ron/declaration/certificate-document/"+documentCode+"/"
                    + certificateId;
        };



        //Впихиваем дату печати
        var curDatePrint = jQuery('input[name="DatePrint"]',form).val();
        console.log("'"+curDatePrint+"'");
        var date = new Date();
        if (curDatePrint==''){
            var mount=(date.getMonth()+1);
            if (mount<10){
                mount = '0'+mount;
            }
            curDatePrint = date.getDate()+'.'+mount+'.'+date.getFullYear();
            console.log("'"+curDatePrint+"'");
            jQuery('input[name="DatePrint"]',form).val(curDatePrint);
        }

        //doPrintAfterSave = jQuery('.cert-maket-print-after-save-to-form-input').click;

        certificateTemplateSave(element , doPrintAfterSave  );
    }


