<?php
/**
 * Esia authentication through omniauth protocol
 * based on https://esia.gosuslugi.ru
 *
 * More information on OAuth: http://rnds.pro
 *
 * @copyright    Copyright © 2014-2017 Rnd Soft (http://rnds.pro)
 * @link         http://rnds.pro
 * @package      EsiaOmniAuth
 * @license      MIT License
 **/

class EsiaOmniAuth {

  /**
   * save to log debug info
   **/
  const DEBUG = true;


  /**
   * UUID for request
   **/
  public $uuid;

  /**
   * Config object
   **/
  public $config;

  /**
	 * Compulsory config keys, listed as unassociative arrays
   * site:       'https://esia-portal1.test.gosuslugi.ru/',
   * auth_uri:   'http://localhost:3000/auth/callback',
   * logout_uri: 'http://localhost:3000/',
   * client_id:  'GRST01611',
   * scope:      'openid http://esia.gosuslugi.ru/usr_brf http://esia.gosuslugi.ru/usr_inf',
   * cert_path:  "${keys_path}/cert.crt",
   * pkey_path:  "${keys_path}/secret.key",
   * use_curl:   'Y',
   * openssl_utility_path: 'C:\bin\utilities\OpenSSL\openssl.exe'
   **/
	private $expects = array('site', 'logout_uri', 'auth_uri', 'client_id', 'scope', 'cert_path', 'pkey_path', 'use_curl', 'openssl_utility_path');

  /**
   * Signed file path
   **/
  public $temp_dir;

  /**
   * Constructor of class's
   **/
	public function __construct($config = array()) {

    /* compare arrays keys config or empty $config **/
    if(sizeof($config) == 0 || sizeof(array_diff(array_keys($config), $this->expects)) > 0) {
      $this->configError();
      exit();
    }
    else { /* set config **/
      $this->config = $config;
      $this->temp_dir = realpath(dirname(__FILE__)) . "/temp";
    }

  }

  /**
   * Destructor of class's
   **/
  public function __destruct() {

    if (file_exists($this->signed_file()) && !DEBUG) {
      unlink($this->signed_file());
    }

    if (file_exists($this->for_sign_file()) && !DEBUG) {
      unlink($this->for_sign_file());
    }
  }

	/**
	 * Create request and redirect to esia site
	 **/
  public function create() {

    $this->uuid = $this->generate_GUID();
    $time = $this->strftimeWithTimeZone();
    $params = array();

    //query format
    $params["client_id"]     = $this->config["client_id"];
    $params["redirect_uri"]  = $this->config["auth_uri"];
    $params["client_secret"] = "";
    $params["scope"]         = $this->config["scope"];
    $params["response_type"] = 'code';
    $params["state"]         = $this->uuid;
    $params["timestamp"]     = $time;
    $params["access_type"]   = 'online';

    //signing params
    $client_secret = $this->sign( $params );

    if($client_secret){
      $params["client_secret"] = $client_secret;

      //get code
      $_SESSION['esia_oauth_state'] = $this->uuid;
      $url = $this->config['site']."/aas/oauth2/ac".'?'.http_build_query($params);

      $this->redirect($url);
    }
    else{

        $this->debugSave($client_secret, "Can't sign!!!");
    }
  }

	/**
	 * Create request and redirect to esia site
   * @param code is code has gotten from create function
   * @return access_token
	 **/
  public function get_token($code) {
    
    $this->debugSave($this->strftimeWithTimeZone(), "Get token started...");
    
    $this->debugSave($code, "Autorization code:");
    
    $tokenIsExpires = isset($_SESSION['esia_oauth_expires_in']) && isset($_SESSION['esia_oauth_refresh_token']) && (time() >= $_SESSION['esia_oauth_expires_in']);
    $this->debugSave($tokenIsExpires ? 'expires' : 'valid', "Current access token state (if it is not empty):");

    $this->uuid = $this->generate_GUID();
    $time = $this->strftimeWithTimeZone();
    $params = array();

    //query format
    $params["client_id"]= $this->config["client_id"];
    $params["redirect_uri"]  = $this->config["auth_uri"];
    $params["client_secret"] = "";
    $params["scope"] = $this->config["scope"];
    $params["state"] = $this->uuid;
    $params["timestamp"] = $time;

    //differents
    if($tokenIsExpires) $params['refresh_token'] = $_SESSION['esia_oauth_refresh_token'];
    else $params["code"]= $code;
    $params["grant_type"]= $tokenIsExpires ? 'refresh_token' : 'authorization_code';
    $params["token_type"] = 'Bearer';

    //signing params
    $client_secret = $this->sign( $params );

    if($client_secret)
    {
      $this->debugSave($this->strftimeWithTimeZone(), "Sign is correct...");
      $params["client_secret"] = $client_secret;

      //get token
      $url = $this->config['site'] . "/aas/oauth2/te";
      $this->debugSave($url, "URL for request token:");

      $token_query = http_build_query($params);
      $this->debugSave($token_query, "Query for request token:");

      $response = $this->post($url, $token_query);
      if($response === false)
      {
          $this->debugSave("Function file_get_contents() could not be execute for current context or optional parameter allow_url_fopen is not set (option 'fopen wrappers' must be ON)...", "Request access token error:");

          return false;
      }
      else
      {
          $json = json_decode($response);
          $access_token = $json->access_token;

          $this->debugSave($access_token, "Access token:");
          $this->debugSave($json->expires_in, "Expires in:");
          $this->debugSave($json->state, "State:");
          $this->debugSave($json->token_type, "Token type:");
          $this->debugSave($json->refresh_token, "Refresh token:");

          $_SESSION['esia_oauth_expires_in'] = time() + $json->expires_in;
          $_SESSION['esia_oauth_refresh_token'] = $json->refresh_token;

          if($access_token == '')
          {
            $this->debugSave('Access token is empty...', "ESIA access token erorr:");
            trigger_error('ESIA access token erorr: [Access token is empty...]', E_USER_ERROR);
          }

          return $access_token;
      }

    }
    else
    {

      $this->debugSave($client_secret, "Can't sign!!!");

      return false;
    }

  }

  /**
   * Get info obout user
   * @param access_token has gotten by  get_token function
   * @return an array with two elements user_contacts and user_info that is contain JSON objects
   **/
  public function get_info($access_token) {
    
    $this->debugSave($this->strftimeWithTimeZone(), "Get info started...");

    $header = "Authorization:  Bearer ${access_token}";
    $this->debugSave($header, "Request header:");

    //get user_id from token
    $splited = explode(".", $access_token);
    $decoded = base64_decode($this->url_data_decode($splited[1]));
    $json = json_decode($decoded);

    $user_id = $json->{'urn:esia:sbj_id'};

    //get user info
    $info_url = $this->config["site"] . "/rs/prns/${user_id}";
    $user_info = $this->get($info_url, $header);

    // get contacts
    $contact_url = $this->config["site"] . "/rs/prns/${user_id}/ctts?embed=(elements)";
    $user_contacts = $this->get($contact_url, $header);

    // get contacts
    $orgs_url = $this->config["site"] . "/rs/prns/${user_id}/roles";
    $user_orgs = $this->get($orgs_url, $header);
    
    
    //get user all documents that contain a {doc_id} by {user_id}
    $docs_list_url = $this->config["site"] . "/rs/prns/${user_id}/docs?embed=(elements)";
    $docs_list = $this->get($docs_list_url, $header);
    
    return array(
                  'user_id'       => $user_id,
                  'user_info'     => $user_info,
                  'user_contacts' => $user_contacts,
                  'docs_list'     => $docs_list,
                  'user_orgs'     => $user_orgs
                );
  }

   /**
    * Create logout url for ESIA service and send POST to server
    * @param none
    * @return none
    **/
  public function request_logout() {
    
    $query_format = '%sidp/ext/Logout?%s';

    $query_data = array();
    $query_data['client_id'] = $this->config['client_id'];
    $redirect_url = $this->config['logout_uri'];
    if($redirect_url != ''){ //that URL on which will make redirect after logout, if parameter is empty then ESIA make redirect on URL set by default 
      $query_data['redirect_uri'] = $redirect_url;
    }
    $this->debugSave('Client id = '.$query_data['client_id'].' | Redirect url = '.$query_data['redirect_uri'], "Logout parameters:");
    
    $logout_url = sprintf($query_format, $this->config['site'], http_build_query($query_data));
    $this->debugSave($logout_url, "Logout URL:");

    $this->redirect($logout_url);

    return;
  }

  /********** Private area **********/

  /**
   * Does sign PKSC7
   * @param $params containts config for sign
   * @return get result of signing it can be a false or decoded content of sign file
   **/
  private function sign($params) {
    
    $result = false;

    $secret = $params["scope"] . $params["timestamp"] . $params["client_id"] . $params["state"];
    $this->debugSave($secret, "Secret:");
    $this->save_for_sign($secret);

    $pkey = $this->config['pkey_path'];
    $cert = $this->config['cert_path'];
    
    $this->debugSave($pkey, "Key file path:");
    $this->debugSave($cert, "Cert file path:");

    $this->debugSave(file_get_contents($pkey), "Key file contents:");
    $this->debugSave(file_get_contents($cert), "Cert file contents:");

    $outfile = $this->signed_file();
    $this->debugSave($outfile, "Signed file path:");

    $infile = $this->for_sign_file();
    $this->debugSave($infile, "For signed file path:");

    if($this->config['openssl_utility_path'] == '') {

      $command = "openssl smime -sign -in ${infile} -signer ${cert} -inkey ${pkey} -outform DER -out ${outfile} > /dev/null 2>&1";
    }
    else {

      $openssl_utility_path = $this->config['openssl_utility_path'];

      if( file_exists($openssl_utility_path) ) {

        $command = "\"${openssl_utility_path}\" smime -sign -in \"${infile}\" -signer \"${cert}\" -inkey \"${pkey}\" -outform DER -out \"${outfile}\"";
        $command = str_replace('/','\\', $command);
      }
      else {

        $this->debugSave($openssl_utility_path, "OpenSSL utility exec file is not exists...");
        trigger_error('ESIA sign erorr: [OpenSSL utility exec file is not exists...]', E_USER_ERROR);
      }
    }

    $this->debugSave($command, "Exec command for signing file:");
    exec($command);

    $this->debugSave(file_get_contents($this->for_sign_file()), "For signed file contents:");
    $this->debugSave(file_get_contents($this->signed_file()), "Signed file contents:");
    
    if( file_exists($outfile) ) {

      $encoded = base64_encode(file_get_contents($this->signed_file()));
      $result = urlencode($this->url_data_encode($encoded));
    }
    
    return $result;
  }

  /**
   * Virify signature by PKCS7 (this function is not use in this module)
   * @param $params thay define needed parameters
   * @return result of veifying
   **/
  private function verify_signature($params) {

    $secret = $params["scope"] . $params["timestamp"] . $params["client_id"] . $params["state"];
    return openssl_pkcs7_verify($this->signed_file(), PKCS7_NOVERIFY);
  }

  /**
   * Change data accorded with esia standart
   **/
  private function url_data_encode($data) {

    $data = str_replace(array('+', '/'), array('-', '_'), $data);
    $data = preg_replace('/[=]+$/', '', $data);
    return $data;
  }

  /**
   * Change data accorded with esia standart
   **/
  private function url_data_decode($data) {

    return str_replace(array('-', '_'), array('+', '/'), $data);
  }

  /**
   * Generate uuid
   * @param $prefix default '_'
   **/
  private static function generate_GUID($prefix='') {

    $uuid = md5(uniqid(rand(), true));
    $guid =  $prefix.substr($uuid,0,8)."-".
            substr($uuid,8,4)."-".
            substr($uuid,12,4)."-".
            substr($uuid,16,4)."-".
            substr($uuid,20,12);
    return $guid;
   }

  /**
   * Get formated date with time zone +0000
   * @return GM date
   **/
  protected function strftimeWithTimeZone() {

     return gmdate("Y.m.d G:i:s O"); // gmdate - get Grinvich date time +0000, but need with time zone +0400 for example
  }


  /**
   * Debug
   **/
  private static function debugSave($content, $notice = '') {

    if(self::DEBUG){
      $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL.
              $notice.PHP_EOL.
              $content.PHP_EOL.
              "-------------------------".PHP_EOL;
      //Save string to log, use FILE_APPEND to append.
      $full_name_log_file = self::debug_file_path() . 'class_log.log';
      file_exists($full_name_log_file) ? file_put_contents($full_name_log_file, $log, FILE_APPEND) : file_put_contents($full_name_log_file, $log);
    }
  }

  private static function debug_file_path() {

    return realpath(dirname(__FILE__)) . '/log/';
  }

  protected function configError(){

    trigger_error('You should set $config. Example: "new Esia(array(
     site: "https://esia-portal1.test.gosuslugi.ru/",
     logout_uri: "http://yoursite.ru/",
     auth_uri: "http://yoursite.ru/auth/callback"
     client_id: "YOURID123",
     scope: "openid http://esia.gosuslugi.ru/usr_brf http://esia.gosuslugi.ru/usr_inf",
     cert_path: "${your_keys_dir}/config/esia/cert.crt",
     pkey_path: "${your_keys_dir}/config/esia/secret.key"

    ));"', E_USER_ERROR);
  }

  /**
   * Get signed file name
   * @return string file name
   **/
  protected function signed_file() {

    return $this->temp_dir . '/'. 'signed-' . $this->uuid;
  }

  /**
   * Get for signed file name
   * @return string file name
   **/
  protected function for_sign_file() {

    return $this->temp_dir . '/'. 'for-sign-' . $this->uuid;
  }

  /**
   * Save data in sing file
   * @param $data string for record
   **/
  protected function save_for_sign($data) {

    $fp = fopen($this->for_sign_file(), "w");
    fwrite($fp, $data);
    fclose($fp);
  }

	/**
	 * Redirect to $url with HTTP header (Location: )
	 *
	 * @param string $url URL to redirect user to
	 * @param boolean $exit Whether to call exit() right after redirection
	 **/
	public function redirect($url, $exit = true) {

		header("Location: $url");
		if ($exit) {
			exit();
		}
	}

  /**
   * POST request
   * @return request result
   **/
  private function post($url, $body) {

    if($this->config['use_curl'] != 'Y') {

      $context = stream_context_create(
                  array(
                    'http' => array(
                        'method' => 'POST',
                        'header' => 'Content-Type: application/x-www-form-urlencoded' . PHP_EOL,
                        'content' => $body,
                    ),
                    'ssl' => array(
                        'verify_peer'      => false,
                        'verify_peer_name' => false,
                    ),
                  ));
      
      if( ( $result = file_get_contents($file = $url, $use_include_path = false, $context) ) === FALSE ) {

        $this->debugSave('Data request failed...', "Function get() error:");
      }
    }
    else {

      $this->debugSave('', "Initialize cURL object...");
      $curl = curl_init();

      $header = 'Content-Type: application/x-www-form-urlencoded';
      
      curl_setopt( $curl, CURLOPT_URL,                      $url );
      $this->debugSave($url, "CURLOPT_URL");

      curl_setopt( $curl, CURLOPT_RETURNTRANSFER,              1 );
      $this->debugSave('true', "CURLOPT_RETURNTRANSFER");

      curl_setopt( $curl, CURLOPT_POST,                        1 );
      $this->debugSave('true', "CURLOPT_POST");

      curl_setopt( $curl, CURLOPT_POSTFIELDS,              $body );
      $this->debugSave($body, "CURLOPT_POSTFIELDS");

      curl_setopt( $curl, CURLOPT_HTTPHEADER,     array($header) );
      $this->debugSave($header, "CURLOPT_HTTPHEADER");

      $result = curl_exec ($curl);
      $this->debugSave('', "Exec cURL...");
    }

    return $result;
  }

  /**
   * GET request
   * @return request result
   **/
  private function get($url, $header) {

    if($this->config['use_curl'] != 'Y') {

      $this->debugSave($url, "Send GET request to:");

      $context =  stream_context_create(
                      array(
                        'http' => array(
                            'method'          => 'GET',
                            'header'          => $header . PHP_EOL,
                            'follow_location' => 0 //Следовать переадресациям заголовка Location. Для отключения установите в значение 0 (win => 0)
                                                   //Опции контекста http://php.net/manual/ru/context.http.php
                        ),
                        'ssl' => array(
                            'verify_peer'      => false,
                            'verify_peer_name' => false,
                        ),
                  ));

      $this->debugSave($context, "Created context:");
      
      if( ( $result = file_get_contents($file = $url, $use_include_path = false, $context) ) === FALSE ) {

        $this->debugSave('Data request failed...', "Function get() error:");
      }
    }
    else {

      $this->debugSave('', "Initialize cURL object...");
      $curl = curl_init();

      curl_setopt( $curl, CURLOPT_URL,                      $url );
      $this->debugSave($url, "CURLOPT_URL");

      curl_setopt( $curl, CURLOPT_RETURNTRANSFER,              1 );
      $this->debugSave('true', "CURLOPT_RETURNTRANSFER");

      curl_setopt( $curl, CURLOPT_HTTPHEADER,     array($header) );
      $this->debugSave($header, "CURLOPT_HTTPHEADER");
      
      $result = curl_exec ($curl);
      $this->debugSave('', "Exec cURL...");
    }

    return $result;
  }

}//end EsiaOmniAuth