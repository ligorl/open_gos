<?php
  
  error_reporting(E_ALL & ~E_NOTICE);
  //ini_set('display_errors','On');
  
  //date_default_timezone_set("Asia/Sakhalin");

  require_once "EsiaOmniAuth.php";
  
  $keys_dir = realpath( dirname(__FILE__) );
  
  $config = array("site"			 => "https://esia-portal1.test.gosuslugi.ru",
                  "auth_uri"	 => "http://besia.d.rnds.pro/native_module/",
                  "logout_uri" => "http://besia.d.rnds.pro/native_module/",
                  "client_id"	 => "RSKL02611",
                  "scope"			 => "openid fullname id_doc",
                  "cert_path"	 => "$keys_dir/keys/cert.crt",
                  "pkey_path"	 => "$keys_dir/keys/secret.key",
                  "use_curl"   => 'Y');

  session_start();

  $action = "";
  if( isset($_POST["action"]) ) {

    $action = $_POST["action"]; 
  }
  elseif( isset($_GET["action"]) ) {

    $action = $_GET["action"];
  }
  
  switch($action) {

      case "auth":
          // action after "Login by ESIA" link click
          $esia = new EsiaOmniAuth($config);
          $esia->create(); //redirected to esia login page
      break;

      default:

            if( isset($_GET['code']) && isset($_GET['state']) ) { // action after redirecting from ESIA

              if ($_GET['code'] && $_SESSION['esia_oauth_state'] == $_GET['state']) {

                $esia = new EsiaOmniAuth($config);
                $token = $esia->get_token($_GET['code']);
                $info = $esia->get_info($token);

                $userContactsJSON = $info['user_contacts'];
                $userContacts = json_decode($userContactsJSON, true);
                $userContacts = $userContacts['elements'];
                
                $userInfoJSON = $info["user_info"];
                $userInfo = json_decode($userInfoJSON, true);

                $docsInfoJSON = $info["user_info"];
                $docsInfo = json_decode($docsInfoJSON, true);

                //----- Debug info ----------------------------------

                echo $userInfo["firstName"].'<br/>';
                echo $userContacts[0]['value'].'<br/>';
                echo "Documents: <br/>";

                echo $info['docs_list'].'<br/>';
                //$docs_list = $data['docs_list'];
                /*foreach ($docs_list as $doc) {
                  echo $doc['doc_id']."<br/>";
                }*/

                /*foreach($docsInfo as $doc) {
                  echo $doc["type"]." | ".$doc["series"]." | ".$doc["number"]." | ".$doc["issueDate"]."<br/>";
                }*/

                //----- Debug info ----------------------------------

              }
            }
            else {
?>
                <!DOCTYPE html>
                <html lang="ru">
                  <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <title>Тест ЕСИА OAuth</title>
                  </head>
                  <body>
                    <form action="index.php" method="post">
                      <input type="hidden" name="action" value="auth">
                      <input id="signin-button" type="submit" value="Вход через ЕСИА">
                    </form>
                  </body>
                </html>
<?php
            }

      break;
  }

?>