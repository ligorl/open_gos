<?php

return [
    'controllers' => [
        'factories' => [
            'Reestr\Controller\Reestr' => 'Reestr\Controller\ReestrControllerFactory',
            'Reestr\Controller\Details' => 'Reestr\Controller\DetailsControllerFactory',
        ],
    ],
    'router' => [
        'routes' => [
            'isga-export' => [
                'type' => 'literal',
                'options' => [
                    'route' => '/isga-export/',
                    'defaults' => [
                        'controller' => 'Reestr\Controller\Reestr',
                        'action' => 'isga-export',
                    ],
                ],
            ],
            'accredreestr' => [
                'type' => 'literal',
                'options' => [
                    'route' => '/accredreestr/',
                    'defaults' => [
                        'controller' => 'Reestr\Controller\Reestr',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'search' => [
                        'type' => 'literal',
                        'options' => [
                            'route' => 'search/',
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Reestr',
                                'action' => 'search',
                            ],
                        ],
                    ],
                    'details' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => 'details/[:action/][:guid/][:organization/]',
                            'constraints' => [
                                'action' => '[a-zA-Z]*',
                                'guid' => '[a-zA-Z0-9]{8}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{4}\-[a-zA-Z0-9]{12}',
                                'organization' => '[0-1]',
                            ],
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Details',
                                'action' => 'index',
                            ],
                        ],
                    ],
                    'supplement' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => 'supplement/[:id/]',
                            'constraints' => [
                                'id' => '\d+',
                            ],
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Details',
                                'action' => 'supplement',
                            ]
                        ],
                    ],
                    'reporting' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => 'reporting/[:action/]',
                            'constraints' => [
                                'action' => '[a-zA-Z]*',
                            ],
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Reestr',
                                'action' => 'reporting',
                            ]
                        ],
                    ],
                    'xml' => [
                        'type' => 'segment',
                        'options' => [
                            'route' => 'xml/[:key]',
                            'constraints' => [
                                'key' => '[a-zA-Z0-9]{256}',
                            ],
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Reestr',
                                'action' => 'xml',
                            ]
                        ],
                    ],
                    'opendata' => [
                        'type' => 'literal',
                        'options' => [
                            'route' => 'opendata/',
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Reestr',
                                'action' => 'opendata',
                            ],
                        ],
                    ],

                    'perechen' => [
                        'type' => 'literal',
                        'options' => [
                            'route' => 'perechen',
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Reestr',
                                'action' => 'perechenArhivReestr',
                            ],
                        ],
                    ],

                    'parse' => [
                        'type' => 'literal',
                        'options' => [
                            'route' => 'parse/',
                            'defaults' => [
                                'controller' => 'Reestr\Controller\Reestr',
                                'action' => 'parse',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/reestr' => __DIR__ . '/../view/layout/layout.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'partial/result' => __DIR__ . '/../view/reestr/reestr/partial/result.phtml',
            'partial/paginator' => __DIR__ . '/../view/reestr/reestr/partial/paginator.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'layout' => 'layout/reestr',
    ],
];
