<?php

namespace Reestr\Entity;

use Reestr\Exception\ReestrException;

/**
 * Description of AbstractEntity
 *
 * @author Дмитрий Бубякин
 */
class AbstractEntity
{

    /**
     * Свойства таблицы из БД
     * @var array 
     */
    protected $properties = [];
    /**
     * Название таблицы
     * @var string
     */
    protected $table = null;
    /**
     * Бросать исключение если нет поля в объекте
     * @var bool
     */
    public $noError = false;
    /**
     * Параметры для методов toString, toKeyValue
     *  $params = [
     *      'pattern' => (string) Параметр sprintf для __toString()
     *      'properties' => [
     *          Список полей для отображения в __toString
     *      ]
     *      'id' => (string) Поле, значение которого будет у тега <option value="id"></option>
     *  ]
     * @var array 
     */
    protected $params = [
        'pattern'    => '%s',
        'properties' => [
            'Name',
        ],
        'id'         => '_id',
    ];

    /**
     * 
     * @param type $table Название таблицы для объекта
     * @param array $params Параметры объекта для методов 
     *  $params = [
     *      'pattern' => (string) Параметр sprintf для __toString()
     *      'properties' => [
     *          Список полей для отображения в __toString
     *      ]
     *      'id' => (string) Поле, значение которого будет у тега <option value="id"></option>
     *  ]
     */
    public function __construct($table = null, array $params = null)
    {
        if (!empty($params)) {
            $this->params = $params;
        }
        $this->table = $table;
    }

    /**
     * 
     * @param array $data
     */
    public function exchangeArray(array $data)
    {
        foreach ($data as $key => $value) {
            $this->__set($key, $value);
        }
    }

    /**
     * Установить значение свойства
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->properties[$name] = $value;
    }

    /**
     * Получить значение свойства
     * @param string $name
     * @return mixed
     * @throws ReestrException
     */
    public function __get($name)
    {
        if (!array_key_exists($name, $this->properties)) {
            if ($this->noError) {
                return null;
            }
            throw new ReestrException(sprintf('Property "%s" is not defined', $name));
        }
        return $this->properties[$name];
    }

    /**
     * 
     * @param string $name
     * @return boolean
     */
    public function __isset($name)
    {
        return isset($this->properties[$name]);
    }

    /**
     * Возвращает название таблицы
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Устанавливает название таблицы
     * @param string $table
     */
    public function setTable(string $table)
    {
        $this->table = $table;
    }

    /**
     * Возвращаем массив свойств объекта
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    public function mergeWith(AbstractEntity $entity)
    {
        if ($entity->getTable() !== $this->getTable()) {
            throw new ReestrException(sprintf('Entities are not compatible (%s,%s)', $entity->getTable(),
                                              $this->getTable()));
        }
        foreach ($entity->getProperties() as $key => $value) {
            if (empty($this->properties[$key])) {
                $this->properties[$key] = $value;
            }
        }
    }

    /**
     * 
     * @return array
     */
    public function prepareDataSet()
    {
        $dataSet = [];
        foreach ($this->properties as $name => $value) {
            if (empty($value) && $value != '0') {
                continue;
            }
            $dataSet[$name] = $value;
        }
        return $dataSet;
    }

    /**
     * Получаем свойства из объекта, или же из массива объектов
     * @param mixed $entity
     * @return array
     */
    public static function tryGetProperties($entity)
    {
        //если массив объектов
        if (is_array($entity)) {
            $proterties_array = [];
            foreach ($entity as $item) {
                if (!empty($item)) {
                    $proterties_array[] = $item->getProperties();
                }
            }
            return $proterties_array;
        }

        if (!empty($entity)) {
            return $entity->getProperties();
        }
        return [];
    }

    /**
     * 
     * @param \Reestr\Entity\AbstractEntity $entity
     * @param string $property
     * @return mixed
     */
    public static function tryGetProperty($entity, $property)
    {
        if (is_array($entity) && !empty($entity)) {
            $entity = $entity[0];
        }
        //var_dump($entity);

        if ($entity instanceof AbstractEntity && !empty($entity)) {
            if (isset($entity->$property)) {
                return $entity->$property;
            }
        }
        return null;
    }

    /**
     * 
     * @return \Reestr\Entity\AbstractEntity
     */
    public function toArray()
    {
        return $this;
    }

    /**
     * Устанавливаем ключ и значение для массива <option value="id">toString()</option>
     * @param array $options
     */
    public function toKeyValue(array &$options, $ifKey = null )
    {
        if( !empty($ifKey)){
            $key = $this->__get($ifKey);
            if( !empty($key)){
                $options[$key] = (string) $this;
            }
        }else{
            $options[$this->__get($this->params['id'])] = (string) $this;
        }
        
    }

    /**
     * Получаем строковое представление объекта
     * @return string
     */
    public function __toString()
    {
        if (!is_array($this->params['properties'])) {
            throw new ReestrException('toString\'s properties is not defined');
        }
        foreach ($this->params['properties'] as $property) {
            $properties[] = $this->__get($property);
        }
        return vsprintf($this->params['pattern'], $properties);
    }

}