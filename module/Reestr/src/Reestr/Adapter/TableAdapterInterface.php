<?php

namespace Reestr\Adapter;

use Reestr\Entity\AbstractEntity;

/**
 * Description of TableAdapterInterface
 *
 * @author Дмитрий Бубякин
 */
interface TableAdapterInterface {

    /**
     * Возвращает шлюз для объекта
     * @param AbstractEntity $prototype
     * @return TableGateway
     * @throws ReestrException
     */
    public function getTableGateway(AbstractEntity $prototype);
}
