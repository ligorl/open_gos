<?php

namespace Reestr\Model;

use Reestr\Model\TableAdapter;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Reestr\Entity\AbstractEntity;
use Reestr\Exception\ReestrException;
use Reestr\Adapter\TableAdapterInterface;

/**
 * Description of SearchEngine
 *
 * @author Дмитрий Бубякин
 */
class SearchEngine
{

    /**
     *  
     * @var TableAdapter 
     */
    protected $tableAdapter;
    /**
     *  $tables = [
     *      'tableName' => TableGateway
     *  ]
     * @var TableGateway|array
     */
    protected $tables = [];

    /**
     * 
     * @param TableAdapter $abstractTable
     */
    public function __construct(TableAdapterInterface $abstractTable)
    {
        $this->tableAdapter = $abstractTable;
    }

    /**
     * 
     * @param string $table
     * @return TableGateway
     * @throws ReestrException
     */
    public function getTable($table)
    {
        if (!array_key_exists($table, $this->tables)) {
            throw new ReestrException(sprintf('Table "%s" does not exists', $table));
        }
        return $this->tables[$table];
    }

    /**
     * Добавить сущность
     * @param string $table
     * @param array $params
     * @return \Reestr\Model\SearchEngine
     * @throws ReestrException
     */
    public function add($table, array $params = null)
    {
        if (array_key_exists($table, $this->tables)) {
            throw new ReestrException(sprintf('Table "%s" already exists', $table));
        }
        $prototype = new AbstractEntity($table, $params);
        $this->tables[$table] = $this->tableAdapter->getTableGateway($prototype);
        return $this;
    }

    /**
     * Добавляем массив таблиц
     * @param array $params
     * <pre>
     * $params = [
     *      'tableName',
     *      'tableName' => [options]
     * ]
     * </pre>
     * @return \Reestr\Model\SearchEngine
     */
    public function addAll(array $params)
    {
        foreach ($params as $table => $options) {
            if (is_array($options)) {
                $this->add($table, $options);
            } else {
                $this->add($options);
            }
        }
        return $this;
    }

    /**
     * Проверяем наличае таблицы
     * @param string $table
     * @return boolean
     */
    public function has($table)
    {
        return array_key_exists($table, $this->tables);
    }

    /**
     * 
     * @param array $params Параметры запроса 
     * <pre>
     *  $params = [
     *      'table' => (string) Название таблицы для выборки. Обязательно
     *      'alias' => (string)
     *      'columns' => [
     *          'field' //Названия столбцов
     *          //new Expression('count(id)')
     *          //'alias' => 'field'
     *      ]
     *      'where' => [
     *          'equal' => [
     *              'field' => 'value' //WHERE `A` = 'B'
     *          ]
     *          'like' => [
     *              'field' => 'like' // WHERE `A` like 'value'
     *          ],
     *          'isNotNull' => [
     *              'field'
     *          ]
     *          'in' => [
     *              'field' => [
     *                  'value' //Массив возможых значений
     *              ]
     *          ]
     *          'greater' => [
     *              'field' => 'value' //WHERE `A` > 'B'
     *          ]
     *          'greaterEqual' => [
     *              'field' => 'value' //WHERE `A` >= 'B'
     *          ]
     *          'less' => [
     *              'field' => 'value' //WHERE `A` < 'B'
     *          ]
     *          'lessEqual' => [
     *              'field' => 'value' //WHERE `A` <= 'B'
     *          ]
     *      ]
     *      'join' => [
     *          'tableName' => [
     *              'alias' => (string) Алиас
     *              'type' => (string) left,right,inner,outer
     *              'on' => (string) Условие JOIN
     *              'columns' => [
     *                  'field' //Названия столбцов
     *              ]
     *          ]
     *      ]
     *      'order' => [
     *          'field'
     *          //'id ASC'
     *      ]
     *      'group' => [
     *          'field'
     *      ]
     *      'limit' => (int) 
     *      'offset' => (int)
     *  ]
     * </pre>
     * @return Select
     * @throws ReestrException
     */
    public function prepareSelect(array $params)
    {
        if (empty($params['table'])) {
            throw new ReestrException('Table is undefined');
        }
        $select = new Select($params['table']);
        if (!empty($params['columns'])) {
            $select->columns($params['columns']);
        }
        if (!empty($params['where'])) {
            $where = $params['where'];

            if (!empty($where['equal'])) {
                $select->where($where['equal']);
            }
            if (!empty($where['isNotNull'])) {
                foreach ($where['isNotNull'] as $field) {
                    $select->where->isNotNull($field);
                }
            }
            if (!empty($where['like'])) {
                foreach ($where['like'] as $field => $value) {
                    if (empty($value)) {
                        continue;
                    }
                    $select->where->like($field, $value);
                }
            }

            if (!empty($where['in'])) {
                foreach ($where['in'] as $in => $values) {
                    if (empty($values)) {
                        continue;
                    }
                    $select->where->in($in, $values);
                }
            }

            if (!empty($where['greater'])) {
                foreach ($where['greater'] as $field => $value) {
                    $select->where->greaterThan($field, $value);
                }
            }

            if (!empty($where['greaterEqual'])) {
                foreach ($where['greaterEqual'] as $field => $value) {
                    $select->where->greaterThanOrEqualTo($field, $value);
                }
            }

            if (!empty($where['less'])) {
                foreach ($where['less'] as $field => $value) {
                    $select->where->lessThan($field, $value);
                }
            }
            if (!empty($where['lessEqual'])) {
                foreach ($where['lessEqual'] as $field => $value) {
                    $select->where->lessThanOrEqualTo($field, $value);
                }
            }
        }

        if (!empty($params['join'])) {
            foreach ($params['join'] as $table => $join) {
                $on = $join['on'];
                if (empty($on)) {
                    throw new ReestrException('Empty JOIN ON predicate');
                }
                $columns = empty($join['columns']) ? [] : $join['columns'];
                $type = empty($join['type']) ? 'inner' : $join['type'];
                $joinTable = empty($join['alias']) ? $table : [$join['alias'] => $table];
                $select->join($joinTable, $on, $columns, $type);
            }
        }

        if (!empty($params['order'])) {
            $select->order($params['order']);
        }

        if (!empty($params['group'])) {
            $select->group($params['group']);
        }

        if (!empty($params['limit'])) {
            $select->limit($params['limit']);
        }

        if (!empty($params['offset'])) {
            $select->offset($params['offset']);
        }
        return $select;
    }

    /**
     * 
     * @param Select $select
     * @param string $table
     * @param Boolean $throwNothing
     * @param Boolean $toArray
     * @return mixed
     */
    public function executeSelect(Select $select, $table, $throwNothing = false, $toArray = true)
    {
        $tableGateway = $this->getTable($table);
        $tableGateway->getAdapter()->getPlatform();
        $resultSet = $tableGateway->selectWith($select);
        if (!$resultSet->count() && $throwNothing) {
            throw new ReestrException(sprintf('Nothing found [%s]', $table));
        }
        return $toArray ? $resultSet->count() > 0 ? $resultSet->toArray() : [$resultSet->toArray()] : $resultSet;
    }

    /**
     * 
     * @param array $params
     * @return string
     */
    public function getSqlString(array $params)
    {
        $select = $this->prepareSelect($params);
        $tableGateway = $this->getTable($params['table']);
        $platform = $tableGateway->getAdapter()->getPlatform();
        return $select->getSqlString($platform);
    }

    /**
     * 
     * @param array $params
     * @param boolean $throwNothing Бросать исключение если 0 строк вернуло
     * @param boolean $toArray Вернуть массив
     * @return mixed
     */
    public function select(array $params, $throwNothing = false, $toArray = true, $limit = null, $offset = null, $orderBy = null)
    {
        $select = $this->prepareSelect($params);
        if( !empty($limit) ){
            $select->limit($limit);
        }
        if(!empty($offset)){
            $select->offset($offset);
        }
        if(!empty($orderBy)){
            $select->order($orderBy);
        }
        return $this->executeSelect($select, $params['table'], $throwNothing, $toArray);
    }

    /**
     * Возвращает массив значений для form select
     * @param string $table
     * @param array $where
     * @param string $order
     * @return array
     */
    public function getOptions(array $params, $empty = true, $ifKey = null)
    {
        $resultSet = $this->executeSelect($this->prepareSelect($params), $params['table']);
        $options = [];
        if ($empty) {
            $options[''] = 'Не выбрано';
        }
        foreach ($resultSet as $result) {
            if (!empty($result)) {
                $result->toKeyValue($options, $ifKey);
            }
        }
        return $options;
    }

    /**
     * Получаем указанные столбцы по условию(одну запись)
     * @param array $params
     *  $params = [
     *      'table' => (string) Название таблицы
     *      'where' => []
     *      'column' => (string) Название столбца с guid
     *  ]
     * @param boolean single
     * @return array
     */
    public function selectSingleProperty(array $params, $single = true)
    {
        $selectParams = [
            'table'   => $params['table'],
            'where'   => ['like' => $params['where']],
            'columns' => [$params['column']],
        ];
        $result = $this->select($selectParams);
        return $single ? $result[0] : $result;
    }

    public function getColumnsByWhere($table, array $columns, array $where)
    {
        $params = [
            'table'   => $table,
            'columns' => $columns,
            'where'   => ['like' => $where],
        ];
        return $this->select($params)[0];
    }

    public function update(AbstractEntity $entity, array $where = null, array &$errors = [])
    {
        try {
            $tableGateway = $this->getTable($entity->getTable());
            $set = $entity->prepareDataSet();
            if (empty($set)) {
                throw new ReestrException(sprintf('Update set is empty for %s', $entity->getTable()));
            } else {
                $tableGateway->update($set, empty($where) ? ['Id' => $entity->Id] : $where);
            }
        } catch (\Exception $ex) {
            $errors[] = $ex->getMessage();
        }
    }

    /**
     * Добавляем запись в базу
     * @param AbstractEntity $entity
     * @return int id последней добавленной записи
     */
    public function insert(AbstractEntity $entity)
    {
        $tableGateway = $this->getTable($entity->getTable());
        $set = $entity->prepareDataSet();
        $tableGateway->insert($set);
        return $tableGateway->getLastInsertValue();
    }

    /**
     * Выполняет SQL запрос
     * @param string $sql
     */
    public function executeSql($sql){
        /* @var $tableGateway TableGateway */
        $tableGateway = current($this->tables);
        $tableGateway->getAdapter()->query($sql,'execute');
    }

    /**
     * Сохраняем данные в базу через транзакцию
     * @param string $table название таблицы
     * @param array $dataSet массив из данных объектов
     * @param string $column id записи
     * @param string $error ошибка
     */
    public function multiSave($table, array &$dataSet, $column = 'unknown', array &$errors = [])
    {
        $tableGateway = $this->getTable($table);
        $connection = $tableGateway->getAdapter()->getDriver()->getConnection();
        try {
            $connection->beginTransaction();
            foreach ($dataSet as $set) {
                //если в списке полей объекта есть поле $column то обновляем
                if (array_key_exists($column, $set)) {
                    $tableGateway->update($set, [$column => $set[$column]]);
                } else {
                    $tableGateway->insert($set);
                }
            }
            $connection->commit();
        } catch (\Exception $e) {
            $errors[] = sprintf('%s [%s]%s', $table, $e->getMessage(),print_r(current($dataSet),true));
            $connection->rollback();
        }
    }

    /**
     * 
     * @param string $table
     * @param \Zend\Db\Sql\Where|array $where
     */
    public function delete($table, $where)
    {
        $tableGateway = $this->getTable($table);
        $tableGateway->delete($where);
    }

    public function exists(AbstractEntity $entity)
    {
        $table = $entity->getTable();
        $result = $this->select([
              'table' => $table,
              'where' => [
                  'equal' => ['Id' => $entity->Id],
              ],
          ])[0];
        return !empty($result);
    }

    /**
     * 
     * @param AbstractEntity $entity
     */
    public function insertOrUpdate(AbstractEntity $entity, $onlyInsert)
    {

        if ($onlyInsert) {
            $this->insert($entity);
            return;
        }
        if ($this->exists($entity)) {
            $this->update($entity);
        } else {
            $this->insert($entity);
        }
    }

    /**
     * 
     * @param array $params
     * @return Paginator
     */
    public function getSearchPaginator(array $params)
    {
        $tableGateway = $this->getTable($params['table']);
        $select = $this->prepareSelect($params);
        $paginatorAdapter = new DbSelect(
          $select, $tableGateway->getAdapter(), $tableGateway->getResultSetPrototype()
        );
        return new Paginator($paginatorAdapter);
    }

}