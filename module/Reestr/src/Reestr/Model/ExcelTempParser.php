<?php

namespace Reestr\Model;

use \XMLReader;
use Reestr\Params\TablesParams;
use Reestr\Model\SearchEngine;
use Reestr\Entity\AbstractEntity;
use Reestr\Exception\ReestrException;

/**
 * Description of ExcelTempParser   
 *
 * @author Дмитрий Бубякин
 */
class ExcelTempParser extends \XMLReader {

    /**
     *
     * @var SearchEngine 
     */
    protected $searchEngine;
    protected $packageId = null;
    protected $dateConvertParams = [
        'short' => 'd.m.Y',
        'long' => 'd.m.Y H:i:s',
        'mysql' => 'Y-m-d H:i:s', //2016-11-13 01:57:59
    ];
    protected $tablesOrder = [
        'package' => TablesParams::RECEIVED_PACKAGE,
        'organization' => TablesParams::EDUCATION_ORGANIZATION,
        'employee' => TablesParams::INDIVIDUAL_EMPLOYEE,
        'certificate' => TablesParams::ACCREDITATION_CERTIFICATE,
        'supplement' => TablesParams::CERTIFICATE_SUPPLEMENT,
        'decision' => TablesParams::CERTIFICATE_DECISION,
        'programm' => TablesParams::ACCREDITED_PROGRAMM,
    ];
    protected $tableMap = [
        //alias
        'certificate' => TablesParams::ACCREDITATION_CERTIFICATE,
        'organization' => TablesParams::EDUCATION_ORGANIZATION,
        'employee' => TablesParams::INDIVIDUAL_EMPLOYEE,
        'supplement' => TablesParams::CERTIFICATE_SUPPLEMENT,
        'programm' => TablesParams::ACCREDITED_PROGRAMM,
        'decision' => TablesParams::CERTIFICATE_DECISION,
        'package' => TablesParams::RECEIVED_PACKAGE,
        //fk
        'EduOrgFormId' => TablesParams::EDU_ORG_FORM,
        'EduOrgTypeId' => TablesParams::EDU_ORG_TYPE,
        'EduOrgKindId' => TablesParams::EDU_ORG_KIND,
        'RegionId' => TablesParams::REGION,
        'CertificateTypeId' => TablesParams::CERTIFICATE_TYPE,
        'CertificateStatusId' => TablesParams::CERTIFICATES_STATUS,
        'CertificateSupplementStatusId' => TablesParams::CERTIFICATE_SUPPLEMENT_STATUS,
        'EduProgrammTypeId' => TablesParams::EDU_PROGRAMM_TYPE,
        'EduLevelId' => TablesParams::EDUCATION_LEVEL,
        'CertificateDecisionTypeId' => TablesParams::CERTIFICATE_DECISION_TYPE,
    ];
    protected $queryQueue = [];

    /**
     * 
     * @param SearchEngine $searchEngine
     */
    public function __construct(SearchEngine $searchEngine) {
        $this->searchEngine = $searchEngine;
        $this->packageId = strtolower($this->GUID());
    }

    public function parseExcel($fileName) {

        if (!file_exists($fileName)) {
            throw new ReestrException(sprintf('File %s not found', $fileName));
        }

        $this->beginQueue();
        $this->open($fileName);
        while ($this->read()) {
            if ($this->isNecessaryRow('Worksheet')) {
                $this->parseWorksheet();
            }
        }
        $this->endQueue($this->tablesOrder);
    }

    protected function parseWorksheet() {

        $sheetName = $this->getAttribute('ss:Name');
        switch ($sheetName) {
            case 'Свидетельства':

                $this->parseRows([
                    'startRow' => 3,
                    'columns' => TablesParams::$CERTIFICATE_SHEET,
                    'sheet' => $sheetName,
                ]);

                break;
            case 'Приложения':
                $this->parseRows([
                    'startRow' => 3,
                    'columns' => TablesParams::$SUPPLEMENT_SHEET,
                    'sheet' => $sheetName,
                ]);
                break;
            case 'Образ.прогр':
                $this->parseRows([
                    'startRow' => 3,
                    'columns' => TablesParams::$PROGRAMM_SHEET,
                    'sheet' => $sheetName,
                ]);
                break;
            case 'Решения':
                $this->parseRows([
                    'startRow' => 3,
                    'columns' => TablesParams::$DECISION_SHEET,
                    'sheet' => $sheetName,
                ]);
                break;
            case 'Служебная информация':
                $this->parseRows([
                    'startRow' => 2,
                    'limit' => 1,
                    'columns' => TablesParams::$PACKAGE_SHEET,
                    'sheet' => $sheetName,
                ]);
                break;
            default:
                break;
        }
    }

    public function pushQuery(AbstractEntity $entity, $onlyInsert = false) {
        $this->queryQueue[] = [
            'entity' => $entity,
            'onlyInsert' => $onlyInsert,
        ];
    }

    public function beginQueue() {
        $this->queryQueue = [];
    }

    protected function getHeadOrgIdByCertificateId(array $certificates, $id) {
        foreach ($certificates as $certificate) {
            if (AbstractEntity::tryGetProperty($certificate, 'Id') === $id && !empty($id)) {
                return AbstractEntity::tryGetProperty($certificate, 'EduOrgId');
            }
        }
        return null;
    }

    public function prepareOrganizationsAndSupplements() {

        $certificates = [];
        $branchesId = [];
        foreach ($this->queryQueue as $item) {
            if ($item['entity']->getTable() === $this->tableMap['certificate']) {
                $certificates[] = $item['entity'];
            }
        }

        foreach ($this->queryQueue as &$item) {
            if ($item['entity']->getTable() === $this->tableMap['organization']) {
                $headOrgId = $this->getHeadOrgIdByCertificateId($certificates, $item['entity']->CertificateId);
                $id = AbstractEntity::tryGetProperty($item['entity'], 'Id');
                if ($headOrgId !== $id && !empty($headOrgId)) {
                    $item['entity']->HeadEduOrgId = $headOrgId;
                    $item['entity']->IsBranch = 1;
                    $branchesId[] = $id;
                }
                $item['entity']->CertificateId = null;
            }
        }
        foreach ($this->queryQueue as &$item) {
            if ($item['entity']->getTable() === $this->tableMap['supplement']) {
                $id = AbstractEntity::tryGetProperty($item['entity'], 'EduOrgId');
                if (array_search($id, $branchesId) !== false) {
                    $item['entity']->IsForBranch = 1;
                }
            }
        }
    }

    /**
     * 
     * @param array|AbstractEntity $order
     */
    public function endQueue(array $order) {
        $this->prepareOrganizationsAndSupplements();
        foreach ($order as $table) {

            foreach ($this->queryQueue as $item) {
                if ($item['entity']->getTable() === $table) {
                    $this->searchEngine->insertOrUpdate($item['entity'], $item['onlyInsert']);
                }
            }
        }
    }

    protected function parseRows(array $parseOptions) {
        $rowNumber = 0;
        $startRow = (int) $parseOptions['startRow'];
        $parent = $this->name;
        $limit = isset($parseOptions['limit']) ? (int) $parseOptions['limit'] : -1;
        while ($this->read() && $this->isNext($parent) && $limit !== 0) {
            if ($this->isNecessaryRow('Row')) {
                $rowNumber++;
            }
            if (!($this->isNecessaryRow('Row') && $rowNumber >= $startRow)) {
                continue;
            }
            $this->parseRow($parseOptions);
            $limit--;
        }
    }

    protected function parseRow(array $parseOptions) {

        $parent = $this->name;
        $data = [];
        $rowNumber = 0;
        $columns = $parseOptions['columns'];
        while ($this->read() && $this->isNext($parent)) {
            if (!$this->isNecessaryRow('Data')) {
                continue;
            }
            $value = $this->readInnerXml();
            $data[$columns[$rowNumber]] = $this->tryBoolean($this->isGuid($value) ? strtolower($value) : $value);
            $rowNumber++;
        }
        $this->processData($data, $parseOptions['sheet']);
    }

    /**
     * Проверяет, является ли элемент закрытием родительского блока
     * @param string $parent
     * @return boolean
     */
    protected function isNext($parent) {
        return ($this->nodeType === XMLReader::END_ELEMENT && $this->name === $parent) === false;
    }

    /**
     * Проверяет, является ли элемент тем, который ищем
     * @param string $name
     * @return boolean
     */
    protected function isNecessaryRow($name) {
        return $this->name === $name && $this->nodeType === XMLReader::ELEMENT;
    }

    /**
     * Проверяем значение, является ли guid
     * @param string $subject
     * @return int 1 если guid иначе 0
     */
    protected function isGuid($subject) {
        $pattern = '/^[A-Z0-9]{8}\-[A-Z0-9]{4}\-[A-Z0-9]{4}\-[A-Z0-9]{4}\-[A-Z0-9]{12}$/';
        return preg_match($pattern, $subject);
    }

    /**
     * 
     * @param string $value
     * @return mixed
     */
    protected function tryBoolean($value) {
        $bool = ['да' => '1', 'нет' => '0', 'Да' => '1', 'Нет' => '0'];
        return isset($bool[$value]) ? $bool[$value] : $value;
    }

    /**
     * Проверяем
     * @param string $column
     * @return boolean true если FK иначе false
     */
    protected function isForeignKey($column) {
        return !(strpos($column, 'fk') === false);
    }

    /**
     * Получаем название таблицы из префикса
     * @param string $column
     * @return string Название таблицы
     */
    protected function getTableAlias($column) {
        return substr($column, 0, strpos($column, '.'));
    }

    /**
     * Получаем название столбца
     * @param string $column
     * @return string
     */
    protected function getColumnName($column) {
        return substr($column, strrpos($column, '.') + 1);
    }

    /**
     * 
     * @param string $table
     * @param array $where
     * @param string $column
     * @return mixed
     */
    protected function getGuid($table, $where, $column) {
        $reslt = $this->searchEngine->selectSingleProperty([
            'table' => $table,
            'where' => $where,
            'column' => $column,
        ]);
        return AbstractEntity::tryGetProperty($reslt, $column);
    }

    /**
     * 
     * @param strig $column
     * @return boolean
     */
    protected function isDate($column) {
        return !(strpos($column, 'dt') === false);
    }

    /**
     * 
     * @param string $column
     * @return string
     */
    protected function getDateFormat($column) {
        foreach (array_keys($this->dateConvertParams) as $format) {
            if (!(strpos($column, $format) === false)) {
                return $format;
            }
        }
        return null;
    }

    protected function processData(array $data, $sheet) {
        unset($data['empty']);
        //prepare
        foreach (array_keys($data) as $key) {
            if ($this->isDate($key) && trim($data[$key]) !== '') {
                $data[$key] = $this->convertDate($this->dateConvertParams[$this->getDateFormat($key)], $this->dateConvertParams['mysql'], $data[$key]);
            }
            if ($this->isForeignKey($key)) {
                $table = $this->tableMap[$this->getColumnName($key)];
                $where = ['Name' => sprintf('%s', $data[$key])];
                $column = 'Id';
                $guid = $this->getGuid($table, $where, $column);
//                if (empty($guid)) {
//                    echo sprintf('<span style="color:red;">Не удалось найти GUID, таблица - %s, поле - Name, значение - "%s"</span><br>', $table, $data[$key]);
//                }
                $data[$key] = $guid;
            }

            //echo "{$this->tableMap[$this->getTableAlias($key)]} => {$this->getColumnName($key)} => {$data[$key]}<br>";
        }
        //die;
        $entities = $this->getEntities($data, $sheet);
        foreach ($entities as $entity) {
            $entity->guid = '';
            //add packageId here
            if (isset($entity->Id) || isset($entity->PackageId)) {
                //$this->searchEngine->insertOrUpdate($entity, $entity->getTable() === TablesParams::RECEIVED_PACKAGE);
                $this->pushQuery($entity, $entity->getTable() === TablesParams::RECEIVED_PACKAGE);
            }
        }
    }

    protected function getEntities(array $data, $sheet) {
        switch ($sheet) {
            case 'Свидетельства':
                return $this->prepareCertificateEntities($data);
            case 'Приложения':
                return $this->prepareSupplementEntities($data);
            case 'Образ.прогр':
                return $this->prepareProgrammEntities($data);
            case 'Решения':
                return $this->prepareDecisionEntities($data);
            case 'Служебная информация':
                return $this->preparePackageEntities($data);
            default:
                break;
        }
        return null;
    }

    /**
     * 
     * @param array $data
     * @return array 
     */
    protected function prepareCertificateEntities(array $data) {
        $organization = new AbstractEntity($this->tableMap['organization']);
        $certificate = new AbstractEntity($this->tableMap['certificate']);
        $employee = new AbstractEntity($this->tableMap['employee']);
        foreach ($data as $name => $value) {
            $alias = $this->getTableAlias($name);
            $property = $this->getColumnName($name);
            if ($this->tableMap[$alias] === $organization->getTable()) {
                $organization->$property = $value;
            } elseif ($this->tableMap[$alias] === $certificate->getTable()) {
                $certificate->$property = $value;
            } elseif ($this->tableMap[$alias] === $employee->getTable()) {
                $employee->$property = $value;
            }
        }
        //костыли
        $organization->Id = AbstractEntity::tryGetProperty($certificate, 'EduOrgId');
        $organization->CertificateId = AbstractEntity::tryGetProperty($certificate, 'Id');
        $employee->Id = AbstractEntity::tryGetProperty($certificate, 'IndividualEmployeeId');
        $certificate->RegionId = $organization->RegionId;
        $certificate->PackageId = $this->packageId;
        //remove all
        $this->totalRemove(AbstractEntity::tryGetProperty($certificate, 'Id'));
        return [$organization, $employee, $certificate];
    }

    protected function prepareSupplementEntities(array $data) {
        $organization = new AbstractEntity($this->tableMap['organization']);
        $supplement = new AbstractEntity($this->tableMap['supplement']);
        foreach ($data as $name => $value) {
            $alias = $this->getTableAlias($name);
            $property = $this->getColumnName($name);
            if ($this->tableMap[$alias] === $organization->getTable()) {
                $organization->$property = $value;
            } elseif ($this->tableMap[$alias] === $supplement->getTable()) {
                $supplement->$property = $value;
            }
        }
        //костыли
        $organization->Id = AbstractEntity::tryGetProperty($supplement, 'EduOrgId');
        $organization->CertificateId = AbstractEntity::tryGetProperty($supplement, 'CertificateId');
        return [$organization, $supplement];
    }

    protected function prepareProgrammEntities(array $data) {
        $programm = new AbstractEntity($this->tableMap['programm']);
        foreach ($data as $name => $value) {
            $property = $this->getColumnName($name);
            $programm->$property = $value;
        }
        return [$programm];
    }

    protected function prepareDecisionEntities(array $data) {
        $decision = new AbstractEntity($this->tableMap['decision']);
        foreach ($data as $name => $value) {
            $property = $this->getColumnName($name);
            $decision->$property = $value;
        }
        return [$decision];
    }

    //2015-11-11 01:37:26
    protected function preparePackageEntities(array $data) {
        $package = new AbstractEntity($this->tableMap['package']);
        foreach ($data as $name => $value) {
            $property = $this->getColumnName($name);
            $package->$property = $value;
        }
        $package->PackageId = $this->packageId;
        return [$package];
    }

    /**
     * Удалить все тчо связано с текущим сертификатом
     * @param string $certificateGuid
     */
    protected function totalRemove($certificateGuid) {
        $certificate = $this->searchEngine->selectSingleProperty([
            'table' => $this->tableMap['certificate'],
            'where' => ['Id' => $certificateGuid],
            'column' => '_id',
        ]);

        $certificateId = AbstractEntity::tryGetProperty($certificate, '_id');

        $inSet = $this->searchEngine->prepareSelect([
            'table' => $this->tableMap['supplement'],
            'columns' => [
                '_id',
            ],
            'where' => [
                'equal' => [
                    'certificate_id' => $certificateId,
                ],
            ],
        ]);

        if (!empty($certificateId)) {
            //Удаляем все программы
            $this->searchEngine->delete($this->tableMap['programm'], (new \Zend\Db\Sql\Where())
                            ->in('certificatesupplement_id', $inSet));
            //Удаляем решения
            $this->searchEngine->delete($this->tableMap['decision'], (new \Zend\Db\Sql\Where())
                            ->equalTo('certificate_id', $certificateId));
            //Удаляем все приложения
            $this->searchEngine->delete($this->tableMap['supplement'], (new \Zend\Db\Sql\Where())
                            ->equalTo('certificate_id', $certificateId));
            //$this->searchEngine->delete($this->tableMap['certificate'], (new \Zend\Db\Sql\Where())
            //                ->equalTo('_id', $certificateId));
        }
    }

    protected function convertDate($from, $to, $date) {
        return date_format(date_create_from_format($from, $date), $to);
    }

    protected function GUID() {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}
