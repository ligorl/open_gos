<?php

namespace Reestr\Form;

use Zend\Form\Form;

/**
 * Description of ReportForm
 *
 * @author Дмитрий Бубякин
 */
class ReportForm extends Form {

    public function __construct($name = 'ReportForm', $options = array()) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'role' => 'form',
        ]);

        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Фамилия, имя, отчество:',
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
        $this->add([
            'name' => 'organization',
            'type' => 'text',
            'options' => [
                'label' => 'Организация:',
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'email',
            'type' => 'email',
            'options' => [
                'label' => 'E-mail:',
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'phone',
            'type' => 'text',
            'options' => [
                'label' => 'Телефон для связи:',
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
                'required' => true,
            ],
        ]);
        $this->add([
            'name' => 'description',
            'type' => 'textarea',
            'options' => [
                'label' => 'Описание проблемы:',
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
        $this->add([
            'name'       => 'submit',
            'type'       => 'submit',
            'attributes' => [
                'value' => 'Отправить',
                'class' => 'btn btn-primary',
            ],
        ]);
    }

}
