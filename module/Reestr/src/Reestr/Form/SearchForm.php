<?php

namespace Reestr\Form;

use Zend\Form\Form;

/**
 * Description of SearchForm
 *
 * @author Дмитрий Бубякин
 */
class SearchForm extends Form {

    /**
     * Select elements options
     * @var array
     */
    protected $selectOptions;

    public function __construct($name = 'SearchForm', $options = []) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'role' => 'form',
            'class' => 'form-horizontal',
        ]);

        $this->add([
            'name' => 'regionId',
            'type' => 'select',
            'options' => [
                'label' => 'Наименование',
            ],
        ]);

        $this->add([
            'name' => 'eduOrgName',
            'type' => 'text',
            'options' => [
                'label' => 'Наименование',
            ],
        ]);
        $this->add([
            'name' => 'eoId',
            'type' => 'hidden',
            'options' => [
                'label' => 'GUID организации',
            ],
        ]);
        $this->add([
            'name' => 'eduOrgInn',
            'type' => 'text',
            'options' => [
                'label' => 'ИНН',
            ],
        ]);
        $this->add([
            'name' => 'eduOrgOgrn',
            'type' => 'text',
            'options' => [
                'label' => 'ОГРН',
            ],
        ]);
        $this->add([
            'name' => 'eduOrgAddress',
            'type' => 'text',
            'options' => [
                'label' => 'Место нахождения',
            ],
        ]);
        $this->add([
            'name' => 'eduOrgTypeId',
            'type' => 'select',
            'options' => [
                'label' => 'Тип образовательной организации',
            ],
        ]);
        $this->add([
            'name' => 'eduOrgKindId',
            'type' => 'select',
            'options' => [
                'label' => 'Вид образовательной организации',
            ],
        ]);

        $this->add([
            'name' => 'indEmplLastName',
            'type' => 'text',
            'options' => [
                'label' => 'Фамилия',
            ],
        ]);
        $this->add([
            'name' => 'indEmplFirstName',
            'type' => 'text',
            'options' => [
                'label' => 'Имя',
            ],
        ]);
        $this->add([
            'name' => 'indEmplMiddleName',
            'type' => 'text',
            'options' => [
                'label' => 'Отчество',
            ],
        ]);
        $this->add([
            'name' => 'indEmplAddress',
            'type' => 'text',
            'options' => [
                'label' => 'Место жительства',
            ],
        ]);
        $this->add([
            'name' => 'indEmplEgrip',
            'type' => 'text',
            'options' => [
                'label' => 'ЕГРИП',
            ],
        ]);
        $this->add([
            'name' => 'indEmplInn',
            'type' => 'text',
            'options' => [
                'label' => 'ИНН',
            ],
        ]);

        $this->add([
            'name' => 'certRegNum',
            'type' => 'text',
            'options' => [
                'label' => 'Регистрационный номер',
            ],
        ]);

        $this->add([
            'name' => 'certSerialNum',
            'type' => 'text',
            'options' => [
                'label' => 'Серия бланка',
            ],
        ]);

        $this->add([
            'name' => 'certFormNum',
            'type' => 'text',
            'options' => [
                'label' => 'Номер бланка',
            ],
        ]);
        $this->add([
            'name' => 'certIssueFrom',
            'type' => 'date',
            'options' => [
                'label' => 'с',
            ],
        ]);
        $this->add([
            'name' => 'certIssueTo',
            'type' => 'date',
            'options' => [
                'label' => 'по',
            ],
        ]);
        $this->add([
            'name' => 'certEndFrom',
            'type' => 'date',
            'options' => [
                'label' => 'с',
            ],
        ]);
        $this->add([
            'name' => 'certEndTo',
            'type' => 'date',
            'options' => [
                'label' => 'по',
            ],
        ]);
        $this->add([
            'name' => 'certStatusId',
            'type' => 'select',
            'options' => [
                'label' => 'Текущий статус свидетельства',
            ],
        ]);
        $this->add([
            'name' => 'eduProgCode',
            'type' => 'text',
            'options' => [
                'label' => 'Код образовательной программы',
            ],
        ]);
        $this->add([
            'name' => 'eduProgName',
            'type' => 'text',
            'options' => [
                'label' => 'Наименование образовательной программы',
            ],
        ]);
        $this->add([
            'name' => 'eduProgLevelId',
            'type' => 'select',
            'options' => [
                'label' => 'Уровень образования',
            ],
        ]);
        
        $this->add([
            'name' => 'certificatesupplementstatusId',
            'type' => 'select',
            'options' => [
                'label' => 'Статус приложения',
            ],
        ]);

        foreach ($this->getElements() as $element) {
            $this->get($element->getName())
                    ->setOptions([
                        'label_attributes' => [
                            'class' => 'control-label col-sm-4',
                        ],
                    ])
                    ->setAttributes([
                        'class' => 'form-control',
            ]);
        }
        
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Поиск',
                'class' => 'btn btn-primary ',
            ],
        ]);
        $this->add([
            'name' => 'reset',
            'attributes' => [
                'value' => 'Очистить',
                'class' => 'btn btn-default ',
            ],
        ]);

        $dateLabels = ['certIssueFrom', 'certIssueTo', 'certEndFrom', 'certEndTo'];

        foreach ($dateLabels as $label) {
            $this->get($label)->setOptions([
                'label_attributes' => [
                    'class' => 'control-label col-sm-1',
                ],
            ]);
        }

    }

    /**
     * 
     * @param array $selectOptions
     */
    public function setSelectOptions(array $selectOptions) {
        $this->selectOptions = $selectOptions;
        $this->prepareSelectOptions();
    }

    protected function prepareSelectOptions() {
        foreach ($this->selectOptions as $key => $value) {
            if ($this->has($key)) {
                $this->get($key)->setOptions([
                    'value_options' => $value,
                ]);
            }
        }
    }

    /**
     * 
     * @return array
     */
    public function getSelectOptions() {
        return $this->selectOptions;
    }

}
