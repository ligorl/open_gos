<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Reestr\Form;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilter;

/**
 * Description of ReportFormInputFilter
 *
 * @author Дмитрий Бубякин
 */
class ReportFormInputFilter implements InputFilterAwareInterface {

    /**
     *
     * @var InputFilter 
     */
    protected $inputFilter;

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $this->inputFilter = new InputFilter();

            $this->inputFilter->add([
                'name' => 'email',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    [
                        'name' => 'EmailAddress',
                        'options' => [
                            'messages' => [
                                'emailAddressInvalidFormat' => 'Неверно указан E-mail',
                            ],
                        ],
                    ],
                ],
            ]);
            $this->inputFilter->add([
                'name' => 'organization',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                ],
            ]);
            $this->inputFilter->add([
                'name' => 'phone',
                'required' => true,
                'filters' => [
                    ['name' => 'StripTags'],
                ],
            ]);
        }
        return $this->inputFilter;
    }

    public function setInputFilter(\Zend\InputFilter\InputFilterInterface $inputFilter) {
        throw new \Exception('Not used');
    }

}
