<?php

namespace Reestr\Controller;

use Reestr\Controller\BaseController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\Model\ViewModel;
use Reestr\Entity\AbstractEntity;
use Reestr\Params\TablesParams;
use Reestr\Params\ReestrParams;
use Reestr\Form\SearchForm;
use Reestr\Form\ReportForm;
use Reestr\Form\ReportFormInputFilter;
use Zend\Session\Container;
use Reestr\Exception\ReestrException;
use Reestr\Model\ExcelTempParser;

/**
 * Description of ReestrController
 * srv/www/work/f11_mon/module/Reestr/src/Reestr/Controller/ReestrController.php
 *
 * @author Дмитрий Бубякин
 */
class ReestrController extends BaseController {

    public function __construct(ServiceLocatorInterface $serviceLocator) {
        parent::__construct($serviceLocator);
    }

    /**
     * url:  /accredreestr/
     * @return ViewModel
     */
    public function indexAction() {

        $regions = $this->searchEngine
                ->getOptions(['table' => TablesParams::REGION, 'order' => ['code ACS']]);
        $types = $this->searchEngine
                ->getOptions(['table' => TablesParams::EDU_ORG_TYPE, 'order' => ['name ACS']]);
        $kinds = $this->searchEngine
                ->getOptions(['table' => TablesParams::EDU_ORG_KIND, 'order' => ['name ACS']]);
        $statuses = $this->searchEngine
                ->getOptions(['table' => TablesParams::CERTIFICATES_STATUS, 'order' => ['name ACS']]);
        $educationLevels = $this->searchEngine
                ->getOptions(['table' => TablesParams::EDUCATION_LEVEL, 'order' => ['name ACS']]);
        $statusesSup = $this->searchEngine
                ->getOptions(['table' => TablesParams::CERTIFICATE_SUPPLEMENT_STATUS, 'order' => ['name ACS']]);
        
        $statusesSup = $this->joinSelectArray($statusesSup);
        
        $dataOption = [
            'regionId' => $regions,
            'eduOrgTypeId' => $types,
            'eduOrgKindId' => $kinds,
            'certStatusId' => $statuses,
            'eduProgLevelId' => $educationLevels,
            'certificatesupplementstatusId' => $statusesSup,
        ];

        $searchForm = new SearchForm();
        $searchForm->setSelectOptions($dataOption);
        $reportingForm = new ReportForm();

        $autoSearch = false;
        $eoInn = $this->getRequest()->getQuery( 'eoInn' , '' );
        if( !empty($eoInn) ){
            $searchForm->setData( array( 'eoInn'=> strip_tags( trim($eoInn) ) ) );
            $autoSearch = true;
        }

        $eoId = $this->getRequest()->getQuery( 'eoId' , '' );
        if( empty( $eoId ) ){
            $eoId = $this->getRequest()->getQuery( 'eoGUID' , '' );
        }
        if( !empty($eoId) ){
            $searchForm->setData( array( 'eoId'=> strip_tags( trim($eoId) ) ) );
            $autoSearch = true;
        }

        return new ViewModel([
            'searchForm' => $searchForm,
            'reportForm' => $reportingForm,
            'autoSearch' => $autoSearch,
        ]);
    }

    public function searchAction() {
        $post = $this->getRequest()->getPost();
        $entity = new AbstractEntity();
        $entity->exchangeArray($post->toArray());

        $organization = $entity->searchby === 'organization';
        $extended = isset($entity->extended) && (!empty($entity->eduProgCode) || !empty($entity->eduProgName) || !empty($entity->eduProgLevelId) || !empty($entity->certificatesupplementstatusId));
        $inArr = [];
        
        if(!empty($entity->certificatesupplementstatusId)){
            $inArr = explode('-', $entity->certificatesupplementstatusId);
        }
        //Расширенный поиск, получаем ID сертификатов


        $subQuery = [
            'table' => TablesParams::CERTIFICATE_SUPPLEMENT,
            'columns' => [
                new \Zend\Db\Sql\Expression('DISTINCT certificate_id'),
            ],
            'where' => [
                'equal' => empty($entity->eduProgLevelId) ? [] : ['edulevel_id' => $entity->eduProgLevelId,],
                'in' => empty($entity->certificatesupplementstatusId) ? [] : ['certificatesupplementstatus_id' => $inArr,],
                'like' => [
                    'ProgrammName' => empty($entity->eduProgName) ? '' : sprintf('%%%s%%', $entity->eduProgName),
                    'ProgrammCode' => empty($entity->eduProgCode) ? '' : sprintf('%%%s%%', $entity->eduProgCode),
                ],
            ],
            'join' => [
                TablesParams::ACCREDITED_PROGRAMM => [
                    'alias' => 'ap',
                    'on' => TablesParams::CERTIFICATE_SUPPLEMENT . '._id = ap.certificatesupplement_id',
                    'columns' => [],
                    'type' => 'inner',
                ],
            ],
        ];
        
        if( !empty($entity->certificatesupplementstatusId) && empty($entity->eduProgName) && empty($entity->eduProgCode) && empty($entity->eduProgLevelId)){
            
            $subQuery = [
                'table' => TablesParams::CERTIFICATE_SUPPLEMENT,
                'columns' => [
                    new \Zend\Db\Sql\Expression('DISTINCT certificate_id'),
                ],
                'where' => [
                    'in' => ['certificatesupplementstatus_id' => $inArr,],
                ],
            ];
        }

        $page = $this->params()->fromQuery('page', 1);
        $params = [
            'table' => TablesParams::ACCREDITATION_CERTIFICATE,
            'organization' => $organization,
            'column' => $this->params()->fromQuery('order', 'IssueDate'), //сортировка по умолчанию
            'type' => $this->params()->fromQuery('direction', 'DESC'),
            'subQuery' => $extended ? $this->searchEngine->prepareSelect($subQuery) : null,
        ];


        //echo $this->searchEngine->getSqlString($this->prepareSelectParams($entity, $params));
        //die;

        $paginator = $this->searchEngine
                ->getSearchPaginator($this->prepareSelectParams($entity, $params));

        $paginator->setItemCountPerPage(ReestrParams::ITEM_COUNT_PER_PAGE);
        $paginator->setCurrentPageNumber((int) $page);
        $model = new ViewModel();
        $model->setTemplate('partial/result');
        $model->setTerminal(true);
        $model->setVariables([
            'paginator' => $paginator,
            'certificates' => $paginator->getCurrentItems()->toArray(),
            'organization' => $organization,
            'total' => $paginator->getTotalItemCount(),
            'column' => $params['column'],
            'type' => $params['type'],
        ]);
        return $model;
    }

    public function reportingAction() {
        $response = $this->getResponse();
        $response->setStatusCode(200);
        $request = $this->getRequest();
        $form = new ReportForm();
        $inputFilter = (new ReportFormInputFilter())->getInputFilter();
        $form->setInputFilter($inputFilter);
        //session для капчи
        $captcha = new Container('captcha');
        $post = $request->getPost();
        $ownMessages = [];
        try {
            if (!$post) {
                throw new ReestrException('Form is not submitted');
            }
            $form->setData($post);
            //проверим наличае кода капчи в сессии
            if ($post['captcha'] !== $captcha->value || empty($captcha->value)) {
                $ownMessages['captcha']['isEmpty'] = 'Переместите ползунок';
            }
            if (!$form->isValid() || count($ownMessages)) {
                throw new ReestrException('Invalid form data');
            }

            $mailOptions = [
                'id' => $this->reestrProstoyConfig['id'],
                'pass' => $this->reestrProstoyConfig['password'],
                'name' => 'Undefined',
                'email' => $post['email'],
                'subject' => $post['organization'],
                'body' => sprintf('Организация: %s<br/>%sE-mail: %s<br/>Телефон для связи: %s<br/><br/>%s', $post['organization'], empty($post['name']) ? '' : "ФИО: {$post['name']}<br/>", $post['email'], $post['phone'], empty($post['description']) ? '' : "Сообщение:<br/>{$post['description']}"
                ),
            ];
            $this->sendMailProstoy($mailOptions);

            $response->setContent(json_encode([
                'status' => 'OK',
            ]));
        } catch (ReestrException $ex) {
            $response->setContent(json_encode([
                'status' => 'FAIL',
                'errors' => array_merge($form->getMessages(), $ownMessages),
                'message' => $ex->getMessage(),
            ]));
        }
        $captcha->getManager()->getStorage()->clear('captcha');
        return $response;
    }

    public function captchaAction() {

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $captcha = new Container('captcha');
        try {
            $post = $this->getRequest()->getPost();
            if (!(isset($post['action']) && isset($post['value']))) {
                throw new ReestrException('Value or action is undefined');
            }
            $captcha->value = $post['value'];
            $response->setContent(json_encode([
                'status' => 'OK',
            ]));
        } catch (ReestrException $ex) {
            $response->setContent(json_encode([
                'status' => 'FAIL',
                'message' => $ex->getMessage(),
            ]));
        }
        return $response;
    }

    /**
     * url:  /accredreestr/xml/2K8dmwvk3WPVyWQmVpuE4i5yCoXKKytTJFSYLTyq4qoIXZBEpiPrsBmEmpjYo5bQUNo9CiRgILKJoAqorsjjzD67hrVV9lLvQLI0VttVQPR2AkqZlIUu15wjAszNsiAhiJZX7V5rNhBWshyLFPJzWju3WDz7HIkqSrGJvwieSPvWKp8iFrmI9m9rnxRIMvhkxGnKH3PXaoCYtDNqbnzSLuhCmslPB2SCLD1Kn95Q1HX3uaGaPHOrQ4IPnenhcy0q?l=2
     */
    public function xmlAction() {
        $key = '2K8dmwvk3WPVyWQmVpuE4i5yCoXKKytTJFSYLTyq4qoIXZBEpiPrsBmEmpjYo5bQUNo9CiRgILKJoAqorsjjzD67hrVV9lLvQLI0VttVQPR2AkqZlIUu15wjAszNsiAhiJZX7V5rNhBWshyLFPJzWju3WDz7HIkqSrGJvwieSPvWKp8iFrmI9m9rnxRIMvhkxGnKH3PXaoCYtDNqbnzSLuhCmslPB2SCLD1Kn95Q1HX3uaGaPHOrQ4IPnenhcy0q';
        $url_key = $this->params('key');
        if ($key !== $url_key) {
            echo 'Invalid key';
            die;
        }
        try{

            echo 'начало ';
            //ограничиваем количество записей, для отладки
            $limit = $this->params()->fromQuery('l', 0);

            $property = 'max_execution_time';
            $newMaxExecutionTime = 300;

            $oldMaxExecutionTime = ini_get($property);
            ini_set($property, $newMaxExecutionTime);

            echo $property . " = {$oldMaxExecutionTime}<br>";


            $start = microtime(true);
            $date = date('Ymd');
            $block_size = 200;
            $i = 0;
            echo 'пропишем полные пути ';
            $filePath = $_SERVER['DOCUMENT_ROOT'];
            $filePath = str_replace('/public', '' , $filePath);
            $filePath = $filePath . '/public/' . ReestrParams::PATH;
            if( !is_dir( $filePath ) ){
                if( !mkdir($filePath , 0777 , true) ){
                    echo 'не смогло создать папку'  ;
                }
            }
            echo 'пути проверены';

            // имена файлов
            $fileName = sprintf('data-%s-structure-20160713', $date);
            $name = sprintf('%s%s', $filePath, $fileName);
            $fileXml = $name. '.xml';
            $fileZip = $name. '.zip';

            //start xml
            $xmlWriter = new \XMLWriter();
            $xmlWriter->openMemory();
            $xmlWriter->startDocument('1.0', 'UTF-8');

            $xmlWriter->startElement('OpenData');
            $xmlWriter->startElement('Certificates');
            //['table' => , 'where' => ['equal' => []]]
            //много кода...

            //оптимизация работы с бд

            $offset = 0;
            if($limit==0)
                $limit = 3000;

            do {
                $certificates = $this->searchEngine
                    ->select(['table' => TablesParams::XML_CERTIFICATE_VIEW,], false, true, $limit, $offset++*$limit, array("Id"));

                $certsCount = 0;
                foreach ($certificates as &$item) {
                    if (empty($item)) {
                        continue;
                    }
                    $certsCount++;
                    //['table' => TablesParams::XML_DECISION_VIEW, 'where' => ['equal' => ['certificate_id' => $item->_id]]]
                    $item->ActualEducationOrganization = AbstractEntity::tryGetProperties($this->searchEngine
                        ->select(['table' => TablesParams::XML_ORGANIZATION_VIEW, 'where' => ['equal' => ['_id' => $item->eduorg_id]]])[0]);
                    $item->IndividualEntrepreneur = AbstractEntity::tryGetProperties($this->searchEngine
                        ->select(['table' => TablesParams::XML_ENTREPRENEUR_VIEW, 'where' => ['equal' => ['_id' => $item->individualemployee_id]]])[0]);
                    $tmp_supplements = $this->searchEngine
                        ->select(['table' => TablesParams::XML_SUPPLEMENT_VIEW, 'where' => ['equal' => ['certificate_id' => $item->_id]]]);
                    foreach ($tmp_supplements as &$tmp_item) {
                        if (empty($tmp_item)) {
                            continue;
                        }
                        $tmp_item->ActualEducationOrganization = AbstractEntity::tryGetProperties($this->searchEngine
                            ->select(['table' => TablesParams::XML_ORGANIZATION_VIEW, 'where' => ['equal' => ['_id' => $tmp_item->eduorg_id]]])[0]);
                        $tmp_item->IndividualEntrepreneur = AbstractEntity::tryGetProperties($this->searchEngine
                            ->select(['table' => TablesParams::XML_ENTREPRENEUR_VIEW, 'where' => ['equal' => ['_id' => $tmp_item->individualemployee_id]]])[0]);
                        $tmp_item->EducationalPrograms = AbstractEntity::tryGetProperties($this->searchEngine
                            ->select(['table' => TablesParams::XML_PROGRAM_VIEW, 'where' => ['equal' => ['certificatesupplement_id' => $tmp_item->_id]]]));
                    }
                    $item->Supplements = AbstractEntity::tryGetProperties($tmp_supplements);
                    $item->Decisions = AbstractEntity::tryGetProperties($this->searchEngine
                        ->select(['table' => TablesParams::XML_DECISION_VIEW, 'where' => ['equal' => ['certificate_id' => $item->_id]]]));
                    //write
                    $this->writeXmlPart($xmlWriter, $item);
                    //write by blocks
                    $i++;
                    if ($i % $block_size === 0) {
                        file_put_contents($fileXml, $xmlWriter->flush(true), FILE_APPEND);
                    }
                }
                echo "</br>Step: ".$offset." Items: ".$certsCount;
            } while($certsCount != 0);

            $xmlWriter->endElement();
            $xmlWriter->endElement();
            file_put_contents( $fileXml , $xmlWriter->flush(true), FILE_APPEND);


            //remove file
            if( file_exists($fileZip) ) {
                unlink( $fileZip );
            }
            $filter = new \Zend\Filter\Compress([
                'adapter' => 'Zip',
                'options' => [
                    'archive' => $fileZip,
                ],
            ]);
            $compressed = $filter->filter( $fileXml );
            /*
            $baseName = basename($fileXml);
            $zip = new \ZipArchive();
            @$res = $zip->open($fileZip , \ZipArchive::CREATE );
            if( ! $res ){
                die( 'ошибка создания арзива ' );
            }
            $res = $zip->addFile($fileXml, $baseName );
            if( ! $res ){
                die( 'ошибка записать в арзив ' );
            }
            $zip->close();
            $compressed = true;
            */
            //удаляем все файлы кроми зип
            $files = array_values( array_diff(scandir($filePath), ['.', '..', '.svn', $fileZip]) );
            if ($compressed) {
                foreach ($files as $file) {
                    $fileArray = explode('.', $file);
                    $fileExt = end($fileArray);
                    if( 'zip' == $fileExt ){
                        continue;
                    }
                    @unlink(sprintf('%s%s', $filePath, $file));
                }
            }

            $end = microtime(true);

            ini_set($property, $oldMaxExecutionTime);
            echo 'OK<br>' . ($end - $start) . ' seconds<br>Compressed ' . (Boolean) $compressed . '<br>';
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            echo $ex->getLine();
            echo $ex->getTraceAsString();
        }
        die;
    }

    /**
     * url:  /accredreestr/opendata/
     *
     * @return \Zend\Http\Response\Stream
     * @throws ReestrException
     */
    public function opendataAction() {
        $response = new \Zend\Http\Response\Stream();
        try {
            //$path = ReestrParams::PATH;
            $path = $_SERVER['DOCUMENT_ROOT'];
            $path = str_replace('/public', '' , $path);
            $path = $path . '/public/' . ReestrParams::PATH;
            $files = glob($path.'*.zip' , GLOB_NOSORT);
            //var_dump($files);
            //die;
            if (empty($files)) {
                throw new ReestrException('Nothing found');
            }
            rsort($files);
            //$fileName = $files[0];
            //$filePath = sprintf('%s%s', $path, $fileName);
            $filePath =  reset($files);
            if( !is_file( $filePath )){
                throw new ReestrException('error read file');
            }
            $fileName = basename( $filePath );
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $ctype = finfo_file($finfo, $filePath);
            $response->setStream(fopen($filePath, 'r'));
            $response->setStatusCode(200);
            $headers = new \Zend\Http\Headers();
            $headers->addHeaderLine('Content-Type', $ctype)
                    ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $fileName . '"')
                    ->addHeaderLine('Content-Length', filesize($filePath));
            $response->setHeaders($headers);
            return $response;
        } catch (ReestrException $ex) {
            $tmp = $this->getResponse();
            $tmp->setStatusCode(200);
            $tmp->setContent($ex->getMessage());
            return $tmp;
        }
    }

    /**
     * url:  /accredreestr/parse/
     *
     */
    public function parseAction() {
        $parser = new ExcelTempParser($this->searchEngine);
        $fileName = 'public/uploads/out.xls';
        $parser->parseExcel($fileName);
        die;
    }


    /**
     * возращаем перечень файло в каталоге архивов реестра свидетельств
     */
    public function perechenArhivReestrAction(){
        $path = ReestrParams::PATH;
        $files1 = glob($path.'*.zip' , GLOB_NOSORT );
        $files2 = glob('public/'.$path.'*.zip' , GLOB_NOSORT);
        $files = array_merge( $files1 , $files2 );
        $files = array_flip( $files );
        unset( $files['public/opendata/data-'.date('Ymd').'-structure-20150421.zip'] );
        unset( $files['public/opendata/data-'.date('Ymd').'-structure-20160713.zip'] );
        unset( $files['opendata/data-'.date('Ymd').'-structure-20150421.zip'] );
        unset( $files['opendata/data-'.date('Ymd').'-structure-20160713.zip'] );
        $files = array_flip( $files );
        if( empty($files) ){
            die( 'нет файлов' );
        }
        rsort( $files );
        echo('<lu><style>a{font-size: 13px;}</style>');
        foreach( $files as $key => $value ){
            $value = '/'.$value;
            $fileName = str_replace('/public/', '' , $value);
            echo('<li style="list-style-type:none;" ><a href="/'.$fileName.'" target="_blank">'.basename($value).'<a></li>');
        }
        echo('</lu>');
        die;
    }

    public function isgaExportAction()
    {
        $force = $this->params()->fromQuery('force', false);
        $cron = $this->params()->fromQuery('cron', false);
        $check = $this->params()->fromQuery('check', false);
        $commandDirectory = $_SERVER['DOCUMENT_ROOT'] . '/cli/export';

        if ($force !== false) {
            return $this->forceExecute($commandDirectory);
        }

        if ($cron !== false) {
            return $this->forceExecute($commandDirectory, true);
        }

        $zipName = date('d-m-Y') . '.zip';
        $zipPath = $commandDirectory . '/export/' . $zipName;

        if ($check !== false) {
            return $this->getResponse()
                ->setStatusCode(file_exists($zipPath) ? 200 : 404);
        }

        $zipSize = filesize($zipPath);

        $response = new \Zend\Http\Response\Stream();
        $response->setStatusCode(200);
        $response->setStream(fopen($zipPath , 'r'));

        $headers = new \Zend\Http\Headers();
        $headers->addHeaders([
            'Content-Type' => 'application/zip',
            'Content-Disposition' => 'attachment; filename="' . $zipName . '"',
            'Content-Length' => $zipSize,
        ]);

        return $response->setHeaders($headers);
    }

    /**
     * @param  string $commandDirectory
     * @param bool    $cron
     *
     * @return \Zend\Http\Response
     */
    protected function forceExecute($commandDirectory, $cron = false)
    {
        $command = './app isga:export' . ($cron ? '' : ' --force');
        $logFile = $commandDirectory . '/logs/export-' . date('Y-m-d') . '.log';

        $process = new \Symfony\Component\Process\Process($command, $commandDirectory);
        $process->setTimeout(null)->run();

        $response = new \Zend\Http\Response();
        $response->setStatusCode(200);

        if ($process->isSuccessful()) {
            $response->setContent(file_get_contents($logFile));
        } else {
            $response->setContent(sprintf(
                "Command dir: %s\nCommand output: %s",
                $commandDirectory,
                $process->getErrorOutput()
            ));
        }

        return $response;
    }


}
