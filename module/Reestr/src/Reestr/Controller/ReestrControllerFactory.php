<?php

namespace Reestr\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Reestr\Controller\ReestrController;

/**
 * Description of ReestrControllerFactory
 *
 * @author Дмитрий Бубякин
 */
class ReestrControllerFactory implements FactoryInterface {

    public function __invoke(ServiceLocatorInterface $container) {
        return new ReestrController($container->getServiceLocator());
    }

    public function createService(ServiceLocatorInterface $container) {
        return $this($container);
    }

}
