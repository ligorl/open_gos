<?php

namespace Reestr\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Reestr\Entity\AbstractEntity;
use Reestr\Model\SearchEngine;
use Reestr\Params\TablesParams;

/**
 * Description of BaseController
 *
 * @author Дмитрий Бубякин
 */
class BaseController extends AbstractActionController {

    /**
     *
     * @var SearchEngine 
     */
    protected $searchEngine;

    /**
     *  id & pass для простого
     * @var array 
     */
    protected $reestrProstoyConfig;

    public function __construct(ServiceLocatorInterface $serviceLocator) {
        $this->searchEngine = $serviceLocator->get('Reestr\SearchEngine');
        $this->reestrProstoyConfig = $serviceLocator->get('config')['reestr_prostoy'];
        //Сущности для работы
        $this->searchEngine->addAll([
            TablesParams::REGION => [
                'pattern' => '%s - %s',
                'properties' => ['Code', 'Name',],
                'id' => '_id',
            ],
            TablesParams::EDU_ORG_KIND,
            TablesParams::CERTIFICATES_STATUS,
            TablesParams::EDU_ORG_TYPE,
            TablesParams::EDUCATION_LEVEL,
            TablesParams::EDU_ORG_FORM,
            TablesParams::CERTIFICATE_TYPE,
            TablesParams::ACCREDITATION_CERTIFICATE,
            TablesParams::CERTIFICATE_ORGANIZATION_DETAILS_VIEW,
            TablesParams::CERTIFICATE_EMPLOYEE_DETAILS_VIEW,
            TablesParams::EMPLOYEE_DETAILS_VIEW,
            TablesParams::ORGANIZATION_DETAILS_VIEW,
            TablesParams::CERTIFICATE_DECISION,
            TablesParams::CERTIFICATE_SUPPLEMENTS_VIEW,
            TablesParams::CERTIFICATE_SUPPLEMENT,
            TablesParams::ACCREDITED_PROGRAMM,
            TablesParams::CERTIFICATE_SUPPLEMENT_STATUS,
            TablesParams::EDU_PROGRAMM_TYPE,
            TablesParams::EDU_PROGRAMM_KIND,
            TablesParams::CERTIFICATE_DECISION_TYPE,
            TablesParams::RECEIVED_PACKAGE,
            TablesParams::EDUCATION_ORGANIZATION,
            TablesParams::INDIVIDUAL_EMPLOYEE,
            TablesParams::XML_CERTIFICATE_VIEW,
            TablesParams::XML_ORGANIZATION_VIEW,
            TablesParams::XML_ENTREPRENEUR_VIEW,
            TablesParams::XML_SUPPLEMENT_VIEW,
            TablesParams::XML_PROGRAM_VIEW,
            TablesParams::XML_DECISION_VIEW,
        ]);
    }
    
    function array_kshift(&$arr)
    {
        reset($arr);
        $firstKey = key($arr);
        $firstVal = current($arr);
        unset($arr[$firstKey]);
        return [ $firstKey => $firstVal];
    }
    
    /**
     * 
     * @param type $array
     */
    protected function joinSelectArray($arr){
        $ret = [];
        
        while( count($arr) > 0){
            $tempArr = $this->array_kshift($arr);
            $key = key($tempArr);
            $curentName = current($tempArr);
            $onArray = false;
            
             foreach ($arr as $keyD => $valueD) {
                
                if( $curentName == $valueD && $key != $keyD){
                    $joinIds = "$key-$keyD";
                    $ret[$joinIds] = $valueD;
                    $onArray = true;
                    break;
                }
            }
            
            if($onArray){
                unset($arr[$keyD]);
                unset($arr[$key]);
            }else{
                $ret[$key] = $curentName;
                unset($arr[$key]);
            }
        }
        
        return $ret;
    }

    /**
     * 
     * @param \XMLWriter $xmlWriter
     * @param AbstractEntity $item
     */
    protected function writeXmlPart(\XMLWriter &$xmlWriter, &$item) {
        $xmlWriter->startElement('Certificate');

        foreach (TablesParams::$XML_CERTIFICATE_PARAMS as $params) {
            $xmlWriter->writeElement($params, $item->$params);
        }
        //ActualEducationOrganization
        $xmlWriter->startElement('ActualEducationOrganization');
        if (!empty($item->ActualEducationOrganization)) {
            foreach (TablesParams::$XML_ORGANIZATION_PARAMS as $params) {
                $xmlWriter->writeElement($params, $item->ActualEducationOrganization[$params]);
            }
        }
        $xmlWriter->endElement();
        //IndividualEntrepreneur
        $xmlWriter->startElement('IndividualEntrepreneur');
        if (!empty($item->IndividualEntrepreneur)) {
            foreach (TablesParams::$XML_ENTREPRENEUR_PARAMS as $params) {
                $xmlWriter->writeElement($params, $item->IndividualEntrepreneur[$params]);
            }
        }
        $xmlWriter->endElement();
        //Supplements
        $xmlWriter->startElement('Supplements');
        foreach ($item->Supplements as $supplement) {
            $xmlWriter->startElement('Supplement');
            foreach (TablesParams::$XML_SUPPLEMENT_PARAMS as $params) {
                $xmlWriter->writeElement($params, $supplement[$params]);
            }
            //ActualEducationOrganization
            $xmlWriter->startElement('ActualEducationOrganization');
            if (!empty($supplement['ActualEducationOrganization'])) {
                foreach (TablesParams::$XML_ORGANIZATION_PARAMS as $params) {
                    $xmlWriter->writeElement($params, $supplement['ActualEducationOrganization'][$params]);
                }
            }
            $xmlWriter->endElement();
            //IndividualEntrepreneur
            $xmlWriter->startElement('IndividualEntrepreneur');
            if (!empty($supplement['IndividualEntrepreneur'])) {
                foreach (TablesParams::$XML_ENTREPRENEUR_PARAMS as $params) {
                    $xmlWriter->writeElement($params, $supplement['IndividualEntrepreneur'][$params]);
                }
            }
            $xmlWriter->endElement();
            //EducationalPrograms
            $xmlWriter->startElement('EducationalPrograms');
            if (!empty($supplement['EducationalPrograms'])) {
                foreach ($supplement['EducationalPrograms'] as $program) {
                    $xmlWriter->startElement('EducationalProgram');
                    foreach (TablesParams::$XML_PROGRAM_PARAMS as $params) {
                        $xmlWriter->writeElement($params, $program[$params]);
                    }
                    $xmlWriter->endElement();
                }
            }
            $xmlWriter->endElement();
            $xmlWriter->endElement();
        }
        $xmlWriter->endElement();
        //Decisions
        $xmlWriter->startElement('Decisions');
        foreach ($item->Decisions as $decision) {
            $xmlWriter->startElement('Decision');
            if (empty($decision)) {
                $xmlWriter->endElement();
                continue;
            }
            foreach (TablesParams::$XML_DECISION_VIEW as $params) {
                $xmlWriter->writeElement($params, $decision[$params]);
            }
            $xmlWriter->endElement();
        }
        $xmlWriter->endElement();
        $xmlWriter->endElement();
        $item = NULL;
    }

    /**
     * Отправить сообщение в клиент простого
     * @param array $params
     */
    protected function sendMailProstoy($params = []) {
        extract($params);
        if (empty($subject))
            $subject = '';
        mb_internal_encoding('WINDOWS-1251');
        file('http://agent.prostoy.ru/addComment.php?tid=' . $id . '&tpass=' . $pass .
                '&tname=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $name)) .
                '&temail=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $email)) .
                '&tsubj=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $subject)) .
                '&tcom=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $body)));
    }

    /**
     * 
     * @param AbstractEntity $entity
     * @param array $params
     *  $params = [
     *      'table' => (strin) Название таблицы
     *      'organization' => значение radio на странице поиска
     *      'column' => (string) Поле сортировки
     *      'type' => (string) ASC || DESC
     *      'subQuery' => (Select) Подзапрос для получения подходящих ID сертификатов
     *  ]
     * @return array
     */
    protected function prepareSelectParams(AbstractEntity $entity, array $params) {

        //упорядочиваем очередь поиска
        $order = [
            'regionId' => $params['table'] . '.region_id',
            'eduOrgKindId' => 'eduorgkind_id',
            'eduOrgTypeId' => 'eduorgtype_id',
            'certStatusId' => 'certificatestatus_id',
            'eduOrgInn' => 'EduOrgINN',
            'eduOrgOgrn' => 'EduOrgOGRN',
            'indEmplAddress' => 'IndividualEmployeeAddress',
            'indEmplEgrip' => 'IndividualEmployeeEGRIP',
            'indEmplInn' => 'IndividualEmployeeINN',
            'certRegNum' => 'RegNumber',
            'certSerialNum' => 'SerialNumber',
            'certFormNum' => 'FormNumber',
            'eoId'          => 'EduOrgId'
        ];
        //поиск по названию
        $likeParams = [
            'eduOrgName' => 'EduOrgFullName',
            'indEmplLastName' => 'IndividualEmployeeLastName',
            'indEmplFirstName' => 'IndividualEmployeeFirstName',
            'indEmplMiddleName' => 'IndividualEmployeeMiddleName',
            'eduOrgAddress' => $params['table'] . '.PostAddress',
        ];
        //сортировка результатов запроса
        $orderParams = [
            'Name' => $params['organization'] ? 'EduOrgFullName' : 'IndividualEmployeeLastName',
            'Reg' => 'RegNumber',
            'Serial' => 'SerialNumber',
            'IssueDate' => 'IssueDate',
            'EndDate' => 'EndDate',
            'Status' => 'CertificateStatusName',
        ];
        $orderGreater = [
            'certIssueFrom' => 'IssueDate',
            'certEndFrom' => 'EndDate',
        ];

        $orderLess = [
            'certIssueTo' => 'IssueDate',
            'certEndTo' => 'EndDate',
        ];

        $where = [];
        $like = [];
        $greaterEqual = [];
        $lessEqual = [];
        foreach ($order as $key => $value) {
            if (!empty($entity->$key)) {
                $where[$value] = $entity->$key;
            }
        }
        foreach ($likeParams as $key => $value) {
            if (!empty($entity->$key)) {
                $like[$value] = sprintf('%%%s%%', $entity->$key);
            }
        }
        foreach ($orderGreater as $key => $value) {
            if (!empty($entity->$key)) {
                $greaterEqual[$value] = $entity->$key;
            }
        }
        foreach ($orderLess as $key => $value) {
            if (!empty($entity->$key)) {
                $lessEqual[$value] = $entity->$key;
            }
        }
        

        /* $selectParams = [
          'table' => $params['table'],
          'where' => [
          'equal' => $where,
          'like' => $like,
          'greaterEqual' => $greaterEqual,
          'lessEqual' => $lessEqual,
          'isNotNull' => [
          $params['organization'] ? 'EduOrgId' : 'IndividualEmployeeId',
          ],
          'in' => [
          'Id' => $params['subQuery'],
          ],
          ],
          'order' => [
          sprintf('%s %s', $orderParams[$params['column']], $params['type']),
          ],
          ]; */
        $selectParams = [
            'table' => $params['table'],
            'columns' => [
                'Id' => '_id',
                'Guid' => 'Id',
                'RegionId' => 'region_id',
                'EduOrgFullName' => 'EduOrgFullName',
                'EduOrgINN' => 'EduOrgINN',
                'EduOrgOGRN' => 'EduOrgOGRN',
                'PostAddress' => 'PostAddress',
                'EduOrgId' => 'eduorg_id',
                'IndividualEmployeeFirstName' => 'IndividualEmployeeFirstName',
                'IndividualEmployeeMiddleName' => 'IndividualEmployeeMiddleName',
                'IndividualEmployeeLastName' => 'IndividualEmployeeLastName',
                'IndividualEmployeeAddress' => 'IndividualEmployeeAddress',
                'IndividualEmployeeEGRIP' => 'IndividualEmployeeEGRIP',
                'IndividualEmployeeINN' => 'IndividualEmployeeINN',
                'IndividualEmployeeId' => 'IndividualEmployeeId',
                'RegNumber' => 'RegNumber',
                'SerialNumber' => 'SerialNumber',
                'FormNumber' => 'FormNumber',
                'IssueDate' => 'IssueDate',
                'EndDate' => 'EndDate',
                'CertificateStatusId' => 'certificatestatus_id',
                'Total' => 'Total',
                'Active' => 'Active',
                'Inactive' => 'Inactive',
                'Suspended' => 'Suspended',
                'Attainted' => 'Attainted',
            ],
            'where' => [
                'equal' => $where,
                'like' => $like,
                'greaterEqual' => $greaterEqual,
                'lessEqual' => $lessEqual,
                'isNotNull' => [
                    $params['organization'] ? 'eduorg_id' : 'individualemployee_id',
                ],
                'in' => [
                    $params['table'] . '._id' => $params['subQuery'],
                ],
            ],
            'join' => [
                'educationorganization' => [
                    'alias' => 'eo',
                    'on' => 'eduorg_id = eo._id',
                    'columns' => [
                        'EduOrgTypeId' => 'eduorgtype_id',
                        'EduOrgKindId' => 'eduorgkind_id',
                    ],
                    'type' => $params['organization'] ? 'inner' : 'left',
                ],
                'certificatestatus' => [
                    'alias' => 'cst',
                    'on' => 'certificatestatus_id = cst._id',
                    'columns' => [
                        'CertificateStatusName' => 'Name',
                    ],
                ],
            ],
            'order' => [
                sprintf('%s %s', $orderParams[$params['column']], $params['type']),
            ],
        ];
        return $selectParams;
    }

}
