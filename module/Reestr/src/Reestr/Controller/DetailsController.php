<?php

namespace Reestr\Controller;

use Zend\ServiceManager\ServiceLocatorInterface;
use Reestr\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Reestr\Params\TablesParams;
use Reestr\Exception\ReestrException;
use Zend\Db\Sql\Expression;

/**
 * Description of DetailsController
 *
 * @author Дмитрий Бубякин
 */
class DetailsController extends BaseController {

    public function __construct(ServiceLocatorInterface $serviceLocator) {
        parent::__construct($serviceLocator);
    }

    public function indexAction() {

        $guid = $this->params('guid', null);
        $organization = $this->params('organization', 1);

        $selectParams = [
            'table' => TablesParams::CERTIFICATE_DECISION,
            'columns' => [
                'Kind' => 'OrderDocumentKind',
                'Number' => 'OrderDocumentNumber',
                'Date' => 'DecisionDate',
            ],
            'where' => [
                'equal' => [
                    'CertificateId' => $guid,
                ],
            ],
            'join' => [
                TablesParams::CERTIFICATE_DECISION_TYPE => [
                    'alias' => 'ct',
                    'on'      => new \Zend\Db\Sql\Expression(
                        'ct._id = certificatedecisiontype_id and CertificateDecisionTypeId != "8fb59816-6a1b-4866-bf66-30e9274f8718"'
                    ),
                    'columns' => [
                        'Name',
                    ],
                    //'type' => 'left',
                ],
            ],
        ];
        $decisions = $this->searchEngine->select($selectParams);
        
        $eduLevels = $this->searchEngine
                ->getOptions(['table' => TablesParams::EDUCATION_LEVEL, 'order' => ['name ACS']]);
        $supplements = $this->searchEngine
                ->select(['table' => TablesParams::CERTIFICATE_SUPPLEMENTS_VIEW, 'where' => ['equal' => ['CertificateGuid' => $guid]], 'group' => ['_id']]);

        $certificateTable = $organization ? TablesParams::CERTIFICATE_ORGANIZATION_DETAILS_VIEW : TablesParams::CERTIFICATE_EMPLOYEE_DETAILS_VIEW;
        $decisionTable = $organization ? TablesParams::ORGANIZATION_DETAILS_VIEW : TablesParams::EMPLOYEE_DETAILS_VIEW;
        $whereField = $organization ? 'Id' : 'IndividualEmployeeId';

        $certificate = $this->searchEngine
                        ->select(['table' => $certificateTable, 'where' => ['equal' => ['Id' => $guid]]], true)[0];
        $details = $this->searchEngine
                        ->select(['table' => $decisionTable, 'where' => ['equal' => ['Id' => $certificate->$whereField]]], true)[0];


        return new ViewModel([
            'organization' => $organization,
            'certificate' => $certificate,
            'details' => $details,
            'decisions' => $decisions,
            'eduLevels' => $eduLevels,
            'supplements' => $supplements,
        ]);
    }

    public function supplementAction() {

        $id = (int) $this->params('id', 0);

        $supplementSelectParams = [
            'table' => TablesParams::CERTIFICATE_SUPPLEMENT,
            'columns' => [
                'id',
                'SerialNumber',
                'FormNumber',
                'EduOrgFullName',
                'EduOrgShortName',
                'EduOrgAddress',
                'Note',
                'IsForBranch',
                'eduorg_id'
            ],
            'where' => [
                'equal' => [
                    TablesParams::CERTIFICATE_SUPPLEMENT . '._id' => $id,
                ]
            ],
            'join' => [
                TablesParams::CERTIFICATE_SUPPLEMENT_STATUS => [
                    'alias' => 'cs',
                    'on' => 'cs._id = certificatesupplementstatus_id',
                    'columns' => [
                        'StatusName' => 'Name',
                    ],
                    'type' => 'left',
                ],
            ],
        ];

        $supplement = $this->searchEngine->select($supplementSelectParams)[0];

        $branch = null;

        if ((int) $supplement->IsForBranch) {
            $eduorg_id = (int) $supplement->eduorg_id;
            $branch = $this->searchEngine->select(['table' => TablesParams::ORGANIZATION_DETAILS_VIEW, 'where' => ['equal' => ['_id' => $eduorg_id]]])[0];
        }

        $decisionSelectParams = [
            'table' => TablesParams::CERTIFICATE_DECISION,
            'columns' => [
                'Kind' => 'OrderDocumentKind',
                'Number' => 'OrderDocumentNumber',
                'Date' => 'DecisionDate',
            ],
            'where' => [
                'equal' => [
                    'startedsupplement_id' => $id,
                ]
            ],
            'join' => [
                TablesParams::CERTIFICATE_DECISION_TYPE => [
                    'alias' => 'ct',
                    'on' => 'ct._id = certificatedecisiontype_id',
                    'columns' => [
                        'Name'
                    ],
                    'type' => 'left',
                ],
            ],
        ];

        $programmSelectParams = [
            'table' => TablesParams::ACCREDITED_PROGRAMM,
            'columns' => [
                'UGSCode',
                'UGSName',
                'ProgrammCode',
                'ProgrammName',
                'Qualification',
                'edulevel_id',
                'Canceled',
                'Suspended',
            ],
            'where' => [
                'equal' => [
                    'certificatesupplement_id' => $id,
                ]
            ],
            'join' => [
                TablesParams::EDUCATION_LEVEL => [
                    'alias' => 'el',
                    'on' => 'el._id = edulevel_id',
                    'columns' => [
                        'LevelName' => 'Name',
                        'elShowOrder' => 'ShowOrder',
                    ],
                    'type' => 'left',
                ],
            ],
            'order' => [
                'elShowOrder asc',
                'ProgrammCode asc',
            ],
        ];

        $decisions = $this->searchEngine->select($decisionSelectParams, false);
        $programms = $this->searchEngine->select($programmSelectParams, false);

        $model = new ViewModel([
            'branch' => $branch,
            'supplement' => $supplement,
            'decisions' => $decisions,
            'programms' => $programms,
        ]);
        $model->setTerminal(true);
        return $model;
    }

    public function searchAction() {

        $response = $this->getResponse();
        $response->setStatusCode(200);

        try {
            $code = $this->params()->fromQuery('code', '');
            $level = $this->params()->fromQuery('level', '');
            $supplements = $this->params()->fromQuery('supplements', []);
            $where = [];
            if (!empty($code)) {
                $where['ProgrammCode'] = $code;
            }
            if (!empty($level)) {
                $where['edulevel_id'] = $level;
            }
            if (!count($where)) {
                throw new ReestrException('Search params is empty');
            }

            $searchSelectParams = [
                'table' => TablesParams::ACCREDITED_PROGRAMM,
                'columns' => [
                    new Expression('DISTINCT(certificatesupplement_id) as certificatesupplement_id'),
                ],
                'where' => [
                    'equal' => $where,
                    'in' => [
                        'certificatesupplement_id' => $supplements,
                    ],
                ],
            ];

            $found = $this->searchEngine->select($searchSelectParams);

            array_walk($found, function(&$item) {
                $item = $item->certificatesupplement_id;
            });

            $response->setContent(json_encode([
                'status' => 'OK',
                'found' => $found,
            ]));

            return $response;
        } catch (ReestrException $ex) {
            $response->setContent(json_encode([
                'status' => 'FAIL',
                'message' => $ex->getMessage(),
            ]));
            return $response;
        }
    }

}
