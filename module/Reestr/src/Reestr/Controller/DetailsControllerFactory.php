<?php

namespace Reestr\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Reestr\Controller\DetailsController;

/**
 * Description of ReestrControllerFactory
 *
 * @author Дмитрий Бубякин
 */
class DetailsControllerFactory implements FactoryInterface {

    public function __invoke(ServiceLocatorInterface $container) {
        return new DetailsController($container->getServiceLocator());
    }

    public function createService(ServiceLocatorInterface $container) {
        return $this($container);
    }

}
