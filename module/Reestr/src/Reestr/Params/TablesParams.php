<?php

namespace Reestr\Params;

/**
 * Description of TablesEntity
 *
 * @author Дмитрий Бубякин
 */
class TablesParams {

    //Переименовать бы все это..
    const REGION = 'region';
    const EDU_ORG_TYPE = 'eduorgtype';
    const EDU_ORG_KIND = 'eduorgkind';
    const CERTIFICATES_STATUS = 'certificatestatus';
    const EDUCATION_LEVEL = 'edulevel';
    const EDUCATION_ORGANIZATION = 'educationorganization';
    const INDIVIDUAL_EMPLOYEE = 'individualemployee';
    const RECEIVED_PACKAGE = 'receivedpackage';
    const ACCREDITATION_CERTIFICATE = 'accreditationcertificate';
    const CERTIFICATE_DECISION = 'certificatedecision';
    const CERTIFICATE_DECISION_TYPE = 'certificatedecisiontype';
    const ORGANIZATION_DETAILS_VIEW = 'organizationdetailsview';
    const EMPLOYEE_DETAILS_VIEW = 'employeedetailsview';
    const CERTIFICATE_ORGANIZATION_DETAILS_VIEW = 'certificateorganizationdetailsview';
    const CERTIFICATE_EMPLOYEE_DETAILS_VIEW = 'certificateemployeedetailsview';
    const CERTIFICATE_SUPPLEMENTS_VIEW = 'certificatesupplementsview';
    const CERTIFICATE_SUPPLEMENT = 'certificatesupplement';
    const CERTIFICATE_SUPPLEMENT_STATUS = 'certificatesupplementstatus';
    const ACCREDITED_PROGRAMM = 'accreditedprogramm';
    const EDUCATION_SUB_LEVEL = 'edusublevel';
    const EDU_ORG_FORM = 'eduorgform';
    const CERTIFICATE_TYPE = 'certificatetype';
    const EDU_PROGRAMM_TYPE = 'eduprogramtype';
    const EDU_PROGRAMM_KIND = 'eduprogramkind';
    const INDIVIDUAL_EMPLOYEE_TEMP = 'individualemployee_temp';
    const XML_CERTIFICATE_VIEW = 'xml_certificate_view';
    const XML_ORGANIZATION_VIEW = 'xml_organization_view';
    const XML_ENTREPRENEUR_VIEW = 'xml_entrepreneur_view';
    const XML_SUPPLEMENT_VIEW = 'xml_supplement_view';
    const XML_PROGRAM_VIEW = 'xml_program_view';
    const XML_DECISION_VIEW = 'xml_decision_view';

    //Столбцы что пишем в XML
    public static $XML_CERTIFICATE_PARAMS = [
        'IsFederal',
        'Id',
        'StatusName',
        'StatusCode',
        'TypeName',
        'TypeCode',
        'RegionName',
        'RegionCode',
        'FederalDistrictName',
        'FederalDistrictShortName',
        'FederalDistrictCode',
        'RegNumber',
        'SerialNumber',
        'FormNumber',
        'IssueDate',
        'EndDate',
        'ControlOrgan',
        'PostAddress',
        'EduOrgFullName',
        'EduOrgShortName',
        'EduOrgINN',
        'EduOrgKPP',
        'EduOrgOGRN',
        'IndividualEntrepreneurLastName',
        'IndividualEntrepreneurFirstName',
        'IndividualEntrepreneurMiddleName',
        'IndividualEntrepreneurAddress',
        'IndividualEntrepreneurEGRIP',
        'IndividualEntrepreneurINN',
    ];
    public static $XML_ORGANIZATION_PARAMS = [
        'Id',
        'FullName',
        'ShortName',
        'HeadEduOrgId',
        'IsBranch',
        'PostAddress',
        'Phone',
        'Fax',
        'Email',
        'WebSite',
        'OGRN',
        'INN',
        'KPP',
        'HeadPost',
        'HeadName',
        'FormName',
        'FormCode',
        'KindName',
        'KindCode',
        'TypeName',
        'TypeCode',
        'RegionName',
        'RegionCode',
        'FederalDistrictCode',
        'FederalDistrictShortName',
        'FederalDistrictName',
    ];
    public static $XML_ENTREPRENEUR_PARAMS = [
        'Id',
        'LastName',
        'FirstName',
        'MiddleName',
        'PostAddress',
        'EGRIP',
        'INN',
    ];
    public static $XML_SUPPLEMENT_PARAMS = [
        'Id',
        'StatusName',
        'StatusCode',
        'Number',
        'SerialNumber',
        'FormNumber',
        'IssueDate',
        'IsForBranch',
        'Note',
        'EduOrgFullName',
        'EduOrgShortName',
        'EduOrgAddress',
        'EduOrgKPP',
    ];
    public static $XML_PROGRAM_PARAMS = [
        'Id',
        'TypeName',
        'TypeCode',
        'KindName',
        'KindCode',
        'EduLevelName',
        'EdulevelShortName',
        'EduLevelCode',
        'EduSubLevelName',
        'EduSubLevelCode',
        'ProgrammName',
        'ProgrammCode',
        'OKSOCode',
        'UGSName',
        'UGSCode',
        'EduNormativePeriod',
        'Qualification',
        'IsAccredited',
        'IsCanceled',
        'IsSuspended',
    ];
    public static $XML_DECISION_VIEW = [
        'Id',
        'DecisionTypeName',
        'DecisionTypeCode',
        'OrderDocumentNumber',
        'OrderDocumentKind',
        'DecisionDate',
        'AdditionalInfo',
        'StartedSupplementId',
        'StartedSupplementNumber',
        'StartedSupplementSerialNumber',
        'StartedSupplementFormNumber',
        'StartedSupplementIssueDate',
        'StoppedSupplementId',
        'StoppedSupplementNumber',
        'StoppedSupplementSerialNumber',
        'StoppedSupplementFormNumber',
        'StoppedSupplementIssueDate',
        'OldRegNumber',
        'OldSerialNumber',
        'OldFormNumber',
        'OldIssueDate',
    ];
    //Параметры для парсинга Excel
    public static $CERTIFICATE_SHEET = [
        'certificate.Id',
        'certificate.EduOrgId',
        'certificate.IndividualEmployeeId',
        'certificate.ControlOrgan',
        'certificate.EduOrgFullName',
        'certificate.EduOrgShortName',
        'certificate.PostAddress',
        'certificate.EduOrgOGRN',
        'certificate.EduOrgKPP',
        'certificate.EduOrgINN',
        'organization.FullName',
        'organization.ShortName',
        'organization.fk.EduOrgFormId',
        'organization.fk.EduOrgTypeId',
        'organization.fk.EduOrgKindId',
        'organization.fk.RegionId',
        'organization.PostAddress',
        'organization.OGRN',
        'organization.KPP',
        'organization.INN',
        'organization.HeadPost',
        'organization.HeadName',
        'organization.Phone',
        'organization.Fax',
        'organization.Email',
        'organization.WebSite',
        'employee.LastName',
        'employee.FirstName',
        'employee.MiddleName',
        'employee.PostAddress',
        'employee.EGRIPNumber',
        'employee.INN',
        'certificate.SerialNumber',
        'certificate.FormNumber',
        'certificate.fk.CertificateTypeId',
        'certificate.dt.short.IssueDate',
        'certificate.RegNumber',
        'certificate.fk.CertificateStatusId',
        'certificate.dt.short.EndDate',
    ];
    public static $SUPPLEMENT_SHEET = [
        'supplement.Id',
        'supplement.CertificateId',
        'supplement.EduOrgId',
        'supplement.EduOrgFullName',
        'supplement.EduOrgShortName',
        'supplement.EduOrgAddress',
        'supplement.EduOrgKPP',
        'organization.FullName',
        'organization.ShortName',
        'organization.PostAddress',
        'organization.KPP',
        'organization.HeadPost',
        'organization.HeadName',
        'organization.Phone',
        'organization.Fax',
        'organization.Email',
        'organization.WebSite',
        'supplement.Number',
        'supplement.SerialNumber',
        'supplement.FormNumber',
        'supplement.fk.CertificateSupplementStatusId',
        'supplement.dt.short.IssueDate',
        'supplement.Source',
        'supplement.Note'
    ];
    public static $PROGRAMM_SHEET = [
        'programm.Id',
        'programm.CertificateSupplementId',
        'programm.ProgrammCode',
        'programm.ProgrammName',
        'programm.fk.EduLevelId',
        'empty',
        'programm.fk.EduProgrammTypeId',
        'empty',
        'programm.UGSCode',
        'programm.UGSName',
        'programm.OKSOCode',
        'programm.Qualification',
        'programm.EduNormativePeriod',
        'programm.IsAccredited',
        'programm.Suspended',
        'programm.Canceled',
    ];
    public static $DECISION_SHEET = [
        'decision.Id',
        'decision.CertificateId',
        'decision.fk.CertificateDecisionTypeId',
        'decision.dt.short.DecisionDate',
        'decision.OrderDocumentKind',
        'decision.OrderDocumentNumber',
        'empty',
        'decision.OldRegNumber',
        'decision.OldFormNumber',
        'decision.OldSerialNumber',
        'decision.dt.short.OldIssueDate',
        'decision.StoppedSupplementNumber',
        'decision.StoppedSupplementFormNumber',
        'decision.StoppedSupplementSerialNumber',
        'decision.dt.short.StoppedSupplementIssueDate',
        'decision.StartedSupplementNumber',
        'decision.StartedSupplementFormNumber',
        'decision.StartedSupplementSerialNumber',
        'decision.dt.short.StartedSupplementIssueDate',
        'empty',
    ];
    public static $PACKAGE_SHEET = [
        'empty',
        'package.dt.long.TransferDate',
    ];

}
