<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Acl;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    protected $router;
    protected $response;
    public function onBootstrap(MvcEvent $e)
    {
        $this -> initAcl($e);
        $e -> getApplication() -> getEventManager() -> attach('route', array($this, 'checkAcl'));

        $this->router = $e->getRouter();
        $this->response = $e->getResponse();

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function initAcl(MvcEvent $e) {

        $acl = new \Zend\Permissions\Acl\Acl();
        $config = $this->getConfig();
        $roles = $config["access"];
        $allResources = array();
        foreach ($roles as $role => $resources) {
            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);
            $acl -> addRole($role);
            $allResources = array_merge($resources, $allResources);
            //adding resources
            foreach ($resources as $resource) {
                // Edit 4
                if(!$acl ->hasResource($resource))
                    $acl -> addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
            }
            //adding restrictions
            foreach ($allResources as $resource) {
                $acl -> allow($role, $resource);
            }
        }
        //testing
        //var_dump($acl->isAllowed('guest','home'));die;
        //true

        //setting to view
        $e -> getViewModel() -> acl = $acl;

    }

    public function checkAcl(MvcEvent $e) {
        $route = $e -> getRouteMatch() -> getMatchedRouteName();
//

//
        //you set your role
        $userRole = 'guest';
        $serviceManager = $e->getApplication()->getServiceManager();
        $authService =  $serviceManager->get('AuthService');
        ////$logger = new \Model\Logger\Logger("aa");
        //$logger->Log("Acl");
        //$logger->Log($authService->getStorage()->getNamespace());
        //$logger->Log("acl hasidentity:");
        //$logger->Log(print_r($authService->hasIdentity(),true));
        //if($authService->hasIdentity())
        //@$logger->Log(print_r($authService->getIdentity(),true));

        if($authService->hasIdentity()){
            $user = $authService->getIdentity();
            if($user){
                if($user->getRole() == 'expert')
                    $userRole = "ron_user";
                else
                    $userRole = "eo_user";
            }
        }

        if (!$e -> getViewModel() -> acl -> isAllowed($userRole, $route)) {
            $url = $this->router->assemble(array(), array('name' => 'auth/default'));
            $this->response->getHeaders()->addHeaderLine('Location', $url);
            $this->response->setStatusCode(302);
            $this->response->sendHeaders();
        }
    }
}
