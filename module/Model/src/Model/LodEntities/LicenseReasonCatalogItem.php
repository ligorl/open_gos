<?php
namespace Model\LodEntities;

use Model\Gateway;
use Zend\Db\Sql\Select;

class LicenseReasonCatalogItem extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseReasonCatalogItem";
    protected $idName = "id";

    // ид-ки причин которые использовать на открытой части на фильты
    public  $licenseReasonForOpenId = array(
        //'aac9fc11db3d4682a3e80a64dabc1ede', //Открытие образовательной деятельности по адресу
        //'9ccbaed80f8248c9b1af9ff6f5bf02c9', //Открытие новых образовательных программ
        //'6d8c38b1ab79486eb8f826943b723f4c', //Открытие филиала

        //'3e5b5f379598446ea2833f4acd2ba01b', //Изменение наименования лицензиата
        //'0ba32b48a6384b5c95dcb630f34c930d', //Прекращение образ. деятельности по адресу
        //'33b935940d1a4c5383ee72accbdf4ee9', //Прекращение реализации ОП
        //'fd77ff8629184ae89d18858d2b4d7a50', //Изменение наименований ОП
        //'2b6b9edcdf394ea9b3b67c9ac8a23b7e', //Установление бессрочного действия лицензии
        //'fb71a4f02e7d4f07be60fcc43fe2e9a2', //Изменение адреса места нахождения

        //'42807cbc9a9c419ba7ebc374652ed68a', //Прекращение осуществления ОД в целом
        //'b4bfaa7993784256890dc19a8deb7e88', //Прекращение осуществления ОД в филиале

        //'34bee06f79144d5584668ba9fad809f8', //Предоставление копии лицензии и (или) приложений

        'efb3b9bf460c4a8aae48b9dd5e5d5d84', //Утрата лицензии
        '1c3acd333a204d5288277d2c2fd3509b', //Порча лицензии – необходимо предоставить испорченный бланк лицензии, поэтому, скорее всего заявление по этой причине предоставляют только лично
    );


    /**
     * возражает типы причин которые надо выводить в причины, с учетом того это тестовый или боевойй сервер
     * 
     * @return type
     * @throws \Exception
     */
    public function getLicenseReasonForOpenId() {        
        $user = \Model\Entities\isgaUsers::getAutorisateClass();
        if(empty($user)){
            throw new \Exception('Пользовательне не аутентифицирован');
        }
        $identityUser = $user;

        $userName = '';
        if( !empty($identityUser) ){
            $userName = $identityUser->getFieldSafe('Login');
        }
        if( \Eo\Model\Export::isTestServer() || 'eouser' == $userName || 'eouser1' == $userName ){
            //типы процедур которые использовать только на открытой
            return  array(
                'aac9fc11db3d4682a3e80a64dabc1ede', //Открытие образовательной деятельности по адресу
                '9ccbaed80f8248c9b1af9ff6f5bf02c9', //Открытие новых образовательных программ
                '6d8c38b1ab79486eb8f826943b723f4c', //Открытие филиала

                '3e5b5f379598446ea2833f4acd2ba01b', //Изменение наименования лицензиата
                '0ba32b48a6384b5c95dcb630f34c930d', //Прекращение образ. деятельности по адресу
                '33b935940d1a4c5383ee72accbdf4ee9', //Прекращение реализации ОП
                'fd77ff8629184ae89d18858d2b4d7a50', //Изменение наименований ОП
                '2b6b9edcdf394ea9b3b67c9ac8a23b7e', //Установление бессрочного действия лицензии
                'fb71a4f02e7d4f07be60fcc43fe2e9a2', //Изменение адреса места нахождения

                '42807cbc9a9c419ba7ebc374652ed68a', //Прекращение осуществления ОД в целом
                'b4bfaa7993784256890dc19a8deb7e88', //Прекращение осуществления ОД в филиале

                '34bee06f79144d5584668ba9fad809f8', //Предоставление копии лицензии и (или) приложений

                'efb3b9bf460c4a8aae48b9dd5e5d5d84', //Утрата лицензии
                '1c3acd333a204d5288277d2c2fd3509b', //Порча лицензии – необходимо предоставить испорченный бланк лицензии, поэтому, скорее всего заявление по этой причине предоставляют только лично
            );
        }
        else{
            return  array(
                //'aac9fc11db3d4682a3e80a64dabc1ede', //Открытие образовательной деятельности по адресу
                //'9ccbaed80f8248c9b1af9ff6f5bf02c9', //Открытие новых образовательных программ
                //'6d8c38b1ab79486eb8f826943b723f4c', //Открытие филиала

                //'3e5b5f379598446ea2833f4acd2ba01b', //Изменение наименования лицензиата
                //'0ba32b48a6384b5c95dcb630f34c930d', //Прекращение образ. деятельности по адресу
                //'33b935940d1a4c5383ee72accbdf4ee9', //Прекращение реализации ОП
                //'fd77ff8629184ae89d18858d2b4d7a50', //Изменение наименований ОП
                //'2b6b9edcdf394ea9b3b67c9ac8a23b7e', //Установление бессрочного действия лицензии
                //'fb71a4f02e7d4f07be60fcc43fe2e9a2', //Изменение адреса места нахождения

                //'42807cbc9a9c419ba7ebc374652ed68a', //Прекращение осуществления ОД в целом
                //'b4bfaa7993784256890dc19a8deb7e88', //Прекращение осуществления ОД в филиале

                //'34bee06f79144d5584668ba9fad809f8', //Предоставление копии лицензии и (или) приложений

                'efb3b9bf460c4a8aae48b9dd5e5d5d84', //Утрата лицензии
                '1c3acd333a204d5288277d2c2fd3509b', //Порча лицензии – необходимо предоставить испорченный бланк лицензии, поэтому, скорее всего заявление по этой причине предоставляют только лично
            );
        }

    }

    public function getFrames(){
        $framesList = $this->getField("frames");
        $framesList = explode(",",$framesList);
        $returnFrames = [];
        foreach($framesList as $frame){
            $frame=trim($frame);
            if($frame!="")
                $returnFrames[] = $frame;
        }

        return $returnFrames;
    }

    public function getDocumentPrint() {
        $result = $this->getFieldOrSpace("DocumentPrint");
        if (empty($result)){
            $result = $this->getFieldOrSpace("documentTitle");
        }
        if (empty($result)){
            $result = $this->getFieldOrSpace("title");
        }
        return $result;
    }

    public function getByServiceReasonCode($serviceReasonCode)
    {
        return $this->getAllByWhere([
            'ServiceReasonCode' => $serviceReasonCode
        ])->getObjectsArray();
    }

    public function getName()
    {
        return $this->getField('title');
    }

    /**
     * получение всех записей в виде массива id => title
     * @return array
     */
    public function getAllData()
    {
        $result = $this->tableGateway->select(function(Select $select)
        {
            $select
                ->columns(array("id", "title"));
        });
        $data = array();

        foreach($result as $row){
            $data[$row->fields['id']] = $row->fields['title'];
        }

        return $data;
    }
}
