<?php

namespace Model\LodEntities;

use Model\Entities\eiisEducationalOrganizations;
use Model\Gateway\LodBaseEntity;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Where;

class ServiceReasonCatalog extends LodBaseEntity
{
    protected $tableName = 'ServiceReasonCatalog';
    protected $idName = 'id';

    public function getByAppealTypeCode($appealTypeCode)
    {
        return $this->getAllByWhere([
            'appealTypeCode' => $appealTypeCode
        ])->getObjectsArray();
    }

    /**
     * Выборка оснований услуг.
     *
     * @return mixed
     */
    public function getAllNotTemp()
    {
        $serviceReasons = $this->getAllByWhereWithOrder(
            array(),
            array('sort ASC', 'code ASC')
        );

        return $serviceReasons;
    }
}