<?php

namespace Model\LodEntities;

use Model\Gateway;

class Deal extends Gateway\LodBaseEntity
{

    protected $tableName = "Deal";
    protected $idName = "id";
    /**
     *  индексы для сортировки
     * @var type
     */
    public static $reasonSortIndex = array(
        '2b6b9edcdf394ea9b3b67c9ac8a23b7e' => 20, //Установление бессрочного действия лицензии 
        '518dfabe3375431684b71d2faa5c7564' => 30, //реорганизация в форме преобразования'
        'c83201802b474c4393305b16b7c05375' => 40, //реорганизация в форме присоединения
        'f9893ac3e72e43b59101a8270f9eaacf' => 50, //реорганизация в форме слияния
        '3e5b5f379598446ea2833f4acd2ba01b' => 60, //Изменение наименования лицензиата
        'fb71a4f02e7d4f07be60fcc43fe2e9a2' => 70, //изменение адреса места нахождения
        '0ba32b48a6384b5c95dcb630f34c930d' => 80, //Прекращение образовательной деятельности по адресу
        'fd77ff8629184ae89d18858d2b4d7a50' => 90, //Изменение наименований образовательных программ
        '33b935940d1a4c5383ee72accbdf4ee9' => 100, //Прекращение реализации образовательных программ
    );

    /**
     * Возвращает объект с процедурой из таблицы LicensingProcedure
     * @return type
     */
    public function getProcedure()
    {
        $LicRequest = $this->getLinkedItem("LicenseRequest", array("id" => $this->fields["mainLicenseRequestId"]));
        if ($LicRequest) {
            $LicProcedure = $LicRequest->getLinkedItem("LicensingProcedure",
                                                       array("id" => $LicRequest->getField("licensingProcedureId")));
            return $LicProcedure;
        } else {
            return false;
        }
    }

    public function getProcedureTitle()
    {
        $proc = $this->getProcedure();
        if ($proc) {
            return $proc->getField("title");
        } else {
            return "";
        }
    }

    public function getProcedureId()
    {
        $proc = $this->getProcedure();
        if ($proc) {
            return $proc->getField("id");
        } else {
            return false;
        }
    }

    /**
     * Получение организации из таблицы eiis_EducationalOrganizations из базы f11_mon
     * @return type
     */
    public function getOrganizationEiis()
    {
        $organisation = $this->getLinkedItemIsga("eiis_EducationalOrganizations",
                                                 array("Id" => $this->fields["organizationId"]));
        return $organisation;
    }

    public function getOrganization()
    {
        $eoId = $this->getField('organizationId');
        if (empty($this->getField('organizationId'))) {
            return null;
        }
        return new \Model\Entities\eiisEducationalOrganizations($eoId);
    }

    /**
     * Получение имени организации из eiis_EducationalOrganizations из базы f11_mon
     */
    public function getOrganNameEiis(){
        $organ = $this->getOrganizationEiis();
        if ($organ) {
            if(!$organ->isEmptyField("RegularName")){
                 return $organ->getField("RegularName");
            }else{
                 return $organ->getField("FullName");
            }
        } else {
            return "";
        }
    }

    /**
     * Получаем исполнителя
     * @return type
     */
    public function getExecutor()
    {
        if ($this->isEmptyField('expertId')) {
            return null;
        }
        $Executor = $this->getLinkedItem(
          "Lod_User", array("uuid" => $this->fields["expertId"])
        );
        return $Executor;
    }

    /**
     * Получаем фио исполнитля
     * @return string
     */
    public function getFioExecutor()
    {
        $Executor = $this->getExecutor();
        if ($Executor) {
            $Lastname = $Executor->getField('lastName');
            $FirstName = $Executor->getField('firstName');
            $middleName = $Executor->getField('middleName');
            return $Lastname . " " . mb_substr($FirstName, 0, 1, "UTF-8") . "." . mb_substr($middleName, 0, 1, "UTF-8") . ".";
        } else {
            return "";
        }
    }

    /**
     * Получаем фио исполнитля полную
     * @return string
     */
    public function getFioExecutorFull()
    {
        $Executor = $this->getExecutor();
        if ($Executor) {
            $Lastname = $Executor->getField('lastName');
            $FirstName = $Executor->getField('firstName');
            $middleName = $Executor->getField('middleName');
            return $Lastname . " " . $FirstName . " " . $middleName;
        } else {
            return "";
        }
    }

    /**
     * Получаем фио исполнитля полную
     * @return string
     */
    public function getFioExecutorFullGen()
    {
        $Executor = $this->getExecutor();
        if ($Executor) {
            $Lastname = $Executor->getField('lastName');
            $FirstName = $Executor->getField('firstName');
            $middleName = $Executor->getField('middleName');
            $morphy = new \Islod\Model\Tools\Morphy();

            if (!empty($Lastname)) {
                //вернет четыре формы
                $fourForm = $morphy->getForms(mb_strtolower($Lastname));
                if (!empty($fourForm['genitive'])) {
                    $Lastname = $morphy->firstLetterToUp($fourForm['genitive']);
                }
            }
            if (!empty($FirstName)) {
                //вернет четыре формы
                $fourForm = $morphy->getForms(mb_strtolower($FirstName));
                if (!empty($fourForm['genitive'])) {
                    $FirstName = $morphy->firstLetterToUp($fourForm['genitive']);
                }
            }
            if (!empty($middleName)) {
                //вернет четыре формы
                $fourForm = $morphy->getForms(mb_strtolower($middleName));
                if (!empty($fourForm['genitive'])) {
                    $middleName = $morphy->firstLetterToUp($fourForm['genitive']);
                }
            }
            $result = $Lastname . " " . $FirstName . " " . $middleName;
            return $result;
        } else {
            return "";
        }
    }

    /**
     * инициалы исполнителя в формате
     * И.О. Фамилия
     *
     * @return string
     */
    public function getFioExecutorShortOposite()
    {
        $Executor = $this->getExecutor();
        if ($Executor) {
            $last = $Executor->getFieldOrSpace('lastName');
            $first = $Executor->getFieldOrSpace('firstName');
            $middle = $Executor->getFieldOrSpace('middleName');
            //первую большой
            $last = mb_strtoupper(mb_substr($last, 0, 1, "UTF-8")) . mb_substr($last, 1, null, "UTF-8");
            $first = mb_substr($first, 0, 1, "UTF-8");
            $middle = mb_substr($middle, 0, 1, "UTF-8");
            $result = $first . '.' . $middle . '.' . ' ' . $last;
            return $result;
        } else {
            return "";
        }
    }

    /**
     * наименование должности в родительном падеже
     *
     * @return string
     */
    public function getExecutorPostGen()
    {
        $result = '';
        $Executor = $this->getExecutor();
        if ($Executor) {
            $result = $Executor->getTitleGen();
        }
        return $result;
    }

    /**
     * получаем главное зявление которое связано через поле mainLicenseRequestId
     * @return type
     */
    public function getMainLicenseRequest()
    {
        $LicenseRequest = $this->getLinkedItem("LicenseRequest", array("id" => $this->fields["mainLicenseRequestId"]));
        return $LicenseRequest;
    }

    /**
     * Получение всех приложений связанный с делом из таблицы eiis_LicenseSupplements
     * @return arrayObject
     */
    public function getAllSupplementsIsga($onlyIds = false)
    {
        $licSupplIds = $this->getUniqueSupplementIds();
        if (count($licSupplIds) < 1) {
            return false;
        }
        if ($onlyIds) {
            return $licSupplIds;
        }
        $isgaSuppl = $this->getLinkedItemsIsga("eiis_LicenseSupplements", array("Id" => $licSupplIds));
        return $isgaSuppl;
    }

    /**
     * получаем регистратора заявления
     * @return type
     */
    public function getRegistrator()
    {
        $Registrator = null;
        $LicenseRequest = $this->getMainLicenseRequest();
        if (!empty($LicenseRequest)) {
            $Registrator = $this->getLinkedItemIsga("ron_Users",
                                                    array("Id" => $LicenseRequest->getField("registratorId")));
        }
        return $Registrator;
    }

    /**
     * Получаем фио регистратора
     * @return string
     */
    public function getFioRegisteator()
    {
        $Registrator = $this->getRegistrator();
        if ($Registrator) {
            $Name = $Registrator->getField('Name');

            return $Name;
        } else {
            return "";
        }
    }

    /**
     * получаем заголовок типа получения
     * @return string
     */
    public function typeCome()
    {
        $licenseRequest = $this->getMainLicenseRequest();
        $TypeCome = false;
        if (!empty($licenseRequest)) {
            $TypeCome = $this->getLinkedItem("CatalogItem", array("id" => $licenseRequest->getField("admissionType")));
        }
        $title = '';
        if ($TypeCome) {
            $title = $TypeCome->getField("title");
        }
        $lastName = $licenseRequest->getField("requestRepresentativeLastName");
        $firstName = $licenseRequest->getField("requestRepresentativeFirstName");
        $middleName = $licenseRequest->getField("requestRepresentativeMiddleName");
        $fullName = $lastName . " " . $firstName . " " . $middleName;
        if ($fullName != "  ") {
            $title = $title . ", " . $fullName;
        }
        return $title;
    }

    /**
     * получаем имя статуса
     * @return string
     */
    public function getStateName()
    {
        $State = $this->getLinkedItem("catalog_deal", array("origin_name" => $this->fields["stateCode"]));
        if ($State) {
            return $State->getField("russian_name");
        } else {
            return "";
        }
    }

    /**
     * получение причины из главной заявки
     * @return type
     */
    public function getListOfCauses()
    {
        $Couses = $this->getLinkedItems("LicenseReasonToLicenseRequest",
                                        array("licenseRequestId" => $this->fields["mainLicenseRequestId"]));
        return $Couses;
    }

    /**
     * получение всех id причин из всех заявок
     * @return type
     */
    public function getListOfCauseIds()
    {
        $requestIds = $this->getAllRequestIds();
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseReasonToLicenseRequest')
          ->columns(array("licenseReasonId"), false)
          ->where(array("licenseRequestId" => $requestIds))
        ;
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), "licenseReasonId");
    }

    /**
     * получение всех title причин из всех заявок
     *
     * @param type $inArray - истина - получить массив
     *                      - ложь   - строку
     * @return string
     */
    public function getListOfCauseTitle($inArray = false, $delimiter = ',')
    {
        $requestIds = $this->getAllRequestIds();
        if (count($requestIds) > 0) {
            $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
            $sql = new \Zend\Db\Sql\Sql($dbAdapter);
            $select = $sql->select();
            $select->from('LicenseReasonToLicenseRequest')
              ->columns(array("licenseReasonId"), false)
              ->where(array("licenseRequestId" => $requestIds))
              ->join("LicenseReasonCatalogItem",
                     "LicenseReasonToLicenseRequest.licenseReasonId = LicenseReasonCatalogItem.id", "title");
            $selectString = $sql->getSqlStringForSqlObject($select);
            $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
            $resultArray = array_column($results->toArray(), "title", 'id');
            if ($inArray) {
                return $resultArray;
            }
            return implode($delimiter, array_unique($resultArray));
        } else {
            if ($inArray) {
                return array();
            }
            return "";
        }
    }

    public function getSupplementWithCouse($couseId, $orgId)
    {
        $requestIds = $this->getAllRequestIds();
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseReasonToLicenseRequest')
          ->columns(array("licenseRequestId"), false)
          ->where(array("licenseRequestId" => $requestIds, "licenseReasonId" => $couseId));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $licenseRequestNeedIds = array_column($results->toArray(), "licenseRequestId");
        $licenseIdsString = implode("','", $licenseRequestNeedIds);
        $selectString = "SELECT `f11_mon_lod`.`LicenseSupplementToLicenseRequest`.`licenseAttachId` as licenseAttachId "
          . "FROM `f11_mon_lod`.`LicenseSupplementToLicenseRequest` "
          . "INNER JOIN `f11_mon`.`eiis_LicenseSupplements` ON `f11_mon_lod`.`LicenseSupplementToLicenseRequest`.`licenseAttachId`=`f11_mon`.`eiis_LicenseSupplements`.`Id` "
          . "WHERE `f11_mon_lod`.`LicenseSupplementToLicenseRequest`.`licenseRequestId` IN('" . $licenseIdsString . "')" . " AND `f11_mon`.`eiis_LicenseSupplements`.`fk_eiisBranch` = '" . $orgId . "' ";
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), "licenseAttachId");
    }

    /**
     * получение всех заявок из таблицы LicenseRequest
     * @return type
     */
    public function getAllRequests()
    {

        $LicenseRequests = $this->getLinkedItemsWithOrder("LicenseRequest", array("dealId" => $this->fields["id"]),
                                                          "number ASC");
        return $LicenseRequests;
    }

    /**
     *
     */
    public function isDuplicateRequest()
    {
        $requests = $this->getAllRequests();
        $result = false;
        foreach ($requests as $request) {
            if ($request->getField("isDuplicate") == 1) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    /**
     * выберем заявления из дела
     *
     * @param type $whereAdd
     * @return type
     */
    public function getRequestAll($whereAdd = array())
    {
        if ($this->isEmptyField($this->getIdName())) {
            return null;
        }
        $where = array('dealId' => $this->getId());
        if (!empty($whereAdd)) {
            $where = array_merge($where, $whereAdd);
        }
        $result = new \Model\LodEntities\LicenseRequest();
        $result = $result->getAllByWhereParseId($where);
        return $result;
    }

    /**
     * Получение списка id связанных заявлений
     * @return array
     */
    public function getAllRequestIds()
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequest')
          ->columns(array("id"), false)
          ->where(array("dealId" => $this->fields["id"]));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), "id");
    }

    /**
     * Получение максимального номера дела
     * @return array
     */
    public function getMaxNumber()
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('Deal')
          ->columns(array("max" => new \Zend\Db\Sql\Expression('MAX( FLOOR( number ) )')), false);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), 'max');
    }

    /**
     * Сохранение заявления с необходимыми полями
     * @param type $requestId
     * @param type $organizationId
     * @param type $newNumb
     * @return type
     */
    public function saveNewDeal($requestId, $organizationId, $newNumb = false)
    {
        if (date("w") == 5) {
            $date = date("Y-m-d", strtotime("next monday"));
        } else {
            $date = date("Y-m-d");
        }
        if (!$newNumb) {
            $max_numb = $this->getMaxNumber();
            $newNumb = reset($max_numb) + 1;
        }

        $fields = array(
            "number"               => $newNumb,
            "stateCode"            => "REGISTRATION",
            "registrationDate"     => $date,
            "mainLicenseRequestId" => $requestId,
            "organizationId"       => $organizationId,
        );
        $this->setFields($fields);
        return $this->save(true);
    }

    /**
     * Получение дел по id организации и id процедуры
     * @param type $organId
     * @param type $procedureId
     * @return type
     */
    public function getDealsByOrganId($organId, $procedureId)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('Deal')
          ->columns(array("stateCode", "id", "mainLicenseRequestId", "number"))
          ->where("Deal.stateCode != 'FAIL_REGISTERED' AND Deal.organizationId='" . $organId . "' AND LicenseRequest.licensingProcedureId='" . $procedureId . "'")
          ->join("LicenseRequest", "Deal.mainLicenseRequestId = LicenseRequest.id", "licensingProcedureId");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }

    /**
     * Получение следующего подномера при выделении заявления
     * @return type
     */
    public function getNextNumberOfIssused()
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $number = $this->fields["number"];
        $organId = $this->fields["organizationId"];
        $select->from('Deal')
          ->columns(array("number"))
          ->where("`number` LIKE '%" . $number . "%' AND Deal.organizationId='" . $organId . "'")
          ->order("number DESC")
          ->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult = array_column($dbResults->toArray(), "number");
        $arrNumb = explode("-", $arrResult[0]);
        if (isset($arrNumb[1])) {
            $arrNumb[1] ++;
        } else {
            $arrNumb[1] = 1;
        }
        return implode("-", $arrNumb);
    }

    /**
     * Меняем статус дела
     * @param str $newState - новый статус
     * @param str $comm - комментарий
     * @return boolean
     */
    public function changeStatus($newState, $comm = '')
    {
        $this->setField("stateCode", $newState);
        if ($comm != "") {
            $this->setField("comment", $comm);
        }
        if ($this->save()) {
            //если меняется на статус На проверку
            //то отправим письмо
            if ('CHECK_LICENSE_REQUEST' == $newState) {
                \Islod\Model\Mail\ToCheck::send($this);
            }
            return true;
        } else {
            return false;
        }
    }

    public function setNewStatus($newState, $comm = '') {
        $oldState = $this->getField("stateCode");
        if( $this->changeStatus($newState, $comm) ){
            $dataHistory = array(
                "IdDeal"    => $this->getId(),
                "oldStatus" => $oldState,
                "newStatus" => $newState,
                "IdUser"    => 0,//$user->getField("Id")
            );
            $DealHistory = new \Model\LodEntities\DealStatusHistory();
            $DealHistory->setFieldsSafe($dataHistory);
            return $DealHistory->save( true );
        }
        return false;
    }

    /**
     * Проверяет есть ли документ с указанным типом
     * @param str $typeDoc
     */
    public function checkDocument($typeDoc, $retId = false)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $id = $this->fields["id"];
        $strType = implode("','", $typeDoc);
        $select->from('Document')
          ->columns(array("count" => new \Zend\Db\Sql\Expression('COUNT( Document.Id)'), "id" => "Id"))
          ->join("DocumentTypeCatalogItem", "Document.documentTypeId = DocumentTypeCatalogItem.id", array("parent"))
          ->where("Document.dealId = '" . $id . "' AND DocumentTypeCatalogItem.parent IN ('" . $strType . "') AND Document.scanCopyId IS NOT NULL AND Document.registrationNumber IS NOT NULL AND Document.registrationDate IS NOT NULL")
          ->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        if (!$retId) {
            $arrResult = array_column($dbResults->toArray(), "count");
            if ($arrResult[0] > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $arrResult = array_column($dbResults->toArray(), "id");
            if (!empty($arrResult[0])) {
                return $arrResult[0];
            } else {
                return false;
            }
        }
    }

    /**
     * Проверяет есть ли документ с указанным типом
     * @param str/array $typeDoc
     */
    public function checkDocumentByType($typeDoc)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        if (is_array($typeDoc) & !empty($typeDoc)) {
            $typeDoc = implode("','", $typeDoc);
        }
        $select = $sql->select();
        $id = $this->fields["id"];
        $select->from('Document')
          ->columns(array("Id", "registrationDate", "registrationNumber", "documentTypeId"))
          ->where("Document.dealId = '" . $id . "' AND documentTypeId IN('" . $typeDoc . "') AND scanCopyId IS NOT NULL AND registrationNumber IS NOT NULL AND registrationDate IS NOT NULL")
          ->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE)->toArray();
        if (isset($dbResults[0])) {
            return $dbResults[0];
        } else {
            return false;
        }
    }

    /**
     * Получает документы по типу.
     * @param str $typeDoc
     */
    public function getDocumentByType($typeDoc)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $id = $this->getId();
        $strType = implode("','", $typeDoc);
        $select->from('Document')
          ->columns(array("id", "documentReasonId", "registrationNumber", "receiveDate", "receiveAnswerDate", "documentTypeId", "documentReasonId", "printFormId", "registrationDate", "sendDate", "returnDate", "stateCode"))
          ->join("DocumentTypeCatalogItem", "Document.documentTypeId = DocumentTypeCatalogItem.id",
                 array("parent", "typeTitle" => "title"), "left")
          ->join("CatalogItem", "CatalogItem.id = Document.documentReasonId", array("catalogTitle" => "title"), "left")
          ->join("catalog_document", "catalog_document.origin_name = Document.stateCode",
                 array("statusTitle" => "russian_name"), "left")
          ->where("Document.dealId = '" . $id . "' AND DocumentTypeCatalogItem.parent IN('" . $strType . "')")
          ->order("documentTypeId DESC");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult = $dbResults->toArray();
        return $arrResult;
    }

    /**
     * дергаем историю измениеня статусов
     *
     * @param type $statusCode - только с этим статусом
     * @return type
     */
    public function getHistoryChange( $statusCode = '' )
    {
        $where = array("IdDeal" => $this->fields["id"]);
        if( !empty($statusCode) ){
            $where['newStatus'] = $statusCode;
        }
        $History = $this->getLinkedItems("DealStatusHistory", $where);
        return $History;
    }

    public function getRequestOrganizationInfo()
    {
        if ($this->isEmptyField('mainLicenseRequestId')) {
            return null;
        }
        $result = new \Model\LodEntities\RequestOrganizationInfo();
        $result = $result->getByWhere(array(
            'requestId' => $this->getField('mainLicenseRequestId'),
            'orgId'     => $this->getField('organizationId'),
        ));
        return $result;
    }

    /**
     * выдергиваем филиала, не адрес головняка а адрес другого филиала. Если же другойорганизации нет в таблице , то выводим головняк.
     * @return type
     */
    public function getRequestOrganizationInfoFilialImenno()
    {
        if ($this->isEmptyField('mainLicenseRequestId')) {
            return null;
        }
        $result = new \Model\LodEntities\RequestOrganizationInfo();
        $result = $result->getByWhere(array(
            'requestId' => $this->getField('mainLicenseRequestId'),
            new \Zend\Db\Sql\Predicate\NotLike('orgId', $this->getField('organizationId')),
        ));
        if (empty($result)) {
            $result = $this->getRequestOrganizationInfo();
        }
        return $result;
    }

    public function getOrganizationHeadEmployee()
    {
        if ($this->isEmptyField('organizationId')) {
            return null;
        }
        $return = new \Model\LodEntities\OrganizationHeadEmployee();
        $return = $return->getByWhere(array(
            'organizationId' => $this->getField('organizationId')
        ));
        return $return;
    }

    public function getLicenseRequestPayment()
    {
        if ($this->isEmptyField('mainLicenseRequestId')) {
            return null;
        }
        $result = new \Model\LodEntities\LicenseRequestPayment();
        $result = $result->getByWhere(array(
            'LicenseRequestId' => $this->getField('mainLicenseRequestId'),
        ));
        return $result;
    }

    public function getWhereArrayReestr()
    {
        $mainRequest = $this->getMainLicenseRequest();
        $procedure = $mainRequest->getField("licensingProcedureId");
        $allRequests = $this->getAllRequests();
        $allRequestsArr = $allRequests->toArray();
        $requestIds = array_column($allRequestsArr, "id");
        $Couses = $this->getLinkedItems("LicenseReasonToLicenseRequest", array("licenseRequestId" => $requestIds));
        $cousesId = array_column($Couses->toArray(), "licenseReasonId");
        return array("TypeProcedures" => $procedure, "Reasons IN('" . implode("','", $cousesId) . "')");
    }

    public function getDocumentWithLicense($licenseId = NULL, $licenseAttachId = NULL)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $id = $this->fields["id"];
        $where = "Document.dealId = '" . $id . "' AND ";
        if ($licenseId != NULL) {
            $where .= " LicenseAndLicenseSupplementToDocument.licenseId = '" . $licenseId . "' AND ";
        }
        if ($licenseAttachId != null) {
            $where .= " LicenseAndLicenseSupplementToDocument.licenseAttachId='" . $licenseAttachId . "'";
        } else {
            $where .= " 1";
        }
        $select->from('Document')
          ->columns(array("id", "documentReasonId", "registrationNumber", "documentTypeId", "documentReasonId", "registrationDate", "sendDate", "returnDate", "stateCode"))
          ->join("LicenseAndLicenseSupplementToDocument",
                 "Document.id = LicenseAndLicenseSupplementToDocument.documentId", array("licenseAttachId", "licenseId"))
          ->where($where)
          ->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $dbResults;
    }

    public function getCountInReestr()
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $id = $this->fields["id"];
        $select->from('RegistryChange')
          ->columns(array("count" => new \Zend\Db\Sql\Expression('COUNT( id)')))
          ->where(array("dealId = '" . $id . "'"));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult = array_column($dbResults->toArray(), "count");
        return $arrResult[0];
    }

    public function getAllOrgInfoIds()
    {
        $requestIds = $this->getAllRequestIds();
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('RequestOrganizationInfo')
          ->columns(array("orgId"), false)
          ->where(array("requestId" => $requestIds))
          ->group("orgId");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), "orgId");
    }

    public function getFullOrgInfo()
    {
        $requestIds = $this->getAllRequestIds();
        $requestIds = implode("','", $requestIds);
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $selectString = "SELECT orgId,fullTitle,shortTitle,RequestOrganizationInfo.address as address,RequestOrganizationInfo.kpp as kpp,RequestOrganizationInfo.ogrn as ogrn,RequestOrganizationInfo.inn AS inn,OrganizationLocation.settlementId as settlementId,fk_eiisParentEducationalOrganization "
          . "FROM RequestOrganizationInfo "
          . "LEFT JOIN OrganizationLocation ON RequestOrganizationInfo.orgId = OrganizationLocation.organizationId "
          . "LEFT JOIN f11_mon.eiis_EducationalOrganizations ON f11_mon.eiis_EducationalOrganizations.Id = RequestOrganizationInfo.orgId "
          . "WHERE requestId IN('" . $requestIds . "') AND OrganizationLocation.addressType = 'OrganizationLocation' "
          . "ORDER BY RequestOrganizationInfo.consecutiveNumber";
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }

    public function getLicenseRequestIds()
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequest')
          ->columns(array("licenseId"), false)
          ->where(array("dealId" => $this->fields["id"]))
          ->group("licenseId");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), "licenseId");
    }

    public function getUniqueSupplementIds()
    {
        $requestIds = $this->getAllRequestIds();
        if (count($requestIds) < 1) {
            return array();
        }
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from("LicenseSupplementToLicenseRequest")
          ->columns(array("licenseAttachId"))
          ->where(array(
              "licenseRequestId" => $requestIds
          ))
          ->group("licenseAttachId");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $Ids = array_column($results->toArray(), "licenseAttachId");
        return $Ids;
    }

    public function getReqOrgInfo($orgId)
    {
        $requestIds = $this->getAllRequestIds();
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('RequestOrganizationInfo')
          ->columns(array("id"), false)
          ->where(array("requestId" => $requestIds, "orgId" => $orgId));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $ids = array_column($results->toArray(), "id");
        return $ids;
    }

    public function getAdressByOrgId($orgInfoIds, $adressType = NULL)
    {
        if ($adressType == NULL) {
            $where = array("requestOrganizationInfoId" => $orgInfoIds);
        } else {
            $where = array("requestOrganizationInfoId" => $orgInfoIds, "requestAddressType" => $adressType);
        }
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequestAddress')
          ->columns(array("id", "postcode", "address", "organizationLocationId"))
          ->where($where);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }

    public function getEduProgramByOrgId($orgInfoIds, $typeOfActivity)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequestEducationProgram')
          ->columns(array("id", "educationProgramCatalogItemId", "typeOfActivity", "educationLevelId"), false)
          ->where(array("requestOrganizationInfoId" => $orgInfoIds, "typeOfActivity" => $typeOfActivity));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $ids = $results->toArray();
        return $ids;
    }

    public function addLicenseSupplementMaket($newLicId = NULL, $adressType = NULL)
    {
        if ($newLicId == NULL) {
            $org = $this->getMainLicenseRequest()->getEducationalOrganization();
            $newLicId = $org->getLicense()->getField("Id");
        }
        $allOrgIds = $this->getAllOrgInfoIds();
        $eiisLicenseSupplement = new \Model\Entities\eiisLicenseSupplements();
        $eiisEduOrg = new \Model\Entities\eiisEducationalOrganizations();
        $eiisControlOrg = new \Model\Entities\eiisControlOrgans();
        $curControlOrg = $eiisControlOrg->getById("BFB891F1F234F61C11835CDEAF8E9CA0");
        foreach ($allOrgIds as $oneOrgs) {
            $registryChange = new \Model\LodEntities\RegistryChange();
            $eiisLicenseSupplement = new \Model\Entities\eiisLicenseSupplements();
            $currentOrg = $eiisEduOrg->getById($oneOrgs);
            $maxNum = $eiisLicenseSupplement->getMaxNumber($oneOrgs, $newLicId);
            $dataArraySupplement = array(
                "lod_signStateCode"             => "FORM",
                "lod_stateCode"                 => "DRAFT",
                "fk_eiisControlOrgan"           => "BFB891F1F234F61C11835CDEAF8E9CA0",
                "fk_eiisBranch"                 => $oneOrgs,
                "OrgName"                       => $curControlOrg->getField("Name"),
                "EOName"                        => $currentOrg->getField("FullName"),
                "EOShortName"                   => $currentOrg->getField("ShortName"),
                "Number"                        => $maxNum,
                "fk_eiisLicense"                => $newLicId,
                "fk_eiisLicenseSupplementState" => "4E09AEA87399AFFD9EC3FE6833DB4B36",
                "SerDoc"                        => "90П01"
            );
            $eiisLicenseSupplement->exchangeArray($dataArraySupplement);
            $newIdSuppl = $eiisLicenseSupplement->save(1);
            $dataArrayRegistryChange = array(
                "dealId"             => $this->fields["id"],
                "registryChangeType" => "addToRegistry",
                "licenseAttachId"    => $newIdSuppl
            );
            $registryChange->exchangeArray($dataArrayRegistryChange);
            $registryChange->save(1);
            $reqOrgsIds = $this->getReqOrgInfo($oneOrgs);
            $adresses = $this->getAdressByOrgId($reqOrgsIds, $adressType);
            foreach ($adresses as $adress) {
                $OrganizationLocationToLicenseAttach = new \Model\LodEntities\OrganizationLocationtoLicenseAttach();
                $dataArrayLocationToLiense = array(
                    "organizationLocationId" => $adress,
                    "licenseAttachId"        => $newIdSuppl
                );

                $OrganizationLocationToLicenseAttach->exchangeArray($dataArrayLocationToLiense);
                $OrganizationLocationToLicenseAttach->save(1);
            }
            $eduPrograms = $this->getEduProgramByOrgId($reqOrgsIds);
            foreach ($eduPrograms as $eduProgram) {
                $EducationProgram = new \Model\LodEntities\EducationProgram();
                $dataArrayEduProgram = array(
                    "organizationId"      => $oneOrgs,
                    "EduProgramsID"       => $eduProgram["educationProgramCatalogItemId"],
                    "educationLevelId"    => $eduProgram["educationLevelId"],
                    "LicenseSupplementID" => $newIdSuppl
                );
                $EducationProgram->exchangeArray($dataArrayEduProgram);
                $EducationProgram->save(1);
            }
        }
        return true;
    }

    public function addLicenseMaket()
    {
        $eiisLicense = new \Model\Entities\eiisLicenses();
        $eiisEduOrg = new \Model\Entities\eiisEducationalOrganizations();
        $eiisControlOrg = new \Model\Entities\eiisControlOrgans();
        $registryChange = new \Model\LodEntities\RegistryChange();
        $currentOrg = $eiisEduOrg->getById($this->fields["organizationId"]);
        $curControlOrg = $eiisControlOrg->getById("BFB891F1F234F61C11835CDEAF8E9CA0");
        $maxNum = $eiisLicense->getMaxNumber();
        $maxNum = (int) $maxNum[0];
        $maxNum++;
        $dataArrayLicense = array(
            "lod_signStateCode"              => "FORM",
            "lod_stateCode"                  => "DRAFT",
            "fk_eiisControlOrgan"            => "BFB891F1F234F61C11835CDEAF8E9CA0",
            "fk_eiisEducationalOrganization" => $this->fields["organizationId"],
            "OrgName"                        => $curControlOrg->getField("Name"),
            "EOName"                         => $currentOrg->getField("FullName"),
            "EOShortName"                    => $currentOrg->getField("ShortName"),
            "EOInn"                          => $currentOrg->getField("Inn"),
            "EOGosRegNum"                    => $currentOrg->getField("GosRegNum"),
            "LicenseRegNum"                  => $maxNum,
            "fk_eiisLicenseState"            => "0727E76E83574B16733A25CA627FE6A0",
            "SerDoc"                         => "90Л01"
        );
        $eiisLicense->exchangeArray($dataArrayLicense);
        $newIdLic = $eiisLicense->save(1);

        $dataArrayRequest = array(
            "registryChangeType" => "addToRegistry",
            "dealId"             => $this->fields["id"],
            "licenseId"          => $newIdLic
        );
        $registryChange->exchangeArray($dataArrayRequest);
        $registryChange->save(1);
        return $newIdLic;
    }

    public function addOldLicenseToReestr()
    {
        $licensesId = $this->getLicenseRequestIds();
        $licenseId = NULL;
        if (count($licensesId) > 0) {
            $licenseId = $licensesId[0];
        }
        $registryChange = new \Model\LodEntities\RegistryChange();
        $dataArrayRegistryChange = array(
            "dealId"             => $this->fields["id"],
            "registryChangeType" => "exclusionFromRegistry",
            "licenseId"          => $licenseId
        );
        $registryChange->exchangeArray($dataArrayRegistryChange);
        $registryChange->save(1);
        return true;
    }

    public function addOldLicenseSupplementToReestr()
    {
        $allSupplIds = $this->getUniqueSupplementIds();
        foreach ($allSupplIds as $oneSuppl) {
            $registryChange = new \Model\LodEntities\RegistryChange();
            $dataArrayRegistryChange = array(
                "dealId"             => $this->fields["id"],
                "registryChangeType" => "exclusionFromRegistry",
                "licenseAttachId"    => $oneSuppl
            );
            $registryChange->exchangeArray($dataArrayRegistryChange);
            $registryChange->save(1);
        }
        return true;
    }

    /**
     * Проверяем, содержится ли в истории дела в поле newStatus значение
     * @param string $newStatus
     * @return boolean
     */
    public function getHistoryWithStatus($newStatus)
    {
        $table = new \Model\LodEntities\DealStatusHistory();
        $result = $table->getByWhere([
            'IdDeal'    => $this->getId(),
            'newStatus' => $newStatus
        ]);
        return $result;
    }

    /**
     * Адреса мест ОД
     * @param string $requestAddressType один из вариантов [OPENING | CLOSING | ACTIVE]
     * @param string $itemFrames один из вариантов [places-od | open-od | stop-od]
     * @param string $licenseRequestId
     * @param string $organizationId
     * @return \Zend\Db\Adapter\Driver\StatementInterface|\Zend\Db\ResultSet\ResultSet
     */
    public function getAddresses($requestAddressType, $itemFrames, $licenseRequestId, $organizationId)
    {
        /* @var $dbAdapter \Zend\Db\Adapter\Adapter */
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = (new \Zend\Db\Sql\Select())
          ->from(['a' => 'LicenseRequest'])
          ->columns([])
          ->join(['b' => 'LicenseReasonToLicenseRequest'], 'b.licenseRequestId = a.id', [])
          ->join(['c' => 'LicenseReasonCatalogItem'],
                 new \Zend\Db\Sql\Expression("c.id = b.licenseReasonId and c.frames like '%$itemFrames%'"), [])
          ->join(['d' => 'RequestOrganizationInfo'], 'd.requestId = a.id', [])
          ->join(['e' => 'LicenseRequestAddress'],
                 new \Zend\Db\Sql\Expression(
            "e.requestOrganizationInfoId = d.id and e.requestAddressType = '$requestAddressType'"
            )
          )
          ->where(['a.id' => $licenseRequestId, 'd.orgId' => $organizationId,])
          ->quantifier(\Zend\Db\Sql\Select::QUANTIFIER_DISTINCT);
        $selectStatement = $sql->getSqlStringForSqlObject($select);
        return $dbAdapter->query($selectStatement, $dbAdapter::QUERY_MODE_EXECUTE);
    }

    /**
     * Получаем программы
     * @param string $typeOfActivity
     * @param string $frames
     * @param string $licenseRequestId
     * @param string $organizationId
     * @return \Zend\Db\Adapter\Driver\StatementInterface|\Zend\Db\ResultSet\ResultSet
     */
    public function getPrograms($typeOfActivity, $frames, $licenseRequestId, $organizationId)
    {
        /* @var $dbAdapter \Zend\Db\Adapter\Adapter */
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = (new \Zend\Db\Sql\Select())
          ->from(['a' => 'LicenseRequest'])
          ->columns(['requestId' => 'id'])
          ->join(['b' => 'LicenseReasonToLicenseRequest'], 'b.licenseRequestId = a.id', [])
          ->join(['c' => 'LicenseReasonCatalogItem'],
                 new \Zend\Db\Sql\Expression("c.id = b.licenseReasonId and c.frames like '%$frames%'"), [])
          ->join(['d' => 'RequestOrganizationInfo'], 'd.requestId = a.id', [])
          ->join(['e' => 'LicenseRequestEducationProgram'],
                 new \Zend\Db\Sql\Expression(
            'e.requestOrganizationInfoId = d.id and e.typeOfActivity = \'' . $typeOfActivity . '\''
            )
          )
          ->where(['a.id' => $licenseRequestId, 'd.orgId' => $organizationId,])
          ->quantifier(\Zend\Db\Sql\Select::QUANTIFIER_DISTINCT);
        $selectStatement = $sql->getSqlStringForSqlObject($select);
        return $dbAdapter->query($selectStatement, $dbAdapter::QUERY_MODE_EXECUTE);
    }

    /**
     * Проверяем, нужно ли подсвечивать программу
     * @param string $requestId
     * @param string $educationProgramCatalogItemId
     * @return boolean
     */
    protected function isHiglightCrossed($requestId, $educationProgramCatalogItemId)
    {
        $licenseSupplementToLicenseRequestTable = new \Model\LodEntities\LicenseSupplementToLicenseRequest();
        $educationProgramAdapterTable = new \Model\LodEntities\EducationProgramAdapter();
        $educationProgramTable = new \Model\LodEntities\EducationProgram();

        /* @var $licenseSupplementToLicenseRequest \Model\LodEntities\LicenseSupplementToLicenseRequest */
        $licenseSupplementToLicenseRequests = $licenseSupplementToLicenseRequestTable->getAllByWhere([
            'licenseRequestId' => $requestId,
        ]);

        $licenseAttachesId = [];
        foreach ($licenseSupplementToLicenseRequests as $licenseSupplementToLicenseRequest) {
            $licenseAttachesId[] = $licenseSupplementToLicenseRequest->getField('licenseAttachId');
        }

        $educationProgramAdapters = $educationProgramAdapterTable->getAllByWhere([
            'actualEducationProgramCIId' => $educationProgramCatalogItemId,
        ]);

        $nonActualEducationProgramCIId = [];
        foreach ($educationProgramAdapters as $educationProgramAdapter) {
            $nonActualEducationProgramCIId = array_merge(
              $nonActualEducationProgramCIId, $educationProgramAdapter->getNonActualProgramsId()
            );
        }

        if (empty($nonActualEducationProgramCIId) || empty($licenseAttachesId)) {
            return false;
        }

        /* @var $resultSet \Zend\Db\ResultSet\ResultSet */
        $resultSet = $educationProgramTable->getAllByWhere(
          (new \Zend\Db\Sql\Where())
            ->in('LicenseSupplementID', $licenseAttachesId)
            ->AND
            ->in('EduProgramsID', $nonActualEducationProgramCIId)
        );
        return $resultSet->count() > 0;
    }

    /**
     * Получаем массив программ
     * @param string $typeOfActivity
     * @param string $frames
     * @param string $licenseRequestId
     * @param string $organizationId
     * @return array ['number','code','name','level','type','qualifications']
     */
    public function getPreparedPrograms($typeOfActivity, $frames, $licenseRequestId, $organizationId)
    {
        $programs = $this->getPrograms($typeOfActivity, $frames, $licenseRequestId, $organizationId);
        $preparedPrograms = [];
        foreach ($programs as $program) {
            $id = $program['educationProgramCatalogItemId'];
            $eduLevelId = $program['educationLevelId'];
            $licenseRequestEducationProgramId = $program['id'];
            $licenseRequestId = $program['requestId'];

            $eiisEduProgram = new \Model\Entities\eiisEduPrograms($id);
            $eiisEduLevel = new \Model\Entities\eiisEduLevels($eduLevelId);


            /* @var $eiisEduProgramType \Model\Entities\eiisEduProgramTypes */
            $eiisEduProgramType = $eiisEduProgram->getEduProgramType();
            $programmTypeName = empty($eiisEduProgramType) ? '' : $eiisEduProgramType->getPrintName();
            $qualifications = \Model\LodEntities\LicenseRequestEducationProgramToQualification::getJoinedQualificationNames($licenseRequestEducationProgramId);
            /**
             * Если true, то выводим в таблицу, иначе выводим как подзаголовок
             */
            $isProgram = $eiisEduLevel->getField('parentlevel') === 'fc85e0a4c4b248a1acf6499c298ea58a';

            $isCrossed = false;
            if ($typeOfActivity === 'EDITING') {
                $isCrossed = $this->isHiglightCrossed($licenseRequestId, $id);
            }

            $preparedPrograms[] = [
                'id'             => $id,
                'code'           => $eiisEduProgram->getField('Code'),
                'name'           => $eiisEduProgram->getField('Name'),
                'level'          => $eiisEduLevel->getForDocumentTitle(),
                'type'           => $programmTypeName,
                'qualifications' => $qualifications,
                'isProgram'      => $isProgram,
                'isCrossed'      => $isCrossed,
            ];
        }
        return $preparedPrograms;
    }

    /**
     * Выполняем произвольный SQL запрос
     * @param string $sql
     */
    public static function executeSql($sql)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $dbResults = $dbAdapter->query($sql, $dbAdapter::QUERY_MODE_EXECUTE);
        return $dbResults->toArray();
    }

    /**
     * Получаем дела для вывода в карточном варианте
     * @param string $userId
     * @return array
     */
    public static function getAllForUser($userId)
    {
        $objectsCountQuery = 'SELECT count(DISTINCT ri.orgId) as `count` FROM RequestOrganizationInfo ri '
          . 'INNER JOIN LicenseRequest lr ON ri.requestId = lr.id AND lr.dealId = \'%s\'';

        $customCountQuery = 'SELECT count(*) as `count` FROM LicenseRequestEducationProgram lre '
          . 'INNER JOIN RequestOrganizationInfo ri ON lre.requestOrganizationInfoId = ri.id '
          . 'INNER JOIN LicenseRequest lr on ri.requestId = lr.id AND lr.dealId = \'%s\' '
          . 'WHERE lre.typeOfActivity = \'%s\';';

        $labelsQuery = 'SELECT DISTINCT lrc.designation as `title` FROM LicenseRequest lr '
          . 'INNER JOIN LicenseReasonToLicenseRequest lrlr ON lr.id = lrlr.licenseRequestId '
          . 'INNER JOIN LicenseReasonCatalogItem lrc ON lrc.id = lrlr.licenseReasonId '
          . 'WHERE lr.dealId =  \'%s\'';

        $dealTable = new \Model\LodEntities\Deal();
        $requestOrganizationInfoTable = new \Model\LodEntities\RequestOrganizationInfo();
        $catalogDealTable = new \Model\LodEntities\catalogdeal();
        $licenseRequestTable = new \Model\LodEntities\LicenseRequest();
        $licenseProcedureTable = new \Model\LodEntities\LicensingProcedure();

        $catalogDeals = $catalogDealTable->getAll();
        $catalogDealsMask = [];
        foreach ($catalogDeals as $catalogDeal) {
            $catalogDealsMask[$catalogDeal->getField('origin_name')] = $catalogDeal->getField('russian_name');
        }

        $dealsWhere = new \Zend\Db\Sql\Where();
        $dealsOrder = 'registrationDate asc';

        if ((int) $userId > 0) {
            $dealsWhere->equalTo('expertId', $userId);
        } else {
            $dealsWhere->isNull('expertId');
        }

        $deals = $dealTable->getAllByWhereWithOrder($dealsWhere, $dealsOrder);

        $filtratedDeals = [
            'REGISTRATION|REGISTERED'                               => [],
            'CHECK_LICENSE_REQUEST'                                 => [],
            'REVIEW'                                                => [],
            'PREPARATION_ACT'                                       => [],
            'PRINT|ISSUE'                                           => [],
            'RETURN|WAITING_REPLY|NOTIFICATION_SEND|RECEIVE_ANSWER' => [],
        ];
        /* @var $deal Deal */
        foreach ($deals as $deal) {
            if (empty($deal)) {
                continue;
            }

            $licenseRequest = $licenseRequestTable->getByWhere([
                'id' => $deal->getField('mainLicenseRequestId'),
            ]);

            $licenseProcedure = $licenseProcedureTable->getByWhere([
                'id' => $licenseRequest->getField('licensingProcedureId'),
            ]);

            $reqOrgInfo = $requestOrganizationInfoTable->getByWhere([
                'requestId' => $deal->getField('mainLicenseRequestId'),
                'orgId'     => $deal->getField('organizationId'),
            ]);

            if (empty($reqOrgInfo)) {
                continue;
            }



            $title = $reqOrgInfo->getField('shortTitle');

            if (empty($title)) {
                $title = $reqOrgInfo->getField('fullTitle');
            }

            $id = $deal->getField('id');
            $organizationId = $deal->getField('organizationId');
            $stateCode = $deal->getField('stateCode');
            $stateCodeRu = $catalogDealsMask[$stateCode];
            $registrationDate = new \DateTime($deal->getField('registrationDate'));

            $objectsCount = self::executeSql(sprintf($objectsCountQuery, $id))[0]['count'];
            $openingCount = self::executeSql(sprintf($customCountQuery, $id, 'OPENING'))[0]['count'];
            $editingCount = self::executeSql(sprintf($customCountQuery, $id, 'EDITING'))[0]['count'];
            $closingCount = self::executeSql(sprintf($customCountQuery, $id, 'CLOSING'))[0]['count'];

            $licensingProcedureTitle = $licenseProcedure->getField('title');

            $labels = self::executeSql(sprintf($labelsQuery, $id));
            $preparedLabels = [];
            foreach ($labels as $label) {
                $preparedLabels[] = $label['title'];
            }

            $preparedDeal = [
                'id'                      => $id,
                'organizationId'          => $organizationId,
                'number'                  => $deal->getField('number'),
                'date'                    => $registrationDate->format('d.m.Y'),
                'title'                   => $title,
                'state'                   => $stateCodeRu,
                'objectsCount'            => $objectsCount,
                'openingCount'            => $openingCount,
                'editingCount'            => $editingCount,
                'closingCount'            => $closingCount,
                'licensingProcedureTitle' => $licensingProcedureTitle,
                'labels'                  => $preparedLabels,
                'envelope'                => $deal->getHistoryWithStatus('RECEIVE_ANSWER'),
            ];
            switch ($stateCode) {
                case 'REGISTERED':
                case 'REGISTRATION':
                    $filtratedDeals['REGISTRATION|REGISTERED'][] = $preparedDeal;
                    break;
                case 'CHECK_LICENSE_REQUEST':
                    $filtratedDeals['CHECK_LICENSE_REQUEST'][] = $preparedDeal;
                    break;
                case 'REVIEW':
                    $filtratedDeals['REVIEW'][] = $preparedDeal;
                    break;
                case 'PREPARATION_ACT':
                    $filtratedDeals['PREPARATION_ACT'][] = $preparedDeal;
                    break;
                case 'PRINT':
                case 'ISSUE':
                    $filtratedDeals['PRINT|ISSUE'][] = $preparedDeal;
                    break;
                case 'RETURN':
                case 'WAITING_REPLY':
                case 'NOTIFICATION_SEND':
                case 'RECEIVE_ANSWER':
                    $filtratedDeals['RETURN|WAITING_REPLY|NOTIFICATION_SEND|RECEIVE_ANSWER'][] = $preparedDeal;
                    break;
            }
        }
        return $filtratedDeals;
    }

    public function hasReasonById($reasonId)
    {
        $reqAll = $this->getRequestAll();
        if (!empty($reqAll)) {
            foreach ($reqAll as $reqOne) {
                if ($reqOne->hasReasonById($reasonId)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *  кеш всех причин из дела
     * @var type
     */
    private $_reasonsAll = 0;

    /**
     *
     * @return type
     */
    public function getLicenseReasonAll()
    {
        if ($this->_reasonsAll === 0) {
            $result = array();
            $req = $this->getAllRequests();
            if (!empty($req)) {
                foreach ($req as $value) {
                    $resArr = $value->getLicenseReasonsParseId();
                    $this->addLicenseReasonAll($resArr);
                }
            }
        }
        return $this->_reasonsAll;
    }

    /**
     * установить причины в кеш
     * @param type $reasonAll
     */
    public function setLicenseReasonAll($reasonAll)
    {
        $this->_reasonsAll = $reasonAll;
    }

    /**
     * добавить в кеш причину
     * @param \Model\LodEntities\LicenseReasonCatalogItem $reasonOne - или причина или ммассив причин
     */
    public function addLicenseReasonAll($reasonOne)
    {
        if ($reasonOne instanceof \Model\LodEntities\LicenseReasonCatalogItem) {
            if ($this->_reasonsAll === 0) {
                $this->_reasonsAll = array();
            }
            $this->_reasonsAll[$reasonOne->getId()] = $reasonOne;
        } elseif (is_array($reasonOne)) {
            if ($this->_reasonsAll === 0) {
                $this->_reasonsAll = array();
            }
            $this->_reasonsAll = array_merge($this->_reasonsAll, $reasonOne);
        }
    }

    public function getLicenseReasonAllParseByField($fieldName = 'DocumentPrint')
    {
        $resault = array();
        $resonAll = $this->getLicenseReasonAll();
        if (!empty($fieldName) && !empty($resonAll)) {
            foreach ($resonAll as $resonOne) {
                $resault[$resonOne->getId()] = $resonOne->getFieldSafe($fieldName);
            }
            $resault = array_filter($resault);
        }
        return $resault;
    }

    /**
     *
     * сортировка причин - выдвинуть на изменение адреов в начало,
     *  а на реорганизацию еще важнее
     * <br>
     * Для индексирвоания применяется массиы индексаы $reasonSortIndex
     *
     * @param type $reasonArray   - массив типа идПричины => чегото там
     * @param type $fieldName
     * @return type
     */
    public function getLicenseReasonAllSortByTop($reasonArray = null, $fieldName = 'DocumentPrint')
    {
        $result = array();
        if ($reasonArray === null) {
            $reasonArray = $this->getLicenseReasonAllParseByField($fieldName);
        }
        $reasonArray = array_filter($reasonArray);
        $reasonArray = array_unique($reasonArray);
        if (is_array($reasonArray)) {
            uksort(//по типам
              $reasonArray,
              function($a, $b) {
                //если просто в списке то в низ
                if (is_numeric($a)) {
                    $a += 10000;
                }
                if (is_numeric($b)) {
                    $b += 10000;
                }
                //проставим индексы, если такие есть
                if (!empty(self::$reasonSortIndex[$a])) {
                    $a = self::$reasonSortIndex[$a];
                }
                if (!empty(self::$reasonSortIndex[$b])) {
                    $b = self::$reasonSortIndex[$b];
                }
                //если индекса нет, то вниз
                if (!is_numeric($a)) {
                    $a = 9999;
                }
                if (!is_numeric($b)) {
                    $b = 9999;
                }
                return $a - $b;
            }
            );
        }
        return $reasonArray;
    }

    /**
     * хитрое соеденение причин
     * <br>
     * если одна - то ее одну
     * если две  - то через и
     * если больше то первые через и, остальыне через ', '
     * если с "изменением" несколько то "изменением" оставляем только первое
     *
     * @param type $reasonArray
     * @return type
     */
    public function resonImplode($reasonArray = null, $fieldName = 'DocumentPrint')
    {
        mb_internal_encoding('UTF-8');
        $reasonStr = '';
        if ($reasonArray === null) {
            $reasonArray = $this->getLicenseReasonAllSortByTop($reasonArray, $fieldName);
        }
        if (!empty($reasonArray)) {
            if (count($reasonArray) == 1) {
                $reasonStr = reset($reasonArray);
            } else {
                //если несколько "изменением", то второе не надо
                $isAlredyIzmeneniem = false;
                $izmStr = "изменением";
                $izmCount = mb_strlen($izmStr);
                foreach ($reasonArray as $key => $value) {
                    if (mb_stripos($value, $izmStr) === 0) {
                        if ($isAlredyIzmeneniem) {
                            $reasonArray[$key] = mb_substr($value, $izmCount + 1);
                        }
                        $isAlredyIzmeneniem = true;
                    }
                }
                if (count($reasonArray) == 2) {
                    $reasonStr = implode(' и ', $reasonArray);
                } else {
                    $first = array_shift($reasonArray);
                    $reasonStr = $first . ' и ' . implode(', ', $reasonArray);
                }
            }
        }
        return $reasonStr;
    }

    /**
     * кеш организаций
     * @var type
     */
    private $_orgInfoAll = 0;

    /**
     * выдергиваем все учебные учереждение в деле (всех заявлениях)
     *
     * @param type $inMain  истина  - только глаыные
     *                      лож     - только филиалы
     *                      нул     - все
     * @return type
     */
    public function getRequestOrganizationInfoAll($inMain = null)
    {
        if ($this->_orgInfoAll === 0) {
            $this->_orgInfoAll = array();
            $req = $this->getRequestAll();
            if (!empty($req)) {
                $reqIdArray = array_keys($req);
            }
            $reqIdArray = $this->getAllRequestIds();
            if (!empty($reqIdArray)) {
                $reqIdArray = array_values($reqIdArray);
                // $deal = $this->getDeal();
                //$mainOrgId = $deal->getFieldSafe('organizationId');
                $orgInfo = new \Model\LodEntities\RequestOrganizationInfo();
                //$where[]            = new \Zend\Db\Sql\Predicate\NotLike('orgId',  $mainOrgId);
                $where['requestId'] = $reqIdArray;
                $this->_orgInfoAll = $orgInfo->getAllByWhereParseId($where);
            }
            //прокешируем в организациях от кокого оно зявлениях, чтоб больше не дергать
            if (!empty($this->_orgInfoAll)) {
                foreach ($this->_orgInfoAll as $orgInfoOne) {
                    $orgInfoOne->setLicenseRequest($req[$orgInfoOne->getFieldSafe('requestId')]);
                }
            }
        }
        $result = array();
        if ($inMain === true || $inMain === false) {
            //фильтр на главный/филиал
            foreach ($this->_orgInfoAll as $key => $value) {
                if ($inMain === true && !$value->isMain()) {
                    continue;
                }
                if ($inMain === false && $value->isMain()) {
                    continue;
                }
                $result[$value->getId()] = $value;
            }
        } else {
            //мутка, чтоб главный в начало ушел
            $result = array_merge($this->getRequestOrganizationInfoAll(true),
                                                                       $this->getRequestOrganizationInfoAll(false));
        }
        return $result;
    }

    /**
     * Получение id заявлений из дела с определенной причиной
     * @param mixed (str/array) $idReason
     * @return array
     */
    public function getRequestIdsByReason($idReason)
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequest')
          ->columns(array("idReq" => "id"))
          ->where(array("dealId" => $this->fields["id"], "LicenseReasonToLicenseRequest.licenseReasonId" => $idReason))
          ->join("LicenseReasonToLicenseRequest", "LicenseRequest.id = LicenseReasonToLicenseRequest.licenseRequestId",
                 array("id2" => "id"));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), "idReq");
    }

    /**
     * Проверяет есть ли среди документов дела, документ с типом 220e621a119846389aa7bb37709d381a, и заполненой receiveAnswerDate
     * @return boolean
     */
    public function checkNoticeReceiveAnswerDate()
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $where = new \Zend\Db\Sql\Where();
        $where->equalTo("dealId", $this->fields["id"]);
        $where->equalTo("documentTypeId", "220e621a119846389aa7bb37709d381a");
        $where->isNotNull("receiveAnswerDate");
        $select->from('Document')
          ->columns(array("count" => new \Zend\Db\Sql\Expression('COUNT( Document.Id)')))
          ->where($where);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $results = array_column($results->toArray(), "count");
        if (isset($results[0]) && !empty($results[0])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Обновляем отправленные данные
     */
    public static function updateExportedRows()
    {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $update = $sql->update();
        $update->table('Deal')
          ->set(['was_updated' => 0])
          ->where(['was_updated' => 1]);
        $updateString = $sql->getSqlStringForSqlObject($update);
        $dbAdapter->query($updateString, $dbAdapter::QUERY_MODE_EXECUTE);
    }

}