<?php
namespace Model\LodEntities;

use Model\Gateway;

class isgaExchangeTable extends Gateway\LodBaseEntity{
    protected $tableName = "isga_ExchangeTable";
    protected $idName = "Id";
}