<?php
namespace Model\LodEntities;

use Model\Gateway;

class DocumentStatusButton extends Gateway\LodBaseEntity{
    protected $tableName = "DocumentStatusButton";
    protected $idName = "Id";
}