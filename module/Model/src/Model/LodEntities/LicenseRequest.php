<?php
namespace Model\LodEntities;

use Model\Entities\eiisLicenseSupplements;
use Model\Gateway;
use Zend\Db\Sql\Predicate\Predicate;

class LicenseRequest extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseRequest";
    protected $idName = "id";

    protected $_eo = null;
    public function getEducationalOrganization(){
        if($this->_eo != null)
            return $this->_eo;

        if($this->isEmptyField("organizationId")){
            $this->_eo;
        }

        //Если это наш айди и это новая организация, то не надо преобразовывать айди
        $orgId = $this->fields["organizationId"];

        //Если это не наш айди то извращаемся над ним
        if(!$this->isOurId("organizationId")){
            $orgId = str_replace('-','',strtoupper( $this->fields["organizationId"]));
        }
        $EO = new \Model\Entities\eiisEducationalOrganizations($orgId);
        if($EO != null){
            $this->_eo = $EO;
            return $this->_eo;
        } else {
            return null;
        }
    }

    public function getLicensingProcedureName(){
        if($this->isEmptyField("licensingProcedureId"))
            return "";

        $licensingProcedure = new \Model\LodEntities\LicensingProcedure($this->getField("licensingProcedureId"));

        return $licensingProcedure->getFieldHtml("title");
    }

    public function getLicensingProcedurePrintName(){
        if($this->isEmptyField("licensingProcedureId"))
            return "";
        $licensingProcedure = new \Model\LodEntities\LicensingProcedure($this->getField("licensingProcedureId"));
        return $licensingProcedure->getField("PrintDoc");
    }

    public function getlicenseReasonName(){
        if($this->isEmptyField("licenseReasonId"))
            return "";

        $licensingProcedure = new \Model\LodEntities\LicensingProcedure($this->getField("licensingProcedureId"));

        return $licensingProcedure->getField("fullTitle");
    }

    public function getLicenseReasonsParseId($isCache = true){
        $reson = $this->getLicenseReasons($isCache);
        $result = array();
        if(!empty($reson)){
            foreach ( $reson as $resonOne ) {
               $result[$resonOne->getId()] = $resonOne;
            }
        }
        return $result;
    }

    private $_licenseReasons = 0;
    /**
     * дергаем причины из заявления
     *
     * @param type $isCache - закешировать запрос
     * @return type
     */
    public function getLicenseReasons($isCache = true){
        if(!$isCache || $this->_licenseReasons === 0){
            //Получаем список причин
            $LicenseReasonsTable = new \Model\Gateway\LodEntitiesTable("LicenseReasonCatalogItem");

            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $subselectMtM = $sql->select();
            $subselectMtM->from("LicenseReasonToLicenseRequest")
                ->columns(array("licenseReasonId"))
                ->where(array(
                    "licenseRequestId" => $this->getId()
                ));

            $reasons =  $LicenseReasonsTable->getAllEntities(null, null, array(
                new \Zend\Db\Sql\Predicate\In("id", $subselectMtM)
            ));
            $this->_licenseReasons = $reasons->getObjectsArray();
        }
        return $this->_licenseReasons;
    }

    public function getLicenseReasonsList($onlyArray=false){
        $reasons = $this->getLicenseReasons();

        $titles = [];
        foreach($reasons as $reason){
            $titles[] = $reason->getFieldHtml("title");
        }
        if($onlyArray){
            return $titles;
        }else{
            return implode(", ",$titles);
        }
    }

    public function getLicenseReasonsIds(){
        $reasons = $this->getLicenseReasons();
        $ids = [];
        foreach($reasons as $reason){
            $ids[] = $reason->getField("id");
        }
        return $ids;
    }

    public function getRequestSigner(){
        $fio = [];
        if(!$this->isEmptyField("requestSignerLastName")) $fio[] = $this->getField("requestSignerLastName");
        if(!$this->isEmptyField("requestSignerFirstName")) $fio[] = $this->getField("requestSignerFirstName");
        if(!$this->isEmptyField("requestSignerMiddleName")) $fio[] = $this->getField("requestSignerMiddleName");
        return implode(" ", $fio);
    }

    /**
     * Возвращает Тип поступления из словаря
     * @return Gateway\type|string
     */
    public function getAdmissionTypeName(){
        if($this->isEmptyField("admissionType"))
            return "";

        $admissionType = new \Model\LodEntities\CatalogItem($this->getField("admissionType"));

        return $admissionType->getField("title");
    }

    /**
     * Добавляет записи в таблицу MtM для сохранения связей между заявлениями и причинами
     *
     * @param $reasons - массив айдишников причин
     */
    public function addReasons($reasons){
        if(count($reasons) > 0){
            foreach($reasons as $reasonId){
                $mtmEntity = new LicenseReasonToLicenseRequest();
                $mtmEntity->setFields([
                    "licenseReasonId" => $reasonId,
                    "licenseRequestId" => $this->getId()
                ]);
                $mtmEntity->save();
            }
        }
    }

    /**
     * Удаляем записи из таблицы связи с причинами
     */
    public function deleteReasons(){
        $reasonsLinkTable = new Gateway\LodEntitiesTable("LicenseReasonToLicenseRequest");
        $reasonsLinkTable->delete([
            "licenseRequestId" => $this->getId()
        ]);
    }

    public function getSupplements($onlyIds=false){
        $supplementsTable = new Gateway\EntitiesTable("eiis_LicenseSupplements");

        $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
        $subselectMtM = $sql->select();
        $subselectMtM->from("LicenseSupplementToLicenseRequest")
            ->columns(array("licenseAttachId"))
            ->where(array(
                "licenseRequestId" => $this->getId()
            ));

        $statement = $sql->prepareStatementForSqlObject($subselectMtM);
        $results = $statement->execute();

        $Ids = array();
        foreach($results as $org){
            $Ids[] = $org["licenseAttachId"];
        }
        if($onlyIds){
            return $Ids;
        }
        $supps = [];
        if(count($Ids)>0)
            $supps =  $supplementsTable->getAllEntities(null, null, array(
                new \Zend\Db\Sql\Predicate\In("id", $Ids)
            ))->getObjectsArray();

        return $supps;
    }
    public function getFilials($type=null){
        $where = array(
            "requestId" => $this->fields["id"]
        );

        switch($type){
            case "new":
                //$where["additionalInfo"] = "new";
                break;
            case "edit":
                $predicate = new  \Zend\Db\Sql\Where();
                $predicate
                    ->equalTo("additionalInfo","edit_from")
                    ->or
                    ->equalTo("additionalInfo","edit_to")
                    ->or
                    ->equalTo("additionalInfo","")
                    ->or
                    ->isNull("additionalInfo");
                $where[]=$predicate;
                break;
            case "filials-only":
                $where[] = new \Zend\Db\Sql\Predicate\NotLike("orgId", $this->getField("organizationId"));
                break;
        }

        if($type!="edit"){
            $predicate = new  \Zend\Db\Sql\Where();
            $predicate->notLike("additionalInfo","edit_from")
                ->or
                ->isNull("additionalInfo");
            $where[] = $predicate;
        }
            //$where[] = new \Zend\Db\Sql\Predicate\NotLike("additionalInfo","edit_from");
            //$where[]

        $filials = $this->getLinkedItems("RequestOrganizationInfo",$where);

        return $filials;
    }
    public function getRealLicense(){
        $licenseId = $this->getField("licenseId");
        $license   = new \Model\Entities\eiisLicenses($licenseId);
        return $license;
    }

    public function addDeal($dealId){
        $this->setField("dealId",$dealId);
        $result = $this->save();
        return $result;
    }

    public function getPayments(){
        $paymentsTable = new Gateway\LodEntitiesTable("LicenseRequestPayment");
        $payments = $paymentsTable->getAllEntities(null, "Date DESC",[
            "LicenseRequestId" => $this->getId()
        ])->getObjectsArray();
        return $payments;
    }

    /**
     * Метод подчищает из бд все связанные объекты (филиалы/оп)
     * дергается при изменении организации при редактировании запроса
     */
    public function clearAllFramesData(){
        //Собираем id филиалов
        $filials = $this->getFilials();
        $filialsId = [];
        foreach($filials as $filial){
            $filialsId[]=$filial->getId();
        }
        //Удаляем все филиалы
        $filialsTable = new Gateway\LodEntitiesTable("RequestOrganizationInfo");
        $filialsTable->delete([
            "requestId" => $this->getId()
        ]);
        //Удаляем все оп
        $opTable = new Gateway\LodEntitiesTable("LicenseRequestEducationProgram");
        $opTable->delete([
            new \Zend\Db\Sql\Predicate\In("requestOrganizationInfoId",$filialsId)
        ]);
    }
    public function getAllOrgInfoIds(){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('RequestOrganizationInfo')
            ->columns(array("orgId"), false)
            ->where(array("requestId"=>$this->fields["id"]))
            ->group("orgId");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($results->toArray(), "orgId");
    }
    public function getReqOrgInfo($orgId=null){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();


        if($orgId == null && !$this->isEmptyField("organizationId")){
            $orgId = $this->getField("organizationId");
        }
        if($orgId == null)
            return null;

        $select->from('RequestOrganizationInfo')
            ->columns(array("id"), false)
            ->where(array("requestId"=>$this->fields["id"],"orgId"=>$orgId));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $ids = array_column($results->toArray(),"id");
        return $ids;
    }

    public function getReqOrgInfoObject($orgId=null){
        if($orgId == null && !$this->isEmptyField("organizationId")){
            $orgId = $this->getField("organizationId");
        }
        if($orgId == null)
            return null;

        $orgInfoTable = new Gateway\LodEntitiesTable("RequestOrganizationInfo");
        $results = $orgInfoTable->getEntitiesByWhere(array("requestId"=>$this->fields["id"],"orgId"=>$orgId))->getObjectsArray();
        if(count($results) > 0)
            return $results[0];
        else
            return null;
    }

    /**
     * дергаем все филиалы или конкретный
     *
     * @param type $orgId - если надо конкретный
     * @param array $whereAdd - добачим к запросу
     * @return type - ассоц масив классов RequestOrganizationInfo() или пустой
     */
    public function getRequestOrganizationInfoAll($orgId = null, $whereAdd = array(),$parseFieldName = null){
        $where = array();
        $where['requestId'] = $this->getId();
        if(!empty($orgId)){
            $where['orgId'] = $orgId;
        }
        if(!empty($whereAdd)){
            $where = array_merge_recursive($where, $whereAdd);
        }
        $orgInfoAll = new \Model\LodEntities\RequestOrganizationInfo();
        $orgInfoAll = $orgInfoAll->getAllByWhereParseId($where, $parseFieldName);
        if(!empty($orgInfoAll)){
            foreach ( $orgInfoAll as $orgInfoOne ) {
                $orgInfoOne->setLicenseRequest($this);
            }
        }
        return $orgInfoAll;
    }


    /**
     * @return ResultSet
     */
    public function getFullOrganizationsInfo(){
        $orgsTable = new \Model\LodEntities\RequestOrganizationInfo();
        $allOrgs = $orgsTable->getAllByWhere(array("requestId"=>$this->fields["id"]));
        return $allOrgs;
    }

    /**
     * @return ResultSet
     */
    public function getOrganizationsInfoWhithoutMain(){
        $orgsTable = new \Model\LodEntities\RequestOrganizationInfo();
        $allOrgs = $orgsTable->getAllByWhere(array("requestId"=>$this->fields["id"],"orgId !='".$this->fields["organizationId"]."'"));
        return $allOrgs;
    }

    /**
     * @return ResultSet
     */
    public function getMainOrganizationInfo(){
        $orgsTable = new \Model\LodEntities\RequestOrganizationInfo();
        $oneOrg = $orgsTable->getByWhere(array("requestId"=>$this->fields["id"],"orgId"=>$this->fields["organizationId"]));
        return $oneOrg;
    }
    public function getAdressByOrgId($orgInfoIds){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequestAddress')
            ->columns(array("organizationLocationId"), false)
            ->where(array("requestOrganizationInfoId"=>$orgInfoIds));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $ids = array_column($results->toArray(),"organizationLocationId");
        return $ids;
    }
    public function getEduProgramByOrgId($orgInfoIds){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequestEducationProgram')
            ->columns(array("educationProgramCatalogItemId","typeOfActivity","educationProgramCatalogItemId","educationLevelId"), false)
            ->where(array("requestOrganizationInfoId"=>$orgInfoIds));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $ids = $results->toArray();
        return $ids;
    }
    public function getOneReorganizationData(){
        $reorgTable = new Gateway\LodEntitiesTable("ReorganizedOrganizationInfo");
        $reorgs = $reorgTable->getAllEntities(null, null, [
            "licenseRequestId" => $this->getId()
        ])->getObjectsArray();

        if(count($reorgs)>0)
            return $reorgs[0];
        else
            return new ReorganizedOrganizationInfo();
    }

    public function getReorganizationsData(){
        $reorgTable = new Gateway\LodEntitiesTable("ReorganizedOrganizationInfo");
        $reorgs = $reorgTable->getAllEntities(null, null, [
            "licenseRequestId" => $this->getId()
        ])->getObjectsArray();

        return $reorgs;
    }

    /**
     * кеш дела
     * @var type
     */
    private $_deal = 0;

    /**
     * дергаем дело
     *
     * @return type
     */
    public function getDeal() {
        if( 0 === $this->_deal ){
            if($this->isEmptyField('dealId')){
                $this->_deal = null;
            }
            else{
                $this->_deal = new \Model\LodEntities\Deal();
                $this->_deal = $this->_deal->getByWhere(
                        array($this->_deal->getIdName() => $this->getField('dealId'))
                );
            }
        }
        return $this->_deal;
    }

    /**
     * помогалка опредеяемт есть ли в этом запросе причина с таким ид
     *
     * @param type $reasonId    - ид причины
     * @return boolean          - да/нет
     */
    public function hasReasonById($reasonId) {
        $reasonAll = $this->getLicenseReasonsParseId();
        if(empty($reasonAll)){
            return false;
        }
        if(empty( $reasonAll[$reasonId]) ){
            return false;
        }
        return true;
    }

    /**
     * есть  ли запрос
     * Прекращение образовательной деятельности по адресу
     * rs_104
     * @return type истина/лож
     */
    public function hasReasonCloseAdres($inMain = null) {
        return $this->hasReasonById('0ba32b48a6384b5c95dcb630f34c930d');
    }

    /**
     * есть  ли запрос
     * Открытие образовательной деятельности по адресу
     * rs_103

     * @return type истина/лож
     */
    public function hasReasonOpenAdres($inMain = null) {
        return $this->hasReasonById('aac9fc11db3d4682a3e80a64dabc1ede');
    }

    /**
     * есть  ли запрос
     * Открытие филиала 
     * rs_115

     * @return type истина/лож
     */
    public function hasReasonOpenFilial($inMain = null) {
        return $this->hasReasonById( '6d8c38b1ab79486eb8f826943b723f4c' );
    }

    /**
     * есть  ли запрос и филиалы с такими требованиями
     * Прекращение образовательной деятельности по адресу
     * rs_104
     * @param  $inMain      - истина - только у главных
     *                      - лож    - только у филиалов
     *                      - нул    - все подряд
     * @return type
     */
    public function hasCloseAdres($inMain = null) {
        $toReason = $this->hasReasonCloseAdres();
        if(!$toReason){
            return false;
        }
        $orgAll = $this->getRequestOrganizationInfoAll();
        if( empty($orgAll) ){
            return false;
        }
        foreach ( $orgAll as $key => $value ) {
        //провери а есть ли программы на изменение
            $prog = $value->hasCloseAdres($inMain);
            if($prog){
                return true;
            }
        }
        return false;
    }
    /**
     * есть  ли запрос
     *  Прекращение реализации образовательных программ
     * rs_110
     * @return type
     */
    public function hasReasonCloseOp() {
        return $this->hasReasonById('33b935940d1a4c5383ee72accbdf4ee9');
    }

    /**
     * есть  ли запрос и филиалы с такими требованиями
     *  Прекращение реализации образовательных программ
     * rs_110
     * @param  $inMain      - истина - только у главных
     *                      - лож    - только у филиалов
     *                      - нул    - все подряд
     * @return type
     */
    public function hasCloseOp($inMain = null) {
        $toReason = $this->hasReasonCloseOp();
        if(!$toReason){
            return false;
        }
        $orgAll = $this->getRequestOrganizationInfoAll();
        if(empty($orgAll )){
            return false;
        }
        foreach ( $orgAll as $key => $value ) {
        //провери а есть ли программы на изменение
            $prog = $value->hasCloseOp($inMain);
            if($prog){
                return $prog;
            }

        }
        return false;
    }
    /**
     *  есть  ли запрос
     *  Установление бессрочного действия лицензии
     * rs_113
     * @return type
     */
    public function hasReasonSetForeve() {
        return $this->hasReasonById('2b6b9edcdf394ea9b3b67c9ac8a23b7e');
    }
    /**
     *  есть  ли запрос
     *  Изменение наименования лицензиата
     * rs_102
     * @return type
     */
    public function hasReasonChangeName() {
        return $this->hasReasonById('3e5b5f379598446ea2833f4acd2ba01b');
    }

    /**
     *  есть  ли запрос и филиалы с такими требованиями
     *  Изменение наименования лицензиата
     * rs_102
     * @param  $inMain      - истина - только у главных
     *                      - лож    - только у филиалов
     *                      - нул    - все подряд
     * @return type
     */
    public function hasChangeName($inMain = null) {
        $toReason = $this->hasReasonChangeName();
        if(!$toReason){
            return false;
        }
        $orgAll = $this->getRequestOrganizationInfoAll();
        if(empty($orgAll )){
            return false;
        }
        foreach ( $orgAll as $key => $value ) {
        //провери а есть ли программы на изменение
            if($inMain === true && !$value->isMain()){
                continue;
            }
            if($inMain === false && $value->isMain()){
                continue;
            }
            $prog = $value->hasChangeName($inMain);
            if(!empty($prog)){
                 return true;
            }
        }
        return false;
    }
    /**
     * есть  ли запрос
     *  Реорганизация лицензиата в форме преобразования
     * rs_105
     * @return type
     */
    public function hasReasonReorgTransform() {
        return $this->hasReasonById('518dfabe3375431684b71d2faa5c7564');
    }
    /**
     *  реорганизация в форме присоединения - новая лицензия
     * rs_106
     * @return type
     */
    public function hasReasonReorgJoin() {
        return $this->hasReasonById('c83201802b474c4393305b16b7c05375');
    }
    /**
     * есть  ли запрос
     * реорганизация в форме слияния - новая лицензия
     * rs_107
     * @return type
     */
    public function hasReasonReorgMerge() {
        return $this->hasReasonById('f9893ac3e72e43b59101a8270f9eaacf');
    }

    /**
     * есть  ли запрос
     * изменение адреса места нахождения, если головняк меняет - то новая лицензия, если же меняет адрес только филиал,то новое приложение
     * rs_114
     * @return type
     */
    public function hasReasonChangeAdres() {
        return $this->hasReasonById('fb71a4f02e7d4f07be60fcc43fe2e9a2');
    }

    /**
     * есть  ли запрос и филиалы с таким запросом
     * изменение адреса места нахождения, если головняк меняет - то новая лицензия, если же меняет адрес только филиал,то новое приложение
     * rs_114
     * @param  $inMain      - истина - только у главные
     *                      - лож    - только у филиалы
     *                      - нул    - все подряд
     * @return type
     */
    public function hasChangeAdres($inMain = null) {
        $toReason = $this->hasReasonChangeAdres();
        if(!$toReason){
            return false;
        }
        $orgAll = $this->getRequestOrganizationInfoAll();
        if(empty($orgAll )){
            return false;
        }
        foreach ( $orgAll as $key => $value ) {
        //провери а есть ли программы на изменение
            if($inMain === true && !$value->isMain()){
                continue;
            }
            if($inMain === false && $value->isMain()){
                continue;
            }
            $prog = $value->hasChangeAdres($inMain);
            if(!empty($prog)){
                 return true;
            }
        }
        return false;
    }
    /**
     * есть  ли запрос
     * Изменение наименований образовательных программ
     * rs_112
     * @return type
     */
    public function hasReasonChangeOpName() {
        return $this->hasReasonById('fd77ff8629184ae89d18858d2b4d7a50');
    }

    /**
     * есть  ли запрос и филиалы с такими требованиями
     * Изменение наименований образовательных программ
     * rs_112
     * @return type
     */
    public function hasChangeOpName() {
        $toReason = $this->hasReasonChangeOpName();
        if(!$toReason){
            return false;
        }
        $orgAll = $this->getRequestOrganizationInfoAll();
        if(empty($orgAll )){
            return false;
        }
        foreach ( $orgAll as $key => $value ) {
        //провери а есть ли программы на изменение
            $prog = $value->hasChangeOpName();
            if(!empty($prog)){
                 return true;
            }
        }
        return false;
    }
    /**
     * Получение заголовка должности выбранного в заявлении
     * @return string всегда возвращает строку, пустую или с заголовком должности
     */
    public function getEmployeePostTitle(){
        if(!empty($this->fields["requestSignerPosition"])){
            $employeePost = $this->getLinkedItem("employeepost", array("id"=>$this->fields["requestSignerPosition"]));
            if($employeePost){
                return $employeePost->getField("title");
            }else{
                return "";
            }
        }else{
            return "";
        }

    }


    /**
     * дергаем должность подписавшего заявление из этого заявления
     * @return type
     */
    public function getEmploePostTitleFromIt() {
        return $this->getFieldSafe('requestSignerPosition');
    }


    public function getEmploePostFromIt() {
        if($this->isEmptyField('requestSignerPosition')){
            return null;
        }
        $postClass = new \Model\LodEntities\employeepost();
        $postClass = $postClass->getByWhere(array($postClass->getIdName()=>  $this->getFieldSafe('requestSignerPosition') ));
        return $postClass;
    }
    /**
     * есть  ли запрос и филиалы с такими требованиями
     *  Прекращение осуществления образовательной деятельности организацией в целом
     * rs_118
     * @return type
     */
    public function hasReasonCloseOrganisation() {
        return $this->hasReasonById('42807cbc9a9c419ba7ebc374652ed68a');
    }

    /**
     * есть  ли запрос и филиалы с такими требованиями
     *  Прекращение осуществления образовательной деятельности организацией в целом
     * rs_118
     * @return type
     */
    public function hasCloseOrganisation( $inMain = null ) {
        $toReason = $this->hasCloseOrganisation();
        if(!$toReason){
            return false;
        }
        $orgAll = $this->getRequestOrganizationInfoAll();
        if(empty($orgAll )){
            return false;
        }
        foreach ( $orgAll as $key => $value ) {
        //провери а есть ли программы на изменение
            if($inMain === true && !$value->isMain()){
                continue;
            }
            if($inMain === false && $value->isMain()){
                continue;
            }
            $prog = $value->hasChangeAdres();
            if(!empty($prog)){
                 return true;
            }
        }
        return false;
    }


    private $_isSended = null;
    private $_isSendedFull = null;
    /**
     * определяет отправленно ли заявление
     *
     * @param boolean $isCheckWaitingReply - проверять на наличие статуса ждем ответа
     * @return boolean  - true - да -нет
     */
    public function isSended( $isCheckWaitingReply = false ) {
        //определимся с отправкой по дате, т.е. первоначальное отправление
        if( null === $this->_isSended ){
            $deal = $this->getDeal();
            if( empty($deal) ){
                $dateSubmission = $this->getDate('DateSubmission');
                if( empty($dateSubmission) ){
                    $isSubmission = false;
                }
                else{
                    $isSubmission = true;
                }
            }
            else{
                $isSubmission = true;
            }

            if( $isSubmission ){
                $this->_isSended = true;
            }
            else{
                $this->_isSended = false;
            }
        }
        //проверим отправку по статусу или повторна я ли отправка
        if( $isCheckWaitingReply ){
            if( null === $this->_isSendedFull ){
                if( empty($this->_isSended) ){
                    $this->_isSendedFull = false;
                }
                else{
                    $this->_isSendedFull = true;
                    //если нарушения, то не отправлено
                    $deal = $this->getDeal();
                    if( !empty($deal) ){
                        if( $deal->getFieldSafe('stateCode') ==  'WAITING_REPLY' ){
                            $this->_isSendedFull = false;
                        }
                    }
                }
            }
        }
        //определимся с чего вернуть
        if( $isCheckWaitingReply ){
            return $this->_isSendedFull;
        }
        else {
            return $this->_isSended;
        }
    }


    public function getEmployeePost(){
        if( $this->isEmptyField('requestSignerPosition') ){
            return false;
        }
        $result = new employeepost();
        $result = $result->getByWhere(array(
            $result->getIdName() => getFieldSafe('requestSignerPosition')
        ));
        return $result;
    }

    /*
    public function getEmployeePostTitle() {
        $result = '';
        $postClass = $this->getEmployeePost();
        if( !empty($postClass) ){
            $result = $postClass->getFieldSafe('title');
        }
        return $result;
    }
    */


    /**
     * дергаем макет файла из файлохранилища
     * @return type
     */
    public function getMaketFile() {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('RequestMaket', $this->getId());
        return $result;
    }

    /**
     * дергаем  файл скана заявления из файлохранилища
     * @return type
     */
    public function getScanFile() {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('RequestScan', $this->getId());
        return $result;
    }
    
    /**
     * дергаем  файлы сканов заявлений из файлохранилища
     * @return type
     */
    public function getScanFileAll() {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileAllByCodeId('RequestScan', $this->getId());
        return $result;
    }

    /**
     * дергаем  файл подписанного заявления из файлохранилища
     * @return type
     */
    public function getDocumentFile() {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('RequestDocument', $this->getId());
        return $result;
    }

    /**
     * дергаем  файл подписанного заявления из файлохранилища
     * точнее последний
     *
     * @param type $onlyEditable  - отбор редактируемых или не редактируемых
     *                                true -вернуть только редактируемае
     *                                false -вернуть только не редактируемае
     *                                прочее - отдает все
     * @return type
     */
    public function getSigntFile( $onlyEditable = null ) {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        //@$fileBank = new \Model\LodEntities\EiFile();
        //$result = $fileBank->getFileByCodeId('RequestSign', $this->getId());
        $result = false;
        $signFileAll = $this->getSignFileAll( $onlyEditable );
        if( !empty($signFileAll) ){
            $result = end($signFileAll);
        }
        return $result;
    }

    /**
     * Проверяет подпись документа файла подписи
     *
     * @return string   - "Ok" -Все путем, прочее - что не так
     * @throws \Exception
     */
    public function checkSign( $onlyEditable = null ) {
        if( $this->isEmptyField($this->getIdName()) ){
            return 'заявление не выбрано';
        }
        try{
            $result = 'не установленная ошибка';
            $signFile = $this->getSigntFile( $onlyEditable );
            if( empty($signFile) ){
                throw new \Exception('не загружен файл, подписанный эл. подписью');
            }
            $result = $signFile->checkSign();
        }
        catch (\Exception $e) {
            $result = $e->getMessage();
        }
        return $result;
    }


    /**
     * Получими перечень файлов этого заявления по кодам
     *
     * @param type $fileCode
     * <ul>
     * <li>'RequestDocument'  - заявление подписаное электронной подписью</li>
     * <li>'RequestSign'  - электронная подпись документа</li>
     * </ul>
     * @return type
     */
    public function getFileAll( $fileCode, $sortField = null, $sortReverse = true ){
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        if( empty($sortField) ){
            $sortField = 'created';
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileAllByCodeId( $fileCode, $this->getId() );
        //отсортируем
        uasort(
            $result,
            function($a, $b) use ($sortField, $sortReverse) {
                $aS = $a->getFieldSafe($sortField);
                $bS = $b->getFieldSafe($sortField);
                return (($sortReverse)?-1:1) * (strnatcmp($aS, $bS));
            }
        );
        return $result;
    }

    /**
     * дергаем подписаные файлы, проверяем на можно ли их редактить
     * если иесть такое,  то подними ему флажок
     * isEditable
     *
     * @param type $onlyEditable    - отбор редактируемых или не редактируемых
     *                                true -вернуть только редактируемае
     *                                false -вернуть только не редактируемае
     *                                прочее - отдает все
     * @param type $sortReverse     - true - обратный порядок сортировки
     * @return type
     */
    public function getSignFileAll( $onlyEditable = null, $sortReverse = false) {
        //определим со статусом еще не отправлено и
        //на исправление
        //если эти по установим влаг что емо можно редактить
        $signFileAll = $this->getFileAll('RequestSign', null, $sortReverse);

        $this->_signFileHasEditable = false;
        $isWaiting = $this->isSended( true );
        if( !$isWaiting ){
            $submissionDate = $this->getDate('DateSubmission');
            $deal = $this->getDeal();
            if( empty($submissionDate) && empty($deal) ){
                //если редактить и не отправлено, то добавим всем файлам
                foreach(  $signFileAll as $signFileKey => $signFileOne ){
                    $signFileAll[ $signFileKey ]->setField('isEditable',1);
                    $this->_signFileHasEditable = true;
                }
            }
            else{
                //если не новое, значит отредактить
                //посмотреть, когда последний раз сменило статус на ожидание
                // и посмотрим даты создания файлов после нее после него
                $last = $this->getLastStatus('WAITING_REPLY');
                if( !empty($last) ){
                    $lastDate = $last->getFieldSafe('changeDate');
                    foreach(  $signFileAll as $signFileKey => $signFileOne ){
                        $signFileDate = $signFileOne->getFieldSafe('created');
                        $cmp = strcmp($signFileDate, $lastDate );
                        if( $cmp > 0 ){
                            $signFileAll[ $signFileKey ]->setField('isEditable',1);
                            $this->_signFileHasEditable = true;
                        }
                    }
                }
            }
        }
        if( is_bool($onlyEditable) ){
            $signFileAllMew = array();
            foreach ( $signFileAll as $key => $value ) {
                if( $value->isEmptyField('isEditable') ){
                    //не редактируемая
                    if( !$onlyEditable ){
                        $signFileAllMew[ $value->getId() ] = $value;
                    }
                }
                else{
                    //редактируемая
                    if( $onlyEditable ){
                        $signFileAllMew[ $value->getId() ] = $value;
                    }
                }
            }
            $signFileAll = $signFileAllMew;
        }
        return $signFileAll;
    }

    public function getLicense(){
        if($this->isEmptyField("licenseId"))
            return null;

        $license = new \Model\Entities\eiisLicenses($this->getField("licenseId"));

        return $license;
    }

    private $_signFileHasEditable = false;

    public function hasSignFileEditable() {
        return $this->_signFileHasEditable;
    }

    /**
     * дергаем последний статус дела
     *
     * @param type $statusCode
     * @return type
     */
    public function getLastStatus($statusCode = '') {
        $result = false;
        $deal = $this->getDeal();
        if( !empty($deal) ){
            $stateHisoryAll = $deal->getHistoryChange($statusCode);
            $stateHisoryAll = $stateHisoryAll->getObjectsArray();
            if( !empty($stateHisoryAll) ){
                uasort(
                    $stateHisoryAll,
                    function($a, $b){
                        $as = $a->getFieldSafe('changeDate');
                        $bs = $b->getFieldSafe('changeDate');
                        return strcmp($as, $bs);
                    }
                );
                $result = end($stateHisoryAll);
            }
        }
        return $result;
    }

    //почледняя строка
}