<?php
namespace Model\LodEntities;

use Model\Gateway;

class IslodUserRole extends Gateway\LodBaseEntity{
    protected $tableName = "islod_user_role";
    protected $idName = "id";
}