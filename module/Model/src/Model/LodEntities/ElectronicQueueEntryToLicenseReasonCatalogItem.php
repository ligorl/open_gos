<?php

namespace Model\LodEntities;

use Model\Entities\eiisEducationalOrganizations;
use Model\Gateway\LodBaseEntity;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Where;

class ElectronicQueueEntryToLicenseReasonCatalogItem extends LodBaseEntity
{
    protected $tableName = 'ElectronicQueueEntryToLicenseReasonCatalogItem';
    protected $idName = 'id';
}