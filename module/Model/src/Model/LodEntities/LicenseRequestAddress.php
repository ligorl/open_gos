<?php
namespace Model\LodEntities;

use Model\Gateway;

class LicenseRequestAddress extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseRequestAddress";
    protected $idName = "id";
    
    /**
     * дергаем филиал
     * @return type
     */
    public function getRequestOrganizationInfo() {
        if($this->isEmptyField('requestOrganizationInfoId')){
            return null;
        }
        $result = new \Model\LodEntities\RequestOrganizationInfo();
        $result = $result->getByWhere(
            array(
                $result->getIdName() => $this->getFieldSafe('requestOrganizationInfoId')
            )
        );
        return $result;
    }
    
    /**
     * дергаем дело
     * @return type
     */
    public function getDeal(){
        $result = null;
        $filial = $this->getRequestOrganizationInfo();
        if(!empty($filial)){
            $result = $filial->getDeal();
        }        
        return $result;
    }
}