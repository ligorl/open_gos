<?php
namespace Model\LodEntities;

use Model\Gateway;

class DocumentPrintedForms extends Gateway\LodBaseEntity{
    protected $tableName = "DocumentPrintedForms";
    protected $idName = "id";
}