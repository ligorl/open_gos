<?php
namespace Model\LodEntities;

use Model\Gateway;

class ReorganizationType extends Gateway\LodBaseEntity{
    protected $tableName = "ReorganizationType";
    protected $idName = "id";
}