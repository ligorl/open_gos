<?php
namespace Model\LodEntities;

use Model\Gateway;

class EducationProgram extends Gateway\LodBaseEntity{
    protected $tableName = "EducationProgram";
    protected $idName = "id";

    public function getGroupedByEduLevels( $where = null){
        $gateway = $this->getGateway();
        $result = $gateway->select(function(\Zend\Db\Sql\Select $select)use($where){
                        $select
                            ->group('educationLevelId')
                                            ->where($where);
                    });
        return $result;
    }

    /**
     * ведергиваем уровни образования из лодовских
     *
     * @return type
     */
    public function getEductionLevel(){
        $level = null;
        if(!$this->isEmptyField('educationLevelId')){
            $level = new \Model\LodEntities\EducationLevel();
            $level = $level->getById($this->getField('educationLevelId'));
        }
        if(!empty($level)){
            return $level;
        }
        $program = $this->getEduProgram();
        if(!empty($program)){
            $level = $program->getEduProgramLevel();
        }
        return $level;
    }

    /**
     * Дергаем уровни образования из еиис(заявлений)
     *
     * @param $fromGetEduProgram - если true то методы вызывается из метода GetEduProgram, просто возник случай,
     * что методы бесконечном цикле вызывали друг друга
     * @return type
     */
    public function getEduProgramLevel($fromGetEduProgram = false){
        $level = null;
        if(!$this->isEmptyField("educationLevelId")){
            $level = new \Model\Entities\eiisEduLevels();
            $level = $level->getById(
                str_replace('-','',strtoupper($this->fields["educationLevelId"]))
            );
            if($level != null){
                return $level;
            }
        }
        if(!$fromGetEduProgram) {
            $program = $this->getEduProgram();
            if ($program != null) {
                $level = $program->getEduProgramLevel();
            }
        }
        return $level;
    }

    private $_eduProgram = 0;
    /**
     * выдергнем програму из еиис
     *
     * @return type
     */
    public function getEduProgram() {
        if($this->_eduProgram !== 0){
            return $this->_eduProgram;
        }
        //Добавляем доп логику, что если ссылка на программу не задана, значит это такой уровень где нет программ, типа основного образования
        //но мы к таким уровням добавили программы, вот их и будем возвращать
        if($this->isEmptyField('EduProgramsID')){
            //идем по пути с уровнем
            if($this->isEmptyField("educationLevelId")){
                return null;
            }
            $eduLevel = $this->getEduProgramLevel(true);
            if($eduLevel == null){
                $this->_eduProgram = null;
                return null;
            }
            $program = $eduLevel->getLinkedProgram();
            $this->_eduProgram = $program;
            return $program;
        } else{
            $program = new \Model\Entities\eiisEduPrograms($this->getField('EduProgramsID'));
            if($program->getId() == null) {
                $this->_eduProgram = null;
            } else {
                $this->_eduProgram = $program;
            }
            return $this->_eduProgram;
        }
    }

    /**
     * дергаем тип програмы из еиис
     * @return type
     */
    public function getEduProgramType(){
        $program = $this->getEduProgram();
        if(empty($program)){
            return $program ;
        }
        $eduProgType = $program->getEduProgramType();
        if(empty($eduProgType)){
            $x=0;//для отладки
        }
        return $eduProgType;
    }

    /**
     * выдергиваем названия квалификациий
     *
     * @return type - массив с названиями квалификаций
     */
    public function getQualificationsName() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $mtmQualifications = new \Model\LodEntities\EducationProgramToQualification();
        $mtmQualifications =$mtmQualifications->getAllByWhereParseId(
            array('educationProgramId' => $this->getId(),),
            'qualificationId'
        );
        if(empty($mtmQualifications)){
            return array();
        }
        $mtmQualificationsId = array_keys($mtmQualifications);
        $qualifications = new \Model\Entities\isgaQualifications();
        $qualifications = $qualifications->getAllByWhereParseId(array(
            //пляски с тире и регистрами
            new \Zend\Db\Sql\Predicate\Expression('UPPER(replace(`Id`,"-","")) in ('.'"'.strtoupper(implode('","', $mtmQualificationsId)).'"'.')'),
            //'Id' => $mtmQualificationsId,
            ),
            'Name'
        );
        if(empty($qualifications)){
            $qualificationsNames = array();
        } else {
            $qualificationsNames = array_keys($qualifications);
        }
        return $qualificationsNames;
    }
    
    public function gelLicenseSupplement() {
        if($this->isEmptyField('LicenseSupplementID')){
            return null;
        }
        $licenseSupplement = new \Model\Entities\eiisLicenseSupplements();
        $licenseSupplement = $licenseSupplement->getById($this->getField('LicenseSupplementID'));
        return $licenseSupplement;
    }

     public function getProgramsBySupplId($supplIds){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('EducationProgram')
                ->columns(array("id","educationProgramCatalogItemId"=>"EduProgramsID","educationLevelId"))
                ->where(array("LicenseSupplementID"=>$supplIds));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
}
