<?php

namespace Model\LodEntities;

use Model\Entities\eiisEducationalOrganizations;
use Model\Gateway\LodBaseEntity;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Where;

class NumberOfProgramCatalog extends LodBaseEntity
{
    protected $tableName = 'NumberOfProgramCatalog';
    protected $idName = 'id';
}