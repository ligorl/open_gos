<?php
namespace Model\LodEntities;

use Model\Gateway;

class OrganizationLocation extends Gateway\LodBaseEntity{
    protected $tableName = "OrganizationLocation";
    protected $idName = "id";

    public function getLocationFromSupplement( $suppId ){
        return $this->getDataJoin_ArrayCondition(
          [
              'id'=>new \Zend\Db\Sql\Expression('OrganizationLocation.id'),
              'organizationLocationLink'=>new \Zend\Db\Sql\Expression('OrganizationLocationToLicenseAttach.id'),
              'organizationId'=>'organizationId',
              'settlementId'=>'settlementId',
              'postcode'=>'postcode',
              'address'=>'address',
              'addressType'=>'addressType',
              'licenseAttachId'=>new \Zend\Db\Sql\Expression('OrganizationLocationToLicenseAttach.licenseAttachId'),
          ],
          [
              'table' => 'OrganizationLocationToLicenseAttach',
              'binder' => 'OrganizationLocation.id = OrganizationLocationToLicenseAttach.organizationLocationId',
              'type' => 'inner'
          ],
          [ 
              ['column' => 'licenseAttachId="'.$suppId.'"'],
          ]);
    }
    public function getOrganizationLocationFromOrg($orgId){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('OrganizationLocation')
          ->columns(array("organizationId", "settlementId", "postcode", "address", "addressType"), false)
          ->where(array("organizationId" => $orgId,"addressType"=>"OrganizationLocation"));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    public function getOrganizationLocationFromOrgAndSettlement($orgIds,$settlementId){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('OrganizationLocation')
          ->columns(array("organizationId", "settlementId", "postcode", "address", "addressType"), false)
          ->where(array("organizationId" => $orgIds,"addressType"=>"OrganizationLocation","settlementId"=>$settlementId));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }

    //����� ���������� �������� �� ��������� ���������� ��������
    public function isLinkedSupplementValid(){
        $sqlSupps = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
        $suppsIdList = $sqlSupps->select();
        $suppIdFromLocs =  $suppsIdList->from("OrganizationLocationToLicenseAttach")
            ->columns(array("licenseAttachId"))
            ->where(array(
                "organizationLocationId" => $this->getId()
            ));
        //������������ ���� � ������
        $statement = $sqlSupps->prepareStatementForSqlObject($suppIdFromLocs);
        $results = $statement->execute();

        //print_r($sql->getSqlstringForSqlObject($subselectOrgs));die;

        $Ids = array();
        foreach ($results as $supp) {
            $Ids[] = $supp["licenseAttachId"];
        }
        $where = null;
        if (count($Ids) > 0) {
            $where[] = new \Zend\Db\Sql\Predicate\In("Id", $Ids);
        } else
            return false;

        $suppTable = new \Model\Gateway\EntitiesTable("eiis_LicenseSupplements");
        $suppsList = $suppTable->getAllEntities(null, null, $where)->getObjectsArray();

        //����� �������� ����������
        $hasValidSupps = false;
        foreach($suppsList as $supp){
            if($supp->isValid()){
                $hasValidSupps = true;
                break;
            }
        }

        if($hasValidSupps)
            return true;
        return false;
    }
}