<?php
namespace Model\LodEntities;

use Model\Gateway;

class LicenseSupplementToLicenseRequest extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseSupplementToLicenseRequest";
    protected $idName = "id";
    
    public function getLicenseSupplement(){
        if( $this->isEmptyField( 'licenseAttachId' ) ){
            return null;
        }
        $result = new \Model\Entities\eiisLicenseSupplements( $this->getFieldSafe( 'licenseAttachId' ) );
        if( $result->isEmpty() ){
            return false;
        }
        return $result;
    }
    
}