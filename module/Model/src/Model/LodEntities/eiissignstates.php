<?php
namespace Model\LodEntities;

use Model\Gateway;

class eiissignstates extends Gateway\LodBaseEntity{
    protected $tableName = "eiis_sign_states";
    protected $idName = "id";
}