<?php
namespace Model\LodEntities;

use Model\Gateway;

class EducationProgramCIToQualification extends Gateway\LodBaseEntity{
    protected $tableName = "EducationProgramCIToQualification";
    protected $idName = "id";
}