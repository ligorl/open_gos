<?php

namespace Model\LodEntities;

use Model\Gateway;

class RegistryChange extends Gateway\LodBaseEntity {

    protected $tableName = "RegistryChange";
    protected $idName    = "Id";

    public function getLicense() {
        $License = $this->getLinkedItemIsga("eiis_Licenses", array("Id"=>$this->fields["licenseId"]));
        return $License;
    }

    public function getSupplement() {
        $Supplement = $this->getLinkedItemIsga("eiis_LicenseSupplements", array("Id"=>$this->fields["licenseAttachId"]));
        return $Supplement;
    }
    public function getCountAdress() {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $id = $this->fields["licenseAttachId"];
        $select->from('OrganizationLocationToLicenseAttach')
            ->columns(array("count" => new \Zend\Db\Sql\Expression('COUNT( id)')))
            ->where("licenseAttachId = '".$id."'");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult = array_column($dbResults->toArray(), "count");
        return $arrResult[0];
    }
    public function getCountEduProgram() {
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $id = $this->fields["licenseAttachId"];
        $select->from('EducationProgram')
            ->columns(array("count" => new \Zend\Db\Sql\Expression('COUNT( id)')))
            ->where("LicenseSupplementID = '".$id."'");
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult = array_column($dbResults->toArray(), "count");
        return $arrResult[0];
    }
    
    public function getButtonsLicense() {
        $buttons = array();
        if ($this->fields["licenseId"] !== NULL) {
            $ReestrStatusButton = new \Model\LodEntities\ReestrStatusButton();
            $License            = $this->getLicense();
            if($License){
                $stateCode          = $License->getField("fk_eiisLicenseState");
                $signCode           = $License->getField("lod_signStateCode");
                $where              = "TypeReestr = 'License' AND ((TypeCode = 'stateCode' AND CurrentStatus ='" . $stateCode . "') OR (TypeCode = 'signCode' AND CurrentStatus ='" . $signCode . "'))";
                $buttons            = $ReestrStatusButton->getAllByWhere($where);
            }
        }
        return $buttons;
    }
     public function getButtonsSupplement() {
        $buttons = array();
       if ($this->fields["licenseAttachId"] !== NULL) {
            $ReestrStatusButton = new \Model\LodEntities\ReestrStatusButton();
            $Supplement         = $this->getSupplement();
            if($Supplement){
                $stateCode          = $Supplement->getField("fk_eiisLicenseSupplementState");
                $signCode           = $Supplement->getField("lod_signStateCode");
                $where              = "TypeReestr = 'Supplement' AND ((TypeCode = 'stateCode' AND CurrentStatus ='" . $stateCode . "') OR (TypeCode = 'signCode' AND CurrentStatus ='" . $signCode . "'))";
                $buttons            = $ReestrStatusButton->getAllByWhere($where);
            }
        }
        return $buttons;
    }
    public function checkSupplementStatus($dealId,$type = "addToRegistry",$status="9416C0E6E84F4194B57DA930AD10A4D0"){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $selectString =   "SELECT count(f11_mon.eiis_LicenseSupplements.id) as count FROM f11_mon_lod.RegistryChange "
                        . "LEFT JOIN f11_mon.eiis_LicenseSupplements ON f11_mon.eiis_LicenseSupplements.Id=f11_mon_lod.RegistryChange.licenseAttachId "
                        . "WHERE f11_mon_lod.RegistryChange.dealId='".$dealId."' AND f11_mon.eiis_LicenseSupplements.fk_eiisLicenseSupplementState NOT LIKE '".$status."' AND f11_mon_lod.RegistryChange.registryChangeType ='".$type."'";
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult = array_column($dbResults->toArray(), "count");
        if($arrResult[0]>0){
            return false;
        }else{
            return true;
        }
    }
    public function getOrgIdsFromRegistryType($dealId,$type = "addToRegistry"){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $selectString =   "SELECT f11_mon.eiis_LicenseSupplements.fk_eiisBranch FROM f11_mon_lod.RegistryChange "
                        . "LEFT JOIN f11_mon.eiis_LicenseSupplements ON f11_mon.eiis_LicenseSupplements.Id=f11_mon_lod.RegistryChange.licenseAttachId "
                        . "WHERE f11_mon_lod.RegistryChange.dealId='".$dealId."' AND  f11_mon_lod.RegistryChange.registryChangeType ='".$type."'";
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return array_column($dbResults->toArray(), "fk_eiisBranch");
    }

}
