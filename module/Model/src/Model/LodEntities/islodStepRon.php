<?php

namespace Model\LodEntities;

use Model\Gateway;

class islodStepRon extends Gateway\LodBaseEntity {

    protected $tableName = "islod_StepRon";
    protected $idName    = "Id";

    public function getCount() {
        
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        if($this->fields["Event"]=="step/2"){
            $table = "LicenseRequest";
        }else{
            $table = "Deal";
        }
        if($this->fields["Event"]=="step/2"){
            $where = [];
            $where[] = new \Zend\Db\Sql\Predicate\IsNull("dealId");
        } else if($this->fields["Statuses"] == "All" ){
            $where = [];
            $where[] = new \Zend\Db\Sql\Predicate\Expression(" 1 ");
        }else{
            $status_array = explode(";",$this->fields["Statuses"]);
            $status_str = implode("','",$status_array);
            $where = "stateCode IN ('".$status_str."')";
        }
        $select->from($table)
            ->columns(array("count"=>new \Zend\Db\Sql\Expression('COUNT(*)')), false)
            ->where($where);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $result_arr = array_column($results->toArray(), 'count');
        return $result_arr[0];
    }

}
