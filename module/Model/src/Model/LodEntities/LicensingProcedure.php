<?php
namespace Model\LodEntities;

use Model\Gateway;

class LicensingProcedure extends Gateway\LodBaseEntity{
    protected $tableName = "LicensingProcedure";
    protected $idName = "id";

    


    /**
     * вернем причин, которые применимы на открытой части
     * типы процедур которые использовать только на открытой
     * с замутом, чтоб на публике и на отладке разные были
     */
    public function getLicensingProcedureForOpenId() {
        $user = \Model\Entities\isgaUsers::getAutorisateClass();
        if(empty($user)){
            throw new \Exception('Пользовательне не аутентифицирован');
        }       
        $identityUser = $user;
        
        $userName = '';
        if( !empty($identityUser) ){
            $userName = $identityUser->getFieldSafe('Login');
        }
        if( \Eo\Model\Export::isTestServer() || 'eouser' == $userName || 'eouser1' == $userName ){
            //типы процедур которые использовать только на открытой
            return  array(
                //закомити для показа
                '3381eb13928c4260b59cfa85f7ff8dec', //Переоформление лицензии (открытие адресов, программ, филиалов)
                'd13f3a96d6a94f1685469a6b0b61cc4c', //Переоформление лицензии (другие причины)
                '7d2f8700fb8d43a5a428de7cfc6e2bea', //Прекращение осуществления образовательной деятельности
                '008f6531542f4dfba6180ec74f1c6c4e', //Предоставление копии лицензии (приложения)
                'eb9e2f563ef24b3aa127811a27cd7009', //Предоставление дубликата лицензии
            );
        }
        else{
            return  array(
                //закомити для показа
                //'3381eb13928c4260b59cfa85f7ff8dec', //Переоформление лицензии (открытие адресов, программ, филиалов)
                //'d13f3a96d6a94f1685469a6b0b61cc4c', //Переоформление лицензии (другие причины)
                //'7d2f8700fb8d43a5a428de7cfc6e2bea', //Прекращение осуществления образовательной деятельности
                //'008f6531542f4dfba6180ec74f1c6c4e', //Предоставление копии лицензии (приложения)
                'eb9e2f563ef24b3aa127811a27cd7009', //Предоставление дубликата лицензии
            );
        }
    }

    public function getReasons(){
        $reasons = $this->getLinkedItems("LicenseReasonCatalogItem",array(
            "licensingProcedureId" => $this->fields["Id"]
        ));

        return $reasons;
    }

}