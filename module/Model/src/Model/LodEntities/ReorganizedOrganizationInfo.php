<?php

namespace Model\LodEntities;

use Model\Gateway;

class ReorganizedOrganizationInfo extends Gateway\LodBaseEntity {

    protected $tableName = "ReorganizedOrganizationInfo";
    protected $idName = "id";

    /**
     * Наименование организации
     * @return string|null
     */
    public function getOrganizationTitle(){
        return $this->getField('organizationTitle');
    }
    public function getReorganizedOrganization(){
        if($this->isEmptyField("organizationId")){
            return null;
        }

        $eo = new \Model\Entities\eiisEducationalOrganizations($this->getField("organizationId"));

        return $eo;
    }
    public function getFullOrgInfo($requestIds)
    {
        $requestStr = implode("','",$requestIds);
        $selectString = "SELECT "
                . "f11_mon_lod.ReorganizedOrganizationInfo.organizationId AS orgId, "
                . "f11_mon_lod.ReorganizedOrganizationInfo.reorganizationType AS reorganizationType, "
                . "f11_mon_lod.ReorganizedOrganizationInfo.licenseId AS licenseId, "
                . "f11_mon.eiis_EducationalOrganizations.FullName AS fullTitle, "
                . "f11_mon.eiis_EducationalOrganizations.ShortName AS shortTitle, "
                . "f11_mon.eiis_EducationalOrganizations.LawAddress AS address, "
                . "f11_mon.eiis_EducationalOrganizations.Inn AS inn, "
                . "f11_mon.eiis_EducationalOrganizations.Kpp AS kpp,"
                . "f11_mon_lod.OrganizationLocation.settlementId AS settlementId,"
                . "f11_mon.eiis_EducationalOrganizations.GosRegNum AS ogrn  FROM f11_mon_lod.ReorganizedOrganizationInfo "
                . "LEFT JOIN f11_mon.eiis_EducationalOrganizations ON f11_mon.eiis_EducationalOrganizations.Id = f11_mon_lod.ReorganizedOrganizationInfo.organizationId "
                . "LEFT JOIN f11_mon_lod.OrganizationLocation ON f11_mon_lod.OrganizationLocation.organizationId=f11_mon_lod.ReorganizedOrganizationInfo.organizationId "
                . "WHERE licenseRequestId IN('". $requestStr."') AND addressType = \"OrganizationLocation\"";       
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    public function getSettlementByRequestIds($requestIds,$type=null)
    {
        if($type == null){
            $type = $this->getField("reorganizationType");
        }

        $requestStr = implode("','",$requestIds);
        $selectString = "SELECT "
                . "f11_mon_lod.ReorganizedOrganizationInfo.organizationId AS orgId, "
                . "f11_mon.eiis_EducationalOrganizations.FullName AS fullTitle, "
                . "f11_mon.eiis_EducationalOrganizations.ShortName AS shortTitle, "
                . "f11_mon.eiis_EducationalOrganizations.LawAddress AS address, "
                . "f11_mon.eiis_EducationalOrganizations.Inn AS inn, "
                . "f11_mon.eiis_EducationalOrganizations.Kpp AS kpp,"
                . "f11_mon_lod.OrganizationLocation.settlementId AS settlementId,"
                . "f11_mon.eiis_EducationalOrganizations.GosRegNum AS ogrn  FROM f11_mon_lod.ReorganizedOrganizationInfo "
                . "LEFT JOIN f11_mon.eiis_EducationalOrganizations ON f11_mon.eiis_EducationalOrganizations.Id = f11_mon_lod.ReorganizedOrganizationInfo.organizationId "
                . "LEFT JOIN f11_mon_lod.OrganizationLocation ON f11_mon_lod.OrganizationLocation.organizationId=f11_mon_lod.ReorganizedOrganizationInfo.organizationId AND f11_mon_lod.OrganizationLocation.addressType = 'OrganizationLocation' "
                . "WHERE licenseRequestId IN('". $requestStr."') AND reorganizationType ='".$type."'";       
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }
    
    public function getReorgInfoForDealEdit($requestIds){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('ReorganizedOrganizationInfo')
            ->columns(array("id","licenseId"),true)
            ->join( 
                    new \Zend\Db\Sql\TableIdentifier('eiis_EducationalOrganizations', 'f11_mon'), 
                    "f11_mon.eiis_EducationalOrganizations.Id =ReorganizedOrganizationInfo.organizationId ", 
                    array("RegularName","Address")
                  )
            ->join( 
                    new \Zend\Db\Sql\TableIdentifier('eiis_Licenses', 'f11_mon'), 
                    "f11_mon.eiis_Licenses.Id =ReorganizedOrganizationInfo.licenseId", 
                    array("LicenseRegNum")
                  )
            ->where(array("ReorganizedOrganizationInfo.licenseRequestId"=>$requestIds));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }
    public function getEiisLicese() {
        $eiisLicense = new \Model\Entities\eiisLicenses();
        $eiisLicense = $eiisLicense->getById($this->getFieldSafe('licenseId'));
        return $eiisLicense;        
    }
    
}
