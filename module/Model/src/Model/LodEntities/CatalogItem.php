<?php
namespace Model\LodEntities;

use Model\Gateway;

class CatalogItem extends Gateway\LodBaseEntity{
    protected $tableName = "CatalogItem";
    protected $idName = "id";

    /**
     * Выборка типов обращения.
     *
     * @return mixed
     */
    public function getAppealTypes()
    {
        $appealTypes = $this->getAllByWhereWithOrder(
            array('catalog' => 'serviceReason'),
            'code'
        );

        return $appealTypes;
    }
}