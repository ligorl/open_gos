<?php
namespace Model\LodEntities;

use Model\Gateway;

class LicenseRequestPayment extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseRequestPayment";
    protected $idName = "id";
    
    /**
     * дернгаем скан платежного пораучения
     * 
     * @return type
     */
    public function getScanFile()
    {        
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('LicenseRequestPayment', $this->getId());
        return $result;
    }
    
}