<?php
namespace Model\LodEntities;

use Model\Gateway;

class LodUser extends Gateway\LodBaseEntity{
    protected $tableName = "Lod_User";
    protected $idName = "uuid";

    public function getFio(){
        $Lastname = $this->getField('lastName');
        $FirstName = $this->getField('firstName');
        $middleName = $this->getField('middleName');
        return $Lastname." ".mb_substr($FirstName,0,1,"UTF-8").".".mb_substr($middleName,0,1,"UTF-8").".";
    }

    /**
     *получаем и кешируем данные о должности эксперта
     *
     * @var type
     */
    private $_emploeePost = 0;
    public function getEmploeePost() {
        if($this->_emploeePost !== 0){
            return $this->_emploeePost;
        }
        $this->_emploeePost = null;
        if($this->isEmptyField('employeePostId')){
            return null;
        }
        $emplPost = new \Model\LodEntities\employeepost();
        $this->_emploeePost =  $emplPost->getByWhere(array(
            $emplPost->getIdName() => $this->getField('employeePostId'),
        ));
        return $this->_emploeePost;
    }

    /**
     * наименование должности  в родительном падеже
     *
     * @return type
     */
    public function getTitleGen() {
        $result = '';
        $emploeePost = $this->getEmploeePost();
        if(!empty($emploeePost)){
            $result = $emploeePost->getTitleGen();
        }
        return $result;
    }

    /**
     * наименование должности
     *
     * @return type
     */
    public function getTitle() {
        $result = '';
        $emploeePost = $this->getEmploeePost();
        if(!empty($emploeePost)){
            $result = $emploeePost->getFieldOrSpace('title');
        }
        return $result;
    }
    /**
     * Проверяем, экперт ли юзер
     * @return boolean
     */
    public function isExpert(){
        return $this->getField('role') === 'expert';
    }
}