<?php
namespace Model\LodEntities;

use Model\Gateway;

/**
 * прописываем какие  документы необходимы заявлению
 */
class LicenseReasonDocument extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseReasonDocument";
    protected $idName = "id";

    
    protected static  $documentTypeCache = array();

    /**
     * дергаем класса типа документов
     * 
     * @return type
     */
    public function getDocumentType(){
        $docTypeId = $this->getFieldSafe( 'DocumentTypeId' );
        $result = false;
        if( !isset( self::$documentTypeCache[ $docTypeId ]) ){
            $docTypeClass = new \Model\Entities\isgaDocumentTypes();
            $result = $docTypeClass->getById( $docTypeId );
        }
        return $result;
    }
    
    /**
     * дергаем наименование тика документа
     * 
     * @return type
     */
    public function getDocumentTypeName(){
        $result = '';
        $docTypeClass = $this->getDocumentType();
        if( !empty($docTypeClass) ){
            $result = $docTypeClass->getFieldSafe( 'Name' );
        }
        return $result;
    }
    
    /**
     * дергаем классы документов данного типа у заявления
     * 
     * @param type $requestId   - ид  заявления
     * @return type             - массив классов документов
     */
    public function getLicenseRequestDocumentAll( $requestId ){
        $licReqDocAll = new LicenseRequestDocument();
        return  $licReqDocAll->getAllByWhereParseId( array( 
            'LicenseRequestId' => $requestId,
            'DocumentTypeId' => $this->getFieldSafe( 'DocumentTypeId' )
        ) );
    }
    
    /**
     * вернем строку расширений файлов
     * @return string
     */
    public function getExtentionString(){
        $result = '';
        $optionStr = $this->getFieldSafe( 'Extention' );
        if( !empty($optionStr) ){
            $resultArray = array();
            $optionArray = explode( ',' , $optionStr );
            foreach( $optionArray as $optionOne ){
                $one = trim( $optionOne );
                $first = substr( $one , 0, 1 );
                if( '.' == $first ){
                    $one = substr( $one , 1 );
                }
                $resultArray[$one] = '.'.$one;
            }
            $result = implode( ',', $resultArray );
        }
        return $result;
    }
    
    /**
     * Документ обязателен для загрузки ? 
     * 
     * @return boolean
     */
    public function isRequired( ){
        $isRequired = (int) $this->getFieldSafe( 'Required' );
        if( empty($isRequired) ){
            return false;
        }
        return true;
    }
    
    public function isCheckSign(){
        $isChek = $this->getFieldSafe( 'CheckSign' );
        if( empty($isChek) ){
            return false;
        }
        return true;
    }
    
}