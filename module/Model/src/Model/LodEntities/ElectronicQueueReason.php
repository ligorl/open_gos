<?php

namespace Model\LodEntities;

use Model\Gateway\LodBaseEntity;


class ElectronicQueueReason extends LodBaseEntity
{
    protected $tableName = 'ElectronicQueueReason';
    protected $idName = 'id';
}