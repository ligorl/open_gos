<?php

namespace Model\LodEntities;

use Model\Entities\isgaApplicationReasons;
use Model\Gateway\LodBaseEntity;
use Zend\Session\Container;

class ElectronicQueueEntry extends LodBaseEntity
{
    protected $tableName = 'ElectronicQueueEntry';
    protected $idName = 'id';

    /**
     * @param $idOrg
     * @param $where
     * @param $addWhere
     * @return array
     */
    public function getDataQueue($idOrg, $where = null, $addWhere = null)
    {
        (isset($where)) ? $where['ElectronicQueueEntry.organizationId'] = $idOrg : $where = ['ElectronicQueueEntry.organizationId'=>$idOrg];

        $entities = $this->selectComplicated(
            'ElectronicQueueEntry',                         //имя таблици для выборки
            array('id', 'organizationId', 'electronicQueueReasonId', 'startReceptionTime', 'status'),      // поля
            $where,                                          // условие
            null,                                           // групировка
            null,                                           // сортировка
            null,                                           // количество записей.
            null,                                           // страница
            array(                                          // join имя таблицы
                'ElectronicQueueReason',
                'ServiceReasonCatalog',
                'ElectronicQueueEntryToLicenseReasonCatalogItem',
                'ElectronicQueueEntryToAccreditationReason'
            ),
            array(                                          // условие join
                'ElectronicQueueEntry.electronicQueueReasonId = ElectronicQueueReason.id',
                'ElectronicQueueEntry.serviceReasonCatalogId = ServiceReasonCatalog.id',
                'ElectronicQueueEntry.id = ElectronicQueueEntryToLicenseReasonCatalogItem.electronicQueueEntryId',
                'ElectronicQueueEntry.id = ElectronicQueueEntryToAccreditationReason.electronicQueueEntryId',
            ),
            array(                                          // поля выборки с join
                array(
                    'TypeService'=>'serviceType',
                    'TypeTreatment'=>'name',
                    'TypeTreatmentId'=>'id'
                ),
                array(
                    'BasisTreatment'=>'name',
                    'BasisTreatmentId'=>'id',
                ),
                 array(
                     'licenseReasonCatalogItemId'=>'licenseReasonCatalogItemId',
                 ),
                array(
                    'AccredApplicationReasons'=>'AccredApplicationReasons',
                )
            ),
            array('INNER', 'INNER', 'LEFT', 'LEFT')             // тип join
        )->toArray();

        $idEntryLic = [];
        $idEntryAcr = [];
        $entitiesOut = [];
        foreach ($entities as $id=>$entity){
            if(!isset($entitiesOut[$entity['id']])){
                $entity['status'] = $this->getStatus($entity['status']);
                $entitiesOut[$entity['id']] = $entity;
            }

            if(in_array($entity['electronicQueueReasonId'], ['0d49ba7467504c43a17600691a198f23', '4e53f68a10974c8a99b15258544b3930'])){
                $idEntryLic[$entity['id']][] = $entity['licenseReasonCatalogItemId'];
            }elseif (in_array($entity['electronicQueueReasonId'], ['f5c12eeb141e47c5a314c235bdceec55', 'fb80f7c6613e458aaddf7dd0b1ca9b5d'])){
                $idEntryAcr[$entity['id']][] = $entity['AccredApplicationReasons'];
            }

        }

        $isgaApplicationReasonsClass = new isgaApplicationReasons();
        $licenseReasonCatalogItemClass = new LicenseReasonCatalogItem();
        $applicationReasons = $isgaApplicationReasonsClass->getAllData();
        $licenseReasonCatalog = $licenseReasonCatalogItemClass->getAllData();

        $selectOptionReason = [];
        // Проход по причинам лицензий
        foreach ($idEntryLic as $idEntity => $idReason){
            $strArr = [];
            if(!is_null($idReason)){
                foreach ($idReason as $id){
                    if(isset($licenseReasonCatalog[$id])){
                        $strArr[] = $licenseReasonCatalog[$id];
                        $selectOptionReason[$id] = $licenseReasonCatalog[$id];
                    }
                }
                $entitiesOut[$idEntity]['ReasonTreatmentId'] = $idReason;
            }
            $entitiesOut[$idEntity]['ReasonTreatment'] = implode(', ', $strArr);
        }

        // Проход по причинам акредитаций
        foreach ($idEntryAcr as $idEntity => $idReason){
            $strArr = [];
            if(!is_null($idReason)){
                foreach ($idReason as $id){
                    if(isset($applicationReasons[$id])){
                        $strArr[] = $applicationReasons[$id];
                        $selectOptionReason[$id] = $applicationReasons[$id];
                    }
                }
                $entitiesOut[$idEntity]['ReasonTreatmentId'] = $idReason;
            }
            $entitiesOut[$idEntity]['ReasonTreatment'] = implode(', ', $strArr);
        }
        
        foreach ($entitiesOut as $k => $val) {
            $date = new \DateTime($val['startReceptionTime']);
            $entitiesOut[$k]['startReceptionTime'] = $date->format('d.m.Y H:i:s');
            if(!empty($addWhere)) {
                if (!in_array($addWhere, $val['ReasonTreatmentId'])) {
                    unset($entitiesOut[$k]);
                }
            }
        }

        $optionReason = new \Zend\Session\Container('optionReason');
        $optionReason->exchangeArray($selectOptionReason);

        return $entitiesOut;
    }

    /**
     * Возвращает коректное название статуса.
     *
     * @param $status
     * @return string
     */
    public function getStatus($status)
    {
        switch ($status) {
            case 'confirmed':
                return 'Подтвержден';
                break;
            case 'appeared':
                return 'Явился';
                break;
            case 'cancelled':
                return 'Отменен';
                break;
            case 'notappeared':
                return 'Не явился';
                break;
            case 'unapproved':
                return 'Не подтвержден';
                break;
        }

        return $status;
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param string $reasonId
     *
     * @return \Model\Gateway\type
     */
    public function getForDates($from, $to, $reasonId)
    {
        $reasons = [];
        $str = '';
        $reasons[] = $reasonId;

        if ($reasonId == "'4e53f68a10974c8a99b15258544b3930'") {
            $reasons[] = "'fb80f7c6613e458aaddf7dd0b1ca9b5d''";
        } elseif ($reasonId == "'fb80f7c6613e458aaddf7dd0b1ca9b5d'") {
            $reasons[] = "'4e53f68a10974c8a99b15258544b3930'";
        }

        $str = "electronicQueueReasonId IN ('". implode("', '", $reasons) ."')";

        $f = clone $from;
        $f->setTime(0, 0, 0);
        $t = clone $to;
        $t->setTime(23, 59, 59);

        $entities = $this->getByWhere([
            'startReceptionTime >= "' . $f->format('H:i:s') . '"' ,
            'endReceptionTime <= "'   . $t->format('H:i:s') . '"',
            'status != "cancelled"',
            $str
        ]);

        return $entities;
    }

    /**
     * Проверка на перекрытие времени.
     *
     * @param \DateTime $from
     * @param \DateTime $to
     * @return bool
     */
    public function overlapsTime($from, $to)
    {
        return ($from >= $this->getField('startReceptionTime') && $from < $this->getField('endReceptionTime'))
            || ($to > $this->getField('startReceptionTime') && $to <= $this->getField('endReceptionTime'));
    }
}