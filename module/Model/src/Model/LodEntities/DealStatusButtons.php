<?php
namespace Model\LodEntities;

use Model\Gateway;

class DealStatusButtons extends Gateway\LodBaseEntity{
    protected $tableName = "DealStatusButtons";
    protected $idName = "Id";
}