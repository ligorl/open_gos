<?php

namespace Model\LodEntities;

use Model\Entities\eiisEducationalOrganizationProperties;
use Model\Gateway;

class RequestOrganizationInfo extends Gateway\LodBaseEntity
{

    protected $tableName = "RequestOrganizationInfo";
    protected $idName = "id";

    public function getCountRequestEducationProgram($type = null)
    {
        /*
          if(0){//$type=="current-op"){
          $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
          $subselectSupps = $sql->select();
          $subselectSupps->from("LicenseSupplementToLicenseRequest")
          ->columns(array("licenseAttachId"))
          ->where(array(
          "licenseRequestId" => $this->getField("requestId")
          ));

          $epTable = new \Model\Gateway\LodEntitiesTable("EducationProgram");

          $where = [];
          $where[] = new In("LicenseSupplementID",$subselectSupps);
          $where["organizationId"] = $this->getField("orgId");

          $pPrograms = $epTable->getAllEntities(null, null, $where)->getObjectsArray();
          return count($pPrograms);
          } */

        //Таблица с лиц программами
        $LPTable = new \Model\Gateway\LodEntitiesTable("LicenseRequestEducationProgram");

        //Запрос на количество
        $where = array(
            "requestOrganizationInfoId" => $this->getId()
        );

        switch ($type) {
            case "new-op":
                $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity", "OPENING");
                break;
            case "current-op":
                $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity", "ACTIVE");
                break;
            case "stop-op":
                $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity", "CLOSING");
                break;
            case "change-op":
                $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity", "EDITING");
                break;
        }

        $LPCount = $LPTable->getCount($where);
        return $LPCount;
    }

    public function getCountODPlaces($type)
    {
        //Таблица с адресами
        $LRATable = new \Model\Gateway\LodEntitiesTable("LicenseRequestAddress");

        //
        $where = array(
            "requestOrganizationInfoId" => $this->getId()
        );
        if ($type == "places")
            $where["requestAddressType"] = "ACTIVE";
        if ($type == "open")
            $where["requestAddressType"] = "OPENING";
        if ($type == "close")
            $where["requestAddressType"] = "CLOSING";
        //Запрос на количество
        $LRACount = $LRATable->getCount($where);
        return $LRACount;
    }

    public function getODPlaces($type = null)
    {

        $where = array(
            "requestOrganizationInfoId" => $this->getId()
        );

        if ($type == "places")
            $where["requestAddressType"] = "ACTIVE";
        if ($type == "open")
            $where["requestAddressType"] = "OPENING";
        if ($type == "close")
            $where["requestAddressType"] = "CLOSING";

        //Таблица с адресами
        $addresses = $this->getLinkedItems("LicenseRequestAddress", $where)->getObjectsArray();

        return $addresses;
    }

    public function checkCountSupplement($requestId) {
        $dbAdapter    = \Model\Gateway\LodDbAdapter::getInstance();
        $sql          = new \Zend\Db\Sql\Sql($dbAdapter);
        $select       = $sql->select();
        $select->from('LicenseSupplementToLicenseRequest')
                ->columns(array("count" => new \Zend\Db\Sql\Expression('COUNT(id)')), false)
                ->where(array("licenseRequestId" => $requestId));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults    = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult    = array_column($dbResults->toArray(), "count");
        if ((int)$arrResult[0] > 0) {
            return false;
        } else {
            return true;
        }
    }
     public function checkByOrgAndReq($requestId, $orgId)
    {
        $organization = new \Model\Entities\eiisEducationalOrganizations($orgId);
        $dbAdapter    = \Model\Gateway\LodDbAdapter::getInstance();
        $sql          = new \Zend\Db\Sql\Sql($dbAdapter);
        $select       = $sql->select();
        $select->from('RequestOrganizationInfo') 
                ->columns(array("count" => new \Zend\Db\Sql\Expression('COUNT(id)')), false)
                ->where(array("requestId" => $requestId,"fullTitle"=>$organization->getField("FullName"),"shortTitle"=>$organization->getField("ShortName"),"address"=>$organization->getField("LawAddress")));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $arrResult = array_column($dbResults->toArray(), "count");
        if((int)$arrResult[0] == count(array_unique($requestId))){
            return false;
        }else{
            return true;
        }
    }

    /**
     * прописываем  заявление, чтоб не дергать постоянно из базы
     * @param \Model\LodEntities\LicenseRequest $licReq
     */
    public function setLicenseRequest($licReq){
        if($licReq instanceof \Model\LodEntities\LicenseRequest){
            $this->_licenseRequest = $licReq;
        }
    }
    /**
     * тут хранится заявление
     * @var type 
     */
    private $_licenseRequest = 0;
    
    /**
     * дергнуть заявление, прикручен чеш, потому как постоянно запрашивается
     * @return type
     */
    public function getLicenseRequest()
    {
        if($this->_licenseRequest === 0){
            $this->_licenseRequest = null;
            if ($this->isEmptyField('requestId')) {
                return null;
            }
            $requestClasas = new \Model\LodEntities\LicenseRequest();
            $this->_licenseRequest = $requestClasas->getByWhere(
                array($requestClasas->getIdName() => $this->getField('requestId'))
            );
        }
        return $this->_licenseRequest;
    }

    /**
     *тут хранится дело
     * @var type 
     */
    private $_deal = 0;
    
    /**
     * дергнуть дело, закешировал, так как постоянно надо 
     * @return type
     */
    public function getDeal()
    {
        if($this->_deal === 0){
            $this->_deal = null;
            $requestClasas = $this->getLicenseRequest();
            if (!empty($requestClasas)) {
                $this->_deal = $requestClasas->getDeal();
            }
        }
        return $this->_deal;
    }

    /**
     * Имя организационно-правовой формы
     * @return string|null
     */
    public function getOrganizationPropertiesName()
    {
        if(!$this->isEmptyField("fk_eiisEducationalOrganizationProperties")){
            $property = new \Model\Entities\eiisEducationalOrganizationProperties($this->getField("fk_eiisEducationalOrganizationProperties"));
            return $property->getField("Name");
        }
        return null;
    }

    /**
     * Получаем причину
     * @return \Model\LodEntities\LicenseReasonCatalogItem|null
     */
    public function getReason()
    {
        $requestId = $this->getField('requestId');
        if (!$requestId) {
            return null;
        }
        $reasonToRequestTable = new \Model\LodEntities\LicenseReasonToLicenseRequest();
        /* @var $reasonToRequestTable \Model\LodEntities\LicenseReasonToLicenseRequest */
        $reasonToRequest = $reasonToRequestTable->getByWhere(['licenseRequestId' => $requestId]);
        $reasonId = $reasonToRequest->getField('licenseReasonId');
        return new \Model\LodEntities\LicenseReasonCatalogItem($reasonId);
    }

    /**
     * Получаем дополнительные поля
     * @return array ['fieldTitle' => 'fieldValue']
     */
    public function getExtraFields()
    {
        $extra = [];
        $requestId = $this->getField('requestId');
        $reason = $this->getReason();



        if (!$reason) {
            return $extra;
        }
        /* @var $reorganizedInfo \Model\LodEntities\ReorganizedOrganizationInfo */
        $reorganizedInfo = (new \Model\LodEntities\ReorganizedOrganizationInfo())
          ->getByWhere(['licenseRequestId' => $requestId]);

        //id причин для которых добавляем поле "Форма реорганизации"
        $reasonIdForForm = [
            '08f4049094984955ab29d17d6f178d17',
            '518dfabe3375431684b71d2faa5c7564',
            'c83201802b474c4393305b16b7c05375',
            '317c24a9fb7c42b88400432d210a01c4',
            'f9893ac3e72e43b59101a8270f9eaacf',
        ];
        //id причин для которых добавляем поле "Наименование реорганизованного лицензиата"
        $reasonIdForName = [
            '518dfabe3375431684b71d2faa5c7564',
            '317c24a9fb7c42b88400432d210a01c4',
        ];

        $fieldFormTitle = 'Форма реорганизации';
        $fieldNameTitle = 'Наименование реорганизованного лицензиата';

        $reasonId = $reason->getId();

        if (in_array($reasonId, $reasonIdForForm)) {
            $extra[$fieldFormTitle] = $reason->getField('designation');
        }
        if (in_array($reasonId, $reasonIdForName) && $reorganizedInfo) {
            $extra[$fieldNameTitle] = $reorganizedInfo->getOrganizationTitle();
        }
        return $extra;
    }

    /**
     * исправление ковычек в полном названии
     * @return type
     */
    public function getFullTitleFixQuotes() {
        $name = $this->getFieldSafe('fullTitle');
        $name = \Ron\Model\Export::fixQuotes($name);
        return $name;
    }

    /**
     * возращаем короткое название с исправление ковычек на елочки
     * @return type
     */
    public function getShortTitleFixQuotes() {
        $name = $this->getFieldSafe('shortTitle');
        $name = \Ron\Model\Export::fixQuotes($name);
        return $name;
    }

    /**
     * дергаем все адреса у заявления
     *
     * @param type $whereAdd - дополнительные поля если надо
     * @return type
     */
    public function getLicenseRequestAddressAll($whereAdd = array(), $parseField = null) {
        if($this->isEmptyField($this->getIdName())){
            return null;
        }
        if(empty( $parseField)){
            $parseField = $this->getIdName();
        }
        $adress = new \Model\LodEntities\LicenseRequestAddress();
        $where = array();
        $where['requestOrganizationInfoId'] = $this->getId();
        if(!empty($whereAdd) && is_array($whereAdd)){
           $where = array_merge($where, $whereAdd);
        }
        $result = $adress->getAllByWhereParseId(
               $where
        );
        return $result;
    }

    /**
     * опредлялка это главный или филиал
     * @return type истина - главный 
     *              лож    - филиал
     */
    public function isMain() {
        $deal = $this->getDeal();
        $mainOrgId = $deal->getFieldSafe('organizationId');
        $itOrgId   = $this->getFieldSafe('orgId');
        $result = strtoupper($mainOrgId) == strtoupper($itOrgId );
        return $result;
    }

    /**
     * определим имеет ли филиал запросы на закрытие программ
     * @param type $inMain  труе   - смотрим только у главного
     *                      фалсе  - смотрим только у филиала
     *                      нул    - смотрим у всех
     * @return boolean
     */
    public function hasCloseOp($inMain = null) {
        $req = $this->getLicenseRequest();
        $esonRes = $req->hasReasonCloseOp();
        if(!$esonRes){
            return false;
        }
            if($inMain === true && !$this->isMain()){
                return false;
            }
            if($inMain === false && $this->isMain()){
                return false;
            }        
        $prog = $this->getCountRequestEducationProgram('stop-op');
        if(!empty($prog)){
             return true;
        }
        return false;
    }
    
    /**
     * определится имеет ли филиал запрос на изменение программ
     * @param type $inMain  труе   - смотрим только у главного
     *                      фалсе  - смотрим только у филиала
     *                      нул    - смотрим у всех
     * @return boolean
     */
    public function hasChangeOpName($inMain = null) {
        $req = $this->getLicenseRequest();
        $esonRes = $req->hasReasonChangeOpName();
        if(!$esonRes){
            return false;
        }
            if($inMain === true && !$this->isMain()){
                return false;
            }
            if($inMain === false && $this->isMain()){
                return false;
            }        
        $prog = $this->getCountRequestEducationProgram('change-op');
            if(!empty($prog)){
                 return true;
            }
        return false;
    }
    
    /**
     * опредлится имеет ли филиал запрос на закрытие программ
     * @param type $inMain  труе   - смотрим только у главного
     *                      фалсе  - смотрим только у филиала
     *                      нул    - смотрим у всех
     * @return boolean
     * @return boolean
     */
    public function hasCloseOpName($inMain = null) {
        $req = $this->getLicenseRequest();
        $esonRes = $req->hasReasonCloseOpName();
        if(!$esonRes){
            return false;
        }
            if($inMain === true && !$this->isMain()){
                return false;
            }
            if($inMain === false && $this->isMain()){
                return false;
            }        
        $prog = $this->getCountRequestEducationProgram('stop-op');
            if(!empty($prog)){
                 return true;
            }
        return false;
    }
    
    /**
     * опрделится имеет ли филиал запрос на изменение адресов осуществлени деетельности
     * @param type $inMain  труе   - смотрим только у главного
     *                      фалсе  - смотрим только у филиала
     *                      нул    - смотрим у всех
     * @return boolean
     */
    public function hasChangeAdres($inMain = null) {
        $req = $this->getLicenseRequest();
        $esonRes = $req->hasReasonChangeAdres();
        if(!$esonRes){
            return false;
        }
            if($inMain === true && !$this->isMain()){
                return false;
            }
            if($inMain === false && $this->isMain()){
                return false;
            }
            
        $prog = $this->getCountODPlaces('places'); 
            if(empty($prog)){
                 return false;
            }
        return true;
    }
    
    /**
     * опрделится имеет ли филиал запрос на закрытие адресов осуществлени деетельности
     * @param type $inMain  труе   - смотрим только у главного
     *                      фалсе  - смотрим только у филиала
     *                      нул    - смотрим у всех
     * @param type $inMain
     * @return boolean
     */
    public function hasCloseAdres($inMain = null) {
        $req = $this->getLicenseRequest();
        $esonRes = $req->hasReasonCloseAdres();
        if(!$esonRes){
            return false;
        }
            if($inMain === true && !$this->isMain()){
                return false;
            }
            if($inMain === false && $this->isMain()){
                return false;
            }
        
        $prog = $this->getCountODPlaces('close'); 
            if(!$prog){
                 return false;
            }
        return true;
    }

    /**
     * опрделится имеет ли филиал запрос на открытие адресов осуществлени деетельности
     * @param type $inMain  труе   - смотрим только у главного
     *                      фалсе  - смотрим только у филиала
     *                      нул    - смотрим у всех
     * @param type $inMain
     * @return boolean
     */
    public function hasOpenAdres($inMain = null) {
        $req = $this->getLicenseRequest();
        $esonRes = $req->hasReasonOpenAdres();
        if(!$esonRes){
            return false;
        }
            if($inMain === true && !$this->isMain()){
                return false;
            }
            if($inMain === false && $this->isMain()){
                return false;
            }
        
        $prog = $this->getCountODPlaces('open'); 
            if(!$prog){
                 return false;
            }
        return true;
    }
    
    /**
     * смотрим имеется  ли запрос на изменинеи название этого учереждения
     * @param type $inMain  труе   - смотрим только у главного
     *                      фалсе  - смотрим только у филиала
     *                      нул    - смотрим у всех
     * @param type $inMain
     * @return boolean
     */
    public function hasChangeName($inMain = null) {
        $req = $this->getLicenseRequest();
        $esonRes = $req->hasReasonChangeName();
        if(!$esonRes){
            return false;
        }
            if($inMain === true && !$this->isMain()){
                return false;
            }
            if($inMain === false && $this->isMain()){
                return false;
            }
        //дергаем приложение из лицензии
        //если это филиал
        if( !$this->isMain() )    {
            $eiisSupl = $this->getEiisLicenseSuplementAllItReal();
            if(empty($eiisSupl)){
                return false;
            }
            //смотрим как указано в заявленых приложениях
            $eiisSuplFirst = reset($eiisSupl);
            $eiisSupName = $eiisSuplFirst->getFieldSafe('EOName');
            $orgInfoName = $this->getFieldSafe('fullTitle');
            $eiisSupNameShort = $eiisSuplFirst->getFieldSafe('EOShortName');
            $orgInfoNameShort = $this->getFieldSafe('shortTitle');
                if(
                   ($eiisSupName ==  $orgInfoName)
                   &&
                   ($eiisSupNameShort == $orgInfoNameShort)     
                ){
                     return false;
                }
            return true;
        }
        //дергаем саму лицензию
        //если головняк
        if( $this->isMain() ){
            $licReq = $this->getLicenseRequest();
            if(empty($licReq)){
                return false;
            }
            $eiisLic = $licReq->getRealLicense();
            if(empty($eiisLic)){
                return false;
            }
            //смотрим как указано в заявленых приложениях            
            $eiisSupName = $eiisLic->getFieldSafe('EOName');
            $orgInfoName = $this->getFieldSafe('fullTitle');
            $eiisSupNameShort = $eiisLic->getFieldSafe('EOShortName');
            $orgInfoNameShort = $this->getFieldSafe('shortTitle');
                if(
                   ($eiisSupName ==  $orgInfoName)
                   &&
                   ($eiisSupNameShort == $orgInfoNameShort)     
                ){
                     return false;
                }
            return true;                        
        }
    }
    
    /**
     * дергним приложения, которое можно выловить
     * @return array
     */
    public function getEiisLicenseSuplementAllIt() {
        //дергаем заявление
        $supAll = array();
        //$req = $this->getLicenseReques();
        //
        //if(!empty($req)){            
        //    $supAll = $req->getSupplements();
        //}
        $deal = $this->getDeal();
        $supAll = $deal->getAllSupplementsIsga();
        $result = array();
        if(!empty($supAll)){          
            foreach ( $supAll as $value ) { 
                $eiisId = $value ->getFieldSafe('fk_eiisBranch');
                $itId   = $this ->getFieldSafe('orgId');
                if(
                    strtoupper($eiisId)
                    ==    
                    strtoupper($itId)
                ){
                   $result[$value->getId()] = $value ;    
                }
            }
        }
        return $result;        
    }
    
    /**
     * дергним приложения, которое можно выловить? 
     * без смотреть на связи
     * @return array
     */
    public function getEiisLicenseSuplementAllItReal() {
        //дергаем заявление
        $supAll = array();
        $result = null;
        //$req = $this->getLicenseReques();
        //
        //if(!empty($req)){            
        //    $supAll = $req->getSupplements();
        //}
        //$deal = $this->getDeal();
        $eiisLicId = $this->getFieldSafe('licenseId');
        if(empty($eiisLicId)){
            $req = $this->getLicenseRequest();
            $eiisLicId = $req->getFieldSafe('licenseId');
        }
        $orgId = $this ->getFieldSafe('orgId');
        
        
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('eiis_LicenseSupplements')
            ->columns(array('*',"max" => new \Zend\Db\Sql\Expression('MAX(DateLicDoc)')))
            ->where(array(
              'fk_eiisLicense' => $eiisLicId,
              //"requestId" => $requestId, 
              'fk_eiisLicenseSupplementState' => array('9416C0E6E84F4194B57DA930AD10A4D0'),
              "fk_eiisBranch" => $orgId,
            ))
            //->limit(1)
            ;
        $selectString = $sql->getSqlStringForSqlObject($select);
        $dbResults = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        //$arrResult = array_column($dbResults->toArray(), "max");
        $arrResult = $dbResults->toArray();
        //$arrResult = reset($arrResult);
        if(!empty($arrResult)){
            foreach ( $arrResult as $value ) {                            
                $supAll = new \Model\Entities\eiisLicenseSupplements();
                $supAll->setFields($value);
                $result[] =$supAll;
            }
        }
        /*
        $supAll = $deal->getAllSupplementsIsga();
        $result = array();
        if(!empty($supAll)){          
            foreach ( $supAll as $value ) { 
                $eiisId = $value ->getFieldSafe('fk_eiisBranch');
                $itId   = $this ->getFieldSafe('orgId');
                if(
                    strtoupper($eiisId)
                    ==    
                    strtoupper($itId)
                ){
                   $result[$value->getId()] = $value ;    
                }
            }
        }         
         */
        return $result;        
    }    
 
    /**
     * получим перечень адресов
     * 
     * @param type $requestAddressType
     * <br>
     * 'ACTIVE' 
     * @return type
     */
    public function getAdresWithPostArray($requestAddressType = null) {
        $where = array();
        if(!empty($requestAddressType)){
            $where['requestAddressType'] = $requestAddressType;
        }
        $adres = $this->getLicenseRequestAddressAll( $where );
        $strArray= array();
        foreach ( $adres as $value ) {
            $postCope = $value->getFieldSafe('postcode');
            $locationId = $value->getFieldSafe('organizationLocationId');
            if(!empty($postCope)){
                $postCope .= ', ';
            }
            if( empty($locationId) ){
                $strArray[] = $postCope.$value->getFieldSafe('address');
            } else {
                $strArray[$locationId] = $postCope.$value->getFieldSafe('address');
            }
        }
        $strArray = array_filter($strArray);
        natcasesort($strArray);
        return $strArray;
    }
    
    /**
     * долучим данные о вузе
     * @param type $param
     * @return type
     */
    public function getEiisEducationalOrganizations() {
        if($this->isEmptyField('orgId')){
            return null;
        }
        $orgId = $this->getFieldSafe('orgId');
        $eiisEduOrgClass = new \Model\Entities\eiisEducationalOrganizations();
        $eiisEduOrgClass = $eiisEduOrgClass->getById($orgId);
        return $eiisEduOrgClass;
    }
    
    /**
     * дергаем адрес место нахождения организации
     * @return type
     */
    public function getOrganizationLocation(){
        if( $this->isEmptyField('addressId') ){
            return null;            
        }
        $orgLoc = new \Model\LodEntities\OrganizationLocation();
        $orgLoc = $orgLoc->getByWhere(array($orgLoc->getIdName() => $this->getFieldSafe('addressId')));
        return   $orgLoc;      
    }
    
    /**
     * дергаем адрес из метоположения организации
     * @return type
     */
    public function getOrganizationLocationAddress(){
        $result = '';
        $adrClass = $this->getOrganizationLocation();
        if( !empty($adrClass) ){
            $result = $adrClass->getFieldOrSpace('address');
        }
        return $result;
    }
    
    
    /**
     * Организационно-правовая форма лицензиата
     * 
     * @return type
     */
    public function getEducationalOrganizationProperties() {
        if( $this->isEmptyField('fk_eiisEducationalOrganizationProperties') ){
            return null;
        }
        $properiesClass = new eiisEducationalOrganizationProperties();
        $properiesClass = $properiesClass->getById( $this->getField('fk_eiisEducationalOrganizationProperties') );
        return  $properiesClass;
    }
    
    //lastString
}