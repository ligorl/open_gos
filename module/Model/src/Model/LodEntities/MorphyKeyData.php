<?php
namespace Model\LodEntities;

use Model\Gateway;

class MorphyKeyData extends Gateway\LodBaseEntity{
    protected $tableName = "morphy_key_data";
    protected $idName = "id";

    public function getBreakExspressionData(){
        return $this->getAllByWhere([
            'main_type'=>'break_expression'
        ]);
    }

    public function getBreakWordData(){
        return $this->getAllByWhere([
            'main_type'=>'break_word'
        ]);
    }

    public function getProcBreakStrData(){
        return $this->getAllByWhere([
            'main_type'=>'proc_break_string'
        ]);
    }

    public function getExeptStringData(){
        return $this->getAllByWhere([
            'main_type'=>'exeption_string'
        ]);
    }

     public function getHooksData(){
        return $this->getAllByWhere([
            'main_type'=>'hooks'
        ]);
    }
}