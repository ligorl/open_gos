<?php
namespace Model\LodEntities;

use Model\Gateway;

class governmentorganization extends Gateway\LodBaseEntity{
    protected $tableName = "GovernmentOrganization";
    protected $idName = "id";
}