<?php
namespace Model\LodEntities;

use Model\Gateway;

class StatusHistory extends Gateway\LodBaseEntity{
    protected $tableName = "status_history";
    protected $idName = "id";

}