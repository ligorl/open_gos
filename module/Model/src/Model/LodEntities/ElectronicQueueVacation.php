<?php

namespace Model\LodEntities;

use Model\Gateway\LodBaseEntity;


class ElectronicQueueVacation extends LodBaseEntity
{
    protected $tableName = 'ElectronicQueueVacations';
    protected $idName = 'id';
}