<?php
namespace Model\LodEntities;

use Model\Gateway;

class DocumentTypeCatalogItem extends Gateway\LodBaseEntity{
    protected $tableName = "DocumentTypeCatalogItem";
    protected $idName = "id";
    
    /**
     *  ид-ки типов распорядительных документов с положительны результатом
     * @var type 
     */
    static public $DocumentTypeOrderPositivIdArray = array(
        '0b3288d1a5b747cda21b47fdd96da552',
        '48e2fab4c6c741fbb2ef55491cff37b3',
        '64ac45348f0b4b439f48b04bf257bb76',
        '9e5b7ed8b2404b099f3fd36c18ecedb7',
        'c1665406436e4504bcb451ade0ad90bb',
        'c81f0428cdb649cf90321dc962ae78a4', 
        'd79274ca72884c308a93deb460f33dc6', //распоряжение о переоформлении
        'ee74d57f12114b509e22eda327e1a293', //Распоряжение о прекращении осуществления образовательной деятельности
                                            //положительное, типаи принято к исполнению
    );
    
    /**
     *  ид-ки типов распорядительных документов с отрицательным результатом
     * @var type 
     */    
    static public $DocumentTypeOrderNegativIdArray = array(
        '560834ffb86a41afbfff8f867bfe7f5b',
        '6c6f47d148294754b2dc7e2b55d287b3',
        '924df3ef87e74d9e9bf4c9326304987e',
    );  
}