<?php
namespace Model\LodEntities;

use Model\Gateway;

class OrganizationToReorganizedOrganization extends Gateway\LodBaseEntity{
    protected $tableName = "OrganizationToReorganizedOrganization";
    protected $idName = "id";

    public function getReorganizedOrganization(){
        if($this->isEmptyField("reorganizedOrganizationId")){
            return null;
        }

        $eo = new \Model\Entities\eiisEducationalOrganizations($this->getField("reorganizedOrganizationId"));

        return $eo;
    }

    public function getReorganizedOrganizationInfo(){
        if($this->isEmptyField("reorganizedOrganizationId")){
            return null;
        }

        $eo = new \Model\Entities\eiisEducationalOrganizations($this->getField("reorganizedOrganizationId"));

        return $eo;
    }
}