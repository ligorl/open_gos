<?php

namespace Model\LodEntities;

use Model\Gateway;

class LicenseRequestEducationProgramToQualification extends Gateway\LodBaseEntity {

    protected $tableName = "LicenseRequestEducationProgramToQualification";
    protected $idName = "id";

    /**
     * Получаем наименовании квалификации
     * @return string|null
     */
    public function getQualificationName() {
        $qualificationId = $this->getField('qualificationId');
        $isgaQualification = new \Model\Entities\isgaQualifications($qualificationId);
        return $isgaQualification->getName();
    }

    /**
     * Получаем все наименования квалификаций для программы
     * @param string $licenseRequestEducationProgramId
     * @return string
     */
    public static function getJoinedQualificationNames($licenseRequestEducationProgramId) {
        $table = new LicenseRequestEducationProgramToQualification();

        /* @var $qualifications array|LicenseRequestEducationProgramToQualification */
        $qualifications = $table->getAllByWhere([
            'licenseRequestEducationProgramId' => $licenseRequestEducationProgramId
        ]);

        $names = [];
        foreach ($qualifications as $qualification) {
            $names[] = $qualification->getQualificationName();
        }
        return implode(',', $names);
    }
    
    public function getQualificationIdsByEP($licenseRequestEducationProgramId){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('LicenseRequestEducationProgramToQualification')
            ->columns(array("qualificationId"), false)
            ->where(array("licenseRequestEducationProgramId"=>$licenseRequestEducationProgramId));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

}
