<?php
namespace Model\LodEntities;

use Model\Gateway;

class EducationProgramAdapter extends Gateway\LodBaseEntity{
    protected $tableName = "EducationProgramAdapter";
    protected $idName = "id";

    /**
     * Получаем nonActualEducationProgramCIId как массив
     * @return array массив из id
     */
    public function getNonActualProgramsId(){
        $ids = $this->getField('nonActualEducationProgramCIId');
        if(!$ids){
            return [];
        }
        return explode(';',$ids);
    }
}
