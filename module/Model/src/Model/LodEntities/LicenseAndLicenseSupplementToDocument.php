<?php
namespace Model\LodEntities;

use Model\Gateway;

class LicenseAndLicenseSupplementToDocument extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseAndLicenseSupplementToDocument";
    protected $idName = "id";

    /**
     * выдерем дело по лицензии
     * 
     * @param type $licenseId
     * @return type
     */    
    public function getDealIdByLicId($licenseId) {
        if(empty($licenseId)){
            return null;
        }
        $documentResult = $this->getAllByWhere(array(
            new \Zend\Db\Sql\Predicate\Like('licenseId', $licenseId),
        ));
        if(empty($documentResult)){
            $documentResult = $this->getAllByWhere(array(
                 new \Zend\Db\Sql\Predicate\Like('licenseAttachId', $licenseId),
            ));
        }
        if(empty($documentResult)){
            return null;
        }
        $documentResult = $documentResult->getObjectsArray();
        if(empty($documentResult)){
            return null;
        }
        $documentId= array();
        foreach ( $documentResult as $value ) {
            $docId = $value->getField('documentId');
            if(!empty($docId)){
                $documentId[] = $docId;
            }
        }
        $documentId = array_unique($documentId);
        if(empty($documentId)){
            return null;
        }
        $documentClass = new \Model\LodEntities\document();
        $documentClass = $documentClass->getByWhere(array(
            'id' => $documentId,
            new \Zend\Db\Sql\Predicate\IsNotNull('dealId'),
        ));
        if(empty($documentClass)){
            return null;
        }
        $result = $documentClass->getField('dealId');
        return $result;
    }

    public function getDocument() {
        if($this->isEmptyField('documentId')){
            return null;
        }
        $result = new \Model\LodEntities\document();
        $result = $result->getByWhere(array(
            new \Zend\Db\Sql\Predicate\Like('id', $this->getField('documentId')),
        ));
        return $result;
    }

}