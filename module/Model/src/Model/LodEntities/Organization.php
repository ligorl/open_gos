<?php
namespace Model\LodEntities;

use Model\Gateway;

class Organization extends Gateway\LodBaseEntity{
    protected $tableName = "Organization";
    protected $idName = "id";
}