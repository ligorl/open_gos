<?php
namespace Model\LodEntities;

use Model\Gateway;

class MorphyCase extends Gateway\LodBaseEntity{
    protected $tableName = "morphy_case";
    protected $idName = "id";

    public function getForms($word){
        return $this->getData_ArrayCondition([
            [ 'column' => 'nominative="'.$word.'"' ],
            [ 'or' => 'genitive="'.$word.'"' ],
            [ 'or' => 'dative="'.$word.'"' ],
            [ 'or' => 'accusative="'.$word.'"' ],
            [ 'or' => 'instrumental="'.$word.'"' ],
            [ 'or' => 'prepositional="'.$word.'"' ],
        ])->current();
    }

    public function getFieldOrNominative($field){
        $ret = $this->getFieldOrSpace($field);
        if( empty($ret) || $ret == false){
            $ret = $this->getFieldOrSpace('nominative');
        }
        return $ret;
    }
    
    public function nominative(){
        return $this->getFieldOrNominative('nominative');
    }

    public function genitive(){
        return $this->getFieldOrNominative('genitive');
    }

    public function dative(){
        return $this->getFieldOrNominative('dative');
    }

    public function accusative(){
        return $this->getFieldOrNominative('accusative');
    }

    public function ablative(){
        return $this->getFieldOrNominative('instrumental');
    }

    public function prepositive(){
        return $this->getFieldOrNominative('prepositional');
    }

}