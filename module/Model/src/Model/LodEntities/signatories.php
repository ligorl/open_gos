<?php
namespace Model\LodEntities;

use Model\Gateway;

class signatories extends Gateway\LodBaseEntity{
    protected $tableName = "Signatories";
    protected $idName = "id";
    
    public function getEmployeePost() {
        if($this->isEmptyField('employeePostId')){
            return null;
        }
        $result = new \Model\LodEntities\employeepost();
        $result = $result->getByWhere(array($result->getIdName() => $this->getField('employeePostId')));
        return $result;         
    }
    
    public function getShortFio() {
        $last = $this->getFieldOrSpace('lastName');
        $first = $this->getFieldOrSpace('firstName');
        $middle = $this->getFieldOrSpace('middleName');
        //первую большой
        $last = mb_strtoupper(mb_substr($last, 0,1, "UTF-8")).mb_substr($last, 1,null, "UTF-8");
        $first = mb_substr($first, 0,1, "UTF-8");
        $middle = mb_substr($middle, 0,1, "UTF-8");
        $result = $last.' '.$first.'.'.$middle.'.';
        return $result;
    }

    public function getShortFioOposite() {        
        $last = $this->getFieldOrSpace('lastName');
        $first = $this->getFieldOrSpace('firstName');
        $middle = $this->getFieldOrSpace('middleName');
        //первую большой
        $last = mb_strtoupper(mb_substr($last, 0,1, "UTF-8")).mb_substr($last, 1,null, "UTF-8");
        $first = mb_substr($first, 0,1, "UTF-8");
        $middle = mb_substr($middle, 0,1, "UTF-8");
        $result = $first.'.'.$middle.'.'.' '.$last;
        return $result;
    }
    
}