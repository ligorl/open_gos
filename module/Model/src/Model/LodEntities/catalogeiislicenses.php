<?php
namespace Model\LodEntities;

use Model\Gateway;

class catalogeiislicenses extends Gateway\LodBaseEntity{
    protected $tableName = "catalog_eiis_licenses";
    protected $idName = "id";
}