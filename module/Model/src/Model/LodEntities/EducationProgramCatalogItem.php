<?php
namespace Model\LodEntities;

use Model\Gateway;

class EducationProgramCatalogItem extends Gateway\LodBaseEntity{
    protected $tableName = "EducationProgramCatalogItem";
    protected $idName = "id";
}