<?php
namespace Model\LodEntities;

use Model\Gateway;

class OrganizationHeadEmployee extends Gateway\LodBaseEntity{
    protected $tableName = "OrganizationHeadEmployee";
    protected $idName = "id";

    public function getFioDpShort() {
        $last = $this->getLastPd();
        $name = $this->getFirstPdShort();
        if(!empty($name)){
            $name = $name.'.';
        }
        $otch = $this->getPatronymicShort();
        if(!empty($otch)){
            $otch = $otch.'.';
        }
        $name = $name.$otch;
        if(!empty($name)){
            $last .= \Ron\Model\Export::unichr(160).$name;
        }
        return $last;
    }

    public function getLastPd() {        
        $result = $this->getFieldOrSpace('lastNameDP');
        if('??' == $result){
            $result = '';
        }
        if(empty($result)){
            $result = $this->getFieldOrSpace('lastName');
            $morph = new \Islod\Model\Tools\Morphy();
            $forms = $morph->getForms($result);
            if(!empty($forms['dative'])){
                $result = $forms['dative'];
            }
            $result = $morph->firstLetterToUp($result);            
        }                
        return $result;
    }

    public function getFirstPd() {
        $result = $this->getFieldOrSpace('firstNameDP');
        if('??' == $result){
            $result = '';
        }
        if(empty($result)){
            $result = $this->getFieldOrSpace('firstName');
        }
        return $result;
    }

    public function getFirstPdShort() {
        $result = $this->getFieldOrSpace('firstNameDP');
        if('??' == $result){
            $result = '';
        }
        if(empty($result)){
            $result = $this->getFieldOrSpace('firstName');
        }
        $result = mb_substr($result, 0,1,'UTF-8');
        $result = mb_strtoupper($result,'UTF-8');
        return $result;
    }

    public function getpatronymicPd() {
        $result = $this->getFieldOrSpace('firstNameDP');
        if(empty($result)){
            $result = $this->getFieldOrSpace('firstName');
        }
        return $result;
    }

    public function getPatronymicShort() {
        $result = $this->getFieldOrSpace('patronymicDP');
        if('??' == $result){
            $result = '';
        }
        if(empty($result)){
            $result = $this->getFieldOrSpace('patronymic');
        }
        $result = mb_substr($result, 0,1,'UTF-8');
        $result = mb_strtoupper($result,'UTF-8');
        return $result;
    }
    
    public function getEmployeePost() {
        if($this->isEmptyField('postId')){
            return null;
        }
        $postClass = new \Model\LodEntities\employeepost();
        $postClass = $postClass->getByWhere(array(
            $postClass->getIdName() => $this->getField('postId'),
        ));
        return $postClass;
    }

}