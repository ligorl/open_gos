<?php
namespace Model\LodEntities;

use Model\Gateway;

class LicenseAndCertificateOrdering  extends Gateway\LodBaseEntity{
    protected $tableName = "license_and_certificate_ordering";
    protected $idName = "id";

    /**
     * Получаем все по where с сортировкой
     * @param \Zend\Db\Sql\Where $where
     * @param string $order
     */
    public function getAllToPage($where, $order, $limit=20, $offset=0) {
        $table = $this->getGateway()->getTable();

        $select = new \Zend\Db\Sql\Select($table);

        $select->where($where)
                ->order($order)
                 ->limit($limit)
                   ->offset($offset);
                      

        $result = $this->getGateway()->selectWith($select);
        return $result;
    }
}