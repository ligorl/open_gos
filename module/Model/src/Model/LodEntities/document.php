<?php
namespace Model\LodEntities;

use Model\Gateway;

class document extends Gateway\LodBaseEntity{
    protected $tableName = "Document";
    protected $idName = "id";  
    
    public function getDocumentAndDealFromOrgId( $id){
        return $this->getDataJoin_ArrayCondition(
            [
                'id' => 'id',
                'dealId' => 'dealId',
                'documentTypeId' => 'documentTypeId',
                'documentReasonId' => 'documentReasonId',
                'registrationNumber' => 'registrationNumber',
                'registrationDate' => 'registrationDate',
                'printFormId' => 'printFormId',
                'scanCopyId' => 'scanCopyId',
                'sendDate' => 'sendDate',
                'receiveDate' => 'receiveDate',
                'signerId' => 'signerId',
                'stateCode' => 'stateCode',
                'receiveAnswerDate' => 'receiveAnswerDate',
                'sendTypeCode' => 'sendTypeCode',
                'islodId' => 'islodId',
            ],
            [
                'table' => 'Deal',
                'binder' => 'Document.dealId = Deal.id',
                'type' => 'inner',
            ],
            [
                ['column' => 'Document.documentTypeId="220e621a119846389aa7bb37709d381a"'],
                ['and'],
                ['column' => 'Deal.organizationId="'.$id.'"'],
            ]
        );
    }

    public function getAdministrativeDocumentAndDealFromOrgId( $id){
        

        return $this->getDataJoin_ArrayCondition(
            [
                'id' => 'id',
                'dealId' => 'dealId',
                'documentTypeId' => 'documentTypeId',
                'documentReasonId' => 'documentReasonId',
                'registrationNumber' => 'registrationNumber',
                'registrationDate' => 'registrationDate',
                'printFormId' => 'printFormId',
                'scanCopyId' => 'scanCopyId',
                'sendDate' => 'sendDate',
                'receiveDate' => 'receiveDate',
                'signerId' => 'signerId',
                'stateCode' => 'stateCode',
                'receiveAnswerDate' => 'receiveAnswerDate',
                'sendTypeCode' => 'sendTypeCode',
                'islodId' => 'islodId',
            ],
            [
                'table' => 'Deal',
                'binder' => 'Document.dealId = Deal.id',
                'type' => 'inner',
            ],
            [
                ['column' => 'Document.documentTypeId'],
                [
                    'in' => [
                        '"48e2fab4c6c741fbb2ef55491cff37b3"',
                        '"0b3288d1a5b747cda21b47fdd96da552"',
                        '"6c6f47d148294754b2dc7e2b55d287b3"',
                        '"560834ffb86a41afbfff8f867bfe7f5b"',
                        '"b0605404db06485db34beb2eca2a70bc"',
                        '"d79274ca72884c308a93deb460f33dc6"',
                        '"924df3ef87e74d9e9bf4c9326304987e"',
                        '"ee74d57f12114b509e22eda327e1a293"',
                        '"64ac45348f0b4b439f48b04bf257bb76"',
                        '"c1665406436e4504bcb451ade0ad90bb"',
                        '"497e2e13c24446e0b4d17a8a714bc00a"',
                        '"207e613599d44275ac006993a97f066f"'
                    ]
                ],
                ['and'],
                ['column' => 'Deal.organizationId="'.$id.'"'],
            ]
        );
    }

    public function getDocumentType() {
        if($this->isEmptyField('documentTypeId')){
            return null;
        }
        $result = new \Model\LodEntities\DocumentTypeCatalogItem();
        $result = $result->getByWhere(array(
            new \Zend\Db\Sql\Predicate\Like('id', $this->getField('documentTypeId')),
        ));
        return $result;
    }

    public function getFileScanCopy(){
        if(!empty($this->fields["scanCopyId"])){
            return $this->getLinkedItem("EiFile", array("id"=>$this->fields["scanCopyId"]));
        }
        return '';
    }

    public function getFilePrintForm(){
        return $this->getLinkedItem("EiFile", array("id"=>$this->fields["printFormId"]));
    }

    public function getDeal() {
        if($this->isEmptyField('dealId')){
            return null;
        }
        $deal = new \Model\LodEntities\Deal();
        $deal = $deal->getByWhere(array($deal->getIdName() => $this->getFieldSafe('dealId')));
        return $deal;
    }

    public function getSignatories() {
        if($this->isEmptyField('signerId')){
            return null;
        }
        $result = new \Model\LodEntities\signatories();
        $result = $result->getByWhere(array($result->getIdName() => $this->getField('signerId')));
        return $result;
    }
    /**
     * пересоздает печатный документ
     * и впихивает его в файлохранилище
     * 
     * @return $fileBank - запись в файло хранилище
     */
    public function remakePrintForm() {
        if($this->isEmptyField($this->getIdName())){
            return null;
        }
        $docType = $this->getField('documentTypeId');
        $fileBank = false;
        if(!empty($docType)){
            //$notifyTypeArray = \Islod\Model\Notify\Notify::$typeNotify;
            //$notifyTypeArray = array_filter($notifyTypeArray);
            if( /* in_array($docType, $notifyTypeArray)*/1){
                //формируем документ
                $docId = $this->getId();
                $documentClass = \Islod\Model\Notify\Notify::fabricNotify($this);
                $code = 'Document';
                $dealId = $this->getField('dealId');
                if(!empty($documentClass)){
                    //впихиваем его в хранилище
                    $content = $documentClass->makeDocument();
                    if(empty($content)){
                        throw new \Exception('лод - документы - формирование документа - пполучился пустое содержимое');
                    }
                    $fileName = $documentClass->getFileOutName();
                    @$fileBank = new \Model\LodEntities\EiFile();
                    $resultSend = $fileBank->updateByCodeIdContent($dealId,  $code, $docId, $fileName, $content );
                    $this->setField("printFormId",$fileBank->getId());
                    $res = $this->save();
                }
            }
        }
        return $fileBank;
    }
    /**
     * Получаем signatureLevelCode из DocumentTypeCatalogItem
     * @return boolean or int если все норм то INT если нет кода то false
     */
    public function getSignatureLevelCode(){
        $documentType = $this->getDocumentType();
        if($documentType){
            return $documentType->getField("signatureLevelCode");
        }else{
            return false;
        }
    }
    
    public function getButtonsByDocument(){
        $status = $this->getField("stateCode");
        $signatureCode = $this->getSignatureLevelCode();
        if($status && $signatureCode ){
            return $this->getLinkedItems("DocumentStatusButton", array("CurrentStatus"=>$status,"SignatureLevelCode LIKE '%".$signatureCode."%'"));
        }else{
            return false;
        }
    }
    
    /**
     * Меняем статус документа
     * @param str $newState - новый статус
     * @return boolean
     */
    public function changeStatus($newState){
        $this->setField("stateCode",$newState);
        if($this->save()){
            //если успешно, то отправим письмо
            if('REGISTERED' == $newState){
                //если это уведомление
                $docType = $this->getDocumentType();
                if(!empty($docType)){
                    $parentId  = $docType->getFieldSafe('parent');
                    $docTypeId = $docType->getId();
                    if(!empty($parentId) && '20a4674be28c4b36ad69de14a0e4c8df' == $parentId ){
                        \Islod\Model\Mail\NotifyRegistre::send($this);
                    }
                    if(  in_array($docTypeId, DocumentTypeCatalogItem::$DocumentTypeOrderPositivIdArray)
                      || in_array($docTypeId, DocumentTypeCatalogItem::$DocumentTypeOrderNegativIdArray)     
                    ){
                        \Islod\Model\Mail\OrderRegistre::send($this); 
                    }                    
                }
            }
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Меняем статус документа
     * @param str $newState - новый статус
     * @return boolean
     */
    public function setRegNumberNotify(){
        $Type =$this->getDocumentType();
        $deal = $this->getLinkedItem("Deal", array("id"=>$this->fields["dealId"]));
        if($deal && $Type){
            if($Type->getField("parent")=="20a4674be28c4b36ad69de14a0e4c8df"){
                $this->setField("registrationNumber","06-".$deal->getField("number"));
                $this->save();
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public function getDocumentFile() {
        if( $this->isEmptyField($this->idName) ){
            return false;
        }
        $fileClass = new EiFile();
        $fileClass = $fileClass->getByWhere(array(
            'ownerId' => $this->getId(),
            'ownerClassCode' => 'Document',
        ));
        return $fileClass;
    }

}
