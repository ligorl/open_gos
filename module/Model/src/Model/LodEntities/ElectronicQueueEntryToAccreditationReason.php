<?php

namespace Model\LodEntities;

use Model\Entities\eiisEducationalOrganizations;
use Model\Gateway\LodBaseEntity;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\Predicate;
use Zend\Db\Sql\Where;

class ElectronicQueueEntryToAccreditationReason extends LodBaseEntity
{
    protected $tableName = 'ElectronicQueueEntryToAccreditationReason';
    protected $idName = 'id';
}