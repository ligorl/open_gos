<?php
namespace Model\LodEntities;

use Model\Gateway;

class EducationLevel extends Gateway\LodBaseEntity{
    protected $tableName = "EducationLevel";
    protected $idName = "id";
}