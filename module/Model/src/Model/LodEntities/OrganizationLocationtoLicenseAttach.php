<?php
namespace Model\LodEntities;

use Model\Gateway;

class OrganizationLocationtoLicenseAttach extends Gateway\LodBaseEntity{
    protected $tableName = "OrganizationLocationToLicenseAttach";
    protected $idName = "id";
    
    public function getAdressesBySupplId($supplIds){
        $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
        if (!empty($supplIds)) {
            $sql          = new \Zend\Db\Sql\Sql($dbAdapter);
            $select       = $sql->select();
            $select->from('OrganizationLocationToLicenseAttach')
                    ->columns(array())
                    ->where(array("OrganizationLocationToLicenseAttach.licenseAttachId" => $supplIds))
                    ->join("OrganizationLocation", "OrganizationLocationToLicenseAttach.organizationLocationId = OrganizationLocation.id", array("id", "postcode", "address", "addressType"));
            $selectString = $sql->getSqlStringForSqlObject($select);
            $results      = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
            return $results->toArray();
        }else{
            return array();
        }
    }
}