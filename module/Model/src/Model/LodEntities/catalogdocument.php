<?php
namespace Model\LodEntities;

use Model\Gateway;

class catalogdocument extends Gateway\LodBaseEntity{
    protected $tableName = "catalog_document";
    protected $idName = "id";
}