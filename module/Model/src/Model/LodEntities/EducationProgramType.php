<?php
namespace Model\LodEntities;

use Model\Gateway;

class EducationProgramType extends Gateway\LodBaseEntity{
    protected $tableName = "EducationProgramType";
    protected $idName = "id";
}