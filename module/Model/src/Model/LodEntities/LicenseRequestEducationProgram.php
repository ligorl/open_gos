<?php
namespace Model\LodEntities;

use Model\Entities\eiisEduLevels;
use Model\Entities\eiisEduPrograms;
use Model\Entities\eiisEduProgramTypes;
use Model\Entities\isgaQualifications;
use Model\Gateway;

class LicenseRequestEducationProgram extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseRequestEducationProgram";
    protected $idName = "id";

    static public function programExists($requestOrganizationInfoId,$educationProgramCatalogItemId){
        $LicenseRequestEducationProgramTable = new Gateway\LodEntitiesTable("LicenseRequestEducationProgram");
        $results = $LicenseRequestEducationProgramTable->getEntitiesByWhere([
            "requestOrganizationInfoId" => $requestOrganizationInfoId,
            "educationProgramCatalogItemId" => $educationProgramCatalogItemId
        ])->getObjectsArray();
        if(count($results) > 0)
            return true;
        return false;
    }

    public function getEduProgram(){
        if($this->isEmptyField("educationProgramCatalogItemId"))
            return null;

        $epCatalogItemTable = new Gateway\EntitiesTable("eiis_EduPrograms");
        $epCatalogItem = $epCatalogItemTable->getAllEntities(null,null,array(
            "Id" => $this->getField("educationProgramCatalogItemId")
        ))->getObjectsArray();

        if(count($epCatalogItem)>0)
        return $epCatalogItem[0];
    }

    public function getEduLevel(){
        $eduLevel = new eiisEduLevels($this->getField("educationLevelId"));
        return $eduLevel;
    }
    
    public function getEduProgramType() {
        $result = null;
        if(!$this->isEmptyField('eduProgramTypeId')){            
            $resultClass = new eiisEduProgramTypes();
            $result = $resultClass->getById( $this->getField("eduProgramTypeId"));           
        }
        return $result;
        
    }

    public function getProgramTypeName(){
        $pType = new eiisEduProgramTypes($this->getField("eduProgramTypeId"));
        if($pType!=null)
            return $pType->getField("Name");

        return "";
    }

    public function getQualifictionsNames(){
        //��� ������������
        $mtmQualTable = new Gateway\LodEntitiesTable("LicenseRequestEducationProgramToQualification");
        $mtmQualifications = $mtmQualTable->getAllEntities(null,null,[
            "licenseRequestEducationProgramId" => $this->getId()
        ]);

        $qualIds = [];
        foreach($mtmQualifications as $qual){
            $qualIds[] = $qual->getField("qualificationId");
        }

        //������� ������������
        $qualifications = [];
        if(count($qualIds)>0) {
            $qualTable = new Gateway\EntitiesTable("isga_Qualifications");
            $qualifications = $qualTable->getAllEntities(null, null, [
                new \Zend\Db\Sql\Predicate\In("Id", $qualIds)
            ]);
        }

        $qualNames = [];
        foreach($qualifications as $qual){
            $qualNames[] = $qual->getField("Name");
        }
        return implode(", ",$qualNames);
    }

    private $_program = null;
    public function getProgram(){
        if($this->_program != null)
            return $this->_program;

        $program= new eiisEduPrograms($this->getField("educationProgramCatalogItemId"));
        if($program!=null)
            $this->_program = $program;
        else
            $this->_program = new eiisEduPrograms();

        return $this->_program;
    }
}