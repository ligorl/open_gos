<?php

namespace Model\LodEntities;

use Model\Gateway;

/**
 * файлохранилище
 */
class EiFile extends Gateway\LodBaseEntity {

    protected $tableName = "EiFile";
    protected $idName    = "id";

    static public $ownerClassCodeToTable = array(
                        //документ
                        'Document' => array('db' => 'lod', 'table' => 'Document'),
                        //документы распечатанный ???
                        'DocumentPrintedForms' => array('db' => 'lod',' table' => 'DocumentPrintedForms'),

                        //скан лицензии
                        'License'           => array('db' => 'f11', 'table' => 'eiis_Licenses'),
                        //макет лицензии
                        'LicensePrint'      => array('db' => 'f11', 'table' => 'eiis_Licenses'),
                        //макет лицензии, подготовленный к подписанию
                        'LicenseMaket'      => array('db' => 'f11', 'table' => 'eiis_Licenses'),
                        //документ электронной лицензии, подписанный
                        'LicenseDocument'   => array('db' => 'f11', 'table' => 'eiis_Licenses'),
                        //подись документа
                        'LicenseSign'       => array('db' => 'f11', 'table' => 'eiis_Licenses'),
                        //ХМЛ файл лицензии
                        'LicenseXml'        => array('db' => 'f11', 'table' => 'eiis_Licenses'),

                        //скан приложения
                        'LicenseAttach'     => array('db' => 'f11', 'table' => 'eiis_LicenseSupplements'),
                        //печатный макет прложения
                        'SupplementPrint'   => array('db' => 'f11', 'table' => 'eiis_LicenseSupplements'),
                        //макет приложения для печати, подготовленный к подписанию
                        'SupplementDocument'=> array('db' => 'f11', 'table' => 'eiis_LicenseSupplements'),
                        //макет прложения для электронного подписания
                        'SupplementMaket'   => array('db' => 'f11', 'table' => 'eiis_LicenseSupplements'),
                        //подись приложения
                        'SupplementSign'    => array('db' => 'f11', 'table' => 'eiis_LicenseSupplements'),
                        //ХМЛ файл приложения
                        'SupplementXml'     => array('db' => 'f11', 'table' => 'eiis_LicenseSupplements'),

                        //макет заявления
                        'RequestMaket'      => array('db' => 'lod', 'table' => 'LicenseRequest'),
                        //скан заявления
                        'RequestScan'       => array('db' => 'lod', 'table' => 'LicenseRequest'),
                        //заявление подписаное электронной подписью
                        //теперь их может быть много думаю курить их под датам
                        'RequestDocument'   => array('db' => 'lod', 'table' => 'LicenseRequest'),
                        //электронная подпись документа
                        //или это теперь их может быть много думаю курить их под датам
                        'RequestSign'       => array('db' => 'lod', 'table' => 'LicenseRequest'),
        
                        //скан платежного поручения
                        'LicenseRequestPayment' => array('db' => 'lod', 'table' => 'LicenseRequestPayment'),
        
                        //Файлы с заявлением
                        'RequestFile' => array('db' => 'lod', 'table' => 'LicenseRequestDocument'),
                    );
    /**
     * Ведерним все файлы по коду и ид
     *
     * @param type $code
     * @param type $id
     * @return type
     */
    public function getFileAllByCodeId($code, $id) {
        if(empty($code) || empty($id)){
            return null;
        }
        $result = $this->getAllByWhereParseId(array(
            'ownerClassCode' => $code,
            'ownerId'        => $id,
        ));
        return $result;
    }

    /**
     * выдерним первый попавшийся файл по коду и ид
     *
     * @param type $code
     * @param type $ownerId
     * @return type
     */
    public function getFileByCodeId($code, $ownerId) {
        if(empty($code) || empty($ownerId)){
            return null;
        }
        $result = $this->getByWhere(array(
            'ownerClassCode' => $code,
            'ownerId'        => $ownerId,
        ));
        return $result;
    }

    /**
     * Изменяем или добавляем файл в файлохранилище
     *
     * @param string $dealId    - номер дела, можно без
     * @param type $code        - код документа(или имя таблички с которой связывается)
     * @param type $ownerId     - ид в табличке(кодом) с которой связан файл
     * @param type $fileName    - истинное имя файла
     * @param type $content     - содержание файла
     * @param      $tmpFileName - если сохранять через сохранялку со закинуть сюда временный файл
     * @param type $editItId    - если труе, то тупо создаем новую запись, если ид, то редактим эту запись
     * 
     * @return type             - если все путем то путь, куда это дело сохранится, без /public/uploads
     * @throws \Exception       - какие-то неполадки
     */
    public function updateByCodeIdContent(
            $dealId = null,     $code,      $ownerId,       $fileName, 
            &$content,          $tmpFileName = null,        $editItId = null 
    ){
        $prefix = 'Банк файлов - запись файлов: ';
        if(empty($code) || empty($ownerId) || (empty($content) && empty($tmpFileName)) ||  empty($fileName) ){
            throw new \Exception($prefix.'отсутстувую данные');
        }
        
        if( !empty($dealId) && $dealId instanceof Deal){
            $dealId = $dealId->getId();
        }
        if(empty($dealId)){
            $dealId = 'all';
        }
        
        $isNew = false;     
        
        //танцы вокруг повторного вклинивания
        if( empty($editItId) ){
            $file = $this->getFileByCodeId($code, $ownerId);
        }
        else{
            if( true === $editItId ){
                
            }
            else{
                $file = $this->getByWhere(array($this->getIdName() => $editItId) );
                if( empty($file) ){
                    throw new \Exception($prefix.'не найдена запись о файле для редактирования');
                }
            }
        }
        if(empty($file)){
            $file = new self;
            $file->setField('ownerId', $ownerId);
            $file->setField('ownerClassCode', $code);
            $file->setField('withFileName', 1);
            $isNew = true;
        }
        //определимся с именами путей и файлов
        $ext = '.'.substr(strrchr($fileName, '.'), 1);
        if( 4 != strlen( $ext ) ){
            $ext = '';
        }
        //если файл подписи, то ему в случае есть оставляем двойное расширение
        if( '.sig' == $ext ){
            //вырежим имя файла без расширения подписи
            $fileNameSig = substr( $fileName , 0, strlen( $fileName )-4 );
            //второе расширение
            $ext2 = '.'.substr(strrchr( $fileNameSig , '.'), 1);
            if( 4 == strlen( $ext ) ){
                $ext = $ext2.$ext;
            }            
        }
        $fileHashName = md5(\Eo\Controller\DeclarationController::translit('', $fileName . '_' . md5(microtime(true).  rand(0, 1000).rand(0, 1000).rand(0, 1000)))) . $ext;
        $shortPathDir = 'synch/'.date('Y').'_lod/'.$dealId.'/'.$code.'/';

            //зазачиваем на открытую часть
            $SendFileClass  = new \Eo\Model\SendFile();
            //временный файл, чтоб перекачать на открытую чатсь
            if(empty($tmpFileName)){
                $tmpFileName = tempnam(sys_get_temp_dir(),'lod_templater_');
            }
            if(empty($tmpFileName)){
                throw new \Exception($prefix.'не смогло созадать временый файл в '.sys_get_temp_dir());
            }
            if(!empty($content) && !file_put_contents($tmpFileName, $content)){
                throw new \Exception($prefix.'не смогло сохнарить временый файл');
            }
            

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_file($finfo, $tmpFileName);
            finfo_close($finfo);
            
            $fileSize = filesize($tmpFileName);
            
            $resultSendFile = $SendFileClass->SendFileToOpenServer(
                    array(
                        'error' => 0,
                        'name' => $fileHashName,
                        'size' => $fileSize,
                        'type' => 'application/octet-stream',
                        'tmp_name' =>$tmpFileName,
                    ),
                    array(
                        "path" => '/public/uploads/'.$shortPathDir,
                        "name" => $fileHashName,
                        'isChange' => 1,
                    )
            );
            if($resultSendFile !=1 /*!file_put_contents($path, $content)*/){
                throw new \Exception($prefix.'не смогло сохранить '.$shortPathDir.$fileHashName.',  результат сохранения: '.$resultSendFile);
            }
            $created = $file->getDate('created');
            if(empty($created)){
                $file->setField('created', date('Y-m-d'));
            }
            $crc = $hash = hash_file('crc32b', $tmpFileName );
            $size = $fileSize;
            //$file->setField('ownerClassCode', $code);
            $file->setField('ownerClassCode', $code);
            $file->setField('filename', $fileName);
            $file->setField('size', $size);
            $file->setField('crc',$crc);
            $file->setField('mimeType', $mimeType);
            $file->setField('sourceUrl', $shortPathDir.$fileHashName);
            $file->save($isNew);
            //мутка, чтоб текущий объект стал актуальным
            $fields = $file->getFields();
            $this ->setFields($fields);
            //удалим временный файл
            unlink($tmpFileName);
            return $shortPathDir.$fileHashName;
    }

    public function getUrl() {
        if( !$this->isEmptyField('sourceUrl')){
            return '/uploads/'.$this->getFieldSafe('sourceUrl');
        }else{
            return false;
        }
    }
    
    public function getFileName() {
        return $this->getFieldSafe('filename');
    }
    
    public function getPathOnServer() {
        if( !$this->isEmptyField('sourceUrl')){
            return $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$this->getFieldSafe('sourceUrl');
        }else{
            return false;
        }
    }

    private $_chekSignResult = array();
    /**
     * Проверяет подпись документа файла подписи
     *
     * @return string   - "Ok" -Все путем, прочее - что не так
     * @throws \Exception
     */
    public function checkSign( $isExFunction = false ) {
        if( $this->isEmptyField($this->getIdName()) ){
            return 'заявление не выбрано';
        }
        try{
            $result = 'не установленная ошибка';
            $signFile = $this;//->getSigntFile();

            if( empty($signFile) ){
                throw new \Exception('Не загружен файл подписи');
            }
            $sign_class = new \Eo\Service\Sign(); //класс по работе с сервером провверки подписи
            //проверка является ли файл подписанным SIG, а также проверка имени файла
            //$result_verify может содержать код ошибки или true, если всё верно
            //$result_verify = $sign_class->verify_name_signfile($name_info_file);
            $result_verify = true;
            if ($result_verify === true){
                $logFile = 'public/log/signCheck.log';
                $mailClas = new \Eo\Model\SendMail();
                $mailClas->mailLog('Начинаем проверку подписи у лицензии '.$this->getId(), $logFile);

                //создаем хэшированое имя например имя загружаемого файла certificat.docx.sig, а мы получаем хэшированное jfshgjbgajgb4df.docx.sig
                //$name_info_file_hash = $sign_class->get_hash_name($name_info_file);
                //$name_info_file_hash = $signFile->getPathOnServer();
                $name_info_file_hash = $signFile->getFieldSafe('sourceUrl');
                $mailClas->mailLog('путь к файлу '.$name_info_file_hash, $logFile);

                    //отправка подписанного файла на сервер проверки подписи  (СПП)
                    //предварительно файл нужно сохранить в папке /public/uploads/
                    //$sign_data будет содержать информацию о сертификате и валидности файла
                    //$sign_data['status'] = 'ok' - файл отправлен на СПП и пришел какой-то ответ,
                    //$sign_data['status'] = 'DSS_dont_answer' - СПП недоступен или не отвечает
                    //$sign_data['result'] = true - подпись валидна, false - подпись невалидна
                    //$sign_data['message'] содержит сообщение о невалидности подписи (что именно не так), если подпись валидна - сообщение пустое

                    $this->_chekSignResult = array();
                    //заклин, чтоб можно было пройти далее
                    $host = $_SERVER['HTTP_HOST'];
                    //if( 'f11.' == substr($host, 0, 4) || $host =='85.192.47.4'){
                    //    $sign_data['status'] = 'ok';
                    //}
                    //else{
                    //    $sign_data = $sign_class->valid_signature($name_info_file_hash);
                    //}
                    $this->_chekSignResult = $sign_class->validSignature( $name_info_file_hash ,true , false , $isExFunction  );

                    $mailClas->mailLog('ответ: '.  print_r($this->_chekSignResult, true), $logFile);

                    if ( !empty($this->_chekSignResult['status']) && $this->_chekSignResult['status'] == 'ok'){
                        $result = 'Ok';
                    }
                    else{
                        if( empty($this->_chekSignResult['result']) ){
                            $this->_chekSignResult['result'] = 'неопределенная ошибка';
                        }
                        //получение текста ошибки по коду
                        $error = $sign_class->getErrorText($this->_chekSignResult['result']);
                        throw new \Exception($error);
                    }
            }
            else{
                //получение текста ошибки по коду
                $error = $sign_class->getErrorText($result_verify);
                throw new \Exception($error);
            }
        }
        catch (\Exception $e) {
            $result = $e->getMessage();
        }
        return $result;
    }
    
    /**
     * <pre>
     * вернет результат последней проверки подписи
     *  Array
     *  (
     *      [status] => ok
     *      [result] => 1
     *      [message] => 
     *      [certificate_info] => Array
     *          (
     *              [SubjectName] => Array
     *                  (
     *                      [SNILS] => 03949214377
     *                      [OGRN] => 1033801008218
     *                      [STREET] => ул Карла Маркса, дом 1
     *                      [G] => Александр Валерьевич
     *                      [SN] => Аргучинцев
     *                      [T] => Ректор
     *                      [CN] => ФГБОУ ВО
     *                      [E] => t_s_l@home.isu.ru
     *                      [INN] => 003808013278, РНС ФСС=3802000149
     *                  )
     *
     *              [IssuerName] => CN="УЦ1 ЗАО ""ПФ ""СКБ Контур""", O="ЗАО ""ПФ ""СКБ Контур""", OU=Удостоверяющий центр, STREET=Пр. Космонавтов д. 56, L=Екатеринбург, S=66 Свердловская область, C=RU, ИНН=006663003127, ОГРН=1026605606620, E=ca@skbkontur.ru
     *              [NotBefore] => 2017-04-11T05:52:01
     *              [NotAfter] => 2018-07-11T05:52:01
     *              [SerialNumber] => 00F349E07AC40CC780E711631ED492FC22
     *              [Thumbprint] => BD84FCA0E83B71B3FDF61B4F16E76B0D96216E74
     *          )
     *
     *  )     
     * </pre>
     * @return type
     */
    public function checkSignLastResult(){
        return $this->_chekSignResult;
    }
    
}