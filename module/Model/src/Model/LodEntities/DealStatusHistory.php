<?php

namespace Model\LodEntities;

use Model\Gateway;

class DealStatusHistory extends Gateway\LodBaseEntity
{

    protected $tableName = "DealStatusHistory";
    protected $idName = "Id";

    public function getNameUser()
    {
        $User = $this->getLinkedItemIsga("ron_Users", array("id" => $this->fields["IdUser"]));
        if ($User) {
            return $User->getField("Name");
        } else {
            return "";
        }
    }

    public function getOldStatusName()
    {
        $Status = $this->getLinkedItem("catalog_deal", array("origin_name" => $this->fields["oldStatus"]));
        if ($Status) {
            return $Status->getField("russian_name");
        } else {
            return "";
        }
    }

    public function getNewStatusName()
    {
        $Status = $this->getLinkedItem("catalog_deal", array("origin_name" => $this->fields["newStatus"]));
        if ($Status) {
            return $Status->getField("russian_name");
        } else {
            return "";
        }
    }

    /**
     * Возвращаем дату получения ответа для дела, если таковой нет - null
     * @return string|null
     */
    public function getReceiveAnswerDate()
    {
        $dealId = $this->getField('IdDeal');
        $documentTable = new \Model\LodEntities\document();
        /* @var $document \Model\LodEntities\document */
        $document = $documentTable->getByWhere([
            'dealId'    => $dealId,
            'stateCode' => 'RECEIVED_ANSWER',
        ]);
        return $document instanceof \Model\LodEntities\document ? $document->getDate('receiveAnswerDate') : null;
    }

}