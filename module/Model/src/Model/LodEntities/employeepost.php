<?php
namespace Model\LodEntities;

use Model\Gateway;

class employeepost extends Gateway\LodBaseEntity{
    protected $tableName = "EmployeePost";
    protected $idName = "id";

    public function getWhereIds(array $idArray){
        return $this->getData_ArrayCondition([
            [ 'column' => 'id' ],
            [ 'in' => $idArray ]
        ]);
    }
    
    /**
     * наименование в родительном падеже
     * @return type
     */
    public function getTitleGen() {
        $result = '';
        $result = $this->getFieldSafe('descriptionInDative');
        if( empty($result) ){
            $result = $this->getFieldOrSpace('title');
            if(!empty($result)){
                $morphy = new \Islod\Model\Tools\Morphy();
                //вернет четыре формы
                $fourForm = $morphy->getForms($result, false);
            }
            if(!empty($fourForm['genitive'])){
                $result = $fourForm['genitive'];
            }
        }
        return $result;
    }
}
