<?php
/**
 * Created by PhpStorm.
 * User: hagri
 * Date: 10.11.2017
 * Time: 16:22
 */

namespace Model\LodEntities;

use Model\Gateway\LodBaseEntity;

class ElectronicQueueEntrySlot extends LodBaseEntity
{
    protected $tableName = 'ElectronicQueueEntrySlots';
    protected $idName = 'id';

    /**
     * Проверяет на наличие записи в промежуток времени.
     *
     * @param \DateTime $from
     * @param \DateTime $to
     * @return \Model\Gateway\type
     */
    public function getBySlot($from, $to)
    {
        $entities = $this->getByWhere([
            'slot > "' . $from->format("Y-m-d H:i:s") . '"',
            'slot < "' . $to->format("Y-m-d H:i:s") . '"',
        ]);

        return $entities;
    }
}