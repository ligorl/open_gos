<?php
namespace Model\LodEntities;

use Model\Gateway;

class Calendar extends Gateway\LodBaseEntity{
    protected $tableName = "Calendar";
    protected $idName = "id";

    public function checkPeriod($dateStart=null,$dateEnd=null)
    {
        $dateCondition = [];

        if(!empty($dateStart)){
            $dateCondition[] = ['column'=>'`date`>="'.$dateStart.'"'];
        }

        if(!empty($dateEnd)){
            $dateCondition[] = ['column'=>'`date`<="'.$dateEnd.'"'];
        }

        $proocDate = [];
        $count = 0;

        foreach($dateCondition as $condition ){
            if($count>0){
                $proocDate[] = ['and'];
            }
            $proocDate[] = $condition;
            $count++;
        }
        return $this->getData_ArrayCondition($proocDate);
    }
}