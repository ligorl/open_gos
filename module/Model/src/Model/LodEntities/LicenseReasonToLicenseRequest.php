<?php
namespace Model\LodEntities;

use Model\Gateway;

class LicenseReasonToLicenseRequest extends Gateway\LodBaseEntity{
    protected $tableName = "LicenseReasonToLicenseRequest";
    protected $idName = "id";

    public function getNameCause(){
        $State = $this->getLinkedItem("LicenseReasonCatalogItem", array("id"=>$this->fields["licenseReasonId"]));
        return $State->getField("title");
    }
}