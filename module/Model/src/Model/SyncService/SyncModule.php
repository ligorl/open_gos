<?php
namespace Model\SyncService;
use Model\Entities\isgaExchangeTable;
use Zend\Log\Logger;

/**
 * Класс для синхронизации между кабинетами
 *
 * Class SyncModule
 * @package Model\SyncService
 */
class SyncModule{
    private $lodClassNames = [
        "statushistory" => "StatusHistory",
        "Document" => "document",
        "OrganizationLocationToLicenseAttach" => "OrganizationLocationtoLicenseAttach",
        "isloduserrole" => "IslodUserRole",
        "licenseandcertificateordering" => "LicenseAndCertificateOrdering"
    ];

    //В массиве будут храниться айди экспертов, что обработались
    private $expertIds = [];


    private function getClassName($name){
        if(isset($this->lodClassNames[$name]))
            return $this->lodClassNames[$name];
        else
            return $name;
    }

    /**
     * Метод генерирует пароль для соединения с кабинетом вуза
     */
    private function generatePass(){
        //Берем текущую дату , соединяем с строкой и кодируем в md5
        $date = new \DateTime();
        $currentDate = $date->format("d.m.Y");
        $pass = md5($currentDate."passSalt567");
        return $pass;
    }

    public function checkPass($pass){
        $ourPass = $this->generatePass();
        if($pass == $ourPass)
            return true;
        else
            return false;
    }

    /**
     * Метод парсит запрос кабинета рона
     * @param $data
     */
    public function parseRequest($data){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $requestType = $data->requestType;
        switch($requestType){
            //Пинг работает ли кабинет вуза
            case "ping":
                echo "ping result";
                break;

            case "getNews":
                //Дергаем метод что отвечает за обработку этого случая
                $this->getNewsAnswer("general");
                break;

            case "expertsGetNews":
                //Дергаем метод что отвечает за обработку этого случая
                $this->getNewsAnswer("experts");
                break;

            case "lodGetNews":
                //Дергаем метод что отвечает за обработку этого случая
                $this->getNewsAnswer("lod");
                break;

            case "queueGetNews":
                //Дергаем метод что отвечает за обработку этого случая
                $this->getNewsAnswer("queue");
                break;

            case "parseNews":
                $this->parseNewsHandler($data, "general");
                break;

            case "parseExpertsNews":
                $this->parseNewsHandler($data, "experts");
                break;

            case "parseLodNews":
                $this->parseNewsHandler($data, "lod");
                break;
        }
    }

    private function parseNewsHandler($data, $type){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        $debug = false;
        $typeForDebug = "experts";
        try {
if($debug){
    $logger = new \Model\Logger\Logger("sync_vuz",true);
    $logger->Log("=========");
    $logger->Log("type:".$type);
    $logger->Log("objects raw:");
    $logger->Log(print_r($data, true));
}
            $data = unserialize($data->data);

if ($debug && $type == $typeForDebug) {
    $logger->Log("===========");
    //$logger->Log("data:".print_r($data->data,true));
    $logger->Log("===========");
    $logger->Log("objectsForVuziCab:".count($data["objectsForVuziCab"]));
    $logger->Log("objectsFromVuziDone:".count($data["objectsFromVuziDone"]));
    $logger->Log("objectsFromVuziFault:".count($data["objectsFromVuziFault"]));
}

            //Чистим в таблице изменений все записи что были отработаны
            if ($type == "general")
                $exchangeTable = new \Model\Gateway\EntitiesTable("isga_ExchangeTable");
            else if ($type == "experts")
                $exchangeTable = new \Model\Gateway\ExpertsEntitiesTable("isga_ExchangeTable");
            else if ($type == "lod" || $type == "queue")
                $exchangeTable = new \Model\Gateway\LodEntitiesTable("isga_ExchangeTable");

            $gateway = $exchangeTable->getGateway();
if ($debug && $type == $typeForDebug) $logger->Log("Сколько объектов обработано там и которых необходимо удалить тут: ".count($data["objectsFromVuziDone"]));
            $delCount = 0;


            if(isset($data["objectsFromVuziDone"]))
            foreach ($data["objectsFromVuziDone"] as $objDone) {
                $delCount++;

                $gateway->delete(array(
                    "ObjectId" => $objDone["ObjectId"],
                    "TableName" => $objDone["TableName"],
                    new \Zend\Db\Sql\Predicate\NotLike("SaveType", "delete")
                ));
if ($debug && $type == $typeForDebug && $delCount%100 ==0) $logger->Log("delCount: ".$delCount);
            }
            if(isset($data["objectsFromVuziDelete"]))
            foreach ($data["objectsFromVuziDelete"] as $objDone) {
                $delCount++;

                $gateway->delete(array(
                    "ObjectId" => $objDone["ObjectId"],
                    "TableName" => $objDone["TableName"]
                ));

            }
if ($debug && $type == $typeForDebug) $logger->Log("delCount: ".$delCount);

            //Для всех записей, обработка которых привела к ошибке помечаем статус что ошибка и описание ошибки
if ($debug && $type == $typeForDebug) $logger->Log("Сколько объектов обработано с ошибкой: ".count($data["objectsFromVuziFault"]));
            $errorCount = 0;
            if(isset($data["objectsFromVuziFault"]))
            foreach ($data["objectsFromVuziFault"] as $faultObj) {
                $exportRow = new isgaExchangeTable($faultObj["Id"]);
                if ($exportRow->getId() != null) {
                    $exportRow->setField("State", "Error");
                    $exportRow->setField("Error", $faultObj["Error"]);
                    $exportRow->save(false, true);
                    $errorCount++;
                }
            }
if ($debug && $typeForDebug) $logger->Log("errorCount: ".$errorCount);

            //--
            $objectsByType = $data["objectsForVuziCab"];
            $objectsFromRonDone = array(); //Объекты из каб рона что были обработаны и могут быть удалены
            $objectsFromRonFault = array(); //Объекты из каб рона что не были обработаны и не должны удаляться
            $objectsFromRonDelete = array(); //Объекты из каб рона что удалены

            $newObjs = 0;


            if ($type == "general")
                $exchangeTable = new \Model\Gateway\EntitiesTable("isga_ExchangeTable");
            else if ($type == "experts")
                $exchangeTable = new \Model\Gateway\ExpertsEntitiesTable("isga_ExchangeTable");
            else if ($type == "lod" || $type == "queue")
                $exchangeTable = new \Model\Gateway\LodEntitiesTable("isga_ExchangeTable");

            if(isset($data["objectsForVuziCab"]))
            foreach ($objectsByType as $tableName => $objects) {

                //Собираем имя класса
                $classNameParts = explode("_", $tableName);
                $className = implode("", $classNameParts);

                if ($type == "general")
                    $entityClassName = "Model\\Entities\\" . $className;
                else if ($type == "experts")
                    $entityClassName = "Model\\ExpertEntities\\" . $className;
                else if ($type == "lod" || $type == "queue") {
                    $className = $this->getClassName($className);
                    $entityClassName = "Model\\LodEntities\\" . $className;
                }

                if (!class_exists($entityClassName)) {
                    foreach ($objects as $id => $object) {
                        $objectsFromRonDone[] = array(
                            "ObjectId" => $object["ObjectId"],
                            "TableName" => $object["TableName"]
                        );
                    }
                    continue;
                }


                foreach ($objects as $object) {
                    //если эксперты, добавляем в список id
                    if($object["TableName"] == "expert"){
                        $this->expertIds[] = $object["ObjectId"];
                    }
                    $newObjs++;
                    //получаем/создаем объект и сохраняем в бд
                    $entity = new $entityClassName($object["ObjectId"]);
                    //Определяем новый объект или нет
                    $isNew = false;
                    if ($entity->getId() == null) {
                        $isNew = true;
                    }


                    try {
                        //Добавлена обработка операции по удалению
                        if ($object["SaveType"] == "delete") {
                            if (!$isNew)
                                $entity->simpleDelete();
                            $objectsFromRonDelete[] = array(
                                "ObjectId" => $object["ObjectId"],
                                "TableName" => $object["TableName"]
                            );
                            $gateway = $exchangeTable->getGateway();

                            $gateway->delete(array(
                                "ObjectId" => $object["ObjectId"],
                                "TableName" => $object["TableName"]
                            ));
                        } else {
                            if ($object["TableName"] == "isga_EducationalOrganizationData")
                                $entity->setFields($object["fields"], $isNew, true);
                            else
                                $entity->setFields($object["fields"], $isNew);
                            //
                            $entity->save($isNew, true);
                            $objectsFromRonDone[] = array(
                                "ObjectId" => $object["ObjectId"],
                                "TableName" => $object["TableName"]
                            );
                            $gateway = $exchangeTable->getGateway();

                            $gateway->delete(array(
                                "ObjectId" => $object["ObjectId"],
                                "TableName" => $object["TableName"],
                                new \Zend\Db\Sql\Predicate\NotLike("SaveType", "delete")
                            ));
                        }
                    } catch (\Exception $e) {
                        $objectsFromRonFault[] = array(
                            "Id" => $object["Id"],
                            "ObjectId" => $object["ObjectId"],
                            "Error" => $object["TableName"] . ": " . $e->getMessage()
                        );
                    }
                }
            }

if ($debug && $type == $typeForDebug) $logger->Log("Сколько объектов было обработано по факту, проход по ним был: ".$newObjs);
            $returnData = array(
                "state" => "allDone",
                "objectsFromRonDone" => $objectsFromRonDone,
                "objectsFromRonFault" => $objectsFromRonFault,
                "objectsFromRonDelete" => $objectsFromRonDelete
            );
if ($debug && $type == $typeForDebug) $logger->Log("objectsFromRonDone: ".count($objectsFromRonDone));
if ($debug && $type == $typeForDebug) $logger->Log("objectsFromRonFault: ".count($objectsFromRonFault));
            $ser = serialize($returnData);
            echo $ser;
        } catch(\Exception $e){
            $logger = new \Model\Logger\Logger("exchange", true);
            $logger->Log($e->getLine()." - ".$e->getMessage());
        }
        //инициируем отправку экспертов
        $this->startExpertExchange();
    }

    private function startExpertExchange(){
        if(count($this->expertIds) > 0) {
            $curl = curl_init();
            $ids = implode(",", $this->expertIds);
            $destination_url = "http://" . $_SERVER['SERVER_NAME'] . "/service/index/expert-exchange?id=" . $ids;

            curl_setopt($curl, CURLOPT_URL, $destination_url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($curl, CURLOPT_TIMEOUT, 1);

            $res = curl_exec($curl);
            curl_close($curl);
        }
    }



    /**
     * Метод подготавливает ответ на getNews
     * @param $type - тип, либо для обычной бд либо для экспертов (general|experts)
     */
    private function getNewsAnswer($type){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        if($type=="general")
            $exchangeTable = new \Model\Gateway\EntitiesTable("isga_ExchangeTable");
        else if($type=="experts")
            $exchangeTable = new \Model\Gateway\ExpertsEntitiesTable("isga_ExchangeTable");
        else if($type=="lod" || $type == "queue")
            $exchangeTable = new \Model\Gateway\LodEntitiesTable("isga_ExchangeTable");

        $gateway = $exchangeTable->getGateway();

        //Удаляем записи о таблице обмена
        $gateway->delete(array(
            "TableName" => "isga_ExchangeTable",
        ));
        //Всем текущим записям проставляем статус, что мол они обрабатываются
        $fields = array(
            "State" => "InExchange",
            "Error" => ""
        );
        $where = null;
        if($type == "queue"){
            $where = [];
            $where["TableName"] = array("ElectronicQueueEntry","ElectronicQueueEntrySlots", "ElectronicQueueEntryToAccreditationReason", "ElectronicQueueEntryToLicenseReasonCatalogItem","ElectronicQueueVacations");
        }
        $gateway->update($fields, $where);

        //Затем забираем все объекты, что в состоянии "InExchange"
        //Это сделано чтобы если в момент обмена что-то добавится, не затереть/потерять эти изменения
        $whereNeed = array(
            "State"     => "InExchange"
        );
        if($type == "queue"){
            $whereNeed["TableName"] = array("ElectronicQueueEntry","ElectronicQueueEntrySlots", "ElectronicQueueEntryToAccreditationReason", "ElectronicQueueEntryToLicenseReasonCatalogItem","ElectronicQueueVacations");
        }
        $objectsNeed = $exchangeTable->getAllEntities(null,"Timestamp DESC",$whereNeed);

        $arrayById = array();
        //Теперь проходим по объектам и запрашиваем из таблиц
        //Сохраняем в массив по Id, так что более свежая версия будет переписывать более старую
        foreach($objectsNeed as $object){
            if(isset($arrayById[$object->getId()]))
                continue;
            //if($object->getField("TableName") == "isga_Users")
                //continue;
            if($object->getField("TableName") == "expert")
                $this->expertIds[] = $object->getField("ObjectId");

            $returnObject = array(
                "Id" => $object->getId(),
                "Timestamp" => $object->getField("Timestamp"),
                "SaveType" => $object->getField("SaveType"),
                "TableName" => $object->getField("TableName"),
                "ObjectId" => $object->getField("ObjectId"),
                "Source" => $object->getField("Source"),
            );

            if(!isset($arrayById[$returnObject["ObjectId"]]))
                $arrayById[$returnObject["ObjectId"]] = $returnObject;
        }
        //Проходим по массиву и скидываем в обычный массив
        $returnArray = array();
        foreach($arrayById as $key => $returnObject){

            //Запрашиваем объект c бд
            $classNameParts = explode("_",$returnObject["TableName"]);
            $className = implode("",$classNameParts);

            if($type=="general")
                $entityClassName = "Model\\Entities\\".$className;
            else if($type=="experts")
                $entityClassName = "Model\\ExpertEntities\\".$className;
            else if($type=="lod" || $type == "queue") {
                $className = $this->getClassName($className);
                $entityClassName = "Model\\LodEntities\\" . $className;
            }

            if( $returnObject["SaveType"] != "delete") {
                $entity = new $entityClassName($returnObject["ObjectId"]);
                if ($entity->getId() == null) {
                    $gateway = $exchangeTable->getGateway();

                    $gateway->delete(array(
                        "ObjectId"=>$returnObject["ObjectId"],
                        "Tablename" => $returnObject["TableName"],
                        new \Zend\Db\Sql\Predicate\NotLike("SaveType", "delete")
                    ));

                    continue;
                }

                if ($returnObject["TableName"] == "isga_EducationalOrganizationData")
                    $returnObject["fields"] = $entity->getFields(true);
                else
                    $returnObject["fields"] = $entity->getFields();
            }

            $returnArray[] = $returnObject;
        }
        echo serialize($returnArray);
    }
}