<?php
namespace Model\SyncService;

/**
 * Класс для синхронизации экспертов
 *
 * Class SyncExpertModule
 * @package Model\SyncService
 */
class SyncExpertModule{
    private $token = "PB4Xd6PMUkRX";
    private $link = "http://expert.f23.prostoy.ru/user-api/store";
    private $checkLink = "http://expert.f23.prostoy.ru/user-api/check-connection";

    /**
     * Метод начинает процесс обмена данными
     * @param $type - тип обмена (general|experts)
     */
    public function startExchange($idsArr){
        $where = '';
        if(empty($idsArr)){
            return false;
        }else{
            $where = 'id IN ("'.implode('","',$idsArr).'")';
        }
        try{
            /*Запрашиваем интересующий нам сервер и смотри все ли хорошо*/
            $this->checkToken();

            $data = array();
            $expertModel = new \Model\ExpertEntities\expert();
            $experts = $expertModel->getAllByWhere($where);
            foreach($experts as $expert){
                $data[] = array(
                    'id' => $expert->getField('id'),
                    'last_name' => $expert->getField('last_name '),
                    'middle_name' => $expert->getField('middle_name'),
                    'name' => $expert->getField('name'),
                    'login' => $expert->getField('login'),
                    'password' => $expert->getField('password'),
                    'gender' => $expert->getField('gender'),
                    'is_close' => $expert->getField('is_close'),
                    'region_id' => $expert->getField('region_id'),
                    'user_role' => $expert->getField('code')!=''?1:4,//4 - претендент
                    'status' => $expert->checkExpertCertificates()?1:0, // если кончилась аккредитация
                    'is_test' => \Ron\Model\Export::isTestServer()?1:0
                );
            }
            $ch = curl_init($this->link);
            $arrayToLoad = json_encode(array('token' => $this->token, 'data' => $data));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $arrayToLoad);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            $res = json_decode($result);
            if($res->status == '204'){
                echo 'ok';
            }else{
                $this->sendProstoy([
                        "msg" => "Не удалось отправить экспертов на url ".$this->link."\n<br>"."Ошибка: ".$res->message
                    ]);
            }
        } catch (\Exception $e){
        }
        die;
    }

    /**
     * Метод генерирует пароль для соединения с кабинетом вуза
     */
    private function checkToken(){
        $ch = curl_init($this->checkLink);
        $arrayToLoad = json_encode(array('token' => $this->token));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arrayToLoad);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        if($res->status == '200'){
            return true;
        }else{
            $this->sendProstoy([
                    "msg" => "Не удается соединиться с ".$this->checkLink."\n<br>"."Ошибка: ".$res->message
                ]);
            die;
        }
    }

    /**
     * Отправить сообщение в клиент простого
     * @param $options
     */
    protected function sendProstoy($options)
    {
        $mess = [];
        $config = new \Zend\Config\Config(include (__DIR__.'/../../../config/module.config.php'));
        $mess[] = "Система источник ошибки: ". $config["sysSource"];

        if(isset($options["msg"]))
            $mess[] = "Текст ошибки:<br>".$options["msg"];

        //Отправляем в простой
        $name = "Сообщение об ошибке синхронизации";
        $data = array(
            'tid' => "1222877",
            'tpass' => "yhTCm7vE6BZ6",
            'tname' => $name,
            'tcom' => iconv("utf-8","windows-1251",implode("<br>",$mess)),
            'com_type' => 'system'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://agent.prostoy.ru/addComment.php");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $obj = simplexml_load_string(curl_exec($ch));
        curl_close($ch);
    }

}