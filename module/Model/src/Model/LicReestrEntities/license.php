<?php
namespace Model\LicReestrEntities;

use Model\Gateway;

class license extends Gateway\LicReestrBaseEntity{
    protected $tableName = "license";
    protected $idName = "_id";

    //====================== ����������� ������
    public static function getLicenseByGUID($guid){
        $table = new Gateway\LicReestrEntitiesTable("license");
        $result = $table->getEntitiesByWhere([
            "guid" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

    // ======================= �� ����������� ������

    public function getLicOrgan(){
        $licOrgan = new \Model\LicReestrEntities\licenseorgan($this->getField("license_organ_id"));
        if(!$licOrgan->isEmpty())
            return $licOrgan;
        else
            return null;
    }

    /**
     * ����� ���������� ������ - ��������� ��������
     */
    public function getStartDoc(){
        $table = new \Model\Gateway\LicReestrEntitiesTable("decision");
        $decision = $table->getEntitiesByWhere([
            "license_guid" => $this->getField("guid"),
            "type_guid"  => "e0e8eb9952464ea798f7e41b6a81c93f"
        ])->getObjectsArray();

        if(count($decision) > 0){
            return $decision[0];
        } else {
            return null;
        }
    }

    /**
     * ����� ���������� ������ - �������� ��������
     */
    public function getEndDoc(){
        $table = new \Model\Gateway\LicReestrEntitiesTable("decision");
        $decision = $table->getEntitiesByWhere([
            "license_guid" => $this->getField("guid"),
            "type_guid"  => "1dc3e282134a4b7e9578216e6f07f096"
        ])->getObjectsArray();

        if(count($decision) > 0){
            return $decision[0];
        } else {
            return null;
        }
    }
    
    public function getToExcerpt( $orgId, $notId) {        
        $results = $this->getGateway()->select(function(\Zend\Db\Sql\Select $select) use ( $orgId, $notId){
            $select
                ->where
                ->notEqualTo('_id',$notId)
                ->like('eo_guid','%'.$orgId.'%');
            $select->order('issue_date DESC');
        });
        $arr = [];
        
        foreach($results as $el){
            $cert = new license();
            $cert->setFields($el,true);
            $arr[] = $cert;
        }
        return $arr;
    }
}