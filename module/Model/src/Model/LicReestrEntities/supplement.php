<?php
namespace Model\LicReestrEntities;

use Model\Gateway;

class supplement extends Gateway\LicReestrBaseEntity{
    protected $tableName = "supplement";
    protected $idName = "_id";

    //====================== ����������� ������
    public static function getSupplementByGUID($guid){
        $table = new Gateway\LicReestrEntitiesTable("supplement");
        $result = $table->getEntitiesByWhere([
            "guid" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

    /**
     * Метод получает все записи таблицы
     * @param $paging - если задано, то вернет сущности для определенной страницы
     *                  Формат:
     *                    array(
     *                      "page" => номер страницы
     *                      "onPage" => количество сущностей на странице
     *                    )
     * @param order - сортировка, например 'id ASC';
     * @param where-массив с полями и значениями;
     * @return ResultSet
     */
    public function getAllEntities($paging = null,$order = null,$where=null, $showQuery = false){
        $tg = $this->tableGateway;
        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($paging, $order,$where,$tg, $showQuery) {

            if(isset($where)){
                $select->where($where);
            }
            if(isset($order)){
                $select->order($order);
            }

            if(isset($paging) && isset($paging["onPage"])){
                $page = 0;
                if(isset($paging["page"]))
                    $page = $paging["page"]-1;

                $select
                    ->limit($paging["onPage"])
                    ->offset($page*$paging["onPage"]);
            }
            if($showQuery){
                $sql = $tg->getSql();
                print_r($sql->getSqlstringForSqlObject($select));die;
            }
        });
        
        $ret = [];
        
        foreach ($resultSet as $value) {
            $sup = new supplement(); 
            $sup->setFieldsSafe($value);
            $ret[] = $sup;
        }
        
        return $ret;
    }

    /**
     * ����� ���������� ������ - ��������� ��������
     */
    public function getStartDoc(){
        $table = new \Model\Gateway\LicReestrEntitiesTable("decision");
        $decision = $table->getEntitiesByWhere([
            "license_attach_guid" => $this->getField("guid"),
            "type_guid"  => "e0e8eb9952464ea798f7e41b6a81c93f"
        ])->getObjectsArray();

        if(count($decision) > 0){
            return $decision[0];
        } else {
            return null;
        }
    }

    /**
     * ����� ���������� ������ - �������� ��������
     */
    public function getEndDoc(){
        $table = new \Model\Gateway\LicReestrEntitiesTable("decision");
        $decision = $table->getEntitiesByWhere([
            "license_attach_guid" => $this->getField("guid"),
            "type_guid"  => "1dc3e282134a4b7e9578216e6f07f096"
        ])->getObjectsArray();

        if(count($decision) > 0){
            return $decision[0];
        } else {
            return null;
        }
    }
}