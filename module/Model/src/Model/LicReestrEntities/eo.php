<?php
namespace Model\LicReestrEntities;

use Model\Gateway;

class eo extends Gateway\LicReestrBaseEntity{
    protected $tableName = "eo";
    protected $idName = "_id";

    public static function getOrgByGUID($guid){
        $table = new Gateway\LicReestrEntitiesTable("eo");
        $result = $table->getEntitiesByWhere([
            "guid" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}