<?php
namespace Model\LicReestrEntities;

use Model\Gateway;

class licensestate extends Gateway\LicReestrBaseEntity{
    protected $tableName = "license_state";
    protected $idName = "_id";

    public static function getStateByCode($code){
        $table = new Gateway\LicReestrEntitiesTable("license_state");
        $result = $table->getEntitiesByWhere([
            "code" => $code
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}