<?php
namespace Model\LicReestrEntities;

use Model\Gateway;

class program extends Gateway\LicReestrBaseEntity{
    protected $tableName = "program";
    protected $idName = "_id";

    public static function getProgramByGUID($guid){
        $table = new Gateway\LicReestrEntitiesTable("program");
        $result = $table->getEntitiesByWhere([
            "guid" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
    
    public static function getProgramsBySupId($supid){
        $table = new Gateway\LicReestrEntitiesTable("program");
        $result = $table->getEntitiesByWhere([
            "supplement_id" => $supid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result;
        else
            return [];
    }
    
    public static function getProgramsByDefaultSupId($supid){
        $table = new Gateway\LicReestrEntitiesTable("program");
        $result = $table->getEntitiesByWhere([
            "supplement_guid" => $supid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result;
        else
            return [];
    }
}