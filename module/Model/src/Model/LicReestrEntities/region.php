<?php
namespace Model\LicReestrEntities;

use Model\Gateway;

class region extends Gateway\LicReestrBaseEntity{
    protected $tableName = "region";
    protected $idName = "_id";

    public static function getRegionByGUID($guid){
        $table = new Gateway\LicReestrEntitiesTable("region");
        $result = $table->getEntitiesByWhere([
            "guid" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}