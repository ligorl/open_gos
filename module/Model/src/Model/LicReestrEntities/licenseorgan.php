<?php
namespace Model\LicReestrEntities;

use Model\Gateway;

class licenseorgan extends Gateway\LicReestrBaseEntity{
    protected $tableName = "license_organ";
    protected $idName = "_id";

    public static function getOrgByGUID($guid){
        $table = new Gateway\LicReestrEntitiesTable("license_organ");
        $result = $table->getEntitiesByWhere([
            "guid" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}