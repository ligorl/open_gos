<?php
namespace Model\Entities;

use Model\Gateway;

class isgaFounders extends Gateway\BaseEntity{
    protected $tableName = "isga_Founders";
    protected $idName = "Id";

    public function getFounderName(){
        $name = array();
        if(isset($this->fields['OrganizationFullName']) && $this->fields['OrganizationFullName'] != null && strtolower($this->fields['OrganizationFullName']) != "null")
            $name[] = $this->fields['OrganizationFullName'];
        if(isset($this->fields['LastName']) && $this->fields['LastName'] != null && strtolower($this->fields['LastName']) != 'null')
            $name[] = $this->fields['LastName'];
        if(isset($this->fields['FirstName']) && $this->fields['FirstName'] != null && strtolower($this->fields['FirstName']) != "null")
            $name[] = $this->fields['FirstName'];
        if(isset($this->fields['Patronymic']) && $this->fields['Patronymic'] != null && strtolower($this->fields['Patronymic']) != "null")
            $name[] = $this->fields['Patronymic'];

        return implode(" ",$name);
    }
}