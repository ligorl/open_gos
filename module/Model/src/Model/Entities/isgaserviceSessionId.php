<?php
namespace Model\Entities;

use Model\Gateway;

class isgaserviceSessionId extends Gateway\BaseEntity{
    protected $tableName = "isgaservice_SessionId";
    protected $idName = "Id";


    public function delete(){
        if(isset($this->fields[$this->idName])){
            //Delete
            return $this->deleteWhere(array($this->idName => $this->fields[$this->idName]));
        }
        return false;
    }

    public function save($isNew = false){
        if(isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $saveType = "update";
            $ret = $this->tableGateway->update($this->fields,array($this->idName => $this->fields[$this->idName]));
            return $ret;
        }
        //insert
        else{
            if(!isset($this->fields[$this->idName]))
                $this->fields[$this->idName] = $this->_generateId();
            $this->tableGateway->insert($this->fields);

            $newId = $this->tableGateway->lastInsertValue;

            if($newId == 0)
                $newId = $this->fields[$this->idName];

            $saveType = "insert";
            return $newId;
        }
        return false;
    }

    /**
     * @param $where array()
     */
    public function deleteWhere($where)
    {
        $result = false;
        if (isset($where) && is_array($where)) {
            //�������
            $result =  $this->tableGateway->delete($where);
        }
        return $result;
    }
}