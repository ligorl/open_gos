<?php
namespace Model\Entities;

use Model\Gateway;

class eiisLicensedPrograms extends Gateway\BaseEntity{
    protected $tableName = "eiis_LicensedPrograms";
    protected $idName = "Id";

    public function getQualificationName(){
        $resultStr = '';
        if( !$this->isEmptyField("fk_isgaQualification") ){
            $result = $this->getLinkedItem("isga_Qualifications",array(
                "Id" => $this->fields["fk_isgaQualification"]
            ));
            if( empty($result) ){
                $resultStr = $this->getFieldHtml('QualificationName');
            }
            else{
                $resultStr = $result->getFieldHtml("Name");
            }
            
        }
        else{
            $resultStr = $this->getFieldHtml('QualificationName');
        }        
        return $resultStr;
    }

    public function getEduProgram(){
        if($this->isEmptyField("fk_eiisEduProgram")){
            $search['Code'] = $this->getField('Code');
            $search['fk_eiisEduLevels'] = $this->getField('fk_eiisEduLevel');
            $search['fk_eiisEduProgramType'] = $this->getField('fk_eiisEduProgramType');
            $search['QualificationCode'] = $this->getField('QualificationCode');
            $search['Period'] = $this->getField('Period');
            $result = $this->getLinkedItem("eiis_EduPrograms",$search);
            return $result;
        }
        $result = $this->getLinkedItem("eiis_EduPrograms",array(
                "Id" => $this->fields["fk_eiisEduProgram"]
            ));
        return $result;
    }


    public function getEduProgramType(){
        if(!isset($this->fields["fk_eiisEduProgramType"]) || $this->fields["fk_eiisEduProgramType"]=="" || strtolower($this->fields["fk_eiisEduProgramType"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_EduProgramTypes",array(
            "Id" => $this->fields["fk_eiisEduProgramType"]
        ));
        return $result;
    }

    public function getEduLevelName()
    {
        if($this->isEmptyField("fk_eiisEduLevel"))
            return null;

        $result = $this->getLinkedItem("eiis_EduLevels",array(
            "Id" => $this->fields["fk_eiisEduLevel"]
        ));

        return $result->getFieldSafe("Name");
    }
    
    /**
     * 
     * @return string
     */
    public function getEduLevelPrintName()
    {
        $level = $this->getEduProgramLevel();
        if(!empty($level))   {
            $result = $level->getPrintName();
        }   else {
            $result = '';
        }  
        return $result;
    }       

    public function getLicenseSupplement(){
        if(!isset($this->fields["fk_eiisLicenseSupplement"]) || $this->fields["fk_eiisLicenseSupplement"]=="" || strtolower($this->fields["fk_eiisLicenseSupplement"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_LicenseSupplements",array(
            "Id" => $this->fields["fk_eiisLicenseSupplement"]
        ));
        return $result;
    }
    

    public function getUgs($ugsId=null) {
        if(empty($ugsId)){
            $eduProgramId = $this->getField('fk_eiisEduProgram');
            if ($this->getField('fk_eiisEduProgram') != NULL && $this->getField('fk_eiisEduProgram') != 'NULL') {
                $eduProgram = $this->getLinkedItem("eiis_EduPrograms", array('Id' => $eduProgramId));
            } else {
                $eduProgramCode           = $this->getField('Code');
                $eduProgramEduProgramType = $this->getField('fk_eiisEduProgramType');
                $eduProgram              = $this->getLinkedItem("eiis_EduPrograms", array('Code' => $eduProgramCode, 'fk_eiisEduProgramType' => $eduProgramEduProgramType));
            }
            /*$isgaGS = $this->getLinkedItem("isga_GroupSpecialities", array('Id' =>$eduProgram->getField('fk_eiisUGS')));*/
            if ( !empty($eduProgram) ){
                $ugsId = $eduProgram->getField('fk_eiisUgs');
            }
        }
        if( !empty($ugsId) ){
            $isgaEGS = $this->getLinkedItem("isga_EnlargedGroupSpecialities", array('Id'=>$ugsId ));
        } else {
            $isgaEGS = ''; 
        }
        return $isgaEGS;
    }

    public function getName() {
        if(!$this->isEmptyField('Name')){
            return $this->getField('Name');
        }
        $eduProg = $this->getEduProgram();
        if ($eduProg){
            return $this->getField("Name");
        }
        return null;
    }
    /**
     * Отдает или дергает код образовательной программы
     * @return type
     */
    public function getCode() {
        if(!$this->isEmptyField('Code')){
            return $this->getField('Code');
        }
        $eduProg = $this->getEduProgram();
        if ($eduProg){
            return $this->getField("Code");
        }
        return null;
    }

    public function getPeriod() {
        if(!$this->isEmptyField('Period')){
            return $this->getField('Period');
        }
        $eduProg = $this->getEduProgram();
        if ($eduProg){
            return $this->getFieldOrSpace("Period");
        }
        return null;
    }

    /**
     * получение данных для выгрузки
     * @param Array $application_ids
     */
    public function getProgramsByApplicatins($licensed_id){

        $result = $this->tableGateway->select(function(Select $select) use ($application_ids)
            {
                $select
                    ->columns(array("Code", "Name"))
                    ->where(array("fk_isgaApplication" => $application_ids) )
                    ->order('Name ASC');
            });
        $data = array(); $license_ids = array();
        foreach($result as $row){
            $data[$row->fk_isgaApplication][] =  $row->fk_eiisLicensedPrograms;
            $license_ids[] = $row->fk_eiisLicensedPrograms;
        }

        return array("app_data" => $data, "license" => $license_ids);
    }

    public function getItemsData($ids)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        if(count($ids) == 0) return array();
        $fields = array("Id", "Code", "Name", "QualificationCode", "QualificationName", "fk_eiisEduLevel", "fk_eiisEduProgramType", "fk_eiisEduProgram");

        $result = $this->tableGateway->select(function(Select $select) use ($ids, $fields, $sql)
            {

                $select
                    ->columns($fields)
                    ->where(array("Id" => $ids) )
                    ->order('Id ASC')
                ;
                $sqlString = $sql->getSqlStringForSqlObject($select);
                //var_dump($sqlString);die;
            });

        $data = array(); $levels = array(); $edu_programs_types = array(); $edu_programs = array();
        foreach($result as $row){
            foreach($fields as $field){
                $data[mb_strtoupper($row->Id, "UTF-8")][$field] = $row->$field;
            }
            $levels[] = mb_strtoupper($row->fk_eiisEduLevel, "UTF-8");
            $edu_programs_types[] =  mb_strtoupper($row->fk_eiisEduProgramType, "UTF-8");
            $edu_programs[] =  mb_strtoupper($row->fk_eiisEduProgram, "UTF-8");
        }
        return array("data" => $data, "levels" => $levels, "edu_programs_type" => $edu_programs_types, "edu_programs" => $edu_programs );
    }

    public function getItemsBySupplements($supplement_id)
    {
        if(empty($supplement_id)) return false;
        $fields = array("Id", "Code", "Name", "fk_eiisEduLevel", "fk_eiisEduProgramType", "Period");

        $result = $this->tableGateway->select(function(Select $select) use ($supplement_id, $fields)
            {

                $select
                    ->columns($fields)
                    ->join('eiis_EduLevels', 'eiis_LicensedPrograms.fk_eiisEduLevel =  eiis_EduLevels.Id', array("name_level" => "Name"), $select::JOIN_LEFT)
                    ->join('eiis_EduProgramTypes', 'eiis_LicensedPrograms.fk_eiisEduProgramType =  eiis_EduProgramTypes.Id', array("name_type" => "Name"), $select::JOIN_LEFT)
                    ->where(array("fk_eiisLicenseSupplement" => $supplement_id) )
                    ->order('Id ASC')
                ;
                ///$sqlString = $sql->getSqlStringForSqlObject($select);
                //var_dump($sqlString);die;
            });

        $data = array();
        foreach($result as $row){
            foreach($fields as $field){
                $data[mb_strtoupper($row->Id, "UTF-8")][$field] = (!empty($row->$field) && $row->$field != "NULL") ? $row->$field : "";
            }
            $data[mb_strtoupper($row->Id, "UTF-8")]['name_level'] = $row->name_level;
            $data[mb_strtoupper($row->Id, "UTF-8")]['name_type'] = $row->name_type;
        }
        return $data;
    }

    public function getEduProgramLevel(){
        if($this->isEmptyField("fk_eiisEduLevel"))
            return null;

        $result = $this->getLinkedItem("eiis_EduLevels",array(
            "Id" => str_replace('-','',strtoupper($this->fields["fk_eiisEduLevel"]))
        ));
        return $result;

    }

    public function getUgsId() {
        if (!$this->isEmptyField( 'fk_eiisEduProgram')) {
            $eduProgramId = $this->getField('fk_eiisEduProgram');
            $eduProgram = $this->getLinkedItem("eiis_EduPrograms", array('Id' => $eduProgramId));
        }
        if(empty($eduProgram)){
            $eduProgramCode           = $this->getField('Code');
            $eduProgramEduProgramType = $this->getField('fk_eiisEduProgramType');
            $eduProgram              = $this->getLinkedItem("eiis_EduPrograms", array('Code' => $eduProgramCode, 'fk_eiisEduProgramType' => $eduProgramEduProgramType));
        }
        //если программа вывявлена или и есть ид угс , дергаем
        if($eduProgram!=null && !$eduProgram->isEmptyField('fk_eiisUgs')){
            $ugsId = $eduProgram->getField('fk_eiisUgs');
            return $ugsId;
        }
        //нет программы или ид угс
        if ($eduProgram == null || empty($eduProgram)||$eduProgram->isEmptyField('fk_eiisGS') ){
            // попытаемся выудить по соду угс в лиц программе
            if(!$this->isEmptyField('Code')){
                $code = $this->getField('Code');
                //с кодом типа 00,00,00
                if ( preg_match('/^\d\d\.\d\d\.\d\d$/', $code) ) {
                    $codeNew = substr($code, 0,2).'.00.00';
                    $isgaEGS = $this->getLinkedItem("isga_EnlargedGroupSpecialities", array('Code'=>$codeNew ));
                    if(!empty($isgaEGS )){
                        //есть ура
                        return $isgaEGS->getField('Id');
                    }
                }
            }
            return false;
        }
        $ugsId = $eduProgram->getField('fk_eiisUgs');
        return $ugsId;

    }

    public function isProfy(){
        $eduLevel=  $this->getEduProgramLevel();
        if(!$eduLevel){
            return null;
        }
        return $eduLevel->isProfy();
    }

    //последняя строка
}