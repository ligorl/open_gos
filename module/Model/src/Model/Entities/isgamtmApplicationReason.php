<?php
namespace Model\Entities;

use Model\Gateway;

class isgamtmApplicationReason extends Gateway\BaseEntity{
    protected $tableName = "isga_mtm_Application_Reason";
    protected $idName = "Id";
    
    public function getReasons($applicationId) {
        $reasonsId = $this->getReasonIdIArray($applicationId);
        if (empty($reasonsId )){
            return null;
        }        
        foreach ($reasonsId as $key => $value){
            $reasonsId[$key]=  strtoupper($value);
        }
        $reasonsString = '("'.implode('", "', $reasonsId).'")';
        $reasons = $this->getLinkedItems(
                'isga_ApplicationReasons', array('upper(`Id`) in '. $reasonsString));
        $reasons = $reasons -> getObjectsArray();
        return $reasons;
    }

    public function getReasonArray($applicationId) {
        $reasonsId = $this->getReasonIdIArray($applicationId);
        if (empty($reasonsId )){
           return null;
        } else {       
            $reasons = $this->selectComplicated('isga_ApplicationReasons',array(), array('Id'=>$reasonsId ))->toArray();
        }
        return $reasons;
    }    

    public function getReasonIdIArray($applicationId) {       
        if(empty($applicationId)){
            return null;
        }
        $reasons = $this->selectComplicated(
                $this->tableName,
                array('fk_isgaApplicationReason'), 
                array('fk_isgaApplication'=>$applicationId))
                ->toArray();
        if(empty($reasons)){
            return null;
        }
        $reasonsId = array_column($reasons, 'fk_isgaApplicationReason');
        return $reasonsId;
    }    

}