<?php
namespace Model\Entities;

use Model\Gateway;

class isgamtmDocumentReason extends Gateway\BaseEntity{
    protected $tableName = "isga_mtm_Document_Reason";
    protected $idName = "Id";
}