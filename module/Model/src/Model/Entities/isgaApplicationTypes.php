<?php
namespace Model\Entities;

use Model\Gateway;

class isgaApplicationTypes extends Gateway\BaseEntity{
    protected $tableName = "isga_ApplicationTypes";
    protected $idName = "Id";

    public function getReasons(){
        $reasons = $this->getLinkedItems("isga_ApplicationReasons",array(
            "fk_isgaApplicationType" => $this->fields["Id"]
        ), "Name ASC");

        return $reasons;
    }

    public function getCode()
    {
        return $this->getField('Code');
    }

    public function getName()
    {
        return $this->getField('Name');
    }
}