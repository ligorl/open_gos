<?php
namespace Model\Entities;

use Model\Gateway;

class isgaApplicationBack extends Gateway\BaseEntity{
    protected $tableName = "isga_ApplicationBack";
    protected $idName = "Id";
    //put your code here

    public function getDateCreate($param){
        if ($this->fields['DateCreata']){
            $time=strtotime($this->fields['DateCreata']);
            return time('d.m.Y',$time);
        }
    }
    /*
    public function getByApplication($param){
        if (!isset($param)){
            return null;
        }
        $result=$this->getEntityById($param, 'fk_isgaApplication');
        return result;

    }
     * 
     */
}