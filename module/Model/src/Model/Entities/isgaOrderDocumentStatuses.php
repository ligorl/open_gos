<?php
namespace Model\Entities;

use Model\Gateway;

class isgaOrderDocumentStatuses extends Gateway\BaseEntity{
    protected $tableName = "isga_OrderDocumentStatuses";
    protected $idName = "Id";
}