<?php
namespace Model\Entities;

use Model\Gateway;

class eiisLicenseSupplements extends Gateway\BaseEntity{
    protected $tableName = "eiis_LicenseSupplements";
    protected $idName = "Id";

    public function getEducationalOrganization(){
        if(!isset($this->fields["fk_eiisBranch"]) || $this->fields["fk_eiisBranch"] == "" || strtolower($this->fields["fk_eiisBranch"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_EducationalOrganizations",array(
            "Id" => $this->fields["fk_eiisBranch"],
        ));
        return $result;
    }

    public function getStatus(){
        if(!isset($this->fields["fk_eiisLicenseSupplementState"]) || $this->fields["fk_eiisLicenseSupplementState"] == "" || strtolower($this->fields["fk_eiisLicenseSupplementState"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_LicenseSupplementStates",array(
            "Id" => $this->fields["fk_eiisLicenseSupplementState"]
        ));
        return $result->getField("Name");
    }

    public function getStatusObject(){
        if(!isset($this->fields["fk_eiisLicenseSupplementState"]) || $this->fields["fk_eiisLicenseSupplementState"] == "" || strtolower($this->fields["fk_eiisLicenseSupplementState"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_LicenseSupplementStates",array(
            "Id" => $this->fields["fk_eiisLicenseSupplementState"]
        ));
        return $result;
    }

    public function getEducationalPrograms(){
        $programs = $this->getLinkedItems("eiis_LicensedPrograms",array(
            "fk_eiisLicenseSupplement" => $this->fields["Id"]
        ));

        return $programs;
    }

    public function isValid(){
        if(!isset($this->fields["fk_eiisLicenseSupplementState"]) || $this->fields["fk_eiisLicenseSupplementState"] == "" || strtolower($this->fields["fk_eiisLicenseSupplementState"]) == "null")
            return false;

        $result = $this->getLinkedItem("eiis_LicenseSupplementStates",array(
            "Id" => $this->fields["fk_eiisLicenseSupplementState"]
        ));
        if($result && $result->getField("IsValid") == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getLicense(){
        if(!isset($this->fields["fk_eiisLicense"]) || $this->fields["fk_eiisLicense"] == "" || strtolower($this->fields["fk_eiisLicense"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_Licenses",array(
            "Id" => $this->fields["fk_eiisLicense"],
        ));
        return $result;

    }

    /**
     * �������� ��� ������ �� ������� ������ ������������ ��������������� ������������
     *
     * @return type
     */
    public function getLocationAdress() {
        if($this->isEmptyField($this->idName)){
            return null;
        }

        $mtmAdress = new \Model\LodEntities\OrganizationLocationtoLicenseAttach();
        $adressIdArray = $mtmAdress->getAllByWhereParseId(
            array('licenseAttachId' => $this->getId()),
            'organizationLocationId'
        );

        if(empty($adressIdArray)){
            return $adressIdArray;
        }

        $adressIdArray = array_keys($adressIdArray);

        $organisationLocation = new \Model\LodEntities\OrganizationLocation();
        $organisationLocation = $organisationLocation->getAllByWhereParseId(array(
            'id' => $adressIdArray,
        ));

        return $organisationLocation;
    }

    public function getDocumentFromId( $supplementId = null){
        try {
            if (empty($supplementId)) {
                $supplementId = $this->getId();
            }
            if (empty($supplementId)) {
                return null;
            }
            $licAndLicSupp = new \Model\LodEntities\LicenseAndLicenseSupplementToDocument();
            $licAndLicSupp = $licAndLicSupp->getDataJoin_ArrayCondition(
                [
                    'dealId' => new \Zend\Db\Sql\Expression('Document.dealId'),
                    'registrationDate' => new \Zend\Db\Sql\Expression('Document.registrationDate'),
                    'registrationNumber' => new \Zend\Db\Sql\Expression('Document.registrationNumber'),
                    'documentTypeId' => new \Zend\Db\Sql\Expression('Document.documentTypeId')
                ],
                [
                    'table' => 'Document',
                    'binder' => 'LicenseAndLicenseSupplementToDocument.documentId = Document.id',
                    'type' => 'left'
                ],
                [
                    ['column' => 'LicenseAndLicenseSupplementToDocument.licenseAttachId="' . $supplementId . '"'],
                    ['and'],
                    ['column' => 'LicenseAndLicenseSupplementToDocument.documentDecisionTypeId="e0e8eb9952464ea798f7e41b6a81c93f"']
                ]);
            $licAndLicSupp = $licAndLicSupp->getObjectsArray();
            if(count($licAndLicSupp) > 0) {
                return $licAndLicSupp[0];
            } else {
                return null;
            }
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license_request");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    public function getEndDocumentFromId( $supplementId){
        try {
            $licAndLicSupp = new \Model\LodEntities\LicenseAndLicenseSupplementToDocument();
            $licAndLicSupp = $licAndLicSupp->getDataJoin_ArrayCondition(
                [
                    'dealId' => new \Zend\Db\Sql\Expression('Document.dealId'),
                    'registrationDate' =>  new \Zend\Db\Sql\Expression('Document.registrationDate'),
                    'registrationNumber' => new \Zend\Db\Sql\Expression('Document.registrationNumber'),
                    'documentTypeId' => new \Zend\Db\Sql\Expression('Document.documentTypeId')
                ],
                [
                    'table' => 'Document',
                    'binder' => 'LicenseAndLicenseSupplementToDocument.documentId = Document.id',
                    'type' => 'left'
                ],
                [
                    ['column'=>'LicenseAndLicenseSupplementToDocument.licenseAttachId="'.$supplementId.'"'],
                    ['and'],
                    ['column'=>'LicenseAndLicenseSupplementToDocument.documentDecisionTypeId="1dc3e282134a4b7e9578216e6f07f096"']
                ]);
            return $licAndLicSupp->current();
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license_request");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    public function getOtherDocumentFromId($supplementId)
    {
        $licAndLicSupp = new \Model\LodEntities\LicenseAndLicenseSupplementToDocument();

        $licAndLicSupp = $licAndLicSupp->getDataJoin_ArrayCondition(
          [
            'id'                 => new \Zend\Db\Sql\Expression('Document.id'),
            'dealId'             => new \Zend\Db\Sql\Expression('Document.dealId'),
            'registrationDate'   => new \Zend\Db\Sql\Expression('Document.registrationDate'),
            'registrationNumber' => new \Zend\Db\Sql\Expression('Document.registrationNumber'),
            'documentTypeId'     => new \Zend\Db\Sql\Expression('Document.documentTypeId')
          ],
                                                                [
            'table'  => 'Document',
            'binder' => 'LicenseAndLicenseSupplementToDocument.documentId = Document.id',
            'type'   => 'left'
          ],
                                                                [
                ['column' => 'LicenseAndLicenseSupplementToDocument.licenseAttachId="' . $supplementId . '"'],
                ['and'],
                ['column' => 'LicenseAndLicenseSupplementToDocument.documentDecisionTypeId'],
                ['not'],
                [
                'in' => [
                    '"1dc3e282134a4b7e9578216e6f07f096"',
                    '"e0e8eb9952464ea798f7e41b6a81c93f"'
                ]
            ]
        ]);

        return $licAndLicSupp;
    }

    /**
     * получаем запись на файл макета(сформированый печатный вариант
     * @return type
     */
    public function getPrintFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('SupplementPrint', $this->getId());
        return $result;
    }

    /**
     * получим запись о скан копии из файлохранилища
     * @return type
     */
    public function getScanFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('LicenseAttach', $this->getId());
        return $result;
    }

    /**
     * дергаем здапись о файле, который подписан
     * @return type
     */
    public function getDocumentFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('SupplementDocument', $this->getId());
        return $result;
    }

    /**
     * дергаем подпись подписаного файла
     * @return type
     */
    public function getSignFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('SupplementSign', $this->getId());
        return $result;
    }
    
    /**
     * выдергиваем лицензионные программы приложения в Лоде
     *
     * @return type
     */
    public function getLodPrograms()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        $programs = new \Model\LodEntities\EducationProgram();
        $programs = $programs->getAllByWhereParseId(array(
            "LicenseSupplementID" => $this->getId()
        ));
        return $programs;
    }
    

    //последниц строка
}