<?php
namespace Model\Entities;

use Model\Gateway;

class isgaCertificates extends Gateway\BaseEntity{
    protected $tableName = "isga_Certificates";
    protected $idName = "Id";

    static public function getByGuid($guid){
        $certTable = new \Model\Gateway\EntitiesTable("isga_Certificates");
        $result = $certTable->getEntitiesByWhere([
            "GUID" => $guid
        ])->getObjectsarray();
        if(count($result)>0)
            return $result[0];
        else
            return null;
    }

    public function getStatusName(){
        if(!isset($this->fields["fk_isgaCertificateStatus"]) || $this->fields["fk_isgaCertificateStatus"] == "" || strtolower($this->fields["fk_isgaCertificateStatus"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_CertificateStatuses",array(
            "Id" => $this->fields["fk_isgaCertificateStatus"]
        ));
        return $result->getField("Name");
    }

    public function getTypeName(){
        if(!isset($this->fields["fk_isgaCertificateType"]) || $this->fields["fk_isgaCertificateType"] == "" || strtolower($this->fields["fk_isgaCertificateType"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_CertificateTypes",array(
            "Id" => $this->fields["fk_isgaCertificateType"]
        ));
        return $result->getField("Name");
    }

    private $_controlOrgan = null;

    public function getControlOrgan(){
        if($this->_controlOrgan==null) {
            $eoTable = new \Model\Gateway\EntitiesTable("eiis_ControlOrgans");

            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
            $subselect = $sql->select();

            $subselect->from('eiis_Licenses')
                ->columns(array('fk_eiisControlOrgan'))
                ->where(array(
                    'fk_eiisEducationalOrganization' => $this->getField("fk_eiisEducationalOrganization")
                ));

            $resultSet = $eoTable->getEntitiesByWhere([
                new \Zend\Db\Sql\Predicate\In('Id', $subselect)
            ])->getObjectsArray();

            if(count($resultSet)>0){
                $this->_controlOrgan = $resultSet[0];
            } else {
                $this->_controlOrgan = false;
            }
        }
        return $this->_controlOrgan;
    }

    public function getControlOrganName(){
        $co = $this->getControlOrgan();
        if($co != null && $co != false){
            return $co->getField("Name");
        }
        return "";
    }

    public function getControlOrganId(){
        $co = $this->getControlOrgan();

        if($co != null && $co != false)
            return $co->getId();
        return "";
    }

    public function getControlOrganName2(){
        if($this->isEmptyField("fk_eiisControlOrgan"))
            return null;

        $result = $this->getLinkedItem("eiis_ControlOrgans",array(
                "Id" => $this->fields["fk_eiisControlOrgan"]
            ));
        return $result->getField("Name");
    }

    public function getSupplements(){
        $supplements = $this->getLinkedItems("isga_CertificateSupplements",array(
            "fk_isgaCertificate" => $this->fields["Id"]
        ));

        return $supplements;
    }

    public function getEducationalOrganization(){
        if(!isset($this->fields["fk_eiisEducationalOrganization"]) || $this->fields["fk_eiisEducationalOrganization"] == "" || strtolower($this->fields["fk_eiisEducationalOrganization"]) == "null"){
            return null;
        }

        $result = $this->getLinkedItem("eiis_EducationalOrganizations",array(
            "Id" => $this->stripIdDashes($this->fields["fk_eiisEducationalOrganization"])
        ));
        return $result;
    }

    public function getApplication(){
        if($this->isEmptyField("fk_oisgaApplication")){
            return null;
        }

        $result = $this->getLinkedItem("isga_Applications",array(
            "Id" => $this->fields["fk_oisgaApplication"]
        ));
        return $result;
    }
    
    /**
     * дергаемм ссылки на файл  макета
     * они лежат в фиде имя;имя;имя
     * в папке /uploads/имя
     * 
     * @param type $index       - если индекс есть, вернем чего файле от 0, если и такого нет , вернем пустую строку
     * @param type $fieldName   - поле с которого дергать
     *      ScanMaketCertificate        -   макет для печатьи
     *      ScanCertificate             -   скан свидетельства
     *      sign_maket                  -   макет для електронной подписи
     * @return string           - или массив или строку(если задан индекс)
     */
    public function getScanMaketUrlAll( $index = null , $fieldName = 'ScanMaketCertificate' ){
        $result = array();
        if( !$this->isEmptyField( $fieldName ) ){
            $result = explode(";",$this->getFieldSafe( $fieldName ));
        }
        foreach( $result as $key => $value ){
            $result[ $key ] = '/uploads/'. trim( $value );
        }
        if( is_numeric( $index ) ){            
            if( !empty( $result[ $index ] ) ){
                $result = $result[ $index ];
            }     
            else{
                $result = '';
            }
        }
        return $result;
    }
    
    /**
     * возрщает перечено имен файлов  мавкетов
     * возращает только имена файлов, без пути
     * 
     * @param type $index - если есть вернет имя
     * @return string      - или массив или строку(если задан индекс)
     */
    public function getScanMaketFileNameAll( $index = null , $fieldName = 'ScanMaketCertificate' ){
        $result = $this->getScanMaketUrlAll( null , $fieldName );
        foreach( $result as $key => $value ){
            $valueArray = explode( '/' , $value);
            $result[ $key ] = end( $valueArray );
        }
        if( is_numeric( $index ) ){           
            if( !empty( $result[ $index ] ) ){
                $result = $result[ $index ];
            } 
            else{
                 $result = '';
            }
        }
        return $result;
    }

    public function getCertificatePeriod(){

//$logger = new \Model\Logger\Logger("test",true);
//$logger->Log("=======");
        try {
//$logger->Log("1");
            //���� ���� ������, ����������
            if (!$this->isEmptyField("CertificatePeriod")){
                $value = $this->getField("CertificatePeriod");
                if($value == "-") $value = "";
                return $value;
            }
//$logger->Log("2");
            //����� ������� ��������� ����
            $isBaseProgram = false;
            $isProfProgram = false;
//$logger->Log("3");
            $supps = $this->getSupplements();
//$logger->Log("4");
            foreach($supps as $sup){
//$logger->Log("5");
                $accrPrograms = $sup->getAccreditedPrograms()->getObjectsArray();
//$logger->Log("6 ".count($accrPrograms));
                foreach($accrPrograms as $program){
//$logger->Log("6.1");
                    $isProfy = $program->isProfy();
//$logger->Log("7 ".print_r($isProfy,true));
                    //$logger->Log("4.2");
                    if ($isProfy === false) {
                        $isBaseProgram = true;
                    } else if ($isProfy === true) {
                        $isProfProgram = true;
                        break;
                    }
                }
//$logger->Log("8 b-".print_r($isBaseProgram,true)." p-".$isProfProgram);
                if($isProfProgram)
                    break;
            }
            $period = "-";
            if($isProfProgram || $isBaseProgram){
                if($isProfProgram)
                    $period = "6";
                else if($isBaseProgram)
                    $period = "12";
            }
//$logger->Log("9 ".print_r($period,true));
            $this->setField("CertificatePeriod", $period);
            $this->save(false, true);

            return $period;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }
    
    public function getToExcerpt( $orgId, $notId) {        
        $results = $this->getGateway()->select(function(\Zend\Db\Sql\Select $select) use ( $orgId, $notId){
            $select
                ->where
                ->notEqualTo('Id',$notId)
                ->equalTo('fk_eiisEducationalOrganization',$orgId);
            $select->order('DateEnd DESC');
        });
        $arr = [];
        
        foreach($results as $el){
            $cert = new isgaCertificates();
            $cert->setFields($el,true);
            $arr[] = $cert;
        }
        return $arr;
    }
    
}