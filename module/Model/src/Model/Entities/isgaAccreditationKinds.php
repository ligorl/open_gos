<?php
namespace Model\Entities;

use Model\Gateway;

class isgaAccreditationKinds extends Gateway\BaseEntity{
    protected $tableName = "isgaAccreditationKinds";
    protected $idName = "Id";
}