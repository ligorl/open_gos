<?php



namespace Model\Entities;

use Model\Gateway;

class isgaApplicationDocumentGroupGroupStatusReason extends Gateway\BaseEntity{
    protected $tableName='isga_ApplicationDocumentGroupGroupStatusReason';
    protected $idName = "Id";


    public function delRecords($documentGroudId,$documentGroupStatusId)
    {
        if(empty($documentGroudId) || empty($documentGroupStatusId)){
            return null;
        }

        $where=array('fk_isgaApplicationDocumentGroup'=>$documentGroudId
          ,'fk_isgaApplicationDocumentGroupStatus'=>$documentGroupStatusId
          );
        return $this->deleteWhere($where);
    }

    /**
     * выбирает сохраняем приччины не соответствий/отказов у причин
     * 
     * @param type $applicationId
     * @return boolean
     */
    public function getReasons($applicationId)
    {
        //дергаем документГроуп
        $docGroup = new \Model\Entities\isgaApplicationDocumentGroups();
        $docGroup->getByApplicationId($applicationId);
        if (!$docGroup->getByApplicationId($applicationId)) {
            return false;
        }
        //дергнуть  гроуп гроуп, удалить старые
        $documentGroudId=$docGroup->getField('Id');        
        $docGroupStatusId=$docGroup->getField('fk_ApplicationDocumentGroupStatus');       

        $where=array(
           'fk_isgaApplicationDocumentGroup'=>$documentGroudId,
           'fk_isgaApplicationDocumentGroupStatus'=>$docGroupStatusId,           
        );
        return $this->selectComplicated($this->tableName, array('fk_isgaApplicationDocumentGroupStatusReasons','Comment'), $where);
    }

    public function getStatusReason($fk_isgaApplicationDocumentGroupStatus=null)
    {
        if($fk_isgaApplicationDocumentGroupStatus===null){
            $fk_isgaApplicationDocumentGroupStatus=$this->getField('fk_isgaApplicationDocumentGroupStatus');
            if (!isset($fk_isgaApplicationDocumentGroupStatus)){
                return false;
            }
        }
        return $this->getLinkedItem(
          'isga_ApplicationDocumentGroupStatusReason',
          array('Id'=>$fk_isgaApplicationDocumentGroupStatus)
          );
    }

    public function getStatusReasonValue($fk_isgaApplicationDocumentGroupStatus=null){
        return $this->getStatusReason($fk_isgaApplicationDocumentGroupStatus)->getField('Name');
    }

    public function saveReasons($reasons)
    {
        foreach ($reasons as $value){
            //$value['Id']=$this->_generateId();
        }
        $this->setFields($reasons);
        return  $this->save(true);
        //$this->tableGateway
    }

}