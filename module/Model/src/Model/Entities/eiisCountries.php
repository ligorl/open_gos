<?php
namespace Model\Entities;

use Model\Gateway;

class eiisCountries extends Gateway\BaseEntity{
    protected $tableName = "eiis_Countries";
    protected $idName = "Id";
}