<?php
namespace Model\Entities;

use Model\Gateway;

class isgaCertificateSupplementStatuses extends Gateway\BaseEntity{
    protected $tableName = "isga_CertificateSupplementStatuses";
    protected $idName = "Id";
}