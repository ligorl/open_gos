<?php
namespace Model\Entities;

use Model\Gateway;
use Zend\Db\Sql\Select;

class isgaApplicationStatuses extends Gateway\BaseEntity{
    protected $tableName = "isga_ApplicationStatuses";
    protected $idName = "Id";
    
     public function getItemsData($ids){
         if(count($ids) == 0) return array();
        $ids = array_values($ids);
        $result = $this->tableGateway->select(function(Select $select) use ($ids)
        {

            $select
                ->columns(array("Id", "Name"))
                ->where(array("Id" => $ids) )
                ->order('Id ASC')
                ;
                
        });
        $data = array();
        foreach($result as $row){
            $data[mb_strtoupper($row->Id, "UTF-8")] = $row->Name;
        }
        return $data;
    }
    
    /**
     * получение всех записей в виде массива
     * @return array
     */
    public function getAllData()
    {
        $result = $this->tableGateway->select(function(Select $select)
        {

            $select
                ->columns(array("Id", "Name"))
                ->order('Id Name')
                ;
                
        });
        $data = array();
        foreach($result as $row){
            $data[mb_strtoupper($row->Id, "UTF-8")] = $row->Name;
        }
        return $data;
    }
}