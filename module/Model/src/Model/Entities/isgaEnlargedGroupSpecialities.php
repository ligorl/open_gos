<?php
namespace Model\Entities;

use Model\Gateway;
use Zend\Db\Sql\Select;
class isgaEnlargedGroupSpecialities extends Gateway\BaseEntity{
    protected $tableName = "isga_EnlargedGroupSpecialities";
    protected $idName = "Id";

    public function getField($fieldName){
        if(isset($this->fields[$fieldName]) && strtolower($this->fields[$fieldName]) != "null"){
            if(strtolower($this->fields[$fieldName]) != "void" && strtolower($this->fields[$fieldName])!="unnamed")
                return $this->fields[$fieldName];
            else
                return "";
        }
        else
            return null;
    }

    public function getEdition(){
        if ($this->isEmptyField("fk_isgaEGSEdition")){
            return null;
        }
        $idEdition =  $this->fields['fk_isgaEGSEdition'];
        $result = $this->getLinkedItem(
            'isga_EnlargedGroupSpecialityEditions', array('Id' => $idEdition)
        );
        return $result;
    }
}