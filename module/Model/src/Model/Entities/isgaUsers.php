<?php
namespace Model\Entities;

use Model\Gateway;

class isgaUsers extends Gateway\BaseEntity{
    protected $tableName = "isga_Users";
    protected $idName = "Id";

    /**
     * пїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ пїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅпїЅ
     * @return Gateway\ResultSet|null
     */
    public function getEducationOrganization(){
        if(!isset($this->fields["fk_eiisEducationalOrganization"]) ||
            $this->fields["fk_eiisEducationalOrganization"] == ""
        )
            return null;

        $result = $this->getLinkedItem("eiis_EducationalOrganizations",array(
            "Id" => $this->fields["fk_eiisEducationalOrganization"]
        ));
        return $result;
    }
    public function getRole(){
        if(!isset($this->fields["Role"]) ||
            $this->fields["Role"] == ""
        )
            return null;
        return $this->fields["Role"];
    }

    public function checkLogin(){
        $wasLogin = $this->fields["WasLogin"];
        if($wasLogin!=null && $wasLogin == 0){
            $this->setField("WasLogin",1);
            $this->save();
        }
    }

    static public function getByEmail($email){
        $table = new Gateway\EntitiesTable("isga_Users");
        $result = $table->getEntitiesByWhere([
            "Email" => $email
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

    static public function getByOrgId($orgId){
        $table = new Gateway\EntitiesTable("isga_Users");
        $result = $table->getEntitiesByWhere([
            "fk_eiisEducationalOrganization" => $orgId
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
    
    
    /**
     * возращаяе класс авторизированого пользователя
     * 
     * @throws \Exception
     */
    static public function getAutorisateClass( ){             
        $result = null;
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $dbTableAuthAdapter  = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter($dbAdapter,
            'ron_Users','Login','Password');
        $authService = new \Auth\Storage\AuthService(); // \Zend\Authentication\AuthenticationService();
        $authService->setAdapter($dbTableAuthAdapter);
        $authService->setStorage(new \Auth\Storage\AuthStorage('Auth'));
        $user = $authService->getIdentity();
        //if(empty($user)){
        //    throw new \Exception('Пользовательне не аутентифицирован');
        //}
        if( $user instanceof self ){
            $result = $user;
        }
        if( is_string( $user ) ){
            $userClass = new self;
            $result = $userClass->getById( $user );
        }
        return $result;
    }
    
    
}