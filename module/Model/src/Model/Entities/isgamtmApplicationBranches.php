<?php
namespace Model\Entities;

use Model\Gateway;

class isgamtmApplicationBranches extends Gateway\BaseEntity{
    protected $tableName = "isga_mtm_Application_Branches";
    protected $idName = "Id";
    protected $isMtm = true;
}