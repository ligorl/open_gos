<?php
namespace Model\Entities;

use Model\Gateway;

class isgaApplicationDocumentTypes extends Gateway\BaseEntity{
    protected $tableName = "isga_ApplicationDocumentTypes";
    protected $idName = "Id";
}