<?php

namespace Model\Entities;

use Model\Gateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class isgaNotification extends Gateway\BaseEntity{
    protected $tableName = "isga_Notification";
    protected $idName = "Id";


    public function registreNotify($appId,$regNumber,$notifyApplTemplate,$appRegNumber)
    {
        if(empty($regNumber)){
            return false;
        }
        //$regNumber = $this->getRegNumber($appId);

        //проверям наличи уведомлени
        $find = $this
            ->selectComplicated($this->tableName, array(),
                                array('fk_isgaApplication' => $appId,
              'Number'          => $regNumber
            ))->toArray();
        //->getLinkedItem($this->tableName, array('fk_isgaApplication' => $appId,
        //   //'RegNumber'          => $regNumber
        //    ))->toArray();
        if (count($find) > 0) {
            //если уже создано
            $find = $find[0];
            //if ($find['RegNumber'] == $regNumber) {
            //    //и оно то отдаем его
            //
            //    $this->exchangeArray($find);
            //    return $this;
            //}
            //если изменен номер(создан другой)
            //то все обнуляем
            $notifyData['Number'] = $regNumber;
            $notifyData['DateCreation'] = date('Y-m-d');
            $notifyData['DateSigning'] = null;
            //$notifyData['DatePoison'] = null;
            $notifyData['DateSending'] = null;
            $this->exchangeArray($notifyData);
            $this->save();
            return $this;
        } else {
            //если новый документ
            $notifyData['DateCreation'] = date('Y-m-d');
            $notifyData['Number'] = $regNumber;
            $notifyData['ApplicationRegNumber'] =  $appRegNumber;
            $notifyData['fk_isgaApplication'] = $appId;
            $notifyData['fk_isgaNotificationTemplate'] = $notifyApplTemplate;
            $this->exchangeArray($notifyData);
            $this->save(1);
            return $this;
        }
        //return
    }

    /**
     *  ищет запись по уведомлению.
     * @param type $appId
     * @return \Model\Entities\isgaNotification
     */
    public function getByAppId($appId,$templateId)
    {
        if (empty($appId)){
            return null;
        }
        $find = $this
            ->selectComplicated($this->tableName, array(),
                                array('fk_isgaApplication' => $appId,
              'fk_isgaNotificationTemplate'          => $templateId,
            ))->toArray();
        //->getLinkedItem($this->tableName, array('fk_isgaApplication' => $appId,
        //   //'RegNumber'          => $regNumber
        //   ))->toArray();
        if (count($find) > 0){
            $this->exchangeArray($find[0]);
           return  $this;
        }else{
            return false;
        }
    }


    //не перебиралось
   /**
    * Вносит метку о подписе и меняет статус на подписаное
    * 
    * @param type $appId
    * @return int
    */
    public function signNotify($appId,$notyfyTemplate)
    {
        if (empty($appId)) {
            return 0;
        }
        $toStatus['192660B9-977F-4CBD-9E5A-279097849359'] = '342FA3BA-27A5-421C-9FAA-3784E3E7F7A9';
        $toStatus['2F7A2025-FDBF-448A-8635-2AEE51227151'] = 'D870787A-609D-4090-91A7-1D553D1F3175';
        $toStatus['040677F0-45F2-4997-9E33-860B475C8779'] = '73647D3E-744D-40F4-8995-C92B17126269';
        $notify = $this->getLinkedItem($this->tableName, array('fk_isgaApplication' => $appId,'fk_isgaNotificationTemplate'=>$notyfyTemplate))->toArray();
        $app = $this->getLinkedItem('isga_Applications', array('Id' => $appId));
        $oldStatus = $app->getField('fk_isgaApplicationStatus');
        if (empty($oldStatus) || empty($toStatus[$oldStatus])) {
            return 0;
        }
        $notify['DateSigning'] = date('Y-m-d');
        //Здесь надо добавить еще кто куда кому отправил
        $this->exchangeArray($notify);
        $this->save();
        $app->setNewStatus($toStatus[$oldStatus]);
        return 1;
    }

    //не перебиралось
    /*
     * Вносит метку о отправке  и меняет статус на в работу
     */
    public function sendNotify($appId)
    {
        if (empty($appId)) {
            return 0;
        }
        $toStatus['342FA3BA-27A5-421C-9FAA-3784E3E7F7A9'] = '342FA3BA-27A5-421C-9FAA-3784E3E7F7A9';
        $toStatus['2F7A2025-FDBF-448A-8635-2AEE51227151'] = 'D870787A-609D-4090-91A7-1D553D1F3175';
        $toStatus['73647D3E-744D-40F4-8995-C92B17126269'] = 'FFCFD0FF-9CE4-4683-870B-3D87B420C1D3';
        $notify = $this->getLinkedItem($this->tableName, array('fk_isgaApplication' => $appId))->toArray();
        $app = $this->getLinkedItem('isga_Applications', array('Id' => $appId));
        $oldStatus = $app->getField('fk_isgaApplicationStatus');
        if (empty($oldStatus) || empty($toStatus[$oldStatus])) {
            return 0;
        }
        $notify['DateSign'] = date('Y-m-d');
        $this->exchangeArray($notify);
        $this->save();
        $app->setNewStatus($toStatus[$oldStatus]);
        return 1;
    }

    public function getByApplicationTemplate($applicationId,$templateId)
    {
        $where=new \Zend\Db\Sql\Where();
        $where->expression('upper(fk_isgaApplication)=?', strtoupper($applicationId));
        $where->and;
        $where->expression('upper(fk_isgaNotificationTemplate)=?',strtoupper($templateId));
        $result = $this->getLinkedItem(
          $this->tableName,
          $where
          //array('fk_isgaApplication'=>$applicationId,
          //    'fk_isgaNotificationTemplate'=>$templateId,
          //   )
          );
        return $result;
    }

    /**
     * получение количества уведомлений для выгрузки "Квартальный отчет с показателями по количеству уведомлений и отказах"
     * @param $date Date dd.mm.YYY
     * @param $date_end Date dd.mm.YYY
     * @param $notification_template array fk_isgaNotificationTemplate
     * @param $gos int государственный/не государственный
     * @param $filial int головной/филиал
     * @return Integer
     */
    public function geCountForReportNotify($date, $date_end, $notification_template = null, $gos = 0, $filial = 0)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $result = $this->tableGateway->select(function(Select $select) use ($sql, $date, $date_end, $notification_template, $gos, $filial)
            {
                $select
                    ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(isga_Notification.Id)')))
                    ->join('isga_Applications', 'isga_Applications.Id = isga_Notification.fk_isgaApplication', array(), $select::JOIN_LEFT)
                    ->join('isga_mtm_Application_Branches', 'isga_Applications.id = isga_mtm_Application_Branches.fk_isgaApplication', array(), $select::JOIN_LEFT)
                    ->join('eiis_EducationalOrganizations', 'isga_mtm_Application_Branches.fk_eiisEOBranch = eiis_EducationalOrganizations.Id', array(), $select::JOIN_LEFT)
                    ->join('eiis_EducationalOrganizationProperties', 'eiis_EducationalOrganizations.fk_eiisEducationalOrganizationProperties = eiis_EducationalOrganizationProperties.Id', array(), $select::JOIN_LEFT);
                $select->where->lessThanOrEqualTo('isga_Notification.DateSigning', date("Y-m-d", strtotime($date_end)));
                $select->where->greaterThanOrEqualTo('isga_Notification.DateCreation', date("Y-m-d", strtotime($date)));
                if(!empty($notification_template)){
                    $select->where->in('isga_Notification.fk_isgaNotificationTemplate', $notification_template);
                }
                $select->where->equalTo('eiis_EducationalOrganizations.Branch', $filial);
                $select->where->equalTo('eiis_EducationalOrganizationProperties.IsGos', $gos);
            });
        $result = $result->toArray();
        if(!$result) return 0;
        $count = 0;
        foreach($result as $res){
            $count += $res['count'];
        }

        return $count;
    }

    /**
     * получение количества Полный отказ в аккредитации для выгрузки "Квартальный отчет с показателями по количеству уведомлений и отказах"
     * @param $date Date dd.mm.YYY
     * @param $date_end Date dd.mm.YYY
     * @param $gos int государственный/не государственный
     * @param $filial int головной/филиал
     * @return Integer
     */
    public function geCountForReportNotifyRefused($date, $date_end, $gos = 0, $filial = 0)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $result = $this->tableGateway->select(function(Select $select) use ($sql, $date, $date_end, $gos, $filial)
            {
                $select
                    ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(isga_Notification.Id)')))
                    ->join('isga_Applications', 'isga_Applications.Id = isga_Notification.fk_isgaApplication', array(), $select::JOIN_LEFT)
                    ->join('isga_mtm_Application_Branches', 'isga_Applications.id = isga_mtm_Application_Branches.fk_isgaApplication', array(), $select::JOIN_LEFT)
                    ->join('eiis_EducationalOrganizations', 'isga_mtm_Application_Branches.fk_eiisEOBranch = eiis_EducationalOrganizations.Id', array(), $select::JOIN_LEFT)
                    ->join('eiis_EducationalOrganizationProperties', 'eiis_EducationalOrganizations.fk_eiisEducationalOrganizationProperties = eiis_EducationalOrganizationProperties.Id', array(), $select::JOIN_LEFT);
                $select->where->lessThanOrEqualTo('isga_Notification.DateSigning', date("Y-m-d", strtotime($date_end)));
                $select->where->greaterThanOrEqualTo('isga_Notification.DateCreation', date("Y-m-d", strtotime($date)));
                $select->where->equalTo('isga_Applications.fk_OrderDocumentKind', '4E44AE33-A493-4158-A038-4DCEF7B169CA');
                $select->where->equalTo('eiis_EducationalOrganizations.Branch', $filial);
                $select->where->equalTo('eiis_EducationalOrganizationProperties.IsGos', $gos);

            });
        $result = $result->toArray();
        if(!$result) return 0;
        $count = 0;
        foreach($result as $res){
            $count += $res['count'];
        }

        return $count;
    }

}