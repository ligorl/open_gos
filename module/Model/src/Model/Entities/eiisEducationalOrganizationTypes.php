<?php
namespace Model\Entities;

use Model\Gateway;

class eiisEducationalOrganizationTypes extends Gateway\BaseEntity{
    protected $tableName = "eiis_EducationalOrganizationTypes";
    protected $idName = "Id";
}