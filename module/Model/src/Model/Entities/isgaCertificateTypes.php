<?php
namespace Model\Entities;

use Model\Gateway;

class isgaCertificateTypes extends Gateway\BaseEntity{
    protected $tableName = "isga_CertificateTypes";
    protected $idName = "Id";
}