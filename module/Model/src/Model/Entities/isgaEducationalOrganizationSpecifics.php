<?php
namespace Model\Entities;

use Model\Gateway;

class isgaEducationalOrganizationSpecifics extends Gateway\BaseEntity{
    protected $tableName = "isga_EducationalOrganizationSpecifics";
    protected $idName = "Id";

    public function getAll(){
        $result=$this->tableGateway->select(function(Select $select)
            {
                $select
                    ->order('Id ASC')
                    ->where(array());
            });
        return $result;
    }
}