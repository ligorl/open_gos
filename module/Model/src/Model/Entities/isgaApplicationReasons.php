<?php
namespace Model\Entities;

use Model\Gateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class isgaApplicationReasons extends Gateway\BaseEntity{
    protected $tableName = "isga_ApplicationReasons";
    protected $idName = "Id";

    /**
     * получение всех записей в виде массива id => Name
     * @return array
     */
    public function getAllData()
    {
        $result = $this->tableGateway->select(function(Select $select)
        {
            $select
                ->columns(array("id", "Name"));
        });
        $data = array();
        foreach($result as $row){
            $data[$row->id] = $row->Name;
        }
        return $data;
    }

    public function getAll()
    {
        $serviceReasons = $this->getAllByWhere(
            array()
        );
    }

    public function getAllObjects()
    {
        $where = new Where();
        $where->notEqualTo('id', '');
        return $this->getAllByWhere($where);
    }
}