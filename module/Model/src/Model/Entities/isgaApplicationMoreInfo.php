<?php
namespace Model\Entities;

use Model\Gateway;

class isgaApplicationMoreInfo extends Gateway\BaseEntity{
    protected $tableName = "isga_ApplicationMoreInfo";
    protected $idName = "Id";
}