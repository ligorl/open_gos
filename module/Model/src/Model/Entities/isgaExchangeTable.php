<?php
namespace Model\Entities;

use Model\Gateway;

class isgaExchangeTable extends Gateway\BaseEntity{
    protected $tableName = "isga_ExchangeTable";
    protected $idName = "Id";
}