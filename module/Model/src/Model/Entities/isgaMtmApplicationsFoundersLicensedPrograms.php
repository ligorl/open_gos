<?php
namespace Model\Entities;

use Model\Gateway;

class isgaMtmApplicationBranches extends Gateway\BaseEntity{
    protected $tableName = "isga_mtm_Applications_Founders_LicensedPrograms";
    protected $idName = "Id";
}