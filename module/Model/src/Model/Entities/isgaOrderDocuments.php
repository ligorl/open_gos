<?php
namespace Model\Entities;

use Model\Gateway;

class isgaOrderDocuments extends Gateway\BaseEntity{
    protected $tableName = "isga_OrderDocuments";
    protected $idName = "Id";
}