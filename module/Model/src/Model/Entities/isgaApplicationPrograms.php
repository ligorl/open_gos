<?php

namespace Model\Entities;

use Model\Gateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class isgaApplicationPrograms extends Gateway\BaseEntity {

    protected $tableName = "isga_ApplicationPrograms";
    protected $idName = "Id";

    /**
     *коды программ лишение перебивка с красивого названия в код
     * @var type 
     */
    public static $lishenieCodeToId = array(
        'deprivation'   => 1, //лишение
        'removal'       => 2, //исключение 
    );    
    
    private $_licProgram = null;
    public function getLicensedProgram(){
        if($this->_licProgram != null)
            return $this->_licProgram;

        if($this->isEmptyField("fk_eiisLicensedPrograms"))
            return null;

        $result = $this->getLinkedItem("eiis_LicensedPrograms",array(
                "Id" => $this->fields["fk_eiisLicensedPrograms"]
            ));
        $this->_licProgram = $result;
        return $result;
    }

    public function getComliteLicensedProgram($id_type){
        if($this->_licProgram != null)
            return $this->_licProgram;

        if($this->isEmptyField("fk_eiisLicensedPrograms"))
            return null;

        $result = $this->getLinkedItem("eiis_LicensedPrograms",array(
                "Id" => $this->fields["fk_eiisLicensedPrograms"], 'fk_eiisEduProgramType'=> $id_type
            ));
        $this->_licProgram = $result;
        return $result;
    }

    public function isProfy(){
        try {
            $type = $this->getEduProgramType();
            if (!empty($type)) {
                $isProfy = $type->isProfy();
                if (isset($isProfy)) {
                    return $isProfy;
                }

            }


            $licensedProgram = $this->getLicensedProgram();
            if (!$licensedProgram) {
                return null;
            }
            return $eduLevel = $licensedProgram->isProfy();
        }catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }

    }

    private $_EGS = null;
    public function getEGS(){
        if($this->_EGS != null)
            return $this->_EGS;

        if($this->isEmptyField("fk_isgaEnlargedGroupSpecialities")){
            $licProg = $this->getLicensedProgram();
            if(!empty($licProg)){
                $ugs = $licProg->getUgs();
                if(!empty($ugs)){
                    return $ugs;
                }
            }
            return null;
        }

        $result = $this->getLinkedItem("isga_EnlargedGroupSpecialities",array(
                "Id" => $this->fields["fk_isgaEnlargedGroupSpecialities"]
            ));
        $this->_EGS = $result;
        return $result;
    }

    public function getEduProgramType(){
        if($this->isEmptyField("fk_eiisEduProgramTypes"))
            return null;

        $result = $this->getLinkedItem("eiis_EduProgramTypes",array(
                "Id" => $this->fields["fk_eiisEduProgramTypes"]
            ));
        //$this->_licProgram = $result;
        return $result;
    }

    public function getName() {
        if(!$this->isEmptyField('Name')){
            return $this->getField('Name');
        }
        $licensedProgram = $this->getLicensedProgram();
        if(!$licensedProgram){
            return '';
        }
        return  $licensedProgram->getName();
        //$program->getLicensedProgram()->getEduProgram()->getField("Name")
    }

    /**
     * Получаем код образовательной програамы или дергаем код из лицензионной программы
     * @return type
     */
    public function getCode() {
        if(!$this->isEmptyField('Code')){
            return $this->getField('Code');
        }
        $licensedProgram = $this->getLicensedProgram();
        if(!$licensedProgram){
            return '';
        }
        return $licensedProgram->getCode();
        //$program->getLicensedProgram()->getEduProgram()->getField("Name")
    }

    public function getPeriod() {
        if(!$this->isEmptyField('termEdu')){
            return $this->getField('termEdu');
        }
        $licensedProgram = $this->getLicensedProgram();
        return  $licensedProgram->getPeriod();
    }

    /**
     * получение данных для выгрузки
     * @param Array $application_ids
     */
    public function getProgramsByApplicatins($application_ids){

        $result = $this->tableGateway->select(function(Select $select) use ($application_ids)
            {
                $select
                    ->columns(array("fk_eiisLicensedPrograms", "fk_isgaApplication"))
                    ->where(array("fk_isgaApplication" => $application_ids) )
                    ->order('Name ASC');
            });
        $data = array(); $license_ids = array();
        foreach($result as $row){
            $data[$row->fk_isgaApplication][] =  $row->fk_eiisLicensedPrograms;
            $license_ids[] = $row->fk_eiisLicensedPrograms;
        }

        return array("app_data" => $data, "license" => $license_ids);
    }

    public function getCountItems()
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $result = $this->tableGateway->select(function(Select $select) use ($sql)
            {
                $select
                    ->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(isga_ApplicationPrograms.Id)')))
                    ->join('isga_Applications', 'isga_ApplicationPrograms.fk_isgaApplication = isga_Applications.Id', array(), $select::JOIN_LEFT);
                $select->where->greaterThanOrEqualTo('isga_Applications.DateCreate', date("Y-m-d",  strtotime( '2016-03-14')));
                $select->where(array(new \Zend\Db\Sql\Predicate\IsNotNull('isga_Applications.RegNumber')))
                ;
                $sqlString = $sql->getSqlStringForSqlObject($select);
                // var_dump($sqlString);die;
            });
        $result = $result->toArray();
        $result = reset($result);
        return $result['count'];
    }

    public function getItems($page, $step)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $result = $this->tableGateway->select(function(Select $select) use ($sql, $page, $step)
            {
                $page = $page - 1;
                $select
                    ->columns(array("Id","fk_isgaApplication", "fk_isgaEOBranches", "fk_eiisLicensedPrograms", "fk_eiisEduProgramTypes"))
                    ->join('isga_Applications', 'isga_ApplicationPrograms.fk_isgaApplication = isga_Applications.Id', array("fk_isgaApplicationStatus","RegNumber","fk_isgaApplicationReason", "fk_isgaApplicationStatus"), $select::JOIN_LEFT);
                $select->where->greaterThanOrEqualTo('isga_Applications.DateCreate', date("Y-m-d",  strtotime( '2016-03-14')));
                $select->where(array(new \Zend\Db\Sql\Predicate\IsNotNull('isga_Applications.RegNumber')))
                ;
                $select->order('isga_ApplicationPrograms.Id DESC')
                    ->limit($step);
                if($page > 0){
                    $offset = $page * $step;
                    $select->offset($offset);
                }
                // $sqlString = $sql->getSqlStringForSqlObject($select);
            });

        $data = array(); $organizations_ids = array(); $programs = array(); $statuses = array();$pr_types = array();
        foreach($result as $row){
            $row->Id = mb_strtoupper($row->Id, "UTF-8");
            $data[$row->Id]["fk_isgaApplication"] = mb_strtoupper($row->fk_isgaApplication, "UTF-8");
            $data[$row->Id]["fk_isgaEOBranches"] = mb_strtoupper($row->fk_isgaEOBranches, "UTF-8");
            $data[$row->Id]["fk_eiisLicensedPrograms"] = mb_strtoupper($row->fk_eiisLicensedPrograms, "UTF-8");
            $data[$row->Id]["fk_eiisEduProgramTypes"] = mb_strtoupper($row->fk_eiisEduProgramTypes, "UTF-8");
            $data[$row->Id]["fk_isgaApplicationStatus"] = mb_strtoupper($row->fk_isgaApplicationStatus, "UTF-8");
            $data[$row->Id]["RegNumber"] = $row->RegNumber;
            $data[$row->Id]["fk_isgaApplicationReason"] = $row->fk_isgaApplicationReason;
            $data[$row->Id]["fk_isgaApplicationStatus"] = $row->fk_isgaApplicationStatus;

            $organizations_ids[] = mb_strtoupper($row->fk_isgaEOBranches, "UTF-8");
            $programs[] = mb_strtoupper($row->fk_eiisLicensedPrograms, "UTF-8");
            $app_ids[] = mb_strtoupper($row->fk_isgaApplication, "UTF-8");
            $statuses[] = mb_strtoupper($row->fk_isgaApplicationStatus, "UTF-8");
            $reasons[] = mb_strtoupper($row->fk_isgaApplicationReason, "UTF-8");
            $pr_types[] = mb_strtoupper($row->fk_eiisEduProgramTypes, "UTF-8");
        }
        return array("data" => $data, "organizations_ids" => $organizations_ids, "programs" => $programs, "app" => $app_ids, "statuses" => $statuses, "reasons" => $reasons , "pr_types" => $pr_types);
    }

    public function getEduLevel() {
        $licProgramm = $this->getLicensedProgram();
        if(!empty($licProgramm)){
            $eduLevel = $licProgramm->getEduProgramLevel();
            if(!empty($eduLevel)){
                return $eduLevel;
            }
            $eduType = $licProgramm->getEduProgramType();
            $eduLevel = $eduType->getEduLevel();
            if(!empty($eduLevel)){
                return $eduLevel;
            }
        }
        $eduType = $this->getEduProgramType();
        $eduLevel = $eduType->getEduLevel();
        if(!empty($eduLevel)){
            return $eduLevel;
        }
        return null;
    }
    public function getProgramInfoDeprivation(){
        $result = $this->getLinkedItem("isga_AccreditedPrograms",array(
                "Id" => $this->fields["fk_eiisLicensedPrograms"]
        ));
        return $result;
    }
}