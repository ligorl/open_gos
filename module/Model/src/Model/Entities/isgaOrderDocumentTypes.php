<?php
namespace Model\Entities;

use Model\Gateway;

class isgaOrderDocumentTypes extends Gateway\BaseEntity{
    protected $tableName = "isga_OrderDocumentTypes";
    protected $idName = "Id";
}