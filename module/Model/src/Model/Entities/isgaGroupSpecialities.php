<?php
namespace Model\Entities;

use Model\Gateway;

class isgaGroupSpecialities extends Gateway\BaseEntity{
    protected $tableName = "isga_GroupSpecialities";
    protected $idName = "Id";

    public function getEnlargedGroupSpeciality(){
        $idUGS =  $this->fields['fk_isgaEGS'];
        if (!$idUGS || strtolower($idUGS) == "null"){
            return null;
        }
        $result = $this->getLinkedItem(
            'isga_EnlargedGroupSpecialities', array('Id' => $idUGS)
        );
        return $result;
    }
}