<?php
namespace Model\Entities;

use Model\Gateway;

class isgaCertificateStatuses extends Gateway\BaseEntity{
    protected $tableName = "isga_CertificateStatuses";
    protected $idName = "Id";
}