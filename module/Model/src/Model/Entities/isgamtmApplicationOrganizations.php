<?php
namespace Model\Entities;

use Model\Gateway;

class isgamtmApplicationOrganizations extends Gateway\BaseEntity{
    protected $tableName = "isga_mtm_Application_Organizations";
    protected $idName = "Id";
    protected $isMtm = true;
}