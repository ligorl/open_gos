<?php
namespace Model\Entities;

use Model\Gateway;

class eiisEduProgramTypes extends Gateway\BaseEntity{
    protected $tableName = "eiis_EduProgramTypes";
    protected $idName = "Id";

    protected $_EduPrograms;
    public function getEduPrograms(){
        /*
    	if($this->_EduPrograms == null){
            $this->_EduPrograms = $this->getLinkedItems("eiis_EduPrograms",array(
                "fk_eiisEduProgramType" => "fk_eiisEduProgramType"
            ));
        }*/
        $linkedTable = new Gateway\EntitiesTable("eiis_EduPrograms");

        $fields = $this->fields;
        //$linkedTable->select($fields);die;
        $this->_EduPrograms = $linkedTable->select(function(\Zend\Db\Sql\Select $select) use ($fields){
            $select->columns(array(
                new \Zend\Db\Sql\Expression("DISTINCT(Code) AS Code"),
                new \Zend\Db\Sql\Expression("MAX(Id) AS Id"),
                new \Zend\Db\Sql\Expression("MAX(Name) AS Name"),
                new \Zend\Db\Sql\Expression("MAX(fk_eiisEduProgramType) AS fk_eiisEduProgramType"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaGS) AS fk_isgaGS"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaQualification) AS fk_isgaQualification"),
                new \Zend\Db\Sql\Expression("MAX(QualificationGrade) AS QualificationGrade"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaEduStandard) AS fk_isgaEduStandard")
                )
            ) 
            ->where
            ->nest
                ->equalTo('fk_isgaEduStandard', '3D90D5F1-E8B5-45E9-8B5F-BBF9FAD47A06')
                ->or
                ->equalTo('fk_isgaEduStandard', '14DA0D98-F4BB-466C-876C-E9916C22AC9F')
                ->or
                ->equalTo('fk_isgaEduStandard', 'A08D5DE2-E283-4169-BF37-DC0F6539CC89')
            ->unnest
            ->and
            ->equalTo("fk_eiisEduProgramType", $fields['Id']);
            $select
            ->group('Code');

        });        

        return $this->_EduPrograms;
    }

    public function getEduLevel($param=null)
    {
        if (empty($this->fields['fk_eiisEduLevels']) || strtolower($this->fields['fk_eiisEduLevels']) == "null"){
            return null;
        }

        $eduProgramType = $this->getLinkedItem(
          'eiis_EduLevels', array('Id' =>$this->fields['fk_eiisEduLevels'] )
        );
        if ($param !== null) {
            $result = $eduProgramType->getField($param);
        } else {
            $result = $eduProgramType;
        }
        return $result;
    }
    
    public function getEduProgramsList(){
        $epTable = new Gateway\EntitiesTable("eiis_EduPrograms");
        $epList = $epTable->getAllEntities(null,"Code ASC",array(
            new \Zend\Db\Sql\Predicate\Like("fk_eiisEduProgramType" , $this->getId()),
            "actual" => '1',
        ));

        return $epList;
    }

    /**
     * определялка тип программы общего или професионального образования
     * @return type
     */
    public function isProfy(){
        $eduLevel=  $this->getEduLevel();
        if(!$eduLevel){
            return null;
        }
        return $eduLevel->isProfy();
    }

    /**
     * дергаем имя для лода
     * 
     * @return type
     */
    public function getLodTitle() {
        $lodTitle = $this->getFieldOrSpace('LodTitle');
        if(empty($lodTitle)){
            $lodTitle = $this->getFieldOrSpace('Name');
        }
        return $lodTitle;
    }

    
    //последняя строка
}
