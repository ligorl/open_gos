<?php
namespace Model\Entities;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Model\Gateway;

class eiisEduPrograms extends Gateway\BaseEntity{
    protected $tableName = "eiis_EduPrograms";
    protected $idName = "Id";

    
    public function getParentEGSId(){
        if($this->isEmptyField("fk_eiisUgs"))
            return null;

        return $this->fields["fk_eiisUgs"];
    }

    public function getParentGS(){
        if($this->isEmptyField("fk_eiisGS"))
            return null;
        $result = $this->getLinkedItem("isga_GroupSpecialities",array(
                "Id" => $this->fields["fk_eiisGS"]
            ));
        return $result;
    }

    public function getStandardName(){
        if($this->isEmptyField("fk_eiisEduStandard"))
            return null;
        $result = $this->getLinkedItem("isga_EduStandards",array(
                "Id" => $this->fields["fk_eiisEduStandard"]
            ));
        return $result->getField("Name");
    }

    public function getParentEgs(){
        if($this->isEmptyField("fk_eiisUgs"))
            return null;

        $result = $this->getLinkedItem("isga_EnlargedGroupSpecialities",array(
                "Id" => $this->fields["fk_eiisUgs"]
            ));
        return $result;
    }

    public function getQualification(){
        if($this->isEmptyField("fk_isgaQualification"))
            return null;

        $result = $this->getLinkedItem("isga_Qualifications",array(
                "Id" => $this->fields["fk_isgaQualification"]
            ));
        return $result;
    }
    
    public function getQualificationName(){
        $result = '';
        $qualificationClass = $this->getQualification();
        if(!empty($qualificationClass)){
            $result = $qualificationClass->getFieldOrSpace('Name');
        }
        if(empty($result)){
            $result = $this->getFieldOrSpace('QualificationName');
        }
        return $result;
    }

    public function getQualificationCode(){
        $result = '';
        $qualificationClass = $this->getQualification();
        if(!empty($qualificationClass)){
            $result = $qualificationClass->getFieldOrSpace('Code');
        }
        if(empty($result)){
            $result = $this->getFieldOrSpace('QualificationCode');
        }
        return $result;
    }
    
    

    public function getEduProgramType(){
        if($this->isEmptyField("fk_eiisEduProgramType"))
            return null;

        $result = $this->getLinkedItem("eiis_EduProgramTypes",array(
                "Id" => $this->fields["fk_eiisEduProgramType"]
            ));
        return $result;
    }

    public function getEduProgramLevel()
    {
        if($this->isEmptyField("fk_eiisEduLevels")){
            return null;
        }

        $result = $this->getLinkedItem("eiis_EduLevels",array(
                "Id" => str_replace('-','',strtoupper($this->fields["fk_eiisEduLevels"]))
            ));
        return $result;

    }

    public function getNameProgram($id){
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->columns(array('Name','Code'));
        $select->from('eiis_EduPrograms');
        $select->where(array('Id'=>$id));
        $selectString = $sql->getSqlStringForSqlObject($select);//print $selectString;die;
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    public function getProgramsByEducation($level_id){
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new Sql($dbAdapter);

        $results = $dbAdapter->query("SELECT `eiis_EduPrograms`.`Name` AS `Name`, `eiis_EduPrograms`.`Code` AS `Code`, `eiis_EduPrograms`.`Id` AS `Id` FROM 	`eiis_EduPrograms` WHERE fk_eiisEduLevels = '{$level_id}' AND Name is not null AND Name NOT in ('NU', 'void', 'NULL') GROUP BY `Name`, `Code` ORDER BY Name asc", $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    public function getPeriod() {
        if(!$this->isEmptyField('Period')){
            return $this->getField('Period');
        }
        return '';
    }

    /**
     * получение записей для отчета
     * @param Array $ids ид программ
     * @return Array
     */
    public function getItemsForReposrts($ids)
    {
        if(count($ids) == 0) return array("data" => array());
        $fields = array("Id", "Name", "fk_eiisEduLevels", "UgsName", "UgsCode", "fk_eiisEduProgramType", "QualificationName");
        $result = $this->tableGateway->select(function(Select $select) use ($ids, $fields)
            {
                $select
                    ->columns($fields)
                    ->where(array('Id' => $ids))
                ;

            });

        $data = array();
        foreach($result as $row){
            foreach($fields as $field){
                $data[mb_strtoupper($row->Id, "UTF-8")][$field] = $row->$field;
            }
        }
        return array("data" => $data);
    }
    
    public function getLodQualifications(){
        $mtmQualificationsTable = new \Model\Gateway\LodEntitiesTable("EducationProgramCIToQualification");
        $mtm = $mtmQualificationsTable->getAllEntities(null,null,[
            "educationProgramId" => $this->getId()
        ]);
        $qualIds = [];
        foreach($mtm as $link)
            $qualIds[] = $link->getField("qualificationId");

        $returnArray = array();

        if(count($qualIds) > 0) {
            $qualificationsTable = new \Model\Gateway\EntitiesTable("isga_Qualifications");
            $returnArray = $qualificationsTable->getAllEntities(null, null, [
                new \Zend\Db\Sql\Predicate\In("Id", $qualIds)
            ])->getObjectsArray();
        }
        return $returnArray;
    }

    //последняя строка
}