<?php
namespace Model\Entities;

use Model\Gateway;

class BugReports extends Gateway\BaseEntity{
    protected $tableName = "BugReports";
    protected $idName = "Id";

    public function save($isNew = false, $doNotSaveToExchangeTable = false){
        //���� ��� RevisionId
        if(isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $ret = $this->tableGateway->update($this->fields,array($this->idName => $this->fields[$this->idName]));
            $saveType = "update";
            $this->saveToExchangeTable($this->fields[$this->idName], $saveType);
            return $ret;
        }
        //insert
        else{
            $this->tableGateway->insert($this->fields);

            $newId = $this->tableGateway->lastInsertValue;

            $saveType = "insert";
            $this->saveToExchangeTable($newId, $saveType);
        }
        return false;
    }
}