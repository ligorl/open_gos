<?php

namespace Model\Entities;

use Model\Gateway;

class eiisEduLevels extends Gateway\BaseEntity
{
    protected $tableName = "eiis_EduLevels";
    protected $idName = "Id";
    
    
    public function getPrintName()
    {
        $printName =  $this->getField('PrintName');
        if(!empty($printName)){
            return $printName;
        }
        $printName =  $this->getField('Name');
        return $printName;
    }

    public function getEduProgramTypes(){
        $eptTable = new Gateway\EntitiesTable("eiis_EduProgramTypes");
        $eptList = $eptTable->getAllEntities(null,"Name ASC",[
            "fk_eiisEduLevels" => $this->getId()
        ])->getObjectsArray();
        return $eptList;
    }

    public function getLinkedProgram(){
        if(!$this->isEmptyField("fk_eiisEduProgram")){
            $ep = new eiisEduPrograms($this->getField("fk_eiisEduProgram"));
            return $ep;
        }
        return null;
    }
    
    /**
     * Получаем подуровни
     */
    public function getSublevels(){
        $lvlTable = new Gateway\EntitiesTable("eiis_EduLevels");
        $sublevels = $lvlTable->getAllEntities(null,"Name ASC",[
            "parentlevel" => $this->getId()
        ])->getObjectsArray();
        return $sublevels;
    }

    /**
     * получим родительски уровень образования
     * 
     * @return type
     */
    public function getParentLevel() {
        if($this->isEmptyField('parentlevel')){
            return null;
        }
        $parentLevel = new self;
        $parentLevel = $this->getById($this->getField('parentlevel'));
        return $parentLevel;
    }

    /**
     * возращает это образовательная программа илиобщее
     *
     *  поправил, в IsProgram:  0-ooo 1-впо 2-спо
     * 
     * @return boolean
     *          true - профецианальное
     *          falde - общее
     */
    public function isProfy(){    
        $isProfy = $this->getField('IsProgram');
        if (  $isProfy ==1 || $isProfy =='1'
           || $isProfy ==2 || $isProfy =='2'  
        ){
            return true;
        } else {
            return false;
        }
    }

    
    /**
     * дергаем надпись для документа,если нет то название
     * @return type
     */
    public function getForDocumentTitle()
    {
        $printName =  $this->getFieldHtml('forDocumentTitle');
        if(!empty($printName)){
            return $printName;
        }
        $printName =  $this->getFieldHtml('Name');
        return $printName;
    }

    
    //последняя строка
}
