<?php
namespace Model\Entities;

use Zend\Db\Sql\Sql;
use Model\Gateway;

class ronUsers extends Gateway\BaseEntity{
    protected $tableName = "ron_Users";
    protected $idName = "Id";

    /**
     * Метод возвращает объект связаннго учебного учреждения
     * @return Gateway\ResultSet|null
     */
    public function getEducationOrganization(){
        if($this->isEmptyField("fk_eiisEducationalOrganization") ||
            $this->fields["fk_eiisEducationalOrganization"] == ""
        )
            return null;

        $result = $this->getLinkedItem("eiis_EducationalOrganizations",array(
            "Id" => $this->fields["fk_eiisEducationalOrganization"]
        ));
        return $result;
    }
	
	public function getExpertTable(){
        $result = $this->getLinkedItem("expert",array(
            "id" => $this->fields["id"]
        ));
        return $result;
    }
	
    public function getExpertProgram(){
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('expert_edu_programms');
        $select->where(array('expert_id'=>$this->fields['id']));
        $select->order(array('education_level_id'=>'ASC','speciality_group_id'=>'ASC'));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }
	
    public function getExpertCertificates(){
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('expert_certificates');
        $select->where(array('expert_id'=>$this->fields['id']));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    private $_role = null;
    public function getRole(){
        if($this->_role != null)
            return $this->_role;

        $result = $this->getLinkedItem("ron_UserRole",array(
            "Id" => $this->fields["fk_isgaUserRole"]
        ));
        return $result;
    }

    /**
     * Метод для проверки правд доступа
     * @param $accessName - совпадает с полем в бд
     * @return bool
     */
    public function hasAccess($accessName){
        $role = $this->getRole();
        if($role->getField($accessName) == 1)
            return true;
        else
            return false;
    }
}