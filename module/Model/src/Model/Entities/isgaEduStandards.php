<?php
namespace Model\Entities;

use Model\Gateway;

class isgaEduStandards extends Gateway\BaseEntity{
    protected $tableName = "isga_EduStandards";
    protected $idName = "Id";
}