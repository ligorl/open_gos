<?php
namespace Model\Entities;

use Model\Gateway;

class eiisLicenses extends Gateway\BaseEntity{
    protected $tableName = "eiis_Licenses";
    protected $idName = "Id";

    public function getStatusName(){
        if(!isset($this->fields["fk_eiisLicenseState"]) || $this->fields["fk_eiisLicenseState"] == "" || strtolower($this->fields["fk_eiisLicenseState"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_LicenseStates",array(
            "Id" => $this->fields["fk_eiisLicenseState"]
        ));
        if($result)
            return $result->getField("Name");
        else
            return "";
    }

    public function getStatus(){
        if(!isset($this->fields["fk_eiisLicenseState"]) || $this->fields["fk_eiisLicenseState"] == "" || strtolower($this->fields["fk_eiisLicenseState"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_LicenseStates",array(
            "Id" => $this->fields["fk_eiisLicenseState"]
        ));
        if($result)
            return $result;
        else
            return null;
    }

    public function getSupplements(){
        $supplements = $this->getLinkedItems("eiis_LicenseSupplements",array(
            "fk_eiisLicense" => $this->fields["Id"]
        ));

        return $supplements;
    }

    public function isValid(){
        if(!isset($this->fields["fk_eiisLicenseState"]) || $this->fields["fk_eiisLicenseState"] == "" || strtolower($this->fields["fk_eiisLicenseState"]) == "null")
            return false;

        $result = $this->getLinkedItem("eiis_LicenseStates",array(
            "Id" => $this->fields["fk_eiisLicenseState"]
        ));

        if($result && $result->getField("IsValid") == 1)
            return true;
        else
            return false;
    }

    public function getDocumentFromId( $licenseId){
        try {
            $licAndLicSupp = new \Model\LodEntities\LicenseAndLicenseSupplementToDocument();
            $licAndLicSupp = $licAndLicSupp->getDataJoin_ArrayCondition(
                [
                    'documentId' => new \Zend\Db\Sql\Expression('Document.Id'),
                    'dealId' => new \Zend\Db\Sql\Expression('Document.dealId'),
                    'registrationDate' => new \Zend\Db\Sql\Expression('Document.registrationDate'),
                    'registrationNumber' => new \Zend\Db\Sql\Expression('Document.registrationNumber'),
                    'documentTypeId' => new \Zend\Db\Sql\Expression('Document.documentTypeId')
                ],
                [
                    'table' => 'Document',
                    'binder' => 'LicenseAndLicenseSupplementToDocument.documentId = Document.id',
                    'type' => 'left'
                ],
                [
                    ['column' => 'LicenseAndLicenseSupplementToDocument.licenseId="' . $licenseId . '"'],
                    ['and'],
                    ['column' => 'LicenseAndLicenseSupplementToDocument.documentDecisionTypeId="e0e8eb9952464ea798f7e41b6a81c93f"']
                ]);
            return $licAndLicSupp->current();
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license_request");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    public function getEndDocumentFromId( $licenseId){
        $licAndLicSupp = new \Model\LodEntities\LicenseAndLicenseSupplementToDocument();

        $licAndLicSupp = $licAndLicSupp->getDataJoin_ArrayCondition(
            [
                'documentId' => new \Zend\Db\Sql\Expression('Document.Id'),
                'dealId' => new \Zend\Db\Sql\Expression('Document.dealId'),
                'registrationDate' =>  new \Zend\Db\Sql\Expression('Document.registrationDate'),
                'registrationNumber' => new \Zend\Db\Sql\Expression('Document.registrationNumber'),
                'documentTypeId' => new \Zend\Db\Sql\Expression('Document.documentTypeId')
            ],
            [
                'table' => 'Document',
                'binder' => 'LicenseAndLicenseSupplementToDocument.documentId = Document.id',
                'type' => 'left'
            ],
            [
                ['column'=>'LicenseAndLicenseSupplementToDocument.licenseId="'.$licenseId.'"'],
                ['and'],
                ['column'=>'LicenseAndLicenseSupplementToDocument.documentDecisionTypeId="1dc3e282134a4b7e9578216e6f07f096"']
            ]);

        return $licAndLicSupp->current();
    }

    public function getControlOrgan(){
        if($this->isEmptyField("fk_eiisControlOrgan"))
            return null;

        $result = $this->getLinkedItem("eiis_ControlOrgan",array(
            "Id" => $this->fields["eiisControlOrgan"]
        ));
        return $result;
    }

    public function getOtherDocumentFromId( $licenseId){
        try {
            $licAndLicSupp = new \Model\LodEntities\LicenseAndLicenseSupplementToDocument();
            $licAndLicSupp = $licAndLicSupp->getDataJoin_ArrayCondition(
                [
                    'documentId' => new \Zend\Db\Sql\Expression('Document.Id'),
                    'dealId' => new \Zend\Db\Sql\Expression('Document.dealId'),
                    'registrationDate' => new \Zend\Db\Sql\Expression('Document.registrationDate'),
                    'registrationNumber' => new \Zend\Db\Sql\Expression('Document.registrationNumber'),
                    'documentTypeId' => new \Zend\Db\Sql\Expression('Document.documentTypeId')
                ],
                [
                    'table' => 'Document',
                    'binder' => 'LicenseAndLicenseSupplementToDocument.documentId = Document.id',
                    'type' => 'left'
                ],
                [
                    ['column' => 'LicenseAndLicenseSupplementToDocument.licenseId="' . $licenseId . '"'],
                    ['and'],
                    ['column' => 'LicenseAndLicenseSupplementToDocument.documentDecisionTypeId'],
                    ['not'],
                    [
                        'in' => [
                            '"1dc3e282134a4b7e9578216e6f07f096"',
                            '"e0e8eb9952464ea798f7e41b6a81c93f"'
                        ]
                    ]
                ]);
            return $licAndLicSupp;
        } catch (\Exception $e){
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }

    }
    
    /**
     * @param $isSorting - флаг, если указан, то приложения сортируются по номеру
     * @return mixed
     */
    public function getValidSupplements($isSorting){
        $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
        $subselectState = $sql->select();

        $subselectState->from('eiis_LicenseSupplementStates')
            ->columns(array(
                "Id" => new \Zend\Db\Sql\Expression("Id")
            ))
            ->where(array(
                "lodCode" => "VALID"
            ));

        $supplementsTable = new Gateway\EntitiesTable("eiis_LicenseSupplements");

        $order= null;
        if($isSorting)
            $order = new \Zend\Db\Sql\Expression("INET_ATON(SUBSTRING_INDEX(CONCAT(f11_mon.eiis_LicenseSupplements.Number,'.0.0.0'),'.',4)) ASC");

        $supplements = $supplementsTable->getAllEntities(null, $order,[
            new \Zend\Db\Sql\Predicate\In("fk_eiisLicenseSupplementState", $subselectState),
            "fk_eiisLicense" => $this->fields["Id"]
        ])->getObjectsArray();

        return $supplements;
    }   
 

    public function getScanFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('License', $this->getId());
        return $result;
    }

    /**
     * дергаем здапись о файле, который подписан
     * @return type
     */
    public function getDocumentFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('LicenseDocument', $this->getId());
        return $result;
    }

    /**
     * дергаем подпись подписаного файла
     * @return type
     */
    public function getSignFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('LicenseSign', $this->getId());
        return $result;
    }

    public function getPrintFile()
    {
        if ($this->isEmptyField($this->idName)) {
            return null;
        }
        //здесь вываливается
        //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
        //не занаю что это
        @$fileBank = new \Model\LodEntities\EiFile();
        $result = $fileBank->getFileByCodeId('LicensePrint', $this->getId());
        return $result;
    }
    
    //последняя строка
}