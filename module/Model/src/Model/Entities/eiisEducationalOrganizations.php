<?php
namespace Model\Entities;

use Model\Gateway;

class eiisEducationalOrganizations extends Gateway\BaseEntity{
    protected $tableName = "eiis_EducationalOrganizations";
    protected $idName = "Id";

    static public function geyByOGRN($ogrn){
        $table = new Gateway\EntitiesTable("eiis_EducationalOrganizations");
        $result = $table->getEntitiesByWhere([
            "GosRegNum" => $ogrn
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

    public function getEducationalOrganizationTypeName(){
        if(!isset($this->fields["fk_eiisEducationalOrganizationType"]) || $this->fields["fk_eiisEducationalOrganizationType"] == "" || strtolower($this->fields["fk_eiisEducationalOrganizationType"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_EducationalOrganizationTypes",array(
            "Id" => $this->fields["fk_eiisEducationalOrganizationType"]
        ));
        return $result->getField("Name");
    }


    public function getEducationalOrganizationKindName(){
        if(!isset($this->fields["fk_eiisEducationalOrganizationKind"]) ||
            $this->fields["fk_eiisEducationalOrganizationKind"] == "" || strtolower($this->fields["fk_eiisEducationalOrganizationKind"]) == "null"
        )
            return null;

        $result = $this->getLinkedItem("eiis_EducationalOrganizationKinds",array(
            "Id" => $this->fields["fk_eiisEducationalOrganizationKind"]
        ));
        return $result->getField("Name");
    }

    public function getOrgFormName(){
        if(!isset($this->fields["fk_eiisEducationalOrganizationProperties"]) ||
            $this->fields["fk_eiisEducationalOrganizationProperties"] == "" || strtolower($this->fields["fk_eiisEducationalOrganizationProperties"]) == "null"
        )
            return null;

        $result = $this->getLinkedItem("eiis_EducationalOrganizationProperties",array(
            "Id" => $this->fields["fk_eiisEducationalOrganizationProperties"]
        ));
        if($result!=null)
            return $result->getField("Name");

        return "";
    }

    public function getFounderName(){
        $founderRows = $this->getLinkedItems("isga_mtm_Founders_EducationalOrganizations",array(
            "fk_eiisEducationalOrganization" => $this->fields["Id"]
        ));

        $founderNames = array();
        foreach($founderRows as $founderRow){
            $founder = $this->getLinkedItem("isga_Founders", array(
                "Id" => $founderRow->getField("fk_isgaFounder")
            ));
            if($founder != null)
                $founderNames[] = $founder->getFounderName();
        }
        if(count($founderNames) > 0)
            return implode(", ",$founderNames);
        else
            return null;
    }

    public function getCountryName(){
        if(!isset($this->fields["fk_eiisCountry"]) || $this->fields["fk_eiisCountry"] == "" || strtolower($this->fields["fk_eiisCountry"]) == "null"){
            return "Российская Федерация";
        }

        $result = $this->getLinkedItem("eiis_Countries",array(
            "Id" => $this->fields["fk_eiisCountry"]
        ));

        if($result == null)
            return $this->fields["fk_eiisCountry"];

        return $result->getField("Name");
    }

    public function getRegionName(){
        if(!isset($this->fields["fk_eiisRegion"]) || $this->fields["fk_eiisRegion"]=="" || strtolower($this->fields["fk_eiisRegion"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_Regions",array(
            "Id" => $this->fields["fk_eiisRegion"]
        ));
        return $result->getField("Name");
    }

    public function getFedOkrName(){
        if(!isset($this->fields["fk_eiisRegion"]) || $this->fields["fk_eiisRegion"]=="" || strtolower($this->fields["fk_eiisRegion"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_Regions",array(
            "Id" => $this->fields["fk_eiisRegion"]
        ));
        return $result->getFedOkrName();
    }

    public function getLAddress(){
        $address = array();
        if(!$this->isEmptyField('LawAddress'))
            $address[] = $this->fields['LawAddress'];
        else{
            if(!$this->isEmptyField('LawPostIndex'))
                $address[] = $this->fields['LawPostIndex'];
            if(!$this->isEmptyField('LawCity'))
                $address[] = $this->fields['LawCity'];
            if(!$this->isEmptyField('LawStreet'))
                $address[] = $this->fields['LawStreet'];
            if(!$this->isEmptyField('LawHouse'))
                $address[] = $this->fields['LawHouse'];
        }
        return implode(", ",$address);
    }

    public function getPAddress(){
        $address = array();
        if(!$this->isEmptyField('Address') && strlen($this->fields['Address']) > 10)
            $address[] = $this->fields['Address'];
        else {
            if(!$this->isEmptyField('PostIndex'))
                $address[] = $this->fields['PostIndex'];
            if(!$this->isEmptyField('TownName'))
                $address[] = $this->fields['TownName'];
            if(!$this->isEmptyField('Street'))
                $address[] = $this->fields['Street'];
            if(!$this->isEmptyField('House'))
                $address[] = $this->fields['House'];
        }

        return implode(", ",$address);
    }

    /**
     * Функция возвращает юридический адрес
     * @return string
     */
    public function getAddressLaw()
    {
        $address = array();
        if (!$this->isEmptyField('LawAddress'))
            $address[] = $this->fields['LawAddress'];
        else {
            if (!$this->isEmptyField('LawPostIndex'))
                $address[] = $this->fields['LawPostIndex'];
            if (!$this->isEmptyField('LawCity'))
                $address[] = $this->fields['LawCity'];
            if (!$this->isEmptyField('LawStreet'))
                $address[] = $this->fields['LawStreet'];
            if (!$this->isEmptyField('LawHouse'))
                $address[] = $this->fields['LawHouse'];
        }

        return implode(", ", $address);
    }

    protected $_affilates = null;
    public function getAffilates(){
        if($this->_affilates == null){
            $this->_affilates = $this->getLinkedItems("eiis_EducationalOrganizations",array(
                "fk_eiisParentEducationalOrganization" => $this->fields["Id"]
            ));
        }

        return $this->_affilates;
    }

    protected $_affilatesCount = null;
    public function getAffilatesNumber(){
        if($this->_affilatesCount != null){
            return $this->_affilatesCount;
        } else {
            $res = $this->getAffilates();
            $count = 0;
            foreach ($res as $affilate) {
                if($affilate->isValidLicenseSupplement())
                    $count++;
            }
            $this->_affilatesCount = $count;
            return $this->_affilatesCount;
        }
        
    }

    public function getAllLicenses(){
        $licTable = new Gateway\EntitiesTable("eiis_Licenses");
        $licenses = $licTable->getAllEntities(null,"DateReg ASC",[
            "fk_eiisEducationalOrganization" => $this->fields["Id"]
        ]);
        return $licenses->getObjectsArray();
    }

    public function getLicense(){
        $result = $this->getLinkedItems("eiis_Licenses",array(
            "fk_eiisEducationalOrganization" => $this->fields["Id"]
        ))->getObjectsArray();

        $last = null;
        $maxDate = 0;

        $hasValid = false;
        $valid = null;

        foreach($result as $license){
            $dateReg = strtotime($license->getField("DateReg"));
            if($dateReg >= $maxDate){
                $maxDate = $dateReg;
                $last = $license;
            }            
            if($license->isValid()){
                $hasValid = true;
                $valid = $license;
                break;
            }
        }

        if($hasValid)
            return $valid;
        else 
            return $last;
    }

    public function getCertificates($param = null){
        /*
        $eoTable = new \Model\Gateway\EntitiesTable("isga_Certificates");
        $tg = $eoTable->getGateway();

        //Строим запрос на получение всех связанных филиалов

        //Подзапрос 1, Лицензии
        $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
        $subselectLicenses = $sql->select();

        $subselectLicenses->from('eiis_Licenses')
            ->columns(array('Id'))
            ->where(array('fk_eiisEducationalOrganization' => $this->getField("Id")));

        //Подзапрос 2, Заявки
        $sql2 = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
        $subselectApps = $sql2->select();

        $subselectApps->from('isga_Applications')
            ->columns(array('Id'))
            ->where->in('fk_eiisLicense', $subselectLicenses);

        //Запрос
        $resultSet = $tg->select(function(\Zend\Db\Sql\Select $select) use ($subselectApps, $tg){
            $select
                ->where->in('fk_oisgaApplication', $subselectApps);

            //$sql = $tg->getSql();
            //print_r($sql->getSqlstringForSqlObject($select));die;
        });
        return $resultSet;
        */
        //небольшой фикс для того что бы правильно отрабатывало GUID организации. Добавляю еще поиск по Id со слешами
        $dashedId=$this->fillIdDashes($this->stripIdDashes($this->fields["Id"]));

        $where = array(
            "fk_eiisEducationalOrganization" => array($this->fields["Id"],$dashedId)
        );

        if($param != null && $param == "noMakets")
            $where[] = new \Zend\Db\Sql\Predicate\NotIn("fk_isgaCertificateStatus",array("abca519a-de38-4c46-a85f-c0b4503fb5e8"));

        $certificates = $this->getLinkedItems("isga_Certificates", $where);

        return $certificates;
    }

    public function getEducationalOrganizationData(){
        //костыль на даные об образовательной организации
        $eduOrgData=new \Model\Entities\isgaEducationalOrganizationData();
        $eduOrgData->checkAddIfNot($this->fields["Id"]);
        $result = $this->getLinkedItem("isga_EducationalOrganizationData",array(
            "fk_eiisEducationalOrganization" => $this->fields["Id"]
        ));
        return $result;
    }

    //Проверяем если связанное приложение к лицензии еще действует
    //Возвращает true/false в зависимости от результатов проверки
    public function isValidLicenseSupplement(){
        //получаем списов связанных LicenseSupplement
        $lsups = $this->getLinkedItems("eiis_LicenseSupplements",array(
            "fk_eiisBranch" => $this->fields["Id"]
        ));

        $retValue = false;

        //Проходим по всем приложениям
        foreach($lsups->getObjectsArray() as $lsup){
            if($lsup->isValid()){
                $retValue = true;
                break;
            }

        }
        return $retValue;
    }

    /**
     * Метод возвращает заявки
     */
    public function getApplications(){
        $predicate = new  \Zend\Db\Sql\Where();
        $predicate
            ->equalTo("fk_eiisEducationalOrganization", $this->fields["Id"])
            ->nest()
            ->equalTo("fk_isgaApplicationStatus","38D871AC-BF1D-469E-AFC1-7A6CD55EF1B6")
            ->or
            ->equalTo("fk_isgaApplicationStatus","A3B952BF-628E-446C-B865-E483503241E8")
            ->or
            ->isNull("fk_isgaApplicationStatus")
            ->unnest();
        $applications = $this->getLinkedItems("isga_Applications",$predicate);

        return $applications;
    }

    /**
     * Возвращает массив с местами осуществления обр деятельности
     */
    public function getEOPlaces(){
        if(empty($this->fields["ImplAddr"])) return false;
        $places = $this->fields["ImplAddr"];
        return $places = explode(";",$places);
    }
    
    /**
     * Метод возвращает предписания для организации
     */
    public function getPrescriptions()
    {
        //тут предписаний не будет
        return array();
        $ids = array(
            $this->getId()
        );

        $filials = $this->getAffilates()->getObjectsArray();
        foreach ($filials as $filial) {
            $ids[] = $filial->getId();
        }

        $table = new Gateway\EntitiesTable("akndou_ActivePrescriptions");
        $result = $table->getAllEntities(null, null,
                                         [
            new \Zend\Db\Sql\Predicate\In("fk_eiisEducationalOrganization", $ids)
        ]);

        return $result->getObjectsArray();
    }
    
    
    /**
     * Метод возвращает объект в котором хранятся данные по главе
     * @return \Model\Entities\OrganizationHeadEmployee
     */
    public function getHead()
    {
        $headTable = new Gateway\LodEntitiesTable("OrganizationHeadEmployee");
        $result = $headTable->getAllEntities(null, null, [
              "organizationId" => $this->getId()
          ])->getObjectsArray();

        if (count($result) > 0) {
            return $result[0];
        }

        return new OrganizationHeadEmployee();
    }
    
    public function getValidAffilates( $toValid = false )
    {
        $predicate = new \Zend\Db\Sql\Where();
        if( $toValid ){
            $predicate
              ->equalTo("lod_stateCode", "VALID")
              ->or
              ->equalTo("lod_stateCode", "DRAFT");
        }
        else{            
            $predicate
              ->notEqualTo("lod_stateCode", "Not_Valid")
              ->or
              ->isNull("lod_stateCode");
        }

        $affilates = $this->getLinkedItems(
            "eiis_EducationalOrganizations",
            array(
                "fk_eiisParentEducationalOrganization" => $this->fields["Id"],
                $predicate
            ),
            'FullName'    
        );
        
        return $affilates;
    }
    
    /**
     * дергаем наименование должности у руководителя вуза
     * 
     * @return type
     */
    public function getChargePositionTitle() {
        $return = '';
        if( !$this->isEmptyField('ChargePosition') ){
            $chargePositionId = $this->getFieldSafe('ChargePosition');
            $positionClass = new \Model\LodEntities\employeepost();
            $positionClass = $positionClass->getByWhere(array(
                $positionClass->getIdName()=> $chargePositionId
            ));
            if( empty($positionClass) ){
                $return = $chargePositionId;
            }
            else{
                $return = $positionClass->getFieldSafe('title');
            }
        }
        return $return;
    }

    public function getEOPlacesLod()
    {
        $address = array();
        if( !empty($this->fields["Id"]) ){
            $result = $this->getLinkedItems("eiis_LicenseSupplements", array(
                "fk_eiisBranch" => $this->fields["Id"],
                "lod_stateCode" => array('VALID','SUSPENDED')
            ))->toArray();
            $supplementsIds = array_column($result, 'Id');
            if(!empty($supplementsIds)){
                $orgLocationAttach = new \Model\LodEntities\OrganizationLocationtoLicenseAttach();
                $orgLocationAttach = $orgLocationAttach->getAllByWhere(array(
                    'licenseAttachId' => $supplementsIds
                ));
                if($orgLocationAttach){
                    $orgLocationAttachIds = array_column($orgLocationAttach->toArray(), 'organizationLocationId');
                    if(!empty($orgLocationAttachIds)){
                        $orgLocation = new \Model\LodEntities\OrganizationLocation();
                        $address = $orgLocation->getAllByWhere([
                                'organizationId' => $this->fields['Id'],
                                'id'    => $orgLocationAttachIds,
                                'addressType'    => 'PlaceOfEducationalActivities'
                            ]);
                    }
                }
            }
        }
        return $address;
    }
    
    //последняя строка класса    
}