<?php
namespace Model\Entities;

use Model\Gateway;

class isgaCollegeResults extends Gateway\BaseEntity{
    protected $tableName = "isga_CollegeResults";
    protected $idName = "Id";
}