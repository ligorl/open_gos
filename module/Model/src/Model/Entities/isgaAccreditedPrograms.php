<?php
namespace Model\Entities;

use Model\Gateway;

class isgaAccreditedPrograms extends Gateway\BaseEntity{
    protected $tableName = "isga_AccreditedPrograms";
    protected $idName = "Id";

    public static function getByLicensedProgramFk($licensedProgramFk){
        $table = new Gateway\EntitiesTable("isga_AccreditedPrograms");
        $result = $table->getEntitiesByWhere([
            "fk_eiisLicensedProgram" => $licensedProgramFk
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

    public function getProgramData(){
        $xml = $this->getField("DynamicColumns");
        $reader = new \Zend\Config\Reader\Xml();
        $returnData = array();
        //var_dump($xml);die;
/*
        $data   = $reader->fromString($xml);
        $items = $data["KeyValueOfCertificateSupplimentOopColumnstringHIe5wLoO"];
        foreach ($items as $item) {
            if (isset($item["Value"]) && is_string($item["Value"])) {
                $value = $item["Value"];
            } else {
                $value = '';
            }
            if (isset($item["Key"]["d3p1:Key"]) && is_string($item["Key"]["d3p1:Key"]) && $item["Key"]["d3p1:Key"] != "") {
                $key = $item["Key"]["d3p1:Key"];
            } else {
                $key = '';
            }
            //Костыль на некоторые уровни образования
            if ($key == '_edulevel') {
                 $expComa = explode("','", $value);
                if (count($expComa) > 1) {
                    foreach ($expComa as $value) {
                        $first=$value[0];
                        if($first=="'"){
                            $value=  substr($value, 1);
                        }
                        $length=strlen($value);
                        $last=$value[$length-1];
                        if($last=="'"){
                            $value=  substr($value, 0,$length-1);
                        }
                        $expDefis = explode(' - ', $value);
                        if (count($expDefis) > 1) {
                            if($expDefis[0]=='Высшее образование'){
                                $value = $expDefis[0];
                                break;
                            }

                        }else{                            
                            break;
                        }
                    }
                } else {
                }
                $returnData["Уровень образования"]=$value;
                continue;
            }
            if (isset($item["Key"]["d3p1:ColumnName"]) && is_string($item["Key"]["d3p1:ColumnName"]) && $item["Key"]["d3p1:ColumnName"] != "") {
                $columnName = $item["Key"]["d3p1:ColumnName"];
            } else {
                continue;
            }
            if ($columnName == "УГС") {
                $expRes = explode(" - ", $value);
                if (count($expRes) > 0) {
                    if ($expRes[0] == 'Unknown') {
                        $expRes[0] = ' ';
                    }
                    $returnData["Код УГС"] = $expRes[0];
                    $returnData["Наименование УГС"] = $expRes[1];
                } else {
                    $returnData["Код УГС"] = $value;
                    $returnData["Наименование УГС"] = $value;
                }
                continue;
            }


            if ($columnName == 'Наименование ОП' && $value == '' ) {
                return null;
            }
            $returnData[$columnName] = $value;

            /*
              if ($key == '_1/_group_spec') {
              if (!empty($value)){
              $expRes = explode(" - ",$value);
              if(count($expRes) > 0) {
              $returnData["Код УГС"] = $expRes[0];
              $returnData["Наименование УГС"] = $expRes[1];
              }
              else{
              $returnData["Код УГС"] = $value;
              $returnData["Наименование УГС"] = $value;
              }
              }else{
              $returnData["Код УГС"]='';
              $returnData["Наименование УГС"]='';
              }
              continue;
              }
             */
/*        }

                //$returnData['Уровень образования'] = $this->getEduLevel('Name');
                //var_dump($returnData);
                if(isset($returnData["Код ООП"])) $returnData["Код ОП"] = $returnData["Код ООП"];
                if(isset($returnData["Наименование ООП"])) $returnData["Наименование ОП"] = $returnData["Наименование ООП"];
                if(!isset($returnData["Код ОП"])) $returnData["Код ОП"] = "";
                if(!isset($returnData["Код УГС"])) $returnData["Код УГС"] = "";
                if(!isset($returnData["Наименование УГС"])) $returnData["Наименование УГС"] = "";
                if(!isset($returnData["Наименование ОП"])) $returnData["Наименование ОП"] = "";
                if(!isset($returnData["Квалификация"])) $returnData["Квалификация"] = "";
                if(!isset($returnData["Уровень образования"])) $returnData["Уровень образования"] = "";
        */
        return $returnData;
    }

    public function getCertificateSupplement(){
        if(!isset($this->fields["fk_isgaCertificateSupplement"]) || $this->fields["fk_isgaCertificateSupplement"] == "" || strtolower($this->fields["fk_isgaCertificateSupplement"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_CertificateSupplements",array(
            "Id" => $this->fields["fk_isgaCertificateSupplement"]
        ));
        return $result;
    }

    public function isCertificateSupplementValid(){
        $csupp = $this->getCertificateSupplement();
        if($csupp != null){
            $statusCode = $csupp->getStatusCode();
                if ($statusCode == 'InForce'//действующие
                    ||$statusCode == 'Resumed'//возобновленные
                ) return true;
        }
        return false;
    }


    public function getLicensedProgram($param=null)
    {
        $licensedProgramsFieldName='fk_eiisLicensedProgram';
        $idLicensedProgram = $this->getField($licensedProgramsFieldName);

        if (!$idLicensedProgram || strtolower($idLicensedProgram) == "null"){
             return null;
        }

        $licensedProgram = $this->getLinkedItem(
          "eiis_LicensedPrograms", array('Id' => $this->stripIdDashes($idLicensedProgram))
        );

        if ($param !== null) {
            $result = $licensedProgram->getField($param);
        } else {
            $result = $licensedProgram;
        }
        return $result;
    }

    public function getEduProgramType($param=null)
    {
        $idEduProgramType = $this->getField('fk_eiisEduProgramType');
        if (!$idEduProgramType || strtolower($idEduProgramType) == "null"){
             return null;
        }
        $eduProgramType = $this->getLinkedItem(
          'eiis_EduProgramTypes', array('Id' => $idEduProgramType)
        );
        if ($param !== null) {
            $result = $eduProgramType->getField($param);
        } else {
            $result = $eduProgramType;//->toArray();
        }
        return $result;
    }

    public function getEduLevel($param=null)
    {
        $eoTable = new \Model\Gateway\EntitiesTable("eiis_EduLevels");
        $tg = $eoTable->getGateway();

        //Подзапрос
        $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
        $subselect = $sql->select();

       /* if(!$this->isEmptyField("fk_eiisLicensedProgram")){
            $subselect->from('eiis_LicensedPrograms')
                ->columns(array('fk_eiisEduLevel'))
                ->where(array(
                    'Id' => $this->getField("fk_eiisLicensedProgram")
                ));
        } else {*/
            $subselect->from('eiis_EduPrograms')
                ->columns(array('fk_eiisEduLevels'))
                ->where(array(
                    'Id' => $this->getField("fk_eiisEduProgram")
                ));
        //}


        //Запрос
        $resultSet = $tg->select(function(\Zend\Db\Sql\Select $select) use ($subselect, $tg){
            $select
                ->where->in('Id', $subselect);
            //$sql = $tg->getSql();
            //var_dump($sql->getSqlstringForSqlObject($select));die;
        });
        $resultSet = $resultSet->getObjectsArray();

        if(count($resultSet)>0)
            return $resultSet[0];
        else
            return null;
    }

    public function getEnlargedGroupSpeciality(){
        //Если задана связь fk_isgaEnlargedGroupSpeciality
        $EGSFieldName="fk_isgaEnlargedGroupSpeciality";
        if(!$this->isEmptyField($EGSFieldName)){
            $EGS = $this->getLinkedItem(
                "isga_EnlargedGroupSpecialities", array('Id' => $this->fields[$EGSFieldName])
            );
            if($EGS != null)
                return $EGS;
            else
                return null;
        } else {
            $eoTable = new \Model\Gateway\EntitiesTable("isga_EnlargedGroupSpecialities");
            $tg = $eoTable->getGateway();

            //Подзапрос
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
            $subselect = $sql->select();

            $subselect->from('isga_GroupSpecialities')
                ->columns(array('fk_isgaEGS'))
                ->where(array(
                    'Id' => $this->getField("fk_isgaGroupSpeciality")
                ));

            //Запрос
            $resultSet = $tg->select(function(\Zend\Db\Sql\Select $select) use ($subselect, $tg){
                $select
                    ->where->in('Id', $subselect);
            });
            $resultSet = $resultSet->getObjectsArray();

            if(count($resultSet)>0)
                return $resultSet[0];
            else
                return null;
        }
    }



    private $_EGS = null;
    public function getEGS(){
        if($this->_EGS != null)
            return $this->_EGS;

        if($this->isEmptyField("fk_isgaEnlargedGroupSpecialities")){            
            $licProg = $this->getLicensedProgram();
            if(!empty($licProg)){
                $ugs = $licProg->getUgs();
                if(!empty($ugs)){
                    return $ugs;
                }
            }
            return null;
/*
        $gsId=$this->getField('fk_isgaGroupSpeciality');
        if (!empty($gsId)){
            $egs=$this->getEgsByGsId($gsId);
            if($egs){
                return $egs;
            }
        }

        $licensedProgram=$this->getLicensedProgram();
        if ($licensedProgram){
            $eduProgram=$licensedProgram->getEduProgram();
            if($eduProgram){
                $gsId=$eduProgram->getField('fk_eiisGS');
                $egs=$this->getEgsByGsId($gsId);
                if ($egs){
                    return $egs;
                }
                $egsId=$eduProgram->getField('fk_eiisUgs');
                $egs= new \Model\Entities\isgaEnlargedGroupSpecialities($egsId);
                $egsA=$egs->toArray();
                if (count($egsA)){
                    return $egs;
                }
            }
            $egs = $licensedProgram->getUgs();
            if(!empty($egs)){
                return $egs;
            }

        }
        //$eduProgramId=$this->getField('fk_eiisEduProgram');
       // if ($eduProgramId){
            $eduProgram= $this->getEduProgram();
            if($eduProgram){
                $gsId=$eduProgram->getField('fk_eiisGS');
                $egs=$this->getEgsByGsId($gsId);
                if ($egs){
                    return $egs;
                }
                $egsId=$eduProgram->getField('fk_eiisUgs');
                $egs= new \Model\Entities\isgaEnlargedGroupSpecialities($egsId);
                $egsA=$egs->toArray();
                if (count($egsA)){
                    return $egs;
                }
            }
       // }
        return new \Model\Entities\isgaEnlargedGroupSpecialities();
        */            
        }

        $result = $this->getLinkedItem("isga_EnlargedGroupSpecialities",array(
            "Id" => $this->fields["fk_isgaEnlargedGroupSpecialities"]
        ));
        $this->_EGS = $result;
        return $result;
    }

    public function getQualificationName()
    {
        if (!$this->isEmptyField('fk_isgaQualification')){
            $qualificationName=$this->getQualificationNameById($this->getField('fk_isgaQualification'));
            if(!empty($qualificationName)){
                return $this->getField('Name');
            }
        }
        $licenseProgram=$this->getLicensedProgram();
        if($licenseProgram){
            $qualificationName=$licenseProgram->getField('QualificationName');
            if($qualificationName){
                return $qualificationName;
            }
            $eduProgram=$licenseProgram->getEduProgram();
            if($eduProgram){
                $qualificationName=$eduProgram->getField('QualificationName');
                if($qualificationName){
                    return $qualificationName;
                }
                $qualificationId=$eduProgram->getField('fk_eiisQualification');
                if($qualificationId){
                    $qualificationName=$this->getQualificationNameById($qualificationId);
                    if(!empty($qualificationName)){
                        return $this->getField('Name');
                    }
                }
            }
        }
        $eduProgram =  $this->getEduProgram();

        if($eduProgram){
            $qualificationName=$eduProgram->getField('QualificationName');
            if($qualificationName){
                return $qualificationName;
            }
            $qualificationId=$eduProgram->getField('fk_eiisQualification');
            if($qualificationId){
                $qualificationName=$this->getQualificationNameById($qualificationId);
                if(!empty($qualificationName)){
                    return $this->getField('Name');
                }
            }
        }
        return false;

    }

    public function getQualificationCode()
    {
        if (!$this->isEmptyField('fk_isgaQualification')){
            $qualificationName=$this->getQualificationCodeById($this->getField('fk_isgaQualification'));
            if(!empty($qualificationName)){
                return $this->getField('Code');
            }
        }
        $licenseProgram=$this->getLicensedProgram();
        if($licenseProgram){
            $qualificationName=$licenseProgram->getField('QualificationCode');
            if($qualificationName){
                return $qualificationName;
            }
            $eduProgram=$licenseProgram->getEduProgram();
            if($eduProgram){
                $qualificationName=$eduProgram->getField('QualificationCode');
                if($qualificationName){
                    return $qualificationName;
                }
                $qualificationId=$eduProgram->getField('fk_eiisQualification');
                if($qualificationId){
                    $qualificationName= $this->getQualificationCodeById($qualificationId);
                    if(!empty($qualificationName)){
                        return $this->getField('Code');
                    }
                }
            }
        }
        $eduProgram =  $this->getEduProgram();

        if($eduProgram){
            $qualificationName=$eduProgram->getField('QualificationCode');
            if($qualificationName){
                return $qualificationName;
            }
            $qualificationId=$eduProgram->getField('fk_eiisQualification');
            if($qualificationId){
                $qualificationName=$this->getQualificationCodeById($qualificationId);
                if(!empty($qualificationName)){
                    return $this->getField('Code');
                }
            }
        }
        return false;

    }

    public function getQualificationNameById($id)
    {
        if(empty($id)){
            return false;
        }

        $qualification= new \Model\Entities\isgaQualifications($id);
        $qualificationArray=$qualification->toArray();
        if (count($qualificationArray)){
            return $qualification->getField('Name');
        }else{
            return false;
        }

    }

    public function getQualificationCodeById($id)
    {
        if(empty($id)){
            return false;
        }

        $qualification= new \Model\Entities\isgaQualifications($id);
        $qualificationArray=$qualification->toArray();
        if (count($qualificationArray)){
            return $qualification->getField('Code');
        }else{
            return false;
        }

    }



    protected  $eduProgram=null;
    public function getEduProgram()
    {
        if ($this->eduProgram===null){
            if($this->isEmptyField("fk_eiisEduProgram"))
                return null;

            $result = $this->getLinkedItem("eiis_EduPrograms",array(
                "Id" => $this->fields["fk_eiisEduProgram"]
            ));
            $eduProgram=$result;
        }
        return $eduProgram;
    }


    public function getGroupSpeciality(){
        if($this->isEmptyField("fk_isgaGroupSpeciality"))
            return null;

        $result = $this->getLinkedItem("isga_GroupSpecialities",array(
            "Id" => $this->fields["fk_isgaGroupSpeciality"]
        ));
        return $result;
    }

    public function isProfy(){
        try {
            $type = $this->getEduProgramType();
            if (!empty($type)) {
                $isProfy = $type->isProfy();
                if (isset($isProfy)) {
                    return $isProfy;
                }

            }

            $licensedProgram = $this->getLicensedProgram();
            if (!$licensedProgram) {
                return null;
            }
            return $eduLevel = $licensedProgram->isProfy();
        }catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }

    }
}