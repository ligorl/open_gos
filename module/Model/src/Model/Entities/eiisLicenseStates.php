<?php
namespace Model\Entities;

use Model\Gateway;

class eiisLicenseStates extends Gateway\BaseEntity{
    protected $tableName = "eiis_LicenseStates";
    protected $idName = "Id";
}