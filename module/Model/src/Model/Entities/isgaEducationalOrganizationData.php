<?php
namespace Model\Entities;

use Model\Gateway;

class isgaEducationalOrganizationData extends Gateway\BaseEntity{
    protected $tableName = "isga_EducationalOrganizationData";
    protected $idName = "Id";


    public function setFieldsEduOrgData($fields, $overwrite = false, $saveRawFields = false){
		if($overwrite)
			$this->fields = array();

		$this->fields = array_merge($this->fields,$fields);

		if(!$saveRawFields) {
			if (isset($this->fields["PubAccr"]))
				$this->fields["PubAccr"] = json_encode($this->fields["PubAccr"]);
			if (isset($this->fields["ProfPubAccr"]))
				$this->fields["ProfPubAccr"] = json_encode($this->fields["ProfPubAccr"]);
			if (isset($this->fields["IntPubAccr"]))
				$this->fields["IntPubAccr"] = json_encode($this->fields["IntPubAccr"]);
		}
    }

    public function getFields($returnRawFields = false){
		if($returnRawFields)
			return $this->fields;

    	$retFields = $this->fields;

		if(isset($retFields["PubAccr"]))
    		$retFields["PubAccr"] = $this->decode($retFields["PubAccr"]);
    	if(isset($retFields["ProfPubAccr"]))
    		/*$retFields["ProfPubAccr"] = $retFields["ProfPubAccr"];*/
    		$retFields["ProfPubAccr"] = $this->decode($retFields["ProfPubAccr"]);
    	if(isset($retFields["IntPubAccr"]))
    		$retFields["IntPubAccr"] = $this->decode($retFields["IntPubAccr"]);
    	return $retFields;
    }

    public function setField($fieldName,$value){
    	if($fieldName == "PubAccr" || $fieldName == "ProfPubAccr" || $fieldName == "IntPubAccr")
			$this->fields[$fieldName] = json_encode($value);
    	else
        	$this->fields[$fieldName] = $value;
    }

    public function getField($fieldName){
    	if($fieldName == "PubAccr" || $fieldName == "ProfPubAccr" || $fieldName == "IntPubAccr")
			return json_decode(json_decode($this->fields[$fieldName]),true);
        else if(isset($this->fields[$fieldName]))
            return $this->fields[$fieldName];
        else return null;
    }

    private function decode($value){

		$value = stripslashes($value);

		if(substr($value,0,1) == '"' && substr($value,-1)=='"')
			$value = substr($value, 1,-1);

		if(substr($value,0,1) == '"' && substr($value,-1)=='"')
			$value = substr($value, 1,-1);

		return json_decode($value, true);
    }

	public function checkAddIfNot($orgId)
	{
		if(empty($orgId)){
			return null;
		}

		$org = new \Model\Entities\eiisEducationalOrganizations($orgId);
		if($org->isEmpty())
			return null;

		$license = $org->getLicense();

		$ChiefSurname = $org->getField('ContactSecondName');
		if($ChiefSurname == "")
			$ChiefSurname = $org->getField('ChargeFio');

		$eduOrgData=  $this->getLinkedItem(
			$this->tableName
			, array('fk_eiisEducationalOrganization'=>$orgId)
		);
		if(empty($eduOrgData)){
			$data=array();
			$data['fk_eiisEducationalOrganization']=$orgId;

			$data['Phones'] = $org->getField("Phones");
			$data["Faxes"] = $org->getField("Faxes");
			$data["Emails"] = $org->getField("Mails");
			$data["Site"] = $org->getField('Www');
			$data["ChiefSurname"] = $ChiefSurname;
			$data["ChiefName"] = $org->getField('ContactFirstName');
			$data["ChiefPatronymic"] = $org->getField('ContactLastName');
			$data["ChiefPosition"] = $org->getField('ChargePosition');
			$data['ChiefPhones']='';
			$data['ChiefFax']='';
			$data['ChiefEmail']='';
			$data['PubAccr']='[{"organization":"","date":"","requisite":""}]';
			$data['ProfPubAccr']='[{"organization":"","date":"","requisite":""}]';
			$data['IntPubAccr']='[{"organization":"","date":"","requisite":""}]';
			$data['fk_isgaEducationalOrganizationSpecific']=1;
			$data['NameDatCase']=($license != null && !$license->isEmptyField("EONameDatCase"))?$license->getField("EONameDatCase"):"";
			$data['NameInstrCase']=($license != null && !$license->isEmptyField("OrgNameInstrCase"))?$license->getField("OrgNameInstrCase"):"";
			$data['NameAccCase']='';
			$data['BankAccountNumber']='';
			$data['BankName']='';
			$data['BankCorrespondentAccount']='';
			$data['BankBik']='';
			$eduOrgData=new \Model\Entities\isgaEducationalOrganizationData();
			$eduOrgData->exchangeArray($data);
			$eduOrgData->save(true);
		}
	}

    /**
     *
     * @param $organization_id
     * @return array|\ArrayObject|null|string|\Zend\Db\Adapter\Driver\StatementInterface|\Zend\Db\ResultSet\ResultSet
     */
    public function getEduOrgData($organization_id) {
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $selectString = "SELECT * FROM isga_EducationalOrganizationData 
            WHERE  fk_eiisEducationalOrganization = '".$organization_id."'";
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        if($results){
            $results = $results->current();
            return $results;
        }
        return '';
    }
}