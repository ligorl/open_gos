<?php

namespace Model\Entities;

use Model\Gateway;

class isgaApplicationDocumentGroups extends Gateway\BaseEntity
{

    protected $tableName = 'isga_ApplicationDocumentGroups';
    protected $idName = "Id";

    public function getByApplicationId($applicationId)
    {
        $applicationId =  strtolower($applicationId);
        $where = 
            '`fk_isgaApplication` LIKE "'.$applicationId.'"'
            .'AND `fk_isgaApplicationDocumentType` = "0B4B6324-5DAE-4AB8-B973-86C4A39409E1"';

        $result = $this->getLinkedItem($this->tableName,$where);
        if ($result) {
            $data=$result->toArray();
            $this->exchangeArray($data);
            return true;
        }
        return false;
    }

    public function createNew($applicationId)
    {
        $data = array();
        $data['fk_isgaApplication'] = strtolower($applicationId);
        $data['fk_isgaApplicationDocumentType'] = '0B4B6324-5DAE-4AB8-B973-86C4A39409E1';
        $data['fk_ApplicationDocumentGroupStatus'] = 'e7d3dec6-a38c-409b-85af-345065dc70fd';
        $this->exchangeArray($data);
        $this->save(true);
    }

    /**
     * устанавливает статус проверенно и укомплектовано
     */
    public function chekedPositiv()
    {
        $data = array();
        $data['fk_ApplicationDocumentGroupStatus'] = '1d8d2fd4-e20c-42ba-a24a-9fe0268d5f89';
        $this->exchangeArray($data);
        $this->save(false);
        //очижаем причины несоответствий
        $docGroupId=$this->getField('Id');
        $docGroupStatusId=$this->getField('fk_ApplicationDocumentGroupStatus');
        $docGroupGroup= new \Model\Entities\isgaApplicationDocumentGroupGroupStatusReason();
        $docGroupGroup->delRecords($docGroupId, $docGroupStatusId);
    }

    public function chekedNegativ()
    {
        $data = array();
        $data['fk_ApplicationDocumentGroupStatus'] = '43cfbc22-4f50-4017-8d01-64be02c05069';
        $this->exchangeArray($data);
        $this->save(false);
        //очижаем причины несоответствий
        $docGroupId=$this->getField('Id');
        $docGroupStatusId=$this->getField('fk_ApplicationDocumentGroupStatus');
        $docGroupGroup= new \Model\Entities\isgaApplicationDocumentGroupGroupStatusReason();
        $docGroupGroup->delRecords($docGroupId, $docGroupStatusId);
    }

    /**
     * 
     */
    public function chekedDenied() {
        $data = array();
        $data['fk_ApplicationDocumentGroupStatus'] = '165e0873-02dc-4aaf-a9d0-2cd7713b67a2';
        $this->exchangeArray($data);
        $this->save(false);
        //очижаем причины несоответствий
        $docGroupId=$this->getField('Id');
        $docGroupStatusId=$this->getField('fk_ApplicationDocumentGroupStatus');
        $docGroupGroup= new \Model\Entities\isgaApplicationDocumentGroupGroupStatusReason();
        $docGroupGroup->delRecords($docGroupId, $docGroupStatusId);
    }

    /**
     *
     */
        public function chekedReturn()
    {
        $data = array();
        $data['fk_ApplicationDocumentGroupStatus'] = '1f98ed4f-60e7-46c6-81be-7e10daffa4a0';
        $this->exchangeArray($data);
        $this->save(false);
    }    
    
}