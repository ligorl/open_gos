<?php
namespace Model\Entities;

use Model\Gateway;

class isgaEnlargedGroupSpecialityEditions extends Gateway\BaseEntity{
    protected $tableName = "isga_EnlargedGroupSpecialityEditions";
    protected $idName = "Id";
}