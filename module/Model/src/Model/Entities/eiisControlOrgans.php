<?php
namespace Model\Entities;

use Model\Gateway;

class eiisControlOrgans extends Gateway\BaseEntity{
    protected $tableName = "eiis_ControlOrgans";
    protected $idName = "Id";

    //====================== Статические методы
    public static function getByName($name){
        $table = new Gateway\EntitiesTable("eiis_ControlOrgans");
        $result = $table->getEntitiesByWhere([
            "Name" => $name
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

}