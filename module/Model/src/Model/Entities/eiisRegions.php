<?php
namespace Model\Entities;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Model\Gateway;

class eiisRegions extends Gateway\BaseEntity{
    protected $tableName = "eiis_Regions";
    protected $idName = "Id";

    public function getFedOkrName(){
        if(!isset($this->fields["fk_eiisFedOkr"]) || $this->fields["fk_eiisFedOkr"]=="" || strtolower($this->fields["fk_eiisFedOkr"]) == "null")
            return null;

        $result = $this->getLinkedItem("eiis_FedOkr",array(
            "Id" => $this->fields["fk_eiisFedOkr"]
        ));
        return $result?$result->getField("Name"):'';
    }

    public function getArrRegion(){
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('eiis_Regions');
        $selectString = $sql->getSqlStringForSqlObject($select); //print $selectString;die;
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $result = array();
        foreach($results as $v){
            $result[$v->Id] = $v->Name;
        }
        return $result;
    }
}