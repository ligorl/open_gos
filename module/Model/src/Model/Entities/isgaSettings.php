<?php
namespace Model\Entities;

use Model\Gateway;

class isgaSettings extends Gateway\BaseEntity{
    protected $tableName = "isga_Settings";
    protected $idName = "id";
}