<?php
namespace Model\Entities;

use Model\Gateway;

class eiisFedOkr extends Gateway\BaseEntity{
    protected $tableName = "eiis_FedOkr";
    protected $idName = "Id";
}