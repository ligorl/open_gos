<?php
namespace Model\Entities;

use Model\Gateway;

class isgaHistoryStatus extends Gateway\BaseEntity{
    protected $tableName = "isga_HistoryStatus";
    protected $idName = "Id";
}