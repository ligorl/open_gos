<?php
namespace Model\Entities;

use Model\Gateway;

class isgaQualifications extends Gateway\BaseEntity{
    protected $tableName = "isga_Qualifications";
    protected $idName = "Id";
}