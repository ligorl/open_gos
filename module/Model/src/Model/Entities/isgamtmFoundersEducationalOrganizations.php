<?php
namespace Model\Entities;

use Model\Gateway;

class isgamtmFoundersEducationalOrganizations extends Gateway\BaseEntity{
    protected $tableName = "isga_mtm_Founders_EducationalOrganizations";
    protected $idName = "Id";
}