<?php
namespace Model\Entities;

use Model\Gateway;

class isgaUserRegRequest extends Gateway\BaseEntity{
    protected $tableName = "isga_UserRegRequest";
    protected $idName = "id";

    static public function getByEmail($email){
        $table = new Gateway\EntitiesTable("isga_UserRegRequest");
        $result = $table->getEntitiesByWhere([
            "email" => $email
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}