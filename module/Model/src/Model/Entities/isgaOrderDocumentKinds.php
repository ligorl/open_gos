<?php
namespace Model\Entities;

use Model\Gateway;

class isgaOrderDocumentKinds extends Gateway\BaseEntity{
    protected $tableName = "isga_OrderDocumentKinds";
    protected $idName = "Id";

    public function getOrderDocumentType(){
        $logger = new \Model\Logger\Logger("service",true);
        try {
            if ($this->isEmptyField("fk_isgaOrderDocumentType"))
                return null;
            $result = $this->getLinkedItem("isga_OrderDocumentTypes", array(
                "Id" => $this->fields["fk_isgaOrderDocumentType"]
            ));
            if ($result)
                return $result->getField("Name");
            return null;
        } catch(\Exception $e){

            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
        }
    }
}