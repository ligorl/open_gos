<?php
namespace Model\Entities;

use Model\Gateway;

class  eiisEducationalOrganizationProperties extends Gateway\BaseEntity{
    protected $tableName = "eiis_EducationalOrganizationProperties";
    protected $idName = "Id";
}