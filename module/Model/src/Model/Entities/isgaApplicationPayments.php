<?php

namespace Model\Entities;

use Model\Gateway;

class isgaApplicationPayments extends Gateway\BaseEntity
{

    protected $tableName = "isga_ApplicationPayments";
    protected $idName = "Id";

    public function getPayment($appType, $appReason, $eduLevel = null)
    {
        $field = array();
        //$field['fk_isgaApplicationType'] = $appType;
        $field['fk_isgaApplicationReason'] = $appReason;
        if (!empty($eduLevel)) {
            $field['fk_eiisEduLevels'] = $eduLevel;
        }
        return $this->getLinkedItem($this->tableName, $field);
    }

    public function getPrice()
    {
        $price = $this->getField('Price');
        return (!empty($price))?$price : 0;
    }

    public function getCode()
    {
        $code=$this->getField('Code');
        return (!empty($code))?$code : '';

    }

    public function getComment()
    {
        return $this->getField('Comment');
    }

    public function getLevelsForEach()
    {
        $qwestion=  $this->selectComplicated(
        $this->tableName,
          array('fk_eiisEduLevels','EachProgram'),
          array('EachProgram'=>1)
          );
        if (!$qwestion){
            return array();
        }
        $answer=$qwestion->toArray();
        $result=  array_column($answer, 'fk_eiisEduLevels');
        $result=array_unique($result);

        return $result;
    }

}