<?php
namespace Model\Entities;

use Model\Gateway;

class isgaCertificateSupplements extends Gateway\BaseEntity{
    protected $tableName = "isga_CertificateSupplements";
    protected $idName = "Id";

    public function getEducationalOrganization(){
        //var_dump($this->fields["fk_eiisEducationalOrganization"]);
        if(!isset($this->fields["fk_eiisEducationalOrganization"]) || $this->fields["fk_eiisEducationalOrganization"] == "" || strtolower($this->fields["fk_eiisEducationalOrganization"]) == "null"){
            //Получаем текущую организацию
            return $this->getCertificate()->getEducationalOrganization();
        }

        $result = $this->getLinkedItem("eiis_EducationalOrganizations",array(
            "Id" => $this->stripIdDashes($this->fields["fk_eiisEducationalOrganization"])
        ));
        return $result;
    }

    public function getCertificate(){
        if(!isset($this->fields["fk_isgaCertificate"]) || $this->fields["fk_isgaCertificate"] == "" || strtolower($this->fields["fk_isgaCertificate"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_Certificates",array(
            "Id" => $this->fields["fk_isgaCertificate"]
        ));
        return $result;
    }

    public function getStatusName(){
        if(!isset($this->fields["fk_isgaCertificateSupplementStatus"]) || $this->fields["fk_isgaCertificateSupplementStatus"] == "" || strtolower($this->fields["fk_isgaCertificateSupplementStatus"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_CertificateSupplementStatuses",array(
            "Id" => $this->fields["fk_isgaCertificateSupplementStatus"]
        ));
        return $result->getField("Name");
    }

    public function getStatusCode(){
        if($this->isEmptyField("fk_isgaCertificateSupplementStatus"))
            return null;

        $result = $this->getLinkedItem("isga_CertificateSupplementStatuses",array(
            "Id" => $this->fields["fk_isgaCertificateSupplementStatus"]
        ));
        return $result->getField("Code");
    }

    public function getAccreditedPrograms(){
        $programs = $this->getLinkedItems("isga_AccreditedPrograms",array(
            "fk_isgaCertificateSupplement" => $this->fields["Id"]
        ));

        return $programs;
    }
}