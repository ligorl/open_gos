<?php
namespace Model\Entities;

use Model\Gateway;

class eiisEducationalOrganizationKinds extends Gateway\BaseEntity{
    protected $tableName = "eiis_EducationalOrganizationKinds";
    protected $idName = "Id";
}