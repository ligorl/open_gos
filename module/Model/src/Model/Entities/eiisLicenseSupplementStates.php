<?php
namespace Model\Entities;

use Model\Gateway;

class eiisLicenseSupplementStates extends Gateway\BaseEntity{
    protected $tableName = "eiis_LicenseSupplementStates";
    protected $idName = "Id";
}