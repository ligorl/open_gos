<?php

namespace Model\Entities;

use Model\Gateway;
use Zend\Db\Sql\Select;

class isgaApplications extends Gateway\BaseEntity
{

    protected $tableName = "isga_Applications";
    protected $idName    = "Id";




    public function getApplicationByPage($page){
        $result=$this->tableGateway->select(function(Select $select)
        {
            $select
                ->order('order ASC')
                ->where(array())
                ->limit(15);
        });
        return $result;
    }

    public function getFileByApplicationAndOrderType($orderTypeId)
    {
        if($this->isEmptyField('Id')||empty($orderTypeId)){
            return null;
        }
        $result = $this->getLinkedItem(
            "isga_ApplicationFiles",
            array(
                'fk_isgaApplication' => $this->getId(),
                'fk_isgaDocumentType'=> $orderTypeId
            )
        );
        return $result;
    }

    public function setProgramChekedInFalse(){
        if( empty( $this->fields[ 'Id' ] ) ){
            return null;
        }
        $program = $this->getLinkedItems( "isga_ApplicationPrograms", array(
                "fk_isgaApplication" => $this->fields[ "Id" ]
            ) );
        foreach( $program as $oneItem ){
            $oneItem->setField( 'Checked', 0 );
            $oneItem->save( false );
        }
    }

    public function getTypeName(){
        if(!isset($this->fields["fk_isgaApplicationType"]) || $this->fields["fk_isgaApplicationType"]=="" || strtolower($this->fields["fk_isgaApplicationType"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_ApplicationTypes",array(
            "Id" => $this->fields["fk_isgaApplicationType"]
        ));
        return $result->getField("Name");
    }

     public function getType(){
        if($this->getField('fk_isgaApplicationReason') != '1'){

            $reason = $this->getLinkedItem("isga_ApplicationReasons",array(
                "Id" => $this->fields["fk_isgaApplicationReason"]
            ));

            $result=null;
            if($reason){
                $result = $reason->getLinkedItem("isga_ApplicationTypes",array(
                    "Id" => $reason->fields["fk_isgaApplicationType"]
                ));
            }
            return $result;
        }else{
            $type_id= $this->getTypeId();
            $result=null;
            if($type_id){
                $result = $this->getLinkedItem("isga_ApplicationTypes",array(
                    "Id" => $type_id
                ));
            }
             return $result;

        }
     }
        public function getNamesReasons() {
        if ($this->getField('fk_isgaApplicationReason') == '1') {
            $reasonsAppl = $this->getLinkedItems("isga_mtm_Application_Reason", array(
                        "fk_isgaApplication" => $this->fields["Id"]
                    ))->toArray();
            $reasons_id  = array_column($reasonsAppl, 'fk_isgaApplicationReason');
            if(!empty($reasons_id)){
                $reasons     = $this->getLinkedItems("isga_ApplicationReasons", array(
                        "Id" => $reasons_id
                    ))->toArray();
                $result      = array_column($reasons, 'Name');
            }else{
                $result = FALSE;
            }
        } else {
            $reason = $this->getLinkedItem("isga_ApplicationReasons", array(
                        "Id" => $this->getField('fk_isgaApplicationReason')
                    ));
            if(!empty($reason)){
                $result = $reason->getField('Name');
            }else{
                $result = FALSE;
            }
        }

        return $result;
    }
    public function getIdsReasons2() {
        if ($this->getField('fk_isgaApplicationReason') == '1') {
            $reasonsAppl = $this->getLinkedItems("isga_mtm_Application_Reason", array(
                    "fk_isgaApplication" => $this->fields["Id"]
                ))->toArray();
            $reasons_id  = array_column($reasonsAppl, 'fk_isgaApplicationReason');
            if(!empty($reasons_id)){
                $result      = $reasons_id;
            }else{
                $result = FALSE;
            }
        } else {
            $reason = $this->getLinkedItem("isga_ApplicationReasons", array(
                    "Id" => $this->getField('fk_isgaApplicationReason')
                ));
            if(!empty($reason)){
                $result = array($reason->getField('Id'));
            }else{
                $result = FALSE;
            }
        }

        return $result;
    }
     public function getTypeId() {
        if ($this->getField('fk_isgaApplicationReason') == '1') {
            $reasonsAppl = $this->getLinkedItem("isga_mtm_Application_Reason", array(
                "fk_isgaApplication" => $this->fields["Id"]
            ));
            if(empty($reasonsAppl)) return null;
            if($reasonsAppl->isEmptyField('fk_isgaApplicationReason')) return null;
            $reasons     = $this->getLinkedItem("isga_ApplicationReasons", array(
                "Id" => $reasonsAppl->getField('fk_isgaApplicationReason')
            ));
            if(empty($reasons)) return null;
            if($reasons->isEmptyField('fk_isgaApplicationType')) return null;
            $result      = $reasons->getField('fk_isgaApplicationType');
        } elseif($this->getField('fk_isgaApplicationReason') !='NULL') {
            $reason = $this->getLinkedItem("isga_ApplicationReasons", array(
                        "Id" => $this->getField('fk_isgaApplicationReason')
                    ));
            if(empty($reason)) return null;
            if($reason->isEmptyField('fk_isgaApplicationType')) return null;
            $result = $reason->getField('fk_isgaApplicationType');
        }
        if($result){
            return $result;
        }else{
            return null;
        }
    }
    public function getReason(){
        if(!isset($this->fields["fk_isgaApplicationReason"]) || $this->fields["fk_isgaApplicationReason"]=="" || strtolower($this->fields["fk_isgaApplicationReason"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_ApplicationReasons",array(
            "Id" => $this->fields["fk_isgaApplicationReason"]
        ));
        return $result;
    }

    /**
     * Метод возвращает связанные с заявкой филиалы , через связь многие ко многим
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getLinkedBranches(){
        $eoTable = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");
        $tg = $eoTable->getGateway();

        //Строим запрос на получение всех связанных филиалов


        //Подзапрос
        $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
        $subselect = $sql->select();

        $subselect->from('isga_mtm_Application_Branches')
            ->columns(array('fk_eiisEOBranch'))
            ->where(array('fk_isgaApplication' => $this->getField("Id")));

        //Запрос
        $resultSet = $tg->select(function(\Zend\Db\Sql\Select $select) use ($subselect, $tg){
            $select
                ->where->in('Id', $subselect);

            //$sql = $tg->getSql();
            //print_r($sql->getSqlstringForSqlObject($select));die;
        });
        return $resultSet;
    }

    public function getLinkedPrograms(){
        $programs = $this->getLinkedItems("isga_ApplicationPrograms",array(
            "fk_isgaApplication" => $this->fields["Id"]
        ));
        return $programs;
    }
     public function getLinkedMoreInfo(){
        $programs = $this->getLinkedItem("isga_ApplicationMoreInfo",array(
            "fk_isgaApplication" => $this->fields["Id"]
        ));
        return $programs;
    }
    public function getLinkedBranchesAppl(){
        $programs = $this->getLinkedItems("isga_mtm_Application_Branches",array(
            "fk_isgaApplication" => $this->fields["Id"]
        ));
        return $programs;
    }
    public function getLinkedFiles(){
        $files = $this->getLinkedItems("isga_ApplicationFiles",array(
            "fk_isgaApplication" => $this->fields["Id"]
        ));
        return $files;
    }
    public function generateBarcode()
    {
        $code = $this->getBarcode();
        $result = $this->getLinkedItem($this->tableName, array('Barcode' => $code));
        if ($result) {
            return $this->generateBarcode();
        }
        return $code;
    }

    public function getBarcode()
    {
        $country_code = '460';
        $manufacturer_code = $this->randNumber(4);
        $product_code = $this->randNumber(5);
        $code = $country_code . $manufacturer_code . $product_code;
        return $code;
    }

    function randNumber($count = 1)
    {
        $res = '';
        for ($i = 0; $i < $count; ++$i) {
            $res .= rand(0, 9);
        }
        return $res;
    }

    public function calcControlBarcodeNumber($code)
    {
        $odd  = $even = 0;
        for ($i=0; $i < strlen($code); ++$i) {
            if (($i + 1) % 2 === 0) {
                $even += intval($code[$i]);
            } else {
                $odd += intval($code[$i]);
            }
        }
        $sum = $odd + ($even * 3);
        $number = 10 - ($sum % 10);
        if ($number === 10) {
            return 0;
        }
        return $number;
    }

   public function getLicensedProgramsFieldIds($id_license, $id_org, $field = 'fk_eiisEduProgramType',$id_program_type=Null,$ugs=null)
    {
        $licenseSupplements = $this->selectComplicated(
            "eiis_LicenseSupplements",
            array('Id'),
            array('fk_eiisLicense' => $id_license, 'fk_eiisBranch' => $id_org,"fk_eiisLicenseSupplementState"=>"9416C0E6E84F4194B57DA930AD10A4D0")
        )->toArray();
        if(!count($licenseSupplements)) {
            return array();
        }
        $licenseSupplementsIds = array_unique(array_column($licenseSupplements, 'Id'));
        if($id_program_type!=NULL){
            $licensedPrograms = $this->selectComplicated(
                "eiis_LicensedPrograms",
                array('Id','Code','fk_eiisEduProgramType',$field),
                array('fk_eiisLicenseSupplement' => $licenseSupplementsIds,'fk_eiisEduProgramType'=>$id_program_type)
            )->toArray();
        } else {
            $licensedPrograms = $this->selectComplicated(
                "eiis_LicensedPrograms",
                array('Id','Code','fk_eiisEduProgramType',$field),
                array('fk_eiisLicenseSupplement' => $licenseSupplementsIds)
            )->toArray();
        }
        if(!count($licensedPrograms)) {
            return array();
        }
        if($ugs==null){
            $mas = array_column($licensedPrograms, $field);
            return array_unique($mas);
        } else {

            $ids                = array_column($licensedPrograms, $field);
                $eduProgramsIds = $this->selectComplicated(
                                "eiis_EduPrograms", array('Id'), array('Id' => $ids)
                        )->toArray();
            if (count($eduProgramsIds) < 1) {
                $codes                  = array_column($licensedPrograms, 'Code');
                $fk_eiisEduProgramTypes = array_column($licensedPrograms, 'fk_eiisEduProgramType');
                $eduProgramsIds         = $this->selectComplicated(
                            "eiis_EduPrograms", array('Id'), array('fk_eiisEduProgramType' => $fk_eiisEduProgramTypes, 'Code' => $codes)
                    )->toArray();
            }
            return array_column($eduProgramsIds, 'Id');
        }
    }

    public function getLicensedProgramsByCertificateFieldIds($id_certificate, $id_org, $field = 'fk_eiisEduProgramType',$id_program_type=Null)
    {
        $new_certificate=array();
        if(!is_array($id_certificate)){
            $new_certificate[]=$id_certificate;
            $this->changeDesh($id_certificate);
            $new_certificate[]=$id_certificate;
        }else{
            $new_certificate = $id_certificate;
            array_walk($id_certificate,"self::changeDesh");
            $new_certificate=array_merge($new_certificate, $id_certificate);
        }
        $new_org=array();
        if(!is_array($id_org)){
            $new_org[]=$id_org;
            $this->changeDesh($id_org);
            $new_org[]=$id_org;
        }else{
            $new_org = $id_org;
            array_walk($id_org,"self::changeDesh");
            $new_org=array_merge($new_org, $id_org);
        }
        $certificateSupplements = $this->selectComplicated(
            "isga_CertificateSupplements",
            array('Id'),
            array('fk_isgaCertificate' => $new_certificate, 'fk_eiisEducationalOrganization' => $new_org)
        )->toArray();
        if(!count($certificateSupplements)) {
            return array();
        }
        $certificateSupplementsIds = array_unique(array_column($certificateSupplements, 'Id'));
        $accredProg = $this->selectComplicated(
            "isga_AccreditedPrograms",
            array('fk_eiisLicensedProgram'),
            array('fk_isgaCertificateSupplement' => $certificateSupplementsIds)
        )->toArray();
        if(!count($accredProg)) {
            return array();
        }
        $licensedIds = array_unique(array_column($accredProg, 'fk_eiisLicensedProgram'));
        $newLicenseIds=$licensedIds;
        array_walk($licensedIds,"self::changeDesh");
        $newLicenseIds=array_merge($newLicenseIds, $licensedIds);
        if($id_program_type!=NULL){
            $licensedPrograms=$this->selectComplicated(
                "eiis_LicensedPrograms",
                array($field),
                array('Id' => $newLicenseIds,'fk_eiisEduProgramType'=>$id_program_type)
            )->toArray();
        }else{
            $licensedPrograms=$this->selectComplicated(
                "eiis_LicensedPrograms",
                array($field),
                array('Id' => $newLicenseIds)
            )->toArray();
        }
        $mas = array_column($licensedPrograms, $field);

        return array_unique($mas);
    }

    public function getApplicationBranches($applications)
    {
        if (!$applications) {
            return array();
        }
        $applicationsIds = array_column($applications, 'Id');
        $applicationsOrgs = array_column($applications, 'fk_eiisEducationalOrganization', 'Id');
        $applicationBranchesArray = $this->selectComplicated(
            'isga_mtm_Application_Branches',
            array('fk_isgaApplication', 'fk_eiisEOBranch'),
            array('fk_isgaApplication' => $applicationsIds)
        )->toArray();
        if (!$applicationBranchesArray) {
            return array();
        }
        $applicationBranchesIds = array_column($applicationBranchesArray, 'fk_eiisEOBranch');
        $educationalOrganizations = $this->selectComplicated(
            'eiis_EducationalOrganizations',
            array('Id', 'ShortName', 'RegularName', 'fk_eiisParentEducationalOrganization'),
            array('Id' => array_values($applicationBranchesIds))
        )->toArray();
        if (!$educationalOrganizations) {
            return array();
        }
        $applicationBranchesNames = array_column($educationalOrganizations, 'ShortName', 'Id');
        $applicationBranchesRegularNames = array_column($educationalOrganizations, 'RegularName', 'Id');
        $applicationBranchesParent = array_column($educationalOrganizations, 'fk_eiisParentEducationalOrganization', 'Id');
        $applicationBranches = array();
        $applicationsOrgs = array_change_key_case($applicationsOrgs,CASE_UPPER);
        foreach ($applicationBranchesArray as $branche) {
            if(empty($applicationBranchesParent[$branche['fk_eiisEOBranch']])){
                continue;
            }
            $branche['fk_isgaApplication'] = strtoupper($branche['fk_isgaApplication']);
            if (
                isset($applicationsOrgs[  $branche['fk_isgaApplication']]) &&
                $branche['fk_eiisEOBranch'] === $applicationsOrgs[ $branche['fk_isgaApplication']]
            ) {
                continue;
            }
            if(isset($applicationBranchesNames[$branche['fk_eiisEOBranch']])  ){
                if($applicationBranchesNames[$branche['fk_eiisEOBranch']]=='NULL' || empty($applicationBranchesNames[$branche['fk_eiisEOBranch']])){
                    /*$eduQuest = new \Model\Entities\eiisEducationalOrganizations($branche['fk_eiisEOBranch']);
                    if(!$eduQuest->isEmptyField('ShortName')){
                        $nameBranch = $eduQuest->getFieldOrSpace('ShortName');
                    }else{
                        $nameBranch = $eduQuest->getFieldOrSpace('RegularName');
                    }
                    $applicationBranchesNames[$branche['fk_eiisEOBranch']]= $nameBranch;
                    unset ($eduQuest);*/
                    $applicationBranches[$branche['fk_isgaApplication']][] = $applicationBranchesRegularNames[$branche['fk_eiisEOBranch']];
                }else{
                    $applicationBranches[$branche['fk_isgaApplication']][] = $applicationBranchesNames[$branche['fk_eiisEOBranch']];
                }
            /*}else{
                $applicationBranches[$branche['fk_isgaApplication']][]='';*/
            }
        }
        return $applicationBranches;
    }

    public function getApplicationStatus($ids){

        if(gettype($ids)=='array'){
            foreach ($ids as $key => $value) {
                $ids[$key]=  strtoupper($value);
            }
        }else{
            $ids=  strtoupper($ids);
        }
        return $this->selectComplicated(
            'isga_ApplicationStatuses',
            array('Id'=>new \Zend\Db\Sql\Expression('upper(Id)'),
                'Name',
                'IsgaTextForPublication'
                ),
            array('Id' => $ids)
        )->toArray();

    }

   public function getSignedDeclaration(){
        $files = $this->getLinkedItem("isga_ApplicationDocumentGroups",array(
            "fk_isgaApplication" => $this->fields["Id"]
        ));
        return $files;
    }
    public function checkSignedDeclaration() {
        $countArray = $this->selectComplicated(
                        'isga_ApplicationDocumentGroups', array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')), array('fk_isgaApplicationDocumentType'=>"0B4B6324-5DAE-4AB8-B973-86C4A39409E1",'fk_isgaApplication' => $this->getField('Id'),"isArhive"=>0)
                )->toArray();
        $count = array_column($countArray, 'count');
        if ($count[0] > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getCertificate(){
        if(!isset($this->fields["fk_isgaPreviousCertificate"]) || $this->fields["fk_isgaPreviousCertificate"]=="" || strtolower($this->fields["fk_isgaPreviousCertificate"]) == "null")
            return null;

        $result = $this->getLinkedItem("isga_Certificates",array(
            "Id" => $this->fields["fk_isgaPreviousCertificate"]
        ));
        return $result;
    }

    public function changeDesh(&$id_certificate) {
        if (strpos($id_certificate, '-')) {
            $id_certificate = $this->stripIdDashes($id_certificate);
        } else {
            $id_certificate = $this->fillIdDashes($id_certificate);
        }
    }

    public function getEducationalOrganization(){
        if($this->isEmptyField("fk_eiisEducationalOrganization")) {
            //если нет Ид попытка по короткому имени и регулярному имени
            if($this->isEmptyField('DeclaredRegularName')
              || $this->isEmptyField('DeclaredShortName')){
                 return null;
              }
              return $this->getLinkedItem('eiis_EducationalOrganizations',
                     array('RegularName' => $this->getField('DeclaredRegularName'),
                            'ShortName' => $this->getField('DeclaredShortName')
                ));
        }
        $result =  $this->getLinkedItem('eiis_EducationalOrganizations',
           array('Id' => str_replace('-','',strtoupper($this->getField("fk_eiisEducationalOrganization")))));
        if( empty($result ) ){
            $result =  $this->getLinkedItem('eiis_EducationalOrganizations',
               array(
                   'Id' => strtoupper($this->getField("fk_eiisEducationalOrganization"))
                )
            );            
        }
        return $result;
    }
    public function isOrderDeny()
    {
        if (empty($this->fields['Id'])){
            return null;
        }
        $orderDocumentKind=$this->getOrderDoocumentKind();
        $applicationIsDeny=false;
        if (!empty($orderDocumentKind)){
            $kindCode = $orderDocumentKind->getField('Code');
            $applicationIsDeny =
              $kindCode == 'DenyAccreditationInstruction'
              || $kindCode == 'AccreditationDeny'
              || $kindCode == 'ReAccreditationDeny';
        }
        return $applicationIsDeny;
    }
    public function getOrderDoocumentKind()
    {
        if (!empty($this->fields['fk_OrderDocumentKind'])) {
            $result = new \Model\Entities\isgaOrderDocumentKinds($this->fields['fk_OrderDocumentKind']);
            return $result;
        }
        $orderDoc = $this->getOrderDocument();
        if (empty($orderDoc)) {
            return null;
        }
        $result =  $orderDoc->getKind();
        return $result;
    }
    public function getOrderDocument(){
        if (empty($this->fields['fk_isgaOrderDocument'])){
            return null;
        }
        $result=new \Model\Entities\isgaOrderDocuments($this->fields['fk_isgaOrderDocument']);
        return $result;
    }
    public function getEducationalOrgId(){
        if(!$this->isEmptyField('fk_eiisEducationalOrganization')){
            return $this->fields['fk_eiisEducationalOrganization'];
        }else{
            $license = $this->getLinkedItem("eiis_Licenses",array(
                        "Id" => $this->fields["fk_eiisLicense"]
            ));
            if($license){
                return $license->getField('fk_eiisEducationalOrganization');
            }else{
                return false;
            }
        }
    }
     public function getIdsReasons() {
        if ($this->getField('fk_isgaApplicationReason') == '1') {
            $result = $this->getLinkedItems("isga_mtm_Application_Reason", array(
                        "fk_isgaApplication" => $this->fields["Id"]
                    ))->toArray();
            $result = array_column($result, 'fk_isgaApplicationReason');
        } else {
            $result = array($this->getField('fk_isgaApplicationReason'));
        }
        return $result;
    }

    public function getReasonArray() {
        if(empty($this->fields['Id'])){
            return null;
        }
        $reasonsmtm = new \Model\Entities\isgamtmApplicationReason();
        $reasons = $reasonsmtm->getReasonArray($this->fields['Id']);
        if (!empty ($reasons)){
            return $reasons;
        }
        $reasons = array();
        $reasonId = $this->getField('fk_isgaApplicationReason');
        $reasons = $this->selectComplicated('isga_ApplicationReasons',array(), array("Id"=>$reasonId) )->toArray();
        return $reasons;
    }

    public function getReasonCodeArray() {
        if(empty($this->fields['Id'])){
            return null;
        }
        $reasons = $this->getReasonArray();
        if (empty($reasons)){
            return array();
        }
        $reasonsCode = array_column($reasons, 'Code');
        return $reasonsCode;
    }

    public function getReasons() {
        if(empty($this->fields['Id'])){
            return null;
        }
        $reasonsmtm = new \Model\Entities\isgamtmApplicationReason();
        $reasons = $reasonsmtm->getReasons($this->fields['Id']);
        if (!empty ($reasons)){
            return $reasons;
        }
        $reasons = array();
        $reasonId = $this->getField('fk_isgaApplicationReason');
        $reasons[] = new \Model\Entities\isgaApplicationReasons($reasonId);
        return $reasons;
    }

    /**
     * Присваивает новый статус заяввлению
     * и вносит это дело в историю
     *
     * @param $newStatus - Ид нового статуса
     * @param $changeDate - дата изменения, если надо 'YYYY-mm-dd'
     */
    public function setNewStatus($newStatus, $changeDate = null)
    {
        if (empty($newStatus)||$this->isEmptyField($this->idName)) {
            return null;
        }

        $oldStatus = $this->getFieldOrSpace('fk_isgaApplicationStatus');
        $this->exchangeArray(array('fk_isgaApplicationStatus' => $newStatus));

        //сохраняем
        if ($this->save()) {
            $saveHistory = array(
                'StatusOld'          => $oldStatus,
                'StatusNew'          => $newStatus,
                'fk_isgaApplication' => $this->getId(),
                'fk_ronUser'         => 0,
            );
            if(!empty($changeDate)){
                $saveHistory['DateChange'] = $changeDate;
            }
            $historyClass = new \Model\Entities\isgaHistoryStatus();
            $historyClass->exchangeArray($saveHistory);
            $result = $historyClass->save(1);
            return $result;
        }else{
            return 0;
        }
    }
    
    /**
     * дергается программы для исключения
     *
     * @param type $id_cert
     * @param type $id_org
     * @param type $field
     * @param type $id_program_type
     * @param type $ugs      - если пусто то вернуть ид программ
     * @param type $prLish   - тип программ: 1 - лишение 2 - исключение
     * @return type
     */
    public function getProgramsIdsDeprivation($id_cert, $id_org, $field = 'fk_eiisEduProgramType',$id_program_type=Null,$ugs=null, $prLish = 2 )
    {
        $additionalWhere = false;
        if( is_array( $field) ){
            reset($field);
            $fieldName = key($field);
        }
        else{
            $fieldName = $field;
        }
        
        $addWhere = new \Zend\Db\Sql\Where();
        $addWhere->isNull('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended');
        $addWhere->or;
        $addWhere->like('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended','null');
        $addWhere->or;
        $addWhere->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended',0);
        $addWhere->or;
        $addWhere->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended','');
        
        if( 2 == $prLish ){
            $addWhere2 = new \Zend\Db\Sql\Where();
            $addWhere2->or;
            $addWhere2->isNull('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled');
            $addWhere2->or;
            $addWhere2->like('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled','null');
            $addWhere2->or;
            $addWhere2->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled',0);
            $addWhere2->or;
            $addWhere2->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled','');
        }
        if( 1 == $prLish ){
            $addWhere2 = new \Zend\Db\Sql\Where();
            $addWhere2->isNotNull('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled');
            $addWhere2->and;
            $addWhere2->notLike('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled','null');
            $addWhere2->and;
            $addWhere2->notEqualTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled',0);
            $addWhere2->and;
            $addWhere2->notEqualTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled','');
        }
        $additionalWhere = array(
            "fk_isgaCertificateSupplementStatus" => array(
                "4c3c4932-d4c8-4524-83c5-0b5039403a09",
                //'67f82aa6-edf8-4115-bd3f-8a4f348782c0',
                '14b2d4a4-c96b-4e9a-b65e-dab6480d8865' ),
            $addWhere,
            $addWhere2,
        );
        //для лишений добавить статус приложения 14b2d4a4-c96b-4e9a-b65e-dab6480d8866
        if( 1 == $prLish ){
            $additionalWhere['fk_isgaCertificateSupplementStatus'][] ='14b2d4a4-c96b-4e9a-b65e-dab6480d8866';
        }
        if( 2 == $prLish ){
            $additionalWhere['fk_isgaCertificateSupplementStatus'][] ='67f82aa6-edf8-4115-bd3f-8a4f348782c0';
        }        
        if( !empty( $id_program_type ) ){
            $additionalWhere  [ "fk_eiisEduProgramType" ] = $id_program_type;
        }
        $needFields = array( "licProgramIds" => "fk_eiisLicensedProgram", "Code", "fk_eiisEduProgramType" );
        if( is_array( $field ) ){
            $needFields = array_merge( $needFields , $field ) ;                        
        }
        else{
            $needFields[] = $field ;            
        }
        $accredProgram = $this->getAccreditedPrograms( $id_cert, $id_org, $needFields, $additionalWhere );
        if( !count( $accredProgram ) ){
            return array();
        }
        if( empty($ugs) ){
            $mas = array_column( $accredProgram, $fieldName );
            return array_unique( $mas );
        }
        else {
            $ids = array_column( $accredProgram, $fieldName );
            $eduProgramsIds = $this->selectComplicated(
                    "eiis_EduPrograms", array( 'Id' ), array( 'Id' => $ids )
                )->toArray();
            if( count( $eduProgramsIds ) < 1 ){
                $codes = array_column( $accredProgram, 'Code' );
                $fk_eiisEduProgramTypes = array_column( $accredProgram, 'fk_eiisEduProgramType' );
                $eduProgramsIds = $this->selectComplicated(
                        "eiis_EduPrograms", array( 'Id' ), array( 'fk_eiisEduProgramType' => $fk_eiisEduProgramTypes, 'Code' => $codes )
                    )->toArray();
            }
            return array_column( $eduProgramsIds, 'Id' );
        }
        /*
        $additionalWhere =false;
        if($id_program_type!=NULL){
            $additionalWhere =["fk_eiisEduProgramType"=>$id_program_type];
        }
        $needFields = array("licProgramIds"=>"fk_eiisLicensedProgram","Code","fk_eiisEduProgramType",$field);
        $accredProgram =$this->getAccreditedPrograms($id_cert,$id_org,$needFields,$additionalWhere);
        if(!count($accredProgram)) {
            return array();
        }
        if($ugs==null){
            $mas = array_column($accredProgram, $field);
            return array_unique($mas);
        } else {
            $ids                = array_column($accredProgram, $field);
            $eduProgramsIds = $this->selectComplicated(
                                "eiis_EduPrograms", array('Id'), array('Id' => $ids)
                        )->toArray();
            if (count($eduProgramsIds) < 1) {
                $codes                  = array_column($accredProgram, 'Code');
                $fk_eiisEduProgramTypes = array_column($accredProgram, 'fk_eiisEduProgramType');
                $eduProgramsIds         = $this->selectComplicated(
                            "eiis_EduPrograms", array('Id'), array('fk_eiisEduProgramType' => $fk_eiisEduProgramTypes, 'Code' => $codes)
                    )->toArray();
            }
            return array_column($eduProgramsIds, 'Id');
        }
         * 
         */
    }
    /**
     * Получение аккредитованых программ из сертификата для конкретной организации
     * @param string/array $idCertificate - guid сертификата строка или массив если несколько сертификатов
     * @param string/array $idOrgs - guid организации строка или массив если несколько организаций
     * @param array $fields - массив со списком необходимых полей
     * @param array/bool $where - дополнительные условия по умолчанию false -только стандартные условия
     * @return array
     */
    public function getAccreditedPrograms($idCertificate,$idOrgs,$fields,$where=false){
        $standartWhere = array(
                'fk_isgaCertificate' => $idCertificate,
                'isga_CertificateSupplements.fk_eiisEducationalOrganization' => $idOrgs,
                "fk_isgaCertificateSupplementStatus"=>"4c3c4932-d4c8-4524-83c5-0b5039403a09"
            );
        if(!$where){
            $fullWhere = $standartWhere;
        }else{
            $fullWhere = array_merge($standartWhere,$where);
        }
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('isga_CertificateSupplements')
            ->columns(array("Id"), true)
            ->join('isga_AccreditedPrograms',"isga_AccreditedPrograms.fk_isgaCertificateSupplement = isga_CertificateSupplements.Id",$fields)
            ->where($fullWhere);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $AccreditedPrograms = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE)->toArray();
        return $AccreditedPrograms;
    }
    public function getApplicationProgramsDeprivation(){
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('isga_ApplicationProgram')
            ->columns(array("Id"), true)
            ->join('isga_AccreditedPrograms',"isga_ApplicationProgram.fk_eiisLicensedPrograms = isga_AccreditedPrograms.Id",array("startYear","Name","EduNormativePeriod","FullTimeFormStudentsNumber",))
            ->where($fullWhere);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $AccreditedPrograms = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE)->toArray();
        return $AccreditedPrograms;
    }
    /**
     * Проверяет по id есть ли необходимые причины
     * @param string/array $needIds - один id приччины или массив причин
     * @return bool есть ли хоть одна из необходимых причин
     */
    public function checkReasonInApplication($needIds){
        $result = false;
        $this->getLinkedItems("isga_mtm_Application_Reason", array(
                        "fk_isgaApplication" => $this->fields["Id"]
                    ))->toArray();
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $sql = new \Zend\Db\Sql\Sql($dbAdapter);
        $select = $sql->select();
        $select->from('isga_mtm_Application_Reason')
            ->columns(["count" => new \Zend\Db\Sql\Expression('COUNT(Id)')], true)
            ->where(["fk_isgaApplication"=>$this->fields["Id"],"fk_isgaApplicationReason"=>$needIds]);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $countReason = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE)->toArray();
        $count=array_column($countReason,"count");
        if($count[0]>0){
            $result = true;
        }
        return $result;
    }

    public function getLinkedProgramsChecked( $isIskluchit = false ){
        if (empty($this->fields["Id"])){
            return null;
        }
        $where = array(
            "fk_isgaApplication" => $this->fields["Id"],
        );
        if( $isIskluchit ){
            $where['PrLish'] = 1;
        }
        else{
            $where['PrLish'] = 0;
            $where['Checked'] = 1;
        }
        $programs = $this->getLinkedItems("isga_ApplicationPrograms",
            //array('Id','fk_isgaEOBranches'),
            $where
        );
        return $programs;
    }
    
    /**
     * определяется заявление по короткому пути или длинному
     * @return boolean - истина - длинному, фалсе - короткому
     */
    public function isShortPath(){
        $reasonId = $this->getIdsReasons();
        $reasonId = reset( $reasonId );
        if( in_array( $reasonId, array(
            '24e060cb-6384-4009-b9ce-93a775678732',
            '67f97827-57f9-4a0c-b2c7-d9806969bef6',
            '40281165-369a-465e-a3a6-54596aec6990'
        ) )){
            return false;
        }
        return true;
    }
}