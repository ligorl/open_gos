<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class expertiseprogram extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expertise_program";
    protected $idName = "id";
    
    public function getCommissionMember() {
        if($this->isEmptyField('id')||$this->isEmptyField('expertise_id')){
            return null;
        }
        
        $result = $this->getLinkedItem('commission_member', 
            array(
                'expertise_id' => $this->getField('expertise_id'),
                'expertise_program_id' => $this->getId(),
            )
        );
        return $result;
    }
    
    public function getBaseEducationProgram() {
        if ($this->isEmptyField('base_education_program_id')){
            return null;
        }
        $result = $this->getLinkedItem(
                'base_education_program',
                array('id' => 'base_education_program_id')
            
        );
        return $result;
    }
}