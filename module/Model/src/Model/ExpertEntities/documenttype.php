<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class documenttype extends Gateway\ExpertsBaseEntity{
    protected $tableName = "document_type";
    protected $idName = "id";
    
    public function getByIds($param = null) {
        if (empty($param)){
            return null;
        }
        $result =  $this->getLinkedItems($this->tableName,array($this->idName=>$param) );
        return $result;
    }
}
