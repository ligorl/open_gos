<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class chairman extends Gateway\ExpertsBaseEntity{
    protected $tableName = "chairman";
    protected $idName = "id";

    public function getExpert() {
        if($this->isEmptyField('expert_id')){
            return null;
        }
        $result = $this->getLinkedItem('expert',
            array(
                'id'=>  $this->getField('expert_id'),
            )
        );
        return $result;
    }

    public function getEducationalOrganization(){
        if($this->isEmptyField('institution_id')){
            return null;
        }
        $linkedTable = new \Model\Gateway\EntitiesTable('eiis_EducationalOrganizations');
        $result = $linkedTable->getEntitiesByWhere(array('Id'=>$this->getField('institution_id')));
        return $result->current();
    }
    
    public function getUser() {
        if($this->isEmptyField('user_id')){
            return null;
        }
        $user = new \Model\Entities\isgaUsers();
        $result = $user->getLinkedItem('ron_Users',
            array(
                'Id'=>  $this->getField('user_id'),
            )
        );
        return $result;
    }


    public function isRefused() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $contacting = $this->getLinkedItem('contacting', array(
                'expert_id' => $this->getField('expert_id'),
                'expertise_id' => $this->getField('expertise_id'),
            ));
        if($contacting){
            if ($contacting->getFieldOrSpace('contacting_result') == 'СonsentRefused'){
                return true;
            }else{
                return false;
            }
        }
        return null;
    }

    public function isConfirm() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $contacting = $this->getLinkedItem('contacting', array(
                'expert_id' => $this->getField('expert_id'),
                'expertise_id' => $this->getField('expertise_id'),
            ));
        if($contacting){
            if ($contacting->getFieldOrSpace('contacting_result') == 'СonsentConfirm'){
                return true;
            }else{
                return false;
            }
        }
        return null;
    }
    
}