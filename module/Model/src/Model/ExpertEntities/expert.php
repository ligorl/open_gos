<?php
namespace Model\ExpertEntities;

use Zend\Db\Sql\Sql;
use Model\Gateway;

class expert extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expert";
    protected $idName = "id";

    /**
     * получение персональных данных
     * @return Object
     */
    public function getPersonInfo() {
        if($this->isEmptyField('person_id')){
            return null;
        }
        $result = $this->getLinkedItem("person", array(
            "id" => $this->fields["person_id"]
        ));
        return $result;
    }

    /**
     * получение ученой степени
     * @return String
     */
    public function get_academic_degree(){
        if(!isset($this->fields["academic_degree_id"]) || $this->fields["academic_degree_id"] == "")
            return '';
        $result = $this->getLinkedItem("academic_degree", array(
            "id" => $this->fields["academic_degree_id"]
        ));
        return empty($result)?'':$result->getField("name");
    }

    /**
     * получение ученой степени
     * @return String
     */
    public function get_academic_rank(){
        if(!isset($this->fields["academic_rank_id"]) || $this->fields["academic_rank_id"] == "")
            return '';

        $result = $this->getLinkedItem("academic_rank", array(
            "id" => $this->fields["academic_rank_id"]
        ));
        return empty($result)?'':$result->getField("name");
    }

    /**
     * получение ID региона
     * @return Integer
     */
    public function get_geo_address(){
       /* if(!isset($this->fields["const_reg_address"]) || $this->fields["const_reg_address"] == "")
            return null;

        $result = $this->getLinkedItem("geo_address", array(
            "id" => $this->fields["const_reg_address"]
        ));
        return $result->getRegion();*/
        /*if(!isset($this->fields["region_id"]) || $this->fields["region_id"] == "")
            return null;

        $result = $this->getLinkedItem("eiis_Regions", array(
                "id" => $this->fields["region_id"]
            ));
        if ($result) {
            return $result->getField('Name');
        } else {*/
            return '';
       /*}*/
    }

    public function getExpertProgram($expert_id){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $ronDbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $baseName = $ronDbAdapter->getCurrentSchema();

        $selectString = "SELECT e.* FROM expert_edu_programms e
            LEFT JOIN ".$baseName.".eiis_EduProgramTypes ep ON ep.fk_eiisEduLevelsExpert=e.education_level_id
            LEFT JOIN ".$baseName.".isga_EnlargedGroupSpecialities ie ON ie.Id=e.speciality_group_id
            WHERE expert_id = '".$expert_id."'
            GROUP BY e.education_level_id, e.speciality_group_id
            ORDER BY ep.SortOrder ASC,ie.code ASC";
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    public function getExpertComments(){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('expert_comments');
        $select->where(array('expert_id'=>$this->fields['id']));
        $select->order(array('date'=>'DESC', 'version' => 'DESC' ));
        $select->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select); //print $selectString;die;
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }


    public function getExpertCertificates(){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('expert_certificates');
        $select->where(array('expert_id'=>$this->fields['id']));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

     public function getAddressReg() {
         if(!isset($this->fields["const_reg_address"]) || $this->fields["const_reg_address"] == "")
            return null;

        $result = $this->getLinkedItem("geo_address", array(
            "id" => $this->fields["const_reg_address"]
        ));

        return $result;

    }
     public function getAddressFact() {
         if(!isset($this->fields["department_legal_address"]) || $this->fields["department_legal_address"] == "")
            return null;

        $result = $this->getLinkedItem("geo_address", array(
            "id" => $this->fields["department_legal_address"]
        ));
            return $result;

    }

    public function getContact(){
        $result = $this->getLinkedItem("contact", array(
                "id" => $this->fields["contact"]
            ));
        if ($result) {
            return $result;
        } else {
            return '';
        }
    }

    public function getParentOrganizationName(){
        if(!isset($this->fields["parent_id"]) || $this->fields["parent_id"] == "")
            return '';
        $result = $this->getLinkedItem("expert", array(
                "id" => $this->fields["parent_id"]
            ));
        if ($result) {
            return $result->getField('last_name');
        } else {
            return '';
        }
    }

    public function getEducationalOrganizationWork() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $result  = $this->getLinkedItems('person_work', array('person_id'=>  $this->getId(),'now' => '0'));
        return $result;
    }

    public function getEducationalOrganizationWorkNow() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $result  = $this->getLinkedItem('person_work', array('person_id'=>  $this->getId(),'now' => '1'));
        return $result;
    }

    public function getEducationalOrganizationWorkOld() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $result  = $this->getLinkedItems('person_work', array('person_id'=>  $this->getId(),'now' => '2'));
        return $result;
    }

    public function getEducationalOrganizationEducation() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $result  = $this->getLinkedItems('person_education', array('person_id'=>  $this->getId()));
        return $result;
    }

    public function getFio(){
        $name = array();
        if(!$this->isEmptyField("last_name")) $name[]=$this->getField("last_name");
        if(!$this->isEmptyField("name")) $name[]=$this->getField("name");
        if(!$this->isEmptyField("middle_name")) $name[]=$this->getField("middle_name");
        return implode(" ", $name);
    }

    public function getLastExpertiseDate(){
        $workTable = new \Model\Gateway\ExpertsEntitiesTable("expert_work");
        $work = $workTable->getAllEntities(null,"begin_date DESC",array(
            "expert_dictionary_id" => $this->getField("id")
        ))->getObjectsArray();
        if(count($work) > 0){
            $lastWork = $work[0];
            return $lastWork->getDate("begin_date");
        } else return "";
    }

    public function getExpertiseCount(){
        $workTable = new \Model\Gateway\ExpertsEntitiesTable("expert_work");
        $workcount =  $workTable->getCount(array("expert_dictionary_id" => $this->getField("id")));
        return $workcount;
    }

    public function getLastDecline(){
        $contactingTable = new \Model\Gateway\ExpertsEntitiesTable("contacting");

        $contactings = $contactingTable->getAllEntities(array(
            "page" => 1,
            "onPage" => 1
        ), "contact_timestamp DESC", array(
            "expert_id" => $this->getField("id"),
            "contacting_result" => "Отказ"
        ))->getObjectsArray();

        if(count($contactings) == 0) return null;

        return $contactings[0];
    }

    /**
     * возращает первый вуз в списке мест работы эксперта
     * @return type
     */
    public function getPersonWork() {
        $personWorkEduOrg = $this->getEducationalOrganizationWork();
        $personWorkEduOrg = $personWorkEduOrg->current();
        if($personWorkEduOrg){
            return $personWorkEduOrg->getEducationalOrganization();
        }
        return null;
    }

    /**
     * создаем строку запроса из присланого фильтра
     * 
     * @param type $filter
     * @return string
     */
    public function makeWhereStringFromFilter($filter) {
        if (empty($filter)){
            return '1';
        }
        if(!empty($filter['stringFilter'])){
            $stringFilter = $this->makeWhereStringFromSearchString($filter['stringFilter']);
        }
        if(!empty($filter['regionFilter'])){
            $regionFilter = ' ( region_id = '.$filter['regionFilter'] .' ) ';
        }
        if(empty($stringFilter) && empty($regionFilter)){
            return '1';
        }
        if(empty($stringFilter)||empty($regionFilter)){
            $stringWhere = $stringFilter.$regionFilter;
        }else{
            $stringWhere = $stringFilter.' AND '.$regionFilter;
        }
        return $stringWhere;
    }

    
    /**
     * перетворяем строку фильтра в запрос
     * 
     * @param type $filter
     * @return string
     */
    public function makeWhereStringFromSearchString($filter) {

        $filter = strip_tags($filter);
        $filter = \addslashes($filter);
        $orgFilterString='';
        $orgFilterNumberString='';
        $orgFilterNumberArray=array();
        $orgFilterStringArray=array();
        $orgFilterArray=explode(' ',$filter);
        if(!(count($orgFilterArray))){
            $orgFilterArray=array($filter);
        }        
        foreach ( $orgFilterArray as $orgFilterItem) {
            //только номера
            if(preg_match('/^[0-9]+$/', $orgFilterItem)){
                $filterOnlyNumberArray[]=$orgFilterItem;
                $orgFilterNumberArray[]=' ( '
                    .' upper(code) LIKE "%' . $orgFilterItem . '%" '
                    .' )  ';
            }else{
                //формируем перечень названий в вузе
                $orgFilterItem=  mb_strtoupper($orgFilterItem, 'utf8');
                $orgFilterStringArray[] =' ( '
                        .' upper(last_name) LIKE "%' . $orgFilterItem . '%" '
                        .' OR '
                        .' upper(middle_name) LIKE "%' . $orgFilterItem . '%" '
                        .' OR '
                        .' upper(name) LIKE "%' . $orgFilterItem . '%" '
                        .' )  ';
            }
        }
        //сбиваем строки поиска
        if (count($orgFilterStringArray)){//названия
            $orgFilterString=  '('.implode(' and ',$orgFilterStringArray).')';
        }
        if (count($orgFilterNumberArray)){//номера
            $orgFilterNumberString='('.implode(' or ',$orgFilterNumberArray ).')';
        }
        if(!empty($orgFilterNumberString) and !empty($orgFilterString)){//соеденяем
            $orgFilterString=$orgFilterString.' AND '.$orgFilterNumberString;
        }else{
            $orgFilterString=$orgFilterString.$orgFilterNumberString;
        }

        return $orgFilterString;
    }


    public function checkExpertCertificates(){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->columns(array('ordertype_id'));
        $select->from('expert_certificates');
        $select->where(array('expert_id'=>$this->fields['id']));
        $select->order('doc_date DESC');
        $select->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        if($results){
            $results = $results->toArray();
            if($results[0]['ordertype_id'] == '1'){
                return true;
            }
        }
        return false;
    }
}

