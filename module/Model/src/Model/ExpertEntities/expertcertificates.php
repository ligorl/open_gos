<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class expertcertificates extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expert_certificates";
    protected $idName = "id";
}