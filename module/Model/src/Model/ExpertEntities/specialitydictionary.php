<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class specialitydictionary extends Gateway\ExpertsBaseEntity{
    protected $tableName = "speciality_dictionary";
    protected $idName = "id";
}