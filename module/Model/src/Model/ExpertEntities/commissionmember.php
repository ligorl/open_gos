<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class commissionmember extends Gateway\ExpertsBaseEntity{
    protected $tableName = "commission_member";
    protected $idName = "id";
    
    public function getExpert() {
        if($this->isEmptyField('expert_id')){
            return null;            
        }
        $result = $this->getLinkedItem('expert', 
            array(
                'id'=>  $this->getField('expert_id'),
            )
        );
        return $result;
    }

    public function getUser() {
        if($this->isEmptyField('user_id')){
            return null;
        }
        $user = new \Model\Entities\isgaUsers();
        $result = $user->getLinkedItem('ron_Users',
            array(
                'Id'=>  $this->getField('user_id'),
            )
        );
        return $result;
    }

    public function getApplicationProgram() {
        if($this->isEmptyField('expertise_program_id')){
            return null;
        }
        $expertiseProgramId = $this->getField('expertise_program_id');
        $result = new \Model\Entities\isgaApplicationPrograms($expertiseProgramId);
        $resultId = $result->getId();
        if(empty($resultId)){
            return false;
        }
        return $result;
    }

    public function getEducationalOrganization(){
        if($this->isEmptyField('institution_id')){
            return null;
        }
        $linkedTable = new \Model\Gateway\EntitiesTable('eiis_EducationalOrganizations');
        $result = $linkedTable->getEntitiesByWhere(array('Id'=>$this->getField('institution_id')));
        return $result->current();
    }

    public function getExpertiseDate(){
        if($this->isEmptyField('expertise_id')){
            return null;
        }
        $linkedTable = new \Model\Gateway\ExpertsEntitiesTable('expertise');
        $result = $linkedTable->getEntitiesByWhere(array('Id'=>$this->getField('expertise_id')));
        $result = $result->current();
        return date('d.m.Y',strtotime($result->getField('create_timestamp')));
    }

    public function getExpertiseNumber(){
        if($this->isEmptyField('expertise_id')){
            return null;
        }
        $linkedTable = new \Model\Gateway\ExpertsEntitiesTable('expertise');
        $result = $linkedTable->getEntitiesByWhere(array('Id'=>$this->getField('expertise_id')));
        $result = $result->current();
        return $result->getField('accreditation_number');
    }

    /**
     *
     * @param type $idValue
     * @param type $idField
     * @return type
     */
    public function getAllExpertInExpertOrganisation(){
        if  (  $this->isEmptyField('parent_id')
            && $this->isEmptyField('expertise_id')
            && $this->isEmptyField('institution_id')
        ){
            return null;
        }
        $where = array(
            'parent_id' =>$this->getField('parent_id'),
            'expertise_id' =>$this->getField('expertise_id'),
            'institution_id' =>$this->getField('institution_id'),
        );
        $resultSet = $this->tableGateway->select(array($where));
        if(empty($resultSet) ){
            return null;
        }
        $commisin = new \Model\ExpertEntities\commissionmember();
        $result = $commisin ->setFields($resultSet);
        return $result;
    }


    public function isRefused() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $contacting = $this->getLinkedItem('contacting', array(
                'expert_id' => $this->getField('expert_id'),
                'expertise_id' => $this->getField('expertise_id'),
            ));
        if($contacting){
            if ($contacting->getFieldOrSpace('contacting_result') == 'СonsentRefused'){
                return true;
            }else{
                return false;
            }
        }
        return null;
    }

    public function isConfirm() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $contacting = $this->getLinkedItem('contacting', array(
                'expert_id' => $this->getField('expert_id'),
                'expertise_id' => $this->getField('expertise_id'),
            ));
        if($contacting){
            if ($contacting->getFieldOrSpace('contacting_result') == 'СonsentConfirm'){
                return true;
            }else{
                return false;
            }
        }
        return null;
    }
}