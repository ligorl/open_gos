<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class baseeducationprogram extends Gateway\ExpertsBaseEntity{
    protected $tableName = "base_education_program";
    protected $idName = "id";
    
    public function getEducationLevel() {
        if($this->isEmptyField('level_id')){
            return null;
        }
        $result = $this->getLinkedItem('education_level', 
            array('id'=>  $this->getField('level_id'))
        );
        return $result;        
    }
    
    public function getSpecialtyGroup() {
        if($this->isEmptyField('specialty_group_id')){
            return null;
        }
        $result = $this->getLinkedItem('specialty_group', 
            array('id'=>  $this->getField('specialty_group_id'))
        );
        return $result;        
    }
    
}