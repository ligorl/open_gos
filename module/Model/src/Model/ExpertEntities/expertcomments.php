<?php
namespace Model\ExpertEntities;

use Zend\Db\Sql\Sql;
use Model\Gateway;

class expertcomments extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expert_comments";
    protected $idName = "Id";

   
    public function getExpertCommentVersion($expert_id){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('expert_comments');
        $select->where(array('expert_id'=>$expert_id));
        $select->order(array( 'version' => 'DESC' ));
        $select->limit(1);
        $selectString = $sql->getSqlStringForSqlObject($select); //print $selectString;die;
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }
}