<?php
namespace Model\ExpertEntities;

use Model\Gateway;


/**
 * CREATE TABLE IF NOT EXISTS `expertise_document_file_history` (
  `id` varchar(128) NOT NULL,
  `expertise_document_file_id` varchar(128) NOT NULL COMMENT 'ид записи файла',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'дата создания записи',
  `expertise_document_id` varchar(128) NOT NULL COMMENT 'ид зиписи документа',
  `expertise_id` varchar(128) NOT NULL COMMENT 'ид экспертизы',
  `expert_id` varchar(128) NOT NULL COMMENT 'эксперт ид',
  `document_type_id` varchar(128) NOT NULL COMMENT 'тип документа',
  `user_id` varchar(128) NOT NULL COMMENT 'ид усера из рона',
  `action` varchar(128) NOT NULL COMMENT 'create/update/delete',
  `HashName` varchar(512) CHARACTER SET utf8 NOT NULL COMMENT 'где лежит файл'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
 */

class expertisedocumentfilehistory extends Gateway\ExpertsBaseEntity
{
    protected $tableName = "expertise_document_file_history";
    protected $idName = "id";
}
