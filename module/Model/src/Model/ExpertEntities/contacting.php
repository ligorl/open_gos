<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class contacting extends Gateway\ExpertsBaseEntity{
    protected $tableName = "contacting";
    protected $idName = "id";

    static public $resultCode = array(
        '' => array(
            'Name' => 'Эксперт определен',
            'ShortName' => 'Формуляр',
        ),
        'ExpertDefined' => array(
            'Name' => 'Эксперт определен',
            'ShortName' => 'Формуляр',
        ),
        'AgreementSend' => array(
            'Name' => 'Договор отправлен',
            'ShortName' => 'Отправлено',
        ),
        'СonsentConfirm' => array(
            'Name' => 'Согласие получено',
            'ShortName' => 'Получено',
        ),
        //'СonsentDenided' => array(
        'СonsentRefused' => array(//изменил глаза мозолит СonsentDenided/ExpertDefined        
            'Name' => 'Эксперт отказался',
            'ShortName' => 'Отказ',
        ),
        'NowWaiting' => array(
            'Name' => 'Ожидание',
            'ShortName' => 'Ожидание',
        ),        
    );    

    public function getConfirmExperts($expertise_id = false) {
        if (!$expertise_id){
            return null;
        }
        $contacting = $this->getAllByWhere(array(
                'expertise_id' => $expertise_id,
                'contacting_result' =>  array('СonsentConfirm','Согласие'),
            ));
        $experts = array();
        if($contacting){
            foreach($contacting as $cont){
                $experts[$cont->getField('expert_id')] = $cont->getField('expert_id');
            }
        }
        return $experts;
    }
    
    
    public function getExpert(){
        if( $this->isEmptyField( 'expert_id' ) ){
            return null;
        }
        $expertClass = new expert( $this->getFieldSafe( 'expert_id' ) );
        if( $expertClass->isEmpty() ){
            return false;
        }
        return $expertClass;
    }


    public function getExpertise(){
        if( $this->isEmptyField( 'expertise_id' ) ){
            return null;
        }
        $expertiseClass = new expertise( $this->getFieldSafe( 'expertise_id' ) );
        if( $expertiseClass->isEmpty() ){
            return false;
        }
        return $expertiseClass;
    }
    
    public function getOrganisation(){
        $expertId = $this->getExpert();
        if( empty( $expertId ) ){
            return null;
        }
        $expertId = $expertId->getId();
        
        $expertiseId = $this->getExpertise();
        if( empty( $expertiseId ) ){
            return null;
        }        
        $expertiseId = $expertiseId->getId();
        
                    
        $member = new commissionmember();
        $member = $member->getByWhere( array( 'expert_id' => $expertId , 'expertise_id' => $expertiseId ) );
        if( empty($member) ){
            $member = new chairman();
            $member = $member->getByWhere( array( 'expert_id' => $expertId , 'expertise_id' => $expertiseId ) );
        }
        if( empty($member) ){
            return null;
        }
        
        
        if( $this->isEmptyField( 'expertise_id' ) ){
            return null;
        }
        $orgClass = new \Model\Entities\eiisEducationalOrganizations( $member->getFieldSafe( 'institution_id' ) );
        if( $orgClass->isEmpty() ){
            return false;
        }
        return $orgClass;
    }
     
    
    public function getProgrammAll(){
        $result = array();
            if( $this->isEmptyField( 'expert_id' ) || $this->isEmptyField( 'expertise_id' ) ){            
            }
            else{
                $comssion = new commissionmember();
                $comssionAll = $comssion->getAllByWhereParseId( 
                    array(
                        'expert_id' => $this->getFieldSafe( 'expert_id' ),
                        'expertise_id' => $this->getFieldSafe( 'expertise_id' ),
                    ),
                    'expertise_program_id'
                    );
                if( empty($comssionAll) ){

                }
                else{
                    $programIdAll = array_keys( $comssionAll );
                    $program =new \Model\Entities\isgaApplicationPrograms();                
                    $result = $program->getAllByWhereParseId( array( $program->getIdName() => $programIdAll ) );                
                }
            }
        return $result;
    }
    
    public function getChairMain(){
        $result = array();
            if( $this->isEmptyField( 'expert_id' ) || $this->isEmptyField( 'expertise_id' ) ){            
            }
            else{
                $comssion = new chairman();
                $result = $comssion->getByWhere( 
                    array(
                        'expert_id' => $this->getFieldSafe( 'expert_id' ),
                        'expertise_id' => $this->getFieldSafe( 'expertise_id' ),
                    )                    
                );
            }
        return $result;
    }
    

}