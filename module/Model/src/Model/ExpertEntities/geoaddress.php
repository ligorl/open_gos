<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class GeoAddress extends Gateway\ExpertsBaseEntity{
    protected $tableName = "geo_address";
    protected $idName = "id";
    
    public function getCountry(){
        if(!isset($this->fields["country_id"]) || $this->fields["country_id"] == "")
            return null;
        $result = $this->getLinkedItem("country", array(
            "id" => $this->fields["country_id"]
        ));
        if ($result) {
            return $result->getField('name');
        } else {
            return '';
        }
    }
    
    public function getRegion(){
        if(!isset($this->fields["region_id"]) || $this->fields["region_id"] == "")
            return null;
        
        $result = $this->getLinkedItem("region", array(
            "id" => $this->fields["region_id"]
        ));
        if ($result) {
            return $result->getField('name');
        } else {
            return '';
        }
    }
   
}