<?php
namespace Model\ExpertEntities;

use Zend\Db\Sql\Sql;
use Model\Gateway;

class expertdictionary extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expert_dictionary";
    protected $idName = "id";
    
    public function getExpertTable(){
        $result = $this->getLinkedItem("expert",array(
            "id" => $this->fields["id"]
        ));
        return $result;
    }
    public function getExpertProgram(){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('expert_edu_programms');
        $select->where(array('expert_id'=>$this->fields['id']));
        $select->order(array('education_level_id'=>'ASC','speciality_group_id'=>'ASC'));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }
    public function getExpertCertificates(){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('expert_certificates');
        $select->where(array('expert_id'=>$this->fields['id']));
        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }
   
}