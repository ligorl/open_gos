<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class specialtygroup extends Gateway\ExpertsBaseEntity{
    protected $tableName = "specialty_group";
    protected $idName = "id";
}