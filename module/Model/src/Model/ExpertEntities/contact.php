<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class contact extends Gateway\ExpertsBaseEntity{
    protected $tableName = "contact";
    protected $idName = "id";
        
    public function getFieldSpace($field){
        if($this->isEmptyField($field)){
            return '';
        }
        $this->getField($field);
    }    
}