<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class expertisedocumentfile extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expertise_document_file";
    protected $idName = "id";

    /**
     * перегружаем сохраниение, добавляем запись в историю
     * 
     * @param type $isNew
     * @param type $doNotSaveToExchangeTable
     */
    public function save($isNew = false, $doNotSaveToExchangeTable = false){
        $saveType = "create-open";
        if( isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $saveType = "update-open";
        }
        $return = parent::save( $isNew , $doNotSaveToExchangeTable );
        if( !$doNotSaveToExchangeTable ){
            $this-> addHistory( $saveType );                
        }
        return $return;
    }
    
    //public function delete( $doNotSaveToExchangeTable = false ){
    //    $this-> addHistory( 'delete' );       
    //    parent::delete( $doNotSaveToExchangeTable );
    //}
    
    /**
     * удаляем запись, перегружено для добавления истории
     * 
     * @param type $where
     * @param type $doNotSaveToExchangeTable
     */
    public function deleteWhere($where, $doNotSaveToExchangeTable = false){
        if( !$doNotSaveToExchangeTable ){
            $this-> addHistory( 'delete-open' );  
        }
        return parent::deleteWhere($where , $doNotSaveToExchangeTable);
    }    

    /**
     * Закиним в историю изменеия с файлом
     * 
     * @param type $saveType - тип операции
     * @throws \Exception
     */
    public function addHistory( $saveType = "create-open" ){
        //костыль  дергаем авторизацию
        //криво без сервиса
        // выдрано  \Auth\Storage\AuthStorage
        //          \Auth\Module
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $dbTableAuthAdapter = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter( $dbAdapter, 'ron_Users', 'Login', 'Password' );
        $authService = new \Auth\Storage\AuthService();;
        $authService->setAdapter( $dbTableAuthAdapter );
        $authService->setStorage( new \Auth\Storage\AuthStorage( 'Auth' ) );
        $user = $authService->getIdentity();
        if( empty( $user ) ){
            throw new \Exception( 'Пользовательне не аутентифицирован' );
        }
        $userId = $user->getId();
        $expertiseId = '';
        $expertId = '';
        $docTypeId = '';
        $expertise_program_id = '';
        $expertiseDocumentClass = $this->getExpertiseDocument();
        if( !empty($expertiseDocumentClass) ){
            $expertiseId            =   $expertiseDocumentClass->getFieldSafe('expertise_id');
            $expertId               =   $expertiseDocumentClass->getFieldSafe('expert_id');
            $docTypeId              =   $expertiseDocumentClass->getFieldSafe('document_type_id');   
            $expertise_program_id   =   $expertiseDocumentClass->getFieldSafe('expertise_program_id');
        }

        $history = new expertisedocumentfilehistory();
        $history->setField( 'expertise_document_file_id' ,  $this->getId() );
        $history->setField( 'expertise_document_id' ,       $this->getFieldSafe('expertise_document_id') );
        $history->setField( 'expertise_id' ,                $expertiseId );
        $history->setField( 'expert_id' ,                   $expertId );
        $history->setField( 'document_type_id' ,            $docTypeId);
        $history->setField( 'expertise_program_id' ,        $expertise_program_id );
        $history->setField( 'user_id' ,                     $userId );
        $history->setField( 'action' ,                      $saveType );
        $history->setField( 'HashName' ,                    $this->getFieldSafe('HashName') );
        $history->save( true );
    }

    public function getExpertiseDocument() {
        if( $this->isEmptyField( 'expertise_document_id' ) ){
            return null;
        }
        $result =  new expertisedocument();
        return  $result->getByWhere( array($result->getIdName() => $this->getFieldSafe( 'expertise_document_id' ) ) );
    }    
    
    public function getByExpertiseDocumentIds($param = null) {
        if (empty($param)){
            return null;
        }
        $result =  $this->getLinkedItems($this->tableName,array('expertise_document_id'=>$param) );
        return $result;
    }
       
    /**
     * Денгаем хеш нэйм, если нет, пробуем глаянуть через связную таблицу, есл итаковая есть
     * 
     * @return type
     */
    public function getHashName(){
        $hashName = $this->getFieldSafe( 'HashName' );
        if( empty($hashName) ){
            $linkId = $this->getFieldSafe( 'LinkId' );
            $linkName = $this->getFieldSafe( 'LinkClass' );
            $linkField = $this->getFieldSafe( 'LinkField' );
            if( !empty($linkId) && !empty($linkName) && !empty($linkField) ){
                if( 'options' == strtolower($linkName) ){
                    $Options = new options($linkId);
                    if( !$Options->isEmpty() ){
                       $hashName = $Options->getFieldSafe('value');
                    }
                }
            }
        }
        return $hashName;
    }
    
    
}
