<?php
namespace Model\ExpertEntities;

use Zend\Db\Sql\Sql;
use Model\Gateway;

class experteduprogramms extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expert_edu_programms";
    protected $idName = "id";
    
    public function getNameProgram($id){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->columns(array('name','code'));
        $select->from('speciality_dictionary');
        $select->where(array('id'=>$id));
        $selectString = $sql->getSqlStringForSqlObject($select);//print $selectString;die;
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    } 
   
}