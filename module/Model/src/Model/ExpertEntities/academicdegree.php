<?php
namespace Model\ExpertEntities;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Model\Gateway;

class academicDegree extends Gateway\ExpertsBaseEntity{
    protected $tableName = "academic_degree";
    protected $idName = "id";

    public function getArr(){
        $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('academic_degree');
        $selectString = $sql->getSqlStringForSqlObject($select); //print $selectString;die;
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        $result = array();
        foreach($results as $v){
            $result[$v->id] = $v->name;
        }
        return $result;
    }

  
}