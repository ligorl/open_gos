<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class expertise extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expertise";
    protected $idName = "id";

    public function getDoc() {
        if ($this->fields["primary_notice_doc_id"]) {
            $result = $this->getLinkedItem("expertise_document", array(
                "id" => $this->fields["primary_notice_doc_id"]
            ));
            if ($result) {
                return $result;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function getApplication() {
        if($this->isEmptyField('applications_id')){
            return null;
        }
        $application = new \Model\Entities\isgaApplications();

        $result = $application->getLinkedItem(
            'isga_Applications',
            array('Id'=>$this->getField('applications_id'))
        );
        return $result;
    }

    /**
     * Получаем таблице членов комисии
     * @return type
     */
    public function getCommissionMember($whereAdd = array()) {
        if ($this->isEmptyField($this->idName)){
            return null;
        }
        $where = array( 'expertise_id' => $this->getId() );
        if (!empty($whereAdd)){
            if(is_array($whereAdd)){
                $where = array_merge($where,$whereAdd);
            }
            if(is_string($whereAdd)){
                $where[]= $whereAdd;
            }
        }
        $commision = $this->getLinkedItems('commission_member', $where);
        return $commision;
    }

    /**
     * Возращает членов комиссии без отказников
     *
     * @param type $whereAdd
     * @return type
     */
    public function getCommissionMemberWithoutRefused($whereAdd = array()) {
        $commision = $this->getCommissionMember($whereAdd);
        $commision = $commision->getObjectsArray();
        if(empty($commision)){
            return $commision;
        }
        $commisionNew = array();
        foreach ( $commision as  $value ) {
            if($value->isRefused()){
                continue;
            }
            $commisionNew[$value->getId()] = $value;
        }
        return $commisionNew;
    }

    /**
     * получаем руководителей по экспертизе
     * @return type
     */
    public function getChairman($whereAdd = array()) {
        if ($this->isEmptyField($this->idName)){
            return null;
        }
        $where = array( 'expertise_id' => $this->getId() );
        if (!empty($whereAdd)){
            if(is_array($whereAdd)){
                $where = array_merge($where,$whereAdd);
            }
            if(is_string($whereAdd)){
                $where[]= $whereAdd;
            }
        }
        $commision = $this->getLinkedItems('chairman', $where);
        return $commision;
    }

    /**
     * Получаем резульаты контактов, согласия
     * @return type
     */
    public function getContacting() {
        if ($this->isEmptyField($this->idName)){
            return null;
        }
        $commision = $this->getLinkedItems('contacting', array(
                'expertise_id' => $this->getId(),
            ));
        return $commision;
    }

    public function getStatus() {
        if ($this->fields["external_status_type_id"]) {
            $result = $this->getLinkedItem("expertise_external_status", array(
                    "id" => $this->fields["external_status_type_id"]
                ));
            if ($result) {
                return $result;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public function getByApplicationId($applicationId) {
        if (empty($applicationId)){
            return null;
        }
        $result = $this->getLinkedItem($this->tableName, array('applications_id'=>$applicationId));
        return $result;
    }

    public function getEducationalOrganization(){
        if( $this->isEmptyField('organization_id') ){
            return null;
        }        
        $linkedTable = new \Model\Entities\eiisEducationalOrganizations( $this->getField('organization_id') );
        if( $linkedTable->isEmpty() ){
            return false;
        }        
        return $linkedTable;
    }

    public function getExpertisePrograms() {
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $result = $this->getLinkedItems('expertise_program', array('expertise_id'=>  $this->getId()));
        return $result;
    }

    /**
     * Формирует строку раздела WHERE из строки для фильтрации
     * 
     * @param type $filter - строка фильтра
     * @return string - полученая строка запроса
     */
    public function makeWhereStringFromSearchString($filter) {
        if (empty($filter)){
            return ' 1 ';
        }
        $filter = strip_tags($filter);
        $filter = \addslashes($filter);
        //получаем ид вузов
        $orgsIds = $this->findEductionOrganizationIds($filter);
        $org_arr= empty($orgsIds)?array():$orgsIds;        

        $str_org='';
        if (count($org_arr) > 0) {//сбиваем найдение организации
            $list_org = '"' . implode('", "', $org_arr) . '"';
            $str_org = 'expertise.organization_id IN (' . $list_org . ') ';
        }
        //если есть в фильтре цивры, то это номер экспертизы
        $orgFilterNumberString = $this->mekeSqlNumber($filter);
        //соеденяем оба запроса
        if($str_org!==''&& $orgFilterNumberString!==''){
            $str_org=$str_org. ' or '.$orgFilterNumberString;
        }else{
            $str_org=$str_org.$orgFilterNumberString;
        }
        if(empty($str_org)){
            return '0'; //если нечего искать, то ничего и выбирать
        }
        $str_org='('.$str_org.')';
        $where = $str_org;
        return $where;
    }

    /**
     * Ищем образовательные организации по строке фильтра
     * 
     * @param type $filter - строка вильтра поиска
     * @return type - массив ид вузов
     */
    public function findEductionOrganizationIds($filter) {
        if (empty($filter)){
            return array();
        }
        $orgFilterString='';
        $orgFilterNumberString='';
        $orgFilterNumberArray=array();
        $orgFilterStringArray=array();
        $orgFilterArray=explode(' ',$filter);
        if(!(count($orgFilterArray))){
            $orgFilterArray=array($filter);
        }
        foreach ( $orgFilterArray as $orgFilterItem) {
            //только номера
            if(preg_match('/^[0-9]+$/', $orgFilterItem)){
                $filterOnlyNumberArray[]=$orgFilterItem;
                $orgFilterNumberArray[]=' ( GosRegNum = "' . $orgFilterItem . '" '
                  .' OR Inn = "' . $orgFilterItem . '" )  ';
            }else{
                //формируем перечень названий в вузе
                $orgFilterItem=  mb_strtoupper($orgFilterItem, 'utf8');
                $orgFilterStringArray[] =' upper(FullName) LIKE "%' . $orgFilterItem . '%" ' ;
            }
        }
        //сбиваем строки поиска
        if (count($orgFilterStringArray)){//названия
            $orgFilterString=  '('.implode(' and ',$orgFilterStringArray).')';
        }
        if (count($orgFilterNumberArray)){//номера
            $orgFilterNumberString='('.implode(' or ',$orgFilterNumberArray ).')';
        }
        if(!empty($orgFilterNumberString) and !empty($orgFilterString)){//соеденяем
            $orgFilterString=$orgFilterString.' or '.$orgFilterNumberString;
        }else{
            $orgFilterString=$orgFilterString.$orgFilterNumberString;
        }
        //находим нужные вузы
        $eduOrg = new \Model\Entities\eiisEducationalOrganizations();
        $org_arr = $eduOrg->selectComplicated(
                "eiis_EducationalOrganizations", array('Id'),
                $orgFilterString
            )->toArray();
        $org_arr = array_column($org_arr,'Id');
        return $org_arr;
    }

    public function mekeSqlNumber($filter) {
        if (empty($filter)){
            return '';
        }
        $orgFilterNumberArray = array();
        $orgFilterNumberString = '';
        $orgFilterArray=explode(' ',$filter);
        if(!(count($orgFilterArray))){
            $orgFilterArray=array($filter);
        }
        foreach ( $orgFilterArray as $orgFilterItem) {
            //только номера
            if(preg_match('/^[0-9]+$/', $orgFilterItem)){
                // $filterOnlyNumberArray[]=$orgFilterItem;
                $orgFilterNumberArray[] =' ( accreditation_number LIKE "%' . $orgFilterItem . '%"  )  ';
            }else{
                //формируем перечень названий в вузе
                //$orgFilterItem=  mb_strtoupper($orgFilterItem, 'utf8');
                //$orgFilterStringArray[] =' upper(FullName) LIKE "%' . $orgFilterItem . '%" ' ;
            }
        }
        if (count($orgFilterNumberArray)){//номера
            $orgFilterNumberString='('.implode(' or ',$orgFilterNumberArray ).')';
            //$appFilterBarCodeString='('.implode(' or ',$appFilterBarCodeArray ).')';
        }
        return $orgFilterNumberString;
    }

    /**
     * выбираем экспертизы
     * 
     * @param type $where - строка чего отбирать
     * @param type $cols - массив колонок
     * @param type $max_count_page - максимум записей на страничке
     * @param type $page - какая страничка
     * @param type $ord - по чем сортировать
     * @return type - массив с найдеными характеристиками
     */
    public function getSearchExpertise($where, $cols, $max_count_page = null, $page = null, $ord = 'accreditation_number')
    {
        $expertises =  $this->selectComplicated(
                'expertise',
                $cols,
                $where,
                NULL,
                $ord,
                $max_count_page,
                $page
        )->toArray();    
        return $expertises;
    }
    
    /**
     * Получаем количество записей по запросу
     * 
     * @param type $where
     * @return type
     */
    public function getCountByWhere($where)
    {
        $count  = $this->getSearchExpertise($where, array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));
        $count_items = $count[0]['count'];
        return $count_items;
    }

    public function getExpertiseDocument($param=array()) {
        if ($this->isEmptyField($this->idName)){
            return null;
        }
        $param = array_merge(array('expertise_id' => $this->getId()),$param);
        $commision = $this->getLinkedItems('expertise_document',$param );
        return $commision;
    }
    
    /**
     * устанавливаем новый статус экспертизе
     * 
     * @param type $newStatusId - ид статуса экспертизы
     * @return type
     */
    public function setNewStatus($newStatusId) {
        if(empty($newStatusId)){
            return null;
        }
        if($this->isEmptyField($this->idName)){
            return null;
        }
        $this->setField('external_status_type_id', $newStatusId);
        $this->save(false);
    }

    
}