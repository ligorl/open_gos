<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class expertisedocument extends Gateway\ExpertsBaseEntity{
    protected $tableName = "expertise_document";
    protected $idName = "id";

   public function getDocNumb(){
        $result = $this->getLinkedItem("expertise_document", array(
            "id" => $this->fields["document_number"]
        ));
        if ($result) {
            return $result;
        } else {
            return '';
        }
    }

    public function getDateAlert() {
        $result = $this->getLinkedItem("document_blank", array(
                "id" => $this->fields["document_blank_id"]
            ));
        if ($result) {
            return $result;
        } else {
            return '';
        }
    }
    
     /**
     * получение персональных данных
     * @return Object
     */
    public function getExpert() {
         if(!isset($this->fields["expert_id"]) || $this->fields["expert_id"] == "")
            return null;
     
        $result = $this->getLinkedItem("expert", array(
            "id" => $this->fields["expert_id"]
        ));
        return $result;
    }

    public function getExpertise() {
        $result = $this->getLinkedItem("expertise", array(
                "id" => $this->fields["expertise_id"]
            ));
        return $result;
    }
     
  
    public function get_organization() {
         if(!isset($this->fields["expertise_id"]) || $this->fields["expertise_id"] == "")
            return null;
     
        $result = $this->getLinkedItem("expertise", array(
            "id" => $this->fields["expertise_id"]
        ));
        return $result;
    }
    
    /** 
     * получаем эксперта этого договора
     * @return type
     */
    public function getExpertIt() {
        if(!isset($this->fields["expert_id"]) || $this->fields["expert_id"] == "")
            return null;     
        $result = $this->getLinkedItem("expert", array(
            "id" => $this->fields["expert_id"]
        ));
        return $result;
    }
    
    /**
     * возращает запись члена комисии
     * @return type - запись или false если записи нет, null если бита база
     */
    public function getChairman() {
        if(empty($this->fields["expertise_id"])|| empty($this->fields["expert_id"])){
            return null;
        }
        $result = $this->getLinkedItem("chairman", array(
            "expertise_id" => $this->fields["expertise_id"],
            "expert_id"    => $this->fields["expert_id"],            
        ));
        return $result;        
    }
    
    /** 
     * возращает запись члена комисии
     * @return type запись или false если записи нет, null если бита база
     */
    public function getCommissionMember() {
        if(empty($this->fields["expertise_id"])|| empty($this->fields["expert_id"])){
            return null;
        }
        $result = $this->getLinkedItem("commission_member", array(
            "expertise_id" => $this->fields["expertise_id"],
            "expert_id"    => $this->fields["expert_id"],            
        ));
        return $result;        
    }    
    
    public function getDocumentFile() {
        if($this->isEmptyField($this->idName)){
            return null;
        }            
        $result = $this->getLinkedItem("expertise_document_file", array(
            'expertise_document_id'=>$this->getField($this->idName),
        ));
        return $result;    
    }
    
    protected $_documentType=null;    
    public function getDocumentType() {        
        if($this->_documentType ===null){
            if($this->isEmptyField('document_type_id')){
                return null;
            }            
            $result = $this->getLinkedItem("document_type", array(
                'id'=>$this->getField('document_type_id'),
            ));
            $this->_documentType = $result;
        } 
        return $this->_documentType;        
    }    
}