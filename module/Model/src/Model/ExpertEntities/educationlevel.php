<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class educationlevel extends Gateway\ExpertsBaseEntity{
    protected $tableName = "education_level";
    protected $idName = "id";
}