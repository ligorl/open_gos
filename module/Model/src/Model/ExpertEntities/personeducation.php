<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class personeducation extends Gateway\ExpertsBaseEntity{
    protected $tableName = "person_education";
    protected $idName = "id";

   public function getEducationalOrganization(){
        if($this->isEmptyField('organization_id')){
            return null;
        }
        $orgs = new \Model\Entities\eiisEducationalOrganizations();
        $orgs = $orgs->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $this->getField('organization_id')));
        return $orgs ;
   }
}