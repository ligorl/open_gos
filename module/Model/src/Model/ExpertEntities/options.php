<?php
namespace Model\ExpertEntities;

use Model\Gateway;

class options extends Gateway\ExpertsBaseEntity{
    protected $tableName = "options";
    protected $idName = "id";

    static protected $identy = null;

    static public function getIdenty() {
        if(self::$identy===null){
            self::$identy = new \Model\ExpertEntities\options();
        }
        return self::$identy;
    }

    static public function getOption($name){
        $identy = self::getIdenty();
        $result = $identy->getEntityByWhere(array('name'=>$name));
        return isset($result['value'])?$result['value']:false;
    }

    static public function setOption($name,$value){
        $identy = self::getIdenty();
        $result = $identy->tableGateway->update(
                array('value'=>$value),
                array('name',$name)
                );
        return $result;
    }
}