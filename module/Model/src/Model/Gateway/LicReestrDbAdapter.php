<?php

namespace Model\Gateway;

use PDO;

class LicReestrDbAdapter
{
    protected static $_instance = null;

    private function __construct(){

    }

    private function __clone(){
    }

    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            $config = new \Zend\Config\Config(include (__DIR__.'../../../../../../config/autoload/global.php'));
            $dbAdapter = new \Zend\Db\Adapter\Adapter(array(
                'driver' => $config->db->adapters->lic_reestr->driver,
                'dsn' => $config->db->adapters->lic_reestr->dsn,
                'username' => $config->db->adapters->lic_reestr->username,
                'password' => $config->db->adapters->lic_reestr->password,
                'charset'        => 'utf8',
                'driver_options' => array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                )
            ));

            self::$_instance = $dbAdapter;
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }
}