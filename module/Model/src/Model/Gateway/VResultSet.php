<?php

namespace Model\Gateway;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;

class VResultSet extends ResultSet{
    protected $_objectsArray = array();
    protected $_count = null;
    protected $_objectsArrayProcessed = false;

    protected function _prepareObjectsArray(){
        if(!$this->_objectsArrayProcessed){
            foreach($this as $result){
                $this->_objectsArray[] = $result;
                $this->_count++;
            }
            $this->_objectsArrayProcessed = true;
        }
    }

    public function count(){
        $this->_prepareObjectsArray();
        return $this->_count;
    }

    public function getObjectsArray(){
        $this->_prepareObjectsArray();
        return $this->_objectsArray;
    }
}