<?php

namespace Model\Gateway;

class WhereConditionGeneratorEquivalent
{

    protected $defaultEquivalentTable = [
        'like'    => [
            'mysql' => [
                'value'      => 'LIKE',
                'isFunction' => false,
            ]
        ],
        'rlike'   => [
            'mysql' => [
                'value'      => 'RLIKE',
                'isFunction' => true,
            ]
        ],
        'in'      => [
            'mysql' => [
                'value'      => 'IN',
                'isFunction' => true,
            ]
        ],
        'between' => [
            'mysql' => [
                'value'      => 'BETWEEN ',
                'isFunction' => false,
            ]
        ],
        'concat'  => [
            'mysql' => [
                'value'      => 'CONCAT',
                'isFunction' => true,
            ]
        ],
        'is'      => [
            'mysql' => [
                'value'      => 'IS',
                'isFunction' => false,
            ]
        ],
        'not'     => [
            'mysql' => [
                'value'      => 'NOT',
                'isFunction' => false,
            ]
        ],
        'null'    => [
            'mysql' => [
                'value'      => 'NULL',
                'isFunction' => false,
            ]
        ],
        'or'    => [
            'mysql' => [
                'value'      => 'OR',
                'isFunction' => false,
            ]
        ],
        'and'    => [
            'mysql' => [
                'value'      => 'AND',
                'isFunction' => false,
            ]
        ],
        'lower'    => [
            'mysql' => [
                'value'      => 'LOWER',
                'isFunction' => true,
            ]
        ],
        'column'    => [
            'mysql' => [
                'value'      => '',
                'isFunction' => false,
            ]
        ],
        'limit'    => [
            'mysql' => [
                'value'      => 'LIMIT',
                'isFunction' => false,
            ]
        ],
        'offset'    => [
            'mysql' => [
                'value'      => 'OFFSET',
                'isFunction' => false,
            ]
        ],
        'order by'    => [
            'mysql' => [
                'value'      => 'ORDER BY',
                'isFunction' => false,
            ]
        ],
        '('    => [
            'mysql' => [
                'value'      => '(',
                'isFunction' => false,
            ]
        ],
        ')'    => [
            'mysql' => [
                'value'      => ')',
                'isFunction' => false,
            ]
        ],
        'group by'    => [
            'mysql' => [
                'value'      => 'GROUP BY',
                'isFunction' => false,
            ]
        ],
        'having'    => [
            'mysql' => [
                'value'      => 'HAVING',
                'isFunction' => false,
            ]
        ],
        'max'    => [
            'mysql' => [
                'value'      => 'MAX',
                'isFunction' => true,
            ]
        ],
        'concat'    => [
            'mysql' => [
                'value'      => 'CONCAT',
                'isFunction' => true,
            ]
        ],

    ];

    public function getEquivalentTable(){
        return $this->defaultEquivalentTable;
    }

    public function setEquivalentTable( $table ){
        return $this->defaultEquivalentTable = $table;
    }

}