<?php

namespace Model\Gateway;

use Zend\Db\TableGateway\TableGateway;

class LicReestrEntitiesTable extends EntitiesTable{

    protected $tableGateway = null;

    /**
     * Конструктор
     * @param $tableName - имя таблицы с которой будет работать шлюз
     */
    public function __construct($tableName){
        // Consumes the configuration array
        $dbAdapter = LicReestrDbAdapter::getInstance();
        //Устанавливаем в виде набора объектов какого класса будет возвращаться ответ
        $resultSetPrototype = new VResultSet();

        $classNameParts = explode("_",$tableName);
        $className = implode("",$classNameParts);

        $entityClassName = "Model\\LicReestrEntities\\".$className;
        $resultSetPrototype->setArrayObjectPrototype(new $entityClassName());
        //Инициируем шлюз для работы с бд
        $this->tableGateway = new TableGateway($tableName, $dbAdapter, null, $resultSetPrototype);
    }

}