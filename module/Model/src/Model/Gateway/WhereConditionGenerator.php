<?php

namespace Model\Gateway;
/*
 * бета версия строителя условий для where
 */
class WhereConditionGenerator{

    protected
            $defaultEquivalentArr,
            $conditionDefaultFormatArr = [],
            $baseType;

    public function __construct()
    {
        $equivalentTableObj = new WhereConditionGeneratorEquivalent();
        $this->setEquivalentArr( $equivalentTableObj->getEquivalentTable());
        $this->setBaseType('mysql');
    }
    /*
     * добавляем массив условий
     * для исбежания перезатираний условия отдаються в качестве конструкции
     * @param
     * $data =
     * //глобальный уровень только массивы условий
     * [
     *  //массивы условий в формате
     *      'операция или функция' => [
     *               список параметров массивом],
     *      'операция2 или функция2' => параметр строкой,
     *      'операция3 или функция3 без списка'
     *  ],
     *  [
     *      'используем квадратные скобки для экранирования повторяющиеся имя операции и все по той же схеме'
     *  ]
     */
    public function add(array $data){
        foreach ($data as $key => $arrData){
            $this->insideAdd($arrData);
        }
    }

    /*
     * распаковать внутренний массив
     */
    protected function insideAdd( $data){
        switch ( gettype($data)){
            case 'string':
                $this->checkAndAddTo_conditionalDefaultFormatArr( strtolower($data));
            break;
            case 'array':
                foreach ( $data as $key=>$value){
                
                    if( empty($key) && !empty($value)){
                        $this->checkAndAddTo_conditionalDefaultFormatArr( strtolower($value));
                    }else{
                        $this->checkAndAddTo_conditionalDefaultFormatArr( strtolower($key), $value);
                    }
                }
            break;
            default :
                new \Exception('неожиданный тип данных!<br>'.\Zend\Debug\Debug::dump($data));
            break;
        }
    }

    /*
     * проверяет является ли элемент известным оператором
     */
    protected function isOperator( $operator){
    //\Zend\Debug\Debug::dump($operator);
        $equvalientArr = $this->getEquivalentArr();
        if( !empty($equvalientArr[$operator]) ){
            return true;
        }else{
            return false;
        }
    }

    /*
     * получить массив эквивалентов
     */
    public function getEquivalentArr(){
         return $this->defaultEquivalentArr;
    }
    /*
     * установить массив эквивалентов
     */
    public function setEquivalentArr($equivalentArr){
        return $this->defaultEquivalentArr = $equivalentArr;
    }
    /*
     * добавить к массиву условий по умолчанию
     */
    protected function addToConditionalDefaultFormatArr( $key, $value){
        $conditionalArr = $this->conditionDefaultFormatArr;
        $level = count($conditionalArr);
        $conditionalArr[$level] = [];
        $count = count( $conditionalArr[$level]);
        $conditionalArr[$level][$count]['key'] = $key;
        $conditionalArr[$level][$count]['value'] = $value;
        $this->conditionDefaultFormatArr = $conditionalArr;
    }
    /*
     * проверить, и добавить к массиву условий по умаолчанию
     */
    protected function checkAndAddTo_conditionalDefaultFormatArr( $key, $value = ''){
        if( $this->isOperator($key) ){
            $this->addToConditionalDefaultFormatArr( $key, $value);
        }else{
            throw new \Exception('Команда не существует, или не задано соответствие в таблице("'.$key.'","'.$value.'")');
        }
    }
    /*
     * возвращает предикат
     */
    public function getPredicate(){
        $strVal = $this->conditionToString();
        $expression = new \Zend\Db\Sql\Predicate\Expression($strVal);
        return $expression;
    }

    /*
     * возвращает массив услови по умолчанию
     */
    public function getConditionalDefaultFormatArr(){
        return $this->conditionDefaultFormatArr;
    }
    /*
     * преобразует все условия в строку
     */
    public function conditionToString(){
        $retString = '';
        $conditionalFullArr = $this->getConditionalDefaultFormatArr();
        foreach ( $conditionalFullArr as $key=>$conditionalArr){
            $retString.=$this->insideConditionToString($conditionalArr);
        }
        return $retString;
    }

    /*
     * преобразует конкретный массив условий
     */
    protected function insideConditionToString($conditionalArr){
        $baseType = $this->getBaseType();
        $equalArr = $this->getEquivalentArr();
        $conditionString = '';
        
        foreach ( $conditionalArr as $num=>$data){
            $conditionString .= ' ';
            $key = $data['key'];
            $value = $data['value'];
            $isFunction = $equalArr[$key][$baseType]['isFunction'];
            $operator = $equalArr[$key][$baseType]['value'];
            if($isFunction){
                 $conditionString .= $this->toFunctionString( $operator, $value);
            }else{
                 $conditionString .= $this->toOperatorString( $operator, $value);
            }
        }
        return  $conditionString;
    }
    /*
     * Возвращает тип БД
     */
    public function setBaseType($baseType){
        return $this->baseType = $baseType;
    }
    /*
     * Устанавливает тип БД
     */
    public function getBaseType(){
        return $this->baseType;
    }
    /*
     * преобразует в строку вида mysql функции
     */
    protected function toFunctionString( $operator, $data){
        return $this->toStringAll( $operator, $data);
    }
    /*
     * преобразует в строку вида mysql оператора
     */
    protected function toOperatorString( $operator, $data){
        return $this->toStringAll( $operator, $data, $splitter=' ', $pre=' ', $post='');
    }

    /*
     * преобразует по параметрам
     */
    protected function toStringAll(
      $operator,
      $data,
      $splitter=',',
      $preOperator='(',
      $postOperator=')'
    ){
        $returnStr = $operator.$preOperator;
        switch ( gettype($data)){
            case 'string':
               $returnStr .= $data;
            break;
            case 'array':
                $count = 0;
                foreach ( $data as $key=>$value){
                    if( $count>0 ){
                        $returnStr .= $splitter;
                    }
                    $returnStr .= $value;
                    $count++;
                }
            break;
             case 'integer':
               $returnStr .= $data;
            break;
            default :
                new \Exception('неожиданный тип данных!<br>'.\Zend\Debug\Debug::dump($data));
            break;
        }

        return $returnStr.$postOperator;
    }

    
}
