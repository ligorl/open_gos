<?php

namespace Model\Gateway;

class MSSQLDbAdapter
{
    protected static $_instance = null;

    private function __construct(){

    }

    private function __clone(){
    }

    public static function getInstance() {
        // ��������� ������������ ����������
        if (null === self::$_instance) {
            // ������� ����� ���������
            $config = new \Zend\Config\Config(include 'config\autoload\global.php');
            $dbAdapter = new \Zend\Db\Adapter\Adapter(array(
                'driver' => $config->db_old->driver,
                'dsn' => $config->db_old->dsn,
                'username' => $config->db_old->username,
                'password' => $config->db_old->password
            ));

            self::$_instance = $dbAdapter;
        }
        // ���������� ��������� ��� ������������ ���������
        return self::$_instance;
    }
}