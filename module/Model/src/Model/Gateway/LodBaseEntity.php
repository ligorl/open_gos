<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Model\Gateway;
//МЕТОДЫ ОБЩЕГО ХАРАКТЕРА ДОБАВЛЯЕМ В inheritanceBaseEntity СЮДА ТОЛЬКО КОНКРЕТНО 'ЗАТОЧЕННЫЕ' ПОД `f11_mon_lod`
/**
 * Description of LodBaseEntity
 *
 * @author Руслан
 */
class LodBaseEntity extends inheritanceBaseEntity
{
     public function __construct( $id = null){
         // Consumes the configuration array
        $dbAdapter = LodDbAdapter::getInstance();

        parent::__construct( $dbAdapter, $this->getTableName());

        //реинициализируем table gateway  для получения результатов как массива объектов
        $this->tableGatewayVResultSetInit();

        if( !empty($id)){
            $this->getById($id);
        }

        //настраиваем лог
        // Consumes the configuration array
        $dbAdapterForLog = DbAdapter::getInstance();
        //Устанавливаем в виде набора объектов какого класса будет возвращаться ответ
        $resultSetPrototype = new VResultSet();
        $resultSetPrototype->setArrayObjectPrototype( new \Model\Entities\isgaExchangeTable());
        //Инициируем шлюз для работы с бд
        $logGateway = new \Zend\Db\TableGateway\TableGateway(
            'isga_ExchangeTable',
            $dbAdapterForLog,
            null,
            $resultSetPrototype
        );
        $this->setLogGateway($logGateway);
     }

    public function getLinkedItems($table, $linkedFields){
        $linkedTable = new LodEntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result;
    }
    /**
     * @param $table
     * @param $linkedFields
     *
     * @return ResultSet
     */
    public function getLinkedItem($table, $linkedFields){
        $linkedTable = new LodEntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result->current();
    }
    /**
     * @param $table
     * @param $linkedFields
     *
     * @return ResultSet
     */
    public function getLinkedItemIsga($table, $linkedFields){
        $linkedTable = new EntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result->current();
    }

     public function getLinkedItemsIsga($table, $linkedFields){
        $linkedTable = new EntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result;
    }
    public function getLinkedItemsWithOrder($table, $linkedFields,$order){
        $linkedTable = new LodEntitiesTable($table);
        $result = $linkedTable->getAllEntities(null, $order, $linkedFields);
        return $result;
    }

    protected function saveToExchangeTable($id, $saveType){
        $et = new \Model\Gateway\LodEntitiesTable("isga_ExchangeTable");
        $fields = array(
            "SaveType" => $saveType,
            "TableName" => $this->tableName,
            "ObjectId" => $id,
            "Source" => "vuzi"
        );
        $gateway = $et->getGateway();
        $gateway->insert($fields);
    }

    public function save($isNew = false, $doNotSaveToExchangeTable = false){
        if(isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $saveType = "update";
            $ret = $this->tableGateway->update($this->fields,array($this->idName => $this->fields[$this->idName]));
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($this->fields[$this->idName], $saveType);
            return $ret;
        }
        //insert
        else{
            if(!isset($this->fields[$this->idName]))
                $this->fields[$this->idName] = $this->_generateId();
            $this->tableGateway->insert($this->fields);

            $newId = $this->tableGateway->lastInsertValue;

            if($newId == 0){
                $newId = $this->fields[$this->idName];
            }else{
                $this->fields[$this->idName] = $newId;
            }
            $this->fields[$this->idName] = $newId;


            $saveType = "insert";
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($newId , $saveType);
            return $newId;
        }
        return false;
    }

    public function isOurId($fieldName){
        if($this->isEmptyField($fieldName))
            return false;

        if(preg_match(
                "/[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{20}/",
                $this->fields[$fieldName]
            )==1)
            return true;
        else
            return false;
    }
}