<?php

namespace Model\Gateway;

use Zend\Db\TableGateway\TableGateway;

class SqlSrvEntitiesTable{

    protected $tableGateway = null;
    protected $simpleGT = null;
    protected $tablename = null;

    /**
     * �����������
     * @param $tableName - ��� ������� � ������� ����� �������� ����
     */
    public function __construct($tableName){
        // Consumes the configuration array
        $dbAdapter = MSSQLDbAdapter::getInstance();
        //������������� � ���� ������ �������� ������ ������ ����� ������������ �����
        $resultSetPrototype = new VResultSet();
        $entityClassName = "Model\\Gateway\\SqlSrvBaseEntity";
        $resultSetPrototype->setArrayObjectPrototype(new $entityClassName($tableName));
        //���������� ���� ��� ������ � ��
        $this->tableGateway = new TableGateway($tableName, $dbAdapter, null, $resultSetPrototype);

        $this->tablename = $tableName;

    }

    /**
     * ����� �������� ��� ������ �������
     * @param $paging - ���� ������, �� ������ �������� ��� ������������ ��������
     *                  ������:
     *                    array(
     *                      "page" => ����� ��������
     *                      "onPage" => ���������� ��������� �� ��������
     *                    )
     * @param order - ����������, �������� 'id ASC';
     * @return ResultSet
     */
    public function getAllEntities($paging = null,$order = null){
        $tg = $this->tableGateway;
        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($paging, $order, $tg) {


            if(isset($order)){
                $select->order($order);
            }

            if(isset($paging) && isset($paging["onPage"])){
                $page = 0;
                if(isset($paging["page"]))
                    $page = $paging["page"]-1;

                $select
                    ->limit($paging["onPage"])
                    ->offset($paging["page"]*$paging["onPage"]);
            }
            //$sql = $tg->getSql();
            //print_r($sql->getSqlstringForSqlObject($select));die;
        });
        return $resultSet;
    }

    /**
     * ����� ���������� �������� �� id
     *
     * @param $idValue - �������� id ������� ��������
     * @param string $idField - ��� ���� ��������������, ���� ������� �� Id
     * @return ResultSet
     */
    public function getEntityById($idValue, $idField = "Id"){
        $resultSet = $this->tableGateway->select(array($idField => $idValue));
        return $resultSet->current();
    }

    /**
     * ����� ���������� ����� ��������� �������� ��������
     *
     * @param $where - ������, ���� array("Name" => "�����")
     * @return ResultSet
     */
    public function getEntitiesByWhere($where){
        /*
        $tg = $this->tableGateway;
        $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($where, $tg) {


            if(isset($where)){
                $select->where($where);
            }


            $sql = $tg->getSql();
            print_r($sql->getSqlstringForSqlObject($select));
        });
*/
        $result = $this->tableGateway->select($where);

        return $result;
    }

    public function getDistinctIds(){
        $sql = new \Zend\Db\Sql\Sql(MSSQLDbAdapter::getInstance());
        $sql->setTable($this->tablename);

        $select = $sql->select();
        //$select->from("[ISGA].[dbo].[".$this->tablename."]");

        $select->quantifier('DISTINCT');
        $select->columns(array("Id"));
        //$select->columns(array(new \Zend\Db\Sql\Expression('DISTINCT(Id) as Id')));
        //$select->where(array("Id"=>"24"));

        print_r($sql->getSqlStringForSqlObject($select));

        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
            print_r($resultSet);
        return $resultSet;
    }
}