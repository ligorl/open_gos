<?php
namespace Model\Gateway;

use Zend\Db\Sql\Sql;

class ExpertsBaseEntity{

    protected $tableGateway = null;

    protected $tableName;
    protected $idName = "id";
    protected $fields = array();

    public function __construct($id = null, $revisionId = null){
        // Consumes the configuration array
        $dbAdapter = ExpertsDbAdapter::getInstance();
        //Инициируем шлюз для работы с бд
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway($this->tableName, $dbAdapter);

        //Есди задан $id ищем объект с таким id
        if($id != null){
            $resultSet = $this->tableGateway->select(array($this->idName => $id));
            $result = (array)$resultSet->current();
            if(isset($result[$this->idName]))
                $this->fields = (array)$resultSet->current();
        }
    }

    public function delete($doNotSaveToExchangeTable = false){
        if(isset($this->fields[$this->idName])){
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($this->fields[$this->idName], "delete");
            //Delete
            return $this->deleteWhere(array($this->idName => $this->fields[$this->idName]));
        }
        return false;
    }

    public function simpleDelete(){
        if(isset($this->fields[$this->idName])){
            return $this->tableGateway->delete(array($this->idName => $this->fields[$this->idName]));
        }
        return false;
    }

    public function save($isNew = false, $doNotSaveToExchangeTable = false){
        if(isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $saveType = "update";
            $ret = $this->tableGateway->update($this->fields,array($this->idName => $this->fields[$this->idName]));
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($this->fields[$this->idName], $saveType);
            return $ret;
        }
        //insert
        else{
            if(!isset($this->fields[$this->idName]))
                $this->fields[$this->idName] = $this->_generateId();
            $this->tableGateway->insert($this->fields);

            $newId = $this->tableGateway->lastInsertValue;

            if($newId == 0){
                $newId = $this->fields[$this->idName];
            }else{
                $this->fields[$this->idName] = $newId;
            }
            $this->fields[$this->idName] = $newId;
            

            $saveType = "insert";
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($newId , $saveType);
            return $newId;
        }
        return false;
    }

    protected function saveToExchangeTable($id, $saveType){
        $et = new \Model\Gateway\ExpertsEntitiesTable("isga_ExchangeTable");      
        $fields = array(
            "SaveType" => $saveType,
            "TableName" => $this->tableName,
            "ObjectId" => $id,
            "Source" => "vuzi"
        );
        $gateway = $et->getGateway();
        $gateway->insert($fields);
    }

    public function getLastInsertValue()
    {
        return $this->tableGateway->getLastInsertValue();
    }

    public function setField($fieldName,$value){
        $this->fields[$fieldName] = $value;
    }
    
    /**
     * Установить поле безоасно
     * @param type $fieldName
     * @param type $value
     */
    public function setFieldSafe($fieldName, $value) {
        if(!empty($value)){
            $value = strip_tags($value);
        }
        $this->setField($fieldName, $value);
    }    

    public function getField($fieldName){
        if(isset($this->fields[$fieldName]))
            return $this->fields[$fieldName];
        else return null;
    }

    public function getFieldSpec($fieldName)
    {
        if (!$this->isEmptyField($fieldName)) {
            return htmlspecialchars($this->fields[$fieldName]);
        } else {
            return null;
        }
    }

    public function setFields($fields){
        $this->fields = array_merge($this->fields,$fields);
    }

    public function getFields(){
        return $this->fields;
    }

    public function exchangeArray($data)
    {
        $this->setFields($data);
    }

    /**
     * @param $table
     * @param $linkedFields
     *
     * @return ResultSet
     */
    public function getLinkedItem($table, $linkedFields){
        $linkedTable = new ExpertsEntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result->current();
    }

    public function getLinkedItems($table, $linkedFields){
        $linkedTable = new ExpertsEntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result;
    }

    public function toArray(){
        return $this->fields;
    }

    public function getDate($fieldName){
        $fieldValue = $this->getField($fieldName);
        if($fieldValue == "0000-00-00 00:00:00.000" || $fieldValue == "")
            return "";
        return date("d.m.Y",strtotime($fieldValue));
    }

    public function getTimestampDate($fieldName){
        $fieldValue = $this->getField($fieldName);
        if($fieldValue == "0000-00-00 00:00:00")
            return "";
        return date("H:i d.m.Y",strtotime($fieldValue));
    }

    private function _generateId(){
        $mtimeParts = explode(" ",microtime());
        $base = $mtimeParts[1];
        $mtimeParts = explode(".",$mtimeParts[0]);
        $base .= $mtimeParts[1];

        $rand1 = rand ( 1000 , 5000);
        $rand2 = rand ( 1000 , 5000);

        $base .= ($rand1+$rand2);

        $Id = sprintf('%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X', 10+$base[0], 10+$base[1],10+$base[2], 10+$base[3], 10+$base[4], 10+$base[5], 10+$base[6], 10+$base[7],10+$base[8],10+$base[9],10+$base[10],10+$base[11],
            10+$base[12],10+$base[13],10+$base[14],10+$base[15],10+$base[18],10+$base[19],10+$base[20],
            10+$base[21]);
        return $Id;
    }

    /**
     * @param $where array()
     */
    public function deleteWhere($where, $doNotSaveToExchangeTable = false)
    {
        if (isset($where) && is_array($where)) {
            //До удаления запрашиваем объекты что будут удалены
            if(!$doNotSaveToExchangeTable) {
                $eTable = new \Model\Gateway\ExpertsEntitiesTable($this->tableName);
                $items = $eTable->getEntitiesByWhere($where);
            }
            //удаляем
            $result =  $this->tableGateway->delete($where);

            if(!$doNotSaveToExchangeTable){
                foreach($items as $item){
                    $this->saveToExchangeTable($item->getId(),"delete");
                }
            }
            return $result;
        }
        return false;
    }

    /**
     * Сложный запрос с указанием имени таблицы, массива требуемых полей, условия запроса и т.д.
     * @param $table_name Имя таблицы для выборки
     * @param $array_fields массив с именами требуемых полей
     * @param $where условие запроса, массив или строка
     * @param $group Групировка по полю
     * @param $order сортировка по полю
     * @param $limit кол-во записей
     * @param $page страница записей
     * @param $left_table для Джоина таблиц имя таблицы, через массив можно и несколько
     * @param $left_where условие джоина
     * @param $left_column поля сджойненой таблицы
     * @param $joinType - тип джоина
     */
    public function selectComplicated($table_name,$array_fields,$where,$group=NULL,$order=NULL,$limit=NULL,$page=NULL,$left_table=NULL,$left_where=NULL,$left_column=NULL,$joinType='INNER') {
        $dbAdapter = ExpertsDbAdapter::getInstance();
        if(count($array_fields)==0){
            $array_fields=array('*');
        }
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from($table_name);
        $select->columns($array_fields);
        $select->where($where);
        if($limit!==NULL && $page!==NULL){
            $select->limit($limit);
            $offset=$limit*$page;
            $select->offset($offset);
        }
        if($left_table!=NULL){
            //понадобилоссь сджиинить несколько таблиц
            if(is_string($left_table)){
                //если строчка то старое(одно)
                $select->join($left_table, $left_where, $left_column,$joinType);
            }
            if(is_array($left_table)){
                //для много
                $joinTypeDefault = 'Inner';
                if ( is_string($joinType) ) {
                    $joinTypeDefault = $joinType;
                    $joinType = array();
                }
                $joinWhereDefault = $left_where;
                if ( is_string($joinWhereDefault) ) {
                    $joinWhereDefault = $left_where;
                    $left_where = array();
                }
                foreach ($left_table as $key => $value ) {
                    if ( empty($left_column[$key]) ) {
                        $left_column[$key] = '*';
                    }
                    if ( empty($joinType[$key]) ) {
                        $joinType[$key] = $joinTypeDefault;
                    }
                    if ( empty($left_where[$key]) ) {
                        $left_where[$key] = $joinWhereDefault;
                    }
                    $select->join($left_table[$key], $left_where[$key], $left_column[$key], $joinType[$key]);
                }
            }
        }

        if($group!=NULL)
        {
            $select->group($group);
        }
        if($order!=NULL)
        {
            $select->order($order);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    /**
     * Метод убирает черточки из Id
     * @param $id
     * @return return
     */
    public function stripIdDashes($id){
        if(count(explode("-",$id)) > 0)
            return implode("",explode("-",$id));
        else
            return $id;
    }

    public function fillIdDashes($id){
        $subject = $id;
        $pattern = '/^(.{8})(.{4})(.{4})(.{4})(.+)$/i';
        preg_match($pattern, $subject, $matches);
        array_shift($matches);

        return implode("-",$matches);
    }

    /**
     * Метод аозвращает true если поле не задано и false если задано
     * 
     * @param $fieldName
     * @return boolean
     */
    public function isEmptyField($fieldName){
        if(
            !isset($this->fields[$fieldName])
            || $this->fields[$fieldName] == ""
            || strtolower($this->fields[$fieldName]) == "null"
        )
            return true;
        else
            return false;
    }

    public function getId(){
        return $this->getField($this->idName);
    }
    
    public function getIdName(){
        return $this->idName;
    }

    /**
     * Метод возвращает сущность по id
     *
     * @param $idValue - значение id искомой сущности
     * @param string $idField - имя поля идентификатора, если отлично от Id
     * @return ResultSet
     */
    public function getEntityById($idValue, $idField = null){
        if(empty($idField)){
            $idField = $this->idName;
        }
        $resultSet = $this->tableGateway->select(array($idField => $idValue));
        return $resultSet->current();
    }    
    public function getEntityByWhere($where){
        $resultSet = $this->tableGateway->select($where);
        return $resultSet->current();
    }    
    
    /**
     * возращает Ентри по ид
     * 
     * @param type $idValue - значение id искомой сущности
     * @param type $idField - имя поля идентификатор
     * @return type
     */
    public function getById($idValue, $idField = null){
        if(empty($idField)){
            $idField = $this->idName;
        }
        $result = $this->getByWhere(array($idField => $idValue));
        return $result;
    }    
    public function getByWhere($where){
        $result = $this->getLinkedItem($this->tableName,$where);
        return $result;
    }    
    public function getAllByWhere($where){
        $result = $this->getLinkedItems($this->tableName,$where);
        return $result->getObjectsArray();
    }    
    public function getAllById($idValue, $idField = null){
        if(empty($idField)){
            $idField = $this->idName;
        }
        $result = $this->getAllByWhere(array($idField => $idValue));
        return $result;
    }    
           
  
    public function getFieldOrSpace($fieldName){
        if(isset($this->fields[$fieldName]))
            return $this->fields[$fieldName];
        else return '';
    }

    /**
     * возращает безопасное значение или возращает пустую строку
     * @param type $fieldName - имя поля
     * @return strung
     */
    public function getFieldSafe($fieldName)
    {
        $result = $this->getFieldOrSpace($fieldName);
        $result = strip_tags($result);
        return $result;
    }
        
    /**
     * выводим поле, с перебивкой в хтмл сущьности
     * @param type $fieldName
     * @return type
     */
    public function getFieldHtml($fieldName)
    {
        $result = $this->getFieldOrSpace($fieldName);
        //$result = strip_tags($result);
        $result = htmlentities($result);
        return $result;
    }    

    /**
     * Метод проверяет если этот объект пустой, например запрашивали по айди из бд, а ничего не нашло
     */
    public function isEmpty(){
        if($this->getField($this->idName) == null)
            return true;
        return false;
    }
    /**
     * Делает запрос и ракидывает массив по полю $fieldName
     *
     * @param type $where - запрос
     * @param type $fieldName - поле по которому ассоциировать
     * @return type
     */
    public function getAllByWhereParseId($where, $fieldName = null){
        $result = $this->getLinkedItems($this->tableName,$where);
        $result = $result->getObjectsArray();
        $resultOut=array();
        if(empty($fieldName)){
            $fieldName = $this->idName;
        }
        if(!empty($result)){
            foreach ( $result as $value ) {
                $resultOut[$value->getField($fieldName)] = $value;
            }
        }
        return $resultOut;
    }
    
}