<?php

namespace Model\Gateway;

use Zend\Db\TableGateway\TableGateway;

class PEntitiesTable{

    protected $tableGateway = null;

    /**
     * �����������
     * @param $tableName - ��� ������� � ������� ����� �������� ����
     */
    public function __construct($tableName){
        // Consumes the configuration array
        $dbAdapter = DbAdapter::getInstance();
        //������������� � ���� ������ �������� ������ ������ ����� ������������ �����
        $resultSetPrototype = new VResultSet();
        $entityClassName = "Model\\Gateway\\PBaseEntity";//Entities\\".$tableName;
        $resultSetPrototype->setArrayObjectPrototype(new $entityClassName($tableName));
        //���������� ���� ��� ������ � ��
        $this->tableGateway = new TableGateway($tableName, $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * ����� �������� ��� ������ �������
     * @param $paging - ���� ������, �� ������ �������� ��� ������������ ��������
     *                  ������:
     *                    array(
     *                      "page" => ����� ��������
     *                      "onPage" => ���������� ��������� �� ��������
     *                    )
     * @param order - ����������, �������� 'id ASC';
     * @return ResultSet
     */
    public function getAllEntities($paging = null,$order = null){

        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($paging, $order) {


            if(isset($order)){
                $select->order($order);
            }

            if(isset($paging) && isset($paging["onPage"])){
                $page = 0;
                if(isset($paging["page"]))
                    $page = $paging["page"]-1;

                $select
                    ->limit($paging["onPage"])
                    ->offset($paging["page"]*$paging["onPage"]);
            }
            //$sql = $this->tableGateway->getSql();
            //print_r($sql->getSqlstringForSqlObject($select));die;
        });
        return $resultSet;
    }

    /**
     * ����� ���������� �������� �� id
     *
     * @param $idValue - �������� id ������� ��������
     * @param string $idField - ��� ���� ��������������, ���� ������� �� Id
     * @return ResultSet
     */
    public function getEntityById($idValue, $idField = "Id"){
        $resultSet = $this->tableGateway->select(array($idField => $idValue));
        return $resultSet->current();
    }

    /**
     * ����� ���������� ����� ��������� �������� ��������
     *
     * @param $where - ������, ���� array("Name" => "�����")
     * @return ResultSet
     */
    public function getEntitiesByWhere($where){
/*
        $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($where) {


            if(isset($where)){
                $select->where($where);
            }


            $sql = $this->tableGateway->getSql();
            print_r($sql->getSqlstringForSqlObject($select));
        });
*/
        $result = $this->tableGateway->select($where);

        return $result;
    }
}