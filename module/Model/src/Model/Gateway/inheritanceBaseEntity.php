<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Model\Gateway;

use Zend\Db\Sql\Sql;

// ЛОГИРУЙТЕ НОВЫЕ ДЕЙСТВИЯ (UPDATE/INSERT/DELETE) методом $this->loggingIfOn([]);

/**
 * Description of inheritanceBaseEntity
 * Простенький класс для создания базовой сущности 
 * @author Руслан
 */
class inheritanceBaseEntity {

    protected
            $tableGateway = null,
            $tableName = null,
            $idName = null,
            $adapter = null,
            $logTableGateway = null,
            $logSource = 'unset',
            $logIsOn = false,
            $arrayObjectPrototype = null,
            $fields = [];

    public function __construct($adapter, $tableName = null) {
        //инициализируем переменные
        if (!empty($tableName)) {
            $this->tableName = $tableName;
        }
        $this->setAdapter($adapter);
        $this->tableGatewayInit();
    }
    
    public function getFieldDefault($fieldName) {
        if (isset($this->fields[$fieldName])) {
            return $this->fields[$fieldName];
        } else {
            return null;
        }
    }
    

    /**
     * Инициируем шлюз для работы с бд
     */
    public function tableGatewayInit() {
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway(
                $this->getTableName(), $this->getAdapter()
        );
    }

    /**
     * Инициализирует tablegateway улучшенным массивом результатов
     * @param type $entities
     */
    public function tableGatewayVResultSetInit($entities = null) {
        if (empty($entities)) {
            $entities = clone $this;
        }
        //Инициируем шлюз для работы с бд
        $resultSetPrototype = new VResultSet();
        $resultSetPrototype->setArrayObjectPrototype($entities);

        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway(
                $this->getTableName(), $this->getAdapter(), null, $resultSetPrototype
        );
    }

    /**
     * Возвращает массив полей
     * @return type
     */
    public function toArray() {
        return $this->fields;
    }

    /**
     * метод для модификации результатов в массив объектов
     * @param type $data
     */
    public function exchangeArray($data) {
        $this->setFields($data);
    }

    /**
     * Устанавливает массив полей
     * @param type $fields
     */
    public function setFields($fields) {
        $this->fields = array_merge($this->fields, $fields);
    }

    /**
     * Установить поле
     * @param type $fieldName
     * @param type $value
     */
    public function setField($fieldName, $value) {
        $this->fields[$fieldName] = $value;
    }

    /**
     *  Возвращает текущее состояние "тумблера" лога
     * @return type
     */
    public function getLogTumblerValue() {
        return $this->logIsOn;
    }

    /**
     * Включает лог и возвращает текущее состояние
     */
    public function turnOnLogging() {
        return $this->logIsOn = true;
        return $this->logIsOn;
    }

    /**
     * Выключает лог и возвращает текущее состояние
     */
    public function turnOffLogging() {
        return $this->logIsOn = false;
        return $this->logIsOn;
    }

    /**
     * Возвращает Source лога
     * @return type
     */
    public function getLogSource() {
        return $this->logSource;
    }

    /**
     * Устанавливает Source лога
     * @return type
     */
    public function setLogSource($source) {
        $this->logSource = $source;
        return $this->logSource;
    }

    /**
     * Возвращает TableGateway таблицы для лога
     * @return type
     */
    public function getLogGateway() {
        return $this->logTableGateway;
    }

    /**
     * Устанавливает инициализированный TableGateway для лога
     * @param \Zend\Db\TableGateway\TableGateway $gateway
     * @return type
     */
    public function setLogGateway(\Zend\Db\TableGateway\TableGateway $gateway) {
        $this->logTableGateway = $gateway;
        return $this->logTableGateway;
    }

    /**
     * Возвращает адаптер
     * @return type
     */
    public function getAdapter() {
        return $this->adapter;
    }

    /**
     * Устанавливает Адаптер
     * @param \Zend\Db\Adapter\Adapter $adapter
     * @return type
     */
    public function setAdapter(\Zend\Db\Adapter\Adapter $adapter) {
        $this->adapter = $adapter;
        return $this->adapter;
    }

    /**
     * Возващает текущий TableGateway
     * @return type
     */
    public function getGateway() {
        return $this->tableGateway;
    }

    /**
     * Возвращает значение tableName
     * @return type
     */
    public function getTableName() {
        return $this->tableName;
    }

    /**
     * Возвращает ключевой столбец в таблице
     */
    public function getId() {
        return $this->getField($this->idName);
    }

    /**
     * Возвращает имя ключевого столбца в таблице
     * @return type
     */
    public function getIdName() {
        return $this->idName;
    }

    /**
     * Возвращает данные сущности в массив полей класса
     * Если $idName пуст берет его из $this->idName
     * @param type $idValue
     */
    public function getById($idValue) {
        $idName = $this->getIdName();

        $resultSet = $this->getGateway()->select([
            $idName => $idValue
        ]);
        $result = $resultSet->current();
        if ($result) {
            $this->fields = $result->toArray();
        }
    }

    /**
     * логирует действия если логирование включено 
     * @param type $data
     */
    public function loggingIfOn($data) {
        if ($this->getLogTumblerValue()) {
            $this->log($data);
        }
    }

    /**
     * Записывает данные в лог из массива
     * [
     *      action => тип действия
     *      tableName => имя таблицы по ум. буреться текущее
     *      objectId => имя ключевого поля при записи в таблицу по ум. береться текущее
     *      source => источник
     * ]
     * @param array $data
     * Срабатывает при не инициализированом TableGateway лога
     * @throws \Exception 
     */
    public function log(array $data) {
        $logGateway = $this->getLogGateway();
        $fields = [
            "SaveType" => 'some action',
            "TableName" => $this->getTableName(),
            "ObjectId" => $this->getIdName(),
            "Source" => $this->getLogSource()
        ];

        if (empty($logGateway)) {
            throw new \Exception('TableGateway лога не инициализирован!');
        }

        switch (true) {
            case!empty($data['action']):
                $fields['SaveType'] = $data['action'];

            case!empty($data['tableName']):
                $fields['TableName'] = $data['tableName'];

            case!empty($data['objectId']):
                $fields['ObjectId'] = $data['objectId'];

            case!empty($data['source']):
                $fields['Source'] = $data['source'];
            default:
                break;
        }
        $logGateway->insert($fields);
    }

    /**
     * Возвращает значение поля
     * @param type $fieldName
     * @return type
     */
    public function getField($fieldName) {
        if (!$this->isEmptyField($fieldName)) {
            return $this->fields[$fieldName];
        } else {
            return null;
        }
    }

    /**
     * Метод вовращает содержимое поля с дато1 в удобном для вывода формате
     * @param $fieldName
     * @return bool|string
     */
    public function getDate($fieldName) {
        $fieldValue = $this->getFieldSafe($fieldName);
        if ($fieldValue == "0000-00-00 00:00:00.000" 
            || $fieldValue == "0000-00-00" 
            || $fieldValue == "" 
            || $fieldValue == "1970-01-01"
            || $fieldValue == "Invalid date"
            || empty($fieldValue)
        ){
            return "";
        }
        return date("d.m.Y", strtotime($fieldValue));
    }

    public function delete($doNotSaveToExchangeTable = false){
        if(isset($this->fields[$this->idName])){
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($this->fields[$this->idName], "delete");
            //Delete
            return $this->deleteWhere(array($this->idName => $this->fields[$this->idName]));
        }
        return false;
    }

    /**
     * @param $where array()
     * @param $doNotSaveToExchangeTable - если true, то не добавляет объект в таблицу для обмена между кабинетами
     */
    public function deleteWhere($where, $doNotSaveToExchangeTable = false)
    {
        if (isset($where) && is_array($where)) {
            //До удаления запрашиваем объекты что будут удалены
            if(!$doNotSaveToExchangeTable) {
                $eTable = new \Model\Gateway\LodEntitiesTable($this->tableName);
                $items = $eTable->getEntitiesByWhere($where);
            }
            //удаляем
            $result =  $this->tableGateway->delete($where);

            if(!$doNotSaveToExchangeTable){
                foreach($items as $item){
                    $this->saveToExchangeTable($item->getId(),"delete");
                }
            }
            return $result;
        }
        return false;
    }

    protected function saveToExchangeTable($id, $saveType){
        $et = new \Model\Gateway\LodEntitiesTable("isga_ExchangeTable");
        $fields = array(
            "SaveType" => $saveType,
            "TableName" => $this->tableName,
            "ObjectId" => $id,
            "Source" => "vuzi"
        );
        $gateway = $et->getGateway();
        $gateway->insert($fields);
    }

    public function simpleDelete(){
        if(isset($this->fields[$this->idName])){
            return $this->tableGateway->delete(array($this->idName => $this->fields[$this->idName]));
        }
        return false;
    }

    /**
     * отдает значения поля или пустую строку
     * @param type $fieldName - имя поля
     * @return string - результат
     */
    public function getFieldOrSpace($fieldName) {
        if (!$this->isEmptyField($fieldName)) {
            return $this->fields[$fieldName];
        } else {
            return '';
        }
    }
    
    /**
     * возращает безопасное значение или возращает пустую строку
     * @param type $fieldName - имя поля
     * @return strung
     */
    public function getFieldSafe($fieldName)
    {
        $result = $this->getFieldOrSpace($fieldName);
        $result = strip_tags($result);
        return $result;
    }
        
    /**
     * выводим поле, с перебивкой в хтмл сущьности
     * @param type $fieldName
     * @return type
     */
    public function getFieldHtml($fieldName)
    {
        $result = $this->getFieldOrSpace($fieldName);
        //$result = strip_tags($result);
        $result = htmlentities($result);
        return $result;
    }    

    /**
     * Метод аозвращает true если поле не задано и false если задано
     * @param $fieldName
     * @return boolean
     */
    public function isEmptyField($fieldName) {
        if (
                !isset($this->fields[$fieldName]) || empty($this->fields[$fieldName]) || $this->fields[$fieldName] == "" || strtolower($this->fields[$fieldName]) == "null"
        )
            return true;
        else
            return false;
    }

    /**
     * Возвращаем все
     * @return type
     */
    public function getAll() {
        $result = $this->getGateway()->select();
        return $result;
    }

    /**
     * Возвращаем where
     * @return type
     */
    public function getByWhere($where) {
        $result = $this->getGateway()->select($where);
        return $result->current();
    }

    /**
     * Возвращаем все where
     * @return type
     */
    public function getAllByWhere($where) {
        $result = $this->getGateway()->select($where);
        return $result;
    }

    /**
     * Получаем все по where с сортировкой
     * @param \Zend\Db\Sql\Where $where
     * @param string $order
     */
    public function getAllByWhereWithOrder($where, $order) {
        $table = $this->getGateway()->getTable();

        $select = new \Zend\Db\Sql\Select($table);

        $select->where($where)->order($order);

        $result = $this->getGateway()->selectWith($select);
        return $result;
    }

    /**
     * выдергиваем все записи и раскидываем их по ключевому массиву
     * 
     * @param type $where запрос
     * @param type $fieldName - ключевое поле по которому раскинуть массив, 
     *                          по умолчанию ИД
     * @return type - массив 
     */
    public function getAllByWhereParseId($where = null, $fieldName = null) {
        $result = $this->getLinkedItems($this->tableName, $where);
        $result = $result->getObjectsArray();
        $resultOut = array();
        if (empty($fieldName)) {
            $fieldName = $this->idName;
        }
        if (!empty($result)) {
            foreach ($result as $value) {
                $resultOut[$value->getField($fieldName)] = $value;
            }
        }
        return $resultOut;
    }

    /**
     * метод возвращает набор полей
     * @return array
     */
    public function getFields() {
        return $this->fields;
    }

    /**
     * @param bool|false $isNew - если true, то объект запишется в бд через insert , даже если задан Id
     * если false, то через insert будет работать только если не задан id, заодно сгенерирует id
     *
     * @return id
     */
    public function save($isNew = false, $doNotSaveToExchangeTable = false) {
        if ((isset($this->fields[$this->idName]) && !$this->isEmptyField($this->idName)) && !$isNew) {
            //Update
            $saveType = "update";
            $ret = $this->tableGateway->update($this->fields, array($this->idName => $this->fields[$this->idName]));
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($this->fields[$this->idName], $saveType);
            return $ret;
        }
        //insert
        else {
            if (!isset($this->fields[$this->idName]) || $this->isEmptyField($this->idName))
                $this->fields[$this->idName] = $this->_generateId();
            $this->tableGateway->insert($this->fields);

            $newId = $this->tableGateway->lastInsertValue;

            if ($newId == 0)
                $newId = $this->fields[$this->idName];

            $saveType = "insert";
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($newId , $saveType);
            return $newId;
        }
        return false;
    }

    /**
     * Метод генерирует айди
     * @return string
     */
    public function _generateId() {
        $mtimeParts = explode(" ", microtime());
        $base = $mtimeParts[1];
        $mtimeParts = explode(".", $mtimeParts[0]);
        $base .= $mtimeParts[1];

        $rand1 = rand(1000, 5000);
        $rand2 = rand(1000, 5000);

        $base .= ($rand1 + $rand2);

        $Id = sprintf('%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X', 10 + $base[0], 10 + $base[1], 10 + $base[2], 10 + $base[3], 10 + $base[4], 10 + $base[5], 10 + $base[6], 10 + $base[7], 10 + $base[8], 10 + $base[9], 10 + $base[10], 10 + $base[11], 10 + $base[12], 10 + $base[13], 10 + $base[14], 10 + $base[15], 10 + $base[18], 10 + $base[19], 10 + $base[20], 10 + $base[21]);
        return $Id;
    }

    /**
     * Метод генерирует GUID
     * @return string
     */
    protected function _generateGUID() {
        //Генерируемый нами айди 0B0E100C-1310-0D0A-130E-0D110A12130D100E0D12
        //Генерируемый нами guid, для того чтобы подходило всяким sql таблицам, где айдишники такого типа 00038aec-a3eb-4e6d-bdd5-5027b405beb7
        $mtimeParts = explode(" ", microtime());
        $base = $mtimeParts[1];
        $mtimeParts = explode(".", $mtimeParts[0]);
        $base .= $mtimeParts[1];

        $rand1 = rand(1000, 5000);
        $rand2 = rand(1000, 5000);

        $base .= ($rand1 + $rand2);

        $Id = sprintf('%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X', 10 + $base[0], 10 + $base[1], 10 + $base[2], 10 + $base[3], 10 + $base[4], 10 + $base[5], 10 + $base[6], 10 + $base[7], 10 + $base[8], 10 + $base[9], 10 + $base[10], 10 + $base[11], 10 + $base[12], 10 + $base[13], 10 + $base[14], 10 + $base[15]);
        return $Id;
    }

    /**
     * Вернуть выражение
     * Берет массив формата описанного в методе класса WhereConditionGenerator, и возвращает предикат
     * @param array $Arr
     * @return predicate expression
     */
    public function getArrayToPredicate(array $Arr) {
        try {
            $whereCondition = new WhereConditionGenerator();
            $whereCondition->add($Arr);
            return $whereCondition->getPredicate();
        } catch(\Exception $e){
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log($e->getLine().": ".$e->getMessage());
        }
    }

    /**
     * Получает данные по условиям из массива
     * @param array $condition
     * @return resultSet
     */
    public function getData_ArrayCondition(array $condition) {
        $tableGateway = $this->getGateway();
        $where = new \Zend\Db\Sql\Where();
        $expression = $this->getArrayToPredicate($condition);
        $where->addPredicate($expression);
        $resultSet = $this->tableGateway->select(
                function(\Zend\Db\Sql\Select $select) use ($where, $tableGateway) {
            $select->where($where);
        });
        return $resultSet;
    }

    /**
     * Получает количество по условиям из массива
     * @param array $condition
     * @return resultSet
     */
    public function getDataCount_ArrayCondition(array $condition) {
        $tableGateway = $this->getGateway();
        $where = new \Zend\Db\Sql\Where();
        $expression = $this->getArrayToPredicate($condition);
        $where->addPredicate($expression);
        $resultSet = $this->tableGateway->select(
                function(\Zend\Db\Sql\Select $select) use ($where, $tableGateway) {
            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')))->where($where);
        });

        if ($resultSet) {
            $result = $resultSet->toArray();
            if (!empty($result[0])) {
                return $result[0]['num'];
            }
        }
        return 0;
    }

    /*
     * Получить выбранные поля, из выбранных таблиц, используя условный массив
     * @param
     * $columnsArr массив полей
     * $joinArray = [
      'table' => 'table_name',
      'binder' => 'table1.col = table2.col',
      'type' => 'left||right...'
     * ]
     * $arrayCondition = []
     *
     * @return ResultSet
     */

    public function getDataJoin_ArrayCondition($columnsArr, $joinArray, $conditionArray) {
        try {
            $expression = $this->getArrayToPredicate($conditionArray);
            $where = new \Zend\Db\Sql\Where();
            $where->addPredicate($expression);
            $tg = $this->tableGateway;
            $resultSet = $this->tableGateway->select(
                function (\Zend\Db\Sql\Select $select) use ($where, $columnsArr, $joinArray, $tg) {
                    $select
                        ->columns($columnsArr)
                        ->join(
                            $joinArray['table'], $joinArray['binder'], [], $joinArray['type']
                        )
                        ->where($where);

                    //$sql = $tg->getSql();
                    //$logger = new \Model\Logger\Logger("service",true);
                    //$logger->Log(print_r($sql->getSqlstringForSqlObject($select),true));//;die;
                });
            return $resultSet;
        } catch(\Exception $e){
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log($e->getLine().": ".$e->getMessage());
        }
    }

    /**
     * Получаем массив полей таблицы
     * @return type
     */
    public function getColumns() {
        $gateway = $this->getGateway();
        $table = $gateway->getTable();
        $adapter = $gateway->getAdapter();
        $base = $adapter->getCurrentSchema();

        $table = new \Zend\Db\TableGateway\TableGateway($table, $adapter, new \Zend\Db\TableGateway\Feature\MetadataFeature());
        return $table->getColumns();
    }

    /**
     *
     * @param type $multiFieldArr
     * массив c массивами где каждый внутренний массив содержит имя_поля=>значение определенной строки
     * @param type $keyColumn
     * поле по которому определяеться update елемент
     * @return type
     */
    public function multipleUpdate($multiFieldArr, $keyColumn) {
        $updateStr = '';
        $gateway = $this->getGateway();
        $adapter = $gateway->getAdapter();
        $tableName = $gateway->getTable();

        foreach ($multiFieldArr as $fieldArr) {
            $setPart = '';
            $wherePart = '';

            foreach ($fieldArr as $field => $val) {

                if ($field === $keyColumn) {
                    $wherePart = 'WHERE ' . $field . '="' . $val . '"';
                } else {
                    $setPart .=$field . '="' . $val . '",';
                }
            }
            $partLen = strlen($setPart);
            $setPart = substr($setPart, 0, $partLen - 1);
            $updateStr .= 'UPDATE ' . $tableName . ' SET ' . $setPart . ' ' . $wherePart . '; ';
        }
        $statement = $adapter->query($updateStr);
        return $statement->execute()->getResource()->closeCursor();
    }

    
    
    /**
     * Установить поле безоасно
     * @param type $fieldName
     * @param type $value
     */
    public function setFieldSafe($fieldName, $value) {
        if(!empty($value)){
            $value = strip_tags($value);
        }
        $this->setField($fieldName, $value);
    }

    /**
     * безопасное сохранение массива полей
     * @param type $fields
     */
    public function setFieldsSafe($fields) {
        foreach ( $fields as $fieldName => $value ) {
            $this->setFieldSafe($fieldName, $value);
        }
    }

    /**
     * Сложный запрос с указанием имени таблицы, массива требуемых полей, условия запроса и т.д.
     * @param $table_name Имя таблицы для выборки
     * @param $array_fields массив с именами требуемых полей
     * @param $where условие запроса, массив или строка
     * @param $group Групировка по полю
     * @param $order сортировка по полю
     * @param $limit кол-во записей
     * @param $page страница записей
     * @param $left_table для Джоина таблиц имя таблицы, через массив можно и несколько
     * @param $left_where условие джоина
     * @param $left_column поля сджойненой таблицы
     * @param $joinType - тип джоина
     */
    public function selectComplicated($table_name,$array_fields,$where,$group=NULL,$order=NULL,$limit=NULL,$page=NULL,$left_table=NULL,$left_where=NULL,$left_column=array(),$joinType='INNER') {
        $dbAdapter = $this->getAdapter();
        if(count($array_fields)==0){
            $array_fields=array('*');
        }
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from($table_name);
        $select->columns($array_fields);
        $select->where($where);
        if($limit!==NULL && $page!==NULL){
            $select->limit($limit);
            $offset=$limit*$page;
            $select->offset($offset);
        }
        if($left_table!=NULL){
            //понадобилоссь сджиинить несколько таблиц
            if(is_string($left_table)){
                //если строчка то старое(одно)
                $select->join($left_table, $left_where, $left_column,$joinType);
            }
            if(is_array($left_table)){
                //для много
                $joinTypeDefault = 'Inner';
                if ( is_string($joinType) ) {
                    $joinTypeDefault = $joinType;
                    $joinType = array();
                }
                $joinWhereDefault = $left_where;
                if ( is_string($joinWhereDefault) ) {
                    $joinWhereDefault = $left_where;
                    $left_where = array();
                }
                foreach ($left_table as $key => $value ) {
                    if ( empty($left_column[$key]) ) {
                        $left_column[$key] = array();
                    }
                    if ( empty($joinType[$key]) ) {
                        $joinType[$key] = $joinTypeDefault;
                    }
                    if ( empty($left_where[$key]) ) {
                        $left_where[$key] = $joinWhereDefault;
                    }
                    $select->join($left_table[$key], $left_where[$key], $left_column[$key], $joinType[$key]);
                }
            }
        }

        if($group!=NULL)
        {
            $select->group($group);
        }
        if($order!=NULL)
        {
            $select->order($order);
        }

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    /**
     * проверка на пустость записи
     * 
     * @return boolean
     */
    public function isEmpty(){
        if($this->getField($this->idName) == null)
            return true;
        return false;
    }


    //последняя строка    
}
