<?php
namespace Model\Gateway;

use Zend\Db\Sql\Sql;

//Класс для работы с сущностями реестра лицензий, наследуем от  BaseEntity,
//только переопределяем конструктор, для работы с другой бд
class LicReestrBaseEntity extends BaseEntityToReestrLic{


    public function __construct($id = null, $revisionId = null){
        // Consumes the configuration array
        $dbAdapter = LicReestrDbAdapter::getInstance();
        //Инициируем шлюз для работы с бд
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway($this->tableName, $dbAdapter);

        //Есди задан $id ищем объект с таким id
        if($id != null){
            $resultSet = $this->tableGateway->select(array($this->idName => $id));
            $this->fields = (array)$resultSet->current();
        }
    }

    public function save($isNew = false, $doNotSaveToExchangeTable = false){
        if(isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $saveType = "update";
            $ret = $this->tableGateway->update($this->fields,array($this->idName => $this->fields[$this->idName]));
            return $ret;
        }
        //insert
        else{
            //if(!isset($this->fields[$this->idName]))
            //   $this->fields[$this->idName] = $this->_generateId();
            $this->tableGateway->insert($this->fields);

            $newId = $this->tableGateway->lastInsertValue;

            if($newId == 0)
                $newId = $this->fields[$this->idName];
            else
                $this->fields[$this->idName] = $newId;

            $saveType = "insert";
            return $newId;
        }
        return false;
    }
}