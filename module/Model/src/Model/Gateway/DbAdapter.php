<?php

namespace Model\Gateway;

use PDO;

class DbAdapter
{
    protected static $_instance = null;

    private function __construct(){

    }

    private function __clone(){
    }

    public static function getInstance() {
        // проверяем актуальность экземпляра
        if (null === self::$_instance) {
            // создаем новый экземпляр
            $config = new \Zend\Config\Config(include (__DIR__.'../../../../../../config/autoload/global.php'));
            $dbAdapter = new \Zend\Db\Adapter\Adapter(array(
                'driver' => $config->db->driver,
                'dsn' => $config->db->dsn,
                'username' => $config->db->username,
                'password' => $config->db->password,
                'charset'        => 'utf8',
                'driver_options' => array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
                    //PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARSET 'utf8'",
                    //PDO::MYSQL_ATTR_INIT_COMMAND => "set_client='utf8'",
                    //PDO::MYSQL_ATTR_INIT_COMMAND => "set character_set_results='utf8'",
                    //PDO::MYSQL_ATTR_INIT_COMMAND => "SET collation_connection = 'utf8_general_ci",
                )
            ));

            self::$_instance = $dbAdapter;
        }
        // возвращаем созданный или существующий экземпляр
        return self::$_instance;
    }
}