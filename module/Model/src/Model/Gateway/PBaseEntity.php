<?php
namespace Model\Gateway;

class PBaseEntity{

    protected $tableGateway = null;

    protected $tableName;
    protected $idName = "Id";
    protected $fields = array();

    public function __construct($tableName){
        $this->tableName = $tableName;

        $dbAdapter = DbAdapter::getInstance();
        //���������� ���� ��� ������ � ��
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway($this->tableName, $dbAdapter);
    }
/*
    public function __construct(,$id = null, $revisionId = null){
        // Consumes the configuration array
        $config = new \Zend\Config\Config(include 'config\autoload\global.php');
        $dbAdapter = new \Zend\Db\Adapter\Adapter(array(
            'driver' => $config->db->driver,
            'dsn' => $config->db->dsn,
            'username' => $config->db->username,
            'password' => $config->db->password
        ));
        //���������� ���� ��� ������ � ��
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway($this->tableName, $dbAdapter);

        //���� ����� $id ���� ������ � ����� id
        if(isset($id)){
            //���� ��� RevisionId
            if(!$this->hasRevisionId){
                $resultSet = $this->tableGateway->select(array($this->idName => $id));

                    $this->fields = (array)$resultSet->current();
            }
            //���� ���� RevisionId
            else if($revisionId != null){
                $resultSet = $this->tableGateway->select(array($this->idName => $id, "RevisionId" => $revisionId));
                if($resultSet->current() != null)
                    $this->fields = (array)$resultSet->current();
            }
        }
    }
*/

    public function delete(){
        if(isset($this->fields[$this->idName])){
            //Delete
            $this->tableGateway->delete(array($this->idName => $this->fields[$this->idName]));
        }
    }

    public function save($isNew = false){
        //���� ��� RevisionId

        if(isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $this->tableGateway->update($this->fields,array($this->idName => $this->fields[$this->idName]));
        }
        //insert
        else{
            $this->tableGateway->insert($this->fields);
        }
    }

    public function setField($fieldName,$value){
        $this->fields[$fieldName] = $value;
    }

    public function getField($fieldName){
        if(isset($this->fields[$fieldName]))
            return $this->fields[$fieldName];
        else return null;
    }

    public function setFields($fields){
        $this->fields = array_merge($this->fields,$fields);
    }

    public function getFields(){
        return $this->fields;
    }

    public function exchangeArray($data)
    {
        $this->setFields($data);
    }

    /**
     * @param $table
     * @param $linkedFields
     *
     * @return ResultSet
     */
    public function getLinkedItem($table, $linkedFields){
        $linkedTable = new EntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result->current();
    }

    public function getLinkedItems($table, $linkedFields){
        $linkedTable = new EntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result;
    }

    public function toArray(){
        return $this->fields;
    }

    public function getDate($fieldName){
        $fieldValue = $this->getField($fieldName);
        if($fieldValue == "0000-00-00 00:00:00.000")
            return "";
        return date("d.m.Y",strtotime($fieldValue));
    }
}