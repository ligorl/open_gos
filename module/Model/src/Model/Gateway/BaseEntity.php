<?php
namespace Model\Gateway;

use Zend\Db\Sql\Sql;

class BaseEntity{

    protected $tableGateway = null;

    protected $tableName;
    protected $idName = "Id";
    protected $fields = array();

    public function __construct($id = null, $revisionId = null){
        // Consumes the configuration array
        $dbAdapter = DbAdapter::getInstance();
        //Инициируем шлюз для работы с бд
        $this->tableGateway = new \Zend\Db\TableGateway\TableGateway($this->tableName, $dbAdapter);

        //Есди задан $id ищем объект с таким id
        if($id != null){
            $resultSet = $this->tableGateway->select(array($this->idName => $id));
            $this->fields = (array)$resultSet->current();
        }
    }

    public function delete($doNotSaveToExchangeTable = false){
        if(isset($this->fields[$this->idName])){
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($this->fields[$this->idName], "delete");
            //Delete
            return $this->deleteWhere(array($this->idName => $this->fields[$this->idName]));
        }
        return false;
    }

    public function simpleDelete(){
        if(isset($this->fields[$this->idName])){
            return $this->tableGateway->delete(array($this->idName => $this->fields[$this->idName]));
        }
        return false;
    }

    public function save($isNew = false, $doNotSaveToExchangeTable = false){
        if(isset($this->fields[$this->idName]) && !$isNew){
            //Update
            $saveType = "update";
            $ret = $this->tableGateway->update($this->fields,array($this->idName => $this->fields[$this->idName]));
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($this->fields[$this->idName], $saveType);
            return $ret;
        }
        //insert
        else{
            if(!isset($this->fields[$this->idName]))
                $this->fields[$this->idName] = $this->_generateId();
            $this->tableGateway->insert($this->fields);

            $newId = $this->tableGateway->lastInsertValue;

            if($newId == 0)
                $newId = $this->fields[$this->idName];

            $saveType = "insert";
            if(!$doNotSaveToExchangeTable)
                $this->saveToExchangeTable($newId , $saveType);
            return $newId;
        }
        return false;
    }

    public function setField($fieldName,$value){
        $this->fields[$fieldName] = $value;
    }

    public function getField($fieldName){
        if(!$this->isEmptyField($fieldName))
            return $this->fields[$fieldName];
        else return null;
    }

    public function getFieldSpec($fieldName)
    {
        if (!$this->isEmptyField($fieldName)) {
            return htmlspecialchars($this->fields[$fieldName]);
        } else {
            return null;
        }
    }


    /**
     * отдает значения поля или пустую строку
     * @param type $fieldName - имя поля
     * @return string - результат
     */
    public function getFieldOrSpace($fieldName)
    {
        if (!$this->isEmptyField($fieldName)) {
            return $this->fields[$fieldName];
        } else {
            return '';
        }
    }

    /**
     * возращает безопасное значение или возращает пустую строку
     * @param type $fieldName - имя поля
     * @return strung
     */
    public function getFieldSafe($fieldName)
    {
        $result = $this->getFieldOrSpace($fieldName);
        $result = strip_tags($result);
        return $result;
    }

    /**
     * выводим поле, с перебивкой в хтмл сущьности
     * @param type $fieldName
     * @return type
     */
    public function getFieldHtml($fieldName)
    {
        $result = $this->getFieldOrSpace($fieldName);
        //$result = strip_tags($result);
        $result = htmlentities($result);
        return $result;
    }

    public function setFields($fields, $overwrite = false){
        if($overwrite)
            $this->fields = $fields;
        else
            $this->fields = array_merge($this->fields,$fields);
    }

    public function getFields(){
        return $this->fields;
    }

    public function exchangeArray($data)
    {
        $this->setFields($data);
    }

    /**
     * @param $table
     * @param $linkedFields
     *
     * @return ResultSet
     */
    public function getLinkedItem($table, $linkedFields){
        $linkedTable = new EntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields);
        return $result->current();
    }

    public function getLinkedItems($table, $linkedFields, $order = null){
        $linkedTable = new EntitiesTable($table);
        $result = $linkedTable->getEntitiesByWhere($linkedFields, $order);
        return $result;
    }

    public function toArray(){
        return $this->fields;
    }

    public function getDate($fieldName){
        $fieldValue = $this->getField($fieldName);
        if (empty($fieldValue) || $fieldValue === "0000-00-00 00:00:00.000" || $fieldValue === "0000-00-00" || $fieldValue === "" || strtoupper($fieldValue) === 'NULL') {
            return "";
        }
        return date("d.m.Y",strtotime($fieldValue));
    }

    public function getTimestampDate($fieldName){
        $fieldValue = $this->getField($fieldName);
        if($fieldValue == "0000-00-00 00:00:00")
            return "";
        return date("H:i d.m.Y",strtotime($fieldValue));
    }

    protected function _generateId(){
        $mtimeParts = explode(" ",microtime());
        $base = $mtimeParts[1];
        $mtimeParts = explode(".",$mtimeParts[0]);
        $base .= $mtimeParts[1];

        $rand1 = rand ( 1000 , 5000);
        $rand2 = rand ( 1000 , 5000);

        $base .= ($rand1+$rand2);

        $Id = sprintf('%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X', 10+$base[0], 10+$base[1],10+$base[2], 10+$base[3], 10+$base[4], 10+$base[5], 10+$base[6], 10+$base[7],10+$base[8],10+$base[9],10+$base[10],10+$base[11],
            10+$base[12],10+$base[13],10+$base[14],10+$base[15],10+$base[18],10+$base[19],10+$base[20],
            10+$base[21]);
        return $Id;
    }

     /**
     * @param $where array()
     */
    public function deleteWhere($where, $doNotSaveToExchangeTable = false)
    {
        $result = false;
        if (isset($where) && is_array($where)) {
            //Добавляем логирование удаления
            //пользователь
            $userId = "null";
            $userName = "Default";
            try {
                //добавил через единый интерфейс
                $user = \Model\Entities\isgaUsers::getAutorisateClass();
                //Вадим: Закомментил, сервис вылетал
                //if (empty($user)) {
                //    throw new \Exception('Пользовательне не аутентифицирован');
                //}
                if(isset($user)) {
                    $userId = $user->getId();
                    $userName = $user->getFieldOrSpace('Name');
                }
            } catch(\Exception $e){

            }

            //трансакция?
                $dbAdapter= \Model\Gateway\DbAdapter::getInstance();
                $driver=$dbAdapter->getDriver();
                $connection=$driver->getConnection();
                //мутка, бо протект
                $tranArray = (array)$connection;
                $isTransaction = print_r($connection,true);
                $findString = '[inTransaction:protected] => ';
                $isOffset = strpos($isTransaction, $findString);
                $inTransaction = 0;
                if($isOffset){
                    $inString = substr($isTransaction, $isOffset+strlen($findString),1);
                    $inTransaction = (int)$inString;
                }

            //До удаления запрашиваем объекты что будут удалены
            if(!$doNotSaveToExchangeTable) {
                $eTable = new \Model\Gateway\EntitiesTable($this->tableName);
                $items = $eTable->getEntitiesByWhere($where);
            }
            //удаляем
            $result =  $this->tableGateway->delete($where);

            if(!$doNotSaveToExchangeTable){
                foreach($items as $item){
                    $this->saveToExchangeTable($item->getId(),"delete");
                }
            }

            //что Записать
            $date = date('Y-m-d H:i:s (T)');
            $whereString = var_export($where,true);
            $whereString = str_replace("\n", ' ', $whereString);
            $postString = var_export($_POST,true);
            $postString = str_replace("\n", ' ', $postString);
            $msgArray = array();
            $msgArray['date']  = $date;
            $msgArray['requestUri'] = $_SERVER['REQUEST_URI'];
            $msgArray['userId'] = $userId;
            $msgArray['userName'] = $userName;
            $msgArray['result'] = (int)$result;
            $msgArray['tableName'] = $this->tableName;
            $msgArray['tableId'] = (string)$this->getId();
            $msgArray['isTransaction'] = !empty($inTransaction)?'через транзакцию':'без транзакции';
            $msgArray['where'] = $whereString;
            $msgArray['post'] = $postString;
            $msg = implode("\t", $msgArray);
            //куда
            $fileName ='public/log/entitiy_delete.log';
            $fileName  = $_SERVER['DOCUMENT_ROOT'].'/'.$fileName;
            $pathArray = explode('/', $fileName );
            array_pop ($pathArray );
            $pathDir = implode('/', $pathArray);
            if(!is_dir($pathDir)){
                if(!mkdir($pathDir)){
                    throw new \Exception('Не смогло создать папку для логирвания');
                }
            }
            $f = fopen($fileName, 'a');
            if (!empty($f)) {
                fwrite($f, $msg.PHP_EOL);
                fclose($f);
            }else{
                throw new \Exception('Не смогло записать лок при удалении');
            }
        }
        return $result;
    }

    /**
     * Сложный запрос с указанием имени таблицы, массива требуемых полей, условия запроса и т.д.
     * @param $table_name Имя таблицы для выборки
     * @param $array_fields массив с именами требуемых полей
     * @param $where условие запроса, массив или строка
     * @param $group Групировка по полю
     * @param $order сортировка по полю
     * @param $limit кол-во записей
     * @param $page страница записей
     * @param $left_table для Джоина таблиц имя таблицы
     * @param $left_where условие джоина
     * @param $left_column поля сджойненой таблицы
     * @param $type_join тип джоина
     */
    public function selectComplicated($table_name,$array_fields,$where,$group=NULL,$order=NULL,$limit=NULL,$page=NULL,$left_table=NULL,$left_where=NULL,$left_column=NULL, $type_join = NULL) {
        $dbAdapter = DbAdapter::getInstance();
        if(count($array_fields)==0){
            $array_fields=array('*');
        }
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from($table_name);
        $select->columns($array_fields);
        $select->where($where);
        if($limit!==NULL && $page!==NULL){
            $select->limit($limit);
            $offset=$limit*$page;
            $select->offset($offset);
        }
        if($left_table!=NULL){
            $select->join($left_table, $left_where, $left_column, $type_join);
        }

        if($group!=NULL)
        {
            $select->group($group);
        }
        if($order!=NULL)
        {
            $select->order($order);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);

        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    /**
     * Сложный запрос с указанием имени таблицы, массива требуемых полей, условия запроса и т.д.
     * @param $table_name Имя таблицы для выборки
     * @param $array_fields массив с именами требуемых полей
     * @param $where условие запроса, массив или строка
     * @param $group Групировка по полю
     * @param $order сортировка по полю
     * @param $limit кол-во записей
     * @param $page страница записей
     * @param $left_tables для Джоина таблиц - массив джоинов с именем таблиц, условием связывания, полями и типом
     */
    public function selectComplicatedManyJoin($table_name,$array_fields,$where,$group=NULL,$order=NULL,$limit=NULL,$page=NULL,$left_tables=NULL) {
        $dbAdapter = DbAdapter::getInstance();
        if(count($array_fields)==0){
            $array_fields=array('*');
        }
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from($table_name);
        $select->columns($array_fields);
        $select->where($where);
        if($limit!==NULL && $page!==NULL){
            $select->limit($limit);
            $offset=$limit*$page;
            $select->offset($offset);
        }
        if($left_tables!=NULL){
            foreach($left_tables as $left){
                if(isset($left['name']) && isset($left['where']))
                    $select->join(
                        $left['name'],
                        $left['where'],
                        isset($left['column'])?$left['column']:array(),
                        isset($left['type'])?$left['type']:NULL
                    );
            }
        }

        if($group!=NULL)
        {
            $select->group($group);
        }
        if($order!=NULL)
        {
            $select->order($order);
        }
        $selectString = $sql->getSqlStringForSqlObject($select);

        $results = $dbAdapter->query($selectString, $dbAdapter::QUERY_MODE_EXECUTE);
        return $results;
    }

    /**
     * Метод убирает черточки из Id
     * @param $id
     * @return return
     */
    public function stripIdDashes($id){
        if(count(explode("-",$id)) > 0)
            return implode("",explode("-",$id));
        else
            return $id;
    }

    public function fillIdDashes($id){
        $subject = $id;
        $pattern = '/^(.{8})(.{4})(.{4})(.{4})(.+)$/i';
        preg_match($pattern, $subject, $matches);
        array_shift($matches);

        return implode("-",$matches);
    }

    /**
     * Метод аозвращает true если поле не задано и false если задано
     * @param $fieldName
     * @return boolean
     */
    public function isEmptyField($fieldName){
        if(
            !isset($this->fields[$fieldName])
            || empty($this->fields[$fieldName])
            || $this->fields[$fieldName] == ""
            || $this->fields[$fieldName] == "null"
            || $this->fields[$fieldName] == "NULL"
        )
            return true;
        else
            return false;
    }

    public function getId(){
        return $this->getField($this->idName);
    }
    
    public function getIdName(){
        return $this->idName;
    }    

    protected function saveToExchangeTable($id, $saveType){
        $et = new \Model\Gateway\EntitiesTable("isga_ExchangeTable");
        $fields = array(
            "SaveType" => $saveType,
            "TableName" => $this->tableName,
            "ObjectId" => $id,
            "Source" => "vuzi"
        );
        $gateway = $et->getGateway();
        $gateway->insert($fields);
    }

    /**
     * возращает Ентри по ид
     *
     * @param type $idValue - значение id искомой сущности
     * @param type $idField - имя поля идентификатор
     * @return type
     */
    public function getById($idValue, $idField = null){
        if(empty($idField)){
            $idField = $this->idName;
        }
        $result = $this->getByWhere(array($idField => $idValue));
        return $result;
    }
    public function getByWhere($where){
        $result = $this->getLinkedItem($this->tableName,$where);
        return $result;
    }
    public function getAllByWhere($where){
        $result = $this->getLinkedItems($this->tableName,$where);
        return $result->getObjectsArray();
    }
    public function getAllById($idValue, $idField = null){
        if(empty($idField)){
            $idField = $this->idName;
        }
        $result = $this->getAllByWhere(array($idField => $idValue));
        return $result;
    }

    /**
     * Метод проверяет если этот объект пустой, например запрашивали по айди из бд, а ничего не нашло
     */
    public function isEmpty(){
        if($this->getField($this->idName) == null)
            return true;
        return false;
    }
    /**
     * Делает запрос и ракидывает массив по полю $fieldName
     *
     * @param type $where - запрос
     * @param type $fieldName - поле по которому ассоциировать
     * @return type
     */
    public function getAllByWhereParseId($where, $fieldName = null){
        $result = $this->getLinkedItems($this->tableName,$where);
        $result = $result->getObjectsArray();
        $resultOut=array();
        if(empty($fieldName)){
            $fieldName = $this->idName;
        }
        if(!empty($result)){
            foreach ( $result as $value ) {
                $resultOut[$value->getField($fieldName)] = $value;
            }
        }
        return $resultOut;
    }

        /**
     * Получает данные по условиям из массива
     * @param array $condition
     * @return resultSet
     */
    public function getData_ArrayCondition(array $condition){

        $gateway = $this->getGateway();
        $adapter = $gateway->getAdapter();
        $entities = clone $this;
        $resultSetPrototype = new VResultSet();
        $resultSetPrototype->setArrayObjectPrototype($entities);
        $newGateway = new \Zend\Db\TableGateway\TableGateway( $gateway->getTable(), $adapter, null, $resultSetPrototype);

        $where = new \Zend\Db\Sql\Where();
        $expression = $this->getArrayToPredicate($condition);
        $where->addPredicate( $expression );

        $resultSet = $newGateway->select(
          function(\Zend\Db\Sql\Select $select) use ($where) {
            $select->where($where);
        });
        return $resultSet;
    }

    public function getGateway(){
        return $this->tableGateway;
    }

    /**
     * Вернуть выражение
     * Берет массив формата описанного в методе класса WhereConditionGenerator, и возвращает предикат
     * @param array $Arr
     * @return predicate expression
     */
    public function getArrayToPredicate(array $Arr){
        $whereCondition = new WhereConditionGenerator();
        $whereCondition->add( $Arr );
        return $whereCondition->getPredicate();
    }

    /**
     * Установить поле безоасно
     * @param type $fieldName
     * @param type $value
     */
    public function setFieldSafe($fieldName, $value) {
        if(!empty($value)){
            $value = strip_tags($value);
        }
        $this->setField($fieldName, $value);
    }

    /**
     * безопасное сохранение массива полей
     * @param type $fields
     */
    public function setFieldsSafe($fields) {
        foreach ( $fields as $fieldName => $value ) {
            $this->setFieldSafe($fieldName, $value);
        }
    }

    //last string
}