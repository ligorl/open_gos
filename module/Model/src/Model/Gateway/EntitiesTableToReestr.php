<?php

namespace Model\Gateway;

use Zend\Db\TableGateway\TableGateway;

class EntitiesTableToReestr{

    protected $tableGateway = null;

    /**
     * Конструктор
     * @param $tableName - имя таблицы с которой будет работать шлюз
     */
    public function __construct($tableName){
        // Consumes the configuration array
        $dbAdapter = CertReestrDbAdapter::getInstance();
        //Устанавливаем в виде набора объектов какого класса будет возвращаться ответ
        $resultSetPrototype = new VResultSet();

        $classNameParts = explode("_",$tableName);
        $className = implode("",$classNameParts);

        $entityClassName = "Model\\CertReestrEntities\\".$className;
        $resultSetPrototype->setArrayObjectPrototype(new $entityClassName());
        //Инициируем шлюз для работы с бд
        $this->tableGateway = new TableGateway($tableName, $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * Метод получает все записи таблицы
     * @param $paging - если задано, то вернет сущности для определенной страницы
     *                  Формат:
     *                    array(
     *                      "page" => номер страницы
     *                      "onPage" => количество сущностей на странице
     *                    )
     * @param order - сортировка, например 'id ASC';
     * @param where-массив с полями и значениями;
     * @return ResultSet
     */
    public function getAllEntities($paging = null,$order = null,$where=null, $showQuery = false){
        $tg = $this->tableGateway;
        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($paging, $order,$where,$tg, $showQuery) {

            if(isset($where)){
                $select->where($where);
            }
            if(isset($order)){
                $select->order($order);
            }

            if(isset($paging) && isset($paging["onPage"])){
                $page = 0;
                if(isset($paging["page"]))
                    $page = $paging["page"]-1;

                $select
                    ->limit($paging["onPage"])
                    ->offset($page*$paging["onPage"]);
            }
            if($showQuery){
                $sql = $tg->getSql();
                print_r($sql->getSqlstringForSqlObject($select));die;
            }
        });
        return $resultSet;
    }

    /**
     * Метод возвращает сущность по id
     *
     * @param $idValue - значение id искомой сущности
     * @param string $idField - имя поля идентификатора, если отлично от Id
     * @return ResultSet
     */
    public function getEntityById($idValue, $idField = "Id"){
        $resultSet = $this->tableGateway->select(array($idField => $idValue));
        return $resultSet->current();
    }

    /**
     * Метод возвращает набор сущностей согласно условиям
     *
     * @param $where - массив, типа array("Name" => "Вадим")
     * @return ResultSet
     */
    public function getEntitiesByWhere($where, $order = null){
        //$tg = $this->tableGateway;
        if( $where instanceof \Zend\Db\Sql\Select){
            $result = $this->tableGateway->selectWith($where);
        }
        else{
            $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($where, $order) {
                if(isset($where)){
                    if( $where instanceof \Zend\Db\Sql\Select){
                        $select = $where;
                    }
                    else{
                        $select->where($where);
                    }
                }
                if(isset($order)){
                    $select->order($order);
                }
                //$sql = $tg->getSql();
                //print_r($sql->getSqlstringForSqlObject($select));
            });
            $result = $this->tableGateway->select($where);
        }
        return $result;
    }

    /**
     * Метод для отправки кастомных селектов
     */
    public function select($function){
        $resultSet = $this->tableGateway->select($function);
        return $resultSet;
    }

    /**
     * Метод для отправки кастомных селектов для вывода получившегося запроса
     */
    public function selectTest($fields){//$function){
        //$resultSet = $this->tableGateway->select($function);
        $tb = $this->tableGateway;
        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($tb, $fields){
            $select->columns(array(
                new \Zend\Db\Sql\Expression("DISTINCT(Code) AS Code"),
                new \Zend\Db\Sql\Expression("MAX(Id) AS Id"),
                new \Zend\Db\Sql\Expression("MAX(Name) AS Name"),
                new \Zend\Db\Sql\Expression("MAX(fk_eiisEduProgramType) AS fk_eiisEduProgramType"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaGS) AS fk_isgaGS"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaQualification) AS fk_isgaQualification"),
                new \Zend\Db\Sql\Expression("MAX(QualificationGrade) AS QualificationGrade"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaEduStandard) AS fk_isgaEduStandard")
                )
            ) 
            ->where
            ->nest
                ->equalTo('fk_isgaEduStandard', '3D90D5F1-E8B5-45E9-8B5F-BBF9FAD47A06')
                ->or
                ->equalTo('fk_isgaEduStandard', '14DA0D98-F4BB-466C-876C-E9916C22AC9F')
                ->or
                ->equalTo('fk_isgaEduStandard', 'A08D5DE2-E283-4169-BF37-DC0F6539CC89')
            ->unnest
            ->and
            ->equalTo("fk_eiisEduProgramType", $fields['Id']);
            $select
            ->group('Code');

            $sql = $tb->getSql();
            print_r($sql->getSqlstringForSqlObject($select));die;

            //$sql = $this->tableGateway->getSql();
            //print_r($sql->getSqlstringForSqlObject($select));die;
        });
        return $resultSet;
    }

    public function getGateway(){
        return $this->tableGateway;
    }
    /**
     * метод возвращает количество записей, удовлетворяющих всем условиям
     *
     * @param null $paging
     * @param null $order
     * @param null $where
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getCount($where=null, $debug = false){
        $tg = $this->tableGateway;

        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($where, $tg, $debug) {

            $select->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if(isset($where)){
                $select->where($where);
            }

            if($debug) {
                $logger = new \Model\Logger\Logger("service",true);
                $sql = $tg->getSql();
                $logger->Log($sql->getSqlstringForSqlObject($select));
            }
        });
        try{
            return $resultSet->current()->getField("count");
        } catch (\Exception $e){
            return 0;
        }

    }
}