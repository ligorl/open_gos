<?php

namespace Model\Gateway;

use Zend\Db\TableGateway\TableGateway;

class CertReestrEntitiesTable extends EntitiesTable{

    protected $tableGateway = null;

    /**
     * Конструктор
     * @param $tableName - имя таблицы с которой будет работать шлюз
     */
    public function __construct($tableName){
        // Consumes the configuration array
        $dbAdapter = CertReestrDbAdapter::getInstance();
        //Устанавливаем в виде набора объектов какого класса будет возвращаться ответ
        $resultSetPrototype = new VResultSet();

        $classNameParts = explode("_",$tableName);
        $className = implode("",$classNameParts);

        $entityClassName = "Model\\CertReestrEntities\\".$className;
        $resultSetPrototype->setArrayObjectPrototype(new $entityClassName());
        //Инициируем шлюз для работы с бд
        $this->tableGateway = new TableGateway($tableName, $dbAdapter, null, $resultSetPrototype);
    }

}