<?php

namespace Model\Gateway;

use Zend\Db\TableGateway\TableGateway;

class LodEntitiesTable{

    protected $tableGateway = null;

    /**
     * Конструктор
     * @param $tableName - имя таблицы с которой будет работать шлюз
     */
    public function __construct($tableName){
        // Consumes the configuration array
        $dbAdapter = LodDbAdapter::getInstance();
        //Устанавливаем в виде набора объектов какого класса будет возвращаться ответ
        $resultSetPrototype = new VResultSet();

        $classNameParts = explode("_",$tableName);
        $className = implode("",$classNameParts);

        $entityClassName = "Model\\LodEntities\\".$className;
        $resultSetPrototype->setArrayObjectPrototype(new $entityClassName());

        //объект чтобы узнать имя таблицы в бд
        $sampleEntity = new $entityClassName();
        //Инициируем шлюз для работы с бд
        $this->tableGateway = new TableGateway($sampleEntity->getTableName(), $dbAdapter, null, $resultSetPrototype);
    }

    /**
     * Метод получает все записи таблицы
     * @param $paging - если задано, то вернет сущности для определенной страницы
     *                  Формат:
     *                    array(
     *                      "page" => номер страницы
     *                      "onPage" => количество сущностей на странице
     *                    )
     * @param order - сортировка, например 'id ASC';
     * @param where-массив с полями и значениями;
     * @return ResultSet
     */
    public function getAllEntities( $paging = null , $order = null , $where=null , $showQuery = false ){
        $tg = $this->tableGateway;
        
        if( $where instanceof \Zend\Db\Sql\Select  ){
            $select = $where;            
        }
        else{
            $select = new \Zend\Db\Sql\Select( $tg->getTable() );
            if(isset($where)){
                $select->where($where);
            }
            
        }
        if(isset($paging) && isset($paging["onPage"])){
            $page = 0;
            if(isset($paging["page"]))
                $page = $paging["page"]-1;

            $select
                ->limit($paging["onPage"])
                ->offset($page*$paging["onPage"]);
        }
        if($showQuery){
            $sql = $tg->getSql();
            print_r($sql->getSqlstringForSqlObject($select));die;
        }
        $resultSet = $this->tableGateway->selectWith( $select );
            
/*
            $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($paging, $order,$where, $tg, $showQuery) {

            $this->selectWith($select);    
                
                if( $where instanceof \Zend\Db\Sql\Select ){
                    $select = $where;
                }
                elseif(isset($where)){
                    $select->where($where);
                }
                if(isset($order)){
                    $select->order($order);
                }

            });
            */
        return $resultSet;
    }

    /**
     * метод возвращает количество записей, удовлетворяющих всем условиям
     *
     * @param null $paging
     * @param null $order
     * @param null $where
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function getCount($where=null, $debug=false){
        $tg = $this->tableGateway;        
        
        if( $where instanceof \Zend\Db\Sql\Select  ){
            $select = $where;
        }
        else{
            $select = new \Zend\Db\Sql\Select( $tg->getTable() );
            if(isset($where)){
                $select->where($where);
            }
        }

        $select->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));


        if($debug) {
            $logger = new \Model\Logger\Logger("service",true);
            $sql = $tg->getSql();
            $logger->Log($sql->getSqlstringForSqlObject($select));
        }
        $resultSet = $this->tableGateway->selectWith( $select );

           /* 
        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($where, $tg, $debug) {

            $select->columns(array('count' => new \Zend\Db\Sql\Expression('COUNT(*)')));

            if(isset($where)){
                $select->where($where);
            }

            if($debug) {
                $logger = new \Model\Logger\Logger("service",true);
                $sql = $tg->getSql();
                $logger->Log($sql->getSqlstringForSqlObject($select));
            }
        });
        */
        try{
            $result = $resultSet->current()->getField("count");
            if(!$result) return 0;
            else return $result;
        } catch (\Exception $e){
            return 0;
        }

    }

    /**
     * Метод возвращает сущность по id
     *
     * @param $idValue - значение id искомой сущности
     * @param string $idField - имя поля идентификатора, если отлично от Id
     * @return ResultSet
     */
    public function getEntityById($idValue, $idField = "Id"){
        $resultSet = $this->tableGateway->select(array($idField => $idValue));
        return $resultSet->current();
    }

    public function getEntityByWhere($where){
        $resultSet = $this->tableGateway->select($where);
        return $resultSet->current();
    }

    /**
     * Метод возвращает набор сущностей согласно условиям
     *
     * @param $where - массив, типа array("Name" => "Вадим")
     * @return ResultSet
     */
    public function getEntitiesByWhere($where){
/*
        $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($where) {


            if(isset($where)){
                $select->where($where);
            }


            $sql = $this->tableGateway->getSql();
            print_r($sql->getSqlstringForSqlObject($select));
        });
*/
        $result = $this->tableGateway->select($where);

        return $result;
    }

    /**
     * Метод для отправки кастомных селектов
     */
    public function select($function){
        $resultSet = $this->tableGateway->select($function);
        return $resultSet;
    }

    /**
     * Метод для отправки кастомных селектов для вывода получившегося запроса
     */
    public function selectTest($fields){//$function){
        //$resultSet = $this->tableGateway->select($function);
        $tb = $this->tableGateway;
        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($tb, $fields){
            $select->columns(array(
                new \Zend\Db\Sql\Expression("DISTINCT(Code) AS Code"),
                new \Zend\Db\Sql\Expression("MAX(Id) AS Id"),
                new \Zend\Db\Sql\Expression("MAX(Name) AS Name"),
                new \Zend\Db\Sql\Expression("MAX(fk_eiisEduProgramType) AS fk_eiisEduProgramType"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaGS) AS fk_isgaGS"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaQualification) AS fk_isgaQualification"),
                new \Zend\Db\Sql\Expression("MAX(QualificationGrade) AS QualificationGrade"),
                new \Zend\Db\Sql\Expression("MAX(fk_isgaEduStandard) AS fk_isgaEduStandard")
                )
            ) 
            ->where
            ->nest
                ->equalTo('fk_isgaEduStandard', '3D90D5F1-E8B5-45E9-8B5F-BBF9FAD47A06')
                ->or
                ->equalTo('fk_isgaEduStandard', '14DA0D98-F4BB-466C-876C-E9916C22AC9F')
                ->or
                ->equalTo('fk_isgaEduStandard', 'A08D5DE2-E283-4169-BF37-DC0F6539CC89')
            ->unnest
            ->and
            ->equalTo("fk_eiisEduProgramType", $fields['Id']);
            $select
            ->group('Code');

            $sql = $tb->getSql();
            print_r($sql->getSqlstringForSqlObject($select));die;

            //$sql = $this->tableGateway->getSql();
            //print_r($sql->getSqlstringForSqlObject($select));die;
        });
        return $resultSet;
    }

    public function getGateway(){
        return $this->tableGateway;
    }

    /*
     * getLikeConcat
     * поиск по канкатонации столбцов и Like выражений
     * Чтобы икать по множеству столбцов один Like используйте конструкции типа
     * [
     *      'field_1' => '%Search%',
     *      'field_2' => '',
     *      ...
     * ]
     * на выходе будет выражение типа:
     * SELECT * FROM `table` WHERE CONCAT(field_1,field_2) LIKE '%Search%'
     * @param
     * $fieldLikeArray = [
     *      $fieldName => $likeString,
     *      ...
     * ];
     * $additionalCondition = дополнительный фильтр, значение типа string в формате sql условия
     * пример: "fied='value'"
     * @return resultSet
     */
    public function getLikeConcat(array $fieldLikeArray, $additionalCondition = ''){
        $tableGateway = $this->getGateway();
        $sql = $tableGateway->getSql();
        $select = $sql->select();
        $where = new \Zend\Db\Sql\Where();

        $fieldStrConcat = '';
        $likeStrConcat = '';
        $counter = 0;

        foreach($fieldLikeArray as $fieldName=>$likeStr){
            
           if( $counter > 0){
               $fieldStrConcat .= ',';
           }
           $fieldStrConcat .= $fieldName;
           $likeStrConcat .= $likeStr;
           $counter++;
        }
        $expression = '';

        if( $additionalCondition != '' && gettype($additionalCondition) == 'string'){
            $tempStr = $additionalCondition;
            $additionalCondition = ' and '.$tempStr;
        }
        
        if( $counter<2 ){
            $expression =new \Zend\Db\Sql\Predicate\Expression($fieldStrConcat." LIKE '".$likeStrConcat."' ".$additionalCondition);
        }else{
            $expression = new \Zend\Db\Sql\Predicate\Expression('CONCAT('.$fieldStrConcat.')'." LIKE '".$likeStrConcat."' ".$additionalCondition);
        }
        $where->addPredicate($expression);

        $resultSet = $this->tableGateway->select(
          function(\Zend\Db\Sql\Select $select) use ($where, $tableGateway) {
            $select->where($where);
        });
        return $resultSet;
    }

    /**
     * Вернуть выражение
     * Берет массив формата описанного в методе класса WhereConditionGenerator, и возвращает предикат
     * @param array $Arr
     * @return predicate expression
     */
    public function getArrayToPredicate(array $Arr){
        $whereCondition = new WhereConditionGenerator();
        $whereCondition->add( $Arr );
        return $whereCondition->getPredicate();
    }

    /**
     * Получает данные по условиям из массива
     * @param array $condition
     * @return resultSet
     */
    public function getData_ArrayCondition(array $condition){
        $where = new \Zend\Db\Sql\Where();
        $expression = $this->getArrayToPredicate($condition);
        $where->addPredicate( $expression );


        \Zend\Debug\Debug::dump($this->getGateway()->getResultSetPrototype());
        
        $resultSet = $this->getGateway()->select(
          function(\Zend\Db\Sql\Select $select) use ($where) {
            $select->where($where);
        });
        return $resultSet;
    }

    /**
     * Получает количество по условиям из массива
     * @param array $condition
     * @return resultSet
     */
    public function getDataCount_ArrayCondition(array $condition){
        $tableGateway = $this->getGateway();
        $where = new \Zend\Db\Sql\Where();
        $expression = $this->getArrayToPredicate($condition);
        $where->addPredicate( $expression );
        $resultSet = $this->tableGateway->select(
          function(\Zend\Db\Sql\Select $select) use ($where, $tableGateway) {
            $select->columns(array('num' => new \Zend\Db\Sql\Expression('COUNT(*)')))->where($where);
        });
        
        if( !empty( $resultSet->getObjectsArray())){
            $result = $resultSet->getObjectsArray();
            foreach ( $result as $count){
                return $count->getField('num');
            }
        }else{
            return 0;
        }
        
    }

    /*
     *Получить выбранные поля, из выбранных таблиц, используя условный массив
     * @param
     * $columnArray = []
     * $joinArray = [
        'table' => 'table_name',
        'binder' => 'table1.col = table2.col',
        'type' => 'left||right...'
     * ]
     * $arrayCondition = []
     * $entityMod = модифицированный файл ответа
     *
     * @return ResultSet
     */
    public function getData_customFieldAndJoin_arrayCondition( $columnArray, $joinArray ,$conditionArray ,$entityMod){

        $expression = $this->getArrayToPredicate($conditionArray);
        $where = new \Zend\Db\Sql\Where();
        $where->addPredicate( $expression );
        
       
        $gateway = $this->getGateway();
        $adapter = $gateway->getAdapter();
        $resultSetPrototype = new VResultSet();
        $entityClassName = "Model\\Entities\\".$entityMod;
        $resultSetPrototype->setArrayObjectPrototype(new $entityClassName());
        $newGateway = new TableGateway($gateway->getTable(), $adapter, null, $resultSetPrototype);

        $resultSet = $newGateway->select(
            function(\Zend\Db\Sql\Select $select) use ($where, $columnArray, $joinArray) {
                $select
                  
                    ->columns($columnArray)

                        ->join(
                            $joinArray['table'],
                            $joinArray['binder'],
                            [],
                            $joinArray['type']
                          )

                                        ->where($where);
                
        });
        return $resultSet;
    }

    public function delete($where){
        $gateway = $this->getGateway();
        $gateway->delete($where);
    }
}