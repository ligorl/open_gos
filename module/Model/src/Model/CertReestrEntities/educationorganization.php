<?php
namespace Model\CertReestrEntities;

use Model\Gateway;

class educationorganization extends Gateway\CertReestrBaseEntity{
    protected $tableName = "educationorganization";
    protected $idName = "_id";

    //====================== Статические методы
    public static function getByGUID($guid){
        $table = new Gateway\CertReestrEntitiesTable("educationorganization");
        $result = $table->getEntitiesByWhere([
            "Id" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}