<?php
namespace Model\CertReestrEntities;

use Model\Gateway;

class certificatestatus extends Gateway\CertReestrBaseEntity{
    protected $tableName = "certificatestatus";
    protected $idName = "_id";

    //====================== Статические методы
    public static function getByName($name){
        $table = new Gateway\CertReestrEntitiesTable("certificatestatus");
        $result = $table->getEntitiesByWhere([
            "serviceName" => $name
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}