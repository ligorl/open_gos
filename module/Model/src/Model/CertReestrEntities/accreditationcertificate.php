<?php
namespace Model\CertReestrEntities;

use Model\Gateway;

class accreditationcertificate extends Gateway\CertReestrBaseEntity{
    protected $tableName = "accreditationcertificate";
    protected $idName = "_id";

    //====================== ����������� ������
    public static function getByGUID($guid){
        $table = new Gateway\CertReestrEntitiesTable("accreditationcertificate");
        $result = $table->getEntitiesByWhere([
            "Id" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

    public function getToExcerpt( $orgId, $notId) {        
        $results = $this->getGateway()->select(function(\Zend\Db\Sql\Select $select) use ( $orgId, $notId){
            $select
                ->where
                ->notEqualTo('_id',$notId)
                ->equalTo('EduOrgId',$orgId);
            $select->order('IssueDate DESC');
        });
        $arr = [];
        
        foreach($results as $el){
            $cert = new accreditationcertificate();
            $cert->setFields($el,true);
            $arr[] = $cert;
        }
        return $arr;
    }
}