<?php
namespace Model\CertReestrEntities;

use Model\Gateway;

class certificatesupplementstatus extends Gateway\CertReestrBaseEntity{
    protected $tableName = "certificatesupplementstatus";
    protected $idName = "_id";

    //====================== Статические методы
    public static function getByGUID($guid){
        $table = new Gateway\CertReestrEntitiesTable("certificatesupplementstatus");
        $result = $table->getEntitiesByWhere([
            "Id" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }


}