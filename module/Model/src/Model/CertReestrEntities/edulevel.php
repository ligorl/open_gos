<?php
namespace Model\CertReestrEntities;

use Model\Gateway;

class edulevel extends Gateway\CertReestrBaseEntity{
    protected $tableName = "edulevel";
    protected $idName = "_id";

    //====================== Статические методы
    public static function getByName($name){
        $table = new Gateway\CertReestrEntitiesTable("edulevel");
        $result = $table->getEntitiesByWhere([
            "Name" => $name
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
}