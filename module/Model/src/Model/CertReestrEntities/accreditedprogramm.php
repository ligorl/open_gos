<?php
namespace Model\CertReestrEntities;

use Model\Gateway;

class accreditedprogramm extends Gateway\CertReestrBaseEntity{
    protected $tableName = "accreditedprogramm";
    protected $idName = "_id";

    //====================== ����������� ������
    public static function getByGUID($guid){
        $table = new Gateway\CertReestrEntitiesTable("accreditedprogramm");
        $result = $table->getEntitiesByWhere([
            "Id" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }
    
    public function getProgramsByDefaultSupId($guid){
        $table = new Gateway\CertReestrEntitiesTable("accreditedprogramm");
        $result = $table->getEntitiesByWhere([
            "CertificateSupplementId" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result;
        else
            return [];
    }
}