<?php
namespace Model\CertReestrEntities;

use Model\Gateway;

class certificatesupplement extends Gateway\CertReestrBaseEntity{
    protected $tableName = "certificatesupplement";
    protected $idName = "_id";

    //====================== ����������� ������
    public static function getByGUID($guid){
        $table = new Gateway\CertReestrEntitiesTable("certificatesupplement");
        $result = $table->getEntitiesByWhere([
            "id" => $guid
        ])->getObjectsArray();

        if(count($result) > 0)
            return $result[0];
        else
            return null;
    }

    // ======================= �� ����������� ������
    public function getCertificate(){
        $cert= \Model\CertReestrEntities\accreditationcertificate::getByGUID($this->getField("CertificateId"));
        if(!$cert->isEmpty())
            return $cert;
        else
            return null;
    }
    
    /**
     * Метод получает все записи таблицы
     * @param $paging - если задано, то вернет сущности для определенной страницы
     *                  Формат:
     *                    array(
     *                      "page" => номер страницы
     *                      "onPage" => количество сущностей на странице
     *                    )
     * @param order - сортировка, например 'id ASC';
     * @param where-массив с полями и значениями;
     * @return ResultSet
     */
    public function getAllEntities($paging = null,$order = null,$where=null, $showQuery = false){
        $tg = $this->tableGateway;
        $resultSet = $this->tableGateway->select(function(\Zend\Db\Sql\Select $select) use ($paging, $order,$where,$tg, $showQuery) {

            if(isset($where)){
                $select->where($where);
            }
            if(isset($order)){
                $select->order($order);
            }

            if(isset($paging) && isset($paging["onPage"])){
                $page = 0;
                if(isset($paging["page"]))
                    $page = $paging["page"]-1;

                $select
                    ->limit($paging["onPage"])
                    ->offset($page*$paging["onPage"]);
            }
            if($showQuery){
                $sql = $tg->getSql();
                print_r($sql->getSqlstringForSqlObject($select));die;
            }
        });
        
        $ret = [];
        
        foreach ($resultSet as $value) {
            $sup = new certificatesupplement(); 
            $sup->setFieldsSafe($value);
            $ret[] = $sup;
        }
        
        return $ret;
    }

}