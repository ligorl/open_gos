<?php

namespace Model\Logger;

use PDO;

class Logger
{
    private $fileName;

    public function __construct($fileName=null, $prefix = false){
        $date = new \DateTime();
        if($fileName==null)
            $fileName = $date->format("y-m-d").".log";
        if($fileName != null && $prefix){
            $fileName = $fileName."_".$date->format("y-m-d").".log";
        }

        $this->fileName = $fileName;
    }

    private function _getLogger(){
        try {
            $pathToFile = __DIR__ . "/../../../../../service_logs/";


            $writer = new \Zend\Log\Writer\Stream($pathToFile . $this->fileName);
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);

            return $logger;
        } catch(\Exception $e){
            //echo $e->getMessage();
            //echo $e->getTraceAsString();
            return null;
        }
    }

    public function Log($text){
        try {
            $logger = $this->_getLogger();
            if($logger != null)
                $logger->info($text);
        } catch(\Exception $e){

        }
    }

    public function Error($text){
        try {
            $logger = $this->_getLogger();
            if($logger != null)
                $logger->err($text);
        } catch(\Exception $e){

        }
    }


    public function text($text){
        $filePath = __DIR__."/../../../../../service_logs/";
        $fp = fopen($filePath . $this->fileName, 'a');
        fwrite($fp,$text);
        fclose($fp);
    }
}