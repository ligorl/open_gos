<?php

namespace Model\EiisService;

/**
 * Класс для раскидывания сущностей из ЕИИС сервиса по бд
 *
 * Class ServiceEntities
 * @package Model\EiisService
 */
class ServiceEntities
{

    protected $logger;
    protected $serviceClient = null;

    protected $debug = true;
    //Именованный массив где хранятся соответствия между бд и тем, что присылает сервер
    //ну типа чтобы правильно все распихать по бд
    protected $correspondences = array(
        "EIIS.REGIONS"          => array(
            "class"  => "eiisRegions",
            "fields" => array(
                "ID"         => "Id",
                "NAME"       => "Name",
                "FED_OKR_FK" => "fk_eiisFedOkr",
                "REGION"     => "Code"
            )
        ),
        "EIIS.COUNTRIES"        => array(
            "class"  => "eiisCountries",
            "fields" => array(
                'ID'       => 'Id',
                'NAME'     => 'Name',
                'FULLNAME' => 'FullName',
                'CODE2'    => 'Code2',
                'CODE3'    => 'Code3'
            )
        ),
        "EIIS.FED_OKR"          => array(
            "class"  => "eiisFedOkr",
            "fields" => array(
                "ID"        => "Id",
                "NAME"      => "Name",
                "SHORTNAME" => "ShortName"
            )
        ),
        "EIIS.CONTROL_ORGANS"   => array(
            "class"      => "eiisControlOrgans",
            "fields"     => array(
                'ID'            => 'Id',
                'NAME'          => 'Name',
                'OGRN'          => 'Ogrn',
                'LAW_ADDRESS'   => 'LawAddress',
                'FAKT_ADDRESS'  => 'FaktAddress',
                'URL'           => 'Url',
                'INN'           => 'Inn',
                'KPP'           => 'Kpp',
                'BIK'           => 'Bik',
                'KORSCH'        => 'KorSch',
                'RASSCH'        => 'RasSch',
                'BANKNAME'      => 'BankName',
                'BANKPLACE'     => 'BankPlace',
                'DSTARTACTIV'   => 'DateStartActive',
                'CHECKFIN_DATE' => 'DateLastCheck',
                'REGIONSFK'     => 'fk_eiisRegions',
            ),
            "dateFields" => array(
                "DateStartActive",
                'DateLastCheck'
            ),
        ),
        "EIIS.EDUPROGRAM_TYPES" => array(
            "class"  => "eiisEduProgramTypes",
            "fields" => array(
                'ID'                  => 'Id',
                'NAME'                => 'Name',
                'SHORTNAME'           => 'ShortName',
                'EDU_PROGRAM_SUBTYPE' => 'EduProgramSubtype',
            )
        ),
        "EIIS.EDULEVELS"        => array(
            "class"  => "eiisEduLevels",
            "fields" => array(
                'ID'               => 'Id',
                'NAME'             => 'Name',
                'SHORTNAME'        => 'ShortName',
                'EDU_PROGRAM_KIND' => 'EduProgramKind',
            )
        ),
        "EIIS.EDUPROGRAMS"      => array(
            "class"      => "eiisEduPrograms",
            "fields"     => array(
                'ID'                 => 'Id',
                "UGSCODE"            => 'UgsCode',
                'NAME'               => 'Name',
                'EDUPROGRAMTYPEFK'   => 'fk_eiisEduProgramType',
                'GS_FK'              => 'fk_eiisGS',
                'QUALIFICATIONGRADE' => 'QualificationGrade',
                'PERIOD'             => 'Period',
                'RANGE_TARIFF'       => 'RangeTariff',
                'STANDARD_TYPE'      => 'StandardType',
                'NOT_TRUE_PROGRAM'   => 'NotTrueProgram',
                'EDU_NOTE'           => 'EduNote',
                'EDU_DIRECTORY'      => 'fk_eiisEduDirectory',
                'EDULEVELFK'         => 'fk_eiisEduLevels',
                'UGSNAME'            => 'UgsName',
                'QUALIFICATIONCODE'  => 'QualificationCode',
                'QUALIFICATIONNAME'  => 'QualificationName',
                'UGS_FK'             => "fk_eiisUgs",
                'QUALIFICATION_FK'   => "fk_isgaQualification",
                'EDU_STANDARD_FK'    => "fk_isgaEduStandard",
                "CODE"               => "Code"
            ),
            "boolFields" => array(
                "NotTrueProgram"
            )
        ),
        "ISLOD.NEW_EDUPROGRAMS" => array(
            "class"      => "eiisEduPrograms",
            "fields"     => array(
                'ID'               => 'Id',
                'CODE'             => 'Code',
                'NAME'             => 'Name',
                'EDULEVELFK'       => 'fk_eiisEduLevels',
                'EDUPROGRAMTYPEFK' => 'fk_eiisEduProgramType',
                'STANDARD_TYPE'    => 'StandardType',
                "UGSCODE"          => 'UgsCode',
                'UGSNAME'          => 'UgsName',
                'UGS_FK'           => "fk_eiisUgs",
                'PERIOD'           => 'Period',
                'NOT_TRUE_PROGRAM' => 'NotTrueProgram'
            ),
            "boolFields" => array(
                "NotTrueProgram"
            ),
            "custom"     => array(
                'Code'
            )
        ),
        "EIIS.SCHOOLS"                                                 => array(
            "class"      => "eiisEducationalOrganizations",
            "fields"     => array(
                "ID"                    => "Id",
                'ISLOD_GUID'            => "IslodGuid",
                "NAME"                  => "FullName",
                'SHORTNAME'             => "ShortName",
                'REGULARNAME'           => "RegularName",
                'SCHOOL_PROPERTYFK'     => "fk_eiisEducationalOrganizationProperties",
                'SCHOOL_TYPEFK'         => "fk_eiisEducationalOrganizationType",
                'SCHOOL_KINDFK'         => "fk_eiisEducationalOrganizationKind",
                'SCHOOL_CATEGORYFK'     => "fk_eiisEducationalOrganizationCategory",
                'BRANCH'                => "Branch",
                'PARENTFK'              => "fk_eiisParentEducationalOrganization",
                'HASMILITARYDEPARTMENT' => "HasMilitaryDepartment",
                'HASHOSTEL'             => "HasHostel",
                'HOSTELCAPACITY'        => "HostelCapacity",
                'HASHOSTELFORENTRANTS'  => "HasHostelForEntrants",
                'LAW_ADDRESS'           => "LawAddress",
                'LAW_POST_INDEX'        => "LawPostIndex",
                'LAW_COUNTRYFK'         => "fk_eiisLawCountry",
                'LAW_REGIONFK'          => "fk_eiisLawRegion",
                'LAW_TOWNTYPEFK'        => "fk_eiisLawTownType",
                'LAW_CITY_NAME'         => "LawCity",
                'LAW_STREET'            => "LawStreet",
                'LAW_HOUSE'             => "LawHouse",
                'LAW_OFFICE'            => "LawOffice",
                'ADDRESS'               => "Address",
                'POST_INDEX'            => "PostIndex",
                'COUNTRYFK'             => "fk_eiisCountry",
                'REGIONFK'              => "fk_eiisRegion",
                'TOWNTYPEFK'            => "fk_eiisTownType",
                'TOWN_NAME'             => "TownName",
                'STREET'                => "Street",
                'HOUSE'                 => "House",
                'OFFICE'                => "Office",
                'PHONES'                => "Phones",
                'FAXS'                  => "Faxes",
                'MAILS'                 => "Mails",
                'WWW'                   => "Www",
                'GOSREGNUM'             => "GosRegNum",
                'INN'                   => "Inn",
                'KPP'                   => "Kpp",
                'CHARGEPOSITION'        => "ChargePosition",
                'CHARGEFIO'             => "ChargeFio",
                'CONTACT_FIRST_NAME'    => "ContactFirstName",
                'CONTACT_SECOND_NAME'   => "ContactSecondName",
                'CONTACT_LAST_NAME'     => "ContactLastName",
                'STUDENTS_COUNT'        => "StudentsCount",
                'SCHOOLLEAVER_COUNT'    => "EOLeaverCount",
                'DATELASTCHECK'         => "DateLastCheck",
                'LICENSE_REG_NUM'       => "LicenseRegNum",
                'LICENSE_DATE_BEGIN'    => "LicenseDateBegin",
                'LICENSE_DATE_END'      => "LicenseDateEnd",
                'SVAKKR_REG_NUM'        => "SvakkrRegNum",
                'SVAKKR_DATE_BEGIN'     => "SvakkrDateBegin",
                'SVAKKR_DATE_END'       => "SvakkrDateEnd",
                'LICENSE_ADDRESS'       => "LicenseAddress",
                'OUTDATED'              => "Outdated",
                'EX_SCHOOLFK'           => "fk_eiisExEducationalOrganization",
                'IMPL_ADDR'             => "ImplAddr",
                'ADDRESS2'              => "Address2"
            ),
            "dateFields" => array(
                "DateLastCheck",
                "LicenseDateBegin",
                "LicenseDateEnd",
                "SvakkrDateBegin",
                "SvakkrDateEnd"
            ),
            "boolFields" => array(
                "Branch"
            )
        ),
        "EIIS.LICENSES"                                                => array(
            "class"          => "eiisLicenses",
            "fields"         => array(
                "ID"                         => "Id",
                'REGIONFK'                   => "fk_eiisRegion",
                'REGION_LOCATION'            => "fk_eiisRegion_Location",
                'LICENSE_REG_NUM'            => "LicenseRegNum",
                'SERDOC'                     => "SerDoc",
                'NUMDOC'                     => "NumDoc",
                'STATEFK'                    => "fk_eiisLicenseState",
                'ISTERMLESS'                 => "IsTermless",
                'DATA_END'                   => "DateEnd",
                'CONTROL_ORGAN_FK'           => "fk_eiisControlOrgan",
                'SCHOOLFK'                   => "fk_eiisEducationalOrganization",
                'ORGNAME'                    => "OrgName",
                'ORGNAME_INSTR_CASE'         => "OrgNameInstrCase",
                'SCHOOLNAME'                 => "EOName",
                'SCHOOLNAME_DAT_CASE'        => "EONameDatCase",
                'REGULARNAME'                => "EORegularName",
                'SHORTNAME'                  => "EOShortName",
                'SCHOOLPROPERTYNAME'         => "EOPropertyName",
                'LAWADDRESS'                 => "EOLawAddress",
                'GOSREGNUM'                  => "EOGosRegNum",
                'INN'                        => "EOInn",
                'COMMENTLIC'                 => "CommentLic",
                'MANAGERFIO'                 => "ManagerFio",
                'FILEFK'                     => "File",
                'BASISTYPEDOCFK'             => "fk_eiisBaseDocTypes",
                'NUMLICDOC'                  => "NumLicDoc",
                'DATELICDOC'                 => "DateLicDoc",
                'LICENSESTATFK'              => "fk_eiisLicenseState_Old",
                'LICENSEOLDFK'               => "fk_eiisLicense_Old",
                'COMMENTDOC'                 => "CommentDoc",
                'POST'                       => "Post",
                'REG_DATE'                   => "DateReg",
                'SENDING_DATE'               => "DateSending",
                'ON_HANDS_DATE'              => "DateOnHands",
                'PRINTED_DATETIME'           => "DatetimePrinted",
                'TO_THE_SIGNATURE_DATETIME'  => "DatetimeToTheSignature",
                'SIGNED_BY_MANAGER_DATETIME' => "DatetimeSignedByManager",
                'SCANNED_DATETIME'           => "DatetimeScanned",
                'FOLDED_DATETIME'            => "DatetimeFolded",
                'SIGNED_BY_HEAD_DATETIME'    => "DatetimeSignedByHead",
                'REASON_OF_SUSPENSION'       => "ReasonOfSuspension",
                'DATE_OF_SUSPENSION'         => "DateOfSuspension",
                'NUM_DATE_DUPL_ISSUE'        => "NumDateDuplIssue",
                'ADM_SUSP_ORDERS'            => "AdmSuspOrders",
                'ORG_SUSP_DECISIONS'         => "OrgSuspDecisions",
                'CHECK_REASONS_DATES'        => "CheckReasonsDates",
                'COURT_REVOKING_DECISIONS'   => "CourtRevokingDecisions",
                'ADDITIONAL_INFORMATION'     => "AdditionalInformation",
                'DATE_OF_NEW_EVENT'          => "DateOfNewEvent",
                'NEW_EVENT'                  => "NewEvent",
                'SEND_IN_ROS'                => "IsSendInRos",
                'IND_ISSUE'                  => "IndIssue",
                'DOUBLE'                     => "IsDouble",
                'COPY_LICENSE_STATFK'        => "fk_eiisCopyBasedOnApplication"
            ),
            "dateFields"     => array(
                "DateEnd",
                "DateLicDoc",
                "DateReg",
                "DateSending",
                "DateOnHands",
                "DateOfSuspension",
                "DateOfNewEvent"
            ),
            "datetimeFields" => array(
                "DatetimePrinted",
                "DatetimeToTheSignature",
                "DatetimeSignedByManager",
                "DatetimeScanned",
                "DatetimeFolded",
                "DatetimeSignedByHead",
            ),
            "boolFields"     => array(
                "IsTermless"
            )
        ),
        "EIIS.SCHOOL_KINDS"                                            => array(
            "class"          => "eiisEducationalOrganizationKinds",
            "fields"         => array(
                "ID"            => "Id",
                'CODE'          => "Code",
                'NAME'          => "Name",
                'SCHOOL_TYPEFK' => "fk_eiisEducationalOrganizationType"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.SCHOOL_CATEGORIES"                                       => array(
            "class"          => "eiisEducationalOrganizationCategories",
            "fields"         => array(
                "ID"   => "Id",
                'CODE' => "Code",
                'NAME' => "Name"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.SCHOOLPROPERTIES"                                        => array(
            "class"          => "eiisEducationalOrganizationProperties",
            "fields"         => array(
                "ID"       => "Id",
                'CODE'     => "Code",
                'NAME'     => "Name",
                'IS_STATE' => "IsState"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            ),
            "boolFields"     => array(
                "IsState"
            )
        ),
        "EIIS.SCHOOL_TYPES"                                            => array(
            "class"          => "eiisEducationalOrganizationTypes",
            "fields"         => array(
                "ID"                 => "Id",
                'NAME'               => "Name",
                'SCHOOL_SUBTYPECODE' => "EOSubTypeCode"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.EDUDIRECTORIES"                                          => array(
            "class"          => "eiisEduDirectories",
            "fields"         => array(
                'SYS_GUID'  => "Id",
                'NAME'      => "Name",
                'SHORTNAME' => "ShortName",
                'OBSOLETE'  => "Obsolete",
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            ),
            "boolFields"     => array(
                "Obsolete"
            )
        ),
        "EIIS.LICENSED_PROGRAMS"                                       => array(
            "class"           => "eiisLicensedPrograms",
            "fields"          => array(
                "ID"                 => "Id",
                'REGION_LOCATION'    => "fk_eiisRegion_Location",
                'LICENSE_APPFK'      => "fk_eiisLicenseSupplement",
                'EDUPROGRAMFK'       => "fk_eiisEduProgram",
                'CODE'               => "Code",
                'NAME'               => "Name",
                'EDULEVELFK'         => "fk_eiisEduLevel",
                'EDUPROGRAM_TYPEFK'  => "fk_eiisEduProgramType",
                'PERIOD'             => "Period",
                'QUALIFICATIONCODE'  => "QualificationCode",
                'QUALIFICATIONNAME'  => "QualificationName",
                'QUALIFICATIONGRADE' => "QualificationGrade",
                'LICENSE_STATFK'     => "fk_eiisLicenseState",
                'OKSO'               => "OKSO",
                'STANDARD_TYPE'      => "StandardType",
                'NEW_EDUPROGRAMFK'   => "fk_eiisNewEduPrograms"
            ),
            "fieldsForUpdate" => array(
                "ID"            => "Id",
                'LICENSE_APPFK' => "fk_eiisLicenseSupplement",
            ),
            "dateFields"      => array(
            ),
            "datetimeFields"  => array(
            ),
            "custom"          => array(
                "fk_eiisNewEduPrograms"
            )
        ),
        "EIIS.LICENSE_STATES"                                          => array(
            "class"          => "eiisLicenseStates",
            "fields"         => array(
                'ID'      => "Id",
                'NAME'    => "Name",
                'WEBNAME' => "WebName"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.LICENSE_APP_STATES"                                      => array(
            "class"          => "eiisLicenseSupplementStates",
            "fields"         => array(
                'ID'      => "Id",
                'NAME'    => "Name",
                'WEBNAME' => "WebName"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.LICENSE_APPS"                                            => array(
            "class"          => "eiisLicenseSupplements",
            "fields"         => array(
                'ID'                         => "Id",
                'REGIONSFK'                  => "fk_eiisRegion",
                'REGION_LOCATION_FK'         => "fk_eiisRegion_Location",
                'LICENSEFK'                  => "fk_eiisLicense",
                'NUMBER'                     => "Number",
                'SERDOC'                     => "SerDoc",
                'NUMDOC'                     => "NumDoc",
                'BRANCH'                     => "Branch",
                'LICENSE_APP_STATEFK'        => "fk_eiisLicenseSupplementState",
                'MAXSTUDCOUNT'               => "MaxStudCount",
                'DATE_END'                   => "DateEnd",
                'COMMENTSUP'                 => "CommentSup",
                'CONTROL_ORGAN_FK'           => "fk_eiisControlOrgan",
                'ORGNAME'                    => "OrgName",
                'ORGNAME_INSTR_CASE'         => "OrgNameInstrCase",
                'MANAGERFIO'                 => "ManagerFio",
                'BANCHFK'                    => "fk_eiisBranch",
                'SCHOOLNAME'                 => "EOName",
                'SCHOOLNAME_DAT_CASE'        => "EONameDatCase",
                'REGULARNAME'                => "EORegularName",
                'SHORTNAME'                  => "EOShortName",
                'LAWADDRESS'                 => "EOLawAddress",
                'FILEFK'                     => "File",
                'BASEDOC_TYPEFK'             => "fk_eiisBaseDocType",
                'NUMLICDOC'                  => "NumLicDoc",
                'DATELICDOC'                 => "DateLicDoc",
                'LICENSE_STATFK'             => "fk_eiisLicenseState",
                'LICENSE_APPFK'              => "fk_eiisLicenseSupplementOld",
                'COMMENTDOC'                 => "CommentDoc",
                'POST'                       => "Post",
                'IMPL_ADDR'                  => "ImplAddr",
                'REG_DATE'                   => "DateReg",
                'SENDING_MAIL_DATE'          => "DateSendingMail",
                'ON_HANDS_DATE'              => "DateOnHands",
                'REASON_OF_SUSPENSION'       => "ReasonOfSuspension",
                'DATE_OF_SUSPENSION'         => "DateOfSuspension",
                'PRINTED_DATETIME'           => "DatetimePrinted",
                'TO_THE_SIGNATURE_DATETIME'  => "DatetimeToTheSignature",
                'SIGNED_BY_MANAGER_DATETIME' => "DatetimeSignedByManager",
                'SCANNED_DATETIME'           => "DatetimeScanned",
                'FOLDED_DATETIME'            => "DatetimeFolded",
                'SIGNED_BY_HEAD_DATETIME'    => "DatetimeSignedByHead",
                'ADDITIONAL_INFORMATION'     => "AdditionalInformation",
                'DATE_OF_NEW_EVENT'          => "DateOfNewEvent",
                'NEW_EVENT'                  => "NewEvent",
                'IND_ISSUE'                  => "IndIssue",
                'SORT_NUMBER'                => "SortNumber",
                'DOUBLE'                     => "Duplicate",
                'DUPLICATE_LICENSE_STATFK'   => "fk_eiisLicenseState_Duplicate",
                'DUPLICATE_ORDER'            => "DuplicateOrder",
                'DUP_LIC_STATFK'             => "fk_eiisLicenseState_Duplicate2"
            //fk_isgaApplication
            ),
            "dateFields"     => array(
                "DateEnd",
                "DateLicDoc",
                "DateReg",
                "DateSendingMail",
                "DateOnHands",
                "DateOfSuspension",
                "DateOfNewEvent",
            ),
            "datetimeFields" => array(
                "DatetimePrinted",
                "DatetimeToTheSignature",
                "DatetimeSignedByManager",
                "DatetimeScanned",
                "DatetimeFolded",
                "DatetimeSignedByHead",
            ),
            "boolFields"     => array(
                "Branch",
                "Duplicate"
            )
        ),
        "EIIS.ISGA.ACCREDITATION_KINDS"                                => array(
            "class"          => "isgaAccreditationKinds",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_DOCUMENT_TYPES"                         => array(
            "class"          => "isgaApplicationDocumentTypes",
            "fields"         => array(
                'ID'                  => "Id",
                'NAME'                => "Name",
                'CODE'                => "Code",
                'IS_FOR_APPLICANTS'   => "IsForApplicants",
                'IS_FOR_FILIALS_ONLY' => "IsForFilialsOnly",
                'ORDERING'            => "Ordering",
                'EIIS_ID'             => "EiisId",
                'UPDATE_DATE'         => "DateUpdate",
                'CREATE_DATE'         => "DateCreate",
                'FTP_CODE'            => "FtpCode"
            ),
            "dateFields"     => array(
                "DateUpdate",
                "DateCreate"
            ),
            "datetimeFields" => array(
            ),
            "boolFields"     => array(
                "IsForApplicants",
                "IsForFilialsOnly"
            )
        ),
        "EIIS.ISGA.APPLICATION_DOCUMENT_GROUP_STATUSES"                => array(
            "class"          => "isgaApplicationDocumentGroupStatuses",
            "fields"         => array(
                'ID'       => "Id",
                'NAME'     => "Name",
                'CODE'     => "Code",
                'ORDERING' => "Ordering"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.QUALIFICATIONS"                                          => array(
            "class"          => "isgaQualifications",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.ORDER_DOCUMENT_TYPES"                               => array(
            "class"          => "isgaOrderDocumentTypes",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.ORDER_DOCUMENT_STATUSES"                            => array(
            "class"          => "isgaOrderDocumentStatuses",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.ORDER_DOCUMENTS"                                    => array(
            "class"          => "isgaOrderDocuments",
            "fields"         => array(
                'ID'                   => "Id",
                'COLLEGE_FK'           => "fk_isgaCollege",
                'SIGN_DATE'            => "DateSign",
                'SIGNER_POST'          => "SignerPost",
                'EXECUTANT_POST'       => "ExecutantPost",
                'PERFORMER_POST'       => "PerformerPost",
                'TYPE_FK'              => "fk_isgaOrderDocumentType",
                'STATUS_FK'            => "fk_isgaOrderDocumentStatus",
                'SIGNER_FULL_NAME'     => "SignerFullName",
                'EXECUTANT_FULL_NAME'  => "ExecutantFullName",
                'PERFORMER_FULL_NAME'  => "PerformerFullName",
                'SIGNER_LASTNAME'      => "SignerLastName",
                'SIGNER_FIRSTNAME'     => "SignerFirstName",
                'SIGNER_PATRONYMIC'    => "SignerPatronymic",
                'EXECUTANT_LASTNAME'   => "ExecutantLastName",
                'EXECUTANT_FIRSTNAME'  => "ExecutantFirstName",
                'EXECUTANT_PATRONYMIC' => "ExecutantPatronymic",
                'PERFORMER_LASTNAME'   => "PerformerLastName",
                'PERFORMER_FIRSTNAME'  => "PerformerFirstName",
                'PERFORMER_PATRONYMIC' => "PerformerPatronymic"
            //Number
            ),
            "dateFields"     => array(
                "DateSign"
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.ORDER_DOCUMENT_KINDS"                               => array(
            "class"          => "isgaOrderDocumentKinds",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.FOUNDERS"                                                => array(
            "class"          => "isgaFounders",
            "fields"         => array(
                'ID'                     => "Id",
                'TYPE_FK'                => "fk_isgaFounderType",
                'ORGANIZATION_FULLNAME'  => "OrganizationFullName",
                'ORGANIZATION_SHORTNAME' => "OrganizationShortName",
                'LASTNAME'               => "LastName",
                'FIRSTNAME'              => "FirstName",
                'PATRONYMIC'             => "Patronymic",
                'PHONES'                 => "Phones",
                'FAXES'                  => "Faxes",
                'EMAILS'                 => "Emails",
                'OGRN'                   => "Ogrn",
                'INN'                    => "Inn",
                'KPP'                    => "Kpp",
                'L_ADDRESS'              => "LawAddress",
                'L_ADDRESS_COUNTRY_FK'   => "fk_eiisCountry_L",
                'L_ADDRESS_REGION_FK'    => "fk_eiisLawRegion",
                'L_ADDRESS_DISTRICT'     => "LawDistrict",
                'L_ADDRESS_TOWN'         => "LawTown",
                'L_ADDRESS_STREET'       => "LawStreet",
                'L_ADDRESS_HOUSE_NUMBER' => "LawHouseNumber",
                'L_ADDRESS_POSTAL_CODE'  => "LawPostalCode",
                'P_ADDRESS'              => "PAddress",
                'P_ADDRESS_COUNTRY_FK'   => "fk_eiisCountry_P",
                'P_ADDRESS_REGION_FK'    => "fk_eiisPRegion",
                'P_ADDRESS_DISTRICT'     => "PDistrict",
                'P_ADDRESS_TOWN'         => "PTown",
                'P_ADDRESS_STREET'       => "PStreet",
                'P_ADDRESS_HOUSE_NUMBER' => "PHouseNumber",
                'P_ADDRESS_POSTAL_CODE'  => "PPostalCode"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.FOUNDER_TYPES"                                           => array(
            "class"          => "isgaFounderTypes",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.GS"                                                      => array(
            "class"          => "isgaGroupSpecialities",
            "fields"         => array(
                'ID'     => "Id",
                'NAME'   => "Name",
                'CODE'   => "Code",
                'UGS_FK' => "fk_isgaEGS"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.FOUNDERS_EDU_INSTITUTIONS"                               => array(
            "class"          => "isgamtmFoundersEducationalOrganizations",
            "fields"         => array(
                'ID'        => "Id",
                'SCHOOL_FK' => "fk_eiisEducationalOrganization",
                'FONDER_FK' => "fk_isgaFounder",
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_REASONS_DOCUMENT_TYPES"                 => array(
            "class"          => "isgamtmDocumentReason",
            "fields"         => array(
                'ID'               => "Id",
                'DOCUMENT_TYPE_FK' => "fk_isgaDocumentTypes",
                'REASON_FK'        => "fk_isgaApplicationReasons"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.UGS"                                                     => array(
            "class"          => "isgaEnlargedGroupSpecialities",
            "fields"         => array(
                'ID'           => "Id",
                'NAME'         => "Name",
                'CODE'         => "Code",
                'EDU_LEVEL_FK' => "fk_eiisEduLevel",
                'EDITION_FK'   => "fk_isgaEGSEdition",
                'STANDART'     => "Standart"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.UGS_EDITIONS"                                            => array(
            "class"          => "isgaEnlargedGroupSpecialityEditions",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.EDU_STANDARDS"                                           => array(
            "class"          => "isgaEduStandards",
            "fields"         => array(
                'ID'           => "Id",
                'NAME'         => "Name",
                'ABBREVIATION' => "Abbreviation"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.BASEDOC_TYPES"                                           => array(
            "class"          => "isgaBaseDocTypes",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.COLLEGE_STATUSES"                                   => array(
            "class"          => "isgaCollegeStatuses",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.COLLEGES"                                           => array(
            "class"          => "isgaColleges",
            "fields"         => array(
                'ID'                    => "Id",
                'NUMBER'                => "Number",
                'COLLEGE_DATE'          => "DateCollege",
                'ADDRESS'               => "Address",
                'PROTOCOL_DATE'         => "DateProtocol",
                'STATUS_FK'             => "fk_isgaCollegeStatus",
                'XML_PACKAGE'           => "XmlPackage",
                'XML_PACKAGE_FILE_NAME' => "XmlPackageFileName"
            ),
            "dateFields"     => array(
                "DateCollege",
                "DateProtocol"
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.CHECK_STATUSES"                                          => array(
            "class"          => "isgaCheckStatuses",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.CERTIFICATE_TYPES"                                  => array(
            "class"          => "isgaCertificateTypes",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.CERTIFICATE_SUPPLEMENT_STATUSES"                    => array(
            "class"          => "isgaCertificateSupplementStatuses",
            "fields"         => array(
                'ID'          => "Id",
                'NAME'        => "Name",
                'CODE'        => "Code",
                'CREATE_DATE' => "DateCreate",
                'UPDATE_DATE' => "DateUpdate",
                'EIIS_ID'     => "EiisId"
            ),
            "dateFields"     => array(
                "DateCreate",
                "DateUpdate"
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.CERTIFICATE_SUPPLEMENTS"                            => array(
            "class"          => "isgaCertificateSupplements",
            "fields"         => array(
                'ID'                              => "Id",
                'CERTIFICATE_FK'                  => "fk_isgaCertificate",
                'INSTITUTION_FK'                  => "fk_eiisEducationalOrganization",
                'DECISION_DATE'                   => "DateDecision",
                'DECISION_REASON'                 => "DecisionReason",
                'END_DATE'                        => "DateEnd",
                'TERMINATION_REASON'              => "TerminationReason",
                'ADDITIONAL_INFO'                 => "AdditionalInfo",
                'INSTITUTION_L_ADDRESS_REGION_FK' => "fk_Region_L",
                'INSTITUTION_FULL_NAME'           => "EOFullName",
                'INSTITUTION_SHORT_NAME'          => "EOShortName",
                'INSTITUTION_TYPE_FK'             => "fk_EOType",
                'INSTITUTION_KIND_FK'             => "fk_EOKind",
                'ORG_FORM_FK'                     => "fk_EOProperty",
                'INN'                             => "EOInn",
                'NUMBER'                          => "Number",
                'STATUS_FK'                       => "fk_isgaCertificateSupplementStatus",
                'DECISION_DOCUMENT_FK'            => "fk_isgaOrderDocument",
                'ISSUE_DATE'                      => "DateIssue",
                'FORM_NUMBER'                     => "FormNumber",
                'SERIAL_NUMBER'                   => "SerialNumber",
                'CONTROL_ORGAN_FK'                => "fk_eiisControlOrgan",
                'INSTITUTION_L_ADDRESS'           => "EOAddressL",
                'CONTROL_ORGAN_HEAD_FULL_NAME'    => "ControlOrganHeadFullName",
                'CONTROL_ORGAN_HEAD_POST'         => "ControlOrganHeadPost",
                'ORDER_DOCUMENT_NUMBER'           => "OrderDocumentNumber",
                'ORDER_DOCUMENT_SIGN_DATE'        => "DateOrderDocumentSign",
                'APPLICATION_FK'                  => "fk_isgaApplication",
                'NOTE'                            => "Note",
                'ORDER_DOCUMENT_KIND_FK'          => "fk_isgaOrderDocumentKind",
                'ACCREDITATION_KIND_FK'           => "fk_isgaAccreditationKind",
                'SUPPLEMENT_FILE'                 => "SupplementFile",
                'SUPPLEMENT_FILE_NAME'            => "SupplementFileName",
                'ISSUE_TO_APPLICANT_DATE'         => "DateIssueToApplicant",
                'CHECK_DATE'                      => "DateCheck",
                'CHECKER_FK'                      => "Checker",
                'PRINT_DATE'                      => "DatePrint",
                'COLLEGE_FK'                      => "fk_isgaCollege",
                'CHECK_STATUS_FK'                 => "fk_isgaCheckStatus",
                'READY_TO_ISSUE_DATE'             => "DateReadyToIssue",
                'RAA_ID'                          => "RaaId",
                'EIIS_ID'                         => "EiisId",
                'UPDATE_DATE'                     => "DateUpdate",
                'CREATE_DATE'                     => "DateCreate",
                'SPOILED_FORMS_COUNT'             => "SpoiledFormsCount",
                'SPOILED_FORMS_DATA'              => "SpoiledFormsData"
            ),
            "dateFields"     => array(
                "DateIssue",
                "DateIssueToApplicant",
                "DateCheck",
                "DatePrint",
                "DateReadyToIssue",
                "DateDecision",
                "DateEnd",
                "DateOrderDocumentSign",
                "DateUpdate",
                "DateCreate"
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.CERTIFICATE_STATUSES"                               => array(
            "class"          => "isgaCertificateStatuses",
            "fields"         => array(
                'ID'          => "Id",
                'NAME'        => "Name",
                'CODE'        => "Code",
                'CREATE_DATE' => "DateCreate",
                'UPDATE_DATE' => "DateUpdate",
                'EIIS_ID'     => "EiisId"
            ),
            "dateFields"     => array(
                "DateCreate",
                "DateUpdate",
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.CERTIFICATES"                                       => array(
            "class"          => "isgaCertificates",
            "fields"         => array(
                'ID'                              => "Id",
                'REG_NUMBER'                      => "RegNumber",
                'ISSUE_DATE'                      => "DateIssue",
                'END_DATE'                        => "DateEnd",
                'FORM_NUMBER'                     => "FormNumber",
                'SERIAL_NUMBER'                   => "SerialNumber",
                'INSTITUTION_L_ADDRESS_REGION_FK' => "fk_Region_EOAddressL",
                'INSTITUTION_FULL_NAME'           => "EOFullName",
                'INSTITUTION_SHORT_NAME'          => "EOShortName",
                'INSTITUTION_TYPE_FK'             => "fk_EOType",
                'INSTITUTION_KIND_FK'             => "fk_EOKind",
                'ORG_FORM_FK'                     => "fk_EOProperty",
                'INN'                             => "Inn",
                'APPLICATION_FK'                  => "fk_oisgaApplication",
                'CREATE_DATE'                     => "DateCreate",
                'UPDATE_DATE'                     => "DateUpdate",
                'INSTITUTION_FK'                  => "fk_eiisEducationalOrganization",
                'STATUS_FK'                       => "fk_isgaCertificateStatus",
                'TYPE_FK'                         => "fk_isgaCertificateType",
                'INSTITUTION_L_ADDRESS'           => "EOAddressL",
                'CONTROL_ORGAN_HEAD_FULL_NAME'    => "ControlOrganHeadFullName",
                'CONTROL_ORGAN_HEAD_POST'         => "ControlOrganHeadPost",
                'ORDER_DOCUMENT_NUMBER'           => "OrderDocumentNumber",
                'ORDER_DOCUMENT_SIGN_DATE'        => "DateOrderDocumentSign",
                'NOTE'                            => "Note",
                'ORDER_DOCUMENT_KIND_FK'          => "fk_OrderDocumentKind",
                'CERTIFICATE_FILE'                => "CertificateFile",
                'CERTIFICATE_FILE_NAME'           => "CertificateFileName",
                'ISSUE_TO_APPLICANT_DATE'         => "DateIssueToApplicant",
                'CHECK_DATE'                      => "DateCheck",
                'READY_TO_ISSUE_DATE'             => "DateReadyToIssue",
                'PRINT_DATE'                      => "DatePrint",
                'COLLEGE_FK'                      => "fk_isgaCollege",
                'CHECK_STATUS_FK'                 => "fk_isgaCheckStatus",
                'RAA_ID'                          => "RaaId",
                'SPOILED_FORMS_COUNT'             => "SpoiledFormsCount",
                'SPOILED_FORMS_DATA'              => "SpoiledFormsData",
            //fk_isgaOrderDocument
            //fk_eiisControlOrgan
            ),
            "dateFields"     => array(
                "DateIssue",
                "DateEnd",
                "DateIssueToApplicant",
                "DateCheck",
                "DateReadyToIssue",
                "DatePrint",
                "DateCreate",
                "DateUpdate",
                "DateOrderDocumentSign"
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_TYPES"                                  => array(
            "class"          => "isgaApplicationTypes",
            "fields"         => array(
                'ID'                    => "Id",
                'NAME'                  => "Name",
                'CODE'                  => "Code",
                'SHORT_NAME'            => "ShortName",
                'NAME_ACCUSATIVE_CASE'  => "NameAccusativeCase",
                'CODE_FOR_NOTIFICATION' => "CodeForNotification",
                'ORDERING'              => "Ordering",
                'NAME_GENITIVE_CASE'    => "NameGenitiveCase"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_STATUSES"                               => array(
            "class"          => "isgaApplicationStatuses",
            "fields"         => array(
                'ID'                             => "Id",
                'CODE'                           => "Code",
                'ORDERING'                       => "Ordering",
                'SHOW_CERTIFICATE'               => "ShowCertificate",
                'SHOW_COLLEGE'                   => "ShowCollege",
                'ALLOW_SELECT_COLLEGE'           => "AllowSelectCollege",
                'ALLOW_EDIT_FIELDS'              => "AllowEditFields",
                'ALLOW_EDIT_APPLICATION_MEMBERS' => "AllowEditFormMembers",
                'ALLOW_EDIT_DOCUMENTS'           => "AllowEditDocuments",
                'ALLOW_EDIT_ACCREDITED_PROGRAMS' => "AllowEditAccreditedPrograms",
                'ALLOW_DELETE'                   => "AllowDelete",
                'NAME'                           => "Name",
                'PUBLISH_NAME'                   => "PublishName",
                'DESCRIPTION'                    => "Description",
                'PUBLISHED'                      => "Published",
                'ALLOW_REVERSE_TRANSITIONS'      => "AllowReverseTransitions",
                'ISGA_ALLOW_PUBLICATION'         => "IsgaAllowPublication",
                'ISGA_TEXT_FOR_PUBLICATION'      => "IsgaTextForPublication"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            ),
            "boolFields"     => array(
                "ShowCertificate",
                "ShowCollege",
                "AllowSelectCollege",
                "AllowEditFields",
                "AllowEditFormMembers",
                "AllowEditDocuments",
                "AllowEditAccreditedPrograms",
                "AllowDelete",
                "Published",
                "AllowReverseTransitions",
                "IsgaAllowPublication"
            )
        ),
        "EIIS.ISGA.APPLICATIONS"                                       => array(
            "class"          => "isgaApplications",
            "fields"         => array(
                'ID'                                          => "Id",
                'REG_NUMBER'                                  => "RegNumber",
                'SUBMISSION_DATE'                             => "DateSubmission",
                'APPLICANT_PERSON_POST'                       => "ApplicantPersonPost",
                'REG_DATE'                                    => "DateReg",
                'PGU_ID'                                      => "PguId",
                'STATUS_FK'                                   => "fk_isgaApplicationStatus",
                'REASON_FK'                                   => "fk_isgaApplicationReason",
                'SUBMISSION_TYPE_FK'                          => "fk_isgaApplicationSubmissionType",
                'COLLEGE_FK'                                  => "fk_isgaCollege",
                'COLLEGE_RESULT_FK'                           => "fk_isgaCollegeResult",
                'ORDER_DOCUMENT_FK'                           => "fk_isgaOrderDocument",
                'REFUSAL_TYPE_FK'                             => "fk_ApplicationRefusalType",
                'PREVIOUS_CERTIFICATE_FK'                     => "fk_isgaPreviousCertificate",
                'APPLICANT_PERSON_FULL_NAME'                  => "ApplicantPersonFullName",
                'APPLICANT_PERSON_FIRSTNAME'                  => "ApplicantPersonFirstName",
                'APPLICANT_PERSON_LASTNAME'                   => "ApplicantPersonLastName",
                'APPLICANT_PERSON_PATRONYMIC'                 => "ApplicantPersonPatronymic",
                'BRING_PERSON_FULL_NAME'                      => "BringPersonFullName",
                'PREVIOUS_CERTIFICATE_REG_NUMBER'             => "PreviousCertificateRegNumber",
                'PREVIOUS_CERTIFICATE_FORM_NUMBER'            => "PreviousCertificateFormNumber",
                'PREVIOUS_CERTIFICATE_SERIAL_NUMBER'          => "PreviousCertificateSerialNumber",
                'PREVIOUS_CERTIFICATE_ISSUE_DATE'             => "PreviousCertificateDateIssue",
                'PREVIOUS_CERTIFICATE_END_DATE'               => "PreviousCertificateDateEnd",
                'PREVIOUS_CERTIFICATE_INSTITUTION_SHORT_NAME' => "PreviousCertificateEOShortName",
                'PREVIOUS_CERTIFICATE_INSTITUTION_FULL_NAME'  => "PreviousCertificateEOFullName",
                'PREVIOUS_CERTIFICATE_CONTROL_ORGAN_FK'       => "fk_ControlOrgan_PreviousCertificate",
                'PREVIOUS_CERTIFICATE_STATUS_FK'              => "fk_isgaCertificateStatus_PreviousCertificate",
                'PREVIOUS_CERTIFICATE_TYPE_FK'                => "fk_isgaCertificateType_PreviousCertificate",
                'DECLARED_ORG_FORM_FK'                        => "fk_eiisEOProperty_Declared",
                'DECLARED_TYPE_FK'                            => "fk_eiisEOType_Declared",
                'DECLARED_KIND_FK'                            => "fk_eiisEOKind_Declared",
                'DECLARED_REGULAR_NAME'                       => "DeclaredRegularName",
                'DECLARED_SHORT_NAME'                         => "DeclaredShortName",
                'ORDER_DOCUMENT_FILE'                         => "OrderDocumentFile",
                'ORDER_DOCUMENT_FILE_NAME'                    => "OrderDocumentFileName",
                'ORDER_DOCUMENT_KIND_FK'                      => "fk_OrderDocumentKind",
                'ORDER_DOCUMENT_NUMBER'                       => "OrderDocumentNumber",
                'ORDER_DOCUMENT_SIGN_DATE'                    => "DateOrderDocumentSign",
                'ACCEPTANCE_TRANSFER_ACT_NUMBER'              => "AcceptanceTransferActNumber",
                'ACCEPTANCE_TRANSFER_ACT_DATE'                => "DateAcceptanceTransferAct",
                'RECALL_APPLICATION_REG_NUMBER'               => "RecallApplicationRegNumber",
                'RECALL_APPLICATION_SCAN'                     => "RecallApplicationScan",
                'RECALL_APPLICATION_SCAN_FILE_NAME'           => "RecallApplicationScanFileName",
                'RECALL_DATE'                                 => "DateRecall",
                'RECALL_DOCUMENTS_SEND_DATE'                  => "DateRecallDocumentsSend",
                'RECALL_APPLICATION_SUBMISSION_DATE'          => "DateRecallApplicationSubmission",
                'CREATE_DATE'                                 => "DateCreate"
            //fk_isgaApplicationType
            //fk_eiisEducationalOrganization
            //fk_eiisLicense
            ),
            "dateFields"     => array(
                "DateSubmission",
                "DateReg",
                "DateAcceptanceTransferAct",
                "DateRecall",
                "DateCreate",
                "DateRecallDocumentsSend",
                "PreviousCertificateDateIssue",
                "PreviousCertificateDateEnd",
                "DateOrderDocumentSign"
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_REASONS"                                => array(
            "class"          => "isgaApplicationReasons",
            "fields"         => array(
                'ID'                                     => "Id",
                'APPLICATION_TYPE_FK'                    => "fk_isgaApplicationType",
                'CODE'                                   => "Code",
                'NAME'                                   => "Name",
                'ORDERING'                               => "Ordering",
                'SHORT_NAME'                             => "ShortName",
                'CREATE_DEFAULT_APPLICANT'               => "CreateDefaultApplicant",
                'NAME_PREPOSITIONAL_CASE'                => "NamePrepositionalCase",
                'APPLICATION_PROGRAMS_SELECTION_MODE_FK' => "fk_isgaApplicationProgramsSelectionMode",
                'APPLICATION_MEMBERS_SELECTION_MODE_FK'  => "fk_isgaApplicationMembersSelectionMode",
                'SHOW_COLLEGE'                           => "ShowCollege",
                'ALLOW_SEND_TO_RAA'                      => "AllowSendToRaa",
                'SHOW_PREVIOUS_CERTIFICATE'              => "ShowPreviousCertificate",
                'GROUP_CODE'                             => "GroupCode"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_SUBMISSION_TYPES"                       => array(
            "class"          => "isgaApplicationSubmissionTypes",
            "fields"         => array(
                'ID'           => "Id",
                'NAME'         => "Name",
                'IS_AUTOMATIC' => "IsAutomatic"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            ),
            "boolFields"     => array(
                "IsAutomatic"
            )
        ),
        "EIIS.ISGA.COLLEGE_RESULTS"                                    => array(
            "class"          => "isgaCollegeResults",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.REFUSAL_TYPES"                                      => array(
            "class"          => "isgaRefusalTypes",
            "fields"         => array(
                'ID'                => "Id",
                'NAME'              => "Name",
                'CODE'              => "Code",
                'NOTIFICATION_TEXT' => "NotificationText"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_PROGRAMS_SELECTION_MODES"               => array(
            "class"          => "isgaApplicationProgramsSelectionModes",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_MEMBERS_SELECTION_MODES"                => array(
            "class"          => "isgaApplicationMembersSelectionModes",
            "fields"         => array(
                'ID'   => "Id",
                'NAME' => "Name",
                'CODE' => "Code"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.EDU_OBJECT_CHECK_STATUSES"                          => array(
            "class"          => "isgaEduObjectCheckStatuses",
            "fields"         => array(
                'ID'      => "Id",
                'NAME'    => "Name",
                'EIIS_ID' => "EiisId"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.ORDER_DOCUMENTS_LINK_ACCRED_PROGRAMS"               => array(
            "class"          => "isgamtmOrderDocumentAccreditedProgram",
            "fields"         => array(
                'ID'                => "Id",
                'ORDER_DOCUMENT_FK' => "fk_isgaOrderDocument",
                'ACCRED_PROGRAM_FK' => "fk_isgaAccreditedProgram"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.ADDITIONAL_ACCREDITED_PROGRAMS"                     => array(
            "class"          => "isgamtmAccreditedProgramsLicensedProgram",
            "fields"         => array(
                'ID'                    => "Id",
                'ACCREDITED_PROGRAM_FK' => "fk_AccreditedProgram",
                'LICENSED_PROGRAM_FK'   => "fk_LicensedProgram"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.APPLICATION_DOCUMENT_GROUP_STATUSES_DOCUMENT_TYPES" => array(
            "class"          => "isgamtmAppDocumentTypesAppDocumentGroupStatuses",
            "fields"         => array(
                'ID'        => "Id",
                'TYPE_FK'   => "fk_isgaApplicationDocumentType",
                'STATUS_FK' => "fk_isgaApplicationDocumentGroupStatus"
            ),
            "dateFields"     => array(
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.REGULATIONS"                                        => array(
            "class"          => "isgaRegulations",
            "fields"         => array(
                'ID'                     => "Id",
                'NAME'                   => "Name",
                'NAME_INSTRUMENTAL_CASE' => "NameInstrumentalCase",
                'ARTICLE'                => "Article",
                'DOCUMENT_DATE'          => "DateDocument",
                'DOCUMENT_NUMBER'        => "DocumentNumber"
            ),
            "dateFields"     => array(
                "DateDocument"
            ),
            "datetimeFields" => array(
            )
        ),
        "EIIS.ISGA.ACCREDITED_PROGRAMS"                                => array(
            "class"             => "isgaAccreditedPrograms",
            "fields"            => array(
                "ID"                        => "Id",
                "APPLICATION_FK"            => "fk_oisgaApplication",
                "LICENSED_PROGRAM_FK"       => "fk_eiisLicensedProgram",
                "CERTIFICATE_SUPPLEMENT_FK" => "fk_isgaCertificateSupplement",
                "SCHOOL_FK"                 => "fk_eiisEducationalOrganization",
                "CODE"                      => "Code",
                "NAME"                      => "Name",
                "EDUPROGRAM_FK"             => "fk_eiisEduProgram",
                "EDUPROGRAM_TYPE_FK"        => "fk_eiisEduProgramType",
                "OKSO_CODE"                 => "OksoCode",
                "START_YEAR"                => "StartYear",
                "EDU_NORMATIVE_PERIOD"      => "EduNormativePeriod",
                "FORM_DEGREE"               => "FormDegree",
                "QUALIFICATION_FK"          => "fk_isgaQualification",
                "GS_FK"                     => "fk_isgaGroupSpeciality",
                "UGS_FK"                    => "fk_isgaEnlargedGroupSpeciality",
                "IS_ACCREDITED"             => "IsNotAccredited",
                "IS_UGS"                    => "IsSpecialitiesGroup"
            ),
            "dateFields"        => array(
                "DateLicenseEnd",
                "DateCertificateEnd",
                "DateCreate",
                "DateUpdate"
            ),
            "datetimeFields"    => array(
            ),
            "boolFields"        => array(
                "LicenseIsUnlimited",
                "HasAdditionalAccreditedProgram",
                "IsSpecialitiesGroup"
            ),
            "inverseBoolFields" => array(
                "IsNotAccredited"
            )
        ),
        "EIIS.F_LICENSES" => array(
            "db" => "lic_reestr",
            "class" => "tempfLicenses",
            "fields" => array(
                "ID" => "Id",
                "LicOrganFk" => "LicOrganFk",
                "LicOrgan" => "LicOrgan",
                "FullOrgName" => "FullOrgName",
                "ShortOrgName" => "ShortOrgName",
                "NameIP" => "NameIP",
                "OGRN" => "OGRN",
                "INN" => "INN",
                "KPP" => "KPP",
                "RegionFk" => "RegionFk",
                "Region" => "Region",
                "Address" => "Address",
                "LicDate" => "LicDate",
                "LicNumber" => "LicNumber",
                "LicBlankSer" => "LicBlankSer",
                "LicBlankNum" => "LicBlankNum",
                "LicEndDate" => "LicEndDate",
                "IsDubl" => "IsDubl",
                "DocTypeName" => "DocTypeName",
                "DocNumber" => "DocNumber",
                "DocDate" => "DocDate",
                "LicStatusFk" => "LicStatusFk",
                "LicStatus" => "LicStatus",
                "AdmSuspInfo" => "AdmSuspInfo",
                "SuspResumpDecisions" => "SuspResumpDecisions",
                "TerminateBase" => "TerminateBase",
                "TerminateDate" => "TerminateDate",
                "InspectionInfo" => "InspectionInfo",
                "AnnulDecision" => "AnnulDecision",
                "SchoolFk" => "SchoolFk",
                "IndividualFk" => "IndividualFk",
                "LicenseDocXML" => "LicenseDocXML",
                "SignFile" => "SignFile"
            ),
            "dateFields" => array(
                "LicDate",
                "TerminateDate",
                "DocDate",
                "LicEndDate"
            ),
            "datetimeFields" => array(
            ),
            "boolFields" => array(
                "IsDubl"
            )
        ),
        "EIIS.F_SUPPLEMENTS" => array(
            "db" => "lic_reestr",
            "class" => "tempfSupplements",
            "fields" => array(
                "ID" => "Id",
                "LicenseFk" => "LicenseFk",
                "SupNumber" => "SupNumber",
                "SupBlankSer" => "SupBlankSer",
                "SupBlankNum" => "SupBlankNum",
                "FullOrgName" => "FullOrgName",
                "ShortOrgName" => "ShortOrgName",
                "Address" => "Address",
                "ImplAddresses" => "ImplAddresses",
                "DocTypeName" => "DocTypeName",
                "DocNumber" => "DocNumber",
                "DocDate" => "DocDate",
                "IsDubl" => "IsDubl",
                "SupStatusFk" => "SupStatusFk",
                "SupStatus" => "SupStatus",
                "TerminateBase" => "TerminateBase",
                "TerminateDate" => "TerminateDate",
                "SchoolFk" => "SchoolFk",
                "IndividualFk" => "IndividualFk",
                "SupDocXML" => "SupDocXML",
                "SignFile" => "SignFile"
            ),
            "dateFields" => array(
                "DocDate",
                "TerminateDate"
            ),
            "datetimeFields" => array(
            ),
            "boolFields" => array(
                "IsDubl"
            )
        ),
        "EIIS.F_DIRECTIONS" => array(
            "db" => "lic_reestr",
            "class" => "tempfDirections",
            "fields" => array(
                "ID" => "Id",
                "SupplementFk" => "SupplementFk",
                "LevelFk" => "LevelFk",
                "Level" => "Level",
                "Code" => "Code",
                "Name" => "Name",
                "Kind" => "Kind",
                "Qualification" => "Qualification",
                "ProgramFk" => "ProgramFk"
            ),
            "dateFields" => array(
            ),
            "datetimeFields" => array(
            ),
            "boolFields" => array(
            )
        ),

        "EIIS.F_CERTIFICATES" => array(
            "db" => "reestr_new",
            "class" => "tempfCertificates",
            "fields" => array(
                "ID"            => "Id",
                "AkkredOrgan"   => "AkkredOrgan",
                "AkkredOrganFK" => "AkkredOrganFK",
                "FullOrgName"   => "FullOrgName",
                "NameIP"        => "NameIP",
                "OrgRukFio"     => "OrgRukFio",
                "OrgRukPost"    => "OrgRukPost",
                "OGRN"          => "OGRN",
                "INN"           => "INN",
                "Phone"         => "Phone",
                "Fax"           => "Fax",
                "Mail"          => "Mail",
                "WebSite"       => "WebSite",
                "RegionFk"      => "RegionFk",
                "Region"        => "Region",
                "Address"       => "Address",
                "SvidNumber"    => "SvidNumber",
                "SvidDate"      => "SvidDate",
                "SvidEndDate"   => "SvidEndDate",
                "SvidBlankSer"  => "SvidBlankSer",
                "SvidBlankNum"  => "SvidBlankNum",
                "DocTypeName"   => "DocTypeName",
                "DocNumber"     => "DocNumber",
                "DocDate"       => "DocDate",
                "SvidStatusFK"  => "SvidStatusFK",
                "SvidStatus"    => "SvidStatus",
                "OrgULFio"      => "OrgULFio",
                "OrgULPost"     => "OrgULPost",
                "SchoolFk"      => "SchoolFk",
                "SvidPeriod"    => "SvidPeriod",
                "IndividualFk"  => "IndividualFk",
                "SvidDocXML"    => "SvidDocXML",
                "SignFile"      => "SignFile"
            ),
            "dateFields" => array(
                "SvidDate",
                "SvidEndDate",
                "DocDate"
            ),
            "datetimeFields" => array(
            ),
            "boolFields" => array(
            )
        ),
        "EIIS.F_CER_SUPPLEMENTS" => array(
            "db" => "reestr_new",
            "class" => "tempfSupplements",
            "fields" => array(
                "ID"                => "Id",
                "CertificateFk"     => "CertificateFk",
                "SupNumber"         => "SupNumber",
                "SupDate"           => "SupDate",
                "SupBlankSer"       => "SupBlankSer",
                "SupBlankNum"       => "SupBlankNum",
                "FullOrgName"       => "FullOrgName",
                "DocTypeName"       => "DocTypeName",
                "DocNumber"         => "DocNumber",
                "DocDate"           => "DocDate",
                "SupStatus"         => "SupStatus",
                "SupStatusFk"       => "SupStatusFk",
                "SchoolFk"          => "SchoolFk",
                "IndividualFk" => "IndividualFk",
                "SupDocXML" => "SupDocXML",
                "SignFile" => "SignFile"
            ),
            "dateFields" => array(
                "SupDate",
                "DocDate"
            ),
            "datetimeFields" => array(
            ),
            "boolFields" => array(
            )
        ),
        "EIIS.F_CER_DIRECTIONS" => array(
            "db" => "reestr_new",
            "class" => "tempfDirections",
            "fields" => array(
                "ID"                => "Id",
                "SupplementFk"      => "SupplementFk",
                "Level"             => "Level",
                "CodeUgsn"          => "CodeUgsn",
                "NameUgsn"          => "NameUgsn",
                "CodeNp"            => "CodeNp",
                "NameNp"            => "NameNp",
                "DirectionStatus"   => "DirectionStatus"
            ),
            "dateFields" => array(
            ),
            "datetimeFields" => array(
            ),
            "boolFields" => array(
            )
        )
    );

    // Первоначальная инициализация
    public function __construct(array $params = null)
    {
        $this->serviceClient = new ServiceClient($params);
        $this->logger = new \Model\Logger\Logger();
    }

    /**
     * Метод запрашивает с сервиса данные по таблице и распихивает по бд
     * @param $serviceTableCode
     * @param $onlyNew - если true, то обрабатывает только новые записи
     */
    public function getEntitiesFromService($serviceTableCode, $sleepTime, $onlyNew = false)
    {
        //Формируем пакет
        $this->logger->Log("=============================");
        $this->logger->Log("Запрос на формирование пакета ".$serviceTableCode);
        if($this->debug) echo "\n <br> Запрос на формирование пакета ".$serviceTableCode;
        $packageId = $this->serviceClient->CreatePackage($serviceTableCode);

        //ждем чтобы пакет сформировался
        $this->logger->Log("Ожидание " . $sleepTime . " сек");
        if($this->debug) echo "\n <br> Ожидание ".$sleepTime." сек";
        sleep($sleepTime);
        if ($sleepTime > 240)
            $this->serviceClient->GetSessionId();

        $resultsCount = 0;
        //получаем количество страниц в пакете
        $this->logger->Log("Запрос на количество страниц в пакете");
        if($this->debug) echo "\n <br> Запрос на количество страниц в пакете";
        $pageCount = $this->serviceClient->GetPackageMeta($packageId);
        $this->logger->Log("Количество страниц: " . $pageCount);
        if($this->debug) echo "\n <br> Количество страниц: ".$pageCount;
        //Запрашиваем ответ по каждой странице
        for ($i = 1; $i <= $pageCount; $i++) {
            $this->logger->Log("-------");
            $this->logger->Log("Запрашиваем страницу " . $i . " пакета");
            if($this->debug) echo "\n <br> Запрашиваем страницу " . $i . " пакета";

            $results = $this->serviceClient->GetPackage($packageId, $i);
            //$this->logger->Log(print_r($results,true));
            if (0) {
                $logger = new \Model\Logger\Logger($serviceTableCode);
                $logger->text($results);
            }

            $results = new \SimpleXMLElement($results);
            $resultsCount += count($results->children());
            //Обходим результат добавляя в бд
            $this->parseResult($results, $serviceTableCode, $onlyNew);
        }
        if($this->debug) echo "\n <br> Отработало ".$i. "пакетов";
        //Отсылаем мол все отлично отработало
        $this->serviceClient->SetOk($packageId);
        if($this->debug) echo "\n <br> ".$serviceTableCode." - Все отработало, обработано объектов: ".$resultsCount;
        $this->logger->Log("Все отработало, обработано объектов: " . $resultsCount);
    }

    /**
     * Метод выводит результат запроса к сервису
     * @param $serviceTableCode
     */
    public function showEntitiesFromServer($serviceTableCode, $sleepTime)
    {
        //Формируем пакет
        $packageId = $this->serviceClient->CreatePackage($serviceTableCode);
        //ждем чтобы пакет сформировался
        sleep($sleepTime);
        //получаем количество страниц в пакете
        $pageCount = $this->serviceClient->GetPackageMeta($packageId);
        //Запрашиваем ответ по каждой странице
        for ($i = 1; $i <= $pageCount; $i++) {
            $results = $this->serviceClient->GetPackage($packageId, $i);
            echo "<br>Part " . $i . "<textarea style='width:1000px;height:400px'>";
            var_dump($results);
            echo "</textarea>";
        }
        //Отсылаем мол все отлично отработало
        $this->serviceClient->SetOk($packageId);
        echo "full done";
    }

    private function parseResult($results, $serviceTableCode, $onlyNew)
    {
        if (!isset($this->correspondences[$serviceTableCode])) {
            $this->logger->Log(sprintf('Unknown serviceTableCode %s',$serviceTableCode));
            return;
        }


        $fields = $this->correspondences[$serviceTableCode]["fields"];
        $entityClass = $this->correspondences[$serviceTableCode]["class"];


        $numb = 0;
        $newRows = 0;
        foreach ($results->children() as $row) {
            $numb++;
            $entityFields = array();
            foreach ($row->children() as $column) {
                $code = (string) $column["code"];
                if (isset($fields[$code])) {
                    //Проверяем если это поле в списке полей даты
                    if (isset($this->correspondences[$serviceTableCode]["dateFields"]) && in_array($fields[$code],
                                                                                                   $this->correspondences[$serviceTableCode]["dateFields"])) {
                        //Обрабатываем значение как дату
                        $dateObj = \DateTime::createFromFormat("d.m.Y G:i:s", (string) $column);
                        if ($dateObj != null)
                            $date = $dateObj->format('Y-m-d');
                        else
                            $date = "0000-00-00";
                        $entityFields[$fields[$code]] = $date;
                    }
                    //Проверяем если это поле в списке полей с датой и временем
                    else if (isset($this->correspondences[$serviceTableCode]["datetimeFields"]) && in_array($fields[$code],
                                                                                                            $this->correspondences[$serviceTableCode]["datetimeFields"])) {
                        //Обрабатываем значение как дату и время
                        $dateObj = \DateTime::createFromFormat("d.m.Y G:i:s", (string) $column);
                        if ($dateObj != null)
                            $datetime = $dateObj->format('Y-m-d H:i:s');
                        else
                            $datetime = "0000-00-00 00:00:00";
                        $entityFields[$fields[$code]] = $datetime;
                    }
                    //Проверяем если это поле в списке обратных логических полей
                    //Сейчас есть только одно такое поле меняет IsAccredited на IsNotAccredited
                    //Поменялась логика на ЕИИС, чтобы не менять у нас в коде
                    else if (isset($this->correspondences[$serviceTableCode]["inverseBoolFields"]) && in_array($fields[$code],
                                                                                                               $this->correspondences[$serviceTableCode]["inverseBoolFields"])) {
                        $entityFields[$fields[$code]] = 1;
                        if ((string) $column == "True")
                            $entityFields[$fields[$code]] = 0;
                    }
                    //Проверяем если это поле в списке логических полей
                    else if (isset($this->correspondences[$serviceTableCode]["boolFields"]) && in_array($fields[$code],
                                                                                                        $this->correspondences[$serviceTableCode]["boolFields"])) {
                        $entityFields[$fields[$code]] = 0;
                        if ((string) $column == "True")
                            $entityFields[$fields[$code]] = 1;
                        //кастомная обработка
                    } else if (isset($this->correspondences[$serviceTableCode]["custom"]) && in_array($fields[$code],
                                                                                                      $this->correspondences[$serviceTableCode]["custom"])) {
                        //Кастомная обработка Лиц программ
                        if ($fields[$code] == "fk_eiisNewEduPrograms" && $serviceTableCode == "EIIS.LICENSED_PROGRAMS") {
                            //Проверяем, если значение не нулевое, то утсанавливаем в первую
                            if ((string) $column != "" && (string) $column != "NULL") {
                                $entityFields["fk_eiisEduProgram"] = (string) $column;
                            }
                        }
                        //Обработка кода для новых еду программ, убираем лишнее в конце
                        if ($fields[$code] == "Code" && $serviceTableCode == "ISLOD.NEW_EDUPROGRAMS") {
                            $programCode = (string) $column;
                            $partsCode = explode(".", $programCode);
                            if (count($partsCode) > 0 && strlen($partsCode[0]) == 6) {
                                $entityFields[$fields[$code]] = $partsCode[0];
                            } else {
                                $entityFields[$fields[$code]] = $programCode;
                            }
                        }
                    } else {
                        //иначе
                        $entityFields[$fields[$code]] = (string) $column;
                    }
                }
            }
            if (!isset($entityFields["Id"]) || $this->isOurId($entityFields["Id"]))
                continue;

            $entityClassName = "Model\\Entities\\" . $entityClass;
            if(isset($this->correspondences[$serviceTableCode]["db"]) && $this->correspondences[$serviceTableCode]["db"]=="lic_reestr")
                $entityClassName = "Model\\LicReestrEntities\\" . $entityClass;

            if(isset($this->correspondences[$serviceTableCode]["db"]) && $this->correspondences[$serviceTableCode]["db"]=="reestr_new")
                $entityClassName = "Model\\CertReestrEntities\\" . $entityClass;

            $entity = new $entityClassName($entityFields["Id"]);

            $isNew = false;
            $oldfields = $entity->getFields();

            if (isset($oldfields) && (!isset($oldfields["Id"]) || $oldfields["Id"] == "")) {
                $isNew = true;
                if ($entityClass == "isgaCertificates") {
                    $entityFields["WasSentToSigning"] = 1;
                }

                //Обработка для новых Лиц программ
                if ($entityClass == "eiisLicensedPrograms") {
                    /**
                      при уровне fk_eiisEduLevel = dc01f9795be64aa19ce95c3c8ffdd57b
                      Присваивать fk_eiisEduProgramType = e02c2c478f054bd1b9d7b46ba3ceae66
                      fk_eiisEduProgram = 3215c008ca4e457bb9f2d77bf8f8eeb9
                      Name = Начальное общее образование

                     */
                    if ($entityFields["fk_eiisEduLevel"] == 'dc01f9795be64aa19ce95c3c8ffdd57b') {
                        $entityFields["fk_eiisEduProgramType"] = 'e02c2c478f054bd1b9d7b46ba3ceae66';
                        $entityFields["fk_eiisEduProgram"] = '3215c008ca4e457bb9f2d77bf8f8eeb9';
                        $entityFields["Name"] = 'Начальное общее образование';
                    }
                }
                //--

                //$this->logger->Log("Новый объект с id: " . $entityFields["Id"]);
                $newRows++;
            } else {
                if ($entityClass == "isgaCertificates") {
                    $entityFields["WasSentToSigning"] = $oldfields["WasSentToSigning"];
                }

                //Для лиц программ обновляем только два поля
                if ($entityClass == "eiisLicensedPrograms") {
                    $newEFields = array(
                        "Id"                       => $entityFields["Id"],
                        "fk_eiisLicenseSupplement" => $entityFields["fk_eiisLicenseSupplement"]
                    );
                    //Еще проверяем надо ли переписывать ссылку на еду программы
                    if (!isset($oldfields["fk_eiisEduProgram"]) || empty($oldfields["fk_eiisEduProgram"]) || strtolower($oldfields["fk_eiisEduProgram"]) == "null")
                        $newEFields["fk_eiisEduProgram"] = $entityFields["fk_eiisEduProgram"];

                    $entityFields = $newEFields;
                }
            }

            if ($onlyNew && !$isNew)
                continue;



            $entity->setFields($entityFields, $isNew);
            $entity->save($isNew, true);
        }
        $this->logger->Log("Новых объектов: " . $newRows);
        $this->logger->Log("Всего объектов: " . $numb);
    }

    /**
     * метод парсит ответ части и отдает массив именованных массивов поле => значение
     * @param $results
     * @return array
     */
    public function parseResultAsArray($results){
        $items = [];
        foreach( $results->children() as $item){
            $parsedItem = [];
            foreach($item->children() as $field){
                $fieldName = (string)$field["code"];
                $fieldValue = (string)$field;
                $parsedItem[$fieldName] = $fieldValue;
            }
            $items[] = $parsedItem;
        }
        return $items;
    }

    public function parseEntitiesFromServerDate($serviceTableCode, $sleepTime){
        //Формируем пакет
        $packageId = $this->serviceClient->CreatePackage($serviceTableCode);
        //ждем чтобы пакет сформировался
        sleep($sleepTime);
        if ($sleepTime > 240)
            $this->serviceClient->GetSessionId();

        $resultsCount = 0;
        //получаем количество страниц в пакете
        $pageCount = $this->serviceClient->GetPackageMeta($packageId);
        echo "<br>Pages Count: " . $pageCount;
        //Запрашиваем ответ по каждой странице
        for ($i = 1; $i <= $pageCount; $i++) {
            $results = $this->serviceClient->GetPackage($packageId, $i);
            $results = new \SimpleXMLElement($results);
            $resultsCount += count($results->children());
            //Обходим результат отображаем даты

            if (!isset($this->correspondences[$serviceTableCode]))
                return;

            $fields = $this->correspondences[$serviceTableCode]["fields"];
            $entityClass = $this->correspondences[$serviceTableCode]["class"];

            $numb = 0;
            foreach ($results->children() as $row) {
                $numb++;
                $entityFields = array();
                foreach ($row->children() as $column) {
                    $code = (string) $column["code"];
                    if ($code == "ID")
                        echo "<br><b>" . (string) $column . "</b>";
                    if ($code == "ISTERMLESS")
                        echo "<br><b>" . (string) $column . "</b>";
                    continue;
                    if (isset($fields[$code])) {
                        //Проверяем если это поле в списке полей даты
                        if (isset($this->correspondences[$serviceTableCode]["dateFields"]) && in_array($fields[$code],
                                                                                                       $this->correspondences[$serviceTableCode]["dateFields"])) {
                            echo "<br>" . $code . " " . (string) $column;
                            //Обрабатываем значение как дату
                            $dateObj = \DateTime::createFromFormat("d.m.Y G:i:s", (string) $column);
                            if ($dateObj != null)
                                $date = $dateObj->format('Y-m-d');
                            else
                                $date = "0000-00-00";
                            echo " --> " . $date;
                        }
                        //Проверяем если это поле в списке полей с датой и временем
                        else if (isset($this->correspondences[$serviceTableCode]["datetimeFields"]) && in_array($fields[$code],
                                                                                                                $this->correspondences[$serviceTableCode]["datetimeFields"])) {
                            echo "<br>" . $code . " " . (string) $column;
                            //Обрабатываем значение как дату и время
                            $dateObj = \DateTime::createFromFormat("d.m.Y G:i:s", (string) $column);
                            if ($dateObj != null)
                                $datetime = $dateObj->format('Y-m-d H:i:s');
                            else
                                $datetime = "0000-00-00 00:00:00";
                            echo " --> " . $datetime;
                        } else {
                            //иначе
                            //$entityFields[$fields[$code]] = (string)$column;
                        }
                    }
                }
                echo "<br>=========";
            }
            echo $numb;
        }
        //Отсылаем мол все отлично отработало
        $this->serviceClient->SetOk($packageId);
        echo "<br>full done. results: " . $resultsCount;
    }

    /**
     * Метод роверяет наш ли это айдишник
     * наш      - 0B0E100C-1310-0D0A-130E-0D110A12130D100E0D12
     * не наш   - 00038aec-a3eb-4e6d-bdd5-5027b405beb7
     *
     * @param $id
     * @return bool
     */
    private function isOurId($id)
    {
        if (preg_match(
            "/[\w\d]{8}-[\w\d]{4}-[\w\d]{4}-[\w\d]{4}-[\w\d]{20}/", $id
          ) == 1)
            return true;
        else
            return false;
    }

    /**
     * Метод забирает даныые с сервиса, парсит их и отдает каждую часть в виде массива в callback
     *
     * @param $serviceTableCode - код запрвшиваемого объекта с еиис
     * @param $sleepTime        - время ожидания
     * @param $callback         - функция что будет обрабатывать части пакета
     * @param string $parts     - сколько частей запрашивать, по умолчанию "all", для теста можно выставить в 1-2
     */
    public function getServiceEntitiesAsArray($serviceTableCode, $sleepTime, $callback, $parts="all"){
        //Формируем пакет
        $this->logger->Log("Запрос на формирование пакета");
        $packageId = $this->serviceClient->CreatePackage($serviceTableCode);

        //ждем чтобы пакет сформировался
        $this->logger->Log("Ожидание ".$sleepTime." сек");
        sleep($sleepTime);
        if($sleepTime > 240)
            $this->serviceClient->GetSessionId();

        $resultsCount = 0;
        //получаем количество страниц в пакете
        $this->logger->Log("Запрос на количество страниц в пакете");
        $pageCount = $this->serviceClient->GetPackageMeta($packageId);

        $this->logger->Log("Количество страниц: ".$pageCount);
        //Запрашиваем ответ по каждой странице
        $workPages = $pageCount;
        if($parts!= "all")
            $workPages = $parts;

        for($i = 1; $i <= $workPages; $i++) {
            $this->logger->Log("-------");
            $this->logger->Log("Запрашиваем страницу ".$i. " пакета");

            $results = $this->serviceClient->GetPackage($packageId, $i);

            //Это для дампа всего ответа в лог, иногда надо
            if(0) {
                $logger = new \Model\Logger\Logger($serviceTableCode);
                $logger->text($results);
            }

            $results = new \SimpleXMLElement($results);
            $resultsCount+=count($results->children());
            //Обходим результат добавляя в бд
            $partAsArray = $this->parseResultAsArray($results);
            $callback($partAsArray);
        }
        //Отсылаем мол все отлично отработало
        $this->serviceClient->SetOk($packageId);
        $this->logger->Log("Все отработало, обработано объектов: ".$resultsCount);
    }
}