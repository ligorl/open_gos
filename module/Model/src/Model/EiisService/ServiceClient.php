<?php
namespace Model\EiisService;

/**
 * Класс для работы с ЕИИС сервисом, ну типа там отсылаем запросы, получаем, ничего больше
 * анализом и обработкой занимается класс ServiceEntities
 *
 * Class ServiceClient
 * @package Model\EiisService
 */
class ServiceClient{
    // WSDL сервиса
    private $WSDL = "http://eiis.obrnadzor.gov.ru/integrationServices/BaseService.asmx?wsdl";
    //логин доступа к сервису
    private $login = "kisgoa";
    //пароль доступа к сервису
    private $pass = "g%h^f$";

    protected $logger;
    protected $debug = false;


    // Экземпляр класса SoapClient
    protected $soap;

    //Идентификатор сессии
    protected $sessionId = null;

    // Первоначальная инициализация
    public function __construct($options=null)
    {
        if($options != null){
            if(isset($options["wsdl"])) $this->WSDL = $options["wsdl"];
            if(isset($options["login"])) $this->login = $options["login"];
            if(isset($options["pass"])) $this->pass = $options["pass"];
            if(isset($options["debug"])) $this->debug = $options["debug"];
        }
        $this->soap = new \Zend\Soap\Client($this->WSDL);
        //var_dump($this->soap->getFunctions());
        //echo "<br>========<br>";
    }

    /**
     * Метод инициирует соединение, отправляет логин и пароль, получает идентификатор сессии
     * который используется во всех остальных запросах
     */
    public function GetSessionId(){
        $params = array(
            "login" => $this->login,
            "password" => $this->pass
        );

        try {
            $response = $this->soap->GetSessionId($params);

            preg_match(
                "/id='(.+)'/",
                $response->GetSessionIdResult,
                $matches
            );
            $this->sessionId = $matches[1];

            if($this->debug){
                echo "<br>Request GetSessionId<textarea>";
                var_dump($this->soap->getLastRequest());
                echo "</textarea><br>";

                echo "<br>Response GetSessionId<textarea>";
                var_dump($this->soap->getLastResponse());
                echo "</textarea><br>";

                echo "<br>Response GetSessionId<textarea>";
                var_dump($response);
                echo "</textarea><br>";
            }
        } catch(\Exception $e){
            var_dump("<textarea>".$this->soap->getLastRequest()."</textarea>");
            var_dump("<textarea>".$this->soap->getLastResponse()."</textarea>");
            var_dump($e->getMessage()."<br>=====<br>".$e->getTraceAsString());
        }
    }

    /**
     * Метод проверяет был ли получен идентификатор сессии, и если нет, то получает его
     */
    private function initSession(){
        if($this->sessionId == null)
            $this->GetSessionId();
    }

    /**
     * Метод возвращает структуру объекта
     * @param $objType - тип объекта, согласно технологической карте
     */
    public function GetObjectMeta($objType){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "objectCode" => $objType
        );
        $response = $this->soap->GetObjectMeta($params);
        if($this->debug) {
            echo "<textarea>";
            var_dump($response);
            echo "</textarea>";
        }
    }

    /**
     * Метод формирует пакет для получения данных
     */
    public function CreatePackage($objType){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "objectCode" => $objType,
            "historyCreate" => false,
            "documentInclude" => false,
            "filter" => ""
        );

        $response = $this->soap->CreatePackage($params);

        //Проверяем может ошибка
        $this->checkError($response);
        if($this->debug) {
            echo "<br>Request CreatePackage<textarea>";
            var_dump($this->soap->getLastRequest());
            echo "</textarea><br>";

            echo "<br>Response CreatePackage<textarea>";
            var_dump($this->soap->getLastResponse());
            echo "</textarea><br>";

            echo "<br>Response CreatePackage<textarea>";
            var_dump($response);
            echo "</textarea><br>";
        }


        preg_match(
            "/id='(.+)'/",
            $response->CreatePackageResult,
            $matches
        );



        return empty($matches[1]) ? null : $matches[1];
    }

    /**
     * Метод получает пакет данных по идентификатору
     */
    public function GetPackage($packageId, $part, $debug = null){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "packageId" => $packageId,
            "part" => $part
        );
        $response = $this->soap->GetPackage($params);
        //Проверяем может ошибка
        $this->checkError($response);

        if($this->debug){
            echo "<br>Response part " . $part . " ";
            echo "<br>Request GetPackage<textarea>";
            var_dump($this->soap->getLastRequest());
            echo "</textarea><br>";

            echo "<br>Response GetPackage<textarea style='width:600px; height:300px'>";
            var_dump($this->soap->getLastResponse());
            echo "</textarea><br>";

            echo "<br>Response GetPackage<textarea>";
            var_dump($response);
            echo "</textarea><br>";
        }

        return $response->GetPackageResult;
    }

    public function GetObjectList(){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "fieldsInclude" => "true"
        );
        $response = $this->soap->GetObjectList($params);

        if($this->debug){
            echo "<textarea>";
            var_dump($response);
            echo "</textarea>";
        }
    }

    public function GetPackages($packageId){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "packageId" => $packageId,
            "begin" => "1",
            "count" => "1"
        );
        $response = $this->soap->GetPackages($params);
        if($this->debug) {
            echo "<textarea>";
            var_dump($response);
            echo "</textarea>";
        }
    }

    public function GetUpdates($packageId){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "packageId" => $packageId,
            "part" => "0"
        );
        $response = $this->soap->GetUpdates($params);
        if($this->debug) {
            echo "<textarea>";
            var_dump($response);
            echo "</textarea>";
        }
    }

    /**
     * Метод вернет количество страниц в пакете
     * @param $packageId
     * @return mixed
     */
    public function GetPackageMeta($packageId){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "packageId" => $packageId,
        );
        $response = $this->soap->GetPackageMeta($params);

        //Проверяем может ошибка
        $this->checkError($response);
        if($this->debug) {
            echo "<br>GetPackageMeta<textarea>";
            var_dump($response);
            echo "</textarea>";
        }

        preg_match(
            "/capacity='(\d+)'/",
            $response->GetPackageMetaResult,
            $matches
        );

        return empty($matches[1]) ? null : $matches[1];
    }

    public function SetOk($packageId){
        $this->initSession();

        $params = array(
            "sessionId" => $this->sessionId,
            "packageId" => $packageId,
        );
        $response = $this->soap->SetOk($params);

        if($this->debug) {
            echo "<br>SetOK<textarea>";
            var_dump($response);
            echo "</textarea>";
        }
    }

    private function checkError($responce){
        $error = null;
        switch($responce){
            case "0320":
                $error = "Ошибка 0320: IP и MAC адреса не соответствуют открытой сессии.";
                break;
            case "0321":
                $error = "Ошибка 0321: Неверный логин или пароль.";
                break;
            case "0322":
                $error = "Ошибка 0322: Неверный идентификатор сессии.";
                break;
            case "033":
                $error = "Ошибка 033: Информация по объекту недоступна.  Выводится, когда нет опубликованной версии объекта.";
                break;
            case "034":
                $error = "Ошибка 034: Объект не объявлен. Выводится в случае, если описание объекта отсутствует в системе.";
                break;
            case "035":
                $error = "Ошибка 035: Недостаточно прав для доступа к объекту.";
                break;
            case "0540":
                $error = "Ошибка 0540: Нет записей в объекте.";
                break;
            case "0541":
                $error = "Ошибка 0541: Пакет не найден.";
                break;
            case "0542":
                $error = "Ошибка 0542: Не найдена часть пакета.";
                break;
            case "053":
                $error = "Ошибка 053: Пакет не сформирован.";
                break;
            case "064":
                $error = "Ошибка 064: Нарушена последовательность применения обновлений. Есть обновления с более ранней датой.";
                break;
            case "074":
                $error = "Ошибка 074: Информация временно недоступна. Выводится в случае обработки транзитных запросов и отсутствии подключения к системе-поставщику.";
                break;
            case "100":
                $error = "Ошибка 100: Внутренняя ошибка системы. Выводится в случае отказа ключевых узлов ЕИИС, например, отсутствует соединение с базой данных.";
                break;
        }
        if($error != null){
            $this->logger->Error($error);
            die;
        }

    }
}