<?php
namespace Eo\View\Render;

class Elements
{
    /**
     * Возвращает selectmenu в виде строки
     * @param type $optionArr
     * @param type $selectAttributesArr
     * Массив записываемый в аттрибуты [ attribute_name => attribute_value]
     * массив опшинов [ option_title => option_value,... ]
     * @param type $selectedKey
     * Значение текущего опшина
     * @param type $reverse
     * делает инверсию при создании опшинов и тогда массив обрабаываеться [ option_value => option_title,... ]
     */
    public static function selectMenu( $optionArr, $selectAttributesArr = null ,  $selectedKey = null, $reverse = false){
        $selectMenu = '';
        $selectMenuAttr = '';
        $options = '';

        if( gettype( $selectAttributesArr) == 'array'){
            foreach( $selectAttributesArr as $name=>$value){
                $selectMenuAttr .= $name.'="'.$value.'"';
            }
        }

        $selectMenu.= "<select " . $selectMenuAttr . ">";

         if( gettype( $optionArr) == 'array'){
            foreach( $optionArr as $name=>$value){
                if( $reverse ){
                    $options .= '<option ' . (($name == $selectedKey) ? "selected" : "") . ' value="'. $name .'" >'
                                .$value.
                            '</option>';
                }else{
                    $options .= '<option ' . (($value == $selectedKey) ? "selected" : "") . ' value="'. $value . '" >'
                                . $name.
                            '</option>';
                }
                
            }
        }

        $selectMenu .= $options.'</select>';

        return $selectMenu;
    }

    /**
     * Создает поле с датой в формате по умолчанию d.m.Y Возвращает прочерк если пришло пучтое поле или нулевые значения
     * @param type $date
     * дата
     * @param type $attributesArr
     * Массив записываемый в аттрибуты [ attribute_name => attribute_value]
     * @return string
     */
    public static function dateInput( $date, $attributesArr = null, $ifEmptyStr = '-'){
        $input = '';
        $inputAttr = '';
        $rendererDate = $ifEmptyStr;

        if(
          !empty($date) &&
          $date != "0000-00-00" &&
          $date != "0000-00-00 00:00:00" &&
          $date != '-'
        ){
            $rendererDate = new \DateTime( $date);
            $rendererDate = $rendererDate->format('d.m.Y');
        }

        if( gettype( $attributesArr) == 'array'){
            foreach( $attributesArr as $name=>$value){
                $inputAttr .= $name.'="'.$value.'"';
            }
        }

        $input = '<input type="text" value="'.$rendererDate.'" '.$inputAttr.' />';
        return $input;
    }

    /**
     * отрисовывает элемент согласно типу
     * @param array [
     *      'type' => '',
     *      'value' => 'значение',
     *      'editable' => 'Можно ли валидировать',
     *      //остальные аттрибуты будут записаны в аттрибуты
     * ]
     * @return type
     */
    public static function fromType( $renderElement){
        if( isset( $renderElement["editable"])){
            if( !$renderElement["editable"]){
                $renderElement["disabled"] = 'disabled';
            }
            unset( $renderElement["editable"]);
        }
        $typeElement =  $renderElement["type"];
        if( $renderElement["type"] != 'hidden' && $renderElement["type"] !='text' ){
            unset( $renderElement["type"]);
        }
        

        switch ( $typeElement) {

            case "hidden":
                return self::input( $renderElement );
                break;
            
            case "text":
                return self::input( $renderElement );
                break;

            case "textarea":
                $text = $renderElement["value"];
                unset( $renderElement["value"]);
                return self::textarea( $text ,$renderElement );
                break;

            case "function":
                return  $license->$renderElement["function_name"]();
                break;

            case "date":
                $date = $renderElement["value"];
                $ifEmptyStr = '-';
                if( isset( $renderElement["if_empty"])){
                    $ifEmptyStr = $renderElement["if_empty"];
                    unset( $renderElement["value"]);
                }
                
                unset( $renderElement["if_empty"]);
                
                return  self::dateInput($date, $renderElement, $ifEmptyStr);
                break;

            case "file":
                $renderElement['type'] = 'file';
                return self::input( $renderElement );
                break;

            case "select":
                $choosedEl = $renderElement["value"];
                $options = [];
                
                if( isset( $renderElement["options"])){
                    $options = $renderElement["options"];
                    unset( $renderElement["options"]);
                }
                unset( $renderElement["value"]);
                return self::selectMenu( $options, $renderElement, $choosedEl, false);
                break;

            case "select+reverse":
                $choosedEl = $renderElement["value"];
                $options = [];

                if( isset( $renderElement["options"])){
                    $options = $renderElement["options"];
                    unset( $renderElement["options"]);
                }
                unset( $renderElement["value"]);
                return self::selectMenu( $options, $renderElement, $choosedEl, true);
            break;
                
            case "select+empty":
                $choosedEl = $renderElement["value"];
                $options = [ 'Не выбрано' => ''];

                if( isset( $renderElement["options"])){
                    $options = $options + $renderElement["options"];
                    unset( $renderElement["options"]);
                }
                
                unset( $renderElement["value"]);
                return self::selectMenu( $options, $renderElement, $choosedEl, false);
                break;

            case "select+empty+reverse":
                $choosedEl = $renderElement["value"];
                $options = [ '' => 'Не выбрано'];

                if( isset( $renderElement["options"])){
                    $options = $options + $renderElement["options"];
                    unset( $renderElement["options"]);
                }

                unset( $renderElement["value"]);
                return self::selectMenu( $options, $renderElement, $choosedEl, true);
                break;

            case "list+text":
                $textElArray = $renderElement["value"];

                unset( $renderElement["value"]);
                
                return self::listText( $textElArray, $renderElement);
                break;

            case "link":
                if(!empty($renderElement["value"])){
                    $textElArray = $renderElement["value"];
                } else {
                    $textElArray = '';
                }                
                unset( $renderElement["value"]);                
                return self::link( $textElArray, $renderElement);
                break;
                
            case "TableRow":
                return self::tableRow( $renderElement);
                break;

            case "img":
                return self::img( $renderElement);
                break;

            case "checkbox":
                $text = '';
                if( isset($renderElement["text"])){
                    $text = $renderElement["text"];
                    unset( $renderElement["text"]);
                }
                return self::checkbox( '', $renderElement, $text);
                break;
            case "div":
                if(!empty($renderElement["value"])){
                    $textElArray = $renderElement["value"];
                } else {
                    $textElArray = '';
                }
                unset( $renderElement["value"]);
                return self::div( $textElArray, $renderElement);
                break;
        }
    }

    /**
     * Конвертирует массив аттрибутов в строку для html тега
     * @param array $attributesArr
     * @return string
     */
    public static function attributesToString(array $attributesArr){
        $str = '';
        $splitter = '"';
        foreach( $attributesArr as $name=>$value){
            $curSplitter = $splitter;

            if(  mb_stristr($value,'"')){
                $curSplitter = "'";
            }
            
            $str .= $name.'='.$curSplitter.$value.$curSplitter;
        }
        return $str;
    }

    /**
     * Устанавливает переменным $currentArr массива значения из массива $addonArr если те пусты
     * @param array $currentArr
     * @param array $addonArr
     * @return array
     */
     public static function setIfEmpty(array $currentArr ,array $addonArr){
        foreach( $addonArr as $name=>$value){

            if( empty($currentArr[ $name])){
                $currentArr[ $name] = $value;
            }
        }
        return $currentArr;
    }

    /**
     * Возвращает HTML input с заданным массивом фттрибутов
     * @param array $attributesArr
     * @return string
     */
    public static function input( $attributesArr = null){
        $input      = '';
        $inputAttr  = self::attributesToString($attributesArr);
        $input = '<input '.$inputAttr.' />';
        return $input;
    }

    /**
     * Возвращает HTML input с заданным массивом фттрибутов
     * @param string $text
     * @param array $attributesArr
     * @return string
     */
     public static function textarea( $text, $attributesArr = null){
        $textarea = '';
        $attr  = self::attributesToString($attributesArr);
        $textarea = '<textarea '.$attr.'>'.$text.'</textarea>';
        return $textarea;
    }

    public static function listText( $textArr, $attributesArr = null){
        $list = '';
        $attr  = self::attributesToString($attributesArr);
        $list .= '<ul '.$attr.'>';
        foreach( $textArr as $text){
            $list .= '<li>'.$text.'</li>';
        }
        $list .= '</ul>';
        return $list;
    }
    /**
     *  Формируем ссылку
     * @param type $aText - видимое
     * @param type $attributesArr - атрибуты
     * @return string
     */
    public static function link( $aText = '', $attributesArr = array()){        
        $aText = strip_tags($aText);
        if(!empty($attributesArr['href'])){
            //$attributesArr['href'] = urlencode($attributesArr['href']);
        } else {
            $attributesItArr['href'] ='#';
        }
        if(!empty($attributesArr['alt'])){
            $attributesArr['title'] = strip_tags($attributesArr['alt']);
            unset($attributesArr['alt']);
        }
        if(!empty($attributesArr['download'])){
            $attributesArr['download'] = strip_tags($attributesArr['download']);
        }        
        if(!empty($attributesArr['inNew'])){
            $attributesArr['_target'] = '_blank';
        }        
        if(!empty($attributesArr['class'])){
            $attributesArr['class'] .= ' element-link ';
        } else {
            $attributesArr['class'] = 'element-link';
        }
        unset($attributesArr['inNew']);        
        $attr  = self::attributesToString($attributesArr);
        $aTag = '<a '.$attr.'>'.$aText.'</a>';
        return $aTag;
    }
    
    public static function tableRow( $renderElement = array()){
        $result ='';
        if(!empty($attributesArr['class'])){
            $attributesArr['class'] .= ' element-table ';
        } else {
            $attributesArr['class'] = 'element-table';
        }
        $result .='<table class="'.$attributesArr['class'].'"><tr class="element-table-row">';
        foreach ( $renderElement['row'] as $key => $value ) {
            $result .= '<td class="element-table-cell">'.self::fromType($value).'</td>';
        }
        $result .='</tr></table>';
        return $result;         
    }

    public static function img( $renderElement = array()){
        $attr  = self::attributesToString($renderElement);
        $result = '<img '.$attr.'>';
        return $result;
    }    
    
     public static function checkbox( $preTitle = '', $attributesArr = array(), $postTitle = ''){
        $input = '';
        $inputAttr = '';
        
        if( gettype( $attributesArr) == 'array'){
            foreach( $attributesArr as $name=>$value){
                $inputAttr .= $name.'="'.$value.'"';
            }
        }

        $input = '<span class="checkboxBlock" >'.$preTitle.'<input type="checkbox" '.$inputAttr.' />'.$postTitle.'</span>';
        return $input;
     }

    public static function div( $text = '', $attributesArr = array()){
        $attr  = self::attributesToString($attributesArr);
        $div = '<div '.$attr.'>'.$text.'</div>';
        return $div;
    }

    public static function buttonsToDocument( $docId = '', $buttons = false, $dealNumber = '', $dealId = '', $classButton = 'buttonDocument'){
        $buttNext  = "";
        $buttPrev  = "";
        $buttOther = "";
        
        if ($buttons) {
            foreach ($buttons as $key => $Button) {
                switch ($Button->getField("ButtonType")) {
                    case 'next':
                        $buttNext.=
                        '<a class="'.$classButton.' nextButtonDocument" '
                            . 'data-document-id="'.$docId.'" '
                            . 'data-deal-id="'.$dealId.'" '
                            . "data-require='".$Button->getField("RequireField")."' "
                            . 'data-next-status="' . $Button->getField("NextStatus") .'" '
                            . 'data-deal-number="' . $dealNumber .'" '
                            . 'data-message="' . $Button->getField("Message") . '">' . $Button->getField("Name")
                        . '</a>';
                        break;

                    case 'prev':
                        $buttPrev.=
                        '<a class="'.$classButton.' prevButtonDocument" '
                            . 'data-document-id="'.$docId.'" '
                            . 'data-deal-id="'.$dealId.'" '
                            . "data-require='".$Button->getField("RequireField")."' "
                            . 'data-deal-number="' . $dealNumber .'" '
                            . 'data-next-status="' . $Button->getField("NextStatus") .'" '
                            .'data-message="' . $Button->getField("Message") . '">' . $Button->getField("Name")
                        . "</a>";
                        break;

                    case 'other':
                        $buttOther.=
                        '<a class="'.$classButton.' otherButtonDocument" '
                            . 'data-document-id="'.$docId.'" '
                            . 'data-deal-id="'.$dealId.'" '
                            . "data-require='".$Button->getField("RequireField")."' "
                            . 'data-next-status="' . $Button->getField("NextStatus") . '" '
                            . 'data-deal-number="' . $dealNumber . '" '
                            . 'data-message="' . $Button->getField("Message") . '">' . $Button->getField("Name")
                        . '</a>';
                        break;
                }
            }
        }
        return $buttPrev.$buttNext.$buttOther;
    }

    public static function getColorClassFromStatus( $status){
        $statusToColorArr = [
            '9416C0E6E84F4194B57DA930AD10A4D0' => 'state-active',
            '4E09AEA87399AFFD9EC3FE6833DB4B36' => 'state-project',
            '0727E76E83574B10833A25CA627FE6A0' => 'state-suspended',
            '5A9AAA5977024C5EAE3128DD37AC03AB' => 'state-inactive',
            //'приостановлено частично' => 'state-suspended-partial',
            '4D41481DBCA461BF5BAB7C5316D8F2D4' => 'state-annuled',
        ];

        if( isset($statusToColorArr[$status])){
            return $statusToColorArr[$status];
        }else{
            return '';
        }
    }
}