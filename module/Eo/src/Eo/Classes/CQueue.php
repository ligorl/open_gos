<?php

namespace Eo\Classes;

use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Mime;
use Zend\Mime\Part as MimePart;

class CQueue
{
    /**
     * Вычисление свободных дат для записи в электронную очередь
     * @return array
     */
    public function getDateTime()
    {
        $stepData1 = new \Zend\Session\Container('stepData1');
        $queueReasonId = null;
        $dateFree = [];

        $catalogItemClass = new \Model\LodEntities\CatalogItem();
        $appealType = $catalogItemClass->getByWhere(['id' => $stepData1['appeal_type']]);
        if ($appealType != null) {
            $electronicQueueReasonClass = new \Model\LodEntities\ElectronicQueueReason();
            $queueReason = $electronicQueueReasonClass->getByWhere(['code' =>$appealType->getField('code')]);

            if ($queueReason) {
                $room = $queueReason->getField('roomNumber');
                $length = 0;

                // Количество организаций
                $branchesNumber = (int)$stepData1['branches_number'] + 1;
                if (isset($stepData1['check_branches_only']) && $stepData1['check_branches_only'] != 'false') $branchesNumber--;

                $queueReasonId = $queueReason->getField('id');
                $baseReceptionTime = $queueReason->getField('baseReceptionTime');

                // Время в минутах
                $length += $baseReceptionTime * $branchesNumber;

                // Плюс время с поправкой на количество программ.
                if(isset($stepData1['programs_number'])){
                    $CPprogramCatalog = new \Model\LodEntities\NumberOfProgramCatalog();
                    $coef = $CPprogramCatalog->getByWhere(['id' => $stepData1['programs_number']])->getField('coefficient');
                    $length += $coef * $baseReceptionTime * $branchesNumber;
                }

                // Плюс время с поправкой на количество документов.
                if(isset($stepData1['documents_number'])) {
                    $CDocumentsCatalog = new \Model\LodEntities\NumberOfDocumentsCatalog();
                    $coef = $CDocumentsCatalog->getByWhere(['id' => $stepData1['documents_number']])->getField('coefficient');
                    $length += $coef * $baseReceptionTime;
                }

                // Плюс время с поправкой на количество документов.
                if(isset($stepData1['issue_documents_number'])) {
                    $CDocumentsAmount = new \Model\LodEntities\IssueDocumentsAmount();
                    $coef = $CDocumentsAmount->getByWhere(['id' => $stepData1['issue_documents_number']])->getField('coefficient');
                    $length += ((int)$coef-1) * $baseReceptionTime;
                }

                if ($length > 180) $length = 180;
                $audienceLength = new \Zend\Session\Container('audienceLength');
                $audienceLength->exchangeArray(['audienceLength' => $length] );

                $daysEnum = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'];
                $days = explode(';', str_replace(array_values($daysEnum), array_keys($daysEnum), $queueReason->getField('receptionDaysRaw')));
                $dateFree = [];
                $dateToday = new \DateTime();
                $dateMax = new \DateTime();
                $dateMax->modify('+1 month');


                $dateMax->setTime(23, 59, 59);
                $timeStep = '+' . $baseReceptionTime . ' minutes';
                $timeLength = '+' . $length . ' minutes';

                $timesPossible = $this->getPossibleTimes($queueReason, $length);

                $CElectronicQueueReason = new \Model\LodEntities\ElectronicQueueEntry();
                $existingEntries = $CElectronicQueueReason->getForDates($dateToday, $dateMax, $queueReason->getId());

                // Каникулы
                $CElectronicQueueVacation = new \Model\LodEntities\ElectronicQueueVacation();
                $vacations = $CElectronicQueueVacation->getByWhere([]);

                $dateTmp = clone $dateToday;
                $dateTmp->setTime(0, 0, 0);
                $dateMin = clone $dateToday;
                $dateMin->modify('+15 minutes');

                $CElectronicQueueEntrySlot = new \Model\LodEntities\ElectronicQueueEntrySlot();

                while ($dateTmp <= $dateMax) {
                    $day = $dateTmp->format('w');
                    $closed = false;
                    foreach ($vacations as $v) {
                        if ($v->getField('reason_id') == $queueReasonId && $dateTmp >= $v->getField('date_from') && $dateTmp <= $v->getField('date_to')) {
                            $closed = true;
                        }
                    }
                    if (in_array($day, $days) && !$closed) {
                        $date = $dateTmp->format('Y-n-j');
                        $dateFree[$date] = [];

                        $startReceptionTime = new \DateTime($queueReason->getField('startReceptionTime'));
                        $endReceptionTime   = new \DateTime($queueReason->getField('endReceptionTime'));
                        $breakStartTime     = new \DateTime($queueReason->getField('breakStartTime'));
                        $breakEndTime       = new \DateTime($queueReason->getField('breakEndTime'));

                        $qrStartTime = clone $dateTmp;
                        $qrStartTime->setTime(
                            $startReceptionTime->format('H'),
                            $startReceptionTime->format('i')
                        );
                        $qrEndTime = clone $dateTmp;
                        $qrEndTime->setTime(
                            $endReceptionTime->format('H'),
                            $endReceptionTime->format('i')
                        );
                        $qrBreakStartTime = clone $dateTmp;
                        $qrBreakStartTime->setTime(
                            $breakStartTime->format('H'),
                            $breakStartTime->format('i')
                        );
                        $qrBreakEndTime = clone $dateTmp;
                        $qrBreakEndTime->setTime(
                            $breakEndTime->format('H'),
                            $breakEndTime->format('i')
                        );

                        $timeTmpStart = clone $qrStartTime;
                        while ($timeTmpStart < $qrEndTime) {
                            if ($timeTmpStart > $dateMin && ($timeTmpStart < $qrBreakStartTime || $timeTmpStart >= $qrBreakEndTime)) {
                                $timeTmpEnd = clone $timeTmpStart;
                                $timeTmpEnd->modify($timeLength);
                                if (
                                    ($timeTmpStart < $qrBreakStartTime && $timeTmpEnd <= $qrBreakStartTime)
                                    || ($timeTmpStart >= $qrBreakEndTime && $timeTmpEnd > $qrBreakEndTime && $timeTmpEnd <= $qrEndTime)
                                ) {
                                    $occupied = false;
                                    foreach ($existingEntries as $ee) {
                                        if ($ee->overlapsTime($timeTmpStart, $timeTmpEnd)) {
                                            $occupied = true;
                                            break;
                                        }
                                    }
                                    $slot = $CElectronicQueueEntrySlot->getBySlot( $timeTmpStart,$timeTmpEnd );
                                    if ($slot) {
                                        $occupied = true;
                                    }
                                    if (!$occupied) {
                                        $dateFree[$date][] = $timeTmpStart->format('H:i');
                                    }
                                }
                            }
                            $timeTmpStart->modify($timeStep);
                        }

                        if (count($dateFree[$date]) == 0) unset($dateFree[$date]);
                    }
                    $dateTmp->modify('+1 day');
                }
            }
        }

        return $dateFree;
    }

    /**
     * @param ElectronicQueueReason $queueReason
     * @param int $jobLength
     * @return string[]
     */
    private function getPossibleTimes($queueReason, $jobLength)
    {
        $result = [];
        $timeLength = '+' . $jobLength . ' minutes';
        $timeStep = '+' . $queueReason->getField('baseReceptionTime') . ' minutes';

        $timeTmpStart = new \DateTime($queueReason->getField('startReceptionTime'));

        $breakStartTime = new \DateTime($queueReason->getField('breakStartTime'));
        $endReceptionTime = new \DateTime($queueReason->getField('endReceptionTime'));
        while ($timeTmpStart <= $endReceptionTime) {
            if ($timeTmpStart < $breakStartTime || $timeTmpStart >= $breakStartTime) {
                $timeTmpEnd = clone $timeTmpStart;
                $timeTmpEnd->modify($timeLength);
                if (
                    ($timeTmpStart < $breakStartTime && $timeTmpEnd <= $breakStartTime)
                    || ($timeTmpStart >= $breakStartTime && $timeTmpEnd > $breakStartTime && $timeTmpEnd <= $endReceptionTime)
                ) {
                    $t = $timeTmpStart->format('H:i');
                    $result[$t] = $t;
                }
            }
            $timeTmpStart->modify($timeStep);
        }

        return $result;
    }

    /**
     * Сохранение данных о электронной очереди.
     *
     * @param $educationOrganization
     *
     * @return \Model\LodEntities\ElectronicQueueEntry
     */
    public function save($educationOrganization)
    {
        $stepData1 = new \Zend\Session\Container('stepData1');
        $stepData2 = new \Zend\Session\Container('stepData2');
        $audienceLength = new \Zend\Session\Container('audienceLength');

        $receptionTime = new \DateTime($stepData2['date_day'] . ' ' . $stepData2['date_time'] . ':00');
        $receptionTimeEnd = clone $receptionTime;
        $receptionTimeEnd->modify('+' . $audienceLength['audienceLength'] . ' minutes');

        /** @var CatalogItem $CCatalogItem */
        $CCatalogItem = new \Model\LodEntities\CatalogItem();
        $appealType = $CCatalogItem->getByWhere(['id' => $stepData1['appeal_type']]);

        /** @var ElectronicQueueReason $CElectronicQueueReason */
        $CElectronicQueueReason = new \Model\LodEntities\ElectronicQueueReason();
        $queueReason = $CElectronicQueueReason->getByWhere(['code' =>$appealType->getField('code')]);
        $stepData2->offsetSet('queue_reason_id', $queueReason->getField('id'));
        $stepData2->offsetSet('audienceRoom', $queueReason->getField('roomNumber'));

        // TODO Удалить
        /** @var ServiceReasonCatalog $CServiceReasonCatalog */
        $CServiceReasonCatalog = new \Model\LodEntities\ServiceReasonCatalog();
        $serviceReason = $CServiceReasonCatalog->getByWhere(['id' => $stepData1['service_reason']]);

        $entry = new  \Model\LodEntities\ElectronicQueueEntry();
        $entry->setField('createdDateTime', date("Y-m-d H:i:s"));
        $entry->setField('updatedDateTime', date("Y-m-d H:i:s"));
        $entry->setField('electronicQueueReasonId', $stepData2['queue_reason_id']);
        $entry->setField('eMail', $stepData2['email']);
        $entry->setField('endReceptionTime', $receptionTimeEnd->format("Y-m-d H:i:s"));
        $entry->setField('filialAmount', $stepData1['branches_number']);
        $entry->setField('isBranchesOnly', (empty($stepData1['check_branches_only'])) ? '0' : '1');
        $entry->setField('firstName', $stepData2['first_name']);
        $entry->setField('issueDocumentsAmountId', $stepData1['issue_documents_number']);
        $entry->setField('lastName', $stepData2['last_name']);
        $entry->setField('licensingProcedureId', $stepData1['appeal_type']);
        $entry->setField('middleName', $stepData2['middle_name']);
        $entry->setField('mobilePhoneNumber', $stepData2['cell_phone']);
        $entry->setField('numberOfDocumentsId', $stepData1['documents_number']);
        $entry->setField('numberOfProgramCatalogId', $stepData1['programs_number']);
        $entry->setField('organizationId', $educationOrganization->getField('Id')); //
        $entry->setField('organizationName', $educationOrganization->getField('FullName'));//
        $entry->setField('organizationInn', $educationOrganization->getField('Inn'));//
        $entry->setField('phoneNumber', $stepData2['phone']);
        $entry->setField('secretKey', '');
        $entry->setField('serviceReasonCatalogId', $stepData1['service_reason']);
        $entry->setField('sevenDaysNotificationSent', false);
        $entry->setField('startReceptionTime', $receptionTime->format("Y-m-d H:i:s"));
        $entry->setField('status', 'unapproved');
        $entry->setField('id', $this->generateId());
        $entry->save(true);


        // записываем соотношение причин и записи
        if (is_array($stepData1['appeal_reason'])) {
            foreach ($stepData1['appeal_reason'] as $reasonId) {
                if ($appealType->getField('code') == 'reception_license') {
                    $reason = new \Model\LodEntities\ElectronicQueueEntryToLicenseReasonCatalogItem();
                    $reason->setField('id', $this->generateId());
                    $reason->setField('electronicQueueEntryId', $entry->getField('id'));
                    $reason->setField('licenseReasonCatalogItemId', $reasonId);
                    $reason->save(true);
                } elseif ($appealType->getField('code') == 'reception_accreditation') {
                    $reason = new \Model\LodEntities\ElectronicQueueEntryToAccreditationReason();
                    $reason->setField('id', $this->generateId());
                    $reason->setField('electronicQueueEntryId', $entry->getField('id'));
                    $reason->setField('AccredApplicationReasons', $reasonId);
                    $reason->save(true);
                }
            }
        }

        /** @var ElectronicQueueReason $queueReason */
        $CElectronicQueueReason = new \Model\LodEntities\ElectronicQueueReason();
        $queueReason = $CElectronicQueueReason->getByWhere(['code' => $appealType->getField('code')]);

        if ($queueReason) {
            $length = $queueReason->getField('baseReceptionTime');

            $startReceptionTime = new \DateTime($queueReason->getField('startReceptionTime'));
            $endReceptionTime   = new \DateTime($queueReason->getField('endReceptionTime'));
            $breakStartTime     = new \DateTime($queueReason->getField('breakStartTime'));
            $breakEndTime       = new \DateTime($queueReason->getField('breakEndTime'));

            $qrStartTime = clone $receptionTime;
            $qrStartTime->setTime(
                $startReceptionTime->format('H'),
                $startReceptionTime->format('i')
            );
            $qrEndTime = clone $receptionTime;
            $qrEndTime->setTime(
                $endReceptionTime->format('H'),
                $endReceptionTime->format('i')
            );
            $qrBreakStartTime = clone $receptionTime;

            $qrBreakStartTime->setTime(
                $breakStartTime->format('H'),
                $breakStartTime->format('i')
            );
            $qrBreakEndTime = clone $receptionTime;
            $qrBreakEndTime->setTime(
                $breakEndTime->format('H'),
                $breakEndTime->format('i')
            );

            $timeTmpStart = clone $qrStartTime;
            $additionalAppealId = null;
            if ($appealType->getField('id') == '08c58690c64c4d00af201b33583e0adf') {
                $additionalAppealId = 'cb6c9be69d404d1e8678355bbd6501ab';
            } elseif ($appealType->getField('id') == 'cb6c9be69d404d1e8678355bbd6501ab') {
                $additionalAppealId = '08c58690c64c4d00af201b33583e0adf';
            }
            while ($timeTmpStart < $qrEndTime) {
                if ($timeTmpStart < $qrBreakStartTime || $timeTmpStart >= $qrBreakEndTime) {
                    $timeTmpEnd = clone $timeTmpStart;
                    $timeTmpEnd->modify('+' . $length . ' minutes');
                    if ($entry->overlapsTime($timeTmpStart, $timeTmpEnd)) {
                        $slot = new \Model\LodEntities\ElectronicQueueEntrySlot();
                        $slot->setField('entryId', $entry->getField('id'));
                        $slot->setField('appealTypeId', $appealType->getField('id'));
                        $slot->setField('slot', $timeTmpStart->format("Y-m-d H:i:s"));
                        $slot->save(true);
                        if ($additionalAppealId) {
                            $slot = new \Model\LodEntities\ElectronicQueueEntrySlot();
                            $slot->setField('entryId', $entry->getField('id'));
                            $slot->setField('appealTypeId', $appealType->getField('id'));
                            $slot->setField('slot', $timeTmpStart->format("Y-m-d H:i:s"));
                            $slot->save(true);
                        }
                    }
                }

                $timeTmpStart->modify('+' . $length . ' minutes');
            }
        }

        $appealReasons = [];
        if (is_array($stepData1['appeal_reason'])) {
            $CLicenseReasonCatalogItem = new \Model\LodEntities\LicenseReasonCatalogItem();
            $CApplicationReason = new \Model\Entities\isgaApplicationReasons();
            foreach ($stepData1['appeal_reason'] as $reasonId) {
                $appealReasonObj = $CLicenseReasonCatalogItem->getByWhere(['id' => $reasonId]);
                if ($appealReasonObj) {
                    $appealReasons[] = $appealReasonObj->getField('title');
                } else {
                    $appealReasonObj = $CApplicationReason->getByWhere(['id' => $reasonId]);
                    if ($appealReasonObj) $appealReasons[] = $appealReasonObj->getField('ShortName');
                }
            }
        }

        return $entry;
    }

    /**
     * Генерируем id.
     * Так как стандартый метод генерации генерит id другого форматв
     *
     * @return string
     */
    public function generateId()
    {
        $mtimeParts = explode(" ", microtime());
        $base = $mtimeParts[1];
        $mtimeParts = explode(".", $mtimeParts[0]);
        $base .= $mtimeParts[1];

        $rand1 = rand(1000, 5000);
        $rand2 = rand(1000, 5000);

        $base .= ($rand1 + $rand2);

        $Id = sprintf('%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
            10 + $base[0], 10 + $base[1], 10 + $base[2], 10 + $base[3], 10 + $base[4], 10 + $base[5], 10 + $base[6],
            10 + $base[7], 10 + $base[8], 10 + $base[9], 10 + $base[10], 10 + $base[11], 10 + $base[12], 10 + $base[13],
            10 + $base[14], 10 + $base[15], 10 + $base[18], 10 + $base[19], 10 + $base[20], 10 + $base[21]);

        return md5($Id);
    }

    /**
     * Формирование массива с данными для отправки сообщения.
     *
     * @param $entry
     *
     * @return array
     */
    public function getDataMessage($entry)
    {
        $stepData1 = new \Zend\Session\Container('stepData1');
        $stepData2 = new \Zend\Session\Container('stepData2');
        $audienceLength = new \Zend\Session\Container('audienceLength');

        $paymentNotification = '';
        if(!in_array($stepData1['service_reason'], array('9a5eced13c6e40bdb7b6fa2bd447524f', 'c4f78a563d8640ecaa48d8408744e66d'))){
            $paymentNotification = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/payment-notification/' . $entry->getField('id');
        }

        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/confirm/' . $entry->getField('id');
        $cancelLink = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/cancel/' . $entry->getField('id');

        $receptionTime  = new \DateTime($entry->getField('startReceptionTime'));
        $CQueueReason = new \Model\LodEntities\ElectronicQueueReason();
        $queueReason = $CQueueReason->getByWhere(['id' => $entry->getField('electronicQueueReasonId')]);

        // По другому не работает
        $stepData1['audienceLength'] = $audienceLength['audienceLength'];

        $data = [
            'link' => $link,
            'cancel_link' => $cancelLink,
            'payment_notification' => $paymentNotification,
            'date_day' => $receptionTime->format('d.m.y'),
            'date_time' => $receptionTime->format('H:i'),
            'date_register' => (new \DateTime())->format('d.m.y'),
            'appeal_type' => $queueReason->getField('name'),
            'stepData1' => (array)$stepData1->getIterator(),
            'stepData2' => (array)$stepData2->getIterator()
        ];

        return $data;
    }

    /**
     * Отправка mail.
     *
     * @param $content
     * @param $from
     * @param $subject
     * @param string $to
     *
     * @return bool
     */
    public function sendMail($content, $to, $subject, $from = 'noreply@eq.obrnadzor.gov.ru')
    {
        try{
            $html = new MimePart($content);
            $html->type = Mime::TYPE_HTML;
            $html->charset = 'utf-8';
            $html->encoding = Mime::ENCODING_QUOTEDPRINTABLE;

            $body = new MimeMessage();
            $body->setParts([$html]);

            $mail = new Message();
            $mail->setBody($body);
            $mail->setFrom($from);
            $mail->addTo($to);
            $mail->setSubject($subject);
            $transport = new Sendmail();
            $transport->send($mail);
            return true;
        }catch (\Exception $e){
            return false;
        }

    }

    /**
     * Выборка option в select.
     *
     * @param $entities
     *
     * @return array
     */
    public function getSelectOption($entities)
    {
        $selectOption = [
            'arrBasisTreatment' => [],
            'arrTypeTreatment' => [],
            'arrlicenseReasonCatalogItem' => [],
            'arrLicenseType' => []
        ];

        foreach ($entities as $key => $value){
            $selectOption['arrBasisTreatment'][$value['BasisTreatmentId']] = $value['BasisTreatment'];  // Основание услуги
            $selectOption['arrTypeTreatment'][$value['TypeTreatmentId']] = $value['TypeTreatment'];     // Тип обращения
        }

        $optionReason = new \Zend\Session\Container('optionReason');
        $selectOption['arrlicenseReasonCatalogItem'] = (array)$optionReason->getIterator();              // Тип причины

        return $selectOption;
    }

}