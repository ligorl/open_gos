<?php

namespace Eo\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Form\Element\Radio;
use Zend\Form\Element\Select;
use Zend\Form\Element\MultiCheckbox;
use Zend\Form\Element\Number;
use Zend\Form\Element\Text;
use Zend\Validator\Regex;

class EqueueForm extends Form implements InputFilterProviderInterface
{

    protected $isValid = true;

    protected $possibleDates = [];


    public function __construct($name)
    {
        parent::__construct($name, []);

        /** Тип услуги */
        $this->add([
            'type' => Radio::class,
            'name' => 'service_type',
            'options' => [
                'label' => 'Тип услуги',
                'label_attributes' => [
                    'class' => 'col-md-3 radio-inline'
                ],
                'value_options' => [
                    'license' => 'Лицензирование',
                    'accreditation' => 'Акредитация'
                ]
            ]
        ]);

        /** Тип обращения */
        $appealTypes = $this->getOptionType();
        $valueOptions = [];
        foreach ($appealTypes as $appealType) {
            $valueOptions[] = [
                'value' => $appealType->getField('id'),
                'label' => $appealType->getField('title'),
                'attributes' => [
                    'data-code' => $appealType->getField('code'),
                    'data-hide' => 'hide'
                ]
            ];
        }
        $this->add([
            'type' => Select::class,
            'name' => 'appeal_type',
            'options' => [
                'label' => 'Тип обращения',
                'value_options' => $valueOptions
            ],
            'attributes' => [
                'class' => 'col-md-11 form-control'
            ]
        ]);

        /** Основание услуги */
        $serviceReasons = $this->getOptionServiceReason();
        $exception = array('169d45490d0c41e1a55085dd33620f74', '9a2b174cab364241bdb0f9a90498ae55');
        $valueOptions = [];
        foreach($serviceReasons as $reason) {
            if(!in_array($reason->getId(), $exception)){
                $valueOptions[] = [
                    'value' => $reason->getField('id'),
                    'label' => $reason->getField('name'),
                    'attributes' => [
                        'data-code' => $reason->getField('code'),
                        'data-appeal-code' => $reason->getField('appealTypeCode')
                    ]
                ];
            }
        }

        $this->add([
            'type' => Select::class,
            'name' => 'service_reason',
            'options' => [
                'label' => 'Основание услуги',
                'value_options' => $valueOptions
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Количество выдаваемых документов */
        $numberOfDocuments = $this->getIssueDocuments();
        $valueOptions = [];
        foreach($numberOfDocuments as $number) {
            $valueOptions[] = [
                'value' => $number->getField('id'),
                'label' => $number->getField('name'),
                'attributes' => [
                    'data-code' => $number->getField('code'),
                ]
            ];
        }
        $this->add([
            'type' => Select::class,
            'name' => 'issue_documents_number',
            'options' => [
                'label' => 'Количество выдаваемых документов',
                'value_options' => $valueOptions
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Причина обращения */
        $appealReasons = $this->getAppealReasonsLic();
        $valueOptions = [];
        foreach($appealReasons as $reason) {
            $valueOptions[] = [
                'value' => $reason->getField('id'),
                'label' => $reason->getField('title'),
                'attributes' => [
                    'data-code' => $reason->getField('code'),
                    'data-reason-code' => $reason->getField('ServiceReasonCode')
                ]
            ];
        }
        $appealReasons = $this->getAppealReasonsAcr();
        foreach($appealReasons as $reason) {
            $valueOptions[] = [
                'value' => $reason->getField('Id'),
                'label' => $reason->getField('Name'),
                'attributes' => [
                    'data-code' => $reason->getField('Code'),
                    'data-reason-code' => $reason->getField('ServiceReasonCode')
                ]
            ];
        }

        $this->add([
            'type' => MultiCheckbox::class,
            'name' => 'appeal_reason',
            'options' => [
                'label' => 'Выберите причину обращения',
                'value_options' => $valueOptions
            ],
        ]);

        /** Количество филиалов */
        $this->add([
            'type' => Number::class,
            'name' => 'branches_number',
            'options' => [
                'label' => 'Количество филиалов'
            ],
            'attributes' => [
                'class' => 'form-control',
                'min' => 0,
                'value' => '0'
            ]
        ]);

        /** Перечень заявленных организаций */
        $this->add([
            'type' => MultiCheckbox::class,
            'name' => 'check_branches_only',
            'options' => [
                'label' => 'Перечень заявленных организаций',
                'value_options' => [
                    [
                        'value' => 'y',
                        'label' => 'Заявление только на филиал(ы), без головной организации',
                        'attributes' => [
                            'class' => 'check-branches-only'
                        ]
                    ]
                ]
            ]
        ]);


        /** Количество программ */
        $numbersOfProgram = $this->getNumbersOfProgram();
        $valueOptions = [];
        foreach($numbersOfProgram as $number) {
            $valueOptions[] = [
                'value' => $number->getField('id'),
                'label' => $number->getField('name'),
                'attributes' => [
                    'data-code' => $number->getField('code'),
                ]
            ];
        }
        $this->add([
            'type' => Select::class,
            'name' => 'programs_number',
            'options' => [
                'label' => 'Количество программ',
                'value_options' => $valueOptions
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Требуется ли сверка */
        $this->add([
            'type' => MultiCheckbox::class,
            'name' => 'check_needed',
            'options' => [
                'label' => 'Требуется ли сверка?',
                'value_options' => [
                    [
                        'value' => 'y',
                        'label' => 'В составе заявления присутствуют документы, требующие сверки данных с оригиналом (незаверенные нотариально)',
                        'attributes' => [
                            'class' => 'check-unverified-documents'
                        ]
                    ]
                ]
            ]
        ]);

        /** Календарь */
        $this->add([
            'type' => Text::class,
            'name' => 'date_day',
            'options' => [
                'label' => 'Запись на прием'
            ],
            'attributes' => [
                'class' => 'form-control',
            ]
        ]);

        /** Выбор времени */
        $this->add([
            'type' => Select::class,
            'name' => 'date_time',
            'options' => [
                'label' => ''
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Фамилия */
        $this->add([
            'type' => Text::class,
            'name' => 'last_name',
            'options' => [
                'label' => 'Фамилия'
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Имя */
        $this->add([
            'type' => Text::class,
            'name' => 'first_name',
            'options' => [
                'label' => 'Имя'
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Отчество*/
        $this->add([
            'type' => Text::class,
            'name' => 'middle_name',
            'options' => [
                'label' => 'Отчество'
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Телефон */
        $this->add([
            'type' => Text::class,
            'name' => 'phone',
            'options' => [
                'label' => 'Телефон'
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Мобильный телефон */

        $this->add([
            'type' => Text::class,
            'name' => 'cell_phone',
            'options' => [
                'label' => 'Моб. телефон'
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Email */
        $this->add([
            'type' => Text::class,
            'name' => 'email',
            'options' => [
                'label' => 'Эл. почта'
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** Количество документов для сверки */
        $numberOfDocuments = $this->getNumbersOfDocuments();
        $valueOptions = [];
        foreach($numberOfDocuments as $number) {
            $valueOptions[] = [
                'value' => $number->getField('id'),
                'label' => (empty($number->getField('name'))) ? '0' : $number->getField('name'),
                'attributes' => [
                    'data-code' => $number->getField('code'),
                ]
            ];
        }
        $this->add([
            'type' => Select::class,
            'name' => 'documents_number',
            'options' => [
                'label' => 'Количество документов для сверки',
                'value_options' => $valueOptions
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

    }

    public function getInputFilterSpecification()
    {
    }

    /**
     * Выборка всех возможных типов обращений.
     *
     * @return mixed
     */
    public function getOptionType()
    {
        $catalogItemClass = new \Model\LodEntities\CatalogItem();
        return $catalogItemClass->getAppealTypes();
    }

    /**
     * Выборка всех возможных типов обращений.
     *
     * @return mixed
     */
    public function getOptionServiceReason()
    {
        $serviceReasonClass = new \Model\LodEntities\ServiceReasonCatalog();
        return $serviceReasonClass->getAllNotTemp();
    }

    /**
     * Выборка всех причин лицензий.
     *
     * @return mixed
     */
    public function getAppealReasonsLic()
    {
        $licenseReasonCatalogItemClass = new \Model\LodEntities\LicenseReasonCatalogItem();
        return $licenseReasonCatalogItemClass->getAll();
    }

    /**
     * Выборка всех причин акредитаций.
     *
     * @return mixed
     */
    public function getAppealReasonsAcr()
    {
        $isgaApplicationReasonsClass = new \Model\Entities\isgaApplicationReasons();
        return $isgaApplicationReasonsClass->getAllByWhere(array());

    }

    /**
     * Set a single option for an element
     *
     * @param  string $key
     * @param  mixed $value
     * @return self
     */
    public function setOption($key, $value)
    {
        // TODO: Implement setOption() method.
    }

    /**
     * Список возможного количества программ.
     *
     * @return mixed
     */
    public function getNumbersOfProgram()
    {
        $numberOfProgramCatalogClass = new \Model\LodEntities\NumberOfProgramCatalog();
        return $numberOfProgramCatalogClass->getAllByWhereWithOrder(array(), array('coefficient ASC'));
    }

    /**
     * Список возможного количества документов.
     *
     * @return mixed
     */
    public function getNumbersOfDocuments()
    {
        $numberOfDocumentsCatalogClass = new \Model\LodEntities\NumberOfDocumentsCatalog();
        return $numberOfDocumentsCatalogClass->getAllByWhereWithOrder(array(), array('coefficient ASC'));
    }

    /**
     * Список возможного количества документов для выдачи.
     *
     * @return mixed
     */
    public function getIssueDocuments()
    {
        $numberOfProgramCatalogClass = new \Model\LodEntities\IssueDocumentsAmount();
        return $numberOfProgramCatalogClass->getAllByWhereWithOrder(array(), array('coefficient ASC'));

    }

    /**
     * Установить список доступного времени.
     *
     * @param $times
     */
    public function setTimes($times)
    {
        /** @var Select $el */
        $el = $this->get('date_time');
        $el->setValueOptions($times);
    }

    /**
     * Валидация первого этапа регистрации в Электроную очередь.
     *
     * @return bool
     */
    public function isValidStep1()
    {
        if(!isset($this->data['service_type']) || !in_array($this->data['service_type'], ['license', 'accreditation'])){
            $this->setErrorMessage('service_type', 'isCorrect', 'Not correct data');
        }

        $catalogItemClass = new \Model\LodEntities\CatalogItem();
        if(!isset($this->data['appeal_type']) || !$catalogItemClass->getByWhere(['id' => $this->data['appeal_type']])){
            $this->setErrorMessage('appeal_type', 'isCorrect', 'Not correct data');
        }

        $serviceReasonClass = new \Model\LodEntities\ServiceReasonCatalog();
        if(!isset($this->data['service_reason']) || !$serviceReasonClass->getByWhere(['id' => $this->data['service_reason']])){
            $this->setErrorMessage('service_reason', 'isCorrect', 'Not correct data');
        }

        $numberOfProgramCatalogClass = new \Model\LodEntities\NumberOfProgramCatalog();
        if(isset($this->data['programs_number']) && !$numberOfProgramCatalogClass->getByWhere(['id' => $this->data['programs_number']])){
            $this->setErrorMessage('programs_number', 'isCorrect', 'Not correct data');
        }

        if(isset($this->data['check_needed']) && $this->data['check_needed']){
            $numberOfDocumentsCatalogClass = new \Model\LodEntities\NumberOfDocumentsCatalog();
            if(isset($this->data['documents_number']) && !$numberOfDocumentsCatalogClass->getByWhere(['id' => $this->data['documents_number']])){
                $this->setErrorMessage('documents_number', 'isCorrect', 'Not correct data');
            }
        }

        if(isset($this->data['branches_number']) && (!$this->data['branches_number'] != '' && !is_numeric($this->data['branches_number']))){
            $this->setErrorMessage('branches_number', 'isCorrect', 'Not correct data');
        }

        return $this->isValid;
    }

    /**
     * Валидация второго этапа.
     */
    public function isValidStep2()
    {

        if(empty($this->data['last_name'])){
            $this->setErrorMessage('last_name', 'isEmpty', 'Field cannot be empty');
        }

        if(empty($this->data['first_name'])){
            $this->setErrorMessage('first_name', 'isEmpty', 'Field cannot be empty');
        }


        //validate email
        if(empty($this->data['email'])){
            $this->setErrorMessage('email', 'isEmpty', 'Field cannot be empty');
        }else{

            $emailValidator = new \Zend\Validator\EmailAddress();
            if (isset($this->data['email']) && !$emailValidator->isValid($this->data['email'])) {
                $this->setErrorMessage('email', 'invalid', 'Specified email is invalid');
            }
        }

//        var_dump($this->data['email']);
//        var_dump(empty($this->data['email']));die;

        //validate date
        if (isset($this->data['date_day'])) {
            $d = new \DateTime($this->data['date_day']);
            $d = $d->format('Y-n-j');

            if (!in_array($d, $this->possibleDates)) {
                $this->setErrorMessage('date_day', 'invalid', 'Soecified date is not in an allowed range');
            }
        }

        //validate phones
        $phoneValidator = new Regex('/^[0-9+\-() ]*$/');
        if (isset($this->data['phone'])) {
            if (!$phoneValidator->isValid($this->data['phone'])) {
                $this->setErrorMessage('phone', 'invalid', 'Specified phone number is invalid');
            }
        }
        if (isset($this->data['cell_phone'])) {
            if (!$phoneValidator->isValid($this->data['cell_phone'])) {
                $this->setErrorMessage('cell_phone', 'invalid', 'Specified phone number is invalid');
            }
        }

        return $this->isValid;
    }

    protected function setErrorMessage($field, $key, $text)
    {
        $this->isValid = false;
        $messages = $this->get($field)->getMessages();
        if (!is_array($messages) && !($messages instanceof \Traversable)) $messages = [];
        $messages[$key] = $text;
        $this->get($field)->setMessages($messages);
    }

    public function setDates($dates)
    {
        $this->possibleDates = $dates;
    }


}