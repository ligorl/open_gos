<?php

namespace Eo\Model;

use Model\Entities\eiisEducationalOrganizations;


class ExportCertificate extends ExportDocument {

    function getType()
    {
        return  'Certificate';
    }

    public function getData()
    {
        //$applicationsClass  = $this->applicationsClass;
        $currentApplication = $this->currentApplication;
        $monthArray         = $this->monthArray;
        $resultArray        = $this->resultArray;

        $certificate =$this->getCertificate();
        $mainOrg     = $this->mainOrg;
        $arrayOrg    = $mainOrg->toArray();
        $dateStart   = strtotime($certificate['DateIssue']);
        $moreInfo = $this->moreInfo;
        if(isset($moreInfo[$arrayOrg['Id']])){
            $moreInfoMain = $moreInfo[$arrayOrg['Id']]->toArray();
        }elseif(isset($moreInfo[0])){
            $moreInfoMain = $moreInfo[0]->toArray();
        }else{
            $moreInfoMain = array('Information'=>'','Email'=>'');
        }


        $resultArray['field1']  = date('d', $dateStart);
        $resultArray['field2']  = $monthArray[date('n', $dateStart) ];
        $resultArray['field3']  = date('Y', $dateStart);
        $resultArray['field4']  = $certificate['RegNumber'];
        $resultArray['field5']  = $certificate['SerialNumber'];
        $resultArray['field6']  = $certificate['FormNumber'];
        $resultArray['fieldFullName']  = $arrayOrg['FullName'] . ', ' . $arrayOrg['ShortName'];
        $resultArray['fieldLawAddress']  = $arrayOrg['LawAddress'];
        $resultArray['fieldGosRegNum']  = $arrayOrg['GosRegNum'];
        $resultArray['fieldInn']  = $arrayOrg['Inn'];
        $resultArray['fieldKpp']  = $arrayOrg['Kpp'];
        $resultArray['fieldSecretGosNubmer']  = $this->getDetailsLicenseWorkUsingInformationSecret($moreInfo,$arrayOrg['Id']);
        $resultArray['fieldPhones']  = $arrayOrg['Phones'];
        $resultArray['fieldMails']  = $arrayOrg['Mails'];
        $resultArray['fieldWww']  = $this->ifNotEmpty($arrayOrg['Www']);
        $resultArray['fieldI'] = $this->ifActive($moreInfoMain['Information']);
        $resultArray['fieldInformationEmail'] = $this->conditionSymbolActive($moreInfoMain['Information']) ? $moreInfoMain['Email'] : ' ';
        $resultArray['fieldINo'] = $this->ifNotActive($moreInfoMain['Information']);
        $resultArray['fieldChargePosition'] = $arrayOrg['ChargePosition'];
        $resultArray['fieldFIOContactManager'] = $this->getFIOContactManager($arrayOrg);
        $resultArray = $this->dateParse($resultArray);
        $resultArray['shtrihKod'] = $this->getShtrihKod($currentApplication->getField('Barcode'),                                                                                      $currentApplication->getField('fk_isgaApplicationStatus'));
        //$resultArray['fieldReason'] = $this->getApplicationReason('AblativeName');
        $resultArray['fieldReason'] = $this->getApplicationReason('Name');

        $applicationPrograms = $currentApplication
            ->selectComplicated(
              "isga_ApplicationPrograms",
                array(
                    'fk_eiisLicensedPrograms',
                    'yearStartEdu',
                    'termEdu',
                    'fullTimeCount',
                    'partTimeCount',
                    'correspondenceCount',
                    'familyCount',
                    'networkForm',
                    'remoteTechnology',
                    'publicAccreditation'
                ), 
                array(
                    'fk_isgaApplication' => $currentApplication
                    ->getField('Id'),
                    'fk_isgaEOBranches'  => $arrayOrg['Id'])
            )->toArray();
        $newArrApplicationPrograms = array();
        foreach ($applicationPrograms as $oneAplicationProgram) {
            $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
        }
        $programsIds = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
         $programsIds=array_filter($programsIds);
        if (count($programsIds) > 0) {
            $programs = $currentApplication->selectComplicated(
                    "eiis_LicensedPrograms", 
                    array(
                        'Id',
                        'Code',
                        'Name',
                        'fk_eiisEduProgram',
                        'fk_eiisEduProgramType'
                    ), 
                    array('Id' => $programsIds)
                )->toArray();

            $programTypeIds   = array_column($programs, 'fk_eiisEduProgramType');
            $eduProgramIds    = array_column($programs, 'fk_eiisEduProgram');
            $programsTypes    = $currentApplication->selectComplicated("eiis_EduProgramTypes", array('Id', 'Name'), array('Id' => $programTypeIds))->toArray();
            $arrayTypes       = array_column($programsTypes, 'Name', 'Id');
            $eduPrograms      = $currentApplication->selectComplicated("eiis_EduPrograms", array('Id', 'fk_eiisGS'), array('Id' => $eduProgramIds))->toArray();
            $arrayEduPrograms = array_column($eduPrograms, 'fk_eiisGS', 'Id');
            $isgaGSIds        = array_column($eduPrograms, 'fk_eiisGS');
            $isgaGSArray      = $currentApplication->selectComplicated("isga_GroupSpecialities", array('Id', 'Name', 'Code', 'fk_isgaEGS'), array('Id' => $isgaGSIds))->toArray();
            $isgaGSIdEGS      = array_column($isgaGSArray, 'fk_isgaEGS', 'Id');
            $isgaEGSIds       = array_column($isgaGSArray, 'fk_isgaEGS');
            $isgaEGSArray     = $currentApplication->selectComplicated("isga_EnlargedGroupSpecialities", array('Id', 'Name', 'Code'), array('Id' => $isgaEGSIds))->toArray();
            $isgaEGSArrayName = array_column($isgaEGSArray, 'Name', 'Id');
            $isgaGSArrayName  = array_column($isgaGSArray, 'Name', 'Id');
            $isgaEGSArrayCode = array_column($isgaEGSArray, 'Code', 'Id');
            $isgaGSArrayCode  = array_column($isgaGSArray, 'Code', 'Id');

            //учебные программы
            $i = 0;
            foreach ($programs as $oneProgram) {
                $resultArray['table1'][$i]['countcol']=$i+1;
                $resultArray['table1'][$i]['tabfield1'] = $oneProgram['Name'];
                $resultArray['table1'][$i]['tabfield2'] = $arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                $resultArray['table1'][$i]['tabfield3'] = $isgaEGSArrayCode[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                $resultArray['table1'][$i]['tabfield4'] = $isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                $resultArray['table1'][$i]['tabfield5'] = $this->getCodeProgram($oneProgram);
                $resultArray['table1'][$i]['tabfield6'] = $oneProgram['Name'];
                $resultArray['table1'][$i]['tabfield7'] = $newArrApplicationPrograms[$oneProgram['Id']]['yearStartEdu'];
                $resultArray['table1'][$i]['tabfield8'] = $newArrApplicationPrograms[$oneProgram['Id']]['termEdu'];

                $resultArray['table1'][$i]['tabfield9']  = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['fullTimeCount']);
                $resultArray['table1'][$i]['tabfield10'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['partTimeCount']);
                $resultArray['table1'][$i]['tabfield11'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['correspondenceCount']);
                $resultArray['table1'][$i]['tabfield12'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['familyCount']);

                $resultArray['table1'][$i]['tabfield13'] = ($newArrApplicationPrograms[$oneProgram['Id']]['networkForm'] == 1 ? 'Да' : "Нет");
                $resultArray['table1'][$i]['tabfield14'] = ($newArrApplicationPrograms[$oneProgram['Id']]['remoteTechnology'] == 1 ? 'Да' : "Нет");
                $resultArray['table1'][$i]['tabfield15'] = ($newArrApplicationPrograms[$oneProgram['Id']]['publicAccreditation'] == 1 ? 'Да' : "Нет");
                ++$i;
            }
        }

        //филиалы
        $branchesApplication = $currentApplication->selectComplicated("isga_mtm_Application_Branches", array('fk_eiisEOBranch'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        $j                   = 0;
        $educationOrganization = new eiisEducationalOrganizations();
        $mainOrgId=$mainOrg->getId();
        foreach ($branchesApplication as $oneBranch) {
            // пропускаем главный вуз, чтобы
            if ($oneBranch['fk_eiisEOBranch'] === $mainOrgId) {
                continue;
            }
            $mainOrg                               = $currentApplication->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $oneBranch['fk_eiisEOBranch']));
            $arrayOrg                              = $mainOrg->toArray();
            $educationOrganization->setField('Id', $arrayOrg['Id']);
            $educationalOrganizationDataRes = $educationOrganization->getEducationalOrganizationData();
            $educationalOrganizationData = ($educationalOrganizationDataRes !== false) ? $educationalOrganizationDataRes->toArray() : array();
            $resultArray['branches'][$j]['field1'] = $arrayOrg['FullName'] . ', ' . $arrayOrg['ShortName'];
            $resultArray['branches'][$j]['field2'] = $arrayOrg['LawAddress'];
            $resultArray['branches'][$j]['field3'] = $arrayOrg['Kpp'];
            $resultArray['branches'][$j]['field4'] = $this->getDetailsLicenseWorkUsingInformationSecret($moreInfo,$arrayOrg['Id']);
            $resultArray['branches'][$j]['field5'] = $this->getPrhoneFaxeseducationalOrganizationData($educationalOrganizationData);
            $resultArray['branches'][$j]['field6'] = $arrayOrg['Mails'];
            $resultArray['branches'][$j]['field7'] = $this->getWwwOrganizationData($educationalOrganizationData)   ;
            $applicationPrograms                   = $currentApplication->selectComplicated("isga_ApplicationPrograms", array('fk_eiisLicensedPrograms', 'yearStartEdu', 'termEdu', 'fullTimeCount', 'partTimeCount', 'correspondenceCount', 'familyCount', 'networkForm', 'remoteTechnology', 'publicAccreditation'), array('fk_isgaApplication' => $currentApplication->getField('Id'), 'fk_isgaEOBranches' => $oneBranch['fk_eiisEOBranch']));
            $applicationPrograms                   = $applicationPrograms->toArray();
            $applicationPrograms=array_filter($applicationPrograms);
            if (count($applicationPrograms) > 0) {
                $newArrApplicationPrograms = array();
                foreach ($applicationPrograms as $oneAplicationProgram) {
                    $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
                }
                $programsIds      = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
                $programs         = $currentApplication->selectComplicated("eiis_LicensedPrograms", array('Id', 'Code', 'Name', 'fk_eiisEduProgram', 'fk_eiisEduProgramType'), array('Id' => $programsIds))->toArray();
                $programTypeIds   = array_column($programs, 'fk_eiisEduProgramType');
                $eduProgramIds    = array_column($programs, 'fk_eiisEduProgram');
                $programsTypes    = $currentApplication->selectComplicated("eiis_EduProgramTypes", array('Id', 'Name'), array('Id' => $programTypeIds))->toArray();
                $arrayTypes       = array_column($programsTypes, 'Name', 'Id');
                $eduPrograms      = 
                  $currentApplication->selectComplicated(
                    "eiis_EduPrograms",
                    array('Id', 'fk_eiisGS'),
                    array('Id' => $eduProgramIds)
                    )
                  ->toArray();
                $arrayEduPrograms =   array_column($eduPrograms, 'fk_eiisGS', 'Id');
                $isgaGSIds        = array_column($eduPrograms, 'fk_eiisGS');
                $isgaGSArray      = $currentApplication->selectComplicated("isga_GroupSpecialities", array('Id', 'Name', 'Code', 'fk_isgaEGS'), array('Id' => $isgaGSIds))->toArray();
                $isgaGSIdEGS      = array_column($isgaGSArray, 'fk_isgaEGS', 'Id');
                $isgaEGSIds       = array_column($isgaGSArray, 'fk_isgaEGS');
                $isgaEGSArray     = $currentApplication
                  ->selectComplicated(
                    "isga_EnlargedGroupSpecialities",
                    array('Id', 'Name', 'Code'),
                    array('Id' => $isgaEGSIds)
                    )
                  ->toArray();
                $isgaEGSArrayName = array_column($isgaEGSArray, 'Name', 'Id');
                $isgaGSArrayName  = array_column($isgaGSArray, 'Name', 'Id');
                $isgaEGSArrayCode = array_column($isgaEGSArray, 'Code', 'Id');
                $isgaGSArrayCode  = array_column($isgaGSArray, 'Code', 'Id');
                $i                = 0;

                if (count($programs) > 0)
                    foreach ($programs as $oneProgram) {
                        $resultArray['branches'][$j]['table1'][$i]['countcol']  = $i+1;
                        $resultArray['branches'][$j]['table1'][$i]['tabfield1']  = $oneProgram['Name'];
                        $resultArray['branches'][$j]['table1'][$i]['tabfield2']  = $arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                        $resultArray['branches'][$j]['table1'][$i]['tabfield3']  = $isgaEGSArrayCode[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                        $resultArray['branches'][$j]['table1'][$i]['tabfield4']  = $isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                        $resultArray['branches'][$j]['table1'][$i]['tabfield5']  = $this->getCodeProgram($oneProgram);
                        $resultArray['branches'][$j]['table1'][$i]['tabfield6']  = $oneProgram['Name'];
                        $resultArray['branches'][$j]['table1'][$i]['tabfield7']  = $newArrApplicationPrograms[$oneProgram['Id']]['yearStartEdu'];
                        $resultArray['branches'][$j]['table1'][$i]['tabfield8']  = $newArrApplicationPrograms[$oneProgram['Id']]['termEdu'];
                        $resultArray['branches'][$j]['table1'][$i]['tabfield9']  = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['fullTimeCount']);
                        $resultArray['branches'][$j]['table1'][$i]['tabfield10'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['partTimeCount']);
                        $resultArray['branches'][$j]['table1'][$i]['tabfield11'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['correspondenceCount']);
                        $resultArray['branches'][$j]['table1'][$i]['tabfield12'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['familyCount']);
                        $resultArray['branches'][$j]['table1'][$i]['tabfield13'] = ($newArrApplicationPrograms[$oneProgram['Id']]['networkForm'] == 1 ? 'Да' : "Нет");
                        $resultArray['branches'][$j]['table1'][$i]['tabfield14'] = ($newArrApplicationPrograms[$oneProgram['Id']]['remoteTechnology'] == 1 ? 'Да' : "Нет");
                        $resultArray['branches'][$j]['table1'][$i]['tabfield15'] = ($newArrApplicationPrograms[$oneProgram['Id']]['publicAccreditation'] == 1 ? 'Да' : "Нет");
                        ++$i;
                    }
            }
            ++$j;
        }
        
        return $resultArray;
    }

}
