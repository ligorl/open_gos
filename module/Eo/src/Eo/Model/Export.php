<?php



namespace Eo\Model;

/**
 * супер клас для создания классов формирвоания документов
 */
class Export
{

    public function getDayOfDate($dateStrInEn)
    {
        return date('d', strtotime($dateStrInEn));
    }

    public function getDayOfDateWithOutZero($dateStrInEn)
    {
        return date('j', strtotime($dateStrInEn));
    }

    public function getYearOfDate($dateStrInEn)
    {
        return date('Y', strtotime($dateStrInEn));
    }

    public function getShortYearOfDate($dateStrInEn)
    {
        return date('y', strtotime($dateStrInEn));
    }

    public function getYearShortOfDate($dateStrInEn)
    {
        return date('y', strtotime($dateStrInEn));
    }

    public function getMountOfDate($dateStrInEn)
    {
        $months = array(
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        );
        return $months[date('m', strtotime($dateStrInEn))];
    }


    public function getStrOfDate($dateStrInRu) {
        return $this->getDayOfDate($dateStrInRu)
            . ' ' . $this->getMountOfDate($dateStrInRu)
            . ' ' . $this->getYearOfDate($dateStrInRu);
    }

    public function getStrOfDateWithOutZero($dateStrInRu) {
        return $this->getDayOfDateWithOutZero($dateStrInRu)
            . ' ' . $this->getMountOfDate($dateStrInRu)
            . ' ' . $this->getYearOfDate($dateStrInRu);
    }

    /**
     * заготовка , возращает имя шаблока.
     * от {docRoot/}public/templates
     * @return string
     */
    public function getTemplateName()
    {
        return '';
    }

    /**
     * заготовка , возращает имя выходного файла.
     * имя без разширения
     *
     * @return string
     */
    public function getDocumentName()
    {
        return 'Файл';
    }

    /**
     * переводич число в текст и первую букву делаем большой
     *
     * @param type $num
     * @return string
     */
    static public function num2strFirstUp($num, $printKopeek = true, $printRubleyStr = false){
        //сумма текстом
        $result = self::num2str($num, $printKopeek, $printRubleyStr);
        //первую букву в верхний регистр
        $result = mb_strtoupper(mb_substr($result,0,1,'utf-8'),'utf-8')
                           .mb_substr($result,1,null,'utf-8');
        return $result;
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     */
    static public function num2str($num, $printKopeek = true, $printRubleyStr = true)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array(// Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                } else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                }
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = self::morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            } //foreach
        } else {
            $out[] = $nul;
        }
        if($printRubleyStr){
            $out[] = self::morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        }
        if($printKopeek){
            $out[] = $kop . ' ' . self::morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        }
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    static public function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        }
        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }
        if ($n == 1) {
            return $f1;
        }
        return $f5;
    }

    /**
     * возращаяет шаблонизатор
     *
     * @param string ourDocType - тип документа (doc, docx, docm, pdf, odt)
     *                            null или нет - по типу шаблона
     * @return \Ron\Model\Templater
     */
    private $_templater = null;
    public function getTemplater($ourDocType = null) {
        if( empty($ourDocType) ){
            $ourDocType = $this->getOurDocTypeDefault();
        }
        if(  $this->_templater !==null){
            if($ourDocType !== null && $ourDocType != $this->getOurDocType()){

            } else {
                return $this->_templater;
            }
        }
        if( $ourDocType !== null){
            $this->setOurDocType($ourDocType);
        }
        $arrayData = $this->getData();
        $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
        $templateName = $templatePath.$this->getTemplateName(); //шаблон документа
        $ourDocName=$this->getDocumentName();
        if($this->getOurDocTypeDefault() === null){
            $ourDocType = substr($templateName,-(strlen($templateName) - strrpos($templateName,'.') - 1));
            $this->setOurDocType($ourDocType);
        }
        if (!file_exists($templateName)){
            throw new \Exception('Подготовка шаблона: Файла шаблона не существует: '.$this->getTemplateName());
        }
        $templater = new \Eo\Model\Templater( $templateName,$arrayData,$ourDocType,$ourDocName );
        $templater->setDebug( $this->isDebug() );
        $this->_templater = $templater;
        return $templater;
    }

    private $_ourDocType = null;

    /**
     * установим тип выходного файла
     *
     * @param type $ourDocType
     * @return type
     */
    public function setOurDocType($ourDocType = null) {
        if( !empty($ourDocType) ){
            $ourDocType = strtolower($ourDocType);
            if( in_array($ourDocType, array('doc', 'docx', 'docm', 'pdf', 'odt', 'docm'))){
                //$ourDocType = 'docx';
                //$ourDocType = null;
                $this->_ourDocType = $ourDocType;
            }
        }
        return $this->_ourDocType;
    }

    /**
     * получим тип выходного файла
     *
     * @return type
     */
    public function getOurDocType() {
        $ourDocType = 'docx';
        if( $this->_ourDocType !== null){
            $ourDocType = $this->_ourDocType;
        }
        return $this->_ourDocType;
    }

    /**
     * получим тип само значение выходного файла
     *
     * @return type
     */
    public function getOurDocTypeDefault() {
        return $this->_ourDocType;
    }

    /**
     * возращает готовый документ
     *
     * @param type $ourDocType - тип документа (doc, docx, docm, pdf, odt)
     * @return type
     */
    public function makeDocument($ourDocType = null) {
        return $this->getTemplater($ourDocType)->makeDocument();
    }

    /**
     * отправляет заголовок и готовый документ в поток вывода
     *
     * @param type $ourDocType - тип документа (doc, docx, docm, pdf, odt)
     * @return type
     */
    public function makeDocumentOut($ourDocType = null) {
        return $this->getTemplater($ourDocType)->makeDocumentOut();
    }

    /**
     * имя выходного файла
     *
     * @param type $ourDocType - тип документа (doc, docx, docm, pdf, odt)
     * @return type
     */
    public function getFileOutName($ourDocType = null){
        return $this->getTemplater($ourDocType = null)->getFileOutName();
    }

    /**
     * заготовка данных шна шаблон
     * @return type
     */
    public function getData() {
        return array();
    }

    //опциональность, если надо
    private $_options = array();
    public function setOption($optionName, $optionValie) {
        $this->_options[$optionName] = $optionValie;
    }

    public function getOption($optionName) {
        return $this->_options[$optionName] ;
    }

    public function getOptions() {
        return $this->_options;
    }

    /**
     *
     * @param type $search    - что искать
     * @param type $replace   - чем заменти
     * @param string $subject - где
     * @return string
     */
    static public function mb_str_replace($search, $replace, $subject) {
        if ((!function_exists('mb_str_replace')) &&
                (function_exists('mb_substr')) && (function_exists('mb_strlen')) && (function_exists('mb_strpos'))) {
//                function mb_str_replace($search, $replace, $subject) {
                        if(is_array($subject)) {
                                $ret = array();
                                foreach($subject as $key => $val) {
                                        $ret[$key] = mb_str_replace($search, $replace, $val);
                                }
                                return $ret;
                        }

                        foreach((array) $search as $key => $s) {
                                if($s == '') {
                                        continue;
                                }
                                $r = !is_array($replace) ? $replace : (array_key_exists($key, $replace) ? $replace[$key] : '');
                                $pos = mb_strpos($subject, $s, 0, 'UTF-8');
                                while($pos !== false) {
                                        $subject = mb_substr($subject, 0, $pos, 'UTF-8') . $r . mb_substr($subject, $pos + mb_strlen($s, 'UTF-8'), 65535, 'UTF-8');
                                        $pos = mb_strpos($subject, $s, $pos + mb_strlen($r, 'UTF-8'), 'UTF-8');
                                }
                        }
                        return $subject;
                //}
        }
    }

    /**
     * правим ковычки на елочки
     *
     * @param type $string
     * @return string
     */
    static public function fixQuotes($string){
        $string = self::mb_str_replace('"','»', $string);
        $string = self::mb_str_replace("'",'»', $string);
        $string = self::mb_str_replace(' »',' «', $string);
        //если первая ковычка, правим
        if(  mb_substr($string, 0, 1,'UTF-8') == '»'){
            $string = '«'.mb_substr($string, 1, null,'UTF-8');
        }
        return $string;
    }

    /**
     * возращает utf8 символ по коду
     * <br>
     * 160  - нереразрывный пробел
     * <br>
     * 8194 - EN SPACE - равен половине кегля шрифта (исторически происходит от ширины заглавной буквы «N»)
     * <br>
     * 8195 - EM SPACE - равен кеглю шрифта (исторически происходит от ширины заглавной буквы «M»)
     * <br>
     * 8196 - THREE-PER-EM SPACE - ближе всех к обычному пробелу, втрое меньше, чем EM-SPACE
     * <br>
     * 171 - «
     * <br>
     * 187 - »
     * <br>
     * @param type $u - код символа в десятичном цифре
     * @return type - символ utf8
     */
    static public function unichr($u) {
        return mb_convert_encoding('&#' . intval($u) . ';', 'UTF-8', 'HTML-ENTITIES');
    }


    /**
     *
     * @param type $template
     */
    static public function prepareTemplate($template) {
        $nonBreakSpace = self::unichr(160);
         mb_regex_encoding('UTF-8');
        $template = self::mb_str_replace(' ',        '',              $template,'UTF-8');
        $template = self::mb_str_replace(PHP_EOL,    '',              $template,'UTF-8');
        $template = self::mb_str_replace("/t",       '',              $template,'UTF-8');
        $template = self::mb_str_replace('#',        '',              $template,'UTF-8');
        $template = self::mb_str_replace('_',        ' ',             $template,'UTF-8');
        $template = self::mb_str_replace('~',        $nonBreakSpace,  $template,'UTF-8');
        $template = mb_ereg_replace('\/\*.+?\*\/',   '',              $template);
        $template = mb_ereg_replace('\/\*.+?$',      '',              $template);
        $template = mb_ereg_replace('\/\/.+?(\n|\r)','',              $template);
        //preg_replace
        //header('Content-Type: text/plain');
        //die ($template);
        return $template;
    }

    /**
     * влаг отладки
     * @var type
     */
    private $_isDebug = false;

    /**
     * поднять флаг отладки
     *
     * @param type $isDebug - чеого в труе - поднять
     *                            нету/лож - снять
     * @return type
     */
    public function setDebug($isDebug = true) {
        if( empty ($isDebug) ){
            $this->_isDebug = false;
        } else {
            $this->_isDebug = true;
        }
        return $this->_isDebug;
    }

    /**
     * дергаем флаг отладки
     *
     * @return type
     */
    public function isDebug() {
        return $this->_isDebug;
    }

    static public function fixNameOrganisation($name) {
        $name = self::fixQuotes($name);
        if(  mb_ereg_search_init($name, '[А-Я]\.[А-Я]\. ') ){
            $numPos = mb_ereg_search_pos();
            if( $numPos ){
                $name1 = substr($name, 0, $numPos[0]);
                $name2 = substr($name, $numPos[0], $numPos[1]-1);
                $name3 = substr($name, $numPos[0]+$numPos[1]);
                $name = $name1 . $name2 . self::unichr(160) . $name3;
            }
        }
        $name = self::mb_str_replace('им. ', 'им.'.self::unichr(160), $name);
        return $name;
    }

    /**
     * правим адрес, вставлянем не разрывные пробелы после сокрращений
     * @param type $name
     * @return type
     */
    static public function fixAdressOrganisation($name) {
        $name = self::mb_str_replace('д. '  ,'д.'    .self::unichr(160), $name);
        $name = self::mb_str_replace('ул. ' ,'ул.'   .self::unichr(160), $name);
        $name = self::mb_str_replace('пр. ' ,'пр.'   .self::unichr(160), $name);
        $name = self::mb_str_replace('г. '  ,'г.'    .self::unichr(160), $name);
        $name = self::mb_str_replace('им. ' ,'им.'   .self::unichr(160), $name);
        $name = self::mb_str_replace('дом ' ,'дом'   .self::unichr(160), $name);
        $name = self::mb_str_replace('лит. ' ,'лит.'   .self::unichr(160), $name);
        $name = self::mb_str_replace('строение ' ,'строение'   .self::unichr(160), $name);
        //$name = self::mb_str_replace(' - '  ,self::unichr(160).'- '    , $name);
        return $name;
    }

    /**
     * возращает это тестовый ли сервер
     * 
     * @return boolean  true    - сервер тестовый
     *                  false   - сервер не тестовый
     */
    static public function isTestServer(){
        $host = $_SERVER['HTTP_HOST'];
        if(substr_count($host, 'localhost') > 0 || 'f11' == $host || 'f11.' == substr($host, 0, 4) || $host =='85.192.47.4' || $host =='146.120.224.100' || $host =='gos-open.loc' || $host =='open3:8080'){
            return true;
        }
        return false;
    }
    
    
    /**
     * приводит перечень к виду
     * номер            - если один элемент
     * номер,номер      - если два элемента
     * первый-послединй - если больше 2х
     * @param type $numArray
     * @return string
     */
    public static function numberingDiadozon( $numArray = array() ){
        $result = '';
        $resultArray = array();
        $numArray = array_unique($numArray);
        sort($numArray);
        if(count($numArray) == 1){
            $result = reset($numArray);
        }
        if(count($numArray) == 2){
            $result = reset($numArray).', '.end($numArray);
        }
        if(count($numArray) > 2){
            $prev = reset($numArray);
            $newArray = array($prev);
            while ( ($next = next($numArray)) !== false ){
                if( ($prev+1) == $next ){
                    $newArray[] = $next;
                } else {
                    $resultArray[] = self::numberingDiadozon($newArray);
                    $newArray = array($next);
                }
                $prev = $next;
            }
            if(count($newArray) ==  count($numArray)){
                $resultArray[] = reset($numArray).'-'.end($numArray);;
            }else {
                $resultArray[] = self::numberingDiadozon($newArray);
            }
            $result = implode(', ', $resultArray);
        }
        return $result;
    }

    
}