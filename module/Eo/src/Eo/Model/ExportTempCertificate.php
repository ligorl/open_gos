<?php

namespace Eo\Model;

use Model\Gateway\EntitiesTable;
use Model\Entities\eiisEducationalOrganizations;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExportDublicateCertificate
 *
 * @author sany
 */
class ExportTempCertificate extends ExportDocument {

    public  function getTablesOut (){
      return array('table1'=>'tabfield1','table2'=>'tabfield2');
    }

    function getType()
    {
        return  'TempCertificate';
    }

    public function getData()
    {
        //$applicationsClass  = $this->applicationsClass;
        $currentApplication = $this->currentApplication;
        $monthArray         = $this->monthArray;
        $resultArray        = $this->resultArray;

        $certificate = $this->getCertificate();
        $MainOrg     = $this->mainOrg;
        $arrayOrg    = $MainOrg->toArray();
        $moreInfo = $this->moreInfo;

        $reorgOrganNames = array();
        $isga_mtm_Application_Organizations = $currentApplication->selectComplicated("isga_mtm_Application_Organizations", array('fk_eiisEducationalOrganization'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        if ($isga_mtm_Application_Organizations) {
            $reorgOrganIDs = array_column($isga_mtm_Application_Organizations, 'fk_eiisEducationalOrganization');
            $reorgOrgans =  $isgaEGSArray = $currentApplication->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName'), array('Id' => $reorgOrganIDs))->toArray();
            $reorgOrganNames = array_column($reorgOrgans, 'FullName');
        }

        $resultArray['fieldFullName']  = $arrayOrg['FullName'] . ', ' . $arrayOrg['ShortName'];
        $resultArray['fieldLawAddress']  = $arrayOrg['LawAddress'];
        $resultArray['fieldGosRegNum']  = $arrayOrg['GosRegNum'];
        $resultArray['fieldInn']  = $arrayOrg['Inn'];
        $resultArray['fieldKpp']  = $arrayOrg['Kpp'];
        $resultArray['fieldFullNameOnly']  = $arrayOrg['FullName'];
        $resultArray['fieldReason']  = $this->getApplicationReason('Name');
        $resultArray['fieldReorgOrganNames'] = implode(', ', $reorgOrganNames);
        $resultArray['field10'] = $this->getRequisitesCertificate($certificate);
        $resultArray['field11'] = 'Федеральная служба по надзору в сфере образования и науки ';
        $resultArray['fieldPhones']  = $arrayOrg['Phones'];
        $resultArray['fieldMails']  = $arrayOrg['Mails'];
        $resultArray['fieldWww']  =  $this->ifNotEmpty( $arrayOrg['Www']);
        $resultArray['fieldI'] = $this->ifActive($moreInfo['Information']);
        $resultArray['fieldInformationEmail'] = $this->conditionSymbolActive($moreInfo['Information']) ? $moreInfo['Email'] : ' ';
        $resultArray['fieldINo'] = $this->ifNotActive($moreInfo['Information']);
        $resultArray['fieldChargePosition'] = $arrayOrg['ChargePosition'];
        $resultArray['fieldFIOContactManager'] = $this->getFIOContactManager($arrayOrg);
        $resultArray=$this->dateParse($resultArray);
        $resultArray['shtrihKod'] = $this->getShtrihKod($currentApplication->getField('Barcode'), $currentApplication->getField('fk_isgaApplicationStatus'));

        $applicationPrograms       = $currentApplication
            ->selectComplicated(
                "isga_ApplicationPrograms", 
                array(
                    'fk_eiisLicensedPrograms',
                    'yearStartEdu',
                    'termEdu',
                    'fullTimeCount',
                    'partTimeCount',
                    'correspondenceCount',
                    'familyCount',
                    'networkForm',
                    'remoteTechnology',
                    'publicAccreditation'
                ), 
                array(
                    'fk_isgaApplication' => $currentApplication
                    ->getField('Id'),
                    'fk_isgaEOBranches'  => $arrayOrg['Id'])
                )->toArray();
        $newArrApplicationPrograms = array();
        foreach ($applicationPrograms as $oneAplicationProgram) {
            $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
        }
        $programsIds = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
        $programsIds=array_filter($programsIds);
        if (sizeof($programsIds) != 0) {
            $programs    = $currentApplication->selectComplicated(
                    "eiis_LicensedPrograms",
                    array(
                        'Id', 
                        'Code',
                        'Name',
                        'fk_eiisEduProgram',
                        'fk_eiisEduProgramType'
                    ),
                    array('Id' => $programsIds)
                )->toArray();

            $programTypeIds   = array_column($programs, 'fk_eiisEduProgramType');
            $eduProgramIds    = array_column($programs, 'fk_eiisEduProgram');
            $programsTypes    = $currentApplication->selectComplicated("eiis_EduProgramTypes", array('Id', 'Name'), array('Id' => $programTypeIds))->toArray();
            $arrayTypes       = array_column($programsTypes, 'Name', 'Id');
            $eduPrograms      = $currentApplication->selectComplicated("eiis_EduPrograms", array('Id', 'fk_isgaGS'), array('Id' => $eduProgramIds))->toArray();
            $arrayEduPrograms = array_column($eduPrograms, 'fk_isgaGS', 'Id');
            $isgaGSIds        = array_column($eduPrograms, 'fk_isgaGS');
            $isgaGSArray      = $currentApplication->selectComplicated("isga_GroupSpecialities", array('Id', 'Name', 'Code', 'fk_isgaEGS'), array('Id' => $isgaGSIds))->toArray();
            $isgaGSIdEGS      = array_column($isgaGSArray, 'fk_isgaEGS', 'Id');
            $isgaEGSIds       = array_column($isgaGSArray, 'fk_isgaEGS');
            $isgaEGSArray     = $currentApplication->selectComplicated("isga_EnlargedGroupSpecialities", array('Id', 'Name', 'Code'), array('Id' => $isgaEGSIds))->toArray();
            $isgaEGSArrayName = array_column($isgaEGSArray, 'Name', 'Id');
            $isgaGSArrayName  = array_column($isgaGSArray, 'Name', 'Id');
            $isgaEGSArrayCode = array_column($isgaEGSArray, 'Code', 'Id');
            $isgaGSArrayCode  = array_column($isgaGSArray, 'Code', 'Id');

            $eduLevel = array();
            $arrayLevel = array_column($programsTypes, 'fk_eiisEduLevels', 'Id');
            if ($arrayLevel) {
                $arrayLevel = array_unique(array_filter($arrayLevel));
                if ($arrayLevel) {
                    $eduLevel = $currentApplication->selectComplicated("eiis_EduLevels",
                                                                       array('Id', 'Name', 'ShortName', 'EduProgramKind'),
                                                                       array('Id' => $arrayLevel))->toArray();
                }
            }

            $i1 = $i2 = 0;
            $array_unique1 = $array_unique2 = array();
            foreach ($programs as $oneProgram) {
                if (isset($oneProgram['Id']) && isset($eduLevel[$oneProgram['Id']]) && $eduLevel[$oneProgram['Id']] == 'Basic') {
                    $name = $arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                    if (!in_array($name, $array_unique1)) {
                        $resultArray['table1'][$i1]['countcol']=$i1+1;
                        $resultArray['table1'][$i1]['tabfield1'] = $name;
                        $array_unique1[] = $name;
                        ++$i1;
                    }
                } else {
                    $code = $isgaEGSArrayCode[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                    $code = $code=='Void'?' ':$code;
                    if (!in_array($code, $array_unique2)) {
                        $resultArray['table1'][$i2]['countcol']=$i2+1;
                        $resultArray['table2'][$i2]['tabfield2'] = $code;
                        $resultArray['table2'][$i2]['tabfield3'] = $isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                        $resultArray['table2'][$i2]['tabfield4'] = $arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                        $array_unique2[] = $code;
                        ++$i2;
                    }
                }
            }
        }

        $branchesApplication = $currentApplication->selectComplicated("isga_mtm_Application_Branches", array('fk_eiisEOBranch'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        $j                   = 0;
        $educationOrganization = new eiisEducationalOrganizations();
        if (isset($branchesApplication)) {
            foreach ($branchesApplication as $oneBranch) {
                // пропускаем главный вуз, чтобы
                if ($oneBranch['fk_eiisEOBranch'] === $arrayOrg['Id']) {
                    continue;
                }
                $MainOrg                               = $currentApplication->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $oneBranch['fk_eiisEOBranch']));
                $arrayOrg                              = $MainOrg->toArray();
                $educationOrganization->setField('Id', $arrayOrg['Id']);
                $educationalOrganizationDataRes = $educationOrganization->getEducationalOrganizationData();
                $educationalOrganizationData = ($educationalOrganizationDataRes !== false) ? $educationalOrganizationDataRes->toArray() : array();
                $resultArray['branches'][$j]['field1'] = $arrayOrg['FullName'] . ', ' . $arrayOrg['ShortName'];
                $resultArray['branches'][$j]['field2'] = $arrayOrg['LawAddress'];
                $resultArray['branches'][$j]['field3'] = $arrayOrg['Kpp'];
                $resultArray['branches'][$j]['field4'] = $this->getPrhoneFaxeseducationalOrganizationData($educationalOrganizationData);
                $resultArray['branches'][$j]['field5'] = $arrayOrg['Mails'];
                $resultArray['branches'][$j]['field6'] = ($educationalOrganizationData) ? $educationalOrganizationData['Site'] : '';
                $applicationPrograms                   = $currentApplication->selectComplicated("isga_ApplicationPrograms", array('fk_eiisLicensedPrograms', 'yearStartEdu', 'termEdu', 'fullTimeCount', 'partTimeCount', 'correspondenceCount', 'familyCount', 'networkForm', 'remoteTechnology', 'publicAccreditation'), array('fk_isgaApplication' => $currentApplication->getField('Id'), 'fk_isgaEOBranches' => $oneBranch['fk_eiisEOBranch']));
                $applicationPrograms                   = $applicationPrograms->toArray();
                $applicationPrograms=array_filter($applicationPrograms);
                if (count($applicationPrograms) > 0) {
                    $newArrApplicationPrograms = array();
                    foreach ($applicationPrograms as $oneAplicationProgram) {
                        $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
                    }
                    $programsIds      = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
                    $programs         = $currentApplication->selectComplicated("eiis_LicensedPrograms", array('Id', 'Code', 'Name', 'fk_eiisEduProgram', 'fk_eiisEduProgramType'), array('Id' => $programsIds))->toArray();
                    $programTypeIds   = array_column($programs, 'fk_eiisEduProgramType');
                    $eduProgramIds    = array_column($programs, 'fk_eiisEduProgram');
                    $programsTypes    = $currentApplication->selectComplicated("eiis_EduProgramTypes", array('Id', 'Name'), array('Id' => $programTypeIds))->toArray();
                    $arrayTypes       = array_column($programsTypes, 'Name', 'Id');
                    $eduPrograms      = $currentApplication->selectComplicated("eiis_EduPrograms", array('Id', 'fk_eiisGS'), array('Id' => $eduProgramIds))->toArray();
                    $arrayEduPrograms = array_column($eduPrograms, 'fk_eiisGS', 'Id');
                    $isgaGSIds        = array_column($eduPrograms, 'fk_eiisGS');
                    $isgaGSArray      = $currentApplication->selectComplicated("isga_GroupSpecialities", array('Id', 'Name', 'Code', 'fk_isgaEGS'), array('Id' => $isgaGSIds))->toArray();
                    $isgaGSIdEGS      = array_column($isgaGSArray, 'fk_isgaEGS', 'Id');
                    $isgaEGSIds       = array_column($isgaGSArray, 'fk_isgaEGS');
                    $isgaEGSArray     = $currentApplication->selectComplicated("isga_EnlargedGroupSpecialities", array('Id', 'Name', 'Code'), array('Id' => $isgaEGSIds))->toArray();
                    $isgaEGSArrayName = array_column($isgaEGSArray, 'Name', 'Id');
                    $isgaGSArrayName  = array_column($isgaGSArray, 'Name', 'Id');
                    $isgaEGSArrayCode = array_column($isgaEGSArray, 'Code', 'Id');
                    $isgaGSArrayCode  = array_column($isgaGSArray, 'Code', 'Id');

                    $eduLevel = array();
                    $arrayLevel = array_column($programsTypes, 'fk_eiisEduLevels', 'Id');
                    if ($arrayLevel) {
                        $arrayLevel = array_unique(array_filter($arrayLevel));
                        if ($arrayLevel) {
                            $eduLevel = $currentApplication->selectComplicated("eiis_EduLevels",
                                                                               array('Id', 'Name', 'ShortName', 'EduProgramKind'),
                                                                               array('Id' => $arrayLevel))->toArray();
                        }
                    }

                    if (count($programs) > 0) {
                        $i1 = $i2 = 0;
                        $array_unique1 = $array_unique2 = array();
                        foreach ($programs as $oneProgram) {
                            if (isset($oneProgram['Id']) && isset($eduLevel[$oneProgram['Id']]) && $eduLevel[$oneProgram['Id']] == 'Basic') {
                                $name = $arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                                if (!in_array($name, $array_unique1)) {
                                    $resultArray['branches'][$j]['table1'][$i1]['countcol']  = $i1+1;
                                    $resultArray['branches'][$j]['table1'][$i1]['tabfield1'] = $name;
                                    $array_unique1[] = $name;
                                    ++$i1;
                                }
                            } else {
                                $code = $isgaEGSArrayCode[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                                $code = $code=='Void'?' ':$code;
                                if (!in_array($code, $array_unique2)) {
                                    $resultArray['branches'][$j]['table1'][$i2]['countcol']  = $i2+1;
                                    $resultArray['branches'][$j]['table2'][$i2]['tabfield2'] = $code;
                                    $resultArray['branches'][$j]['table2'][$i2]['tabfield3'] = $isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                                    $resultArray['branches'][$j]['table2'][$i2]['tabfield4'] = $arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                                    $array_unique2[] = $code;
                                    ++$i2;
                                }
                            }
                        }
                    }
                }

                ++$j;
            }
        }

        return $resultArray;
    }

}
