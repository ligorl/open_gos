<?php

namespace Eo\Model;
/*
 * суперклас, формиоующий платежку в файл док/пдф
 */
class ExportPayment
{

    /*
     * фабрика
     */
    static function getPaymentByIdApplication($appId)
    {
        $application = new \Model\Entities\isgaApplications($appId);
        //Если такой заявки нет в базе
        if ($application->getField("Id") == null) {
            return null;
        }
        //определяемся какого вида платежку надо собрать
        //$appType = $application->getType();
        //$appTypeCode=$appType->getCode();
        //$appType = $application->getField("fk_isgaApplicationType");
        $appReason = $application->getField("fk_isgaApplicationReason");
        $appReasons = $application->getReasonArray();
        if ( empty($appReason)) {
            return null;
        }
        $appReasonIds = array_column($appReasons, 'Id');
        

        if ( in_array('24e060cb-6384-4009-b9ce-93a775678732',$appReasonIds)
          || in_array('67f97827-57f9-4a0c-b2c7-d9806969bef6',$appReasonIds)
                ){
            //на каждую программу
            return new \Eo\Model\ExportPaymentNewEduProgram($application);
        } else {
            //одна на заявление
            return new \Eo\Model\ExportPaymentOther($application);
        }
    }

    protected $application;
    protected $mainOrg;

    /*
     * конструктор платежке
     * @param $application - класс заявления
     */

    function __construct($application)
    {
        $this->application = $application;
        //$this->mainOrg= $this->application->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $this->application->getField("fk_eiisEducationalOrganization")));
        $this->mainOrg = new \Model\Entities\eiisEducationalOrganizations($this->application->getField("fk_eiisEducationalOrganization"));
        //======Обрабатываем, подготавливаем инфо для калькулятора
        //$this->branches=$this->fillEductionProgramToBranches();
        //$d=$this->doSumm();
        return true;
    }

    /*
     * Формирует массив филиалов из заявлени
     *
     * @return - массив филиалов, с ключем по ид филиала
     */
    public function getBranches()
    {
        $retArray = array();
        $branches = $this->application->getLinkedBranches();
        //Проходим по филиалам, раскидываем в массив
        foreach ($branches->getObjectsArray() as $branch) {
            $retArray[$branch->getField("Id")] = array(
                "Id"       => $branch->getField("Id"),
                "Programs" => array(),
            );
        }
        return $retArray;
    }

    /*
     * в филиалы заливаются образовательные программы из заявлений
     * данные дергаются из getBranches()
     *
     * @return array - массив филиалов по ключам с образовательными
     *      программами по ключам
     */
    public function fillEductionProgramToBranches()
    {
        $retArray = $this->getBranches();
        //Запрашиваем связанные с заявкой обр программы
        $progams = $this->application->getLinkedPrograms();
        //Проходим по программам, раскидываем их по филиалам
        foreach ($progams->getObjectsArray() as $program) {
            $eoId = $program->getField("fk_isgaEOBranches");
            if (isset($retArray[$eoId])) {
                $retArray[$eoId]["Programs"][$program->getField('fk_eiisLicensedPrograms')] = $program;
            }
        }
        return $retArray;
    }


    /*
     * Заполнение полей в документе
     */
    public function templateMakeValue($valueArray, $templateProcessor)
    {
        foreach ($valueArray as $key => $val) {
            if (is_string($val) || is_numeric($val)) {
                $templateProcessor->setValue($key, htmlspecialchars($val, ENT_COMPAT, 'UTF-8'), 1);
            }
        }
        return $templateProcessor;
    }

    /*
     * Форирование документа из шаблона
     *
     * @param $arrayData массив входных данных
     * @param $templateDocPath - полный путь к файлу шаблона
     * @param $outType - тип фыходного документа
     */
    public function templateMakeDoccument($arrayData, $templateDocPath, $outType = 'docx')
    {
        //создаем временное хранилище
        $tempDir = sys_get_temp_dir() . '/' . uniqid() . '/';
        if (!mkdir($tempDir, 0777, true)) {
            return null;
        }
        ///Берем класс шаблонизатора
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateDocPath);

        //Заполняем филиалы
        if (!empty($arrayData['PAYMENT']) && is_array($arrayData['PAYMENT'])) {
            $templateProcessor->cloneBlock('PAYMENT', count($arrayData['PAYMENT']));
            foreach ($arrayData['PAYMENT']as $branchKey => $branchValue) {
                //вставляем значения полей
                $templateProcessor = $this->templateMakeValue($branchValue, $templateProcessor);
                //Заполняем табличку филиалов.
                //$templateProcessor=$this->templateMakeTable($branchValue, $this->getTablesOut(), $templateProcessor);
            }
        } else {
            $templateProcessor->cloneBlock('PAYMENT', 0);
        }
        //Сохраняем полученый документ и готовим к отправке
        $fileTempPath = $tempDir . 'out.docx';
        $templateProcessor->saveAs($fileTempPath);
        $result = '';
        if (file_exists($fileTempPath)) {
            $outTypes = array('pdf', 'doc', 'odt');
            if (in_array($outType, $outTypes)) {
                $s = '/usr/bin/libreoffice5.0 '
                  . ' --headless --convert-to ' . $outType . ' --nofirststartwizard '
                  . ' --nologo --norestore --invisible '
                  . ' --outdir ' . $tempDir
                  . ' ' . $fileTempPath;
                \Eo\Model\Templater::exec( $s );
                $fileTempConvert = $tempDir . 'out.' . $outType;
                if (file_exists($fileTempConvert)) {
                    $result = file_get_contents($fileTempConvert);
                    @unlink($fileTempConvert);
                }
            } else {
                $result = file_get_contents($fileTempPath);
            }
            unset($outTypes);
            @unlink($fileTempPath);
        }
        unset($fileTempPath);
        //Удаляем картинку штрихкода
        if (isset($strihFilePath)) {
            if (file_exists($strihFilePath)) {
                @unlink($strihFilePath);
            }
            unset($strihFilePath);
        }
        if (is_dir($tempDir)) {
            @rmdir($tempDir);
        }
        unset($tempDir);
        return $result;
    }

    /*
     * метод формированяия данных
     * @return - массив с данными
     * перегружать
     */
    function getData()
    {
       return array();
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     */
    function num2str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array(// Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                } else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                }
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            } //foreach
        } else {
            $out[] = $nul;
        }
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . $this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        }
        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }
        if ($n == 1) {
            return $f1;
        }
        return $f5;
    }

    /**
     * сбить массив платежки
     *
     * @param $eduLevel - ид уровень образовательной программы
     * @param $isBranch - true ели филиал
     * @param $eduProgramCount - количество программ данного уровня (умножается)
     * @param $paymentArray - входные данные, если нет черпать из базы
     */
    public function getPayment($eduLevel = null, $isBranch = false, $filialId=false, $eduProgramCount = 1, $paymentArray = null)
    {
        //заливаем стандартные поля
        $result = array();
        $mainOrg = $this->mainOrg->toArray();

        //готовим данные черпнуть и з базы/массива
        //$appTypeId = $this->application->getType()->getId();
        //$appReasonId = $this->application->getField('fk_isgaApplicationReason');

        if (isset($paymentArray)&&  is_array($paymentArray)) {
            //берем из присланного массива
            if(isset($paymentArray[$eduLevel])){
                $payment = $paymentArray[$eduLevel];
            }
        } 
        if(empty($payment)){
            //черпаем из базы
            //$appReasonId = $this->application->getField('fk_isgaApplicationReason');
            $appReason = $this->application->getReasonArray();
            $appReasonId = array_column($appReason, 'Id');       
            $appReasonId =  $appReasonId[0];
            $payment = new \Model\Entities\isgaApplicationPayments();
            $payment = $payment->getPayment(null, $appReasonId, $eduLevel);
            if (empty($payment)) {
                throw new \Exception('не найдена запмсь о цене для данного типа/причины заявлений '
                  ."\n код причины:".$appReasonId
                  ."\n код уровня:".$eduLevel
                  );
            }
            $payment = $payment->toArray();
        }
        //подготовка полей
        $price = $payment['Price'] * $eduProgramCount;
        $code = $payment['Code'];
        $comment = $payment['Comment'];
        //заливаем поля
        $result['fieldInn'] = $mainOrg['Inn'];
        $result['fieldKpp'] = $mainOrg['Kpp'];
        $result['fieldPayment'] = $mainOrg['FullName'];
        $result['fieldSumNumber'] = number_format($price, 0, ',', ' ');
        $result['fieldSumLeter'] = $this->num2str($price);
        $result['fieldUin'] = '0771'
          . $code
          . $mainOrg['Inn']
          . ($isBranch ? '1' : '0')
          . '039';
        $result['fieldComment'] = $comment;
        $result['fieldUinDubl'] = $result['fieldUin'];

        $EdeOrgData=$this->mainOrg->getEducationalOrganizationData();
        $result['fieldBankNumber']=$EdeOrgData->getField('BankAccountNumber');
        $result['fieldBankName']=$EdeOrgData->getField('BankName');
        $result['fieldBankAccount']=$EdeOrgData->getField('BankCorrespondentAccount');
        $result['fieldBankBik']=$EdeOrgData->getField('BankBik');

        if($filialId===false){
            $filial=$this->mainOrg;
        }else{
            $filial= new \Model\Entities\eiisEducationalOrganizations($filialId);
        }
        $result['fieldFilialId']=$filial->getId();
        $result['fieldFilialShortName']=$filial->getField('ShortName');
        $result['fieldFilialRegularName']=$filial->getField('RegularName');

        return $result;
    }

}