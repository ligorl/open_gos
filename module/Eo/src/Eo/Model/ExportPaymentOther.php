<?php

namespace Eo\Model;

class ExportPaymentOther extends ExportPayment
{

    function getData()
    {
        /*
        $result = array();
        $mainOrg = $this->mainOrg->toArray();
        $result['fieldInn'] = $mainOrg['Inn'];
        $result['fieldKpp'] = $mainOrg['Kpp'];
        $result['fieldPayment'] = $mainOrg['FullName'];
        $appType = $this->application->getField('fk_isgaApplicationType');
        $appReason = $this->application->getField('fk_isgaApplicationReason');
        $payment = new \Model\Entities\isgaApplicationPayments();
        $payment = $payment->getPayment($appType, $appReason);
        if (empty($payment)) {
            return null;
        }
        $result['fieldSumNumber'] = number_format($payment->getPrice(), 0, ',', ' ');
        $result['fieldSumLeter'] = $this->num2str($payment->getPrice());
        $result['fieldUin'] = '0771' . $payment->getCode() . $mainOrg['Inn'] . '0' . '039';
        $result['fieldComment'] = $payment->getComment();
        $result['fieldUinDubl'] = $result['fieldUin'];
        */
        $result=$this->getPayment();
        return array('PAYMENT' => array($result));
    }

}