<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class Reorg extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "reorg";
    protected $title = "Реорганизация";

    protected function getViewData(){
        $viewData = [''];
        $viewData["reorgs"] = $this->request->getReorganizationsData();
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
            "btAddOrg" => "Добавить организацию"
        ];
    }
}