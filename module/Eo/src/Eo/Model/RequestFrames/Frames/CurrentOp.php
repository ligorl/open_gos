<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class CurrentOp extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "current-op";
    protected $title = "Действующие ОП";

    protected function getViewData(){
        $viewData["request"] = $this->request;
        //Список филиалов
        $viewData["filials"] = $this->request->getFilials();

        return $viewData;
    }

    public function getButtons(){
        return [
            //"btAddOp" => "Добавить",
            "selectOp" => "Выбрать"
        ];
    }
}