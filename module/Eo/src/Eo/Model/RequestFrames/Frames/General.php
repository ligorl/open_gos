<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;
use Model\LodEntities\RequestOrganizationInfo;

class General extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "general";
    protected $title = "Свойства заявления";

    private $options = [
        "ChoiseSupp"        => 0
    ];

    protected function getViewData(){
        $viewData = [];

        //Заявление
        $viewData["request"] = $this->request;
        $viewData["org"] = $this->request->getEducationalOrganization();
        $viewData["prescriptions"] = count($viewData["org"]->getPrescriptions());
        //передаем айди типа процедуры
        $viewData["procedureTypeId"] = $this->request->getField("licensingProcedureId");
        //передаем массив с айлишниками причин
        $reasons = $this->request->getLicenseReasons();
        $reasonIds = [];
        foreach($reasons as $reason){
            $reasonIds[] = $reason->getId();
        }
        $viewData["reasonIds"] = $reasonIds;

        //Организация
        $orgInfoTable = new Gateway\LodEntitiesTable("RequestOrganizationInfo");
        $orgInfos = $orgInfoTable->getAllEntities(null,null,[
            "requestId" => $this->request->getId(),
            "orgId" => $this->request->getField("organizationId")
        ])->getObjectsArray();
        if(count($orgInfos) > 0) {
            $viewData["orgInfo"] = $orgInfos[0];
        } else {
            $orgInfo = new RequestOrganizationInfo();

            $orgInfo->setFields([
                "requestId" => 	$this->request->getId(),
                "orgId" => $viewData["org"]->getId(),
                "fullTitle" => $viewData["org"]->getField("FullName"),
                "shortTitle" => $viewData["org"]->getField("ShortName"),
                "kpp" => $viewData["org"]->getField("Kpp"),
                "ogrn" => $viewData["org"]->getField("GosRegNum"),
                "inn" => $viewData["org"]->getField("Inn"),
                "fk_eiisEducationalOrganizationProperties" => $viewData["org"]->getField("fk_eiisEducationalOrganizationProperties"),
                "licenseePhone" => $viewData["org"]->getField("Phones"),
                "licenseeEmail" => $viewData["org"]->getField("Mails"),
                "address" => $viewData["org"]->getField("Address"),
                "additionalInfo" => "base"
            ]);
            $viewData["orgInfo"] = $orgInfo;
        }

        //Орг Организационно правовые формы
        $propertiesTable = new Gateway\EntitiesTable("eiis_EducationalOrganizationProperties");
        $eduProperties = $propertiesTable->getAllEntities()->getObjectsArray();
        $viewData["propertiesList"] = $eduProperties;

        //Орг должности глав
        $postTable = new Gateway\LodEntitiesTable("employeepost");
        $posts = $postTable->getAllEntities(null, "title ASC");
        $viewData["posts"]=$posts;

        //Субфреймы
        $subframes = [];
        foreach($this->options as $frame=>$enabled){
            if($enabled){
                $frameName = "Eo\\Model\\RequestFrames\\Frames\\".$frame;
                $frameObj = new $frameName($this->request, $this->sm);
                $subframes[$frame] = $frameObj->getView();
            }
        }
        $viewData["subframes"]=$subframes;

        //Оплаты
        $payments = $this->request->getPayments();
        $viewData["payments"]=$payments;
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
        ];
    }

    public function setOptions($options){
        $this->options = $options;
    }
}