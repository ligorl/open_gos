<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class NewFilial extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "new-filial";
    protected $title = "Открытие нового филиала";

    protected function getViewData(){
        $viewData = [];

        //Список филиалов
        $viewData["filials"] = $this->request->getFilials("new");
        $viewData["orgId"] = $this->request->getField("organizationId");
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
            "btAddFilial" => "Добавить филиал"
        ];
    }
}