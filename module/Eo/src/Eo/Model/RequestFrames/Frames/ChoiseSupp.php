<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class ChoiseSupp extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "choise-supp";
    protected $title = "Выбор действующих приложений к лицензии";

    protected function getViewData(){
        $viewData = [];

        //Подготавливаем приложения
        $supplements = $this->request->getSupplements();
        //распихиваем приложения по лицензиям
        $suppByLic = [];
        foreach($supplements as $supp){
            $licenseId = $supp->getField("fk_eiisLicense");
            if(!isset($suppByLic[$licenseId]))
                $suppByLic[$licenseId]=[];

            $suppByLic[$licenseId][] = $supp;
        }

        //подготавливаем список организаций
        $orgs = [];
        //базовая организация
        $org = $this->request->getEducationalOrganization();
        $orgs[] = $org;

        //берем организации чтобыли добавлены для реорг
        /*
        $reorgs = $this->request->getReorganizationsData();
        $orgsById = [];
        foreach($reorgs as $reorg){
            $reOrg = $reorg->getReorganizedOrganization();

            if(!isset($orgsById[$reOrg->getId()])){
                $orgs[] = $reOrg;
                $orgsById[$reOrg->getId()] = true;
            }
        }
*/

        //подготавливаем инфо для множественного выбора
        $licensesData = [];

        //добавляем лицензии
        $reqDeal = $this->request->getDeal();

        $licenses = [];
        $licById = [];

        //Первая лицензия - связанная с заявлением
        $reqLicense = $this->request->getLicense();
        if($reqLicense!=null){
            $licenses[] = $reqLicense;
            $licById[$reqLicense->getId()] = true;
        }


        foreach($orgs as $org){
            $license = $org->getLicense();
            if($license == null || isset($licById[$license->getId()])) {
                continue;
            }
            $licenses[] = $license;
            $licById[$license->getId()] = true;
        }

        foreach($licenses as $license){
            $supps = [];
            if(isset($suppByLic[$license->getId()]))
                $supps = $suppByLic[$license->getId()];


            $licensesData[] = [
                "license" => $license,
                "supplements" => $supps
            ];
        }

        $viewData['licensesData'] =  $licensesData;
        $viewData["request"] = $this->request;

        return $viewData;
    }
}