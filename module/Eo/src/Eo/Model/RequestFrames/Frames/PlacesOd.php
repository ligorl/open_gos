<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class PlacesOd extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "places-od";
    protected $title = "Места осуществления ОД";

    protected function getViewData(){
        $viewData = [];

        //Список филиалов
        $org = $this->request->getEducationalOrganization();
        $viewData["filials"] = $this->request->getFilials();
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
            "btChoiseAddr" => "Выбрать адрес",
            "btAddAddr" => "Добавить адрес"
        ];
    }
}