<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class StopOp extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "stop-op";
    protected $title = "Прекращение реализации ОП";

    protected function getViewData(){
        $viewData["request"] = $this->request;
        //Список филиалов
        $viewData["filials"] = $this->request->getFilials();        

        return $viewData;
    }

    public function getButtons(){
        return [
            //"btAddOp" => "Добавить",
            "selectOp" => "Выбрать"
        ];
    }
}