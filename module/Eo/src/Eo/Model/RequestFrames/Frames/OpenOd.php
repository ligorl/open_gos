<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class OpenOd extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "open-od";
    protected $title = "Открытие ОД по адресам";

    protected function getViewData(){
        $viewData = [];

        //Список филиалов
        $org = $this->request->getEducationalOrganization();
        $viewData["filials"] = $this->request->getFilials();
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
            "btAddAddr" => "Добавить адрес"
        ];
    }
}