<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class Filial extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "filial";
    protected $title = "Филиал";
    private $filials;

    private function cmp($a, $b)
    {
        $nameA = $a->getField("fullTitle");
        $nameB = $b->getField("fullTitle");
        if ($nameA == $nameB) {
            return 0;
        }
        return ($nameA < $nameB) ? -1 : 1;
    }

    public function __construct($request, $sm){
        parent::__construct($request, $sm);
        //Список филиалов
        $filials = $this->request->getFilials("filials-only")->getObjectsArray();

        //сортируем
        usort($filials, array($this, "cmp"));

        $this->filials = $filials;

        //Если нет филиалов у организации, то не показываем вкладку
        /*
        $reqOrg = $this->request->getEducationalOrganization();
        $orgFilials = $reqOrg->getValidAffilates();

        if(count($orgFilials)==0){
            $this->visible=false;
        }
        */
    }



    protected function getViewData(){
        $viewData = [];

        $viewData["filials"] = $this->filials;
        $viewData["orgId"] = $this->request->getField("organizationId");
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
            "btAddFilial" => "Добавить филиал"
        ];
    }

}