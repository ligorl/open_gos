<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;
use Model\LodEntities\RequestOrganizationInfo;

class ChangeLicTitle extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "change-lic-title";
    protected $title = "Изменение наименования лицензиата";

    protected function getViewData(){
        $viewData = [];

        //Список филиалов
        $filials = $this->request->getFilials("edit")->getObjectsArray();

        //Сортируем филиалы по организации
        $filialsByOrgId = array();
        $cnt = 0;
        foreach($filials as $filial){
            if(!isset($filialsByOrgId[$filial->getField("orgId")]))
                $filialsByOrgId[$filial->getField("orgId")] = [
                    "edit_from" => null,
                    "edit_to"   => null
                ];
            //Для старых филиалов

            if($filial->isEmptyField("additionalInfo")){

                $filial->setField("additionalInfo","edit_from");
                $filial->setField("stateCode","editing");
                $filial->save();
                $filialsByOrgId[$filial->getField("orgId")]["edit_from"] = $filial;


                $newFields =  $filial->getFields();
                $newFields["additionalInfo"] = "edit_to";
                $newFields["stateCode"] = "editing";
                unset($newFields["id"]);
                $newFilial = new RequestOrganizationInfo();
                $newFilial->setFields($newFields);
                $newFilial->save();
                $filialsByOrgId[$filial->getField("orgId")]["edit_to"] = $newFilial;
            }
            //Для новых филиалов
            else{
                $filialsByOrgId[$filial->getField("orgId")][$filial->getField("additionalInfo")] = $filial;
                $cnt++;
            }

        }

        $viewData["filials"] = $filialsByOrgId;

        $viewData["orgId"] = $this->request->getField("organizationId");
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
            "btAddFilial" => "Выбрать Филиал"
        ];
    }
}