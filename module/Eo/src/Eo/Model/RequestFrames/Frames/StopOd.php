<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class StopOd extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "stop-od";
    protected $title = "Прекращение ОД по адресам";

    protected function getViewData(){
        $viewData = [];

        //Список филиалов
        $org = $this->request->getEducationalOrganization();
        $viewData["filials"] = $this->request->getFilials();
        $viewData["request"] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
            "btChoiseAddr" => "Выбрать адрес",
        ];
    }
}