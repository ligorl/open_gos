<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class ChangeOpName extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "change-op-name";
    protected $title = "Изменение наименования ОП";

    protected function getViewData(){
        $viewData["request"] = $this->request;
        //Список филиалов
        $viewData["filials"] = $this->request->getFilials();

        return $viewData;
    }

    public function getButtons(){
        return [
            "selectLevels" => "Выбрать уровни образования"
        ];
    }
}