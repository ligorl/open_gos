<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

/**
 * Настройка выборка для фрейма документы
 * открытая - лицензирование - заявления - карточка заявления - документы
 * 
 * url:  /eo/requests/edit/ид_заявления
 * path: /module/Eo/src/Eo/Model/RequestFrames/Frames/Document.php
 */
class Document extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "document";
    protected $title = "Документы";

    protected function getViewData(){
        $viewData = [];

        $reasonId = $this->request->getLicenseReasonsIds();

        $docTypeNeed = new \Model\LodEntities\LicenseReasonDocument();       

        $viewData["orgId"] = $this->request->getField("organizationId");
        $viewData["request"] = $this->request;
        $viewData["documentNeed"] = $docTypeNeed->getAllByWhereParseId( array('LicenseReasonId' => $reasonId) , 'DocumentTypeId');
        
        //костиль для догонки дополнительных документов
        $typeIdAccepted = array_keys( $viewData["documentNeed"] );
        if( !empty($typeIdAccepted)){
            $docType = new \Model\LodEntities\LicenseRequestDocument();
            $docType = $docType->getAllByWhereParseId( array(
                'LicenseRequestId' => $this->request->getId(),
                new \Zend\Db\Sql\Predicate\NotIn( 'DocumentTypeId',  $typeIdAccepted ),
                )
                , 'DocumentTypeId'
            );
            if( !empty($docType)) {
                $docType = array_keys( $docType );
                $docTypeNeed = new \Model\Entities\isgaDocumentTypes();
                $docTypeNeed = $docTypeNeed->getAllByWhere( array( $docTypeNeed->getIdName() => $docType ) );
                if( !empty($docTypeNeed) ){
                    foreach( $docTypeNeed as  $value ){
                        $docTypeOne = new \Model\LodEntities\LicenseReasonDocument();   
                        $docTypeOne->setFieldSafe( 'DocumentTypeId' , $value->getId());
                        $docTypeOne->setFieldSafe( 'SortOrder' , 10000);
                        $viewData["documentNeed"][] =   $docTypeOne;
                    }
                }
            }               
        }

        //сортировка
        //TODO: Добавить сортировку по имени у одинаковых типов сортировки
        usort( 
            $viewData["documentNeed"] , 
            function( $aO , $bO ){
                $aSort = (int)$aO->getFieldSafe('SortOrder');
                $bSort = (int)$bO->getFieldSafe('SortOrder');
                if( empty($aSort - $bSort) ){
                    $aSort = $aO->getDocumentTypeName();
                    $bSort = $bO->getDocumentTypeName();
                    return strcmp( $aSort , $bSort );
                }
                return $aSort - $bSort;
            }
        );
        

        return $viewData;
    }

    public function getButtons(){
        return [
            "btAddDocumentDop" => "Доп. документ"
        ];
    }

}