<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class ReorgLic extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "reorg-lic";
    protected $title = "Реорганизация";

    protected function getViewData(){
        $viewData = [];
        $viewData["reorg"] = $this->request->getOneReorganizationData();
        $reorgOrg = $viewData["reorg"]->getReorganizedOrganization();

        $org = $this->request->getEducationalOrganization();
        $viewData['org'] = $org;
        if($reorgOrg != null)
            $viewData['license'] = $reorgOrg->getLicense();
        $viewData['request'] = $this->request;

        return $viewData;
    }

    public function getButtons(){
        return [
        ];
    }
}