<?php
namespace Eo\Model\RequestFrames\Frames;

use Model\Gateway;

class NewOp extends \Eo\Model\RequestFrames\FrameBase{
    protected $tag = "new-op";
    protected $title = "Новые ОП";

    protected function getViewData(){
        $viewData["request"] = $this->request;
        //Список филиалов
        $viewData["filials"] = $this->request->getFilials();


        return $viewData;
    }

    public function getButtons(){
        return [
            //"btAddOp" => "Добавить",
            "selectLevels" => "Выбрать уровни образования"
        ];
    }
}