<?php
namespace Eo\Model\RequestFrames;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

/**
 *
 * ����� �������� ����� ������� ��� ���������
 * @package Eo\Model\RequestFrames
 */
class RequestFramesController
{
    private $sm;
    private $reasons;
    private $request;
    private $isTest;
    private $startFrame = "general";

    private $frames=[
        "General"           => 1,
        "ReorgLic"          => 0,
        "Reorg"             => 0,
        "Filial"            => 0,
        "NewFilial"         => 0,
        "ChangeLicTitle"    => 0,
        "NewOp"             => 0,
        "CurrentOp"         => 0,
        "ChangeOpName"      => 0,
        "StopOp"            => 0,
        "PlacesOd"          => 0,
        "OpenOd"            => 0,
        "StopOd"            => 0,
    ];
    private $generalSubframes=[
        "ChoiseSupp"        => 0,
        "Reorg"    => 0
    ];

    private $framesObjList = [];

    public function __construct($request, $sm, $isTest = false, $startFrame = null){
        $this->sm = $sm;
        //�������� ������ ������
        $this->request = $request;
        $this->reasons = $request->getLicenseReasons();
        $this->isTest = $isTest;
        //
        if($startFrame != null)
            $this->startFrame = $startFrame;

        //�������� �� ������ ������
        foreach($this->reasons as $reason){
            $framesList = $reason->getFrames();
            foreach($framesList as $frame){
                switch($frame){
                    case "filial":              $this->frames["Filial"] = 1; break;
                    case "new-filial":          $this->frames["NewFilial"] = 1; break;
                    case "change-lic-title":    $this->frames["ChangeLicTitle"] = 1; break;
                    case "new-op":              $this->frames["NewOp"] = 1; break;
                    case "current-op":          $this->frames["CurrentOp"] = 1; break;
                    case "change-op-name":      $this->frames["ChangeOpName"] = 1; break;
                    case "stop-op":             $this->frames["StopOp"] = 1; break;
                    case "places-od":           $this->frames["PlacesOd"] = 1; break;
                    case "open-od":             $this->frames["OpenOd"] = 1; break;
                    case "stop-od":             $this->frames["StopOd"] = 1; break;
                    case "choise-supp":         $this->generalSubframes["ChoiseSupp"] = 1; break;
                    case "reorg":               $this->frames["Reorg"] = 1; break;
                    case "reorg-lic":           $this->frames["ReorgLic"] = 1; break;
                    //case "document":            $this->frames["Document"] = 1; break;
                }
                if( \Eo\Model\Export::isTestServer() ){
                    $this->frames["Document"] = 1;
                }
                //��� �����
                if($isTest) {
                    $this->frames = [
                        "General"           => 1,
                        "Reorg"             => 1,
                        "ReorgLic"          => 1,
                        "Filial"            => 1,
                        "NewFilial"         => 1,
                        "ChangeLicTitle"    => 1,
                        "NewOp"             => 1,
                        "CurrentOp"         => 1,
                        "ChangeOpName"      => 1,
                        "StopOp"            => 1,
                        "PlacesOd"          => 1,
                        "OpenOd"            => 1,
                        "StopOd"            => 1
                    ];
                    $this->generalSubframes = [
                        "ChoiseSupp"        => 1
                    ];
                }
            }
        };

        //�������� �� ������ �������, ��� ������������ �� ������ ���� ������
        foreach($this->frames as $frame=>$enabled){
            if($enabled){
                $frameName = "Eo\\Model\\RequestFrames\\Frames\\".$frame;
                $frameObj = new $frameName($request, $sm);
                $this->framesObjList[] = $frameObj;
                if($frame == "General"){
                    $frameObj->setOptions($this->generalSubframes);
                }
            }
        }
    }



    public function getDialog(){
        $pTypesByLevel = [];
        $eProgramsByType = [];

        if($this->frames["NewOp"] || $this->frames["ChangeOpName"]){
            //���� ���������
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $subselectLevels = $sql->select();
            $subselectLevels->from("eiis_EduLevels")
                ->columns(array("Id"))
                ->where(array(
                    new \Zend\Db\Sql\Predicate\IsNotNull("Code")
                ));

            $programTypesTable = new \Model\Gateway\EntitiesTable("eiis_EduProgramTypes");
            $programTypes = $programTypesTable->getAllEntities(null, "PrintName ASC",[
                new \Zend\Db\Sql\Predicate\In("fk_eiisEduLevels", $subselectLevels)
            ]);

            //�������� �� �����
            foreach($programTypes as $pt){
                if(!isset($pTypesByLevel[$pt->getField("fk_eiisEduLevels")])){
                    $pTypesByLevel[$pt->getField("fk_eiisEduLevels")] = [];
                }
                $pTypesByLevel[$pt->getField("fk_eiisEduLevels")][] = $pt->getFields();
            }
            //��� ���������
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $subselectLevels = $sql->select();
            $subselectLevels->from("eiis_EduLevels")
                ->columns(array("Id"))
                ->where(array(
                    new \Zend\Db\Sql\Predicate\IsNotNull("Code")
                ));

            $sql2 = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $subselectTypes = $sql2->select();
            $subselectTypes->from("eiis_EduProgramTypes")
                ->columns(array("Id"))
                ->where(array(
                    new \Zend\Db\Sql\Predicate\In("fk_eiisEduLevels", $subselectLevels)
                ));


            $eduProgramsTable = new \Model\Gateway\EntitiesTable("eiis_EduPrograms");
            $eduPrograms = $eduProgramsTable->getAllEntities(null, "Code ASC",[
                new \Zend\Db\Sql\Predicate\In("fk_eiisEduProgramType", $subselectTypes),
                "actual" => 1
            ]);
            //������������ ��������� �� �������
            foreach($eduPrograms as $ep){
                if(!isset($eProgramsByType[$ep->getField("fk_eiisEduProgramType")])){
                    $eProgramsByType[$ep->getField("fk_eiisEduProgramType")] = [];
                }
                $eProgramsByType[$ep->getField("fk_eiisEduProgramType")][] = $ep->getFields();
            }
        }


        $result = new ViewModel();
        $result->setTemplate("eo/requests/frames/FramesDialog.phtml");
        $result->setTerminal(true);
        $result->setVariables(array(
            "frames" => $this->framesObjList,
            "request" => $this->request,
            "isTest" => $this->isTest,
            "pTypesByLevel" => $pTypesByLevel,
            "eProgramsByType" => $eProgramsByType,
            "startFrame" => $this->startFrame
        ));
        return $result;
    }
}