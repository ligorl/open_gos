<?php
namespace Eo\Model\RequestFrames;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class FrameBase
{
    protected $sm;

    protected $tag;
    protected $title;
    protected $request;
    protected $visible = true;

    public function __construct($request, $sm){
        $this->request = $request;
        $this->sm = $sm;
    }

    public function getTag(){
        return $this->tag;
    }

    public function getTitle(){
        return $this->title;
    }

    public function getMenuTitle(){
        return $this->title;
    }

    protected function getViewData(){
        return array();
    }

    public function isVisible(){
        return $this->visible;
    }

    public function getButtons(){
        return [];
    }

    public function getButtonsList(){
        $retString = "";
        foreach($this->getButtons() as $class => $title){
            $retString.='<div class="btButton '.$class.'">'.$title.'</div>';
        }
        return $retString;
    }

    public function getView(){
        $vm = new ViewModel();
        $vm->setTemplate("eo/requests/frames/".$this->tag.".phtml");
        $vm->setTerminal(true);

        $vm->setVariables($this->getViewData());

        $viewRender = $this->sm->get('ViewRenderer');
        $html = $viewRender->render($vm);

        return $html;
    }
}