<?php

namespace Eo\Model;

class ExportXmlCertificate extends ExportXml
{

    public function getCodeUslugi()
    {
        return '2';
    }

    private $codeReasons = array(
        'ReAccreditation_NewProgram'            => '1', //государственная аккредитация в отношении ранее не аккредитованных образовательных программ
        'ReAccreditation_Restructuring'         => '2', //Реорганизация организации в форме преобразования
        'ReAccreditation_ReName'                => '3', //изменение наименования
        'ReAccreditation_RePlace'               => '4', //Изменением места нахождения организации
        'ReAccreditation_ChangeEduProgramToUGS' => '7', //Переоформление на укрупненные группы специальностей
        'ReAccreditation_LossAccreditation'     => '8', //Лишение государственной аккредитации в отношении отдельных уровней образования, укрупненных групп профессий, специальностей и направлений
    );

    public function getData()
    {

        $applicationsClass = $this->applicationsClass;
        $currentApplication = $this->currentApplication;
        $monthArray = $this->monthArray;
        $MainOrg=$this->MainOrg;
        $arrayOrg = $this->arrayOrg;
        $resultArray = array();
        $resultArray['code'] = $arrayOrg['Id'];
        $resultArray['code_ref']  = '';
        $resultArray['applicant'] = 'true';
        //добавляем общие данные главного филиала
        $resultArray = $this->addFilialeMainData($arrayOrg, $resultArray);
        //Добавление данных о общественных лицензиях
        $resultArray = $this->addAccreditationOf($arrayOrg, $resultArray);
        //Добавляем сведения об учредителях
        //$resultArray = $this->getUchrArray($arrayOrg,$resultArray);
        //Добавляем образовательные програмы главного вуза
       $resultArray= $this->addMainEductionsProgram($resultArray);
        //перекидываем массив
        $resultArrayOut['institutions']['institution'] = array();
        $resultArrayOut['institutions']['institution'][] = $resultArray;
        $resultArray = array();
        //филиалы
        $resultArrayOut = $this->addBranchData($arrayOrg, $resultArrayOut);
        //Прикрепленныей файлы
        //$resultArrayOut=$this->addAttachment($resultArrayOut);
        return $resultArrayOut;
    }

}