<?php

namespace Eo\Model;

class ExportPaymentNewEduProgram extends ExportPayment
{

    function getData()
    {

        $mainOrgId = $this->mainOrg->getField('Id');
        //$appType = $this->application->getField('fk_isgaApplicationType');
        $appReason = $this->application->getReasonArray();
        $appReason = array_column($appReason, 'Id');
        //берем массив филиалом и ОП
        $branches = $this->fillEductionProgramToBranches();
        //убираем пустые
        foreach ($branches as $branchKey => $branchData) {
            if (count($branchData["Programs"]) == 0){
                unset($branches[$branchKey]);
            }
        }


        //формируем данные об уровнях образования уобразовательных программах
        //сделано муторно, чтоб не происходило по 2 запроса в бд на каждую программу
        // а все решалось 2 мя сапросами
        //сбиваем ид нужных програм лецинзирования
        $licensedProgramsId = array();
        foreach ($branches as $branchKey => $branchData) {
            foreach ($branchData["Programs"] as $program) {
                $licensedProgramsId[] = $program->getField("fk_eiisLicensedPrograms");
            }
        }
        if (count($branches)<1){
            return null;
        }
        //по ид прогрмммы лицензирования оприделяем тип программы
        $licensedProgramsId = array_unique($licensedProgramsId);
        $licensedPrograms = $this->application
            ->selectComplicated("eiis_LicensedPrograms", array('Id', 'fk_eiisEduLevel'),
                                array("Id" => $licensedProgramsId)
            )->toArray();
        //$programTypeId = array_column($licensedPrograms, 'fk_eiisEduProgramType');
        //$licensedProgram2programType = array_column($licensedPrograms, 'fk_eiisEduProgramType', 'Id');
        //по типу программы определяем уровень образования
       // $programTypeId = array_unique($programTypeId);
       // $programTypes = $this->application
        //    ->selectComplicated("eiis_EduProgramTypes", array('Id', 'fk_eiisEduLevels'), array("Id" => $programTypeId)
        //    )->toArray();
        //$programTypes2eduLevel = array_column($programTypes, 'fk_eiisEduLevels', 'Id');

        $programLicensed2EduLevel = array_column($licensedPrograms, 'fk_eiisEduLevel','Id');

        // в масси филиалов прицепляем уровни образования
        foreach ($branches as $branchKey => $branchData) {
            foreach ($branchData["Programs"] as $program) {
                $licensedProgramId = $program->getField("fk_eiisLicensedPrograms");
                //$programType = $licensedProgram2programType[$licensedProgramId];
                //$eduLevel = $programTypes2eduLevel[$programType];
                $eduLevel = $programLicensed2EduLevel[$licensedProgramId];
                $programId = $program->getField('Id');
                $ugs = $program->getLicensedProgram()->getUgs()->getField('Code');
                $branches[$branchKey]['Levels'][][$programId][$eduLevel] = $ugs;
            }
        }
        /*
          $levelsToSearch = array();
          foreach ($branches as $branchKey => $branchData) {
          if (count($branchData["Programs"]) == 0)
          continue;


          foreach ($branchData["Programs"] as $program) {
          $licensedProgram = $program->getLicensedProgram();
          $eduProgramType = $licensedProgram->getEduProgramType();
          $eduProgram = $licensedProgram->getEduProgram();
          $eduLevel = $eduProgramType->getField("fk_eiisEduLevels");
          $programId = $program->getField('Id');
          $branches[$branchKey]['Levels'][][$programId] = $eduLevel;
          $levelsToSearch[] = $eduLevel;
          }
          }
         */
        //проходимся по филиалами и образовательным программа
        //согласно их уровня считаем количество образовательных программ

        $eachProgram= new \Model\Entities\isgaApplicationPayments();
        $eachProgram=$eachProgram->getLevelsForEach();
        $levelsPayment = array();
        foreach ($branches as $branchKey => $branchData) {
            if (count($branchData["Levels"]) == 0)
                continue;
            foreach ($branchData["Levels"] as $levels) {
                foreach ($levels as $eduLevelArray) {
                    foreach ( $eduLevelArray as $eduLevel => $ugs ) {
                        if (in_array($eduLevel,
                           $eachProgram
                          )
                        ) {
                            //if (isset($levelsPayment[$branchKey][$eduLevel])) {
                            //    $levelsPayment[$branchKey][$eduLevel][$ugs] ++;
                            //} else {
                                $levelsPayment[$branchKey][$eduLevel][$ugs] = 1;
                            //}
                        } else {
                            $levelsPayment[$branchKey][$eduLevel] = 1;
                        }
                    }
                }
            }
        }

        foreach ( $levelsPayment as $branchKey => $levelValue ) {
            foreach ($levelValue as $eduLevel => $eduCount) {
                if(  is_array($eduCount)){
                    $levelsPayment[$branchKey][$eduLevel] = count($eduCount);
                }
            }
        }

        //готовим выборку для запроса стоимостей
        //муторно, чтоб не делался запрос каждой платежкой
        //$levelsToSearch = array_values ($programTypes2eduLevel);
        $levelsToSearch = array_values ($programLicensed2EduLevel);
        $levelsToSearch = array_unique($levelsToSearch);
        $applicationPaymentsTable = $this->application
            ->selectComplicated(
              "isga_ApplicationPayments", array(),
              array("fk_eiisEduLevels"         => $levelsToSearch,
                //'fk_isgaApplicationType'   => $appType,
                'fk_isgaApplicationReason' => $appReason
              )
            )->toArray();
        //перебиваем ключи
        $ApplicationPayments = array();
        foreach ($applicationPaymentsTable as $key => $value) {
            $ApplicationPayments[$value['fk_eiisEduLevels']] = $value;
        }
        unset($applicationPaymentsTable);

        //проходимся по массиву с филиалами и образовательными программам
        //формируем массив платежек
        $paymentResult = array();
        foreach ($levelsPayment as $branchKey => $levelValue) {
            foreach ($levelValue as $eduLevel => $eduCount) {
                $paymentResult[] = $this->getPayment($eduLevel
                  , ($branchKey != $mainOrgId)
                  , $branchKey
                  , $eduCount
                  , $ApplicationPayments
                );
            }
        }

        return array('PAYMENT' => $paymentResult);
    }

}