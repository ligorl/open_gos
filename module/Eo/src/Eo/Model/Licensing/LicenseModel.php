<?php
/**
 * Description of License
 *
 * @author Руслан
 */
namespace Eo\Model\Licensing;

class LicenseModel
{
    //Возвращает лицензию по id
    public static function getLicense( $id){
        return new \Model\Entities\eiisLicenses( $id);
    }

    //Возвращает все возможные статусы лицензии
    public static function getStates(){
        $LicensesStateTable = new \Model\Gateway\EntitiesTable("eiis_LicenseStates");
        return $LicensesStateTable->getAllEntities();
    }

    //Возвращает приложения лицензии
    public static function getSupplementsFromLicenseId( $licenseId ,$orderBdFormat=null){
        $supplementTable    = new \Model\Gateway\EntitiesTable("eiis_LicenseSupplements");
        $supplements = $supplementTable->getAllEntities(null, $orderBdFormat, array("fk_eiisLicense" => $licenseId), false);
        return $supplements;
    }

    //Возвращает не валидные лицензии той же организации
    public static function getNotValidLicenseFromOrgaznizationId( $id){
        $license = new \Model\Entities\eiisLicenses();
        return $license->getAllByWhere([
           'lod_stateCode' => 'Not_Valid',
           'fk_eiisEducationalOrganization' => $id
        ]);
    }

     //получаем все возможные подписанные  статусы лицензии
    public static function getSignStates(){
        $catalogLicense = new \Model\LodEntities\catalogeiislicenses();
        return $catalogLicense->getAll();
    }

}