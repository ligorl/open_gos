<?php
/**
 * Description of License
 *
 * @author Руслан
 */
namespace Eo\Model\Licensing;

class LicenseSupplementModel
{
    //Возвращает лицензию по id или класс при id = null
    public static function getSupplement( $id){
        return new \Model\Entities\eiisLicenseSupplements( $id);
    }

    /**
     * Получаем сгрупированные приложения для отчета
     * @param int $limit
     * @param int $offset
     * @return eiisLicenseSupplements
     */
    public static function getReportData( $limit = null, $offset = null ){
        $supp = new \Model\Entities\eiisLicenseSupplements();
        $condition = [];
        $condition[] = ['column' => 'lod_stateCode="VALID"'];
        $condition[] = ['or' => 'lod_stateCode="SUSPENDED"'];
        $condition[] = ['group by' => 'fk_eiisBranch'];

        if(!empty($limit)){
            $condition[] = ['limit' => $limit];
        }
        if(!empty($offset)){
            $condition[] = ['offset' => $offset];
        }

        return $supp->getData_ArrayCondition( $condition );
    }

    /**
     * Получаем сгрупированные приложения для отчета
     * @param int $limit
     * @param int $offset
     * @return eiisLicenseSupplements
     */
    public static function getReportDataLicensedPrograms( $limit = null, $offset = null ){
        $educationProgram = new \Model\LodEntities\EducationProgram();
        $condition = [];
        $condition[] = ['column' => 'lod_stateCode="VALID"'];
        $condition[] = ['or' => 'lod_stateCode="SUSPENDED"'];

        if(!empty($limit)){
            $condition[] = ['limit' => $limit];
        }
        if(!empty($offset)){
            $condition[] = ['offset' => $offset];
        }
        return $educationProgram->getDataJoin_ArrayCondition(
          [
              'Id' => new \Zend\Db\Sql\Expression('eiis_LicenseSupplements.id'),
              'fk_eiisBranch' => new \Zend\Db\Sql\Expression('eiis_LicenseSupplements.fk_eiisBranch'),
              'fk_eiisLicense' =>  new \Zend\Db\Sql\Expression('eiis_LicenseSupplements.fk_eiisLicense'),
              'lod_stateCode' =>  new \Zend\Db\Sql\Expression('eiis_LicenseSupplements.lod_stateCode'),
              'EOName' => new \Zend\Db\Sql\Expression('eiis_LicenseSupplements.EOName'),
              'educationProgramId' => new \Zend\Db\Sql\Expression('EducationProgram.Id'),
              'educationLevelId' => new \Zend\Db\Sql\Expression('EducationProgram.educationLevelId'),
              'EduProgramsID' => new \Zend\Db\Sql\Expression('EducationProgram.EduProgramsID'),
          ],
          [
                'table' => new \Zend\Db\Sql\TableIdentifier('eiis_LicenseSupplements','f11_mon'),
                'binder' => 'EducationProgram.LicenseSupplementID = eiis_LicenseSupplements.Id',
                'type' => 'left'
          ],
          $condition
         );
    }

    /**
     * Возвращает количество приложений к обработке
     * @return int
     */
    public static function getValidOnlyGroupedByBranchCount(){
        $supp = new \Model\Entities\eiisLicenseSupplements();
        return $supp->getCountRowValidAndGroupByBranch();
    }

    /**
     * Возвращает количество приложений к обработке
     * @return int
     */
    public static function getValidOnlyCount(){
        $supp = new \Model\Entities\eiisLicenseSupplements();
        return $supp->getCountRowValidSupplementAndEducationProgram();
//        return $supp->getDataCount_ArrayCondition([
//            [ 'column' => 'lod_stateCode="VALID"'],
//            [ 'or' => 'lod_stateCode="SUSPENDED"' ]
//        ]);
    }
}