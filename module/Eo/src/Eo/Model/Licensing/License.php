<?php

/**
 * Description of License
 *
 * @author Руслан
 */

namespace Eo\Model\Licensing;

class License {

    //получаем лицензию по id
    public function getLicense($id) {
        return new \Model\Entities\eiisLicenses($id);
    }

    //получаем статусы лицензии
    public function getState() {
        $LicensesStateTable = new \Model\Gateway\EntitiesTable("eiis_LicenseStates");
        return $LicensesStateTable->getAllEntities();
    }

    //получаем статусы лицензии
    public function getCurrentState($licenseObj) {
        if (!empty($licenseObj->getField('fk_eiisLicenseState'))) {
            $licenseStates = new \Model\Entities\eiisLicenseStates(
                    $licenseObj->getField('fk_eiisLicenseState')
            );

            return $licenseStates->getField('Name');
        } else {
            return '';
        }
    }

    //получаем sign статусы лицензии
    public function getSignState() {
        $catalogLicense = new \Model\LodEntities\catalogeiislicenses();
        return $catalogLicense->getAll();
    }

    /**
     * Поставщик лицензии
     * @param type $licenseObj
     */
    public function getLicenseProvider($licenseObj) {
        if (!empty($licenseObj->getField('fk_eiisEducationalOrganization'))) {
            $eduOrg = new \Model\Entities\eiisEducationalOrganizations(
                    $licenseObj->getField('fk_eiisEducationalOrganization')
            );

            return $eduOrg->getField('FullName');
        } else {
            return '';
        }
    }

    //получаем данные для генерации информации о лицензии
    public function getLicenseInfo($id) {
        $license = $this->getLicense($id);
        $basisForLicensing = '';
        $dealNumber = '';
        $deal = null;
        $document = null;

        //лицензия выдана
        $license->setField(
                'licenseIsGranted', $this->getLicenseProvider($license)
        );

        //возврат 
        if (!empty($license->getField('basisForLicensing'))) {
            $document = new \Model\LodEntities\document(
                    $license->getField('basisForLicensing')
            );
            $docType = $document->getFieldOrSpace('documentType');
            $docRegDate = new \DateTime($document->getFieldOrSpace('registrationDate'));
            $docRegDate = $docRegDate->format('d.m.Y');
            $docRegNumber = $document->getFieldOrSpace('registrationNumber');
            $basisForLicensing = $docType . ' от ' . $docRegDate . 'г. №' . $docRegNumber;

            //номер дела
            if (!empty($document->getField('dealId'))) {
                $deal = new \Model\LodEntities\deal(
                        $document->getField('dealId')
                );

                $dealNumber = $deal->getField('number');
            }
        }


        $license->setField('documentsOnDelivery', $basisForLicensing);
        $license->setField('dealNumber', $dealNumber);

        //отображаемый статус
        $license->setField('lod_stateCode', $this->getWebStateCode($license));

        return $license;
    }

    /* 'documentsOnDelivery' => [
      "type" => 'text',
      "editable" => true,
      "title" => "Распорядительный документ о выдаче"
      ], */

    public function documentsOnDelivery($licenseOrDocument) {
        $document = false;

        switch (get_class($licenseOrDocument)) {
            case 'document':
                $document = $licenseOrDocument;
                break;
            case 'eiisLicenses':
                $document = $this->getDocument($license);
                break;
            default :
                throw new \Exception('Error: undefined class(' . get_class($licenseOrDocument) . ')');
        }

        if ($document) {
            $docType = $document->getFieldOrSpace('documentType');
            $docRegDate = new \DateTime($document->getFieldOrSpace('registrationDate'));
            $docRegDate = $docRegDate->format('d.m.Y');
            $docRegNumber = $document->getFieldOrSpace('registrationNumber');

            $basisForLicensing = $docType . ' от ' . $docRegDate . 'г. №' . $docRegNumber;
            return $basisForLicensing;
        } else {
            return '';
        }
    }

    /**
     * Возвращает документ, или false
     * @param type $license
     * @return boolean|\Model\LodEntities\signatories
     */
    public function getDocument($license) {
        if (!empty($license->getField('basisForLicensing'))) {
            return new \Model\LodEntities\document(
                    $license->getField('basisForLicensing')
            );
        }
        return false;
    }

    /**
     * Возвращает уполномоченное лицо, или false
     * @param type $license
     * @return boolean|\Model\LodEntities\signatories
     */
    public function getAuthorizedPerson($license) {
        if (!empty($license->getField('authorizedPersonId'))) {
            $signatories = new \Model\LodEntities\signatories(
                    $license->getField('authorizedPersonId')
            );
            return $signatories;
        }
        return false;
    }

    /**
     * Возвращает организацию
     * @param type $licenseObj
     * @return boolean|\Model\Entities\eiisEducationalOrganizations
     */
    function getEducationOrganization($licenseObj) {
        if (!empty($licenseObj->getField('fk_eiisEducationalOrganization'))) {
            $eduOrg = new \Model\Entities\eiisEducationalOrganizations(
                    $licenseObj->getField('fk_eiisEducationalOrganization')
            );

            return $eduOrg;
        } else {
            return false;
        }
    }
    
    
    /**
     * Возвращает контролирующий орган
     * @param type $licenseObj
     * @return boolean|\Model\Entities\eiisControlOrgans
     */
    function getControlOrgan($licenseObj){
        if (!empty($licenseObj->getField('fk_eiisControlOrgan'))) {
            $eduOrg = new \Model\Entities\eiisControlOrgans(
                    $licenseObj->getField('fk_eiisControlOrgan')
            );

            return $eduOrg;
        } else {
            return false;
        }
    }

    /**
     * Получить не валидные лицензии той же организации что и данная лицензия
     * @param type $id
     * @return type
     */
    function getNotvalidLicenseFromOrgaznization($id) {
        $license = new \Model\Entities\eiisLicenses();
        return $license->getAllByWhere([
                    'lod_stateCode' => 'Not_Valid',
                    'fk_eiisEducationalOrganization' => $id
        ]);
    }

    /**
     * Возвращает web статус лицензии
     * @param type $license
     * @return string
     */
    function getWebStateCode($licenseObj) {
        if (!empty($licenseObj->getField('lod_stateCode'))) {
            $status = new \Model\Entities\eiisLicenseStates();
            $status = $status->getByWhere([
                'LodCode' => $licenseObj->getField('lod_stateCode')
            ]);
            if ($status) {
                return $status->getField('WebName');
            } else {
                return 'Не найден эквивалент для(' . $licenseObj->getField('lod_stateCode') . ')';
            }
        } else {
            return '';
        }
    }

    function getRusSignStateCode($licenseObj) {
        if (!empty($licenseObj->getField('lod_signStateCode'))) {
            $catalogLicense = new \Model\LodEntities\eiissignstates();
            $catalogLicense = $catalogLicense->getByWhere([
                'origin_name' => $licenseObj->getField('lod_signStateCode')
            ]);
            return ($catalogLicense) ? $catalogLicense->getField('russian_name') : '';
        } else {
            return '';
        }
    }

}
