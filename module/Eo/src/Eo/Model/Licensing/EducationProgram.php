<?php
/**
 * Description of EducationProgram
 *
 * @author Руслан
 */
namespace Eo\Model\Licensing;

class EducationProgram
{
    public static function getEduProgram( $id = null){
        return new \Model\LodEntities\EducationProgram($id);
    }

    public static function getProgramBySupplementId( $supplementId){
        $eduProg = self::getEduProgram();
        $programs = $eduProg->getAllByWhere(['LicenseSupplementID'=>$supplementId]);
        return $programs;
    }
}