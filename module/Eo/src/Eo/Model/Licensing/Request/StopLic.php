<?php

namespace Eo\Model\Licensing\Request;

/**
 * Заявление на лицендзирование
 *
 * переоформление
 *
 *
 *
 * @author any
 */
class StopLic extends Request {

    public function getData() {
        $result = array();

        $this->parseOrgInfo($result);
        $this->parseAdress($result);

        $reqOrgInfo = $this->getRequestOrganizationInfo();
        $propertiesArray = $this->parseStrtoArray( $reqOrgInfo->getOrganizationPropertiesName() , array(32,70,70,70,70,70,70,70,70,70,70,70) );
        $result['fieldProperties']   = array_shift( $propertiesArray );
        //${tableProperties}  - правоая организационноя форма
        $result['_tableProperties'] = array();
        foreach( $propertiesArray as $key => $value ){
            $result[ '_tableProperties' ][]['tableProperties'] = $value;
        }

        $result['fieldOGRN']         = $reqOrgInfo ->getFieldSafe('ogrn');
        $result['fieldINN']          = $reqOrgInfo ->getFieldSafe('inn');
        $result['fieldReqLicDate']   = $this->requestClass->getDate('licenseDate');
        $result['fieldReqLicNum']    = $this->requestClass->getFieldSafe('licenseRegistrationNumber');
        $result['fieldReqLicSeries'] = $this->requestClass->getFieldSafe('licenseSeries');
        $result['fieldReqLicBlank']  = $this->requestClass->getFieldSafe('licenseBlankNumber');
        $result['fieldPhones'] = $reqOrgInfo ->getFieldSafe('licenseePhone');
        $result['fieldMails'] = $reqOrgInfo ->getFieldSafe('licenseeEmail');
        $this->parsePodpisano($result);





        return $result;

    }

    public function getTemplateName(){
        return 'licensing/Zayavlenie_StopLic.docx';
    }

    public function getDocumentName(){
          return 'Прекращение ОД в целом';
    }



    //last string

}
