<?php

namespace Eo\Model\Licensing\Request;

/**
 * Заявление на лицендзирование
 *
 * супер клас для формирования заявлений
 *
 * @author any
 */
class Request extends \Eo\Model\Export {

    /**
     * фабрика выбора формирователя в зависимости от процедуры,
     * опрделяется по ид заявления
     *
     * @param type $requestId  -    ид заявления
     * @return \Eo\Model\Licensing\Request\Duplicat
     */
    static  public function RequestFabric($requestId){
        if( empty($requestId) ){
            throw new \Exception('формированяе документа заявления - не указан ид заявления');
        }

        $requestClass = new \Model\LodEntities\LicenseRequest();
        $requestClass = $requestClass->getByWhere(array(
            $requestClass->getIdName() => $requestId
        ));

        if( empty($requestClass) ){
            throw new \Exception('формированяе документа заявления - не найдено заявление');
        }

        $procedureId = $requestClass->getFieldSafe('licensingProcedureId');

        switch ($procedureId){
            case 'eb9e2f563ef24b3aa127811a27cd7009':
                return new \Eo\Model\Licensing\Request\Duplicat($requestId);
                break;
            //Переоформление лицензии (открытие адресов, программ, филиалов)
            case '3381eb13928c4260b59cfa85f7ff8dec':
                return new \Eo\Model\Licensing\Request\Pereoformlenie($requestId);
                break;
            // Прекращение ОД
            case '7d2f8700fb8d43a5a428de7cfc6e2bea':
                //Прекращение ОД в целом
                if( $requestClass->hasReasonById( '42807cbc9a9c419ba7ebc374652ed68a' ) ){
                    return new \Eo\Model\Licensing\Request\StopLic($requestId);
                }
                //Прекращение ОД в филиале
                if( $requestClass->hasReasonById( 'b4bfaa7993784256890dc19a8deb7e88' ) ){
                    return new \Eo\Model\Licensing\Request\StopLicFilial($requestId);
                }
                throw new \Exception('формированяе документа заявления - не найдена причина для прекращения');
                break;
            // Переоформление лицензии (другие причины) 
            case 'd13f3a96d6a94f1685469a6b0b61cc4c':
                return new \Eo\Model\Licensing\Request\Pereoformlenie($requestId);
                break;
            default:
                throw new \Exception('формированяе документа заявления - не определен формирователь');
        }
    }


    protected $requestClass = null;


    /**
     * создаем клас формирования макета заявления
     *
     * @param type $requestId  - ид заявления
     */
    public function __construct($requestId) {
        mb_internal_encoding("UTF-8");
        $this->requestClass = new \Model\LodEntities\LicenseRequest();
        $this->requestClass = $this->requestClass->getByWhere(array(
            $this->requestClass->getIdName() => $requestId
        ));
        if( !$this->requestClass instanceof \Model\LodEntities\LicenseRequest){
        }
    }


    private $_licenseClass = null;

    /**
     * дергаем лицензию из заявления
     *
     * @return type
     */
    public function getLicense(){
        if( null === $this->_licenseClass){
            $licenseId = $this->requestClass->getFieldSafe('licenseId');
            $this->_licenseClass = new \Model\Entities\eiisLicenses();
            $this->_licenseClass = $this->_licenseClass->getByWhere(array(
                $this->_licenseClass->getIdName() => $licenseId,
            ));
        }
        return $this->_licenseClass;
    }

    private $_reqOrgInfoClass = null;

    /**
     * дергаем орг инфо главной конторы
     *
     * @return type
     * @throws \Exception
     */
    public function getRequestOrganizationInfo(){
        if( null === $this->_reqOrgInfoClass){
            $reqId = $this->requestClass->getId();
            $this->_reqOrgInfoClass = new \Model\LodEntities\RequestOrganizationInfo();
            $this->_reqOrgInfoClass = $this->_reqOrgInfoClass->getByWhere(array(
                'requestId' => $reqId,
            ));
            if( empty($this->_reqOrgInfoClass) ){
                throw new \Exception('не надено описание филиала к лицензии');
            }
        }
        return $this->_reqOrgInfoClass;
    }

    private $_reqOrgPayAll = null;

    /**
     * выдергиваем сведения об оплатах
     *
     * @return type
     */
    public function getLicenseRequestPaymentAll(){
        if( null === $this->_reqOrgPayAll){
            $reqId = $this->requestClass->getId();
            $this->_reqOrgPayAll = new \Model\LodEntities\LicenseRequestPayment();
            $this->_reqOrgPayAll = $this->_reqOrgPayAll->getAllByWhereParseId(array(
                'LicenseRequestId' => $reqId,
            ));
        }
        return $this->_reqOrgPayAll;
    }

    /**
     * выдергиваем строку с перечнем оплат у данного заявления
     *
     * @return string
     */
    public function getLicenseRequestPaymentAllStr($isReturnArray = false){
        $nonBreakSpace = $this->unichr(160);
        $result = '';
        $paymentAll = $this->getLicenseRequestPaymentAll();
        $resArray = array();
        if( !empty($paymentAll) ){
            foreach ( $paymentAll as $paymentKey => $paymentOne ) {
                $oneStr = '';
                $num = $paymentOne->getFieldSafe('NumDoc');
                $date = $paymentOne->getDate('Date');
                if( !empty($num) ){
                    $num = '№'.$nonBreakSpace.$num;
                }
                if( !empty($date) ){
                    $date = 'от'.$nonBreakSpace.$date.$nonBreakSpace.'г';
                }
                if( empty($date) || empty($num) ){
                    $str = $num.$date;
                }
                else{
                    $str = $num.' '.$date;
                }
                $resArray[] = $str;
            }
            $result = implode(', ', $resArray);
            if( !empty($result) ){
                $result .= '.';
            }
        }
        if( $isReturnArray ){
            return $resArray;
        }
        return $result;
    }

    /**
     * раскидывает строку по массиву с длиной строк
     *
     * @param type $insStr          - строка, которую надо раскидать
     * @param type $strLenArray     - массив с длинами строк на какие надо раскиддать
     * @param type $option          - доп опции
     *        notReturnOnQuotes     - не разрывать строки на ковычках
     * @return type
     */
    static public function parseStrtoArray( $insStr, $strLenArray = array(), $option = array() ){
        $qutFlag = false;
        if( !empty($option['notReturnOnQuotes']) ){
            $qutFlag = true;
        }
        $line = 0;
        $nameLength = $strLenArray;
        $strLenCount = count($strLenArray)-1;
        $result = array();

        while( !empty($insStr) ){
            if( ($strLenCount+1) == $line ){
                $result[/*'fieldNameDatus6'*/$strLenCount-1] .= $insStr;
                break;
            }
            $lineLength = $nameLength[$line];
            $lineStr = mb_substr($insStr,0 , $lineLength);
            $lineLen = mb_strlen($lineStr);
            if( mb_strlen($insStr) <= $lineLen   ){
                $result[''.$line] = $insStr;
                break;
            }
            $letLast = mb_substr($lineStr, $lineLen-1, 1);
            //$lineLen = mb_strlen($lineStr);
            while( $letLast != ' ' &&  $lineLen>0 ){
                $lineStr = mb_substr($insStr, 0, $lineLen-1);
                $lineLen = mb_strlen($lineStr);
                if( empty($lineLen) ){
                    break;
                }
                $letQuot = mb_stripos($lineStr, ' «');
                if( !$qutFlag && !empty($letQuot) &&  $letQuot < $lineLen){
                    $lineStr = mb_substr($insStr, 0, $letQuot+1);
                    $lineLen = mb_strlen($lineStr);
                    $letLast = ' ';
                    $qutFlag = true;
                    break;
                }
                $letLast = mb_substr($lineStr, $lineLen-1, 1);
            }
            if( empty($lineLen) ){
                $lineStr = mb_substr($insStr, 0 , $lineLength);
            } else {
                $lineStr = mb_substr($insStr, 0 , $lineLen-1);
            }
            $lineLen = mb_strlen($lineStr);
            $result[$line] = $lineStr;
            $insStr = mb_substr($insStr, $lineLen+1);
            $line++;
        }
        return $result;
    }

    /**
     * получаем сведения о лицензии в одну строку
     * @return type
     */
    public function getLicenceDataStr() {
        $nonBreakSpace = $this->unichr(160);
        $resultArray = array();
        $resultArray['licDate']        = $this->requestClass->getDate('licenseDate');
        $resultArray['licNum']         = $this->requestClass->getFieldSafe('licenseRegistrationNumber');
        $resultArray['licSer']         = $this->requestClass->getFieldSafe('licenseSeries');
        $resultArray['licBlankNum']    = $this->requestClass->getFieldSafe('licenseBlankNumber');
        if( !empty($resultArray['licDate']) ){
            $resultArray['licDate'] = 'от'.$nonBreakSpace.$resultArray['licDate'];
        }
        if( !empty($resultArray['licNum']) ){
            $resultArray['licNum'] = '№'.$nonBreakSpace.$resultArray['licNum'];
        }
        if( !empty($resultArray['licSer']) ){
            $resultArray['licSer'] = 'серия'.$nonBreakSpace.$resultArray['licSer'];
        }
        if( !empty($resultArray['licBlankNum']) ){
            $resultArray['licBlankNum'] = 'номер бланка'.$nonBreakSpace.$resultArray['licBlankNum'];
        }
        $resultArray = array_filter($resultArray);
        return implode(', ', $resultArray);
    }

    /**
     * получаем должность подписанта заявления
     *
     * @return type
     */
    public function getEmployeePostTitle() {
        $position = $this->requestClass->getEmployeePostTitle();
        if( empty($position) ){
            $org = $this->requestClass->getEducationalOrganization();
            $head = $org->getHead();
            $postId = $head->getField("postId");
            $employeepost = new \Model\LodEntities\employeepost($postId);
            $position = $employeepost->getFieldSafe('title');
        }
        return $position;
    }

    /**
     * получение полного фио подписанта заявления
     *
     * @return string
     */
    public function getRequestSigner() {
        $position = $this->requestClass->getRequestSigner();
        if( empty($position) ){
            $org = $this->requestClass->getEducationalOrganization();
            $head = $org->getHead();
            $last  = $head->getFieldSafa("lastName");
            $first = $head->getFieldSafa("firstName");
            $pant  = $head->getFieldSafa("patronymic");
            $position =  $last.' '.$first.' '.$pant;
        }
        return $position;
    }

    /**
     * получения сокращенно фио подписаанта заявления
     *
     * @return string
     */
    public function getRequestSignerFioShort() {
        //$position = $this->requestClass->getRequestSigner();
        $nonBreakSpace = $this->unichr(160);

        $last  = $this->requestClass->getFieldSafe("requestSignerLastName");
        $first = $this->requestClass->getFieldSafe("requestSignerFirstName");
        $pant  = $this->requestClass->getFieldSafe("requestSignerMiddleName");

        if( empty($last) ){
            $org = $this->requestClass->getEducationalOrganization();
            $head = $org->getHead();
            $last  = $head->getFieldSafe("lastName");
            $first = $head->getFieldSafe("firstName");
            $pant  = $head->getFieldSafe("patronymic");
        }

        if( !empty($first) ){
            $first = mb_substr($first, 0,1).'.';
        }
        if( !empty($first) ){
            $pant = mb_substr($pant, 0,1).'.';
        }
        $name = $first.$pant;
        if( empty($last) || empty($name) ){
            $fio = $last.$name;
        }
        else{
            $fio = $last.$nonBreakSpace.$name;
        }

        return $fio;
    }


    /**
     * вылергиваем масив призин заявления
     * 
     * @return type
     */
    public function getRequestReasonNameArray( $fieldParse = 'documentTitle' ){
        $result = array();
        $reasonArray = $this->requestClass->getLicenseReasons();
        if( !empty($reasonArray) ){
            foreach($reasonArray as $reasonOne){
                $result[] =  $reasonOne->getFieldSafe( $fieldParse );
            }
        }
        return $result;
    }

    /**
     * собираем причины в единый строку
     * 
     * @return type
     */
    public function getRequestReasonString( $fieldParse = 'documentTitle' ) {
        $result = '';
        $reasonArray = $this->getRequestReasonNameArray( $fieldParse );
        if( !empty($reasonArray) ){
            $result = implode(', ', $reasonArray);
        }
        return $result;
    }
    
    

    private $_requestAddressType = array('OPENING','ACTIVE');
    public function getRequestAddressType() {
        return $this->_requestAddressType;
    }
    public function setRequestAddressType($requestAddressType) {
        if(!empty($requestAddressType)){
             $this->_requestAddressType = $requestAddressType;
        }
        return $this->_requestAddressType;
    }


    private $_groupByOrgLoc = array();
    /**
     *
     * @param type $orgId
     * @return type
     */
    public function getRequestAdressAllGroupByOrgLoc($multiOrgClass = null, $evryAsk = false) {
        $requestOrganizationInfo = null;
        if($multiOrgClass instanceof \Model\LodEntities\RequestOrganizationInfo){
            $requestOrganizationInfo = $multiOrgClass;
        }
        $orgIdIt = 0;
        if(!empty($requestOrganizationInfo)){
           $orgIdIt = $requestOrganizationInfo->getId();
        }
        if(!$evryAsk && isset($this->_groupByOrgLoc[$orgIdIt])){
            return $this->_groupByOrgLoc[$orgIdIt];
        }
        $this->_groupByOrgLoc[$orgIdIt] = array();
        if(empty($requestOrganizationInfo)){
            $deal = $this->getDeal();
            if(!empty($deal)){
                $licRequestAll = new \Model\LodEntities\LicenseRequest();
                $licRequestAll = $licRequestAll->getAllByWhereParseId(array(
                    'dealId' => $deal->getId(),
                ));
            }
            if(!empty($licRequestAll)){
                $licRequestIdAll = array_keys($licRequestAll);
                $reqWhere = array();
                $reqWhere['requestId'] = $licRequestIdAll;
                if(!empty($orgIdIt)){
                   $reqWhere['orgId'] = $orgIdIt;
                }
                $reqOrgInfoAll = new \Model\LodEntities\RequestOrganizationInfo();
                $reqOrgInfoAll = $reqOrgInfoAll->getAllByWhereParseId($reqWhere);
            }
        } else {
            $reqOrgInfoAll = array($requestOrganizationInfo->getId() => $requestOrganizationInfo);
        }
        if(!empty($reqOrgInfoAll)){
            $reqOrgInfoIdAll = array_keys($reqOrgInfoAll);
            $licReqAdresAll = new \Model\LodEntities\LicenseRequestAddress();
            $licReqAdresAll = $licReqAdresAll->getAllByWhereParseId(array(
                'requestOrganizationInfoId' => $reqOrgInfoIdAll,
                'requestAddressType' => $this->getRequestAddressType(),
            ));
        }
        if(!empty($licReqAdresAll)){
            foreach ($licReqAdresAll as $licReqAdresOne){
                $organizationLocationId = $licReqAdresOne->getFieldOrSpace('organizationLocationId');
                $orgAdress = $licReqAdresOne->getFieldOrSpace('postcode').', '. $licReqAdresOne->getFieldOrSpace('address');
                $this->_groupByOrgLoc[$orgIdIt][$organizationLocationId][] = $orgAdress;
            }
        }
        //$this->_groupByOrgLoc[$orgIdIt] = $groupByOrgLoc;
        return $this->_groupByOrgLoc[$orgIdIt];
    }    
    

    /**
     * 
     * 
     * @param type $suplementArray          - куда добавлять результат
     * @param type $typeOfActivity          - типы активности программ
     * @param \Model\LodEntities\LicenseRequest || \Model\LodEntities\RequestOrganizationInfo $request - строим приложение по заявлению или филиалу
     * @param array $option
     * @return type
     * @throws \Exception
     */
    public function getSuplementTypecal( &$suplementArray , $typeOfActivity = array(), $request = null, &$option = array()) {
        $programCount = 0;
        if(!empty($option['programCount'])){
            $option['programCount'] = $programCount;
        }
        $programOoCount = 0;
        if(!empty($option['programOoCount'])){
            $option['programOoCount'] = $programOoCount;
        }

        //$suplementArray = array();
        if(empty($suplementArray)){
            $suplementArray = array();
        }
        if(empty($typeOfActivity)){
            throw new \Exception('лод - формирование документов -делаем приложения - не заданы активности программ');
        }
        //$this->setRequestAddressType($typeOfActivity);
        if(empty($request)){
            $dealId = $dealClass->getId();
            $requestAll = new \Model\LodEntities\LicenseRequest();
            $orgAll = $requestAll->getAllByWhereParseId(
                array(
                    'dealId' => $dealId,
                    //new \Zend\Db\Sql\Predicate\NotLike('organizationId',$organizationId),
                )
                //'organizationId'
            );
            $orgAllId = array_keys($orgAll);
            //опционально, - не выводить этот вуз(главный)
            //if(!empty($options['notMakeItVuzId'])){
            //    unset($orgAllId[$options['notMakeItVuzId']]);
            //}
            $requestAll = new \Model\LodEntities\LicenseRequest();
            $requestAll = $requestAll->getAllByWhereParseId(
                    //array(
                    //    $requestAll->getIdName() => $orgAllId,
                    //)
            );
        }
        if(  is_array($request)){
            $requestAll = $request;
        }
        if(  $request instanceof \Model\LodEntities\LicenseRequest
             ||
             $request instanceof \Model\LodEntities\RequestOrganizationInfo
        ){
            $requestAll = array($request);
        }
        $supNum = 1;
        if(isset($option['numberBegin'])){
            $supNum = (int)$option['numberBegin'];
        }
        if(!empty($requestAll)){
            foreach ( $requestAll as $requestOne ) {
                            if( $request instanceof \Model\LodEntities\LicenseRequest ){
                                $requestKey = $requestOne->getId();
                                $organizationAll = new \Model\LodEntities\RequestOrganizationInfo();
                                //$idName = $organizationAll->getIdName();
                                $organizationAll = $organizationAll->getAllByWhereParseId(
                                    array(
                                        'requestId' => $requestKey,
                                    )
                                );
                            }
                            if( $request instanceof \Model\LodEntities\RequestOrganizationInfo ){
                                $organizationAll = array($request);
                            }
                            //$organizationId = $organizationOne
                            if(!empty($organizationAll)){
                                foreach ( $organizationAll as $organizationKey => $organizationOne ) {
                                    $organizationKey = $organizationOne->getId();
                                    $organizationId = (string)$organizationKey;
                                    $eiisOrgId = $organizationOne->getFieldOrspace('orgId');
                                    $organizationId = (string)$eiisOrgId;
                                    //если опционально не надо добавлять вуз, то пропустим его
                                    if(!empty($option['notMakeItVuzId'])
                                    && strtoupper($option['notMakeItVuzId']) == strtoupper($eiisOrgId)
                                    ){
                                        continue;
                                    }
                                    $suplementArray[$organizationId]['fieldSupNum'] = '';
                                    $suplementArray[$organizationId]['fieldfullTitle'] = $this->fixNameOrganisation( $organizationOne->getFieldOrspace('fullTitle') );

                                    $sortTitle = '';
                                    if(!$organizationOne->isEmptyField('shortTitle')){
                                        $sortTitle = $organizationOne->getFieldSafe('shortTitle');
                                    }
                                    if(!empty( $sortTitle)){
                                        $sortTitle = '('.$sortTitle.')';
                                    }
                                    $suplementArray[$organizationId]['fieldShortTitle'] = $sortTitle;
                                    $suplementArray[$organizationId]['fieldAddress'] = $organizationOne->getFieldOrspace('address');
                                    $adressCount = $this->getRequestAdressAllCount($organizationOne, true);
                                    if( $adressCount > 1){
                                        $adressCountPrint = 'адресов мест';
                                    } else {
                                        $adressCountPrint = 'адрес места';
                                    }
                                    $suplementArray[$organizationId]['fieldReqAdressCount']  = $adressCountPrint;
                                    $suplementArray[$organizationId]['fieldReqAdress']       = $this->getRequestAdressAllList($organizationOne);

                                    if(empty($suplementArray[$organizationId]['blokDopProgr'])){
                                        $suplementArray[$organizationId]['blokDopProgr'] = array();
                                    }
                                    if(empty($suplementArray[$organizationId]['blokMegaLevel'])){
                                        $suplementArray[$organizationId]['blokMegaLevel'] = array();
                                    }
                                    if(empty($suplementArray[$organizationId]['blokObjObraz'])){
                                        $suplementArray[$organizationId]['blokObjObraz'] = array();
                                    }

                                    $reqOrgProgAll = new \Model\LodEntities\LicenseRequestEducationProgram();
                                    $reqOrgProgAll = $reqOrgProgAll->getAllByWhereParseId(array(
                                        'requestOrganizationInfoId' => $organizationKey,
                                        'typeOfActivity' => $typeOfActivity,
                                    ));

                                    if(!empty($reqOrgProgAll)){
                                        foreach ( $reqOrgProgAll as $reqOrgProgKey => $reqOrgProgOne ) {
                                            $levelId = $reqOrgProgOne->getFieldOrSpace('educationLevelId');
                                            $levelClasss = null;
                                            if(!empty($levelId)){
                                                $levelClasss = $this->getLevelByProgramm($levelId);
                                            } else {
                                                throw new \Exception('лод - формирование приказов - не прписано уровень у связной программы ид'.$reqOrgProgOne->getId());
                                            }
                                            if(empty($levelClasss)){
                                                throw new \Exception('лод - формирование приказов - не найдека лицензия ид '.$levelId);
                                            }
                                            $levelPrint = $levelClasss->getPrintName();
                                            $parentClass = $this->getLevelParentByProgramm($levelId);
                                            $parentId = $parentClass->getId();
                                            $parentName = $parentClass->getFieldOrSpace('Name');

                                            //общее образование отдельно
                                            if(empty($suplementArray[$organizationId]['blokObjObraz'])){
                                                $suplementArray[$organizationId]['blokObjObraz'] = array();
                                            }
                                            if( '129b813955b94810b92833690d86f8af' == strtolower($parentId)
                                              ||'129b813955b94810b92833690d86f8af' == strtolower($levelId)
                                            ){
                                                $eduLevelTitle = $levelClasss->getForDocumentTitle();
                                                $suplementArray[$organizationId]['blokObjObraz']['ea93fa8ee5bb4be2aa123608778c97b7']['objProgName'] ='Общее образование';
                                                $suplementArray[$organizationId]['blokObjObraz']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableObjProg'][$levelId]['tableObjProg'] = $eduLevelTitle;
                                                //надо отсортировать
                                                //дергаем тип программы
                                                $eduProgramTypeClass = $reqOrgProgOne->getEduProgramType();
                                                $eduProgramTypeOrder = 999;
                                                if(!empty($eduProgramTypeClass)){
                                                    $eduProgramTypeOrder = (int)$eduProgramTypeClass->getFieldOrSpace('SortOrder');
                                                }
                                                $suplementArray[$organizationId]['blokObjObraz']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableObjProg'][$levelId]['tableObjOrder'] = $eduProgramTypeOrder;
                                                uasort(//по типам
                                                    $suplementArray[$organizationId]['blokObjObraz']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableObjProg'],
                                                    function($a, $b){
                                                        $a = (int)$a['tableObjOrder'];
                                                        $b = (int)$b['tableObjOrder'];
                                                        return $a - $b;
                                                    }
                                                );
                                                //пронумеруем программы
                                                $numIt = 1;
                                                foreach ( $suplementArray[$organizationId]['blokObjObraz']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableObjProg'] as $key => $value ) {
                                                    $suplementArray[$organizationId]['blokObjObraz']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableObjProg'][$key]['tableNumber'] = $numIt++;
                                                }
                                                $programCount++;
                                                $programOoCount++;
                                                continue;
                                            }

                                            //дополнительное образование
                                            if( 'ea93fa8ee5bb4be2aa123608778c97b7' == strtolower($parentId )
                                              ||'ea93fa8ee5bb4be2aa123608778c97b7' == strtolower($levelId)
                                              ||'e4ceee07d1ea48dba3699fb897a615ea' == strtolower($parentId )
                                              ||'e4ceee07d1ea48dba3699fb897a615ea' == strtolower($levelId)
                                            ){
                                                $eduLevelTitle = $levelClasss->getForDocumentTitle();
                                                $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg'][$levelId]['tableDopProg'] = $eduLevelTitle;
                                                    //типа сортировка
                                                    if(!empty($suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg'])){
                                                        //поднимим
                                                        //e4ceee07d1ea48dba3699fb897a615ea - Дополнительное профессиональное образование
                                                        if(!empty($suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']['e4ceee07d1ea48dba3699fb897a615ea'])){
                                                            $tmp = $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']['e4ceee07d1ea48dba3699fb897a615ea'];
                                                            unset ($suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']['e4ceee07d1ea48dba3699fb897a615ea']);
                                                            $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']
                                                                    = array_merge(
                                                                            array('e4ceee07d1ea48dba3699fb897a615ea' => $tmp),
                                                                            $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']
                                                                            );
                                                        }
                                                        //поднимим
                                                        //cc0f7a4c13f7444eb8f5d456872ec59f - Дополнительное образование детей и взрослых
                                                        if(!empty($suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']['cc0f7a4c13f7444eb8f5d456872ec59f'])){
                                                            $tmp = $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']['cc0f7a4c13f7444eb8f5d456872ec59f'];
                                                            unset ($suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']['cc0f7a4c13f7444eb8f5d456872ec59f']);
                                                            $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']
                                                                    = array_merge(
                                                                            array('cc0f7a4c13f7444eb8f5d456872ec59f' => $tmp),
                                                                            $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg']
                                                                            );
                                                        }
                                                    }
                                                //пронумеруем программы
                                                $numIt = 1;
                                                foreach ( $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg'] as $key => $value ) {
                                                    $suplementArray[$organizationId]['blokDopProgr']['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg'][$key]['tableNumber'] = $numIt++;
                                                }
                                                $programCount++;
                                                //$blokDopProgr['ea93fa8ee5bb4be2aa123608778c97b7']['dopProgName'] ='Дополнительное образование';
                                                //$blokDopProgr['ea93fa8ee5bb4be2aa123608778c97b7']['_tableDopProg'][]['tableDopProg'] = $eduLevelTitle;
                                                continue;
                                            }
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['megaLevelName'] = $parentName ;
                                            if(empty($suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokmegaShapka'])){
                                                $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokmegaShapka'] = array() ;
                                            }
                                            if(empty($suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'])){
                                                $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'] = array() ;
                                            }
                                            //профессиональное обучение надо только загловок
                                            if('7fe0f78cc98248eeba15fddb7007fcff' == strtolower($parentId)
                                                    ||
                                                '8617B79EA926F24EFE1C6FDA98EE5FA0' == strtoupper($parentId)
                                            //религиозное образование только уровень
                                                    ||
                                                '0864425cd6da44e58d1e1539a488d469' == strtolower($levelClasss->getId())

                                            ){
                                                $programCount++;
                                                continue;
                                            }
                                            $programClass = new \Model\Entities\eiisEduPrograms();
                                            $programId  = $reqOrgProgOne->getFieldOrSpace('educationProgramCatalogItemId');
                                            if(empty($programId )){
                                                //если у указаной программы нет образовательной программы то просто в суперблок и все
                                                continue;
                                                throw new \Exception('лод - формирование приказов - не прписано образовательной программы у связной программы программы '.$reqOrgProgOne->getId());
                                            }
                                            $programClass = $programClass->getById($programId);
                                            if(empty( $programClass )){
                                                throw new \Exception('лод - формирование приказов - не найдена программа '.$programId);
                                            }
                                            $typeClass = $programClass->getEduProgramType();
                                            if(empty( $typeClass )){
                                                continue;
                                                throw new \Exception('лод - формирование приказов - не найдена тип у программы '.$programId);
                                            }
                                            $typeId    = $typeClass->getId();
                                            $typePrint = $typeClass->getLodTitle();
                                            $typeOrder = $typeClass->getFieldOrSpace('SortOrder');
                                            $programCode = $programClass->getFieldOrSpace('Code');
                                            $programName = $programClass->getFieldOrSpace('Name');
                                            $programLevelLod = $levelClasss->getForDocumentTitle();
                                            //$profClass = $programClass->getQualification();
                                            $profName  = $reqOrgProgOne->getQualifictionsNames();
                                            $qualificationId = $reqOrgProgOne->getFieldSafe('qualificationId');
                                            //$programIndex = $reqOrgProgKey;
                                            $programIndex = $programClass->getId();
                                            $programIndex .= $profName;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokmegaShapka'] = array(1) ;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$typeId]['fieldNameMajor'] =  $levelPrint;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$typeId]['fieldNameMinor'] =  $typePrint;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$typeId]['sortOrder'] =  $typeOrder;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$typeId]['_tableProgCode'][$programIndex]['tableProgCode'] = $programCode;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$typeId]['_tableProgCode'][$programIndex]['tableProgName'] = $programName;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$typeId]['_tableProgCode'][$programIndex]['tableProgLevel'] = $programLevelLod ;
                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$typeId]['_tableProgCode'][$programIndex]['tableProgProfesion'] = $profName;
                                            $programCount++;
                                        }
                                    }
                                    //почистим приложения без программ
                                    if(empty($suplementArray[$organizationId]['blokMegaLevel'])
                                    && empty($suplementArray[$organizationId]['blokDopProgr'])
                                    && empty($suplementArray[$organizationId]['blokObjObraz'])
                                    ){
                                        unset($suplementArray[$organizationId]);
                                    }
                                    //пронумеруе
                                    $i = 1; // программы
                                    $j = 2; // доп программы
                                    // отсортиру
                                    if(!empty($suplementArray)){
                                        uasort(//по типам
                                            $suplementArray,
                                            function($a, $b){
                                                $a = $a['fieldfullTitle'];
                                                $b = $b['fieldfullTitle'];
                                                return strcmp($a, $b);
                                            }
                                        );
                                        //главный первым
                                        //$deal = $this->getDeal();
                                        $mainOrgId = -100;
                                        if(!empty($deal)){
                                            $mainOrgId = $deal->getFieldSafe('organizationId');
                                        }
                                        if(!empty($suplementArray[$mainOrgId])){
                                            $tmp = $suplementArray[$mainOrgId];
                                            unset($suplementArray[$mainOrgId]);
                                            $suplementArray = array_merge(array($mainOrgId=>$tmp), $suplementArray);
                                        }
                                    }
                                    //отсортируем
                                    if(!empty($suplementArray[$organizationId]['blokMegaLevel'])){
                                        foreach ( $suplementArray[$organizationId]['blokMegaLevel'] as $parentId => $blokMegaLevelOne ) {

                                            if(!empty($suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'])){
                                                uasort(//по типам
                                                    $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'],
                                                    function($a, $b){
                                                        $a = (int)$a['sortOrder'];
                                                        $b = (int)$b['sortOrder'];
                                                        return $a - $b;
                                                    }
                                                );
                                                foreach ( $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'] as $eduProgramTypeId => $blokTypelOne ) {
                                                    unset($suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['sortOrder']);
                                                    if(!empty($suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['_tableProgCode'])){
                                                        uasort(//по кодам программ
                                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['_tableProgCode'],
                                                            function($a, $b){
                                                                $a = $a['tableProgCode'];
                                                                $b = $b['tableProgCode'];
                                                                return strcmp($a , $b);
                                                            }
                                                        );
                                                        if(1 == count($suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['_tableProgCode'])){
                                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['fieldNameMinor'] =
                                                                $this->mb_str_replace(
                                                                    'программы',
                                                                    'программа',
                                                                    $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['fieldNameMinor']
                                                                );
                                                        }
                                                        //а теперь вклиним нумерацию
                                                        foreach ( $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['_tableProgCode'] as $key => $value ) {
                                                            $suplementArray[$organizationId]['blokMegaLevel'][$parentId]['blokType'][$eduProgramTypeId]['_tableProgCode'][$key]['tableNumber'] = $i++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //сдвинем в низ сепер блоки, которые пустые
                                    if(!empty($suplementArray[$organizationId]['blokMegaLevel'])){
                                        $suplementArrayTmp = $suplementArray;
                                        foreach ( $suplementArrayTmp[$organizationId]['blokMegaLevel'] as $parentId => $blokMegaLevelOne ) {
                                            //задвиним просто надписиуровней в самый низ
                                            if(empty($suplementArrayTmp[$organizationId]['blokMegaLevel'][$parentId]['blokType'])){
                                                $tmp = $suplementArray[$organizationId]['blokMegaLevel'][$parentId];
                                                unset ($suplementArray[$organizationId]['blokMegaLevel'][$parentId]);
                                                $suplementArray[$organizationId]['blokMegaLevel'][$parentId] = $tmp;
                                            }
                                        }
                                        unset($suplementArrayTmp);
                                    }
                                    if(!empty($suplementArray[$organizationId]['blokDopProgr']['_tableDopProg'])){
                                        foreach ( $suplementArray[$organizationId]['blokDopProgr']['_tableDopProg'] as $key => $value ) {
                                            $suplementArray[$organizationId]['blokDopProgr']['_tableDopProg'][$key]['tableNumber'] = $j;
                                        }
                                    }

                                }
                            }
            }
        }
        //пребиваем номера приложений
        if(!empty($suplementArray)){
            if(/*count($suplementArray) > 1 || */$supNum >= 1 ){
                foreach ( $suplementArray as $organizationId => $value ) {
                    if(!empty($suplementArray[$organizationId])){
                        $suplementArray[$organizationId]['fieldSupNum'] = '№ '.((string)$supNum++);
                        //${fieldSupCod}
                        $suplementArray[$organizationId]['fieldSupCod'] = $suplementArray[$organizationId]['fieldSupNum'];
                    }
                }
            }
        }


        $option['programOoCount'] = $programOoCount;
        $option['numberEnd'] = $supNum;
        $option['programCount'] = $programCount;
        return $suplementArray;
    }


    public function getRequestAdressAllCount($requestOrganizationInfo = null, $evryAsk = false){
        $result = 0;
        $groupByOrgLoc = $this->getRequestAdressAllGroupByOrgLoc($requestOrganizationInfo, $evryAsk);
        if ( !empty($groupByOrgLoc) ) {
            $resultAdress = array();
            foreach ( $groupByOrgLoc as $value ) {
                $result++;
            }
        }
        return $result;
    }

    
    public function getRequestAdressAllList($requestOrganizationInfo = null, $evryAsk = false, $glue = '; '){
        $result = '';
        $groupByOrgLoc = $this->getRequestAdressAllGroupByOrgLoc($requestOrganizationInfo, $evryAsk );
        if ( !empty($groupByOrgLoc) ) {
            $resultAdress = array();
            foreach ( $groupByOrgLoc as $value ) {
                $value = array_filter($value);
                $resultAdress[] = implode($glue, $value);
            }
            natsort( $resultAdress );
            $resultAdress = array_filter($resultAdress);
            $result = implode($glue, $resultAdress);
        }
        return $result;
    }
    
    private $_levelClass = array();
    public function getLevelByProgramm($levelId) {
        if(!empty($this->_levelClass[$levelId])){
            return $this->_levelClass[$levelId];
        }
        $levelClaess = new \Model\Entities\eiisEduLevels();
        $this->_levelClass[$levelId] = $levelClaess->getById($levelId);
        return $this->_levelClass[$levelId];
    }
    
    public function getLevelParentByProgramm($levelId) {
        $level = $this->getLevelByProgramm($levelId);
        if(empty($level)){
            return null;
        }
        $parentId = $level->getField('parentlevel');
        if(empty($parentId)){
            return $level;
        }
        $parent =  $this->getLevelByProgramm($parentId );
        return $parent;
    }
    
    
    
    //для шаблона

    /**
     * раскидываем имя полное и короткое
     * ${fieldfullTitle}
     * ${fieldshortTitle}
     * 
     * @param type $result
     */
    public function parseOrgInfo(&$result) {

        $reqOrgInfo = $this->getRequestOrganizationInfo();

        $fieldfullTitle = $reqOrgInfo ->getFieldSafe('fullTitle');
        $fieldfullTitleArray = $this->parseStrtoArray($fieldfullTitle,array(75,75,75,75,75,75,75,75,75,75,75,75,75,75,));
        //${_fieldfullTitle}
        //     ${fieldfullTitle}
        foreach ( $fieldfullTitleArray as $licDataKey => $licDataOne ) {
            $result['_fieldfullTitle'][]['fieldfullTitle'] = $licDataOne;
        }

        //${_fieldshortTitle}
        //      ${fieldshortTitle}
        $result['_fieldshortTitle'] = array();
        $shortTitle = $reqOrgInfo ->getFieldSafe('shortTitle');
        if( !empty($shortTitle) ){
            $shortTitle = '('.$shortTitle.')';
            $fieldfullTitleArray = $this->parseStrtoArray($shortTitle,array(75,75,75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
            foreach ( $fieldfullTitleArray as $licDataKey => $licDataOne ) {
                $result['_fieldshortTitle'][]['fieldshortTitle'] = $licDataOne;
            }
        }
    }    
    

    /**
     * раскидываем филиала  имя полное и короткое, адрес
     *
     * Полное и (в случае, если имеется) сокращенное наименование и место нахождения филиала соискателя лицензии
     *
     * По LicenseRequestId выводим полные наименования fullTitle  филиалов из табл. RequestOrganizationInfo. В скобках выводим краткие наименования shortTitle.
     * Каждый филиал отдельным блоком (данные + адреса + программы).
     * Сортируем филиалы по названию по алфавиту
     * 
     * ${fieldfullTitleFil}
     * ${tableFullTitleFil}
     * ${fieldaddressFil}
     * 
     * @param type $result
     */
    public function parseOrgInfoFilial(&$result, $reqOrgInfo ) {

        $fieldfullTitle = $reqOrgInfo ->getFieldSafe('fullTitle');
        $fieldfullTitleArray = $this->parseStrtoArray($fieldfullTitle,array(45,75,75,75,75,75,75,75,75,75,75,75,75,75,));
        //${fieldfullTitleFil}
        $fieldfullTitleFirst = array_shift($fieldfullTitleArray);
        $fieldfullTitleFirst = empty($fieldfullTitleFirst)?:$fieldfullTitleFirst;
        $result['fieldfullTitleFil'] = $fieldfullTitleFirst;
        //     ${tableFullTitleFil}
        $result['_tableFullTitleFil'] = array();
        foreach ( $fieldfullTitleArray as $licDataKey => $licDataOne ) {
            $result['_tableFullTitleFil'][]['tableFullTitleFil'] = $licDataOne;
        }

        //${fieldshortTitleFil}
        $result['_fieldshortTitleFil'] = array();
        $shortTitle = $reqOrgInfo ->getFieldSafe('shortTitle');
        if( !empty($shortTitle) ){
            $shortTitle = '('.$shortTitle.')';
            $fieldfullTitleArray = $this->parseStrtoArray($shortTitle,array(75,75,75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
            foreach ( $fieldfullTitleArray as $licDataKey => $licDataOne ) {
                $result['_fieldshortTitleFil'][]['fieldshortTitleFil'] = $licDataOne;
            }
        }

        //${fieldaddressFil}
        $fieldaddress = $this->fixAdressOrganisation($reqOrgInfo ->getFieldSafe('address'));
        $fieldaddressArray = $this->parseStrtoArray($fieldaddress,array(75,75,75,75,75,75,75,));
        $result['_fieldaddressFil'] = array();
        foreach ( $fieldaddressArray as $fieldaddressKey => $fieldaddressOne ) {
            $result['_fieldaddressFil'][]['fieldaddressFil'] = $fieldaddressOne;
        }

    }


    /**
     * раскидываем адрес
     * ${fieldaddress}
     * ${tableAddress}
     * 
     * @param type $result
     */
    public function parseAdress(&$result) {

        $reqOrgInfo = $this->getRequestOrganizationInfo();

        $fieldaddress = $this->fixAdressOrganisation($reqOrgInfo ->getFieldSafe('address'));
        //${fieldAddress}
        //      ${tableAddress}
        $result['_tableAddress'] = array();
        //if( mb_strlen($fieldaddress) ){
        //    $result['fieldaddress'] = '';
        //    $result['_tableAddress'][]['tableAddress'] = $fieldaddress;
        //}
        //else{
            $fieldaddressArray = $this->parseStrtoArray($fieldaddress,array(55,75,75,75,75,75,75,75,));
            $result['fieldAddress'] = array_shift($fieldaddressArray);
            foreach ( $fieldaddressArray as $fieldaddressKey => $fieldaddressOne ) {
                $result['_tableAddress'][]['tableAddress'] = $fieldaddressOne;
            }
            //if( empty($result['_tableAddress'][0]['tableAddress']) ){
            //    $result['_tableAddress'][0]['tableAddress'] = '';
            //}
        //}

    }

    /**
     * ${fieldNotifyStr}        - ыводим «Да» если в LicenseRequest.notificationRequired =1, и выводим «Нет» если =0
     * ${fieldFilingDay}        - Текущая дата
     * ${fieldFilingMount} 
     * ${fieldFilingYear} 
     * ${requestSignerPosition} - LicenseRequest.requestSignerPosition
     * ${requestSignerLastName} - requestSignerLastName и инициалы requestSignerFirstName requestSigner MiddleName Табл. LicenseRequest
     * 
     * @param type $result
     */
    public function parsePodpisano(&$result) {
        //${fieldNotifyStr}
        $notifReq =  $this->requestClass->getFieldSafe('notificationRequired');
        if ( empty($notifReq) ){
            $result['fieldNotifyStr'] = 'Нет';
        }
        else{
            $result['fieldNotifyStr'] = 'Да';
        }
        //${fieldFilingDay}
        //${fieldFilingMount}
        //${fieldFilingYear}
        //$fillingDate = $this->requestClass->getDate('fillingDate');
        $fillingDate = date( 'd.m.Y' );
        $result['fieldFilingDay'] =  $this->getDayOfDate($fillingDate);
        $result['fieldFilingMount'] =  $this->getMountOfDate($fillingDate);
        $result['fieldFilingYear'] =  $this->getYearOfDate($fillingDate);
        //${requestSignerPosition}
        $result['requestSignerPosition'] = $this->getEmployeePostTitle();
        //${requestSignerLastName}
        $result['requestSignerLastName'] = $this->getRequestSignerFioShort();
        
    }
    //последняя строка
}
