<?php

namespace Eo\Model\Licensing\Request;

/**
 * Заявление на лицендзирование
 *
 * переоформление
 *
 *
 *
 * @author any
 */
class StopLicFilial extends Request {

    public function getData() {
        $result = array();
        
        $this->parseOrgInfo($result);
        $this->parseAdress($result);                
        $reqOrgInfo = $this->getRequestOrganizationInfo();  
                           
        $reqOrgInfo = $this->getRequestOrganizationInfo();
        $propertiesArray = $this->parseStrtoArray( $reqOrgInfo->getOrganizationPropertiesName() , array(32,70,70,70,70,70,70,70,70,70,70,70) );
        $result['fieldProperties']   = array_shift( $propertiesArray );
        //${tableProperties}  - правоая организационноя форма
        $result['_tableProperties'] = array();
        foreach( $propertiesArray as $key => $value ){
            $result[ '_tableProperties' ][]['tableProperties'] = $value;
        }

        $result['fieldOGRN'] = $reqOrgInfo ->getFieldSafe('ogrn');
        $result['fieldINN'] = $reqOrgInfo ->getFieldSafe('inn');               
        $result['fieldReqLicDate']   = $this->requestClass->getDate('licenseDate');
        $result['fieldReqLicNum']    = $this->requestClass->getFieldSafe('licenseRegistrationNumber');
        $result['fieldReqLicSeries'] = $this->requestClass->getFieldSafe('licenseSeries');
        $result['fieldReqLicBlank']  = $this->requestClass->getFieldSafe('licenseBlankNumber');
        $result['fieldPhones'] = $reqOrgInfo ->getFieldSafe('licenseePhone');
        $result['fieldMails'] = $reqOrgInfo ->getFieldSafe('licenseeEmail');
        $this->parsePodpisano($result);

        $orgInfoFilialAll = $this->requestClass->getOrganizationsInfoWhithoutMain();

        //${blockFilial}  ${/blockFilial}
        $result['blockFilialAll'] = array();
        if(  !empty($orgInfoFilialAll) ){
            $orgInfoFilialAll = $orgInfoFilialAll->getObjectsArray();
            uasort(
                $orgInfoFilialAll,
                function($aO, $bO){
                    $aF = $aO->getFieldSafe('fullTitle');
                    $bF = $bO->getFieldSafe('fullTitle');
                    return strcmp($aF, $bF);
                }
            );
            foreach ( $orgInfoFilialAll as $orgInfoFilialKey => $orgInfoFilialOne ) {
                $orgInfoFilialAllKey = $orgInfoFilialOne->getId();
                //${blockFilial}  ${/blockFilial}
                //${blockFilialAll}  ${/blockFilialAll}
                $result['blockFilialAll'][$orgInfoFilialAllKey] = array();
                $this->parseOrgInfoFilial( $result['blockFilialAll'][$orgInfoFilialAllKey], $orgInfoFilialOne );                
            }
        }
        
        //дергаем связные приложения 
        $suppl = $this->requestClass->getSupplements();        
        //${fieldCountFil}
        $result['fieldCountFil'] = count( $suppl > 1 )? 'приложениями' : 'приложением';
        $suplNumArray = array();
        foreach($suppl as $suplOne){
            $suplNumArray[] = $suplOne->getFieldSafe( 'Number' );
        }
        uasort(
            $suplNumArray, 
            function( $a , $b ){
                return strnatcmp( $a , $b );
            }
        );
        //${fieldCountNum}
        $result['fieldCountNum'] = implode( ', ' , $suplNumArray );//self::numberingDiadozon( $suplNumArray );                     

        return $result;

    }

    public function getTemplateName(){
        return 'licensing/Zayavlenie_StopLic_Filial.docx';
    }

    public function getDocumentName(){
          return 'Прекращение ОД в филиале';
    }



    //last string

}
