<?php

namespace Eo\Model\Licensing\Request;

/**
 * Заявление на лицендзирование
 *
 * дупликат
 *
 * Description of Duplicat
 *
 * @author any
 */
class Duplicat extends Request {

    public function getData() {
        $result = array();
        $nonBreakSpace = $this->unichr(160);


        $suplMtm = new \Model\LodEntities\LicenseSupplementToLicenseRequest();
        $suplMtmAll = $suplMtm->getAllByWhereParseId( 
            array( 'licenseRequestId'=>$this->requestClass->getId() ),
            'licenseAttachId'
        );
        $suplNumArray = array();

        //${fieldTextNumSupp}
        $result['fieldTextNumSupp'] = '';
        if( !empty($suplMtmAll) ){
            $suplMtmAll = array_keys( $suplMtmAll );
            $suplAll = new \Model\Entities\eiisLicenseSupplements();
            $suplAll = $suplAll->getAllByWhereParseId( 
                array( $suplAll->getIdName()=>  $suplMtmAll),
                'Number'
            );
            $suplNumArray = array_keys( $suplAll );
            
            uasort(
                $suplNumArray,
                function( $a , $b ){
                    return strnatcmp( $a , $b );
                }
            );
            //${fieldCountNum}
            $result['fieldTextNumSupp'] = 
                'и (или) приложения (приложений) № '
                .self::numberingDiadozon( $suplNumArray )
                .' к лицензии (временной лицензии) на осуществление образовательной деятельности';
        }
        unset( $suplMtm );
        unset( $suplMtmAll );
        
        
        
        //${fieldText1}
        $result[ 'fieldText1' ] = ''; 
        //${fieldText2}
        $result[ 'fieldText2' ] = ''; 
        //${fieldText3}
        $result[ 'fieldText3' ] = ''; 
        //${fieldText3B}
        $result[ 'fieldText3B' ] = '';
        $isLicens = (int) $this->requestClass->getFieldSafe( 'isDuplicate' );
        //$suplCount = count( $LicenseSupplementToLicenseReques );
        //только лицендзии
        if( $isLicens && empty( $suplNumArray ) ){
            $result[ 'fieldText1' ] = ' лицензии (временной лицензии) на осуществление образовательной деятельности';
            $result[ 'fieldText2' ] = 'на осуществление образовательной деятельности';
            $result[ 'fieldText3' ] = 'лицензии (временной лицензии) на осуществление образовательной деятельности';            
        }
        //только приложения
        if( !$isLicens &&  count( $suplNumArray )  ){
            $result[ 'fieldText1' ] = ' ';
            if( 1 == count( $suplNumArray ) ){
                $result[ 'fieldText1' ] .= 'приложения';
                $result[ 'fieldText2' ] = 'приложения';
                $result[ 'fieldText3' ] = 'приложения';            
            }
            else{
                $result[ 'fieldText1' ] .= 'приложений';
                $result[ 'fieldText2' ] = 'приложений к ней';
                $result[ 'fieldText3' ] = 'приложений';            
            }
            $result[ 'fieldText1' ] .= ' №' . $nonBreakSpace.$this->numberingDiadozon( $suplNumArray ).' к лицензии (временной лицензии) на осуществление образовательной деятельности';            
        }
        //и лицензии и приложйния
        if( $isLicens && count( $suplNumArray ) ){
            $result[ 'fieldText1' ] = ' лицензии (временной лицензии) на осуществление образовательной деятельности и ';
            if( 1 == count( $suplNumArray) ){
                $result[ 'fieldText1' ] .= 'приложения';
                $result[ 'fieldText2' ] = 'на осуществление образовательной деятельности и приложения  к ней';
                $result[ 'fieldText3' ] = 'лицензии (временной лицензии) на осуществление образовательной деятельности и приложения к ней';     
            }
            else{
                $result[ 'fieldText1' ] .= 'приложений';
                $result[ 'fieldText2' ] = 'на осуществление образовательной деятельности и приложений к ней';
                $result[ 'fieldText3' ] = 'лицензии (временной лицензии) на осуществление образовательной деятельности и приложений к ней';      
            }
            $result[ 'fieldText1' ] .= ' №' . $nonBreakSpace.$this->numberingDiadozon( $suplNumArray ).' к лицензии (временной лицензии) на осуществление образовательной деятельности';           
        }        
        $text3Array = $this->parseStrtoArray( $result[ 'fieldText3' ] ,array(75,55,), array('notReturnOnQuotes' =>1 ) );
        //$result[ 'fieldText3' ] = array_shift( $text3Array );
        //$result[ 'fieldText3B' ] = implode( '' , $text3Array );
        $result[ 'fieldText3B' ] = '';
        
        
        
        
        
        
        
        
        

        $result['fieldReqLicDate'] = $this->requestClass->getDate('licenseDate');
        $result['fieldReqLicNum'] = $this->requestClass->getFieldSafe('licenseRegistrationNumber');
        $result['fieldReqLicSeries'] = $this->requestClass->getFieldSafe('licenseSeries');
        $result['fieldReqLicBlank'] = $this->requestClass->getFieldSafe('licenseBlankNumber');

        $licData = $this->getLicenceDataStr();
        $licDataArray = $this->parseStrtoArray($licData,array(85,85));
        //${_tabliLicDat}
        //     ${tabliLicDat}
        foreach ( $licDataArray as $licDataKey => $licDataOne ) {
            $result['_tabliLicDat'][]['tabliLicDat'] = $licDataOne;
        }
        
        $reqOrgInfo = $this->getRequestOrganizationInfo();

        $fieldfullTitle = $reqOrgInfo ->getFieldSafe('fullTitle');
        $fieldfullTitleArray = $this->parseStrtoArray($fieldfullTitle,array(75,75,75,75,75,75,75,75,75,75,75,75,75,75,));
        //${_fieldfullTitle}
        //     ${fieldfullTitle}
        foreach ( $fieldfullTitleArray as $licDataKey => $licDataOne ) {
            $result['_fieldfullTitle'][]['fieldfullTitle'] = $licDataOne;
        }

        $result['_fieldshortTitle'] = array();
        $shortTitle = $reqOrgInfo ->getFieldSafe('shortTitle');
        if( !empty($shortTitle) ){
            $result['_fieldshortTitle'][]['fieldshortTitle'] = $shortTitle;
        }
        
        $result['fieldProperties']   = $reqOrgInfo->getOrganizationPropertiesName();

        $fieldaddress = $this->fixAdressOrganisation($reqOrgInfo ->getFieldSafe('address'));
        //${_tableAddress}
        //      ${tableAddress}        $result['_tableAddress'] = array();
        if( mb_strlen($fieldaddress) ){
            $result['fieldaddress'] = '';
            $result['_tableAddress'][]['tableAddress'] = $fieldaddress;
        }
        else{
            $fieldaddressArray = $this->parseStrtoArray($fieldaddress,array(40,75,75,));
            $result['fieldaddress'] = array_shift($fieldaddressArray);
            foreach ( $fieldaddressArray as $fieldaddressKey => $fieldaddressOne ) {
                $result['_tableAddress'][]['tableAddress'] = $fieldaddressOne;
            }
            //if( empty($result['_tableAddress'][0]['tableAddress']) ){
            //    $result['_tableAddress'][0]['tableAddress'] = '';
            //}
        }

        $result['fieldOGRN'] = $reqOrgInfo ->getFieldSafe('ogrn');
        $result['fieldINN'] = $reqOrgInfo ->getFieldSafe('inn');
        
        $orClass = $this->requestClass->getEducationalOrganization();
        //${fieldtaxAccountCertificateNumber} 
        //${fieldtaxAccountCertificateDate}
        $result['fieldtaxAccountCertificateNumber'] = $orClass->getFieldSafe('taxAccountCertificateNumber');
        $result['fieldtaxAccountCertificateDate'] =   $orClass->getDate('taxAccountCertificateDate');             
        unset( $orClass );
        
        //${fieldProcedureReason}
        $reasonArray = array();
        if( $this->requestClass->hasReasonById('efb3b9bf460c4a8aae48b9dd5e5d5d84') ){
            $reasonArray[] = 'Утратой лицензии';
        }
        if( $this->requestClass->hasReasonById('1c3acd333a204d5288277d2c2fd3509b') ){
            $reasonArray[] = 'Порчей лицензии';
        }
        if( !count($reasonArray) ){
            $result['fieldProcedureReason'] = '-';
        }
        if( count($reasonArray) == 1 ){
            $result['fieldProcedureReason'] = reset($reasonArray);
        }
        if( count($reasonArray) > 1 ){
            $result['fieldProcedureReason'] = implode( ' и ' , $reasonArray );
        }
        if( !$isLicens ){
            $result['fieldProcedureReason'] = str_replace( 'лицензии', '' , $result['fieldProcedureReason'] );
        }
         $result['fieldProcedureReason'] = ' '. $result['fieldProcedureReason'] . ' ';

        //${fieldPaymentStr}
        $result['fieldPaymentStr'] = $this->getLicenseRequestPaymentAllStr();
        $result['fieldPhones'] = $reqOrgInfo ->getFieldSafe('licenseePhone');
        $result['fieldMails'] = $reqOrgInfo ->getFieldSafe('licenseeEmail');

        //${fieldNotifyStr}
        $notifReq =  $this->requestClass->getFieldSafe('notificationRequired');
        if ( empty($notifReq) ){
            $result['fieldNotifyStr'] = 'Нет';
        }
        else{
            $result['fieldNotifyStr'] = 'Да';
        }

        //${fieldFilingDay}
        //${fieldFilingMount}
        //${fieldFilingYear}
        //$fillingDate = $this->requestClass->getDate('fillingDate');
        $fillingDate = date( 'd.m.Y' );
        $result['fieldFilingDay'] =  $this->getDayOfDate($fillingDate);
        $result['fieldFilingMount'] =  $this->getMountOfDate($fillingDate);
        $result['fieldFilingYear'] =  $this->getYearOfDate($fillingDate);

        //${requestSignerPosition}
        $result['requestSignerPosition'] = $this->getEmployeePostTitle();

        //${requestSignerLastName}
        $result['requestSignerLastName'] = $this->getRequestSignerFioShort();
        
        $result[ 'fieldNotifyStr' ] = $nonBreakSpace.$nonBreakSpace.$nonBreakSpace.$result[ 'fieldNotifyStr' ].$nonBreakSpace.$nonBreakSpace.$nonBreakSpace;

        return $result;

    }

    public function getTemplateName(){
        return 'licensing/Zayavlenie_Dublicat.docx';
    }

    public function getDocumentName(){
          return 'Заявление на предоставление дубликата';
    }

}
