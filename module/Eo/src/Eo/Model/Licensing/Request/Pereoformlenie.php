<?php

namespace Eo\Model\Licensing\Request;

/**
 * Заявление на лицендзирование
 *
 * переоформление
 *
 *
 *
 * @author any
 */
class Pereoformlenie extends Request {

    public function getData() {
        mb_internal_encoding('UTF-8');
        $nonBreakSpace = $this->unichr(160);
        $result = array();



        //$dealClass = $this->requestClass->getDeal();
        //$requestClass = $this->requestClass;
        //$isDuplicate = (int) $this->requestClass->getFieldSafe( 'isDuplicate' );
        $LicenseSupplementToLicenseReques = new \Model\LodEntities\LicenseSupplementToLicenseRequest();
        $LicenseSupplementToLicenseReques = $LicenseSupplementToLicenseReques->getAllByWhereParseId( array( 'licenseRequestId' => $this->requestClass->getId() ) );


        //${fieldPeroformSupl}
        //приложение                                            - Если приложение одно, то выводим текст «приложение».Если licenseAttachId >1, то выводим «приложения»
        //или приложение № ${fieldNumSupp} к лицензии (временной лицензии) на осуществление образовательной деятельности    -  этот текст выводим при наличии записи в LicenseSupplementToLicenseRequest. Т.е. по licenseRequestId нам нужно получить из этой таблицы licenseAttachId и вывести номера приложений eiis_LicenseSupplements.Number через запятую
        $result[ 'fieldPeroformSupl' ] = '';
        if( count($LicenseSupplementToLicenseReques) ){
            if( 1 == count($LicenseSupplementToLicenseReques)  ){
                $LicenseSupplementToLicenseRequesOne = reset($LicenseSupplementToLicenseReques);
                $LicenseSuplClass = $LicenseSupplementToLicenseRequesOne->getLicenseSupplement();
                $s  =  ' или приложение №'.$nonBreakSpace. $LicenseSuplClass->getFieldSafe( 'Number' ).' к лицензии ';
                $ss =  'дубликата приложения №'.$nonBreakSpace. $LicenseSuplClass->getFieldSafe( 'Number' ).' к лицензии ';
                unset( $LicenseSupplementToLicenseRequesOne );
                unset(  $LicenseSuplClass );
            }
            else{
                $arrayNum = array();
                foreach( $LicenseSupplementToLicenseReques as $mtmOne ){
                    $LicenseSuplClass = $mtmOne->getLicenseSupplement();
                    $arrayNum[] =  $LicenseSuplClass->getFieldSafe( 'Number' );
                }
                $s  = ' или приложения №№'.$nonBreakSpace.$this->numberingDiadozon( $arrayNum ).' к лицензии ';
                $ss = 'дубликатов приложений №№'.$nonBreakSpace.implode( ', ' , ( $arrayNum ) ).' к лицензии ';
                unset( $arrayNum );
                unset( $LicenseSuplClass );
                unset(  $arrayNum );
            }
             $result[ 'fieldPeroformSupl' ] = $s.' к лицензии (временной лицензии) на осуществление образовательной деятельности';
        }

        //${fieldReqLicDate}            - LicenseRequest.licenseDate в формате  ДД.ММ.ГГГГ
        $result['fieldReqLicDate']   = $this->requestClass->getDate('licenseDate');
        //${fieldReqLicNum},            - LicenseRequest.licenseRegistrationNumber
        $result['fieldReqLicNum']    = $this->requestClass->getFieldSafe('licenseRegistrationNumber');
        //${fieldReqLicSeries}          - LicenseRequest.licenseSeries
        $result['fieldReqLicSeries'] = $this->requestClass->getFieldSafe('licenseSeries');
        //${fieldReqLicBlank}           - LicenseRequest.licenseBlankNumber Желательно все в одну строку вывести
        $result['fieldReqLicBlank']  = $this->requestClass->getFieldSafe('licenseBlankNumber');

        //${fieldProcedureReason}       - для LicenseRequestвыводим через точку с запятой все причины обращений из таблицы LicenseReasonToLicenseRequest. Справочник причин обращений - таблица LicenseReasonCatalogItem . Через точку с запятой выводим значения из поля DocumentPrint. После последней ставим точку
        //${tableProcedureReason}       - на этой строке продолжаем текст причин.Пустую строку не выводим
        $this->parseReason($result);

        //${blockReorg}                 - Начало блока реорганизации
            $result[ 'blockReorg' ] = array();

            $reorgInfoAll = new \Model\LodEntities\ReorganizedOrganizationInfo();
            $reorgInfoAll = $reorgInfoAll->getAllByWhereParseId( array( 'licenseRequestId' => $this->requestClass->getId() ) , 'organizationTitle' );
            foreach( $reorgInfoAll as $reorgInfoKey => $reorgInfoOne ){
                //${fieldfullTitleReorg}        -По licenseRequestId выбираем записи из ReorganizedOrganizationInfo и выводим organizationTitle . Каждую реорганизованную Organization выводим отдельным блоком
                foreach ($arrayNum as $key => $value ){
                    $result[ 'blockReorg' ][ $key ][ 'fieldfullTitleReorg' ] = array();
                    foreach( array_keys($reorgInfoAll) as $value ){
                        $result[ 'fieldfullTitleReorg' ][] = $this->parseStrtoArray( $value ,array(75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
                    }
                }
                $reorLicenseClass = $value->getEiisLicese();
                //${fieldReorgLicDate}  - Из ReorganizedOrganizationInfo получаем licenseId и выводим ее дату из eiis_Licenses.DateLicDoc
                $result[ 'blockReorg' ][ $key ][ 'fieldReorgLicDate' ] = $reorLicenseClass->getDate( 'DateLicDoc' );
                //${fieldReorgLicNum}  - eiis_Licenses.NumLicDoc
                $result[ 'blockReorg' ][ $key ][ 'fieldReorgLicNum' ] = $reorLicenseClass->getFieldSafe( 'NumLicDoc' );
                //${fieldReorgLicSeries}   - eiis_Licenses.SerDoc
                $result[ 'blockReorg' ][ $key ][ 'fieldReorgSerDoc' ] = $reorLicenseClass->getFieldSafe( 'SerDoc' );
                //${fieldReorgLicBlank}     - eiis_Licenses.NumDoc
                $result[ 'blockReorg' ][ $key ][ 'fieldReorgLicBlank' ] = $reorLicenseClass->getFieldSafe( 'NumDoc' );
                //${fieldControlOrgan}      - по полученному eiis_Licenses.fk_eiisControlOrgan выводим eiis_ControlOrgans.Name
                $result[ 'blockReorg' ][ $key ][ 'fieldControlOrgan' ] = $reorLicenseClass->getControlOrgan()->gettFieldSafe( 'Name' );
            }
            unset( $reorgInfoAll );
            unset( $orgInfoStrArray );
            unset( $reorLicenseClass );
        //${/blockReorg}


        //${fieldshortTitle}        - По LicenseRequestId из табл. LicenseRequest берем organizationId головного вуза. Для нее и выводим полное наименование fullTitle из табл.RequestOrganizationInfo.
        //${fieldshortTitle}        - В скобках выводим краткое наименование организации shortTitle из этой же таблицы
        $this->parseOrgInfo($result);
        //${fieldProperties}        - Для головной организации в табл. RequestOrganizationInfo берем fk_eiisEducationalOrganizationProperties и выводим для него Name из табл. eiis_EducationalOrganizationProperties
        $this->parsePropertiesMany($result);
        //${fieldaddress}           - на этой строке продолжаем текст.Пустую строку не выводим
        $this->parseAdress($result);

        //${fieldrequestAddressActive}      - Для requestOrganizationInfoId головной организации выбираем записи из табл. LicenseRequestAddress с типом requestAddressType = ACTIVE и выводим postcode и через запятую address. Каждый адрес с новой строки
        $this->parseRequestAddress($result, array('ACTIVE'),  'fieldrequestAddressActive' );

        //${blockFieldrequestAddressOpen}   ${/blockFieldrequestAddressOpen}
        //${fieldrequestAddressOpen}        - Для requestOrganizationInfoId головной организации выбираем записи из табл. LicenseRequestAddress с типом requestAddressType = OPENING и выводим postcode и через запятую address. Каждый адрес с новой строки. Если таких адресов нет - блок не выводим
        $this->parseRequestAddressInBlock($result, array('OPENING'), 'fieldrequestAddressOpen' );

        //${blockFieldrequestAddressClose}  ${/blockFieldrequestAddressClose}
        //${fieldrequestAddressClose}       - Для requestOrganizationInfoId головной организации выбираем записи из табл. LicenseRequestAddress с типом requestAddressType = CLOSING и выводим postcode и через запятую address. Каждый адрес с новой строки.Если таких адресов нет - блок не выводим
        $this->parseRequestAddressInBlock($result, array('CLOSE'), 'fieldrequestAddressClose' );

        $reqOrgInfo = $this->getRequestOrganizationInfo();
        //${fieldOGRN}          - Для головной организации из табл.requestOrganizationInfo выводим ogrn
        $result['fieldOGRN'] = $reqOrgInfo ->getFieldSafe('ogrn');
        $this->parseStateRegistrationCertificate($result);

        //${fieldStateReg}
        //${tableStateReg}
        //stateRegistrationCertificateNumber
        //stateRegistrationCertificateDate
        //stateRegistrationCertificateAuthorityAddress
        //№, дату и адрес выводим для головной организации из табл. eiis_EducationalOrganizations при наличии
        $eduOrg = $reqOrgInfo->getEiisEducationalOrganizations();
        $result[ 'fieldStateReg' ] = $eduOrg->getFieldSafe( 'stateRegistrationCertificateNumber' );
        $result[ '_tableStateReg' ] = array();
        if( !empty( $result[ 'fieldStateReg' ] ) ){
            $result[ 'fieldStateReg' ] .= ' от'.$nonBreakSpace.$eduOrg->getDate( 'stateRegistrationCertificateDate' );
            $result[ 'fieldStateReg' ] .= $eduOrg->getFieldSafe( 'stateRegistrationCertificateAuthorityAddress' );
            $result[ '_tableStateReg' ] = $this->parseStrtoArray( $result[ 'fieldStateReg' ] , array(75,75,75,75,75,75,75,75,75,75,75,) );
            $result[ 'fieldStateReg' ] = array_shift( $result[ '_tableStateReg' ] );
        }

        //${fieldINN}       -из табл.RequestOrganizationInfo.Inn
        $result['fieldINN'] = $reqOrgInfo ->getFieldSafe('inn');
        //${fieldKPP}       - из табл.RequestOrganizationInfo.Kpp
        $result['fieldKPP'] = $reqOrgInfo ->getFieldSafe('kpp');


        $eduOrgClass = $this->requestClass->getEducationalOrganization();
        //${fieldtaxDate}  taxAccountDate из табл. eiis_EducationalOrganizations в формате ДД.ММ.ГГГГ
        $result['fieldtaxDate'] = $eduOrgClass->getDate('taxAccountDate');
        //${fieldtaxAccountCertificateNumber} ${fieldtaxAccountCertificateDate}
        //${tableTaxAccount}
        //№ taxAccountCertificateNumber  и дату taxAccountCertificateDate свидетельства выводим для головной организации из табл. eiis_EducationalOrganizations при наличии
        $this->parseFieldtaxAccountCertificate($result, $eduOrgClass);

        //приложинеия--------------------------------------------------------------------------------------------------------------------------------------------
        //${blockSuplement} ${/blockSuplement}
        $requestOne = $reqOrgInfo;
        $supNum = 0;
        if(empty($options['notMakeItVuzId'])){
            $options['notMakeItVuzId'] = null;
        }
        $tempArray =
            $option =  array(
                    'numberBegin'    => $supNum+1,
                    'notMakeItVuzId' => $options['notMakeItVuzId'],
                    //'forVuzId'       => $eduOrgClass->getId()
                );
            $this->getSuplementTypecal(
                $result['blockSuplement'],
                array('OPENING','EDITING'),
                $requestOne,
                $option
            );
        $supNum = count($result['blockSuplement']);
        if(count($result['blockSuplement']) == 1){
            reset($result['blockSuplement']);
            $firstKey = key($result['blockSuplement']);
            $result['blockSuplement'][$firstKey]['fieldSupNum'] = '';
        }
        //приложениея конец------------------------------------------------------------

        //${fieldPhones}        - Телефон/факс головного вуза licenseePhone из табл. RequestOrganizationInfo
        $result['fieldPhones']  = $reqOrgInfo ->getFieldSafe('licenseePhone');
        //${fieldMails}         - Почта головного вуза licenseeEmail из табл. RequestOrganizationInfo
        $result['fieldMails']   = $reqOrgInfo ->getFieldSafe('licenseeEmail');

        //филиалы----------------------------------------------------------------------------------
        $orgInfoFilialAll = $this->requestClass->getOrganizationsInfoWhithoutMain();

        //${blockFilial}  ${/blockFilial}
        //Блок филиала Начало
        //Если филиалов в заявлении нет – блок не выводим

        $result['blockFilialAll'] = array();
        if(  !empty($orgInfoFilialAll) ){
            $orgInfoFilialAll = $orgInfoFilialAll->getObjectsArray();
            uasort(
                $orgInfoFilialAll,
                function($aO, $bO){
                    $aF = $aO->getFieldSafe('fullTitle');
                    $bF = $bO->getFieldSafe('fullTitle');
                    return strcmp($aF, $bF);
                }
            );
            foreach ( $orgInfoFilialAll as $orgInfoFilialKey => $orgInfoFilialOne ) {
                $orgInfoFilialAllKey = $orgInfoFilialOne->getId();
                //${blockFilial}  ${/blockFilial}
                //${blockFilialAll}  ${/blockFilialAll}
                $result['blockFilialAll'][$orgInfoFilialAllKey] = array();
                // ${fieldfullTitleFil}         - По LicenseRequestId выводим полные наименования fullTitle филиалов из табл. RequestOrganizationInfo.
                // ${tableFullTitleFil}         -  В скобках выводим краткие наименования shortTitle. Каждый филиал должен быть отдельным блоком (данные + адреса + программы).Сортируем филиалы по fullTitle по алфавиту А-Я
                // ${fieldaddressFil}           - адрес филиала RequestOrganizationInfo.address
                $this->parseOrgInfoFilial( $result['blockFilialAll'][$orgInfoFilialAllKey], $orgInfoFilialOne );
                //${fieldrequestAddressFilActive}       - Для requestOrganizationInfoId филиалов выбираем записи из табл. LicenseRequestAddress с типом requestAddressType = ACTIVE  и выводим postcode и через запятую address. Каждый адрес с новой строки
                //${tablerequestAddressFilActive}
                $this->parseRequestAddressFilial( $result['blockFilialAll'][$orgInfoFilialAllKey], array('ACTIVE'),  'requestAddressFilActive', $orgInfoFilialOne, 60 );
                
                //${blockrequestAddressFilOpen}    ${/blockrequestAddressFilOpen}
                //${fieldrequestAddressFilOpen}         - Для requestOrganizationInfoId филиалов выбираем записи из табл. LicenseRequestAddress с типом requestAddressType = OPENING и выводим postcode и через запятую address. Каждый адрес с новой строки. Если адресов OPENING для филиала нет, то этот блок с адресами не выводим
                //${tablerequestAddressFilOpen}
                $this->parseRequestAddressFilialInBlock( $result['blockFilialAll'][$orgInfoFilialAllKey], array('OPENING'),  'requestAddressFilOpen', $orgInfoFilialOne, 50 );
                
                //${blockrequestAddressClose}      ${/blockrequestAddressClose}
                //${fieldrequestAddressClose}           - Для requestOrganizationInfoId филиала выбираем записи из табл. LicenseRequestAddress с типом requestAddressType = CLOSING и выводим postcode и через запятую address. Каждый адрес с новой строки.Если таких адресов нет - блок не выводим
                //${tablerequestAddressClose}
                $this->parseRequestAddressFilialInBlock( $result['blockFilialAll'][$orgInfoFilialAllKey], array('CLOSING'),  'requestAddressClose', $orgInfoFilialOne, 50 );
                //${fieldKPPFil}                - RequestOrganizationInfo.Kpp филиала
                $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldKPPFil'] = $orgInfoFilialOne->getFieldSafe('kpp');
                $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldKPPFil'] = empty( $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldKPPFil'] )?'':'КПП '.$result['blockFilialAll'][$orgInfoFilialAllKey]['fieldKPPFil'];
                $eduOrgClass = $this->requestClass->getEducationalOrganization();
                //${fieldtaxAccountDateFil}     - RequestOrganizationInfo.Kpp филиала
                //${fieldtaxAccCertNumberFil}   -
                //${fieldtaxAccCertDateFil}     - taxAccountCertificateNumber и дату taxAccountCertificateDate свидетельства выводим для филиала из табл. eiis_EducationalOrganizations
                $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldtaxAccountDateFil'] = $eduOrgClass->getDate('taxAccountDate');
                //$this->parseFieldtaxAccountCertificate( $result['blockFilialAll'][$orgInfoFilialAllKey], $eduOrgClass );
                $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldtaxAccCertNumberFil'] = $eduOrgClass->getFieldSafe('taxAccountCertificateNumber');
                if( !empty($result['blockFilialAll'][$orgInfoFilialAllKey]['fieldtaxAccCertNumberFil']) ){
                    $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldtaxAccountDateFil'] .= ',' ;
                }
                $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldtaxAccCertDateFil'] = $eduOrgClass->getDate('taxAccountCertificateDate');

                $requestOne = $orgInfoFilialOne;
                $supNum = 0;
                if(empty($options['notMakeItVuzId'])){
                    $options['notMakeItVuzId'] = null;
                }
                //${blockSuplementFil} $/{blockSuplementFil}
                $result['blockFilialAll'][$orgInfoFilialAllKey]['blockSuplementFil'] = array();
                //$result['blockFilialAll'][$orgInfoFilialAllKey]['BLOCKSUPLEMENTFIL'] = array();

                $tempArray =
                    $option =  array(
                            'numberBegin'    => $supNum+1,
                            'notMakeItVuzId' => $options['notMakeItVuzId'],
                            //'forVuzId'       => $eduOrgClass->getId()
                        );
                    $this->getSuplementTypecal(
                        $result['blockFilialAll'][$orgInfoFilialAllKey]['blockSuplementFil'],
                        array('OPENING','EDITING'),
                        $requestOne,
                        $option
                    );

                ///$supNum = count($result['blockFilialAll'][$orgInfoFilialAllKey]['blockSuplement']);
                //if(count($result['blockFilialAll'][$orgInfoFilialAllKey]['blockSuplement']) == 1){
                //    reset($result['blockFilialAll'][$orgInfoFilialAllKey]['blockSuplement']);
                //    $firstKey = key($result['blockFilialAll'][$orgInfoFilialAllKey]['blockSuplement']);
                //    $result['blockFilialAll'][$orgInfoFilialAllKey]['blockSuplement'][$firstKey]['fieldSupNum'] = '';
                //}
                //${fieldPhones}        - Телефон/факс головного вуза licenseePhone из табл. RequestOrganizationInfo
                 $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldPhones']  = $orgInfoFilialOne->getFieldSafe('licenseePhone');
                //${fieldMails}         - Почта головного вуза licenseeEmail из табл. RequestOrganizationInfo
                 $result['blockFilialAll'][$orgInfoFilialAllKey]['fieldMails']   = $orgInfoFilialOne->getFieldSafe('licenseeEmail');
            }
        }
        //${/blockFilialAll}

        //отсортировка по наименованиям филиалов
        uasort(
            $result['blockFilialAll'] ,
            function( $a , $b ){
                $aN = $a[ 'fieldfullTitleFil' ];
                $bN = $b[ 'fieldfullTitleFil' ];
                return strcmp( $aN , $bN );
            }
        );

        //${blockEduActiv}
        //  ${educationalActivitiesStoppingDate}      - LicenseRequest.educationalActivitiesStoppingDate в формате ДД.ММ.ГГГГ Если даты нет, то блок «текст+дата»  не выводим
        //$(/blockEduActiv}
            $result['blockEduActiv'][0]['fieldEduActivDate'] = $this->requestClass->getDate( 'educationalActivitiesStoppingDate' );
            if( empty( $result['blockEduActiv'][0]['fieldEduActivDate'] ) ){
                $result['blockEduActiv'] = array();
            }
        //${/blockEduActiv}

        //По LicenseRequestId из табл. LicenseRequestPayment выводим № документа NumDoc и дату Date в формате ДД.ММ.ГГГГ
        //$result['fieldNumDoc'] = '';
        //$result['fieldDate'] = '';
        //${fieldPaymentStr}
        //$result['fieldPaymentStr'] = $this->getLicenseRequestPaymentAllStr();
        //${tablePayment}         <<<<
        $this->parsePayment($result);

        //$licData = $this->getLicenceDataStr();
        //$licDataArray = $this->parseStrtoArray($licData,array(85,85));
        ////${_tabliLicDat}
        ////     ${tabliLicDat}
        //foreach ( $licDataArray as $licDataKey => $licDataOne ) {
        //    $result['_tabliLicDat'][]['tabliLicDat'] = $licDataOne;
        //}

        //${fieldNotifyStr}        - ыводим «Да» если в LicenseRequest.notificationRequired =1, и выводим «Нет» если =0
        //${fieldFilingDay}        - Текущая дата
        //${fieldFilingMount}
        //${fieldFilingYear}
        //${requestSignerPosition} - LicenseRequest.requestSignerPosition
        //${requestSignerLastName} - requestSignerLastName и инициалы requestSignerFirstName requestSigner MiddleName Табл. LicenseRequest
        $this->parsePodpisano($result);
        

        //$result[ 'blockfieldrequestAddressOpen' ]= array();

        return $result;

    }

    public function getTemplateName(){
        return 'licensing/Zayavlenie_Pereoformlenie_01.docx';
        return 'licensing/Zayavlenie_Pereoformlenie_OPEN.docx';
    }

    public function getDocumentName(){
          return 'Заявление на переоформление';
    }

    /**
     * раскидываем причины
     * ${fieldProcedureReason}
     * ${tableProcedureReason}
     *
     * @param type $result
     */
    public function parseReason(&$result){
        $nonBreakSpace = $this->unichr(160);
        //$reasonArray = $this->getRequestReasonNameArray('DocumentPrint');
        $reasonArray[] = $this->getRequestReasonString('DocumentPrint');
        $reasonFirst = array_shift($reasonArray);
        //$reasonFirst .= ';';
        $reasonFirstArray = $this->parseStrtoArray( $reasonFirst, array(65,75,75,75,75,75,75,75,75,75) );

        //${fieldProcedureReason}
        $result['fieldProcedureReason'] = array_shift($reasonFirstArray);
        //${tableProcedureReason}
        $result['_tableProcedureReason'] = array();
        foreach ( $reasonFirstArray as $reasonKey => $reasonOne ) {
            $result['_tableProcedureReason'][]['tableProcedureReason'] = $reasonOne;
        }
        if( !empty($reasonArray) ){
            foreach( $reasonArray as $reasonFirstKey => $reasonFirst ){
                $reasonFirst = $nonBreakSpace.$nonBreakSpace.$nonBreakSpace.$nonBreakSpace.$reasonFirst;
                $reasonFirst = $nonBreakSpace.$nonBreakSpace.$nonBreakSpace.$nonBreakSpace.$reasonFirst;
                $reasonFirst .= ';';
                $reasonFirstArray = $this->parseStrtoArray( $reasonFirst, array(75,75,75,75,75,75,75,75,75) );
                foreach ( $reasonFirstArray as $reasonKey => $reasonOne ) {
                    $result['_tableProcedureReason'][]['tableProcedureReason'] = $reasonOne;
                }
            }
        }
        //в последний ставим точку
        $lastVal = end($result['_tableProcedureReason']);
        $lastKey = key($result['_tableProcedureReason']);
        if( !empty($lastKey) ){
            $lastStr = $result['_tableProcedureReason'][$lastKey]['tableProcedureReason'];
            $lastChar = mb_substr($lastStr, mb_strlen($lastStr)-1);
            if( ';' ==  $lastChar ){
                //уберем последний символ
                $lastStr = mb_substr($lastStr, 0, mb_strlen($lastStr)-1 );
            }
            $lastStr .= '.';
            $result['_tableProcedureReason'][$lastKey]['tableProcedureReason'] = $lastStr;
        }
        else{

        }
    }




    /**
     * Организационно-правовая форма лицензиата
     *${fieldProperties}
     *
     * @param type $param
     */
    public function parseProperties(&$result) {
        //${fieldProperties}
        $result['fieldProperties'] = '';
        $reqOrgInfo = $this->getRequestOrganizationInfo();
        $properiesClass = $reqOrgInfo->getEducationalOrganizationProperties();
        if( !empty($properiesClass) ){
            $result['fieldProperties'] = $properiesClass->getFieldSafe('Name');
        }
    }


    /**
     * Организационно-правовая форма лицензиата на много строк
     * ${fieldProperties}
     * ${_tableProperties}
     *      ${tableProperties}
     *
     * @param type $param
     */
    public function parsePropertiesMany(&$result) {
        $this->parseProperties($result);
        //${fieldProperties}
        $result['_tableProperties'] = array();
        $value = $this->parseStrtoArray( $result['fieldProperties'] , array(35,75,75,75,75,75,75,75,75,75,75,75,75,75,) , array('notReturnOnQuotes'=>1) );
        $result['fieldProperties'] = array_shift( $value );
        foreach( $value as $one ){
            $result['_tableProperties'][]['tableProperties'] = $one;
        }
    }



    /*
     * 
     */
    public function parseRequestAddress(&$result, $requestAddressType, $FieldName){
        //${fieldrequestAddressOpen}
        $result['_'. $FieldName] = array();
        $reqOrgInfo = $this->getRequestOrganizationInfo();
        $this->setRequestAddressType( $requestAddressType );
        $adressArray = $this->getRequestAdressAllGroupByOrgLoc($reqOrgInfo,true);

        if( !empty($adressArray) ){
            foreach ( $adressArray as $key => $value ) {
                foreach ( $value as $key2 => $value2 ) {
                    $value2 = $this->fixAdressOrganisation($value2);
                    $value3 = $this->parseStrtoArray($value2 ,array(75,75,75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
                    foreach ( $value3 as $key4 => $value4 ) {
                        $result['_'. $FieldName][][ $FieldName] = $value4;
                    }
                }
            }
        }
    }

    /**
     * Выводим элемент в блоке
     * ${block[FieldName]}
     *      ${_[FieldName]}
     *          ${[FieldName]}
     * ${/block[FieldName]}
     *
     * @param type $result
     * @param type $requestAddressType
     * @param type $FieldName
     */
    public function parseRequestAddressInBlock(&$result, $requestAddressType, $FieldName){
        $this->parseRequestAddress($result, $requestAddressType, $FieldName);
        $result[ 'block' .  $FieldName ] = array();
        if( !empty( $result['_'. $FieldName] ) ){
            $result[ 'block' .  $FieldName ] = $result['_'. $FieldName];
        }
        unset( $result['_'. $FieldName] );
    }

    /**
     *
     * @param type $result
     * @param type $requestAddressType
     * @param type $FieldName
     * @param type $reqOrgInfo
     * @param type $firstLength
     */
    public function parseRequestAddressFilial(&$result, $requestAddressType, $FieldName, $reqOrgInfo, $firstLength = 45){
        //${fieldrequestAddressActive}
        //${tablerequestAddressActive}
        $result['_table'. $FieldName] = array();
        $result['field'. $FieldName] = '';
        $this->setRequestAddressType( $requestAddressType );
        $adressArray = $this->getRequestAdressAllGroupByOrgLoc($reqOrgInfo,true);

        if( !empty($adressArray) ){
            foreach ( $adressArray as $key => $value ) {
                foreach ( $value as $key2 => $value2 ) {
                    $firstLength = empty( $result['field'. $FieldName] )?$firstLength:75;
                    $value2 = $this->fixAdressOrganisation($value2);
                    $value3 = $this->parseStrtoArray($value2 ,array($firstLength,75,75,75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
                    if( empty($result['field'. $FieldName]) ){
                        $valueFirst = array_shift($value3);
                        $valueFirst = empty($valueFirst)?'':$valueFirst;
                        $result['field'. $FieldName] = $valueFirst;
                    }
                    foreach ( $value3 as $key4 => $value4 ) {
                        $result['_table'. $FieldName][][ 'table'.$FieldName] = $value4;
                    }
                }
            }
        }
    }

    /**
     * ${block[$FieldName]}
     *      ${_table[$FieldName]}
     *          ${[$FieldName]}
     * ${/block[$FieldName]}
     *
     * @param type $result
     * @param type $requestAddressType
     * @param type $FieldName
     * @param type $reqOrgInfo
     * @param type $firstLength
     */
    public function parseRequestAddressFilialInBlock(&$result, $requestAddressType, $FieldName, $reqOrgInfo, $firstLength = 45){
        $this->parseRequestAddressFilial($result, $requestAddressType, $FieldName, $reqOrgInfo, $firstLength );
        $result['block'. $FieldName] = array();
        if( !empty($result['_table'. $FieldName]) ){
            $result['block'. $FieldName] = $result['_table'. $FieldName];
        }
        unset( $result['_table'. $FieldName] );
    }


    /**
     * Данные документа, подтверждающего факт внесения сведений о юридическом лице в
     * Единый государственный реестр юридических лиц
     *
     * stateRegistrationCertificateNumber  stateRegistrationCertificateDate stateRegistrationCertificateAuthorityAddress
     *
     * №, дату и адрес выводим для головной организации из табл. eiis_EducationalOrganizations при наличии
     *
     * @param type $result
     */
    public function parseStateRegistrationCertificate(&$result) {
        //${fieldCertInfo}
        //${tableCertInfo}
        $result['fieldCertInfo'] = '';
        $result['_tableCertInfo'] = array();

        $eduOrgClass = $this->requestClass->getEducationalOrganization();
        $resultArray = array();

        $resultArray['number'] =  $eduOrgClass->getFieldSafe('stateRegistrationCertificateNumber');
        $resultArray['date']   =  $eduOrgClass->getDate('stateRegistrationCertificateDate');
        $resultArray['adres']  =  $eduOrgClass->getFieldSafe('stateRegistrationCertificateAuthorityAddress');

        $resultArray = array_filter($resultArray);
        $resultStr = implode(' ', $resultArray);

            $value3 = $this->parseStrtoArray($resultStr ,array(45,75,75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
            $resultStrFirst = array_shift($value3);
            $resultStrFirst = empty($resultStrFirst)?'':$resultStrFirst;
            $result['fieldCertInfo'] = $resultStrFirst;

            foreach ( $value3 as $key4 => $value4 ) {
                $result['_tableCertInfo'][][ 'tableCertInfo'] = $value4;
            }
    }

    /**
     * Реквизиты свидетельства о постановке на налоговый учет соискателя лицензии
     * ${fieldtaxAccountCertificateNumber} ${fieldtaxAccountCertificateDate}
     * № taxAccountCertificateNumber  и дату taxAccountCertificateDate свидетельства выводим для головной организации из табл. eiis_EducationalOrganizations при наличии
     * ${tableTaxAccount}
     *
     * @param type $result
     */
    public function parseFieldtaxAccountCertificate(&$result, $eduOrgClass) {
        //${tableTaxAccount}
        $result['_tableTaxAccount'] = array();
        //$eduOrgClass = $this->requestClass->getEducationalOrganization();
        $resultArray['number'] =  $eduOrgClass->getFieldSafe('taxAccountCertificateNumber');
        $resultArray['date']   =  $eduOrgClass->getDate('taxAccountCertificateDate');
        $resultArray = array_filter($resultArray);
        $resultStr = implode(' ', $resultArray);
        $value3 = $this->parseStrtoArray($resultStr ,array(75,75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
        foreach ( $value3 as $key4 => $value4 ) {
            $result['_tableTaxAccount'][][ 'tableTaxAccount'] = $value4;
        }
    }

    /**
     * раскидываем платежки
     * ${fieldPayment}
     * ${tablePayment}
     *
     * @param type $result
     */
    public function parsePayment(&$result) {
        $paymentArray = $this->getLicenseRequestPaymentAllStr( true );
        $firstStrLen = 45;
        $result['fieldPayment'] = '';
        $result['_tablePayment'] = array();
        foreach ( $paymentArray as $key3 => $paymentStr ) {
            $paymentStrArray = $this->parseStrtoArray( $paymentStr ,array($firstStrLen,75,75,75,75,75,75,75,75,75,75,75,75,), array('notReturnOnQuotes'=>1) );
            if( empty($result['fieldPayment']) ){
                $paymentFirst = array_shift($paymentStrArray);
                $paymentFirst = empty($paymentFirst)?'':$paymentFirst;
                $result['fieldPayment'] = $paymentFirst;
                $firstStrLen = 75;
            }
            foreach ( $paymentStrArray as $key4 => $value4 ) {
                $result['_tablePayment'][][ 'tablePayment'] = $value4;
            }
        }
    }

    //last string
    //
    //${BLOCKSUPLEMENT} ${/BLOCKSUPLEMENT}
    //${BLOCKSUPLEMENTFIL} ${/BLOCKSUPLEMENTFIL}
}
