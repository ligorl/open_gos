<?php

namespace Eo\Model\Licensing\Service;
use Eo\Model\Tools\Converter;
/**
 * Возвращает полностью подготовленные для вывода на сторону view данные
 * Инициализируеться под конкретный Ид для экономии памяти при выполнении повторных операций
 */
class OrganizationService
{
    protected $organizationId = null,
              $organizationModel = null,
              $states = null,
              $propertyForm = null,
              $types = null,
              $OrgnizationForms = null,
              $currentFounders = null,
              $allFounders = null,
              $fillialsCount = null,
              $licenses = null,
              $branch = null,
              $documents = null,
              $adminDocuments = null,
              $deals = null,
              $parentOrganization = null;
    
    /**
     * инициализируем объект id организации дабы получить все смжные данные
     * @param type $organizationId
     */
    public function __construct( $organizationId, $organizationModel = null){
        $this->organizationId = $organizationId;

        if( empty($organizationModel)){
            $this->organizationModel = new \Model\Entities\eiisEducationalOrganizations( $organizationId);
        }else{
            $this->organizationModel = $organizationModel;
        }
    }

    public function getParentIfExsist(){
        if( empty(  $this->parentOrganization)){
            $parentOrgId = $this->getOrganization()->getFieldOrSpace('fk_eiisParentEducationalOrganization');
            if(!empty($parentOrgId)){
                $this->parentOrganization  = new \Model\Entities\eiisEducationalOrganizations( $parentOrgId );
            }
        }
        return $this->parentOrganization;
    }

    public function getReorganizationNameOrSpace(){
        $orgToReorg = new \Model\LodEntities\OrganizationToReorganizedOrganization();
        $orgToReorg = $orgToReorg->getByWhere([
            'reorganizedOrganizationId' => $this->getOrganization()->getFieldOrSpace('Id')
        ]);
        
        if( !empty( $orgToReorg) && !empty($orgToReorg->getField('organizationId')) ){
            
            $org = new \Model\Entities\eiisEducationalOrganizations(
                        $orgToReorg->getField('organizationId')
                    );
            return $org->getField('RegularName').' ('.$org->getField('Inn').')';
        }
        return '';
    }
    
    public function dealsCount( $searchFilter=null){
        $conditionArray = [['column'=>'organizationId="'.$this->getId().'"']];
        
         if( !empty( $searchFilter)){

                foreach( $searchFilter as $el){
                    if( !empty( $el['value'])){
                        $conditionArray[] = ['and'];
                        $conditionArray[] = [
                            'column' => $el['name'].'="'.$el['value'].'"',
                        ];
                    }
                }
        }
        
        $deals = new \Model\LodEntities\Deal();
        return $deals->getDataCount_ArrayCondition( $conditionArray);
    }

    public function getDeals( $searchFilter=null, $order=null ,$page=1){
        $onPage = 20;
        $offset = ($page-1)*$onPage;
        $conditionArray = [['column'=>'organizationId="'.$this->getId().'"']];
        $retDeals = [];
        $newDeals = [];
        $delId = [];
        $mainLicenseRequestId = [];
        $reauestId = [];
        $expertId = [];
        $dateServ = new CalendarService();

        if( !empty( $searchFilter)){
            
                foreach( $searchFilter as $el){
                    if( !empty( $el['value'])){
                        $conditionArray[] = ['and'];
                        $conditionArray[] = [
                            'column' => $el['name'].'="'.$el['value'].'"',
                        ];
                    }
                }
        }
        
        if( !empty( $order)){
                $conditionArray[] = [
                    'order by' => $order
                ];
        }
                
        if( !empty( $page)){
                $conditionArray[] = [
                    'limit' => $onPage,
                    'offset' => $offset
                ];
        }

        $deals = new \Model\LodEntities\Deal();
        $deals = $deals->getData_ArrayCondition( $conditionArray);

        foreach( $deals as $deal){
            $currDeals[ 'id'] = $deal->getField( 'id');
            $currDeals[ 'number'] = $deal->getField( 'number');
            $currDeals[ 'registrationDate'] = Converter::date( $deal->getField( 'registrationDate'));
            $currDeals[ 'stateCode'] = $deal->getField( 'stateCode');
            $currDeals[ 'mainLicenseRequestId'] = $deal->getField( 'mainLicenseRequestId');
            $currDeals[ 'RequestId'] = $deal->getField( 'RequestId');
            $currDeals[ 'expertId'] = $deal->getField( 'expertId');

            $delId[] = '"'.$deal->getField( 'id').'"';
            $mainLicenseRequestId[] = '"'.$deal->getField( 'mainLicenseRequestId').'"';
            $reauestId[] = '"'.$deal->getField( 'RequestId').'"';
            $expertId[] = '"'.$deal->getField( 'expertId').'"';

            $newDeals[] = $currDeals;
        }

        if( !empty($newDeals)){
            $licRequest = new \Model\LodEntities\LicenseRequest();
            $licTitleAndId = $licRequest->getDataJoin_ArrayCondition([
                        'mainLicenseRequestId' => new \Zend\Db\Sql\Expression('LicenseRequest.id'),
                        'title' => new \Zend\Db\Sql\Expression('LicensingProcedure.title'),
                        'licensingProcedureId' => new \Zend\Db\Sql\Expression('LicenseRequest.licensingProcedureId'),
                    ],[
                        'table' => 'LicensingProcedure',
                        'binder' => 'LicenseRequest.licensingProcedureId = LicensingProcedure.id',
                        'type' => 'left'
                    ],[
                        [ 'column' => 'LicenseRequest.id' ],
                        [ 'in' =>  $mainLicenseRequestId ]
                    ]);
            $licTitleAndId = $licTitleAndId->toArray();
            $procedure = Converter::getTwoCollumnArrFromArray($licTitleAndId, 'mainLicenseRequestId', 'licensingProcedureId');
            $licTitleAndId = Converter::getTwoCollumnArrFromArray($licTitleAndId, 'mainLicenseRequestId', 'title');

            $licReasonIdAndDealId = $licRequest->getDataJoin_ArrayCondition([
                        'dealId' => new \Zend\Db\Sql\Expression('LicenseRequest.dealId'),
                        'licenseReasonId' => new \Zend\Db\Sql\Expression('LicenseReasonToLicenseRequest.licenseReasonId'),
                    ],[
                        'table' => 'LicenseReasonToLicenseRequest',
                        'binder' => 'LicenseRequest.id = LicenseReasonToLicenseRequest.licenseRequestId',
                        'type' => 'left'
                    ],[
                        [ 'column' => 'LicenseRequest.dealId' ],
                        [ 'in' =>  $delId ]
                    ]);

            $licReasonIdAndDealId = $licReasonIdAndDealId->toArray();

            $licenseReason = new \Model\LodEntities\LicenseReasonCatalogItem();
            $licenseReason = $licenseReason->getAll();
            $licenseReason = Converter::getTwoCollumnArrFromResultSet($licenseReason, 'id', 'title');

            $newReason = [];
            
            foreach( $licReasonIdAndDealId as $el){
                if( empty($newReason[ $el['dealId'] ])){
                   $newReason[ $el['dealId'] ] = Converter::checkSetAndGetData( $el['licenseReasonId'], $licenseReason);
                }else{
                    $newReason[ $el['dealId'] ] .=', '.Converter::checkSetAndGetData( $el['licenseReasonId'], $licenseReason);
                }
                
            }


            

            $lodUser = new \Model\LodEntities\LodUser();
            $lodUser = $lodUser->getAll();
            $lodUser = Converter::getTwoCollumnArrFromResultSet($lodUser, 'uuid', ['lastName','firstName','middleName']);

            $catalogDeal = new \Model\LodEntities\catalogdeal();
            $catalogDeal = $catalogDeal->getAll();
            $catalogDeal = Converter::getTwoCollumnArrFromResultSet($catalogDeal, 'origin_name', 'russian_name');

            foreach ( $newDeals as $deal){
                $newDeal = [];

                $dateAddPeriod = '';
                $dayInterval = 0;
                switch(  Converter::checkSetAndGetData( $deal[ 'mainLicenseRequestId'], $procedure)){
                    case 'bef70ad3504d46999db9ea296dc3b258':
                        $dayInterval = 45;
                    break;
                    case '3381eb13928c4260b59cfa85f7ff8dec':
                        $dayInterval = 30;
                    break;
                    case 'd13f3a96d6a94f1685469a6b0b61cc4c':
                        $dayInterval = 10;
                    break;
                    case '01b7aaa07f8b4faa914dadfd72162708':
                        $dayInterval = 10;
                    break;
                    case 'eb9e2f563ef24b3aa127811a27cd7009':
                        $dayInterval = 5;
                    break;
                    case '008f6531542f4dfba6180ec74f1c6c4e':
                        $dayInterval = 5;
                    break;
                    case '010cd91da7644e66bcfec667cfa6d1d0':
                        $dayInterval = 5;
                    break;
                }

                if( !empty($deal['registrationDate'] )){
                    $dateAddPeriod = $dateServ->addDays($deal['registrationDate'], $dayInterval);
                }

                $newDeal[ 'id'] = $deal[ 'id'];
                $newDeal[ 'registrationNumber'] = $deal[ 'number'];
                $newDeal[ 'registrationDate'] = $deal[ 'registrationDate'];
                $newDeal[ 'procedure'] = Converter::checkSetAndGetData( $deal[ 'mainLicenseRequestId'], $licTitleAndId);
                $newDeal[ 'state'] = Converter::checkSetAndGetData( $deal[ 'stateCode'], $catalogDeal);
                $newDeal[ 'performer'] = Converter::checkSetAndGetData( $deal[ 'expertId'], $lodUser);
                $newDeal[ 'reason'] = Converter::checkSetAndGetData( $deal[ 'id'], $newReason );
                $newDeal[ 'dateEnd'] = $dateAddPeriod;

                $retDeals[] = $newDeal ;
            }
        }
        return $retDeals;
    }

    public function countFillials(){
        $ret = '';
        
        if( $this->getOrganization()->getFieldDefault('Branch') == '0'){
            $ret = $this->getFillialsCount();
        }
        return $ret;
    }

    public function getDocument(){
       $documents =   $this->getDocumentFromOrganization();
       $retDocuments = [];

       foreach( $documents as $document){
           $deal = new \Model\LodEntities\Deal( $document->getField('dealId'));
           $newDoc = [];

           $newDoc[ 'id'] = $document->getField('id');
           $newDoc[ 'documentTypeId'] = $document->getField('documentTypeId');
           $newDoc[ 'documentReasonId'] = $document->getField('documentReasonId');
           $newDoc[ 'signerId'] = $document->getField('signerId');
           $newDoc[ 'registrationDate'] = Converter::date( $document->getField('registrationDate'));
           $newDoc[ 'registrationNumber'] = $document->getField('registrationNumber');
           $newDoc[ 'sendDate'] = Converter::date( $document->getField('sendDate'));
           $newDoc[ 'sendTypeCode'] = $document->getField('sendTypeCode');
           $newDoc[ 'receiveDate'] = Converter::date( $document->getField('receiveDate'));
           $newDoc[ 'receiveAnswerDate'] = Converter::date( $document->getField('receiveAnswerDate'));
           $newDoc[ 'returnDate'] = Converter::date( $document->getField('returnDate'));
           $newDoc[ 'printFormId'] = $document->getField('printFormId');
           $newDoc[ 'scanCopyId'] = $document->getField('scanCopyId');
           $newDoc[ 'stateCode'] = $document->getField('stateCode');
           $newDoc[ 'dealNumber'] = $deal->getField('number');
           $newDoc[ 'dealId'] = $deal->getField('id');
           
           $newDoc[ 'buttons'] = $document->getButtonsByDocument();
                      
           $retDocuments[] = $newDoc;
       }

       return $retDocuments;

    }

    public function getAdminDocument(){
       $documents =   $this->getAdministrativeDocumentFromOrganization();
       $retDocuments = [];

       foreach( $documents as $document){
           $deal = new \Model\LodEntities\Deal( $document->getField('dealId'));
           $newDoc = [];

           $newDoc[ 'id'] = $document->getField('id');
           $newDoc[ 'documentTypeId'] = $document->getField('documentTypeId');
           $newDoc[ 'documentReasonId'] = $document->getField('documentReasonId');
           $newDoc[ 'signerId'] = $document->getField('signerId');
           $newDoc[ 'registrationDate'] = Converter::date( $document->getField('registrationDate'));
           $newDoc[ 'registrationNumber'] = $document->getField('registrationNumber');
           $newDoc[ 'sendDate'] = Converter::date( $document->getField('sendDate'));
           $newDoc[ 'sendTypeCode'] = $document->getField('sendTypeCode');
           $newDoc[ 'receiveDate'] = Converter::date( $document->getField('receiveDate'));
           $newDoc[ 'receiveAnswerDate'] = Converter::date( $document->getField('receiveAnswerDate'));
           $newDoc[ 'returnDate'] = Converter::date( $document->getField('returnDate'));
           $newDoc[ 'printFormId'] = $document->getField('printFormId');
           $newDoc[ 'scanCopyId'] = $document->getField('scanCopyId');
           $newDoc[ 'stateCode'] = $document->getField('stateCode');
           $newDoc[ 'dealNumber'] = $deal->getField('number');
           $newDoc[ 'dealId'] = $deal->getField('id');

           $retDocuments[] = $newDoc;
       }

       return $retDocuments;

    }
    
    public function getBranchs( $status = false, $orStatus = false ){
        $ret = '';

        if( $this->getOrganization()->getFieldDefault('Branch') == '0'){
            $ret = $this->getAllBranch( $status,$orStatus);
        }
        return $ret;
    }

    /**
     * Вернуть Id организации
     * @return type
     */
    public function getId(){
        return $this->organizationId;
    }

    /**
     * вернуть запись из базы в виде объекта
     * @return \Model\Entities\eiisLicenses
     */
    public function getOrganization(){
        return $this->organizationModel;
    }

    /**
     * Получить все возможные статусы организации в формате [ id_1 => value_1, id_2 =>...]
     * @return type
     */
    public function getOrganizationStates(){
        return Converter::getTwoCollumnArrFromResultSet(
                            $this->getEiisOrganizationStates(),
                            'russian_name',
                            'origin_name'
                          );
    }

    /**
     * Получить учередителей  в формате [ id_1 => fio_1, id_2 =>...]
     * @return string
     */
     public function getFounders(){
        $retArr = [];
        $foundersOrgznizationLinks = $this->getCurrentFounders();
        
        foreach( $foundersOrgznizationLinks as $founOrgs ){
            $foundersModel = new \Model\Entities\isgaFounders( $founOrgs->getField('fk_isgaFounder'));
            $firstName = $foundersModel->getFieldOrSpace('FirstName');
            $lastName = $foundersModel->getFieldOrSpace('LastName');
            $patronymic  = $foundersModel->getFieldOrSpace('Patronymic');
            $retArr[ $founOrgs->getField('Id')] = $lastName.' '.$firstName.' '.$patronymic.' '.$foundersModel->getFieldOrSpace('OrganizationFullName');
        }
        return $retArr;
    }

    /**
     * Получить все формы собственности в формате [ id_1 => title_1, id_2 =>...]
     * @return type
     */
    public function getPropertyForm(){
        $arr =  Converter::getTwoCollumnArrFromResultSet(
                            $this->getPropertyFormOkfs(),
                            'id',
                            'title'
                          );
        foreach( $arr as $key=>$value ){
            
            $value = json_decode($value);
            if(!empty( $value)){
              $arr[ $key] = $value->RU;
            }
            
        }
        return $arr;
    }

    /**
     * Получить все типы организации  в формате [ id_1 => name_1, id_2 =>...]
     * @return type
     */
    public function getTypes(){
         return  Converter::getTwoCollumnArrFromArray(
                            $this->getAllTypeObj()->toArray(),
                            'Id',
                            'Name',
                            function( $fieldData){
                                if( (int)$fieldData['EOSubTypeCode']%100 !== 0){
                                    $fieldData['Name'] = '&#160; &#160; &#160;'.$fieldData['Name'];
                                }
                                return $fieldData;
                            }
                          );
    }

    /**
     * Получить вс праовые формы  в формате [ id_1 => name_1, id_2 =>...]
     * @return type
     */
    public function getOrganizationProperty(){
         return  Converter::getTwoCollumnArrFromArray(
                            $this->getAllOrgnizationFormObj()->toArray(),
                            'Id',
                            'Name'
                          );
    }

    /**
     * Получить всех учередителей 
     * @return type
     */
    public function getAllFounders( $limit = null, $offset = null){
        return $this->getAllAvaivableFounders( $limit, $offset);
    }

    public function countBranch(){
            $org = new \Model\Entities\eiisEducationalOrganizations();
            return $org->getDataCount_ArrayCondition([
                                       [ 'column' => 'fk_eiisParentEducationalOrganization ="'.$this->getId().'"']
                                   ]);
    }

    /**
     * получить все статусы
     * @return type
     */
    protected function getEiisOrganizationStates(){
         if( empty(  $this->states)){
            $catalogOrgStat = new \Model\LodEntities\catalogorganization();
            $this->states = $catalogOrgStat->getAll();
            return $this->states;
        }else{
            return $this->states;
        }
    }

     protected function getPropertyFormOkfs(){
         if( empty(  $this->propertyForm)){
            $catalogOrgStat = new \Model\LodEntities\CatalogItem();
            $this->propertyForm = $catalogOrgStat->getAllByWhere([
                'catalog' => 'okfs'
            ]);
            return $this->propertyForm;
        }else{
            return $this->propertyForm;
        }
    }

    protected function getAllTypeObj(){
        if( empty(  $this->types)){
             $typeModel = new \Model\Entities\eiisEducationalOrganizationTypes();
             $this->types = $typeModel->getData_ArrayCondition([
                 [ 'column' => 'EOSubTypeCode IS NOT NULL'],
                 [ 'order by' => 'EOSubTypeCode ASC' ],
             ]);
        }
        return $this->types;
        
    }

    protected function getAllOrgnizationFormObj(){
        if( empty(  $this->OrgnizationForms)){
             $typeModel = new \Model\Entities\eiisEducationalOrganizationProperties();
             $this->OrgnizationForms = $typeModel->getAll();
            return $this->OrgnizationForms;
        }else{
            return $this->OrgnizationForms;
        }
    }

    protected function getCurrentFounders(){
        if( empty(  $this->currentFounders)){
            $typeModel = new \Model\Entities\isgamtmFoundersEducationalOrganizations();
            $this->currentFounders = $typeModel->getAllByWhere([
                                       'fk_eiisEducationalOrganization' => $this->getOrganization()->getField('Id')
                                   ]);
            return $this->currentFounders;
        }else{
            return $this->currentFounders;
        }
    }

    protected function getFillialsCount(){
        if( empty(  $this->fillialsCount)){
            $typeModel = new \Model\Entities\eiisEducationalOrganizations();
            $this->fillialsCount = $typeModel->getDataCount_ArrayCondition([
                                        [ 'column' => 'fk_eiisParentEducationalOrganization ="'.$this->getId().'"'],
                                        ['and'],
                                        ['('=>''],
                                        [ 'column' => 'lod_stateCode = "VALID"'],
                                        ['or'],
                                        [ 'column' => 'lod_stateCode = "DRAFT"'],
                                        [')'=>'']
                                   ]);
            return $this->fillialsCount;
        }else{
            return $this->fillialsCount;
        }
    }
    
    protected function getAllBranch( $status = false, $orStatus = false){
        if( empty(  $this->branch)){
            $typeModel = new \Model\Entities\eiisEducationalOrganizations();
            $condition = [
                [ 'column' => 'fk_eiisParentEducationalOrganization ="'.$this->getId().'"']
            ];

            if( $status && $orStatus){
                $condition[] = [
                    'and',
                    '(' => '',
                    'column' => 'lod_stateCode ="'.$status.'"',
                    'or' => 'lod_stateCode ="'.$orStatus.'"',
                    ')' => ''
                ];
            }else{
                if($status){
                    $condition[] = [
                        'and',
                        'column' => 'lod_stateCode ="'.$status.'"'
                    ];
                }
            }
            
            $condition[] = [
                    'order by' => 'lod_stateCode DESC, FullName ASC'
                ];
            $this->branch = $typeModel->getData_ArrayCondition( $condition);
            return $this->branch;
        }else{
            return $this->branch;
        }
    }

    protected function getAllAvaivableFounders( $limit = null, $offset = null){
        if( empty(  $this->allFounders)){
            $currentFounders = $this->getCurrentFounders();
            $notInArr = [];
            $predicateArr = [['column' => 'isga_Founders.Id IS NOT NULL']];

            foreach( $currentFounders as $row){
                $notInArr[]  = '"'.trim( $row->getField( 'fk_isgaFounder'), ' ').'"';
            }

            if( !empty($notInArr)){
                $predicateArr = [
                    ['column' => 'isga_Founders.Id'],
                    ['not'],
                    ['in' => $notInArr]
                ];
            }

            if( !empty($limit)){
                $predicateArr[]['limit'] = $limit;
            }

            if( !empty($offset)){
                $predicateArr[]['offset'] = $offset;
            }
            
            $founders = new \Model\Entities\isgaFounders();
            
            $this->allFounders = $founders->getData_ArrayCondition(
              $predicateArr
            );
            return $this->allFounders;
        }else{
            return $this->allFounders;
        }
    }

    public function getLicenses(){
        if( empty(  $this->licenses)){
            $licModel = new \Model\Entities\eiisLicenses();
            $this->licenses = $licModel->getData_ArrayCondition([
                [ 'column' => 'fk_eiisEducationalOrganization="'.$this->getId().'"'],
                [ 'order by' => 'DateLicDoc DESC'  ]
            ]);
        }
        return $this->licenses;
    }

    public function getLocationFromOrganizationAndSupplement( $supplementId){
        $orgLocationModel = new \Model\LodEntities\OrganizationLocation();
        
        return $orgLocationModel->getLocationFromSupplement( $supplementId);
    }

    protected function getDocumentFromOrganization(){
        if( empty(  $this->documents)){
            $doc = new \Model\LodEntities\document();
            $this->documents = $doc->getDocumentAndDealFromOrgId( $this->getId());
        }
        return $this->documents;
    }

    protected function getAdministrativeDocumentFromOrganization(){
        if( empty(  $this->adminDocuments)){
            $doc = new \Model\LodEntities\document();
            $this->adminDocuments = $doc->getAdministrativeDocumentAndDealFromOrgId( $this->getId());
        }
        return $this->adminDocuments;
    }

     protected function getDealsFromOrganization(){
        if( empty(  $this->deals)){
            $doc = new \Model\LodEntities\Deal();
            $this->deals = $doc->getAllByWhere([ 'organizationId' => $this->getId() ]);
        }
        return $this->deals;
    }
}