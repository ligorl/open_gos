<?php

namespace Eo\Model\Licensing\Service;
use Eo\Model\Licensing\LicenseSupplementModel;
use Eo\Model\Licensing\Tools\Converter;
/**
 * Возвращает полностью подготовленные для вывода на сторону view данные
 * Инициализируеться под конкретный Ид для экономии памяти при выполнении повторных операций
 */
class LicenseSupplementInfoService
{
    protected $supplementId = null,
              $supplementModel = null,
              $document = null,
              $state = null,
              $allStates = null,
              $license = null,
              $govermentOrganizaion = null,
              $signatories = null,
              $documentById = null,
              $endDocumentById = null,
              $otherDocumentById = null;
    

    public function __construct( $supplmentId, $supplement = null){
        $this->supplementId = $supplmentId;
        
        if( empty($supplement)){
            $this->supplementModel = LicenseSupplementModel::getSupplement( $supplmentId);
        }else{
            $this->supplementModel = $supplement;
        }        
    }

    public function getEducationOrganization(){
        return $this->getEducationOrgBySupplement();
    }
    public function getSignatories(){
        $personArr = [];
        $signatories = $this->getSignatoriesBySupplement();
         
            foreach( $signatories as $person){
                $employeepost = new \Model\LodEntities\employeepost( $person->getField('employeePostId') );
                $emPost = '';
                $emPostTitle = $employeepost->getFieldOrSpace('title');
                if( !empty( $emPostTitle)){
                    $emPost = ', '.$emPostTitle;
                }
                $fio = $person->getFieldOrSpace('lastName').' '.$person->getFieldOrSpace('firstName').' '.$person->getFieldOrSpace('middleName');
                $fioAndPost = $fio.$emPost;
                $personArr[ $person->getFieldOrSpace('id')] = $fioAndPost;
            }
        return $personArr;
    }

    public function getAllStates(){
        return Converter::getTwoCollumnArrFromResultSet(
                            $this->getStates(),
                            'Id',
                            'WebName'
                          );
    }
    public function getId(){
        return $this->supplementId;
    }

    public function getGovermentOrg(){
        return Converter::getTwoCollumnArrFromResultSet(
                                    $this->getGovermentOrganizationModel()->getAllByWhere(null),
                                    'Id',
                                    'Name'
                            );
    }
    
    public function getLicense(){
        return $this->getLicenseFromSupplement();
    }

    public function getSupplement(){
        return $this->supplementModel;
    }

    public function getTitle(){
        $supp = $this->getSupplement();
        $license = new \Model\Entities\eiisLicenses($supp->getField('fk_eiisLicense') );
        $date = $license->getDate('DateLicDoc');
        $license = $license->getField('LicenseRegNum');
        
        if( !empty($date)){
            $date = ' от '.$date.'г.';
        }
        return 'Приложение '.$supp->getField('Number').' к лицензии №'.$license.$date;
        
    }

    public function getTitleToInventory(){
        $supp = $this->getSupplement();
        $license = new \Model\Entities\eiisLicenses($supp->getField('fk_eiisLicense') );
        $date = $license->getDate('DateLicDoc');
        $license = $license->getField('LicenseRegNum');

        if( !empty($date)){
            $date = ' от '.$date.'г.';
        }
        return 'Приложение №'.$supp->getField('Number').' к лицензии'.$date.', № '.$license;

    }

    public function getBasisForLicensing(){
        $retStr = '';
        $docType = '';
        $docRegDate = '';
        $docRegNumber = '';
        
        if( !empty( $this->getSupplement()->getField('lod_basisForLicensing'))){
            $document = $this->getDocument();
            $docTypeId = $document->getFieldOrSpace('documentType');

            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $docTypeId);

            if($catalogItem){
                $docType = $catalogItem->getField('title');
            }

            if( !empty( $document->getFieldOrSpace('registrationDate'))){
                $docRegDate = new \DateTime( $document->getFieldOrSpace('registrationDate'));
                $docRegDate = $docRegDate->format('d.m.Y');
            }
            $docRegNumber = $document->getFieldOrSpace('registrationNumber');
        }else{
            $docRegNumber = $this->getSupplement()->getField('NumLicDoc');
            $docRegDate  = $this->getSupplement()->getField('DateLicDoc');

            $docTypeId = $this->getSupplement()->getField('fk_eiisBaseDocType');

            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $docTypeId);

            if($catalogItem){
                $docType = $catalogItem->getField('title');
            }
        }

        switch( true){
            case !empty($docRegDate):
                $docRegDate = ' от '.$docRegDate.'г.';
            case !empty($docRegNumber):
                $docRegNumber = ' №'.$docRegNumber;
        }
        return  $docType.$docRegDate.$docRegNumber;
    }

    public function getOtherDocuments(){
        $documents = $this->getOtherDocumentById();
        $documentArr = [];
        foreach( $documents as $document){
            $docType = '';
            $docRegDate = '';
            $docRegNumber = '';
            $scanCopy = '';

            if( $document){
                $document = $document->toArray();
                $docTypeId = $document['documentTypeId'];
                $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $docTypeId);
                $docType = $catalogItem->getField('title');
                $docRegDate = $document['registrationDate'];
                $docRegNumber = $document['registrationNumber'];
                $thandDocument = new \Model\LodEntities\document(
                    $document['id']
                );
                $eoFile = $thandDocument->getFileScanCopy();
                if ($eoFile) {
                    $url = $eoFile->getUrl();

                    if( $url){
                        $scanCopy = ' <a href="'.$url.'" > <img src="/img/islod/file_expand.png" /></a> ';
                    }
                }
            }else{
                $license = $this->getLicense();
                $docRegDate = $license->getFieldOrSpace('DateLicDoc');
                $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
                
                $docTypeId = $license->getFieldOrSpace('fk_eiisBaseDocTypes');
                $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $docTypeId);
                
                if($catalogItem){
                    $docType = $catalogItem->getField('title');
                }
            }


            switch( true){
                case !empty($docRegDate):
                    $docRegDate = new \DateTime( $docRegDate );
                    $docRegDate = $docRegDate->format('d.m.Y');
                    $docRegDate = ' от '.$docRegDate.'г.';
                case !empty($docRegNumber):
                    $docRegNumber = ' №'.$docRegNumber;
                default: break;
            }
            $documentArr[] = $docType.$docRegDate.$docRegNumber.$scanCopy;
        }

        return $documentArr;
    }

    public function getEndDocument(){
        $document = $this->getEndDocumentById();
        $docType = '';
        $docRegDate = '';
        $docRegNumber = '';

        if( $document){
            $document = $document->toArray();
            $docTypeId = $document['documentTypeId'];
            $docRegDate = $document['registrationDate'];
            $docRegNumber = $document['registrationNumber'];

            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $docTypeId);

            if($catalogItem){
                $docType = $catalogItem->getField('title');
            }
        }


        switch( true){
            case !empty($docRegDate):
                $docRegDate = new \DateTime( $docRegDate );
                $docRegDate = $docRegDate->format('d.m.Y');
                $docRegDate = ' от '.$docRegDate.'г.';
            case !empty($docRegNumber):
                $docRegNumber = ' №'.$docRegNumber;
            default: break;
        }

        return $docType.$docRegDate.$docRegNumber;
    }

    public function getAdministrativeDocument( ){
        $document = $this->getDocumentById();
        $docType = '';
        $docRegDate = '';
        $docRegNumber = '';

        if( $document){
            $document = $document->toArray();
            $docTypeId = $document['documentTypeId'];
            $docRegDate = $document['registrationDate'];
            $docRegNumber = $document['registrationNumber'];



            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $docTypeId);

            if($catalogItem){
                $docType = $catalogItem->getField('title');
            }
        }else{
            $license = $this->getLicense();
            $docRegDate = $license->getFieldOrSpace('DateLicDoc');
            $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $license->getFieldOrSpace('fk_eiisBaseDocTypes'));

            if($catalogItem){
                $docType = $catalogItem->getField('title');
            }
        }


        switch( true){
            case !empty($docRegDate):
                $docRegDate = new \DateTime( $docRegDate );
                $docRegDate = $docRegDate->format('d.m.Y');
                $docRegDate = ' от '.$docRegDate.'г.';
            case !empty($docRegNumber):
                $docRegNumber = ' №'.$docRegNumber;
            default: break;
        }

        return $docType.$docRegDate.$docRegNumber;
    }

    public function getAdministrativeDocumentToInventory( ){
        $document = $this->getDocumentById();
        $docRegDate = '';
        $docRegNumber = '';

        if( $document){
            $document = $document->toArray();
            $docTypeId = $document['documentTypeId'];
            $docRegDate = $document['registrationDate'];
            $docRegNumber = $document['registrationNumber'];


        }else{
            $license = $this->getLicense();
            $docRegDate = $license->getFieldOrSpace('DateLicDoc');
            $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
         
        }


        switch( true){
            case !empty($docRegDate):
                $docRegDate = new \DateTime( $docRegDate );
                $docRegDate = $docRegDate->format('d.m.Y');
                $docRegDate = ' от '.$docRegDate.'г.';
            case !empty($docRegNumber):
                $docRegNumber = ' №'.$docRegNumber;
            default: break;
        }

        return $docRegDate.$docRegNumber;
    }

    public static function supplementsObjectArrInfoRowPrepare( $supplements){
        $retArr = [];
        foreach ($supplements as $sup) {
            $licenseSupplement = new LicenseSupplementInfoService( $sup->getField('Id'), $sup );
            
            $dealNumber = '';
            if( $licenseSupplement->getDocumentById()){
                $doc = $licenseSupplement->getDocumentById();
                if(!empty($doc->getFieldOrSpace('dealId'))){
                    $deal = new \Model\LodEntities\Deal( $doc->getFieldOrSpace('dealId') );
                    $dealNumber = $deal->getFieldOrSpace('number');
                }
            }
            
            $sup->setField('basisForLicensing', $licenseSupplement->getAdministrativeDocument());
            $sup->setField('stateCode', $licenseSupplement->getState()->getField('Name'));
            $sup->setField('dealNumber', $dealNumber);
            $retArr[] = $sup;
        }
        return $retArr;
    }

     public function getWebStatusFromCode( $code){
        $status = new \Model\Entities\eiisLicenseStates();
        $status = $status->getByWhere([
            'LodCode' => $code
        ]);
        return $status->getField('WebName');
    }

    //Возвращает данные о web статусе
    public function getStateStatus(){
        $stateCode = '';

        if( !empty( $this->getSupplement()->getField('lod_stateCode'))){
            $stateCode = $this->getWebStatusFromCode( 
                                        $this->getSupplement()->getField('lod_stateCode')
                            );
        }
        return $stateCode;
    }

    protected function getDocument(){
        if( empty(  $this->document)){
            $this->document = new \Model\LodEntities\document(
                            $this->getSupplement()->getField('lod_basisForLicensing')
                        );
        }
        return $this->document;
    }


    protected function getState(){
        if( empty(  $this->state)){
            $this->state = new \Model\Entities\eiisLicenseSupplementStates(
                            $this->getSupplement()->getField('fk_eiisLicenseSupplementState')
                        );
        }
        return $this->state;
    }

    protected function getStates(){
        if( empty(  $this->allStates)){
//            $StatesClass = new \Model\LodEntities\catalogeiislicenses();
            $supplementStatus = new \Model\Entities\eiisLicenseSupplementStates;
            $this->allStates = $supplementStatus->getAllByWhere(null);
        }
        return $this->allStates;
    }

    protected function getLicenseFromSupplement(){
        if( empty(  $this->license)){
            $this->license = new \Model\Entities\eiisLicenses(
                            $this->getSupplement()->getField('fk_eiisLicense')
                         );
        }
        return $this->license;
    }

    protected function getGovermentOrganizationModel(){
         if( empty(  $this->govermentOrganizaion)){
            $this->govermentOrganizaion = new \Model\Entities\eiisControlOrgans();
        }
        return $this->govermentOrganizaion;
    }

    protected function getSignatoriesBySupplement(){
         if( empty(  $this->signatories)){
            $signatories = new \Model\LodEntities\signatories();
            $this->signatories =  $signatories->getAllByWhere([
                                                'signatureLevelCode' => '2'
                                            ]);
        }
        return $this->signatories;
    }

    protected function getEducationOrgBySupplement(){
         if( empty( $this->educationOrganization) ){
            $this->educationOrganization = new \Model\Entities\eiisEducationalOrganizations(
                                    $this->getSupplement()->getField('fk_eiisBranch') 
                                );
        }
        return $this->educationOrganization;
    }

    public function getDocumentById(){
        if( empty(  $this->documentById)){
            $this->documentById = $this->getSupplement()->getDocumentFromId( $this->getId() );
        }
        return $this->documentById;
    }


    public function getEndDocumentById(){
        if( empty(  $this->endDocumentById)){
            $this->endDocumentById = $this->getSupplement()->getEndDocumentFromId( $this->getId() );
        }
        return $this->endDocumentById;
    }

    protected function getOtherDocumentById(){
        if( empty(  $this->otherDocumentById)){
            $this->otherDocumentById = $this->getSupplement()->getOtherDocumentFromId( $this->getId() );
        }
        return $this->otherDocumentById;
    }

    public function getDeal(){
        $document = $this->getDocumentById();
        $deal = false;
        
        if ($document && !empty($document->getFieldOrSpace('dealId'))) {
            $deal = new \Model\LodEntities\Deal(
              $document->getFieldOrSpace('dealId')
            );
        }
        return $deal;
    }

    public function getDealFromEndDoc(){
        $document = $this->getEndDocumentById();
        $deal = false;
        
        if ($document && !empty($document->getFieldOrSpace('dealId'))) {
            $deal = new \Model\LodEntities\Deal(
              $document->getFieldOrSpace('dealId')
            );
        }
        return $deal;
    }
}