<?php

namespace Eo\Model\Licensing\Service;

use Eo\Model\Licensing\LicenseModel;
use Eo\Model\Licensing\Tools\Converter;

/**
 * Возвращает полностью подготовленные для вывода на сторону view данные
 * Инициализируеться под конкретный Ид для экономии памяти при выполнении повторных операций
 */
class LicenseInfoService
{

    protected $licenseId = null,
      $licenseModel = null,
      $supplements = null,
      $states = null,
      $controlOrgans = null,
      $signStates = null,
      $document = null,
      $documentType = null,
      $deal = null,
      $educationalOrganization = null,
      $documentById = null,
      $endDocumentById = null,
      $otherDocumentById = null;

    public function __construct($licenseId, $licenseModel = null)
    {
        $this->licenseId = $licenseId;
        if (empty($licenseModel)) {
            $this->licenseModel = LicenseModel::getLicense($licenseId);
        } else {
            $this->licenseModel = $licenseModel;
        }
    }

    public function getId()
    {
        return $this->licenseId;
    }

    /**
     * возвращает запись из базы в виде объекта
     * @return \Model\Entities\eiisLicenses
     */
    public function getLicense()
    {
        return $this->licenseModel;
    }

    public function getOtherDocuments()
    {
        $documents = $this->getOtherDocumentById();
        $documentArr = [];
        foreach ($documents as $document) {
            $docType = '';
            $docRegDate = '';
            $docRegNumber = '';
            $link = '';

            if ($document) {
                $document = $document->toArray();
                $docTypeId = $document['documentTypeId'];
                $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem($docTypeId);
                $docType = $catalogItem->getField('title');
                $docRegDate = $document['registrationDate'];
                $docRegNumber = $document['registrationNumber'];

                $lodDocument = new \Model\LodEntities\document( $document['documentId'] );
                $eoFile = $lodDocument->getFileScanCopy();
                if($eoFile){
                    $link = ' <a href="'.$eoFile->getUrl().'" download="'.$eoFile->getFileName().'" target="_blank"><img src="/img/islod/file_expand.png" /> </a> ';
                }
                

            } else {
                $license = $this->getLicense();
                $docType = $license->getFieldOrSpace('fk_eiisBaseDocTypes');
                $docRegDate = $license->getFieldOrSpace('DateLicDoc');
                $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
            }

            switch (true) {
                case!empty($docRegDate):
                    $docRegDate = Converter::date($docRegDate);
                    $docRegDate = ' от ' . $docRegDate . 'г.';
                case!empty($docRegNumber):
                    $docRegNumber = ' №' . $docRegNumber;
                default: break;
            }
            $documentArr[] = $docType . $docRegDate . $docRegNumber . $link;
        }

        return $documentArr;
    }

    public function getEndDocument()
    {
        $document = $this->getEndDocumentById();
        $docType = '';
        $docRegDate = '';
        $docRegNumber = '';

        if ($document) {
            $document = $document->toArray();
            $docTypeId = $document['documentTypeId'];
            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem($docTypeId);
            $docType = $catalogItem->getField('title');
            $docRegDate = Converter::date($document['registrationDate']);
            $docRegNumber = $document['registrationNumber'];            
        }

        if( !empty($docRegDate)){
            $docRegDate = Converter::date($docRegDate);
            $docRegDate = ' от ' . $docRegDate . 'г.';
        }
        if( !empty($docRegNumber)){
            $docRegNumber = ' №' . $docRegNumber;
        }

        return $docType . $docRegDate . $docRegNumber;
    }

    public function getDocumentOnDelivery()
    {
        $document = $this->getDocumentById();
        $docType = '';
        $docRegDate = '';
        $docRegNumber = '';

        if ($document) {
            $document = $document->toArray();
            $docTypeId = $document['documentTypeId'];
            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem($docTypeId);
            $docType = $catalogItem->getFieldOrSpace('title');
            $docRegDate = Converter::date($document['registrationDate'], '');
            $docRegNumber = $document['registrationNumber'];
        } else {
            $license = $this->getLicense();
            $docTypeId = $license->getFieldOrSpace('fk_eiisBaseDocTypes');
            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem( $docTypeId );
            $docType = 'Неизвестный тип документа( '.$docTypeId.' )';
            
            if( $catalogItem){
               $docType = $catalogItem->getFieldOrSpace('title');
            }
            $docRegDate = Converter::date($license->getFieldOrSpace('DateLicDoc'), '');
            $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
        }




        switch (true) {
            case!empty($docRegDate):
                $docRegDate = ' от ' . $docRegDate . 'г.';
            case!empty($docRegNumber):
                $docRegNumber = ' №' . $docRegNumber;
            default: break;
        }

        return $docType . $docRegDate . $docRegNumber;
    }

     public function getDocumentOnDeliveryToInventory()
    {
        $document = $this->getDocumentById();
        $docRegDate = '';
        $docRegNumber = '';

        if ($document) {
            $document = $document->toArray();
            $docRegDate = Converter::date($document['registrationDate'], '');
            $docRegNumber = $document['registrationNumber'];
        } else {
            $license = $this->getLicense();

            
            $docRegDate = Converter::date($license->getFieldOrSpace('DateLicDoc'), '');
            $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
        }




        switch (true) {
            case!empty($docRegDate):
                $docRegDate = ' от ' . $docRegDate . 'г.';
            case!empty($docRegNumber):
                $docRegNumber = ' №' . $docRegNumber;
            default: break;
        }

        return $docRegDate . $docRegNumber;
    }

    public function getDealNumber()
    {
        $document = $this->getDocumentById();
        $number = '';

        if ($document && !empty($document->getFieldOrSpace('dealId'))) {
            $deal = new \Model\LodEntities\Deal(
              $document->getFieldOrSpace('dealId')
            );
            $number = $deal->getFieldOrSpace('number');
        }
        return $number;
    }

    public function getDealFromDoc()
    {
        $document = $this->getDocumentById();
        $deal = false;

        if ($document && !empty($document->getFieldOrSpace('dealId'))) {
            $deal = new \Model\LodEntities\Deal(
              $document->getFieldOrSpace('dealId')
            );
        }
        return $deal;
    }
    
    public function getDealFromEndDoc()
    {
        $document = $this->getEndDocumentById();
        $deal = false;

        if ($document && !empty($document->getFieldOrSpace('dealId'))) {
            $deal = new \Model\LodEntities\Deal(
              $document->getFieldOrSpace('dealId')
            );
        }
        return $deal;
    }

    public function getCurrentStatus()
    {
        $states = $this->getStatesFromModel()->toArray();
        $lodCode = $this->getLicense()->getField('lod_stateCode');

        foreach ($states as $state) {
            if ($state['LodCode'] == $lodCode) {
                return $state['Name'];
            }
        }
        return '';
    }

    public function getLicenseGranted()
    {
        $EoNameDatCase = $this->getLicense()->getFieldOrSpace('EONameDatCase');

        if (empty($EoNameDatCase)) {
            $orgId = $this->getLicense()->getField('fk_eiisEducationalOrganization');

            $isgaOrgData = new \Model\Entities\isgaEducationalOrganizationData();
            $isgaOrgData = $isgaOrgData->getByWhere(["fk_eiisEducationalOrganization" => $orgId]);

            if ($isgaOrgData) {
                $EoNameDatCase = $isgaOrgData->getField('NameDatCase');
            }

            if (empty($EoNameDatCase)) {
                $organization = new \Model\Entities\eiisEducationalOrganizations($orgId);
                if (!empty($organization->getFieldOrSpace('FullName'))) {
                    $orgName = $organization->getFieldOrSpace('FullName');
                    $morphy = new \Islod\Model\Tools\Morphy();
                    $morphy = $morphy->getForms($orgName);
                    $EoNameDatCase = $morphy['dative'];
                }
            }
        }
        return $EoNameDatCase;
    }

    public function getSupplements()
    {
        return $this->getSupplementFromCurrentLicense();
    }

    public function getStates()
    {
        return Converter::getTwoCollumnArrFromResultSet(
            $this->getStatesFromModel(), 'Id', 'WebName'
        );
    }

    public function getControlOrgans()
    {
        return Converter::getTwoCollumnArrFromResultSet(
            $this->getAllControllOrgans(), 'Id', 'Name'
        );
    }

    public function getAuthorizedPerson()
    {
        $signatoriesArrayObj = $this->getAllSignatories();
        return $this->converSignatoriesToArrIdFio($signatoriesArrayObj);
    }

    public function getNotValidLicenses()
    {
        return LicenseModel::getNotValidLicenseFromOrgaznizationId($this->getId());
    }

    public function getSignStates()
    {
        return Converter::getTwoCollumnArrFromResultSet(
            $this->getAllSignStates(), 'origin_name', 'russian_name'
        );
    }

    protected function getStatesFromModel()
    {
//        $StatesClass = new \Model\LodEntities\catalogeiislicenses();
//        $states = $StatesClass->getAll();
        if (empty($this->states)) {
            $this->states = LicenseModel::getStates();
//            $this->states = $states;
            return $this->states;
        } else {
            return $this->states;
        }
    }

    protected function getSupplementFromCurrentLicense()
    {
        $Sort = new \Zend\Db\Sql\Expression("INET_ATON(SUBSTRING_INDEX(CONCAT(f11_mon.eiis_LicenseSupplements.Number,'.0.0.0'),'.',4))  ASC");
        if (empty($this->supplements)) {
            $this->supplements = LicenseModel::getSupplementsFromLicenseId($this->getId(), $Sort);
            return $this->supplements;
        } else {
            return $this->supplements;
        }
    }

    protected function getAllControllOrgans()
    {
        if (empty($this->controlOrgans)) {
            $controlOrgans = new \Model\Gateway\EntitiesTable("eiis_ControlOrgans");
            $this->controlOrgans = $controlOrgans->getAllEntities();
            return $this->controlOrgans;
        } else {
            return $this->controlOrgans;
        }
    }

    public function getTitle()
    {
        $license = $this->getLicense();
        
        $regNumber = $license->getFieldOrSpace('LicenseRegNum');
        $Inn = $license->getFieldOrSpace('EOInn');
        $date = Converter::date($license->getFieldOrSpace('DateLicDoc'),'');
        if( !empty($Inn)){
            $Inn = '('.$Inn.')';
        }
        if( !empty($date)){
            $date = ' от ' . $date . ' ';
        }
        return 'Лицензия № ' . $regNumber . $date . $Inn ;
    }

    public function getTitleToInventory()
    {
        $license = $this->getLicense();

        $regNumber = $license->getFieldOrSpace('LicenseRegNum');
        $serDoc = $license->getFieldOrSpace('SerDoc');
        $numDoc = $license->getFieldOrSpace('NumDoc');
        $date = Converter::date($license->getFieldOrSpace('DateLicDoc'),'');
        if( !empty($serDoc)){
            $serDoc = '('.$serDoc;
            if(empty($numDoc)){
                  $serDoc .= ')';
            }else{
               $serDoc .= ' '.$numDoc.')';
            }
        }
        if( !empty($date)){
            $date = ' от ' . $date . ' ';
        }
        return 'Лицензия'.$date.'№ '.$regNumber.' '.$serDoc;
    }

    public function getDocumentById()
    {
        if (empty($this->documentById)) {
            $this->documentById = $this->getLicense()->getDocumentFromId($this->getId());
        }
        return $this->documentById;
    }

    public function getEndDocumentById()
    {
        if (empty($this->endDocumentById)) {
            $this->endDocumentById = $this->getLicense()->getEndDocumentFromId($this->getId());
        }
        return $this->endDocumentById;
    }

    public function getOtherDocumentById()
    {
        if (empty($this->otherDocumentById)) {
            $this->otherDocumentById = $this->getLicense()->getOtherDocumentFromId($this->getId());
        }
        return $this->otherDocumentById;
    }

    protected function getAllSignatories()
    {
        if (empty($this->signatories)) {
            $signatories = new \Model\LodEntities\signatories();
            $signatories = $signatories->getAllByWhere([
                  'signatureLevelCode' => '2'
              ])->getObjectsArray();
            $this->signatories = $signatories;
            return $this->signatories;
        } else {
            return $this->signatories;
        }
    }

    protected function converSignatoriesToArrIdFio($signatoriesArrayObj)
    {
        $personArr = [];
        foreach ($signatoriesArrayObj as $person) {
            $firstName = $person->getField('firstName');
            $middleName = $person->getField('middleName');
            $lastName = $person->getField('lastName');

            $Id = $person->getField('id');
            $fio = $lastName . ' ' . $firstName . ' ' . $middleName;

            $personArr[$Id] = $fio;
        }
        return $personArr;
    }

    protected function getAllSignStates()
    {
        if (empty($this->signStates)) {
            $StatesClass = new \Model\LodEntities\eiissignstates();
            $states = $StatesClass->getAll();
            $this->signStates = $states;//LicenseModel::getSignStates();
            return $this->signStates;
        } else {
            return $this->signStates;
        }
    }

    protected function getDocument()
    {
        if (empty($this->document)) {
            $this->document = new \Model\LodEntities\document(
              $this->getLicense()->getField('lod_basisForLicensing')
            );
            return $this->document;
        } else {
            return $this->document;
        }
    }

    protected function getDeal()
    {
        if (empty($this->deal)) {
            $this->deal = new \Model\LodEntities\Deal(
              $this->getDocument()->getField('dealId')
            );
            return $this->deal;
        } else {
            return $this->deal;
        }
    }

    protected function getEducationalOrganizations()
    {
        if (empty($this->educationalOrganization)) {
            $this->educationalOrganization = new \Model\Entities\eiisEducationalOrganizations(
              $this->getLicense()->getField('fk_eiisEducationalOrganization')
            );
            return $this->educationalOrganization;
        } else {
            return $this->educationalOrganization;
        }
    }

}