<?php

namespace Eo\Model\Licensing\Service;

class EducationProgramService
{
    protected   $educationLevelIdINstrToQuery = '',
                $supplementId = null,
                $allProgramsBySupplement = [],
                $levelsObjectsFromEduPrograms = [],
                $levelsArrFromEduPrograms = [],
                $allCurrentEducationLevel = [],
                $programsToLevelArray = [],
                $usedLevelsAndData = [],
                $allParentLevel = [],
                $finishLevelAndDataArr = [];

    public function programFromLevelsCount( ){
        $eduProgramsUsed = $this->getUsedLevelsAndData();
        $all = 0;
        $parentArr = [];

        foreach($eduProgramsUsed as $currentLevel => $programArr){

            foreach($programArr['programsByLevel'] as  $eduProgram){
                $upperCurrentLevel = strtoupper( $currentLevel);
                $parentKey =  strtoupper( $eduProgram['parentlevel']);

                if( empty($eduProgram['parentlevel'])){
                    $parentKey = $eduProgram['Id'];
                }
                if( !isset($parentArr[$parentKey])){
                    $parentArr[$parentKey] = 1;
                }else{
                    $parentArr[$parentKey]++;
                }
                $all++;
            }
        }
        $parentArr['all'] = $all;
        return $parentArr;
    }
    
    public function setSupplementId( $id){
        return $this->supplementId = $id;
    }
    
    public function getSupplementId(){
        return $this->supplementId;
    }

    public function getAllProgramBySupplement( $supplementId = null){
        $suppId = $supplementId;
        if( empty($supplementId)){
            $suppId =  $this->getSupplementId();
        }
        
        if( empty($this->allProgramsBySupplement)){
            $retArr = [];
            $dataArr = \Eo\Model\Licensing\EducationProgram::getProgramBySupplementId(
                                           $suppId
                                        );
            if( $dataArr){
                foreach( $dataArr as $obj){
                    $retArr[] = $obj;
                }
            }
            $this->allProgramsBySupplement = $retArr;
        }
        return $this->allProgramsBySupplement;
    }

    protected function getInStrFromObjectsArray( $objectsArray ,$fieldName , $rewind = false){
        $inStr = '';
        $count = 0;
        foreach( $objectsArray as $obj){
            $val = $obj->getFieldOrSpace( $fieldName);
            if( !empty($val)){
                if( $count>0){
                    $inStr .= ', ';
                }
                $inStr .= '"'.$val.'"';
                $count++;
            }else{
                $add = null;
                switch($obj->getFieldOrSpace('educationLevelId')){
                    case 'cc0f7a4c13f7444eb8f5d456872ec59f':
                        $add = '8d2ebc23c4384e31975feac5dcbebcd3';
                    break;
                    case 'e4ceee07d1ea48dba3699fb897a615ea':
                        $add = 'eea61a6ff08c4745829cc3ec291acb42';
                    break;
                    case 'dc01f9795be64aa19ce95c3c8ffdd57b':
                        $add = 'c109164e93954aacb0bf92ab8a19570e';
                    break;
                    case '37555b723e8a4a1c9d4f8b7e47a50942':
                        $add = 'bc02114626fa4b7990e603f46bd3b578';
                    break;
                    case '9b7d89de5da540cf844e8417b3df9661':
                        $add = '5d60bf154916433889a536bca5c8977b';
                    break;
                }
                if(!empty($add)){
                    if( $count>0){
                        $inStr .= ', ';
                    }
                    $inStr .= '"'.$add.'"';
                    $count++;
                }
            }
        }

        if( $rewind == true){
            $objectsArray->rewind();
        }
        return $inStr;
    }
    
    protected function getInStrFromArray( $arrayData ,$fieldName = null){
        $inStr = '';
        $count = 0;

        foreach( $arrayData as $obj){
            if( $count>0){
                 $inStr .= ', ';
            }

            $val = '';
            
            if( empty($fieldName)){
                $val = $obj;
            }else{
                $val = $obj[ $fieldName];
            }
            
            if( !empty($val)){
                $inStr .= '"'.$val.'"';
            }
            $count++;
        }        
        return $inStr;
    }

    public function getAllCurrentEducationLevel(){
        if( empty($this->allCurrentEducationLevel)){
            $levelArr = [];
            $programBySupp = $this->getAllProgramBySupplement();
            
            foreach( $programBySupp as $program){
                $currentLevel = $program->getFieldOrSpace('educationLevelId');
                $upperCurrentLevel = strtoupper( $currentLevel);
                if(
                    empty($levelArr[ $upperCurrentLevel]) &&
                    !empty( $currentLevel)
                ){
                    $levelArr[ $upperCurrentLevel] = $currentLevel;
                }
            }
            $this->allCurrentEducationLevel = $levelArr;
        }
        return $this->allCurrentEducationLevel;
    }
        
    public function getLevelObjectsFromEduPrograms(){
        if( empty($this->levelsObjectsFromEduPrograms)){
            $inStr = $this->getInStrFromArray( $this->getAllCurrentEducationLevel() );

            $eiisEduLevel = [];
            
            if( !empty($inStr)){
                $eiisEduLevel = new \Model\Entities\eiisEduLevels();
                $eiisEduLevel = $eiisEduLevel->getData_ArrayCondition([
                    [ 'column' => 'Id'],
                    [ 'in' => $inStr]
                ])->toArray();
            }
            $this->levelsObjectsFromEduPrograms = $eiisEduLevel;
        }
        return $this->levelsObjectsFromEduPrograms;
    }

    public function getParentLevelObjectsFromEduPrograms(){
        if( empty($this->allParentLevel)){
            $arr = $this->getLevelObjectsFromEduPrograms();

            if( empty($arr)){
               return [];
            }
            
            $parentArr = [];
            
            foreach( $arr as $level){
                if( empty($level['parentlevel'])){
                    $parentArr[ $level['Id'] ] = $level['Id'];
                }else{
                    $parentArr[ $level['parentlevel']] = $level['parentlevel'];
                }
            }
            $inStr = $this->getInStrFromArray( $parentArr );

            $eiisEduLevel = [];

            if( !empty($inStr)){
                $eiisEduLevel = new \Model\Entities\eiisEduLevels();
                $eiisEduLevel = $eiisEduLevel->getData_ArrayCondition([
                    [ 'column' => 'Id'],
                    [ 'in' => $inStr]
                ]);
            }
            $this->allParentLevel = $eiisEduLevel;
        }
        return $this->allParentLevel;
    }

    public function getLevelArrayIdName(){
        $eiisEduLevel = $this->getParentLevelObjectsFromEduPrograms();
        $parentLevel = [];
        if( $eiisEduLevel){
            $eiisEduLevel = $eiisEduLevel->toArray();

            foreach( $eiisEduLevel as $key=>$val){
                 $parentLevel[ $val["Id"]] = $val['Name'];
            }
        }
        return $parentLevel;
    }

    public function getProgramsToLevelArr(){
        if( empty($this->programsToLevelArray)){
            $eduProgGrouped = [];
            $allProgram = $this->getAllProgramBySupplement();
            $i = 0;
            
            foreach ( $allProgram as $prog){
                $prog = $prog->toArray();
                if(empty($prog['educationLevelId'])){
                    $eduProgGrouped['NULL'][] = $prog;
                }else{
                    $eduProgGrouped[ $prog['educationLevelId'] ][] = $prog;
                }
                
            }
            $this->programsToLevelArray = $eduProgGrouped;
        }
        return $this->programsToLevelArray;
    }

    public function getUsedLevelsAndData(){
        if( !empty( $this->finishLevelAndDataArr)){
            return $this->finishLevelAndDataArr;
        }
        //Получаем лод программы к приложению
        $eduProgAll = $this->getAllProgramBySupplement();
        $ret = [];

        //трансформируем массив по ключам в строку формата "'val_0','val_1','val_2',..."
        $eduProgramsIdStrToIn = $this->getInStrFromObjectsArray( $eduProgAll, 'EduProgramsID');
        $lodEduProgramsIdStrToIn = $this->getInStrFromObjectsArray( $eduProgAll, 'id');
        //групируем лод программы к приложению по 'educationLevelId'
        $eduProgGrouped = $this->getProgramsToLevelArr();

        if( empty($eduProgramsIdStrToIn)){
            return [];
        }
        //получаем эти программы в eiis_EduPrograms по EduProgramsID в лод
        $eiisPrograms = new \Model\Entities\eiisEduPrograms();
        $eiisPrograms = $eiisPrograms->getData_ArrayCondition([
            [ 'column' => 'Id'],
            [ 'in' => $eduProgramsIdStrToIn]
        ])->toArray();
        

        //получаем квалификации по id в лод
        $eduProgToQualifications = new \Model\LodEntities\EducationProgramToQualification();
        $eduProgToQualifications = $eduProgToQualifications->getData_ArrayCondition([
                                [ 'column' => 'educationProgramId'],
                                [ 'in' => $lodEduProgramsIdStrToIn]
                            ])->toArray();

        //проходим программы поуровенево
        foreach( $eduProgGrouped as $educationLevel=>$val){

            //если уровень пуст то ничего пока не делаем
            if( !empty($educationLevel)){
                //получаем информацию по уровню
                $eiisEduLevel = new \Model\Entities\eiisEduLevels( $educationLevel);
                $ret[ $educationLevel]['printName'] = $eiisEduLevel->getPrintName();
                $shortName = $eiisEduLevel->getField('ShortName');
                $ret[ $educationLevel]['shortName'] = $shortName;

                
//                $eiisProg = new \Model\Entities\eiisEduPrograms( $val[0][ 'EduProgramsID']);
//                $eiisProgTypesId = $eiisProg->getField('fk_eiisEduProgramType');
                //Получаем тип программы из eiis
                $eiisProgTypesId = '';
                foreach( $eiisPrograms as $searchProg){
                    if( strtoupper( $searchProg['Id']) == strtoupper( $val[0][ 'EduProgramsID'])){
                        $eiisProgTypesId = $searchProg['fk_eiisEduProgramType'];
                        break;
                    }
                }

                $eiisProgTypes = new \Model\Entities\eiisEduProgramTypes( $eiisProgTypesId);
                $ret[ $educationLevel]['LodTitle'] = $eiisProgTypes->getFieldOrSpace('LodTitle');
                $ret[ $educationLevel]['SortOrder'] = $eiisProgTypes->getFieldOrSpace('SortOrder');
                $ret[ $educationLevel]['programsArr'] = [];

                foreach( $val as $program){
                        $processedProgram = [];
                        $eiisProg = [];

                        foreach( $eiisPrograms as $searchProg){
                            if( strtoupper( $searchProg['Id']) == strtoupper( $program[ 'EduProgramsID'])){
                                $eiisProg = $searchProg;
                            }
                        }

                        $eduProgToQualif = [];

                        foreach( $eduProgToQualifications as $searchProg){
                            if( strtoupper( $searchProg['educationProgramId']) == strtoupper( $program[ 'id'])){
                                $eduProgToQualif = $searchProg;
                            }
                        }

                        $qualification ='';

                        if( !empty($eduProgToQualif)){
                            $catalogItem = new \Model\LodEntities\CatalogItem(
                                            $eduProgToQualif['qualificationId']
                                        );
                            $qualification = $catalogItem->getField('title');
                        }

                        $code = '';
                        if( isset($eiisProg['Code']) && $eiisProg['Code']!=='NULL' ){
                            $code = $eiisProg['Code'];
                        }

                        $Name = '';
                        if( isset($eiisProg['Name']) && $eiisProg['Name']!=='NULL' ){
                            $Name = $eiisProg['Name'];
                        }

                        $processedProgram['Id'] = $program[ 'id'];
                        $processedProgram['Code'] = $code;
                        $processedProgram['Name'] = $Name;
                        $processedProgram['ShortName'] = $shortName;
                        $processedProgram['QualificationName'] = $qualification;

                        $parentLevel = $eiisEduLevel->getFieldOrSpace("parentlevel");
                        
                        if( empty($parentLevel)){
                            $parentLevel = $eiisEduLevel->getFieldOrSpace("Id");
                        }
                        $processedProgram['parentlevel'] = $parentLevel;

                        $ret[ $educationLevel] ['programsByLevel'] [] = $processedProgram;
                }

            }
        }
        //сортировка
        if( count($ret)>0 ){
            
            uasort($ret, function( $a, $b){
                return strnatcasecmp($a['SortOrder'],$b['SortOrder']);
            });


            foreach( $ret as $key=>$value){
                uasort($ret[ $key ]['programsByLevel'], function( $a, $b){
                    if( mb_strpos($a['Code'],'.') != false &&  mb_strpos($b['Code'],'.') == false ){
                        return -1;
                    }

                    if( mb_strpos($b['Code'],'.') != false &&  mb_strpos($a['Code'],'.') == false ){
                        return 1;
                    }
                    
                    return strnatcasecmp($a['Code'],$b['Code']);
                });
            }
        }
        return $this->finishLevelAndDataArr = $ret;
        
    }
}