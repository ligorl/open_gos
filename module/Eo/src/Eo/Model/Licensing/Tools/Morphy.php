<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Eo\Model\Licensing\Tools;

/**
 * Description of Morphy_2
 *
 * @author Руслан
 */
/*
 CREATE TABLE `f11_mon_lod`.`morphy_key_data` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `data` VARCHAR(255) NULL COMMENT 'данные',
  `main_type` ENUM('break_word', 'break_expression', 'exeption_expression', 'exeption_string', 'available') NULL COMMENT 'Основной тип для данных\n \'break_word\', \'break_expression\', \'exeption_expression\', \'exeption_collocation\', \'exeption_word\', \'available\'',
  `sub_type` VARCHAR(55) NULL COMMENT 'дополнительный тип',
  PRIMARY KEY (`Id`))
COMMENT = 'Ключевые данные для обработки и склонения предложений';

 */
/*
CREATE TABLE `f11_mon_lod`.`morphy_case` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nominative` VARCHAR(90) NULL,
  `genitive` VARCHAR(90) NULL,
  `dative` VARCHAR(90) NULL,
  `accusative` VARCHAR(90) NULL,
  `instrumental` VARCHAR(90) NULL,
  `prepositional` VARCHAR(90) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = 'nominative/Именительный\ngenitive/Родительный\ndative/Дательный\naccusative/Винительный\ninstrumental/Творительный\nprepositional/Предложный';

  */
class Morphy
{
    protected
        //объект морфи
        $morphyObj              = null,
        //тумблер строк исключений
        $exeptionString         = true,
        //тумблер остановки по слову  при этом слово обрабатывается
        $procBreakString        = true,
        //тумблер слов остановки обработки
        $breakWord              = true,
        //тумблер выражний остановки обработки
        $breakExspression       = true,
        //тумблер исключений в кавычках
        $quotesExeption         = true,
        //Изначальная строка
        $originString           = null,
        //модель ключевых данных
        $keyDataModel           = null,
        //предложение разбитое на массив
        $demountedString        = [],
        //Массив для сброса настроек
        $defaultSettingArray    = [
            'exeptionString'        => true,
            'procBreakString'       => true,
            'breakWord'             => true,
            'breakExspression'      => true,
            'originString'          => null,
            'demountedString'       => []
        ];

    //возвращаем экземпляр phpmorphy
    public function getMorphy(){
        if(empty($this->morphyObj)){
            $morpy = new MorphyProvider();
            $this->morphyObj = $morpy->getMorphy();
        }
        return $this->morphyObj;
    }

    //сеттеры
    function setExeptionString($exeptionString)
    {
        $this->exeptionString = $exeptionString;
    }

    function setExeptionExpression($exeptionExpression)
    {
        $this->exeptionExpression = $exeptionExpression;
    }

    function setBreakWord($breakWord)
    {
        $this->breakWord = $breakWord;
    }

    function setBreakExspression($breakExspression)
    {
        $this->breakExspression = $breakExspression;
    }

    function setOriginString($originString)
    {
        $this->originString = $originString;
    }

    function setDemountedString($demountedString)
    {
        $this->demountedString = $demountedString;
    }

    function setProcBreakString( $procBreakString){
        $this->procBreakString = $procBreakString;
    }

    function setQuotesExeption($quotesExeption)
    {
        $this->quotesExeption = $quotesExeption;
    }
    //************************************

    //геттеры
    function getExeptionString()
    {
        return $this->exeptionString;
    }

    function getExeptionExpression()
    {
        return $this->exeptionExpression;
    }

    function getBreakWord()
    {
        return $this->breakWord;
    }

    function getBreakExspression()
    {
        return $this->breakExspression;
    }

    function getOriginString()
    {
        return $this->originString;
    }

    function getDemountedString()
    {
        return $this->demountedString;
    }

    function getKeyDataModel()
    {
        return new \Model\LodEntities\MorphyKeyData();
    }

    function getCaseModel(){
        return new \Model\LodEntities\MorphyCase();
    }

    function getProcBreakString(){
        return $this->procBreakString;
    }

    function getQuotesExeption()
    {
        return $this->quotesExeption;
    }
    //****************************************

    /**
     * Разбивает строку на две по ближайшей точке остановки по выражению
     * @param type $str
     * @return array
        [
            'first' => string,
            'second' => string
        ];
     */
    private function breakExspressionSplit( $str){
        $dataModel = $this->getKeyDataModel();
        $data = $dataModel->getBreakExspressionData();
        $minPosition = false;
        $len = 0;
        $retArr = [
            'first' => $str,
            'second' => ''
        ];
        
        foreach($data as $breakObj ){
            $exspression = $breakObj->getField('data');
            mb_ereg_search_init($str);
            $pos = mb_ereg_search_pos($exspression);
            if( $pos !== false){
                $pos[0] = ceil( $pos[0]/2);
            }
            if($pos !== false && ( $minPosition === false || ( $minPosition !== false && $pos[0]<$minPosition  ))){
                $minPosition = $pos[0];
                $len = $pos[1];
            }
        }
        if($minPosition !== false){
            $strLen = mb_strlen($str);
            $lenToSubStr = $strLen - ($strLen-$minPosition);
            $retArr['first']    = mb_substr( $str, 0, $lenToSubStr);
            $lenToSubStr = $strLen-$minPosition;
            $retArr['second']   = mb_substr( $str, $minPosition, $lenToSubStr);
        }
        return $retArr;
    }

     /**
     * Разбивает строку на две по ближайшей точке остановки по выражению
     * @param type $str
     * @return array
        [
            'first' => string,
            'second' => string
        ];
     */
    private function breakWordSplit( $str){
        $dataModel = $this->getKeyDataModel();
        $data = $dataModel->getBreakWordData();
        $minPosition = false;
        $len = 0;
        $retArr = [
            'first' => $str,
            'second' => ''
        ];

        foreach($data as $breakObj ){
            $exspression = $breakObj->getField('data');
            $pos = mb_strpos(mb_strtoupper($str, 'UTF-8'), mb_strtoupper($exspression, 'UTF-8') );

            if( $pos !== false && ( $minPosition === false || ( $minPosition !== false && $pos<$minPosition  ))){
                $minPosition = $pos;
            }
            
        }
        if($minPosition !== false){
            $strLen = mb_strlen($str);
            $lenToSubStr = $strLen - ($strLen-$minPosition);
            $retArr['first']    = mb_substr( $str, 0, $lenToSubStr);
            $lenToSubStr = $strLen-$minPosition;
            $retArr['second']   = mb_substr( $str, $minPosition, $lenToSubStr);
        }
        return $retArr;
    }

    /**
     * Разбивает строку на две по ближайшему слову остановки, при этом слово обрабатывается
     * @param type $str
     * @return array
        [
            'first' => string,
            'second' => string
        ];
     */
    private function procBreakWordSplit( $str){
        $dataModel = $this->getKeyDataModel();
        $data = $dataModel->getProcBreakStrData();
        $minPosition = false;
        $strLen = mb_strlen($str);
        $len = 0;
        $retArr = [
            'first' => $str,
            'second' => ''
        ];

        foreach($data as $breakObj ){
            $exspression = $breakObj->getField('data');
            $pos = mb_strpos( mb_strtoupper($str, 'UTF-8'), mb_strtoupper($exspression, 'UTF-8') );
            if( 
              $pos !== false &&
              (
                $minPosition === false ||
                ( $minPosition !== false && $pos<$minPosition  ) ||
                ( $minPosition !== false && $pos==$minPosition && mb_strlen($exspression)>$len)
              )
            ){
                $len = mb_strlen($exspression);
                $minPosition = $pos;
            }

        }
        $afterPos = mb_substr( $str, $minPosition+$len, 1);
        
        if(
            $minPosition !== false &&
          (
            $afterPos === ' '  ||
            $afterPos === ''   ||
            $afterPos === ','  ||
            $afterPos === '!'  ||
            $afterPos === '.'  ||
            $afterPos === '?'  ||
            $afterPos === ':'  ||
            $afterPos === ';'
           )
        ){
            $minPosition = $minPosition+$len;
        }else{
            return $retArr;
        }

        if($minPosition !== false){
            
            $lenToSubStr = $strLen - ($strLen-$minPosition);
            $retArr['first']    = mb_substr( $str, 0, $lenToSubStr);
            $lenToSubStr = $strLen-$minPosition;
            $retArr['second']   = mb_substr( $str, $minPosition, $lenToSubStr);
        }
        return $retArr;
    }

    /**
     *Разбивает строки в массиве на дополнение к массиву по ключу
     * @param type $arrStrData
     * @param type $needle
     * @return array
     */
    private function arrayStringSplitter( $arrStrData, $needle){
        $postProcessedArray = [];
        foreach( $arrStrData as $data){
            if($data['processed']){
                $str = $data['str'];
                $tempArr = $this->stringSplitter( $data['str'], $needle);
                foreach($tempArr as $tempEl){
                    $postProcessedArray[] = $tempEl;
                }
            }else{
                $postProcessedArray[] = $data;

            }
            
        }
        return $postProcessedArray;
    }

    /**
     * Находит элементы в ковычках, и разбивает на массив с указанием обработки
     * @param type $hashstack
     * @return array
     */
    private function quotesSplitter( $hashstack){
        $minPos = false;
        $lastPos = false;
        $posArr = [];
        $quote = '';
        $arr = [];
        if( mb_strpos ( $hashstack, '«' ) || mb_strpos ( $hashstack, '«' )===0  ){
            $posArr[] = [
                'pos' => mb_strpos ( $hashstack, '«' ),
                'sign' => '«'
            ];
        }
        if( mb_strpos ( $hashstack, '"' ) || mb_strpos ( $hashstack, '"' )===0  ){
            $posArr[] = [
                'pos' => mb_strpos ( $hashstack, '"' ),
                'sign' => '"'
            ];
        }
        if( mb_strpos ( $hashstack, "'" ) || mb_strpos ( $hashstack, "'" )===0  ){
            $posArr[] = [
                'pos' => mb_strpos ( $hashstack, "'" ),
                'sign' => "'"
            ];
        }
        if( !empty($posArr)){
            $minPos = $posArr[0]['pos'];
            $quote = $posArr[0]['sign'];
            
            foreach($posArr as $pos){
                if( $pos['pos']<$minPos ){
                    $minPos = $pos['pos'];
                    $quote = $pos['sign'];
                }
            }
            if($quote == '«'){
                $quote = '»';
            }
            $lastPos = mb_strrpos($hashstack, $quote);

            if(!$lastPos){
                $lastPos = $minPos;

            }
            $third = '';
            
            if( $lastPos == $minPos){
                $cutLen = mb_strlen($hashstack)-$minPos;
            }else{
                $cutLen = $lastPos - $minPos+1;
                $third = mb_substr($hashstack, $lastPos+1 );
            }

            $first = mb_substr($hashstack, 0, $minPos );
            $second = mb_substr($hashstack, $minPos, $cutLen );
            
            if( $first !== ''){
                $arr[] = [
                    'str' => $first,
                    'processed' => true
                ];
            }
            if( $second !== ''){
                $arr[] = [
                    'str' => $second,
                    'processed' => false
                ];
            }
            if( $third !== ''){
                $arr[] = [
                    'str' => $third,
                    'processed' => true
                ];
            }
        }else{
            $arr = [[
                'str' => $hashstack,
                'processed' => true
            ]];
        }
        return $arr;
    }

    /**
     * разбивает строку по ключевому слову возвращает массив разделенный по нему
     * с пометкой к обработке либо нет(если ключевое слово/строка)
     * @param type $hashstack
     * @param type $needle
     * @return array
     */
    private function stringSplitter( $hashstack, $needle){
        $proc = true;
        $processedPart = [];
        $otherPart = '';
        $currrentStr = $hashstack;
        $needleLen = mb_strlen( $needle);

        $pos =  mb_strripos( $currrentStr, $needle );
        if( !$pos && $pos !== 0 ){
            $processedPart[] = [
                    'str' => $currrentStr,
                    'processed' => true
                ];
            return $processedPart;
        }

        while ( $proc){
            $pos =  mb_strripos( $currrentStr, $needle );
            if( $pos|| $pos === 0){
               $currentLen =  mb_strlen( $currrentStr);

               $first = mb_substr($currrentStr, 0, $pos );
               $second = mb_substr($currrentStr, $pos, $needleLen );
               $third = mb_substr($currrentStr, $pos+$needleLen );

               if( !empty($third)){
                   $processedPart[] = [
                        'str' => $third,
                        'processed' => true
                    ];
               }

               if(!empty($second)){
                   $processedPart[] = [
                        'str' => $second,
                        'processed' => false
                    ];
               }
                
               $currrentStr = $first;
            }else{
               $proc = false;
            }
        }
        if( !empty($currrentStr)){
            $processedPart[] = [
                    'str' => $currrentStr,
                    'processed' => true
                ];
        }
        $processedPart = array_reverse($processedPart);
        return $processedPart;
    }


    /**
     * разбивает строку по ключу в массив
     * @param type $str
     * @return type
     */
    private function ExeptionStringSplit( $str){
        $dataModel = $this->getKeyDataModel();
        $data = $dataModel->getExeptStringData();
        $stringProcessor = true;
        $processedData = [];

        if( $this->getQuotesExeption()){
            $processedData = $this->quotesSplitter($str);
            $stringProcessor = false;
        }

         foreach($data as $exeptObj ){
            $stringDt = $exeptObj->getField('data');
            if( $stringProcessor ){
                $processedData = $this->stringSplitter($str, $stringDt);
                $stringProcessor = false;
            }else{
                $processedData = $this->arrayStringSplitter( $processedData , $stringDt);
            }
        }
        return $processedData;
    }

    /**
     * проводет деконструкцию строки по "тумблерам"
     * @param type $str
     * @return array
     *  [
     *      arrayToProcessed =>  array
     *      additionalPart   =>  string
     *  ]
     */
    public function offerDeconstructor($str){
        /*
        порядок обработки
        точки остановки
            выражения остновки
            слова остановки
        исключения
            словосочитания исключения
        */
        $firstPart = $str;
        $secondPart = '';
        
        if( $this->getBreakExspression()){
            $tempArr = $this->breakExspressionSplit($firstPart);
            $firstPart = $tempArr['first'];
            $secondPart = $tempArr['second'];
        }

        if( $this->getProcBreakString()){
            $tempArr = $this->procBreakWordSplit($firstPart);
            $firstPart = $tempArr['first'];
            $secondPart = $tempArr['second'].$secondPart;
        }

        if( $this->getBreakWord()){
            $tempArr = $this->breakWordSplit($firstPart);
            $firstPart = $tempArr['first'];
            $secondPart = $tempArr['second'].$secondPart;
        }

        if( $this->getExeptionString()){
            $firstPart = $this->ExeptionStringSplit($firstPart);
            $firstPart = $this->standartSplit($firstPart);
        }else{
            $firstPart = $this->standartSplit($firstPart);
        }


        $firstPart = $this->hooksChecker($firstPart);

        return [
            'arrayToProcessed'  => $firstPart,
            'additionalPart'    => $secondPart
        ];
    }

    private function hooksChecker( $arr){

        if(is_array($arr)){
            $dataModel = $this->getKeyDataModel();
            $data = $dataModel->getHooksData();

            foreach( $data as $el){
                $firstHook = $el->getFieldOrSpace('data');
                $secondHook = $el->getFieldOrSpace('sub_type');
                $isSetFirst = false;
                $isSetSecond = false;

                foreach( $arr as $dt ){
                    $str = $dt['str'];
                    if( mb_strtoupper( $str, 'UTF-8') ===  mb_strtoupper( $firstHook, 'UTF-8') ){
                        $isSetFirst = true;
                    }
                    if( mb_strtoupper( $str, 'UTF-8') ===  mb_strtoupper( $secondHook, 'UTF-8') && $isSetFirst == true ){
                        $isSetSecond = true;
                    }
                }

                if( $isSetFirst && $isSetSecond){
                    $setProc = true;
                    foreach( $arr as $key => $dt){
                        $str = $dt['str'];
                        $pr  = $dt['processed'];
                        if( mb_strtoupper( $str, 'UTF-8') ===  mb_strtoupper( $firstHook, 'UTF-8') ){
                            $setProc = false;
                        }
                        if( mb_strtoupper( $str, 'UTF-8') ===  mb_strtoupper( $secondHook, 'UTF-8')){
                            $setProc = true;
                        }

                        if( !$setProc && $dt['processed']){
                            $arr[$key]['processed'] = false;
                        }
                    }
                }
                
            }
        }
        return $arr;
    }

    private function symbolSplitter($str , $sym){
        $word = '';
        $retArr = [];

        for( $i=0; $i<mb_strlen($str); $i++){
            $curSym = mb_substr($str,$i,1);
            if($curSym==$sym){
               if(!empty($word)){
                   $retArr[]=[
                       'str' => $word,
                        'processed' => true
                    ];
               }
               $retArr[]=[
                       'str' => $sym,
                        'processed' => false
                    ];
                    $word = '';
            }else{
                $word .= $curSym;
            }
        }
        if(!empty($word)){
                   $retArr[]=[
                       'str' => $word,
                        'processed' => true
                    ];
        }

        if(empty($retArr)){
            return false;
        }
        return $retArr;
    }

    private function stringSpaceSplit($str){
        $arr = $this->mb_split($str);
            $retArr = [];

            foreach( $arr as $el){
                $retArr[] = [
                        'str' => $el,
                        'processed' => true
                ];
            }
            return array_reverse($retArr);
    }

    private function standartSplit( $str){
        if(is_string($str)){
            return $this->stringSpaceSplit($str);
        }

        if(is_array($str)){
            $postProcessedArray = [];
            foreach( $str as $data){
                if($data['processed']){
                    $str = $data['str'];
                    $tempArr = $this->stringSpaceSplit( $data['str']);
                    $tempArr = array_reverse( $tempArr);
                    foreach($tempArr as $tkey => $tempEl){
                        $check = $this->symbolSplitter($tempEl['str'], ':');

                        if( $check){
                            foreach( $check as $temp){
                                $postProcessedArray[] = $temp;
                            }
                        }else{
                            $postProcessedArray[] = $tempEl;
                        }
                        
                        if(count($tempArr) != $tkey+1){
                            $postProcessedArray[] = [
                                'str' => ' ',
                                'processed' => false
                            ];
                        }
                        
                        
                    }
                }else{
                    $postProcessedArray[] = $data;

                }

            }
            return $postProcessedArray;
        }
        
        return  [[
                    'str' => $str,
                    'processed' => true
                ]];
    }

    public function getForms( $str){
        $deconstructed = $this->offerDeconstructor($str);
        $arrayToProcessed = $deconstructed['arrayToProcessed'];
        $additionalPart = $deconstructed['additionalPart'];
        $postProcess = [];
        $retArray = [
            'genitive' => '',
            'accusative' => '',
            'dative' => '',
            'ablative' => '',
            'prepositive' => '',
        ];

        foreach( $arrayToProcessed as $data){
            $processedCurrent = $data['processed'];
            if( mb_strlen($data['str'])<3 || empty($data['str'])){
                $processedCurrent = false;
            }
            if($processedCurrent){
                $postProcess = $this->forms( $data['str']);
            }else{
               $postProcess = [
                    'genitive' => $data['str'],
                    'accusative' => $data['str'],
                    'dative' => $data['str'],
                    'ablative' => $data['str'],
                    'prepositive' => $data['str'],
                ];
            }
            $retArray = $this->formArrConcater($retArray, $postProcess);
            $postProcess = [];
        }
        
        $retArray = $this->formArrConcater(
                            $retArray,
                            [
                              'genitive'      => $deconstructed['additionalPart'],
                              'accusative'    => $deconstructed['additionalPart'],
                              'dative'        => $deconstructed['additionalPart'],
                              'ablative'      => $deconstructed['additionalPart'],
                              'prepositive'   => $deconstructed['additionalPart'],
                            ]
                    );
        return $retArray;
    }

    private function formArrConcater( $originArr, $addArray){
        foreach( $addArray as $key=>$value ){
            if( isset( $originArr[$key])){
                $originArr[$key] .= $value;
            }
        }
        return $originArr;
    }

    public function forms( $word, $modification=null, $forced = false, $forcedPos = null, $answerExstractor = null )
    {
        $retWordArr = [];
        $morphy = $this->getMorphy();
        $retWordArr = [];
        if( mb_strlen($word)<4 || empty($word)){
            $retWordArr = [
                'genitive' => $word,
                'accusative' => $word,
                'dative' => $word,
                'ablative' => $word,
                'prepositive' => $word,
            ];
            
        }else{
            $caseModel = $this->getCaseModel();
            $forms = $caseModel->getForms( $word);

            if($forms){
                
                $retWordArr = [
                        'genitive' => $this->restoreCase( $word, $forms->genitive()),
                        'accusative' => $this->restoreCase( $word, $forms->accusative()),
                        'dative' => $this->restoreCase( $word, $forms->dative()),
                        'ablative' => $this->restoreCase( $word, $forms->ablative()),
                        'prepositive' => $this->restoreCase( $word, $forms->prepositive()),
                    ];
            }else{
                $_word = $word;
                $_word = mb_strtoupper($_word, 'UTF-8');
                $wordInfoArr = $morphy->getGramInfo( $_word);
                $_wordInfoArr = $wordInfoArr;
                $lastArray = [];
                 $grammems = [];
                 $pos = null;
                if($_wordInfoArr){
                    $mainArray = array_pop( $_wordInfoArr);
                    $lastArray = array_pop( $mainArray);
                    $grammems = $lastArray['grammems'];
                    $pos = $lastArray["pos"];
                }
                if( $forced && !empty($forcedPos)){
                    $pos = $forcedPos;
                }
                $genitive = '';
                $accusative = '';
                $dative = '';
                $ablative = '';
                $prepositive = '';

                if( $this->wordInfoIsSetGramm( $wordInfoArr, 'РД') && !$forced){
                    $genitive = $word;
                }else{

                    $genitiveGr = $this->grammArrayCompil($grammems, 'РД', $modification, $forced);
                    $_genitive = $morphy->castFormByGramInfo( $_word, $pos, $genitiveGr, true);
                    if( empty($_genitive)){
                        $genitive = $word;
                    }else{
                        if(!empty($answerExstractor)){
                            $genitive = $answerExstractor($_genitive);
                        }else{
                            $genitive = $this->wordRangeMaking( $_word, $_genitive);
                        }
                        $genitive = $this->restoreCase( $word, $genitive );
                    }
                }

                if( $this->wordInfoIsSetGramm( $wordInfoArr, 'ВН') && !$forced){
                    $accusative = $word;
                }else{
                    $accusativeGr = $this->grammArrayCompil($grammems, 'ВН', $modification, $forced);
                    $_accusative = $morphy->castFormByGramInfo( $_word, $pos, $accusativeGr, true);
                    if( empty($_accusative)){
                        $accusative = $word;
                    }else{
                        if(!empty($answerExstractor)){
                            $accusative = $answerExstractor( $_accusative);
                        }else{
                            $accusative = $this->wordRangeMaking( $_word, $_accusative);
                        }
                        $accusative = $this->restoreCase( $word, $accusative);
                    }
                    
                }

                if( $this->wordInfoIsSetGramm( $wordInfoArr, 'ДТ') && !$forced){
                    $dative = $word;
                }else{
                    $dativeGr = $this->grammArrayCompil($grammems, 'ДТ', $modification, $forced);
                    $_dative = $morphy->castFormByGramInfo( $_word, $pos, $dativeGr, true);
                    if( empty($_dative)){
                        $dative = $word;
                    }else{
                        if(!empty($answerExstractor)){
                            $dative = $answerExstractor( $_dative);
                        }else{
                            $dative = $this->wordRangeMaking( $_word, $_dative);
                        }
                        $dative = $this->restoreCase($word, $dative);
                    }
                    
                }

                if( $this->wordInfoIsSetGramm( $wordInfoArr, 'ТВ') && !$forced){
                    $ablative = $word;
                }else{
                    $ablativeGr = $this->grammArrayCompil($grammems, 'ТВ', $modification, $forced);
                    $_ablative = $morphy->castFormByGramInfo( $_word, $pos, $ablativeGr, true);
                    if( empty( $_ablative)){
                        $ablative = $word;
                    }else{
                        if(!empty($answerExstractor)){
                            $ablative = $answerExstractor( $_ablative);
                        }else{
                            $ablative = $this->wordRangeMaking( $_word, $_ablative);
                        }
                        
                        $ablative = $this->restoreCase($word, $ablative);
                    }
                   
                }

                if( $this->wordInfoIsSetGramm( $wordInfoArr, 'ПР') && !$forced){
                    $prepositive = $word;
                }else{
                    $prepositiveGr = $this->grammArrayCompil($grammems, 'ПР', $modification, $forced);
                    $_prepositive = $morphy->castFormByGramInfo( $_word, $pos, $prepositiveGr, true);
                    if( empty($_prepositive)){
                        $prepositive = $word;
                    }else{
                        if(!empty($answerExstractor)){
                            $prepositive = $answerExstractor($_prepositive);
                        }else{
                            $prepositive = $this->wordRangeMaking( $_word, $_prepositive);
                        }
                        $prepositive = $this->restoreCase($word, $prepositive);
                    }
                }

                $retWordArr = [
                        'genitive' => $genitive,
                        'accusative' => $accusative,
                        'dative' => $dative,
                        'ablative' => $ablative,
                        'prepositive' => $prepositive,
                    ];
            }
        }
        return $retWordArr;
    }

    private function wordRangeMaking( $originWord, $wordArray){
        $minRange = mb_strlen($originWord);
        $maxRange = mb_strlen($originWord);
        $find = false;

        while( !$find){
            foreach ( $wordArray as $word ){
                $wordLen = mb_strlen($word);
                if(
                    $wordLen<=$maxRange &&
                    $wordLen>=$minRange
                ){
                    $find = $word;
                    break;
                }
                $minRange--;
                if( $minRange<0){
                    $minRange = 0;
                }
                $maxRange++;
            }
        }
        return $find;
    }

    private function getFirst( $wordArray){
        return array_shift( $wordArray);
    }

     private function restoreCase( $originWord, $restoreWord){
        $isUpperLastCase = true;
        $wordReturn = '';
        $wordLen = mb_strlen($restoreWord);

        if(mb_ereg ('[\-]',$restoreWord) && $wordLen>3){
            $retoreWordArr = $this->symbolSplitter($restoreWord, '-');
            $originWordArr = $this->symbolSplitter($originWord, '-');
            $toReturn = '';
            foreach( $retoreWordArr as $el ){
                if( $el['processed']){
                    $toRestorePart = $el['str'];
                    $originPart = array_shift( $originWordArr);
                    if($originPart['str'] == '-'){
                        $originPart = array_shift( $originWordArr);
                    }
                    $originWordPart = $originPart['str'];
                    $toReturn .= $this->restoreCase( $originWordPart, $toRestorePart);
                }else{
                    $toReturn .= $el['str'];
                }
            }
            return $toReturn;
        }

        for( $i=0; $i<$wordLen; $i++ ){
            $isUpper = $isUpperLastCase;
            if( $i < mb_strlen($originWord)){
                $ch = mb_substr($originWord, $i, 1);
                $isUpper = $this->isUpper($ch);
                $isUpperLastCase = $isUpper;
            }

            if( $isUpper){
                $wordReturn .= mb_strtoupper( mb_substr($restoreWord, $i, 1, 'UTF-8'), 'UTF-8');
            }else{
                $wordReturn .= mb_strtolower( mb_substr($restoreWord, $i, 1, 'UTF-8'), 'UTF-8');
            }
            
        }
        return  $wordReturn;
     }

    private function isUpper( $str){
        $upStr = mb_strtoupper($str, 'UTF-8');
        if( $str == $upStr){
            return true;
        }else{
            return false;
        }
    }


    /**
     * Проверяет наличие конкретного грамматического свойства в массиве информации
     * @param type $wordInfoArr
     * @param type $grammIdentifier
     * @return boolean
     */
    private function wordInfoIsSetGramm( $wordInfoArr, $grammIdentifier){
        if( empty($wordInfoArr)){
            return false;
        }
        $mainArr = array_pop($wordInfoArr);
        foreach( $mainArr as $infoArr ){
            $grammArray = $infoArr["grammems"];
            $res = array_search( $grammIdentifier, $grammArray);
            
            if( !empty($res) || $res === 0){
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param type $defaultGramm
     * @param type $addPart
     */
    private function grammArrayCompil($defaultGramm, $addPart, $modification, $forced){
        $retGramm = [];
        if(!is_array($defaultGramm)){
            $defaultGramm = [];
        }
        foreach($defaultGramm as $temp){
             if(
                  $temp != 'ИМ' &&
                  $temp != 'РД' &&
                  $temp != 'ДТ' &&
                  $temp != 'ВН' &&
                  $temp != 'ТВ' &&
                  $temp != 'ПР' &&
                  $temp != 'ЗВ' &&
                  $temp != '2'  &&
                  !$forced
                ){
                    $retGramm[] = $temp;
                }
        }
        if(is_array($modification)){
            foreach( $modification as $mod){
                $searchResult = array_search( $mod, $retGramm);
                if( empty($searchResult) && $searchResult!== 0){
                    $retGramm[] = $mod;
                }
            }
        }
        $retGramm[] = $addPart;
        return $retGramm;
    }

    public function mb_split($str){
        return mb_split('[\s]', $str);
    }

    public function getGramInfo( $word){
        $morphy = $this->getMorphy();
        $word =  mb_strtoupper($word, 'UTF-8');
        return $morphy->getGramInfo($word);
    }

    public function getFioForms( $surname = '', $name = '' , $patronymic = ''){
        $sex = 'МР';
        $nameArr = [];
        $surNameArr = [];
        $patronumicArr = [];
        $returnArr = [];
        
        $wordInfoArr = $this->getGramInfo($name);
        if( $this->wordInfoIsSetGramm($wordInfoArr, 'ЖР')){
            $sex = 'ЖР';
        }
        $nameArr = $this->forms($name,[ $sex,'ИМЯ']);

        $wordInfoArr = $this->getGramInfo($surname);
        $surNameArr = $this->forms($surname,[ $sex,'ФАМ'], true, 'С', function($arr){ return array_pop($arr);});

       $patronumicArr = $this->forms($patronymic,[ $sex,'ОТЧ'], true, null, function($arr){ return array_shift($arr);});
       
       $caseArr = ['genitive','accusative','dative','ablative','prepositive'];
        foreach( $caseArr as $case){
            $returnArr[$case] = [
                'name' => $nameArr[$case],
                'surname' => $surNameArr[$case],
                'patronymic' => $patronumicArr[$case]
            ];
        }
        $returnArr['preposition'] = $returnArr['prepositive'];
        return $returnArr;
    }
    
    public function test(){
        $str = 'Государственное образовательное учреждение - средняя общеобразовательная школа при войсковой части 2012 Пограничного управления Федеральной службы безопасности Российской Федерации в Республике Армения';
           
        \Zend\Debug\Debug::dump( $this->getForms($str));
        //\Zend\Debug\Debug::dump($split)sd;
        die;
        return $data;
    }
}