<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Eo\Model\Licensing\Tools;
class DevTools
{
    /**
     * Определяем есть корень паблик или ниже
     * @return boolean
     */
    static public function isPublic(){
        $root = $_SERVER['DOCUMENT_ROOT'];
        if(strpos( $root,'public')){
            return true;
        }
        return false;
    }


    /**
     * Возвращает путь Путь к вендор
     * @return string
     */
    static public function getPathToVendor(){
        if( self::isPublic()){
           return '/vendor';
        }else{
            return $_SERVER['DOCUMENT_ROOT'].'/vendor';
        }
    }

    /**
     * Возвращает путь к паблик
     * @return string
     */
    static public function getPathToPublic()
    {
        if( self::isPublic()){
            return $_SERVER['DOCUMENT_ROOT'];
        }else{
             return $_SERVER['DOCUMENT_ROOT'].'/public';
        }
    }
}