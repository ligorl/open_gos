<?php
namespace Eo\Model\Licensing\Tools;

class Converter{
    /**
     * создет массив ключ значение по выбранным столбцам из массива ответов
     * @param type $resultArr
     * @param type $keyFieldName
     * Имя поля из которого будет браться значение, или массив полей
     * @param type $valueFieldNameOrArrayField
     * @return type
     */
    public static function getTwoCollumnArrFromResultSet( $resultArr ,$keyFieldName , $valueFieldNameOrArrayField, $callbk=null ){
        $retArr = [];
        
        if( !empty($resultArr)){
            
            foreach( $resultArr as $result ){
                if(gettype($callbk)=='object'){
                    $result = $callbk( $result);
                }

                switch(gettype( $valueFieldNameOrArrayField)){
                    case 'string':
                        $retArr[ $result->getField( $keyFieldName) ] = $result->getField( $valueFieldNameOrArrayField);
                    break;
                    case 'array':
                        $completeStr ='';
                        $count = 0;
                        
                        foreach( $valueFieldNameOrArrayField as $field ){

                            if( $count>0 ){
                                $completeStr .= ' ';
                            }
                            $completeStr .= $result->getField( $field );
                            $count++;
                        }
                        $retArr[ $result->getField( $keyFieldName) ] = $completeStr;
                    break;
                }
                
            }
        }
        return $retArr;
    }

    /**
     * создет массив ключ значение по выбранным столбцам из массива 
     * @param type $resultArr
     * @param type $keyFieldName
     * @param type $valueFieldName
     * @param type $callbk
     * @return type
     */
    public static function getTwoCollumnArrFromArray( $resultArr ,$keyFieldName, $valueFieldName , $callbk=null){
        $retArr = [];

        if( !empty($resultArr)){

            foreach( $resultArr as $result ){
                
                if(gettype($callbk)=='object'){
                    $result = $callbk( $result);
                }
                $retArr[ $result[ $keyFieldName] ] = $result[ $valueFieldName];
            }
        }
        return $retArr;
    }

    /**
     * Проверяет массив на ключи содержащие "date",
     *  и при нахождении преобразет содержимое к формату использующемуся в базе Y-m-d
     * @param type $arr
     */
    public static function checkArrToDateKeyAndConverToYmd( &$arr){
        if( gettype($arr) == 'array'){

            foreach( $arr as $key => $value ){
                $upkey = strtoupper ( $key );
                if( strpos($upkey, "DATE") !== FALSE){
                    if(
                        !empty($value) &&
                        $value != '-'
                    ){
                        $date = new \DateTime($value);
                        $arr[$key] = $date->format('Y-m-d');
                    }

                    if( $value == '-' || empty($value)){
                        $arr[$key] = "0000-00-00";
                    }
                    
                }
            }
        }
    }

    /**
     * проверяет на пустоту строку из базы с датой, если её нет возвращает $defaultNoCorrectMessage
     * @param $date
     * @param type $defaultNoCorrectMessage
     * @return type
     */
    public static function date( $date, $defaultNoCorrectMessage = '-'){
        
        switch ( true){
            case ($date === 'NULL'):
                return $defaultNoCorrectMessage;
            break;
            case empty( $date):
                return $defaultNoCorrectMessage;
            break;
            case ($date === '0000-00-00'):
                return $defaultNoCorrectMessage;
            break;
            case ($date === '-'):
                return $defaultNoCorrectMessage;
            break;
        }

        $date = new \DateTime( $date);
        return $date->format('d.m.Y');
    }

    /**
     * Проверяет элемент из массива на существование и пустоту, и возвращает его или пустую строку
     * @param type $key
     * @param type $arr
     * @return string
     */
    public static function checkSetAndGetData( $key, $arr){
        if( !empty($arr[$key])){
            return $arr[$key];
        }
        return '';
    }

     /**
     * проверяет на пустоту строку из базы с датой, если её нет возвращает $defaultNoCorrectMessage
     * @param $date
     * @param type $defaultNoCorrectMessage
     * @return type
     */
    public static function dateToBase( $date){

        switch ( true){
            case ($date === 'NULL'):
                return '0000-00-00';
            break;
            case empty( $date):
                return '0000-00-00';
            break;
            case ($date === ''):
                return '0000-00-00';
            break;
            case ($date === '-'):
                return '0000-00-00';
            break;
        }

        $date = new \DateTime( $date);
        return $date->format('Y-m-d');
    }
}