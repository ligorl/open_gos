<?php
namespace Eo\Model\Licensing\Tools;
//require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/phpmorphy/src/common.php');
require_once('/vendor/phpmorphy/src/common.php');
/*
 * Работа с морфи  
 */

class Morphy
{
    const DICTIONARY_CATALOG = '/dicts/',
          LANG_RU = 'ru_RU',
          OPTION_STORAGE = PHPMORPHY_STORAGE_FILE;

    protected $morphyObj = null;

    //возвращаем каталог словарей
    public function getDictionaryCatlog(){
//        return $_SERVER['DOCUMENT_ROOT'].'/public'.self::DICTIONARY_CATALOG;
        return $_SERVER['DOCUMENT_ROOT'].self::DICTIONARY_CATALOG;
    }

    //возвращаем каталог язык
    public function getLang(){
        return self::LANG_RU;
    }

    //возвращаем настройка работы с памятью
    public function getStorageOption(){
        return self::OPTION_STORAGE;
    }

    //возвращаем рабочий экземпляр phpmorphy
    public function getMorphy(){

        
        if( empty( $this->morphyObj)){

            try {

                $this->morphyObj = new \phpMorphy(
                                    $this->getDictionaryCatlog(),
                                    $this->getLang(),
                                    [
                                        'storage' => $this->getStorageOption()
                                    ]
                                );

            } catch(phpMorphy_Exception $e) {

                die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
            }
        }
        return $this->morphyObj;
    }

    //конструктор
    public function __construct( ){
        
    }

    /**
     * 
     * Возвращает нужные формы
     * <ul>
     *  <li>genitive - родительный</li>
     *  <li>accusative - винительный</li>
     *  <li>dative - дательный</li>
     *  <li>ablative - творительрый</li>
     *  <li>prepositive - предложный</li>
     * </ul>
     * @param string $str
     * @param bool $activateException Включает или отключает список исключений
     * @param string $breakWorld Стоп символ или слово 
     * @return array - массив из форм
     */
    public function getForms( $str, $activateException=true, $breakWorld = null ){
        $retStrArr = [
            'genitive' => '',
            'accusative' => '',
            'dative' => '',
            'ablative' => '',
            'prepositive' => '',
        ];

        if( empty($str) || mb_strlen($str)<4 || ( mb_strlen($str)==3 && mb_stripos($str, '.'))){
            return [
                'genitive' => $str,
                'accusative' => $str,
                'dative' => $str,
                'ablative' => $str,
                'prepositive' => $str,
            ];
        }
        
        $addPArt = '';
        $originStr = $str;
        $pos = false;
        $shiftLen = -1;

        try {

            $tpos = [];
            
            if( mb_strpos ( $str, '«' )){
                $tpos[] = mb_strpos ( $str, '«' );
            }
            if( mb_strpos ( $str, '"' )){
                $tpos[] = mb_strpos ( $str, '"' );
            }
            if( mb_strpos ( $str, "'" )){
                $tpos[] = mb_strpos ( $str, "'" );
            }
            if( !empty($breakWorld)){
                if(is_array($breakWorld)){
                    foreach ($breakWorld as $bw){
                        if( mb_stripos( $str, $bw ) !== false){
                            $shiftLen = mb_strlen($bw);
                            $tpos[] = mb_stripos ( $str, $bw );
                        }
                    }
                }else{
                    if( mb_stripos( $str, $breakWorld ) !== false){
                        $shiftLen = mb_strlen($breakWorld);
                        $tpos[] = mb_stripos ( $str, $breakWorld );
                    }
                }
            }

            foreach( $tpos as $n){
                if($pos == false){
                    $pos = $n;
                }
                if($pos>$n){
                    $pos = $n;
                }
            }

            if( $pos !== false ){
                $strLen = mb_strlen($str);
                $addPartLen = $strLen - ( $pos+$shiftLen );
                $addPArt = mb_substr( $str, $pos+$shiftLen, $addPartLen);
                $str = mb_substr( $str, 0, $pos+$shiftLen);
            }

            if( mb_strrpos ( $str, ' ' )){
                $wordArr = mb_split('\s', $str);
                $tempStrArr = [];
                $count = 0;

                foreach( $wordArr as $word){
                    $splitter = '';
                    if($count>0){
                        $splitter = ' ';
                    }

                    if( !( !mb_ereg_match("^[а-яА-Я\-]+$", $word) || empty($word) || mb_strlen($word)<3 ) ){
                        $firstLeter = mb_substr($word,0, 1,'UTF-8');
                        $word = mb_strtoupper($word, 'UTF-8');
                        $origin = $word;
                        $wordInfoArr = $this->getMorphy()->getGramInfo($word);
//                        \Zend\Debug\Debug::dump($word);
    //                    \Zend\Debug\Debug::dump($wordInfoArr);
                        $grammems = $wordInfoArr[0][0]['grammems'];
                        $pos = $wordInfoArr[0][0]["pos"];
                        $origin = $word;

                        if( $this->isLeaningCaseOnGenitive( $wordInfoArr, $activateException)   ){
                            $word = $this->getVariant($grammems, $pos, $word, 'РД');
                        }
                        if( empty($word)){
                            $word = $origin;
                        }
                        $retStrArr['genitive'] .= $splitter.$firstLeter.mb_substr(mb_strtolower( $word, 'UTF-8'),1,null,'UTF-8');
                        $word = $origin;

                        if( $this->isLeaningCaseOnAccusative( $wordInfoArr, $activateException) ){
                            $word = $this->getVariant($grammems, $pos, $word, 'ВН');
                        }
                        if( empty($word)){
                            $word = $origin;
                        }
                        $retStrArr['accusative'] .= $splitter.$firstLeter.mb_substr(mb_strtolower( $word, 'UTF-8'),1,null,'UTF-8');
                        $word = $origin;

                        if( $this->isLeaningCaseOnDative( $wordInfoArr, $activateException) ){
                            $word = $this->getVariant($grammems, $pos, $word, 'ДТ');
                        }
                        if( empty($word)){
                            $word = $origin;
                        }
                        $retStrArr['dative'] .= $splitter.$firstLeter.mb_substr(mb_strtolower( $word, 'UTF-8'),1,null,'UTF-8');
                        $word = $origin;

                        if(  $this->isLeaningCaseOnAblative( $wordInfoArr, $activateException) ){
                            $word = $this->getVariant($grammems, $pos, $word, 'ТВ');
                        }
                        if( empty($word)){
                            $word = $origin;
                        }
                        $retStrArr['ablative'] .= $splitter.$firstLeter.mb_substr(mb_strtolower( $word, 'UTF-8'),1,null,'UTF-8');
                        $word = $origin;

                        //Склоняем в предложный
                        if(  $this->isLeaningCaseOnPrepositve( $wordInfoArr, $activateException) ){
                            $word = $this->getVariant($grammems, $pos, $word, 'ПР');
                        }
                        if( empty($word)){
                            $word = $origin;
                            }
                            $retStrArr['prepositive'] .= $splitter.$firstLeter.mb_substr(mb_strtolower( $word, 'UTF-8'),1,null,'UTF-8');
                            $word = $origin;                    
                        }else{
                            $retStrArr['genitive'] .= $splitter.$word;
                            $retStrArr['accusative'] .= $splitter.$word;
                            $retStrArr['dative'] .= $splitter.$word;
                            $retStrArr['ablative'] .= $splitter.$word;
                            $retStrArr['prepositive'] .= $splitter.$word;
                        }
                        $count++;
                    }
        }else{
                $word = '';

                //if( !empty($str)){
                //    $word = mb_strtoupper($str, 'UTF-8');
                //}
                //если строка не пустая и содержит тольго русски буквы 
                //то склонять слово или отдать исходя исходное 
                if(  !empty($str) &&  mb_ereg_match("^[а-яА-Я]*$", $str) ){
                    $firstLeter = mb_substr($str,0, 1,'UTF-8');
                    $word = mb_strtoupper($str, 'UTF-8');
                    $wordInfoArr = $this->getMorphy()->getGramInfo($word);
                    $grammems = $wordInfoArr[0][0]['grammems'];
                    $pos = $wordInfoArr[0][0]["pos"];
        //            \Zend\Debug\Debug::dump($word);

                    $genitive = (  $this->getVariant($grammems, $pos, $word, 'РД')) ?  $this->getVariant($grammems, $pos, $word, 'РД') : '';
                    $accusative = (  $this->getVariant($grammems, $pos, $word, 'ВН')) ?  $this->getVariant($grammems, $pos, $word, 'ВН') : '';
                    $dative = (  $this->getVariant($grammems, $pos, $word, 'ДТ')) ?  $this->getVariant($grammems, $pos, $word, 'ДТ') : '';
                    $ablative = (  $this->getVariant($grammems, $pos, $word, 'ТВ')) ?  $this->getVariant($grammems, $pos, $word, 'ТВ') : '';
                    $prepositive = (  $this->getVariant($grammems, $pos, $word, 'ПР')) ?  $this->getVariant($grammems, $pos, $word, 'ПР') : '';

                        $retStrArr = [
                            'genitive' => $firstLeter.mb_substr(mb_strtolower(  $genitive, 'UTF-8' ),1,null,'UTF-8'),
                            'accusative' => $firstLeter.mb_substr(mb_strtolower(  $accusative, 'UTF-8' ),1,null,'UTF-8'),
                            'dative' => $firstLeter.mb_substr(mb_strtolower(  $dative, 'UTF-8'  ),1,null,'UTF-8'),
                            'ablative' => $firstLeter.mb_substr(mb_strtolower( $ablative, 'UTF-8'  ),1,null,'UTF-8'),
                            'prepositive' => $firstLeter.mb_substr(mb_strtolower( $prepositive, 'UTF-8'  ),1,null,'UTF-8'),
                        ];
                }else{
                    $retStrArr['genitive'] .= $str;
                    $retStrArr['accusative'] .= $str;
                    $retStrArr['dative'] .= $str;
                    $retStrArr['ablative'] .= $str;
                    $retStrArr['prepositive'] .= $str;
                }

            }
           /*
            $retStrArr['genitive'] = $this->firstLetterToUp($retStrArr['genitive']).$addPArt;
            $retStrArr['accusative'] = $this->firstLetterToUp($retStrArr['accusative']).$addPArt;
            $retStrArr['dative'] = $this->firstLetterToUp($retStrArr['dative']).$addPArt;
            $retStrArr['ablative'] = $this->firstLetterToUp($retStrArr['ablative']).$addPArt;
            $retStrArr['prepositive'] = $this->firstLetterToUp($retStrArr['prepositive']).$addPArt;
            */
            //костыль на склонялку в предложном не склонении
            $retStrArr['prepositive'] = \Ron\Model\Export::mb_str_replace('  ', ' ', $retStrArr['prepositive']);
            $retStrArr['prepositive'] = \Ron\Model\Export::mb_str_replace('учрежденье высочайшем образованиях', 'учреждении высшего образования', $retStrArr['prepositive']);
            $retStrArr['prepositive'] = \Ron\Model\Export::mb_str_replace('высочайшем профессиональном образованиях', 'высшем профессиональном образованиии', $retStrArr['prepositive']);
            $retStrArr['prepositive'] = \Ron\Model\Export::mb_str_replace('учрежденье высочайшем образованиях', 'учреждение высшего  образования', $retStrArr['prepositive']);
            $retStrArr['prepositive'] = \Ron\Model\Export::mb_str_replace('высочайшем образованиях', 'высшего образования', $retStrArr['prepositive']);            
            
            $retStrArr['genitive'] = $retStrArr['genitive'].$addPArt;
            $retStrArr['accusative'] = $retStrArr['accusative'].$addPArt;
            $retStrArr['dative'] = $retStrArr['dative'].$addPArt;
            $retStrArr['ablative'] = $retStrArr['ablative'].$addPArt;
            $retStrArr['prepositive'] = $retStrArr['prepositive'].$addPArt;            
        } catch ( \Exception $exc ) {
            //echo $exc->getTraceAsString();
        }

        return $retStrArr;
    }

    /**
     * Естьли необходимость конвертации в данном падеже
     * @param type $data
     * @param type $activateException Включает или отключает список исключений
     * @return bool
     */
    private function isLeaningCaseOnGenitive( $data, $activateException=true){
        $patternArr = [
            [
                "pos" => "С",
                "grammems" => [
                        0 => "ВН",
                        1 => "МН",
                        2 => "НО",
                        3 => "СР",
                      ],
                "form_no" => 2
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                  5 => "ПРЕВ",
                ]
            ] ,
            [
                "pos" => "С",
                "grammems" =>[
                    0 => "ВН",
                    1 => "МН",
                    2 => "НО",
                    4 => "СР",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЖР",
                  2 => "МН",
                  3 =>  "НО",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "ИМ",
                  4 => "ИМЯ",
                  5 => "ОД"
                ]
            ]
            ,
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 =>  "НО",
                  4 => "ОД",
                ]
            ]
            //
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "НО",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "ДФСТ",
                  2 => "МН",
                  3 => "НО",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЖР",
                  1 => "МН",
                  2 => "НО",
                  3 => "РД",
                ]
            ]
            //
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "РД",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "НО",
                  4 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "МР",
                  2 => "НО",
                  3 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "НО",
                  2 => "ПР",
                  3 => "СР",
                ]
            ]

        ];
        if( $activateException){
            return $this->isLeaningCaseOn( $patternArr , $data);
        }else{
            return $this->isLeaningCaseOn( $patternArr , []);
        }
    }

    /**
     * Естьли необходимость конвертации в данном падеже
     * @param type $data
     * @param type $activateException Включает или отключает список исключений
     * @return bool
     */
    private function isLeaningCaseOnAccusative( $data, $activateException=true){
        $patternArr = [
            [
                "pos" => "С",
                "grammems" => [
                        0 => "ВН",
                        1 => "МН",
                        2 => "НО",
                        3 => "СР",
                      ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                  5 => "ПРЕВ",
                ]
            ],
            [
                "pos" => "П",
                "grammems" =>[
                    0 => "ВН",
                    1 => "ЕД",
                    2 => "НО",
                    3 => "ОД",
                    4 => "СР",
                ]
            ],
            [
                "pos" => "С",
                "grammems" =>[
                    0 => "ВН",
                    1 => "МН",
                    2 => "НО",
                    4 => "СР",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "ИМ",
                  4 => "ИМЯ",
                  5 => "ОД"
                ]
            ]
            ,
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 =>  "НО",
                  4 => "ОД",
                ]
            ]//
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "НО",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "ДФСТ",
                  2 => "МН",
                  3 => "НО",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЖР",
                  1 => "МН",
                  2 => "НО",
                  3 => "РД",
                ]
            ]//
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "РД",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "НО",
                  4 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "МР",
                  2 => "НО",
                  3 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "НО",
                  2 => "ПР",
                  3 => "СР",
                ]
            ]
        ];
        if( $activateException){
            return $this->isLeaningCaseOn( $patternArr , $data);
        }else{
            return $this->isLeaningCaseOn( $patternArr , []);
        }
    }

    /**
     * Естьли необходимость конвертации в данном падеже
     * @param type $data
     * @param type $activateException Включает или отключает список исключений
     * @return bool
     */
    private function isLeaningCaseOnDative( $data, $activateException=true){
        $patternArr = [
            [
                "pos" => "С",
                "grammems" => [
                        0 => "ВН",
                        1 => "МН",
                        2 => "НО",
                        3 => "СР",
                      ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                  5 => "ПРЕВ",
                ]
            ],
            [
                "pos" => "С",
                "grammems" =>[
                    0 => "ВН",
                    1 => "МН",
                    2 => "НО",
                    4 => "СР",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЖР",
                  2 => "МН",
                  3 =>  "НО",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "ИМ",
                  4 => "ИМЯ",
                  5 => "ОД"
                ]
            ]
            ,
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 =>  "НО",
                  4 => "ОД",
                ]
            ]//
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "НО",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "ДФСТ",
                  2 => "МН",
                  3 => "НО",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЖР",
                  1 => "МН",
                  2 => "НО",
                  3 => "РД",
                ]
            ]//
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "РД",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "НО",
                  4 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "МР",
                  2 => "НО",
                  3 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "НО",
                  2 => "ПР",
                  3 => "СР",
                ]
            ]
        ];
        
        if( $activateException){
            return $this->isLeaningCaseOn( $patternArr , $data);
        }else{
            return $this->isLeaningCaseOn( $patternArr , []);
        }
    }

    /**
     * Естьли необходимость конвертации в данном падеже
     * @param type $data
     * @param type $activateException Включает или отключает список исключений
     * @return bool
     */
    private function isLeaningCaseOnAblative( $data, $activateException=true){
        $patternArr = [
            [
                "pos" => "С",
                "grammems" => [
                        0 => "ВН",
                        1 => "МН",
                        2 => "НО",
                        3 => "СР",
                      ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                  5 => "ПРЕВ",
                ]
            ],
            [
                "pos" => "С",
                "grammems" =>[
                    0 => "ВН",
                    1 => "МН",
                    2 => "НО",
                    4 => "СР",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "КАЧ",
                  3 =>  "МР",
                  4 => "ОД",
                ]
            ],[
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЖР",
                  2 => "МН",
                  3 =>  "НО",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "ИМ",
                  4 => "ИМЯ",
                  5 => "ОД"
                ]
            ]
            ,
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 =>  "НО",
                  4 => "ОД",
                ]
            ]//
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "НО",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "ДФСТ",
                  2 => "МН",
                  3 => "НО",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ВН",
                  1 => "МН",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "П",
                "grammems" => [
                  0 => "ВН",
                  1 => "ЕД",
                  2 => "МР",
                  3 => "ОД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЖР",
                  1 => "МН",
                  2 => "НО",
                  3 => "РД",
                ]
            ]//
            ,
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "НО",
                  3 => "РД",
                  4 => "СР",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ДФСТ",
                  1 => "ЕД",
                  2 => "ЖР",
                  3 => "НО",
                  4 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "МР",
                  2 => "НО",
                  3 => "РД",
                ]
            ],
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "НО",
                  2 => "ПР",
                  3 => "СР",
                ]
            ]
        ];
        if( $activateException){
            return $this->isLeaningCaseOn( $patternArr , $data);
        }else{
            return $this->isLeaningCaseOn( $patternArr , []);
        }
        
    }

    /**
     * колдовство на предложный падеж
     * не знаю чего тут должно бытть
     * @param type $data
     * @return type
     */    
    public function isLeaningCaseOnPrepositve($data) {
        $patternArr = [
            [
                "pos" =>  "С",
                "grammems" => [
                  0 => "ЕД",
                  1 => "ЖР",
                  2 => "ИМ",
                  3 => "НО",
                ]
            ]
        ];
        return $this->isLeaningCaseOn( $patternArr , $data);
    }
    
    private function isLeaningCaseOn( $default, $current){
        if( empty($current)){
            return true;
        }
        //нуждаеться в изменение падежа по ум.
        $status = true;

        if( $current == false){
            //если слово не обработалось
            return false;
        }

        //берем првый блок граматичесих данных о слове
        foreach ( $current[0] as $data_arr){

            //берем блоки с иключениями
            foreach ( $default as $data_arr_2){
                //изымаем форму
                if( isset($data_arr["form_no"])){
                    unset( $data_arr["form_no"]);
                }
                //по умолчанию статус необходимости в обработки
                $same = true;
                foreach ( $data_arr as $key=>$value){
                    if( empty($data_arr_2[$key]) || $data_arr_2[$key] != $value){
                        //если значения не совпадают значит их нет в списке
                        $same = false;
                    }
                }

                //слово не нужно менять
                if( $same){
                    $status = false;
                }
            }
            
        }
        return $status;
    }


    /**
     * Трансформирует первую букву предложения в большую
     * @param type $str
     * @return type
     */
    public function firstLetterToUp( $word){
        return mb_strtoupper( mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8'). mb_substr( $word, 1, mb_strlen($word)-1, 'UTF-8');
    }

     private function getVariant( $grammems, $partSpeech, $word, $needCase){
        $toGrammems = [];
        //\Zend\Debug\Debug::dump($word);
        if( count($grammems)>0){
            foreach ( $grammems as $gramm){
                if(
                  $gramm != 'ИМ' &&
                  $gramm != 'РД' &&
                  $gramm != 'ДТ' &&
                  $gramm != 'ВН' &&
                  $gramm != 'ТВ' &&
                  $gramm != 'ПР' &&
                  $gramm != 'ЗВ' &&
                  $gramm != '2'
                ){
                    $toGrammems[] = $gramm;
                }
            }
        }
        $toGrammems[] = $needCase;
        $PrecariousSituation = false;
        $wordVariants = [];
        
        if( $word == 'ПОСОЛ'){
            switch( $needCase){
              case 'РД':
                  return 'ПОСЛА';
                break;
              case 'ВН':
                  return 'ПОСЛА';
                  break;
              case 'ДТ':
                  return 'ПОСЛУ';
                  break;
              case 'ТВ':
                  return 'ПОСЛОМ';
                  break;
            }
            $PrecariousSituation = true;
        }
        
        if( $word == 'НАУЧНО-ИССЛЕДОВАТЕЛЬСКИЙ'){
            switch( $needCase){
              case 'РД':
                  return 'НАУЧНО-ИССЛЕДОВАТЕЛЬСКИЙ';
                break;
              case 'ВН':
                  return 'НАУЧНО-ИССЛЕДОВАТЕЛЬСКИЙ';
                  break;
              case 'ДТ':
                  return 'НАУЧНО-ИССЛЕДОВАТЕЛЬСКОМУ';
                  break;
              case 'ТВ':
                  return 'НАУЧНО-ИССЛЕДОВАТЕЛЬСКИМ';
                  break;
            }
            $PrecariousSituation = true;
        }
        if(!$PrecariousSituation){
            $wordVariants = $this->getMorphy()->castFormByGramInfo( $word, $partSpeech, $toGrammems);
        }
        
        
        if( $wordVariants == false){
            return '';
        }
        
        return $wordVariants[0]["form"];
     }

     /**
      * Принимает ФИО и возвращает падежи
      * @param string $surname
      * @param string $name
      * @param string $patronymic
      * @return array
      */
    public function getFioForms( $surname = null, $name = null , $patronymic = null){
        $answer = [];
        $caseArr = ['genitive'=>'РД','accusative'=>'ВН','dative'=>'ДТ','ablative'=>'ТВ','preposition'=>'ПР'];
        $surname = mb_strtoupper($surname, 'UTF-8');
        $name = mb_strtoupper($name, 'UTF-8');
        $patronymic = mb_strtoupper($patronymic, 'UTF-8');

        foreach( $caseArr as $key=>$case){
            if( !empty($name)){
               $wordInfoArr = $this->getMorphy()->getGramInfo($name);
                $sex = 'МР';
                $grammems = $wordInfoArr[0][0]['grammems'];
                foreach ($wordInfoArr[0] as $grammArr){
                    if(in_array('ЖР', $grammArr['grammems'])){
                     $sex = 'ЖР';
                    }
                }

                $answer[ $key ]['name'] = $this->getMorphy()->castFormByGramInfo( $name, 'С' , [ $sex, $case, 'ИМЯ', 'ЕД', 'ОД']);
                $answer[ $key ]['name'] = $answer[ $key ]['name'][0]["form"];
                $firstLeter = mb_substr($answer[ $key ]['name'],0, 1,'UTF-8');
                $answer[ $key ]['name'] = $firstLeter.mb_substr(mb_strtolower(  $answer[ $key ]['name'] , 'UTF-8' ),1,null,'UTF-8');
            }
            
            if( !empty($surname)){
                $wordInfoArr = $this->getMorphy()->getGramInfo($surname);
                $sex = 'МР';
                $grammems = $wordInfoArr[0][0]['grammems'];
                foreach ($wordInfoArr[0] as $grammArr){
                    if(in_array('ЖР', $grammArr['grammems'])){
                     $sex = 'ЖР';
                    }
                }

                $answer[ $key ]['surname'] = $this->getMorphy()->castFormByGramInfo( $surname, 'С', [ $sex, $case, 'ФАМ', 'ЕД', 'ОД']);
                if( !empty($answer[ $key ]['surname'][0]["form"]) ){
                    $answer[ $key ]['surname'] = $answer[ $key ]['surname'][0]["form"];
                } else {
                    $answer[ $key ]['surname'] = $surname;
                }
                $firstLeter = mb_substr($answer[ $key ]['surname'],0, 1,'UTF-8');
                $answer[ $key ]['surname'] = $firstLeter.mb_substr(mb_strtolower(  $answer[ $key ]['surname'] , 'UTF-8' ),1,null,'UTF-8');
            }
            

            if( !empty($patronymic)){
                $wordInfoArr = $this->getMorphy()->getGramInfo($patronymic);
                $sex = 'МР';
                $grammems = $wordInfoArr[0][0]['grammems'];
                foreach ($wordInfoArr[0] as $grammArr){
                    if(in_array('ЖР', $grammArr['grammems'])){
                     $sex = 'ЖР';
                    }
                }
                
                $answer[ $key ]['patronymic'] = $this->getMorphy()->castFormByGramInfo( $patronymic, 'С', [ $sex, $case, 'ОТЧ', 'ЕД', 'ОД' ]);
                $answer[ $key ]['patronymic'] = $answer[ $key ]['patronymic'][0]["form"];
                $firstLeter = mb_substr($answer[ $key ]['patronymic'],0, 1,'UTF-8');
                $answer[ $key ]['patronymic'] = $firstLeter.mb_substr(mb_strtolower(  $answer[ $key ]['patronymic'] , 'UTF-8' ),1,null,'UTF-8');
            }
        }
        return $answer;
    }
}
