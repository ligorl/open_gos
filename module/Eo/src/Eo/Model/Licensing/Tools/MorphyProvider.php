<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Eo\Model\Licensing\Tools;

/**
 * Description of MorphySettingProvider
 *
 * @author Руслан
 */
class MorphyProvider
{
    const
        REQUIRE_PATH_SECOND_PART = '/phpmorphy/src/common.php' ,
        DICTIONARY_CATALOG = '/dicts/',
        LANG_RU = 'ru_RU';

    protected $morphyObj = null;

    //возвращаем каталог словарей
    public function getDictionaryCatlog(){
        return DevTools::getPathToPublic().self::DICTIONARY_CATALOG;
    }

    //возвращаем язык
    public function getLang(){
        return self::LANG_RU;
    }

    //возвращаем настройка работы с памятью
    public function getStorageOption(){
        return PHPMORPHY_STORAGE_FILE;
    }

    //возвращаем рабочий экземпляр phpmorphy
    public function getMorphy(){


        if( empty( $this->morphyObj)){

            try {

                $this->morphyObj = new \phpMorphy(
                                    $this->getDictionaryCatlog(),
                                    $this->getLang(),
                                    [
                                        'storage' => $this->getStorageOption()
                                    ]
                                );

            } catch(phpMorphy_Exception $e) {

                die('Error occured while creating phpMorphy instance: ' . $e->getMessage());
            }
        }
        return $this->morphyObj;
    }

    private function callRequire(){
        $path = DevTools::getPathToVendor().self::REQUIRE_PATH_SECOND_PART;
        require_once($path);
    }
        //конструктор
    public function __construct( ){
        $this->callRequire();
    }
}

/*
 ВН ЕД КАЧ МР ОД ПРЕВ
 РД ЕД КАЧ МР ОД ПРЕВ НО
 РД ЕД КАЧ МР ОД ПРЕВ НО
 */