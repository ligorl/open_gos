<?php
namespace Eo\Model;

/**
 * Спец класс для отправки файлов на открытую часть
 *
 */
class SendFile {
    /**
     * <pre>
     *              array ( 'name' =>'test.docx',
     *                      'type' =>'application/octet-stream',
     *                      'tmp_name' => 'X:\tmp\php2819.tmp',
     *                      'error' => 0,
     *                      'size' => 9311,)
     *
     * править на открытой!!! и закрыттой части !!!
     * влияют
     * php.ini
     *          post_max_size
     *          upload_max_filesize
     * nginx.conf
     *          client_max_body_size 100m;
     * легкий
     *          server.max-request-size
     *          server.max-request-field-size 
     * апач
     *          LimitRequestBody         
     * лог
     *          /log/SendFileToOpenServer.log
     *
     * </pre>
     *  @param array $file стандартный элемент массива $_FILE вида :
     *  @param array $data должен содержать элементы:
     *              "path" - путь (без $_SERVER['DOCUMENT_ROOT']) куда сохранять файл (передаем из скрипта на закрытой части т.к. путь может генерироваться динамически)
     *              "name" - с каким именем файл будет сохранен на сервере
     *              'isChange'  - можно заменить (убирает проверку на уже есть)
     *              'onLoad' - JSON [controller,[options]] - то что делаем при успешной отправке
     *
     *  @param int $maxsize, по умолчанию 10МБ(10485760) теперь надо 50М(52428800)
     *
     *  @param array $ext_array, по умолчанию  array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx", 'docm','sig')
     *
     *  @return string возвращает "1" если файл загружен успешно, или одну из 4-х ошибок в текстовом формате.
     *
     */
    static function SendFileToOpenServer($file, $data, $max_size = 80428800, $ext_array = array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls","xml", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx", 'docm','sig')) {
        $logFile = 'public/log/SendFileToOpenServer.log';
        $mailer = new \Eo\Model\SendMail();
        $data["maxsize"]   = $max_size;
        $data["allow_ext"] = implode(",", $ext_array);
        $mailer->mailLog('начинаем ', $logFile);
        if (isset($file) && $file['error'] == 0) {
            $ext  = substr($file['name'], 1 + strrpos($file['name'], "."));
            $name = explode('.', $file['name']);
            if ($file['size'] < $max_size && $file['size'] != 0) {
                if (in_array(mb_strtolower($ext), $ext_array)) {
                    $mailer->mailLog('отправляем файл '.$file['name'].' '.$file['size'], $logFile);
                    $app = $_SERVER['HTTP_HOST'];
                    //$FileConfig = new \Zend\Config\Config(include __DIR__ . '../../../../../../config/autoload/fileconfig.php');
                    //$app        = $FileConfig->filelink->realiseLink;
                    if ($_SERVER['HTTP_HOST'] == 'f11.aln') {
                        $app        = 'f11.mon.prostoy.ru';
                    }
                    if ( $_SERVER['HTTP_HOST'] == 'f11.mon.prostoy.ru' ) {
                        $app        = 'localhost';
                    }                    
                    if(!empty($data['debug'])){
                        $app = $data['debug'];
                    }
                    $mailer->mailLog('отправляем с сервера '.$_SERVER['HTTP_HOST'], $logFile);
                    $mailer->mailLog('отправляем на сервер '.$app, $logFile);
                    $url          = 'http://' . $app . '/eo/declaration/file-upload-service';
                    $ch           = curl_init();
                    $cfile        = curl_file_create($file['tmp_name'], $file['type'], $file['name']);
                    $data['file'] = $cfile;
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_NOBODY, false);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    //curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    //curl_setopt($ch,CURLOPT_HTTPHEADER,array("Expect:"));
                    $result       = curl_exec($ch);
                    $mailer->mailLog('результат '.$result, $logFile);
                    curl_close($ch);
                    /*
                    //сохранить в файл
                    $path = '/srv/www/work/f11_mon/'.$data["path"].'/';
                    @mkdir($path,0777,true);
                    $filePath = $path.$data["name"];
                    @copy($file['tmp_name'], $filePath);
                    $result = 1;
                    */
                    return $result;
                } else {
                    return $mailer->mailLog("Недопустимый формат файла.", $logFile);
                }
            } else {
                return  $mailer->mailLog("Размер файла превышает допустимый размер, или файл пустой", $logFile);
            }
        } else {
            return  $mailer->mailLog("Ошибка загрузки файла", $logFile);
        }
        return  $mailer->mailLog("Возникла ошибка при загрузке файла.", $logFile);
    }
    
    static function deleteFile($path) {
        $app = $_SERVER['HTTP_HOST'];
        //$FileConfig = new \Zend\Config\Config(include __DIR__ . '../../../../../../config/autoload/fileconfig.php');
        //$app        = $FileConfig->filelink->realiseLink;
        if ( $_SERVER['HTTP_HOST'] == 'f11ron.aln' ) {
            $app        = 'f11.mon.prostoy.ru';
        }
        if ( $_SERVER['HTTP_HOST'] == 'f11.mon.prostoy.ru' ) {
            $app        = 'localhost';
        }
        $path = str_replace("/public","", $path);
        $path = str_replace("/uploads","", $path);
        $url          = 'http://' . $app . '/eo/declaration/delete-file';
        $ch           = curl_init();
        $data['path'] = $path;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result       = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}
