<?php

namespace Eo\Model;

class ExportXmlTempCertificate extends ExportXml
{

    public function getCodeUslugi()
    {
        return '3';
    }

    private $codeReasons = array(
        'Temporary_Direction'           => '2', //Реорганизация организации в форме  разделения
        'Temporary_Extraction' => '3', //Реорганизация организации в форме выделения
    );

    public function getData()
    {
        $currentApplication = $this->currentApplication;
        $MainOrg=$this->MainOrg;
        $arrayOrg = $this->arrayOrg;
        $resultArray = array();
        $resultArray['code'] = $arrayOrg['Id'];
        $resultArray['code_ref']  = '';
        $resultArray['applicant'] = 'true';
        //добовляем сведения о реоргонизованных учреждениях
        $resultArrayOut=$this->addReorgCert($resultArrayOut);
        //добавляем общие данные главного филиала
        $resultArray = $this->addFilialeMainData($arrayOrg, $resultArray);
        //Добавление данных о общественных лицензиях
        $resultArray = $this->addAccreditationOf($arrayOrg, $resultArray);
        //Добавляем сведения об учредителях
        //$resultArray = $this->getUchrArray($arrayOrg,$resultArray);
        //Добавляем образовательные програмы главного вуза
        $resultArray=$this->addMainEductionsProgram($resultArray);
        //перекидываем массив
        $resultArrayOut['institutions']['institution'] = array();
        $resultArrayOut['institutions']['institution'][] = $resultArray;
        $resultArray = array();
        //филиалы
        $resultArrayOut = $this->addBranchData($arrayOrg, $resultArrayOut);
        //Прикрепленныей файлы
        //$resultArrayOut = $this->addAttachment($resultArrayOut);
        return $resultArrayOut;
    }

    /*
     * Сведения о сертификатах вузах на новые вузы
     */
    public function addReorgCert($resultArray = array())
    {
        $reorgOrganNames = array();
        $isga_mtm_Application_Organizations = $this->currentApplication
          ->selectComplicated("isga_mtm_Application_Organizations", array('fk_eiisEducationalOrganization'),
                              array('fk_isgaApplication' => $this->currentApplication->getField('Id')))
          ->toArray();
        if ($isga_mtm_Application_Organizations) {
            $reorgOrganIDs = array_column($isga_mtm_Application_Organizations, 'fk_eiisEducationalOrganization');
            $reorgOrgans = $this->currentApplication
              ->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName', 'Inn', 'Kpp'),
                                  array('Id' => $reorgOrganIDs))
              ->toArray();
            $cert = $this->currentApplication
              ->selectComplicated("isga_Certificates",
                                  ['max' => new \Zend\Db\Sql\Expression('max(DateIssue)'),
                  'Id',
                  'RegNumber',
                  'SerialNumber',
                  'FormNumber',
                  'DateIssue',
                  'DateEnd',
                  'fk_eiisControlOrgan',
                  'fk_isgaCertificateStatus',
                  'fk_eiisEducationalOrganization'
                ],
                array('fk_eiisEducationalOrganization' => $reorgOrganIDs,
                //'fk_isgaCertificateSupplementStatus'=>$statusId['Id']
                ), array('fk_eiisEducationalOrganization')
              )
              ->toArray();
            if(!empty($cert)) {
                $organ = array_column($cert, 'fk_eiisControlOrgan');
                $organ = $this->currentApplication->selectComplicated(
                    'eiis_ControlOrgans', array('Id', 'Name'), array('Id' => $organ)
                  )->toArray();
                $organ = array_column($organ, 'Name', "Id");
                $status = array_column($cert, 'fk_isgaCertificateStatus');
                $status = $this->currentApplication->selectComplicated(
                    'isga_CertificateStatuses', array('Id', 'Name'), array('Id' => $status)
                  )->toArray();
                $status = array_column($status, 'Name', 'Id');
            }
            $certId = array();
            foreach ($cert as $key => $one) {
                $cert[$key]['CertificateStatus'] = $status[$one['fk_isgaCertificateStatus']];
                $cert[$key]['ControlOrgans'] = $organ[$one['fk_eiisControlOrgan']];
                $certId[$one['fk_eiisEducationalOrganization']] = $key;
            }

            foreach ($reorgOrgans as $key => $one) {
                $arrayTemp = array(
                    'FullName'          => $one['FullName'],
                    'Kpp'               => $one['Kpp'],
                    'Inn'               => $one['Inn'],
                    'RegNumber'         => isset($certId[$one['Id']]) ? $cert[$certId[$one['Id']]]['RegNumber'] : '',
                    'DateIssue'         => isset($certId[$one['Id']]) ? $cert[$certId[$one['Id']]]['DateIssue'] : '',
                    'DateIssue'         => isset($certId[$one['Id']]) ? $cert[$certId[$one['Id']]]['DateIssue'] : '',
                    'SerialNumber'      => isset($certId[$one['Id']]) ? $cert[$certId[$one['Id']]]['SerialNumber'] : '',
                    'FormNumber'        => isset($certId[$one['Id']]) ? $cert[$certId[$one['Id']]]['FormNumber'] : '',
                    'ControlOrgans'     => isset($certId[$one['Id']]) ? $cert[$certId[$one['Id']]]['ControlOrgans'] : '',
                    'CertificateStatus' => isset($certId[$one['Id']]) ? $cert[$certId[$one['Id']]]['CertificateStatus'] : '',
                );
                $resultArray['Insts_reorg']['Inst_reorg'][] = $arrayTemp;
            }
        }
        return $resultArray;
    }

}