<?php

namespace Eo\Model\ExportDocument;

use Model\Entities\eiisEducationalOrganizations;


class ExportCertificate extends ExportDocument {

    function getType()
    {
        return  'Certificate';
    }

    public function getData()
    {
        //$applicationsClass  = $this->applicationsClass;
        $currentApplication = $this->currentApplication;
        $monthArray         = $this->monthArray;
        $resultArray        = $this->resultArray;
        $isShortProcedure   = $currentApplication->isShortPath();        

        $certificate =$this->getCertificate();
        $mainOrg     = $this->mainOrg;
        $arrayOrg    = $mainOrg->toArray();
        $dateStart   = strtotime($certificate['DateIssue']);
        $moreInfo = $this->moreInfo;
        if(isset($moreInfo[$arrayOrg['Id']])){
            $moreInfoMain = $moreInfo[$arrayOrg['Id']]->toArray();
        }elseif(isset($moreInfo[0])){
            $moreInfoMain = $moreInfo[0]->toArray();
        }else{
            $moreInfoMain = array('Information'=>'','Email'=>'');
        }
        $dateEnd   = strtotime($certificate['DateEnd']);
/*
        ${fieldDEDay}  
        ${fieldDEMount}
        ${fieldDEYear}
*/
        
        $orgData = new \Model\Entities\isgaEducationalOrganizationData();
        $orgData = $orgData->getById($arrayOrg['Id'],'fk_eiisEducationalOrganization');
        $phone = '';
        if(!empty($orgData)){
            $phone = $orgData->getFieldOrSpace('Phones');
            $emailData = $orgData->getFieldOrSpace('Emails');
        }
        if(empty($phone)){
            $phone =  $this->ifNotEmpty($arrayOrg['Phones']);
        }
        
        //адрес брать из какогото хитрого реестра
        $orgAdres = '';
        $emailData = '';
        $orgMtmClass = new \Model\Entities\isgamtmApplicationBranches();
        $orgMtmClass = $orgMtmClass->getByWhere(array(
            'fk_isgaApplication' => $currentApplication->getId(),
            'fk_eiisEOBranch'    => $arrayOrg['Id'],
        ));
        if( !empty($orgMtmClass) ){
           $orgAdres =  $orgMtmClass->getFieldSafe('ApplicationAdres');
        }
        if( empty($orgAdres) ){
            $orgAdres = $arrayOrg['LawAddress'];
        }                

        //колдуем с адресом почты        
        if( empty($emailData) ){
            $emailMain = $this->ifNotEmpty($arrayOrg['Mails']);
        }
        else{
            $emailMain = $emailData;
        }        
        
        $resultArray['field1']  = date('d', $dateStart);
        $resultArray['field2']  = $monthArray[date('n', $dateStart) ];
        $resultArray['field3']  = date('Y', $dateStart);
        $resultArray['field4']  = $certificate['RegNumber'];
        $resultArray['field4']  = empty($resultArray['field4'])?'':$resultArray['field4'];
        $resultArray['field5']  = $certificate['SerialNumber'];
        $resultArray['field5']  = empty($resultArray['field5'])?'':$resultArray['field5'];
        $resultArray['field6']  = $certificate['FormNumber'];
        $resultArray['field6']  = empty($resultArray['field6'])?'':$resultArray['field6'];
        $resultArray['fieldFullName']  = $arrayOrg['FullName'] 
                . ((empty($arrayOrg['ShortName'])|| $arrayOrg['ShortName']=='NULL')?'':' (' . $arrayOrg['ShortName'].')' );
        $resultArray['fieldLawAddress']  = $orgAdres;
        $resultArray['fieldGosRegNum']  = $arrayOrg['GosRegNum'];
        $resultArray['fieldInn']  = $arrayOrg['Inn'];
        $resultArray['fieldKpp']  =  $this->ifNotEmpty($arrayOrg['Kpp']);
        $resultArray['fieldDEDay']  = date('d', $dateEnd);
        $resultArray['fieldDEMount']  = $monthArray[date('n', $dateEnd) ];
        $resultArray['fieldDEYear']  = date('Y', $dateEnd);                       
        
        $resultArray['fieldSecretGosNubmer']  = $this->getDetailsLicenseWorkUsingInformationSecret($moreInfo, $arrayOrg['Id']);
        $resultArray['fieldPhones']  =  $phone;
        $resultArray['fieldMails']  =  $emailMain;
        //$resultArray['fieldWww']  = $this->ifNotEmpty($arrayOrg['Www']);
        $resultArray['fieldWww']  = $orgData->getFieldOrSpace('Site');
        $resultArray['fieldI'] = $this->ifActive($moreInfoMain['Information']);
        $resultArray['fieldInformationEmail'] = $this->conditionSymbolActive($moreInfoMain['Information']) ? $moreInfoMain['Email'] : ' ';
        $resultArray['fieldINo'] = $this->ifNotActive($moreInfoMain['Information']);
        $resultArray['fieldChargePosition'] =  $this->getChargePositionTitle();
        $resultArray['fieldFIOContactManager'] =  $this->ifNotEmpty($this->getFIOContactManager($arrayOrg));
        $resultArray = $this->dateParse($resultArray);
        $resultArray['shtrihKod'] = $this->getShtrihKod($currentApplication->getField('Barcode'),                                                                                      $currentApplication->getField('fk_isgaApplicationStatus'));
        //$resultArray['fieldReason'] = $this->getApplicationReason('AblativeName');
        $resultArray['fieldReason'] = $this->getApplicationReasonNames();
        $resultArray['blockMainTable'] = array();
/*
        $applicationPrograms = $currentApplication
            ->selectComplicated(
              "isga_ApplicationPrograms",
                array(
                    'fk_eiisLicensedPrograms',
                    'yearStartEdu',
                    'termEdu',
                    'fullTimeCount',
                    'partTimeCount',
                    'correspondenceCount',
                    'familyCount',
                    'networkForm',
                    'remoteTechnology',
                    'publicAccreditation',
                    'fk_isgaEnlargedGroupSpecialities',                    
                    'fk_eiisEduProgramTypes',
                ), 
                array(
                    'fk_isgaApplication' => $currentApplication
                    ->getField('Id'),
                    'fk_isgaEOBranches'  => $arrayOrg['Id'])
            )->toArray();*/
        $applicationPrograms=$this->getApplicationPrograms($arrayOrg['Id']);
        //$newArrApplicationPrograms = array();
        //foreach ($applicationPrograms as $oneAplicationProgram) {
        //    $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
        //}
        $programsIds = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
        $programsIds=array_filter($programsIds);
        if (count($programsIds) > 0) {
            /*
            $programs = $currentApplication->selectComplicated(
                    "eiis_LicensedPrograms", 
                    array(
                        'Id',
                        'Code',
                        'Name',
                        'fk_eiisEduProgram',
                        'fk_eiisEduProgramType'
                    ), 
                    array('Id' => $programsIds)
                )->toArray();

            $programTypeIds   = array_column($programs, 'fk_eiisEduProgramType');
            $eduProgramIds    = array_column($programs, 'fk_eiisEduProgram');
            $programsTypes    = $currentApplication->selectComplicated("eiis_EduProgramTypes", array('Id', 'Name'), array('Id' => $programTypeIds))->toArray();
            $arrayTypes       = array_column($programsTypes, 'Name', 'Id');
            $eduPrograms      = $currentApplication->selectComplicated("eiis_EduPrograms", array('Id', 'fk_eiisGS'), array('Id' => $eduProgramIds))->toArray();
            $arrayEduPrograms = array_column($eduPrograms, 'fk_eiisGS', 'Id');
            $isgaGSIds        = array_column($eduPrograms, 'fk_eiisGS');
            $isgaGSArray      = $currentApplication->selectComplicated("isga_GroupSpecialities", array('Id', 'Name', 'Code', 'fk_isgaEGS'), array('Id' => $isgaGSIds))->toArray();
            $isgaGSIdEGS      = array_column($isgaGSArray, 'fk_isgaEGS', 'Id');
            $isgaEGSIds       = array_column($isgaGSArray, 'fk_isgaEGS');
            $isgaEGSArray     = $currentApplication->selectComplicated("isga_EnlargedGroupSpecialities", array('Id', 'Name', 'Code'), array('Id' => $isgaEGSIds))->toArray();
            $isgaEGSArrayName = array_column($isgaEGSArray, 'Name', 'Id');
            $isgaGSArrayName  = array_column($isgaGSArray, 'Name', 'Id');
            $isgaEGSArrayCode = array_column($isgaEGSArray, 'Code', 'Id');
            $isgaGSArrayCode  = array_column($isgaGSArray, 'Code', 'Id');

            //учебные программы
            $i = 0;
            foreach ($programs as $oneProgram) {
                $resultArray['table1'][$i]['countcol']=$i+1;
                $resultArray['table1'][$i]['tabfield1'] = $oneProgram['Name'];
                $resultArray['table1'][$i]['tabfield2'] = $arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                $resultArray['table1'][$i]['tabfield3'] = $isgaEGSArrayCode[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                $resultArray['table1'][$i]['tabfield4'] = $isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                $resultArray['table1'][$i]['tabfield5'] = $this->getCodeProgram($oneProgram);
                $resultArray['table1'][$i]['tabfield6'] = $oneProgram['Name'];
                $resultArray['table1'][$i]['tabfield7'] = $newArrApplicationPrograms[$oneProgram['Id']]['yearStartEdu'];
                $resultArray['table1'][$i]['tabfield8'] = $newArrApplicationPrograms[$oneProgram['Id']]['termEdu'];

                $resultArray['table1'][$i]['tabfield9']  = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['fullTimeCount']);
                $resultArray['table1'][$i]['tabfield10'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['partTimeCount']);
                $resultArray['table1'][$i]['tabfield11'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['correspondenceCount']);
                $resultArray['table1'][$i]['tabfield12'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgram['Id']]['familyCount']);

                $resultArray['table1'][$i]['tabfield13'] = ($newArrApplicationPrograms[$oneProgram['Id']]['networkForm'] == 1 ? 'Да' : "Нет");
                $resultArray['table1'][$i]['tabfield14'] = ($newArrApplicationPrograms[$oneProgram['Id']]['remoteTechnology'] == 1 ? 'Да' : "Нет");
                $resultArray['table1'][$i]['tabfield15'] = ($newArrApplicationPrograms[$oneProgram['Id']]['publicAccreditation'] == 1 ? 'Да' : "Нет");
                ++$i;
            }
            */
            $resultArray['table1'] = $this->getProgramToTableArray($programsIds, /*$newArrApplicationPrograms*/$applicationPrograms , $isShortProcedure );
        }
        //блок филиала, если программа
        //${BlockMainTable}  ${/blockMainTable}
        if( !empty($resultArray['table1']) ){
            $resultArray['blockMainTable'][0]['table1'] = $resultArray['table1'] ;
        }
        unset( $resultArray['table1'] );
        

        //филиалы
        $branchesApplication = $currentApplication->selectComplicated("isga_mtm_Application_Branches", array('fk_eiisEOBranch','ApplicationAdres'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        $j                   = 0;
        $educationOrganization = new eiisEducationalOrganizations();
        $mainOrgId=$mainOrg->getId();
        foreach ($branchesApplication as $oneBranch) {
            // пропускаем главный вуз, чтобы
            if ($oneBranch['fk_eiisEOBranch'] === $mainOrgId) {
                continue;
            }
            $mainOrg                               = $currentApplication->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $oneBranch['fk_eiisEOBranch']));
            $arrayOrg                              = $mainOrg->toArray();
            $educationOrganization->setField('Id', $arrayOrg['Id']);
            $educationalOrganizationDataRes = $educationOrganization->getEducationalOrganizationData();
            $educationalOrganizationData = ($educationalOrganizationDataRes !== false) ? $educationalOrganizationDataRes->toArray() : array();
            
            $phone = '';
            $emailData = '';
            if(!empty($educationalOrganizationDataRes)){
                //$phone =  $this->getPrhoneFaxeseducationalOrganizationData($educationalOrganizationData);
                $phone = $educationalOrganizationDataRes->getFieldOrSpace('Phones');
                $emailData = $educationalOrganizationDataRes->getFieldOrSpace('Emails');
            }
            if(empty($phone) && !empty($arrayOrg['Phones']) && strtolower($arrayOrg['Phones'])!='null'){
                $phone =  $arrayOrg['Phones'];
            }
            
            //адрес ввлиниваем из связной таблицы
            $orgAdres = '';
            if( !empty($oneBranch['ApplicationAdres']) ){
               $orgAdres =  $oneBranch['ApplicationAdres'];
            }
            if( empty($orgAdres) ){
                $orgAdres = $arrayOrg['LawAddress'];
            }            
            
            //колдуем с адресом почты        
            if( empty($emailData) ){
                $emailData = $this->ifNotEmpty($arrayOrg['Mails']);
            }                               
                        
            $resultArray['branches'][$j]['field1'] = $arrayOrg['FullName'] 
                    . ((empty($arrayOrg['ShortName'])|| $arrayOrg['ShortName']=='NULL')?'':' (' . $arrayOrg['ShortName'].')' );
            $resultArray['branches'][$j]['field2'] = $orgAdres;
            $resultArray['branches'][$j]['field3'] =  $this->ifNotEmpty($arrayOrg['Kpp']);
            $resultArray['branches'][$j]['field4'] = $this->getDetailsLicenseWorkUsingInformationSecret($moreInfo, $arrayOrg['Id']);
            $resultArray['branches'][$j]['field4']  = empty( $resultArray['branches'][$j]['field4'] ) ? '' : $resultArray['branches'][$j]['field4'] ;
            $resultArray['branches'][$j]['field5'] =  $phone;
            $resultArray['branches'][$j]['field5']  = empty( $resultArray['branches'][$j]['field5'] ) ? '' : $resultArray['branches'][$j]['field5'] ;
            $resultArray['branches'][$j]['field6'] =  $emailData;
            $resultArray['branches'][$j]['field6']  = empty( $resultArray['branches'][$j]['field6'] ) ? '' : $resultArray['branches'][$j]['field6'] ;
            //$resultArray['branches'][$j]['field7'] = $this->getWwwOrganizationData($educationalOrganizationData)   ;
            $resultArray['branches'][$j]['field7'] = ($educationalOrganizationDataRes) ? $educationalOrganizationDataRes->getFieldOrSpace('Site'): '';
            $resultArray['branches'][$j]['blockFilialTable'] = array();
            /*
            $applicationPrograms                   = $currentApplication
                    ->selectComplicated(
                            "isga_ApplicationPrograms",
                            array(
                                'fk_eiisLicensedPrograms', 
                                'yearStartEdu', 
                                'termEdu', 
                                'fullTimeCount', 
                                'partTimeCount', 
                                'correspondenceCount', 
                                'familyCount', 
                                'networkForm', 
                                'remoteTechnology', 
                                'publicAccreditation',
                                'fk_isgaEnlargedGroupSpecialities',                    
                                'fk_eiisEduProgramTypes',
                            ), 
                            array(
                                'fk_isgaApplication' => $currentApplication->getField('Id'), 
                                'fk_isgaEOBranches' => $oneBranch['fk_eiisEOBranch']
                            )
                        );                          
            $applicationPrograms                   = $applicationPrograms->toArray();
            */
            $applicationPrograms = $this->getApplicationPrograms($oneBranch['fk_eiisEOBranch']);
            $applicationPrograms=array_filter($applicationPrograms);
            if (count($applicationPrograms) > 0) {
                $programsIds      = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
                $programs         = $currentApplication->selectComplicated("eiis_LicensedPrograms", array('Id', 'Code', 'Name', 'fk_eiisEduProgram', 'fk_eiisEduProgramType'), array('Id' => $programsIds))->toArray();

                if (count($programs) > 0){
                    $resultArray['branches'][$j]['table1'] = $this->getProgramToTableArray($programsIds, /*$newArrApplicationPrograms*/$applicationPrograms , $isShortProcedure ); 
                }
                //${BlockFilialTable}  ${/BlockFilialTable}
                if( !empty($resultArray['branches'][$j]['table1']) ){
                    $resultArray['branches'][$j]['blockFilialTable'][0]['table1'] = $resultArray['branches'][$j]['table1'];
                }
                unset($resultArray['branches'][$j]['table1']);

                }
            ++$j;
        }        
        //print_r( $resultArray );
        //die;
        return $resultArray;        
    }

}
