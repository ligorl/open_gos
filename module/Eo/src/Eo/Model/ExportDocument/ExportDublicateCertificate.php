<?php

namespace Eo\Model\ExportDocument;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExportDublicateCertificate
 *
 * @author sany
 */
class ExportDublicateCertificate extends ExportDocument {
    
    function getType()
    {
        return  'DublicateCertificate';
    }

    public function getData()
    {
        //$applicationsClass        = $this->applicationsClass ;
        $currentApplication       = $this->currentApplication ;
        $monthArray               = $this-> monthArray ;
        $resultArray              = $this->resultArray;

        $mainOrg                  = $this->mainOrg;
        $arrayOrg                 = $mainOrg->toArray();
        $moreInfo = $this->moreInfo;        
            
        $orgData = new \Model\Entities\isgaEducationalOrganizationData();
        $orgData = $orgData->getById($arrayOrg['Id'],'fk_eiisEducationalOrganization');
        $phone = '';
        if(!empty($orgData)){
            $phone = $orgData->getFieldOrSpace('Phones');
            $emailData = $orgData->getFieldOrSpace('Emails');
        }
        if(empty($phone)){
            $phone =  $this->ifNotEmpty($arrayOrg['Phones']);
        }                            

        //колдуем с адресом почты        
        if( empty($emailData) ){
            $emailMain = $this->ifNotEmpty($arrayOrg['Mails']);
        }
        else{
            $emailMain = $emailData;
        }        
        
        $resultArray['fieldFullName']  = $arrayOrg['FullName'] 
                . ((empty($arrayOrg['ShortName'])|| $arrayOrg['ShortName']=='NULL')?'':' (' . $arrayOrg['ShortName'].' (' );
        $resultArray['fieldLawAddress']  = $arrayOrg['LawAddress'];
        $resultArray['fieldGosRegNum']  = $arrayOrg['GosRegNum'];
        $resultArray['fieldInn']  = $arrayOrg['Inn'];
        $resultArray['fieldKpp'] = $arrayOrg['Kpp'];
        $resultArray['fieldSecretGosNubmer'] = $this->getDetailsLicenseWorkUsingInformationSecret($moreInfo, $arrayOrg['Id']);
        //$resultArray['fieldReason']    = $this->getApplicationReason('AblativeName');
        $resultArray['fieldReason']    = $this->getApplicationReasonNames();
        $resultArray['fieldPhones']  = $phone;
        $resultArray['fieldMails']  = $emailMain;
        //$resultArray['fieldWww']  = $this->ifNotEmpty($arrayOrg['Www']);
        $resultArray['fieldWww']  = $orgData->getFieldOrSpace('Site');
        $resultArray['fieldChargePosition'] = $this->getChargePositionTitle();
        $resultArray['fieldFIOContactManager'] = $this->getFIOContactManager($arrayOrg);
        $resultArray=$this->dateParse($resultArray);
        $resultArray['shtrihKod'] = $this->getShtrihKod($currentApplication->getField('Barcode'), $currentApplication->getField('fk_isgaApplicationStatus'));

        // филиалы
        $branchesApplication         = $currentApplication->selectComplicated("isga_CertificateSupplements", array(), array('fk_isgaCertificate' => $currentApplication->getField("fk_isgaPreviousCertificate")))->toArray();

        $i = 0;
        $mainOrgId=  $arrayOrg['Id'];
        foreach ($branchesApplication as $oneBranch) {
            // пропускаем главный вуз, чтобы
            $oneBranch['fk_eiisEducationalOrganization']=str_replace('-','',strtoupper($oneBranch['fk_eiisEducationalOrganization']));
            if (empty($oneBranch['fk_eiisEducationalOrganization']) || $oneBranch['fk_eiisEducationalOrganization'] === $mainOrgId) {
                continue;
            }
            $mainOrg = $currentApplication
              ->getLinkedItem(
                'eiis_EducationalOrganizations',
                  array('Id' => $oneBranch['fk_eiisEducationalOrganization'])
              
            );
            
            $arrayOrg = $mainOrg->toArray();
            $resultArray['branches'][$i]['field1'] = $oneBranch['EOFullName'] 
                    . ((empty($oneBranch['EOShortName'])|| $oneBranch['EOShortName']=='NULL')?'':' (' . $oneBranch['EOShortName'].')' );
            $resultArray['branches'][$i]['field2'] = $oneBranch['EOAddressL'];
            $resultArray['branches'][$i]['field3'] =  $this->ifNotEmpty($arrayOrg['Kpp']);
            $i++;
        }

        return $resultArray;
    }

}
