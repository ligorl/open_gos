<?php

namespace Eo\Model\ExportDocument;

class ExportDocument
{

    static function getDocumentByIdApplication($id)
    {
        //$applicationsClass = new \Model\Gateway\EntitiesTable('isga_Applications');
        //$currentApplication = $applicationsClass->getEntityById($id);
        $currentApplication = new \Model\Entities\isgaApplications($id);
        //$applicationType = $currentApplication->getTypeCode();
        $applicationType = $currentApplication->getType()->toArray();
        //$type =  $currentApplication->getTypeCode();
        $type = $applicationType ['Code'];
        $name = $applicationType ['NameAccusativeCase'];
        switch ($type) {
            case 'DublicateCertificate':
                return new ExportDublicateCertificate($currentApplication, $name);
                break;
            case 'ReAccreditation':
            case 'Certificate':
                return new ExportCertificate($currentApplication, $name);
                break;
            case 'Accreditation':
                return new ExportAccreditation($currentApplication, $name);
                break;
            case 'TempCertificate':
                return new ExportTempCertificate($currentApplication, $name);
                break;
        }
        return false;
    }

    function getData()
    {

    }

    function getType()
    {

    }

    protected $applicationsClass;
    protected $currentApplication;
    protected $monthArray = array('год', "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
    protected $resultArray = array();
    protected $mainOrg;
    protected $moreInfo;

    function __construct($currentApplication, $name)
    {
        $this->currentApplication = $currentApplication;
        $this->resultArray['templName'] = $name;
        $this->resultArray['templType'] = $this->getType();
        $this->resultArray['dateCreate'] = $currentApplication->getField('DateCreate');
        $this->mainOrg = $this->currentApplication->getEducationalOrganization();
               // $this->currentApplication->getLinkedItem('eiis_EducationalOrganizations',
               //                                                   array('Id' => $this->currentApplication->getField("fk_eiisEducationalOrganization")));
        /*$this->moreInfo = $this->currentApplication->getLinkedItem(
            'isga_ApplicationMoreInfo', array('fk_isgaApplication' => $this->currentApplication->getField("Id"))
        );*/
        $moreInfoData = $this->currentApplication->getLinkedItems(
            'isga_ApplicationMoreInfo', array('fk_isgaApplication' => $this->currentApplication->getField("Id"))
        );
        $moreInfoArr = array();
        if(!empty($moreInfoData)){
            foreach($moreInfoData as $moreInfoItem){
                $idOrg = 0;
                if(!$moreInfoItem->isEmptyField('fk_eiisEducationalOrganization')){
                    $idOrg = $moreInfoItem->getField('fk_eiisEducationalOrganization');
                }
                $moreInfoArr[$idOrg] = $moreInfoItem;
            }
        }
        $this->moreInfo = $moreInfoArr;
    }

    /*
     * возращает перечень таблиц в шаблоне
     * имя таблицы=>имя первого поля
     */
    public function getTablesOut()
    {
        return array('table1' => 'tabfield1');
    }

    public function ifNotEmpty($s)
    {
        $result=((empty($s)|| $s==='NULL')? '' : $s);
        return $result;
    }

    public function ifNotDefis($s)
    {
        return (empty($s)) ? ' - ' : $s;
    }

    public function ifActive($value)
    {
        return $this->writeSymbolActive($this->conditionSymbolActive($value));
    }

    public function ifNotActive($value)
    {
        return $this->writeSymbolActive(!$this->conditionSymbolActive($value));
    }

    protected function conditionSymbolActive($value)
    {
        return $value == 1;
    }

    private function writeSymbolActive($condition)
    {
        return ($condition) ? 'X' : ' ';
    }

    protected function getCodeProgram($program)
    {
        return (array_key_exists('Code', $program)) ? $program['Code'] : '';
    }

    protected function getDetailsLicenseWorkUsingInformationSecret($MoreInfoArr,$idOrg = 0)
    {
        if(isset($MoreInfoArr[$idOrg])){
            $MoreInfo = $MoreInfoArr[$idOrg]->toArray();
        }elseif(isset($MoreInfoArr[0])){
            $MoreInfo = $MoreInfoArr[0]->toArray();
        }else{
            return 'Нет';
        }
        if (empty($MoreInfo['RegNumber'])) {
            return 'Нет';
        }
        $data_reg = '';
        if (!empty($MoreInfo['DateReg'])) {
            $data_reg = " от " . date('d.m.Y', strtotime($MoreInfo['DateReg']));
        }
        $data_end = '';
        if (!empty($MoreInfo['DateReg'])) {
            $data_end = ", действует до " . date('d.m.Y', strtotime($MoreInfo['DateEnd']));
        }
        return "№ {$MoreInfo['RegNumber']}, {$MoreInfo['SiriesNumber']}" . $data_reg . $data_end . ".";
    }

    protected function getRequisitesCertificate($certificate)
    {
        if (empty($certificate)) {
            return '';
        }
        if (!empty($certificate['DateIssue'])) {
            $data_reg = " от " . date('d.m.Y', strtotime($certificate['DateIssue']));
        }
        return "№ {$certificate['RegNumber']}, {$certificate['SerialNumber']}" . $data_reg;
    }

    protected function getPrhoneFaxeseducationalOrganizationData($data)
    {
        $result = array();
        $mas = array('Phones', 'Faxes');
        foreach ($mas as $key) {
            if (array_key_exists($key, $data) && !empty($data[$key])) {
                $result[] = $data[$key];
            }
        }
        return implode(', ', $result);
    }

    protected function getFIOContactManager($arrayOrg)
    {
        //$resuls=$this->ifNotEmpty($arrayOrg['ContactSecondName'])
        //  . ' ' .
        //  $this->ifNotEmpty($arrayOrg['ContactFirstName'])
        //  . ' ' .
        //  $this->ifNotEmpty($arrayOrg['ContactLastName']);
        //if($resuls==='     '){
            $resuls=  $this->ifNotEmpty($arrayOrg['ChargeFio']);
        //}
        return $resuls;
    }

    /*
     * Формирует картинку штрих кода в формате GIF
     */
    public function getShtrihKod($code = null, $fk_isgaApplicationStatus = true)
    {
        if (/*empty($fk_isgaApplicationStatus) ||*/ empty($code)) {
            return '';
        }
        $barcodeOptions = array('text' => abs($code));
        $barcode = new \Zend\Barcode\Object\Ean13($barcodeOptions);
        $rendererOptions = array();
        $renderer = new \Zend\Barcode\Renderer\Image($rendererOptions);
        $image = $renderer->setBarcode($barcode)->draw();

        ob_start();
        imagegif($image);
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    public function dateParse($resultArray = array())
    {
        //$dateCreate = $this->currentApplication->getField('DateCreate');        
        $dateCreate = date('Y-m-d');
        if (($dateCreate = strtotime($dateCreate)) === false) {
            $dateCreate = time();
        }
        $resultArray['fieldDateDay'] = date('d', $dateCreate);
        $resultArray['fieldDateYear'] = date('Y', $dateCreate);
        $resultArray['fieldDateMounth'] = $this->monthArray[(int) date('m', $dateCreate)];
        return $resultArray;
    }

    /*
     * заполнение таблиц в документе
     */
    public function templateMakeTable($tableArray, $table, $templateProcessor)
    {
        foreach ($table as $tableName => $firstField) {
            if (!empty($tableArray[$tableName]) && is_array($tableArray[$tableName])) {
                $templateProcessor->cloneRow($firstField, count($tableArray[$tableName]));
                foreach ($tableArray[$tableName] as $keyRow => $valueRow) {
                    //$valueRow['countcol'] = $keyRow + 1;
                    foreach ($valueRow as $keyFiled => $valueField) {
                        $macro = $keyFiled . '#' . (string) ($keyRow + 1);
                        $templateProcessor->setValue($macro, htmlspecialchars($valueField, ENT_COMPAT, 'UTF-8'), 1);
                    }
                };
            } else {
                //Стираем строки, если для них нет значений.
                $templateProcessor->cloneRow($firstField, 0);
            }
        }
        return $templateProcessor;
    }

    /*
     * Заполнение полей в документе
     */
    public function templateMakeValue($valueArray, $templateProcessor)
    {
        foreach ($valueArray as $key => $val) {
            if (is_string($val) || is_numeric($val)) {
                $templateProcessor->setValue($key, htmlspecialchars($val, ENT_COMPAT, 'UTF-8'), 1);
            }
        }
        return $templateProcessor;
    }

    /*
     * Форирование документа из шаблона
     */
    public function templateMakeDoccument($arrayData, $templateDocPath, $outType = 'docx')
    {
        //создаем временное хранилище
        $tempDir = sys_get_temp_dir() . '/' . uniqid() . '/';
        if (!mkdir($tempDir, 0777, true)) {
            return false;
        }
        ///Берем класс шаблонизатора
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateDocPath);
        //Изменяем штрих код
        if ($outType != 'pdf' || empty($arrayData['shtrihKod'])) {
            $templateProcessor->deleteImage();
        } else
        if (!empty($arrayData['shtrihKod'])) {
            $strihFile = 'image1.gif';
            $strihFilePath = $tempDir . $strihFile;
            file_put_contents($strihFilePath, $arrayData['shtrihKod']);
            $templateProcessor->setImageValue($strihFile, $strihFilePath);
            unset($strihFile);
        }
        //вставляем значения полей
        $templateProcessor = $this->templateMakeValue($arrayData, $templateProcessor);
        //клонируем главную табличку
        if( isset($arrayData['blockMainTable'])){
            if( empty($arrayData['blockMainTable']) ){
                //програм нет
                $templateProcessor->cloneBlock('blockMainTable', 0);
            } else {
                //программы есть
                $templateProcessor->cloneBlock('blockMainTable', count($arrayData['blockMainTable']) );
                //Заполняем таблички.
                $templateProcessor = $this->templateMakeTable(reset($arrayData['blockMainTable']), array_slice($this->getTablesOut(), 0, 1), $templateProcessor);
            }
        }
        if( isset($arrayData['blockMainTable2'])){
            if( empty($arrayData['blockMainTable2']) ){
                //програм нет
                $templateProcessor->cloneBlock('blockMainTable2', 0);
            } else {
                //программы есть
                $templateProcessor->cloneBlock('blockMainTable2', count($arrayData['blockMainTable2']) );
                //Заполняем таблички.
                $templateProcessor = $this->templateMakeTable(reset($arrayData['blockMainTable2']), array_slice($this->getTablesOut(), 1, 1), $templateProcessor);
            }
        }
        //Заполняем филиалы
        if (!empty($arrayData['branches']) && is_array($arrayData['branches'])) {
            $templateProcessor->cloneBlock('BRANCH', count($arrayData['branches']));
            foreach ($arrayData['branches']as $branchKey => $branchValue) {
                //свистопляска с клонированием блоков в филиале
                //из-зи какогото заклина, он кланируются все залпом
                //прилипил эту ...... (вырезано цензурой)
                $blockTable = 'blockFilialTable';
                $blokMaker  = $blockTable.'Marker';
                $blokData   = $blockTable.'Data';   
                //Закидываем маркер
                if( isset($branchValue[$blockTable]) ){
                    $branchValue[$blokData]             = $branchValue[$blockTable];
                    $branchValue[$blockTable]    = '${'.$blokMaker.'}';
                    $branchValue['/'.$blockTable]   = '${/'.$blokMaker.'}';
                }
                
                $blockTable2 = 'blockFilialTable2';
                $blokMaker2  = $blockTable2.'Marker';
                $blokData2   = $blockTable2.'Data';   
                //Закидываем маркер
                if( isset($branchValue[$blockTable2]) ){
                    $branchValue[$blokData2]             = $branchValue[$blockTable2];
                    $branchValue[$blockTable2]    = '${'.$blokMaker2.'}';
                    $branchValue['/'.$blockTable2]   = '${/'.$blokMaker2.'}';
                }
                //вставляем значения полей
                $templateProcessor = $this->templateMakeValue($branchValue, $templateProcessor);
                //клонируем  табличку филиалов         
                if( isset($branchValue[$blokData])){
                    if( empty($branchValue[$blokData]) ){
                        //нет программ у филиала
                        $templateProcessor->cloneBlock($blokMaker, 0);
                    } else {
                        //рисуем табличку с программами
                        $templateProcessor->cloneBlock($blokMaker, count($branchValue[$blokData]) );
                        //Заполняем табличку филиалов.
                        $templateProcessor = $this->templateMakeTable(reset($branchValue[$blokData]), array_slice( $this->getTablesOut(), 0, 1) , $templateProcessor);
                    }
                }            
                //клонируем  табличку филиалов         
                if( isset($branchValue[$blokData2])){
                    if( empty($branchValue[$blokData2]) ){
                        //нет программ у филиала
                        $templateProcessor->cloneBlock($blokMaker2, 0);
                    } else {
                        //рисуем табличку с программами
                        $templateProcessor->cloneBlock($blokMaker2, count($branchValue[$blokData2]) );
                        //Заполняем табличку филиалов.
                        $templateProcessor = $this->templateMakeTable(reset($branchValue[$blokData2]), array_slice( $this->getTablesOut(), 1, 1), $templateProcessor);
                    }
                }   
            } 
        } else {
            $templateProcessor->cloneBlock('BRANCH', 0);
        }
        //Сохраняем полученый документ и готовим к отправке
        $fileTempPath = $tempDir . 'out.docx';
        $templateProcessor->saveAs($fileTempPath);
        $result = '';
        if (file_exists($fileTempPath)) {
            $outTypes = array('pdf', 'doc', 'odt');
            if (in_array($outType, $outTypes)) {
                $s = '/usr/bin/libreoffice5.0 '
                  . ' --headless --convert-to ' . $outType . ' --nofirststartwizard '
                  . ' --nologo --norestore --invisible --nofirststartwizard'
                  . ' --outdir ' . $tempDir
                  . ' ' . $fileTempPath
                  . '  ';                
                \Eo\Model\Templater::exec( $s );
                $fileTempConvert = $tempDir . 'out.' . $outType;
                if (file_exists($fileTempConvert)) {
                    $result = file_get_contents($fileTempConvert);
                    @unlink($fileTempConvert);
                }
            } else {
                $result = file_get_contents($fileTempPath);
            }
            unset($outTypes);
            @unlink($fileTempPath);
        }
        unset($fileTempPath);
        //Удаляем картинку штрихкода
        if (isset($strihFilePath)) {
            if (file_exists($strihFilePath)) {
                @unlink($strihFilePath);
            }
            unset($strihFilePath);
        }
        if (is_dir($tempDir)) {
            @rmdir($tempDir);
        }
        unset($tempDir);
        return $result;
    }

    protected $certificate = null;

    public function getCertificate()
    {
        if ($this->certificate === null) {
            if($this->currentApplication->isEmptyField('fk_isgaPreviousCertificate')){
                return null;
            }
            $this->certificate = $this->currentApplication
              ->getLinkedItem(
                'isga_Certificates', array('Id' => $this->currentApplication->getField("fk_isgaPreviousCertificate"))
              )
              ->toArray();
        }
        return $this->certificate;
    }

    protected $applicationReason;

    public function getApplicationReason($param = null)
    {
        if (!isset($this->applicationReason)) {
            $this->applicationReason = $this->currentApplication->getLinkedItem(
              'isga_ApplicationReasons',
              array(
                'Id' => $this->currentApplication
                  ->getField("fk_isgaApplicationReason")
              )
            );
        }
        if ($param == null) {
            return $this->applicationReason->toArray();
        } else {
            return $this->applicationReason->getField($param);
        }
    }

    public function getApplicationReasonNames()
    {
        $application = $this->currentApplication;
        $reasonsArray = $application->getReasonArray();
        $reasonsAblativeNameArray = array_column($reasonsArray, 'AblativeName');
        $result = '';
        $i=1;
        foreach ($reasonsAblativeNameArray as $oneItem){
            if($i!=1){
              $result .= '; ';
            }
            $result .= $oneItem;
            $i++;
        }
        return $result;
    }

    public function getApplicationPrograms($param)
    {
        $applicationPrograms = $this->currentApplication
            ->selectComplicated(
                "isga_ApplicationPrograms",
                array(
                    //'fk_eiisLicensedPrograms',
                    //'yearStartEdu',
                    //'termEdu',
                    //'fullTimeCount',
                    //'partTimeCount',
                    //'correspondenceCount',
                    //'familyCount',
                    //'networkForm',
                    //'remoteTechnology',
                    //'publicAccreditation'
                ),
                array(
                    'fk_isgaApplication' => $this->currentApplication
                    ->getField('Id'),
                    'fk_isgaEOBranches'  => $param,
                    'PrLish' => 0,
                )
            )->toArray();
        return $applicationPrograms;
    }

    public function getWwwOrganizationData($organizationData){
        if(!empty($organizationData['Www']) && $rgData['Www']!=='NULL'){
            return $organizationData['Www'];
        }
        return '';
    }

    public function getProgramToTableArray($licenseProgramsIds, $newArrApplicationPrograms , $isShortProcedure = false ) {
        $currentApplication = $this->currentApplication;
        $licenseProgramAll = new \Model\Entities\eiisLicensedPrograms();
        $licenseProgramAll = $licenseProgramAll->getAllByWhereParseId(array(
            'Id' => $licenseProgramsIds
        ));

        $licenseProgramAll = array_change_key_case($licenseProgramAll, CASE_LOWER);
            //$licenseProgramAll = $currentApplication->getLinkedItems(
            //        "eiis_LicensedPrograms",
            //        array('Id' => $licenseProgramsIds)
            //)->getObjectsArray();


            //учебные программы
            $i = 0;
            //foreach ($licenseProgramAll as $oneProgram) {
            foreach ($newArrApplicationPrograms as $appProgram) {
                $licenseProgramsId = $appProgram['fk_eiisLicensedPrograms'];
                $licenseProgramsId = strtolower($licenseProgramsId);
                $oneProgram = $licenseProgramAll[$licenseProgramsId];

                $oneProgramtId=$oneProgram->getId();
                //$appProgram=$newArrApplicationPrograms[$oneProgramtId];
                if( $isShortProcedure ){
                    $oneProgramName = '';
                }
                else{
                    $oneProgramName = empty($appProgram['Name'])?$oneProgram->getField('Name'):$appProgram['Name'];
                }
                if(empty($appProgram['fk_eiisEduProgramTypes'])){
                    $programType=$oneProgram->getEduProgramType();
                }else{
                    $programType=$oneProgram->getEduProgramType($appProgram['fk_eiisEduProgramTypes']);
                }

                if(empty($appProgram['fk_isgaEnlargedGroupSpecialities'])){
                    $ugs=$oneProgram->getUgs();
                }else{
                    $ugs=$oneProgram->getUgs($appProgram['fk_isgaEnlargedGroupSpecialities']);
                }
                if( empty($ugs) ){
                    $ugs = new \Model\Entities\isgaEnlargedGroupSpecialities();
                }

                if(!empty(/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['termEdu'])){
                    $period = /*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['termEdu'];
                }else{
                    $period = '';//$oneProgram->getFieldOrSpace('Period');
                }

                $oneProgramCode=$oneProgram->getField('Code');
                $oneProgramCode=empty($oneProgramCode)?"":$oneProgramCode;
                $programCode = $ugs->getFieldSafe('Code');
                if( strtolower($programCode) == 'void' ){
                    $programCode = ' ';
                }

                $resultArray[$i]['countcol']=$i+1;
                $resultArray[$i]['tabfield1'] = $oneProgramName;
                $resultArray[$i]['tabfield2'] = $oneProgram->getEduLevelPrintName();
                $resultArray[$i]['tabfield3'] = $programCode;
                $resultArray[$i]['tabfield4'] = $ugs->getFieldSafe('Name');
                $resultArray[$i]['tabfield5'] = $oneProgramCode;
                $resultArray[$i]['tabfield6'] = $oneProgram->getFieldSafe('Name');
                $resultArray[$i]['tabfield7'] = empty($appProgram['yearStartEdu'])?' ':$appProgram['yearStartEdu'];
                $resultArray[$i]['tabfield8'] = $period;
                $resultArray[$i]['tabfield9']  = $this->ifNotDefis(/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['fullTimeCount']);
                $resultArray[$i]['tabfield10'] = $this->ifNotDefis(/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['partTimeCount']);
                $resultArray[$i]['tabfield11'] = $this->ifNotDefis(/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['correspondenceCount']);
                $resultArray[$i]['tabfield12'] = $this->ifNotDefis(/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['familyCount']);
                $resultArray[$i]['tabfield13'] = (/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['networkForm'] == 1 ? 'Да' : "Нет");
                $resultArray[$i]['tabfield14'] = (/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['remoteTechnology'] == 1 ? 'Да' : "Нет");
                $resultArray[$i]['tabfield15'] = (/*$newArrApplicationPrograms[$oneProgramtId]*/$appProgram['publicAccreditation'] == 1 ? 'Да' : "Нет");
                $resultArray[$i]['sortOrder']  = $programType->getField('SortOrder');
                ++$i;
            }
            //добавим сортировк по типам и кодам программ
            if(!empty($resultArray)){
                usort($resultArray, function($a_obj, $b_obj){
                    $a = $a_obj['sortOrder'].$a_obj['tabfield5'];
                    $b = $b_obj['sortOrder'].$b_obj['tabfield5'];
                    //уберем точки и дефисы
                    $a = str_replace('.', '', $a);
                    $b = str_replace('.', '', $b);
                    $a = str_replace('-', '', $a);
                    $b = str_replace('-', '', $b);
                    //return strnatcasecmp($a , $b);
                    $result = (int)$a -(int)$b;
                    return $result;
                });
                $i = 1;
                foreach ( $resultArray as $key => $value ) {
                    unset($resultArray[$key]['sortOrder']);
                    $resultArray[$key]['countcol']=$i;
                    ++$i;
                }
            }
          return   $resultArray;
    }

    
    /**
     * дергаем наименование должности руководителя организации
     * 
     * @return type
     */
    public function getChargePositionTitle() {
        return $this->mainOrg->getChargePositionTitle();        
    }
    


    //последняя строка
}