<?php

namespace Eo\Model\ExportDocument;

use Model\Gateway\EntitiesTable;
use Model\Entities\eiisEducationalOrganizations;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExportDublicateCertificate
 *
 * @author sany
 */
class ExportTempCertificate extends ExportDocument {

    public  function getTablesOut (){
      return array('table1'=>'tabfield1','table2'=>'tabfield2');
    }

    function getType()
    {
        return  'TempCertificate';
    }

    public function getData()
    {
        $currentApplication = $this->currentApplication;
        $monthArray         = $this->monthArray;
        $resultArray        = $this->resultArray;

        $certificate = $this->getCertificate();
        $MainOrg     = $this->mainOrg;
        $arrayOrg    = $MainOrg->toArray();
        if( empty( $this->moreInfo[ $arrayOrg['Id'] ] ) ){
            $this->moreInfo[ $arrayOrg['Id'] ] ['Information'] = 0;
            $this->moreInfo[ $arrayOrg['Id'] ] ['Email'] = '';            
        }
        if( $this->moreInfo[ $arrayOrg['Id'] ] instanceof \Model\Entities\isgaApplicationMoreInfo ){
            $moreInfo    = $this->moreInfo[ $arrayOrg['Id'] ]->toArray();
        }
        else{
            $moreInfo    = $this->moreInfo[ $arrayOrg['Id'] ];
        }

        $reorgOrganNames = array();
        $isga_mtm_Application_Organizations = $currentApplication->selectComplicated("isga_mtm_Application_Organizations", array('fk_eiisEducationalOrganization'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        if ($isga_mtm_Application_Organizations) {
            $reorgOrganIDs = array_column($isga_mtm_Application_Organizations, 'fk_eiisEducationalOrganization');
            $reorgOrgans =  $isgaEGSArray = $currentApplication->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName'), array('Id' => $reorgOrganIDs))->toArray();
            $reorgOrganNames = array_column($reorgOrgans, 'FullName');
        }

        $orgData = new \Model\Entities\isgaEducationalOrganizationData();
        $orgData = $orgData->getById($arrayOrg['Id'],'fk_eiisEducationalOrganization');
        $phone = '';
        if(!empty($orgData)){
            $phone = $orgData->getFieldOrSpace('Phones');
            $emailData = $orgData->getFieldOrSpace('Emails');
        }
        if(empty($phone)){
            $phone =  $this->ifNotEmpty($arrayOrg['Phones']);
        }

        //адрес брать из какогото хитрого реестра
        $orgAdres = '';
        $orgMtmClass = new \Model\Entities\isgamtmApplicationBranches();
        $orgMtmClass = $orgMtmClass->getByWhere(array(
            'fk_isgaApplication' => $currentApplication->getId(),
            'fk_eiisEOBranch'    => $arrayOrg['Id'],
        ));
        if( !empty($orgMtmClass) ){
           $orgAdres =  $orgMtmClass->getFieldSafe('ApplicationAdres');
        }
        if( empty($orgAdres) ){
            $orgAdres = $arrayOrg['LawAddress'];
        }

        //колдуем с адресом почты        
        if( empty($emailData) ){
            $emailMain = $this->ifNotEmpty($arrayOrg['Mails']);
        }
        else{
            $emailMain = $emailData;
        }        
        
        $resultArray['fieldFullName']  = $arrayOrg['FullName']
                . ((empty($arrayOrg['ShortName'])|| $arrayOrg['ShortName']=='NULL')?'':' (' . $arrayOrg['ShortName'].')' );
        $resultArray['fieldLawAddress']  = $orgAdres;
        $resultArray['fieldGosRegNum']  = $arrayOrg['GosRegNum'];
        $resultArray['fieldInn']  =  $this->ifNotEmpty($arrayOrg['Inn']);
        $resultArray['fieldKpp']  =  $this->ifNotEmpty($arrayOrg['Kpp']);
        $resultArray['fieldFullNameOnly']  = $arrayOrg['FullName'];
        $resultArray['fieldReason']  = $this->getApplicationReasonNames();
        $resultArray['fieldReorgOrganNames'] = implode(', ', $reorgOrganNames);
        $resultArray['field10'] = $this->getRequisitesCertificate($certificate);
        $resultArray['field11'] = 'Федеральная служба по надзору в сфере образования и науки ';
        $resultArray['fieldPhones']  =  $phone;
        $resultArray['fieldMails']  =  $emailMain;
        //$resultArray['fieldWww']  = $this->ifNotEmpty($arrayOrg['Www']);
        $resultArray['fieldWww']  = $orgData->getFieldOrSpace('Site');     
        $resultArray['fieldI'] = $this->ifActive($moreInfo['Information']);
        $resultArray['fieldInformationEmail'] = $this->conditionSymbolActive($moreInfo['Information']) ? $moreInfo['Email'] : ' ';
        $resultArray['fieldINo'] = $this->ifNotActive($moreInfo['Information']);
        $resultArray['fieldChargePosition'] =  $this->getChargePositionTitle();
        $resultArray['fieldFIOContactManager'] =  $this->ifNotEmpty($this->getFIOContactManager($arrayOrg));
        $resultArray=$this->dateParse($resultArray);
        $resultArray['shtrihKod'] = $this->getShtrihKod($currentApplication->getField('Barcode'), $currentApplication->getField('fk_isgaApplicationStatus'));
        //${BlockMainTable}  ${/blockMainTable}
        $resultArray['blockMainTable'] = array();
        //${BlockMainTable2}  ${/blockMainTable2}
        $resultArray['blockMainTable2'] = array();

        $applicationPrograms       = $currentApplication
            ->selectComplicated(
                "isga_ApplicationPrograms",
                array(
                    /*
                    'fk_eiisLicensedPrograms',
                    'yearStartEdu',
                    'termEdu',
                    'fullTimeCount',
                    'partTimeCount',
                    'correspondenceCount',
                    'familyCount',
                    'networkForm',
                    'remoteTechnology',
                    'publicAccreditation'
                    */
                ),
                array(
                    'fk_isgaApplication' => $currentApplication
                    ->getField('Id'),
                    'fk_isgaEOBranches'  => $arrayOrg['Id'])
                )->toArray();
        $newArrApplicationPrograms = array();
        foreach ($applicationPrograms as $oneAplicationProgram) {
            $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
        }
        $programsIds = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
        $programsIds = array_filter($programsIds);
        if (sizeof($programsIds) != 0) {
            $programItems = $currentApplication->getLinkedItems(
                    "eiis_LicensedPrograms",
                    array('Id' => $programsIds)
                )->getObjectsArray();
            $resultArray = array_merge($resultArray, $this->getTablesByProgram($programItems, $newArrApplicationPrograms));
            if( !empty($resultArray['table1']) ){
                $resultArray['blockMainTable'][0]['table1'] = $resultArray['table1'];
            }
            if( !empty($resultArray['table2']) ){
                $resultArray['blockMainTable2'][0]['table2'] = $resultArray['table2'];
            }
        }
        unset( $resultArray['table1'] );
        unset( $resultArray['table2'] );

        $branchesApplication = $currentApplication->selectComplicated("isga_mtm_Application_Branches", array('fk_eiisEOBranch','ApplicationAdres'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        $j                   = 0;
        $educationOrganization = new eiisEducationalOrganizations();
        if (isset($branchesApplication)) {
            foreach ($branchesApplication as $oneBranch) {
                // пропускаем главный вуз, чтобы
                if ($oneBranch['fk_eiisEOBranch'] == $this->mainOrg->getId()) {
                    continue;
                }
                $MainOrg                               = $currentApplication->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $oneBranch['fk_eiisEOBranch']));
                $arrayOrg                              = $MainOrg->toArray();
                $educationOrganization->setField('Id', $arrayOrg['Id']);
                $educationalOrganizationDataRes = $educationOrganization->getEducationalOrganizationData();
                $educationalOrganizationData = ($educationalOrganizationDataRes !== false) ? $educationalOrganizationDataRes->toArray() : array();

                $phone = '';
                $emailData = '';
                if(!empty($educationalOrganizationDataRes)){
                    //$phone =  $this->getPrhoneFaxeseducationalOrganizationData($educationalOrganizationData);
                    $phone = $educationalOrganizationDataRes->getFieldOrSpace('Phones');
                    $emailData = $educationalOrganizationDataRes->getFieldOrSpace('Emails');
                }
                if(empty($phone) && !empty($arrayOrg['Phones']) && strtolower($arrayOrg['Phones'])!='null'){
                    $phone =  $arrayOrg['Phones'];
                }

                //адрес ввлиниваем из связной таблицы
                $orgAdres = '';
                if( !empty($oneBranch['ApplicationAdres']) ){
                   $orgAdres =  $oneBranch['ApplicationAdres'];
                }
                if( empty($orgAdres) ){
                    $orgAdres = $arrayOrg['LawAddress'];
                }
            
                //колдуем с адресом почты        
                if( empty($emailData) ){
                    $emailData = $this->ifNotEmpty($arrayOrg['Mails']);
                }                               

                $resultArray['branches'][$j]['field1'] = $arrayOrg['FullName']
                        . ((empty($arrayOrg['ShortName'])|| $arrayOrg['ShortName']=='NULL')?'':' (' . $arrayOrg['ShortName'].')' );
                $resultArray['branches'][$j]['field2'] = $orgAdres;
                $resultArray['branches'][$j]['field3'] =  $this->ifNotEmpty($arrayOrg['Kpp']);
                $resultArray['branches'][$j]['field4'] = $phone;
                $resultArray['branches'][$j]['field5'] = $emailData;
                //$resultArray['branches'][$j]['field6'] = ($educationalOrganizationData) ? $educationalOrganizationData['Site'] : '';
                $resultArray['branches'][$j]['field6'] = ($educationalOrganizationDataRes) ? $educationalOrganizationDataRes->getFieldOrSpace('Site'): '';
                $resultArray['branches'][$j]['blockFilialTable'] = array();
                $resultArray['branches'][$j]['blockFilialTable2'] = array();
                $applicationPrograms                   = $currentApplication->selectComplicated("isga_ApplicationPrograms", array('fk_eiisLicensedPrograms', 'yearStartEdu', 'termEdu', 'fullTimeCount', 'partTimeCount', 'correspondenceCount', 'familyCount', 'networkForm', 'remoteTechnology', 'publicAccreditation'), array('fk_isgaApplication' => $currentApplication->getField('Id'), 'fk_isgaEOBranches' => $oneBranch['fk_eiisEOBranch']));
                $applicationPrograms                   = $applicationPrograms->toArray();
                $applicationPrograms=array_filter($applicationPrograms);
                if (count($applicationPrograms) > 0) {
                    $newArrApplicationPrograms = array();
                    foreach ($applicationPrograms as $oneAplicationProgram) {
                        $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
                    }
                    $programsIds      = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
                    $programs         = $currentApplication->selectComplicated("eiis_LicensedPrograms", array('Id', 'Code', 'Name', 'fk_eiisEduProgram', 'fk_eiisEduProgramType'), array('Id' => $programsIds))->toArray();
                    $programItems = $currentApplication->getLinkedItems(
                                    "eiis_LicensedPrograms", array('Id' => $programsIds)
                            )->getObjectsArray();
                    if (count($programItems) > 0) {
                        $resultArray['branches'][$j]=array_merge($resultArray['branches'][$j], $this->getTablesByProgram($programItems, $newArrApplicationPrograms));
                    }

                    //${BlockFilialTable}  ${/BlockFilialTable}
                    if( !empty($resultArray['branches'][$j]['table1']) ){
                        $resultArray['branches'][$j]['blockFilialTable'][0]['table1'] = $resultArray['branches'][$j]['table1'];
                    }
                    //${BlockFilialTable2}  ${/BlockFilialTable2}
                    if( !empty($resultArray['branches'][$j]['table2']) ){
                        $resultArray['branches'][$j]['blockFilialTable2'][0]['table2'] = $resultArray['branches'][$j]['table2'];
                    }
                    unset($resultArray['branches'][$j]['table1']);
                    unset($resultArray['branches'][$j]['table2']);
                }
                ++$j;
            }
        }
        return $resultArray;
    }

    /**
     * Сбиваем табличку/и из массива лицензионных программ
     * @param type $programItems - массис лицензионнх кпрограмм - классов
     * @return type - массив таблички
     */
    public function getTablesByProgram($programItems,$newArrApplicationPrograms) {
            $i1 = $i2 = 0;
            $array_unique1 = $array_unique2 = array();
            foreach ($programItems as $oneProgram) {
                $oneProgramId = $oneProgram->getId();
                $appProgram = $newArrApplicationPrograms[$oneProgramId];

                if(empty($appProgram['fk_eiisEduProgramTypes'])){
                    $programType=$oneProgram->getEduProgramType();
                }else{
                    $programType=$oneProgram->getEduProgramType($appProgram['fk_eiisEduProgramTypes']);
                }

                $oneProgramLevel = $programType->getEduLevel();
                if(empty($oneProgramLevel)){
                    $oneProgramLevel = $oneProgram->getEduProgramLevel();
                }
                $oneProgramLevelName = $oneProgramLevel->getField('Name');

                if (empty($appProgram['fk_isgaEnlargedGroupSpecialities'])) {
                    $oneProgramUgs = $oneProgram->getUgs();
                } else {
                    $oneProgramUgs = $oneProgram->getUgs($appProgram['fk_isgaEnlargedGroupSpecialities']);
                }

                $oneProgramUgsCode = $oneProgramUgs->getField('Code');
                $oneProgramUgsName = $oneProgramUgs->getField('Name');
                $oneProgramIsProgram = $oneProgramLevel->getField('IsProgram');
                if (empty($oneProgramIsProgram)) {
                   $name = $oneProgramLevelName;
                    if (!in_array($name, $array_unique1)) {
                        $resultArray['table1'][$i1]['countcol']=$i1+1;
                        $resultArray['table1'][$i1]['tabfield1'] = $name;
                        $array_unique1[] = $name;
                        ++$i1;
                    }
                } else {
                    $code = $oneProgramUgsCode;
                    $code = $code=='Void'?' ':$code;
                    if (!in_array($code, $array_unique2)) {
                        $resultArray['table2'][$i2]['countcol']=$i2+1;
                        $resultArray['table2'][$i2]['tabfield2'] = $code;
                        $resultArray['table2'][$i2]['tabfield3'] = $oneProgramUgsName;//$isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                        $resultArray['table2'][$i2]['tabfield4'] = $oneProgramLevelName;//$arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                        $array_unique2[] = $code;
                        ++$i2;
                    }
                    unset($code);
                }
            }
            return $resultArray;
    }

}


