<?php
namespace Eo\Model;

/**
 * Спец класс для отправки файлов на открытую часть
 *
 */
class SendMail {

    /**
     * проксируем отсылку почты по хитрому
     * с вложениями
     *
     *          $leter['mail']['To']                        - куда
     *          $leter['mail']['CopyTo']                    - куда копия
     *          $leter['mail']['ToName']                    - куда красиваое имя, не обязательно
     *          $leter['mail']['Body']                      - само письмо
     *          $leter['mail']['Sumject']                   - тема
     *          $leter['mail']['Attach'][*]['Name']         - название вложеного файла
     *          $leter['mail']['Attach'][*]['Content']      - содержимое вложеного файла
     *          $leter['mail']['AttachUpload'][*]['Path']   - путь к файлу на этом сервере
     *          $leter['mail']['AttachUpload'][*]['Name']   - как надо его звать
     *          $leter['mail']['ReportTo']                  - сюда отправлять копию в случае успеха
     *          $leter['debug']['printBody']                - возращает тело письма, не обязательно
     *          $leter['debug']['printLeter']               - возращает весь текст письми, не обязательно
     *          $leter['debug']['log']                      - комент в лог,  строка или массив
     *          $leter['debug']['DoSendIt'] = 'DoSendIt'    - отправляем в любом случае
     *          $leter['debug']['debugAdres']               - адреса отладочные
     *          Файлы                                       - принимает как вложения
     *          $isRealAdress = false                       - как отладину
     *          $mailerClass -  клас отпрвитель (этот же), чтоб логи не сбивались
     * 
     *          результат
     *              'Ok' - слава богу
     *              иное - чегото отвалилось
     */
     static function SendMailToOpenServer($leter,$isRealAdress = false, $mailerClass = null){
        if(  is_string($leter['mail']['To'])){
            $leter['mail']['To'] = array($leter['mail']['To']);
        }

        $httpHost = $_SERVER['HTTP_HOST'];
        $leter['mail']['rp'] = md5(date("m.d.y")."j9jdK%%s");
        if(empty($leter['debug']['log'])){
            $leter['debug']['log'] = array();
        }
        if( is_string($leter['debug']['log'])){
            $s = $leter['debug']['log'];
            $leter['debug']['log'] = array();
            $leter['debug']['log'][] = $s;
        }
        $leter['debug']['log'][] = 'отправлено с '.$httpHost;
        $leter['debug']['log'][] = 'ip адрес клиента '.$_SERVER['REMOTE_ADDR'];;
        //впишем пользователя
            $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
            $dbTableAuthAdapter  = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter($dbAdapter,
                'ron_Users','Login','Password');
            $authService = new \Auth\Storage\AuthService();
            $authService->setAdapter($dbTableAuthAdapter);
            $authService->setStorage(new \Auth\Storage\AuthStorage('Auth'));
            $user = $authService->getIdentity();
            if(empty($user)){
                throw new \Exception('Пользовательне не аутентифицирован');
            }
            $userId = $user->getId();
        $leter['debug']['log'][] = 'Пользователь '. $userId.' - '.  $user->getFieldOrSpace('Name');

        $isgaHost = '192.168.66.253';
        //if ( substr($httpHost, 0,6)=='f11ron' || '176.9.166.251' == $httpHost){
        if($httpHost != $isgaHost ){
            $isRealAdress = false;
        } else {
            //заглушка, чтоб на реальном не дергнули
            //throw new \Exception('временно не работает');
        }

        if($isRealAdress){
            $leter['debug']['work'] = $leter['mail']['To'];
        }else{
            $leter['debug']['work'] = 0;
            $leter['mail']['Body'] = 'хотели отправить '. implode(', ',$leter['mail']['To']). "<br>\n". $leter['mail']['Body'];
        }
                //отсылаем на почтовик
                /*
                //а курл не пашет
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://10.0.10.10/exchange/index/mail");
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $sendData);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 300);
                $result = curl_exec($ch);
                */
                //старый добрый стреам, всегда и везде, иногда
                //$sendDataQuery = http_build_query($leter);
                //$sendDataContext = stream_context_create(array(
                //            'http' => array(
                //                'method'  => 'POST',
                //                'header'  => 'Content-type: application/x-www-form-urlencoded',
                //                'content' => $sendDataQuery
                //            )
                //));
                //@$result = file_get_contents($url, false, $sendDataContext );
                if(!empty($mailerClass) && $mailerClass instanceof self){
                    $mailer = $mailerClass;
                } else {
                    $mailer = new self;
                }
                //если отладочная локалка отправим по нарошку
                $httpServer = $_SERVER['SERVER_ADDR'];
                if($httpServer == '192.168.0.10'){
                    return 'Ok';
                }                
                try{
                    $result = $mailer->mailProxyAction($leter);
                } catch (\Exception $e) {
                    $result = $e->getMessage();
                }
                return $result;
     }


    /**
     * проксируем отсылку почты по хитрому
     * с вложениями
     *
     *          $leter['mail']['To']                      - куда
     *          $leter['mail']['CopyTo']                  - куда копия
     *          $leter['mail']['ToName']                  - куда красиваое имя, не обязательно
     *          $leter['mail']['rp']                      - секретный пароль чтоб лишние не лезли
     *          $leter['mail']['Body']                    - само письмо
     *          $leter['mail']['Sumject']                 - тема
     *          $leter['mail']['Attach'][*]['Name']       - название вложеного файла
     *          $leter['mail']['Attach'][*]['Content']    - содержимое вложеного файла
     *          $leter['mail']['AttachUpload'][*]['Path'] - путь к файлу на этом сервере
     *          $leter['mail']['AttachUpload'][*]['Name'] - как надо его звать
     *          $leter['mail']['ReportTo']                - сюда отправлять копию в случае успеха
     *          $leter['debug']['sendOnly']               - отослать не отдавать форму, старое
     *          $leter['debug']['printBody']              - возращает тело письма, не обязательно
     *          $leter['debug']['printLeter']             - возращает весь текст письми, не обязательно
     *          $leter['debug']['work']                   - должен совпадать с $leter['mail']['To'] иначе на тестовый адрес
     *          $leter['debug']['log']                    - комент в лог
     *          $leter['debug']['DoSendIt'] = 'DoSendIt'  - отправляем в любом случае
     *          $leter['debug']['debugAdres']             - отладочные адреса
     *          Файлы - принимает как вложения
     *
     *          результат
     *              'Ok' - слава богу
     *              иное - чего-то отвалилось
     *
     */
    public function mailProxyAction($post=array()){

        $httpHost = $_SERVER['HTTP_HOST'];

        if(  is_string($post['mail']['To'])){
            $post['mail']['To'] = array($post['mail']['To']);
        }

        $this->mailLog('Начинаем отправку письма');
        @$this->mailLog('От '.$_SERVER['REMOTE_ADDR']);
        @$this->mailLog('Это сервер '.$_SERVER['SERVER_ADDR']);
        @$this->mailLog('Это хост '.$httpHost);
        if(empty($post['debug']['work'])){
            $post['debug']['work'] = array('');
        }       
        if(  is_string($post['debug']['work'])){
            $post['debug']['work'] = array($post['debug']['work']);
        }
        @$this->mailLog('рабочий адрес '.implode(', ',$post['debug']['work']));

        //$post = $this->getRequest()->getPost();
        if ( !empty($post['debug']['log']) ) {
            if ( is_string($post['debug']['log']) ) {
                $this->mailLog($post['debug']['log']);
            }
            if ( is_array($post['debug']['log']) ){
                foreach ( $post['debug']['log'] as $value ) {
                    $this->mailLog($value);
                }
            }
        }
        if(empty($post) || empty($post['mail'])){
            $this->mailLog('Данные не приехали');
            return 'Данные не приехали';
        }
        $regPass = md5(date("m.d.y")."j9jdK%%s");
        $mail = $post['mail'];
        if(empty($mail["rp"])||$mail["rp"]!=$regPass){
            $this->mailLog('Неверный проходной пароль');
            return "Неверный проходной пароль";
        }
        if(empty($mail['To'])){
            $this->mailLog('Нет получателя');
            return "Нет получателя";
        }
        $this->mailLog('хотим отправить на '.implode(', ',$mail['To']));
        $isDebug = true;
        //отладочные адреса
        $to = array('test-isga@mail.ru','kater1988@rambler.ru');
        if( !empty($post['debug']['debugAdres']) ){
            $to = $post['debug']['debugAdres'];
        }        
        if((!empty($post['debug']['work']) && $post['debug']['work'] == $mail['To'])
            ||(!empty($post['debug']['DoSendIt']) && $post['debug']['DoSendIt'] == 'DoSendIt')
        ){
            $to  = $mail['To'] ;
            $isDebug = false;
        }
        if(!empty($post['debug']['DoSendIt']) && $post['debug']['DoSendIt'] == 'DoSendIt'){
            $this->mailLog('Режим принудительной отправки');
        }

        $this->mailLog('отправка на реально '.implode(' ,',$to));
        $this->mailLog('режим отладки '.$isDebug);
        //проверка почтовых адресов
        //$validator = new \Zend\Validator\EmailAddress();
        //if (!$validator->isValid($to)) {
        //    echo 'Почтовый адрес '.$to.' не почтовый адрес ';
        //    $this->mailLog('Почтовый адрес '.$to.' не почтовый адрес ');
        //    die;
        //}
        $toName = null;
        if(!empty($mail['ToName'])){
            $toName  = $mail['ToName'] ;
        }
        if(empty($mail['Sumject'])){
            $subject = '';
        } else {
            $subject = $mail['Sumject'];
        }
        if(empty($mail['Body'])){
            $messageText ='';
        } else {
            $messageText = $mail['Body'];
        }
        $messageText = (string)$messageText;

            $html = new \Zend\Mime\Part($messageText);
            //$html->type = \Zend\Mime\Mime::TYPE_TEXT;
            $html->type = \Zend\Mime\Mime::TYPE_HTML;
            $html->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
            $html->charset = 'utf-8';

            $message = new \Zend\Mail\Message();
            //$message->setSender(array('isga_sys@msk.nica.ru'=>'isga_sys'));

            $message->addTo($to,$toName);
            if(!empty($subject)){
                $message->setSubject($subject);
            }
            $message->setEncoding("utf-8");
            $message->setFrom('isga_sys@msk.nica.ru','isga_sys');

            $mimeMessage = new \Zend\Mime\Message();
            $mimeMessage ->addPart($html);

            $this->mailLog('главны блок добавлен ');
            $this->mailLog('долливаем файлы ');

            if(isset($_FILES['mail'])&& !empty($_FILES['mail']['name'][0])){
                $this->mailLog('    прикрепляем файлы из присланых как файлы');
                foreach ($_FILES['mail']['name'] as $key => $one_file) {
                    $this->mailLog('        прикрепляем файл');
                    if ($_FILES['mail']['size'][$key]=0){
                        //echo 'нет длины';
                        $this->mailLog('        нет длины');
                        continue;
                    }
                    if($_FILES['mail']['error'][$key] != 0){
                        $this->mailLog('        ошибка в файле');
                        return 'ошибка в файле';
                        continue;
                    }

                    $fileContent = file_get_contents($_FILES['mail']['tmp_name'][$key]);
                    $attachment = new \Zend\Mime\Part($fileContent);
                    $attachment->type = $_FILES['mail']['type'][$key];
                    $attachment->filename = $_FILES['mail']['name'][$key];
                    $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
                    $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                    $this->mailLog('        файл '.$attachment->filename );

                    $mimeMessage->addPart( $attachment);
                    $this->mailLog('        файл добавлен');
                }
            }

            //если в посте файлы
            if(!empty($post['mail']['Attach'])){
                $this->mailLog('    прикрепляем файлы в запросе');
                foreach ( $post['mail']['Attach'] as $key => $value ) {
                    $this->mailLog('        прикрепляем файл');
                    if(empty($value['Name'])){
                        $this->mailLog('        Нет Имени файла');
                        return 'Нет Имени файла';
                    }
                    if(empty($value['Content'])){
                        $this->mailLog('        Нет содержимого файла');
                        return 'Нет содержимого файла';
                    }
                    $attachment = new \Zend\Mime\Part($value['Content']);
                    $attachment->type = \Zend\Mime\Mime::TYPE_OCTETSTREAM;
                    $attachment->filename = $value['Name'];
                    $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
                    $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                    $this->mailLog('        файл '.$attachment->filename );

                    $mimeMessage->addPart( $attachment);
                    $this->mailLog('        Файл добавлен');
                }
            }

            //Если отправляемые файлы на сервере в хранилище
            if(!empty($post['mail']['AttachUpload'])){
                $this->mailLog('    Собираем файлы с сервера');
                foreach ( $post['mail']['AttachUpload'] as $key => $value ) {
                    if(empty($value['Path'])){
                        $this->mailLog('        нет пути');
                        if($isDebug){
                            continue;
                        } else {
                            return 'нет пути';
                        }
                    }
                    $hashName = $value['Path'];
                    $realName = $value['Name'];
                    //если нет реального сойдет и кривое
                    if(empty($realName)){
                        $hashNameArray = explode('/', $hashName);
                        $hashNameArray = array_filter($hashNameArray);
                        $realName = end($hashNameArray);
                    }
                    //$path = $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$hashName;
                    $path = 'http://'.$httpHost.'/uploads/'.$hashName;
                    // проверим на наличие файла
                    //$isFile = is_file($path );
                    //if(!$isDebug && !$isFile){
                    //    $this->mailLog('        Файл '.$realName.' как '.$hashName.' отсутствует');
                    //    return 'Файл '.$realName.' как '.$hashName.' отсутствует';
                    //}
                    //If(!$isFile){
                    //    $fileContent = 'Заглушка вместо '.$path;
                    //    $this->mailLog('        Заглушка вместо '.$path);
                    //} else {
                    //    $fileContent = file_get_contents($path);
                    //}
                    //дергаем файлы с открытой стороны
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $path);
                    curl_setopt($curl,CURLOPT_TIMEOUT,5);
                    curl_setopt($curl,CURLOPT_CONNECTTIMEOUT,4);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    $fileContent = curl_exec($curl);
                    curl_close($curl);
                    //поавлись не порядки
                    $realName = str_replace('/', ' ', $realName);

                    //@$fileContent = file_get_contents($path);
                    if(!$isDebug && $fileContent === false){
                        $this->mailLog('        Файл '.$realName.' как '.$hashName.' отсутствует');
                        return 'Файл '.$realName.' как '.$hashName.' отсутствует';
                    }
                    If($fileContent === false){
                        $fileContent = 'Заглушка вместо '.$path;
                        $this->mailLog('        Заглушка вместо '.$path);
                    }

                    $attachment = new \Zend\Mime\Part($fileContent);
                    $attachment->type = \Zend\Mime\Mime::TYPE_OCTETSTREAM;
                    $attachment->filename = $realName;
                    $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
                    $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                    $this->mailLog('        файл '.$attachment->filename );

                    $mimeMessage->addPart( $attachment);
                    $this->mailLog('        файл добавлен');
                }
            }

            $message->setBody($mimeMessage);

            if(isset($post['debug']['printBody'])){
                header('Content-Type: text/plain');
                echo $message->getBodyText();
                die;
            }
            if(isset($post['debug']['printLeter'])){
                header('Content-Type: text/plain');
                echo $message->toString();
                die;
            }
            if(isset($post['debug']['printPart'])){
                header('Content-Type: text/plain');
                $mimeMessage->getParts();
                die;
            }
            $this->mailLog('готовим к отправке');
            $smtpOptions = new \Zend\Mail\Transport\SmtpOptions(array(
                    'name' => '192.168.66.240',
                    'host' => '192.168.66.240',
                    //'name' => 'localhost',
                    //'host' => 'localhost',
                    //'name' => 'mx.nica.ru',
                    //'host' => 'mx.nica.ru',
                    'port' => 25,
            ));

            $transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
                //ini_set("SMTP","server.aln" );
                //ini_set("smtp_port","25");
                //ini_set('sendmail_from', 'server.aln');
                //$subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
                //$messageText =  $message->getBodyText();
                //mail($to, $subject, $messageText, $headers);
            try {
                $this->mailLog('отправляем первым сервером '.$smtpOptions->getHost());
                $transport->send($message);
            } catch (\Exception $e) {
                $this->mailLog('отправить первым сервером не удалось ');
                $this->mailLog('Первый сервер отсылки не исправен '."".$e->getMessage());
                $smtpOptions = new \Zend\Mail\Transport\SmtpOptions(array(
                    'name' => 'localhost',
                    'host' => 'localhost',
                    'port' => 25,
                ));
                $transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
                try {
                    $this->mailLog('отправляем вторым сервером '.$smtpOptions->getHost());
                    $transport->send($message);
                } catch (\Exception $e) {
                    $this->mailLog('Сервер отсылки не исправен '."\n".$e->getMessage());
                    $this->mailLog('отправка провалилась');
                    $this->mailLog('');
                    return 'Второй сервер отсылки не исправен '."".$e->getMessage();
                }
            }
            //если не отладка то пошлем копию
            if(!$isDebug ){
                //$toCopy = 't.lubinets@msk.nica.ru';
                $toCopy = 'isga_experts@mail.ru';
                if(!empty($post['mail']['ReportTo'])){
                    $toCopy = $post['mail']['ReportTo'];
                }
                $toCopyName = 'копия письма';
                //$toCopy = 'test_isga@mail.ru';
                $this->mailLog('отправлено на '.implode(', ',$to));
                $this->mailLog('отправим копию на '.$toCopy);
                //$to = $message->getTo()->get();
                $message->setTo($toCopy,$toCopyName);
                //$subject = $message->getSubject();
                $subject = 'копия отправленного на '.implode(',',$to).' '.$subject;
                $message->setSubject($subject);
                try {
                    $transport->send($message);
                } catch (\Exception $e) {
                    $this->mailLog('Сервер отсылки копии не исправен '."\n".$e->getMessage());
                    return 'Сервер отсылки не исправен '."\n".$e->getMessage();
                }
                $this->mailLog('копия отправлена');
            }
            $this->mailLog('Ok');
            $this->mailLog('');
            return 'Ok';
    }

    /**
     * логирование
     *
     * @param type $logMsg
     * @param string $fileName
     */
    function mailLog( $logMsg, $fileName ='public/log/mail.log') {
        if( 'public/log/mail.log' == $fileName ){            
            $fileName ='public/log/mail/mail-'.date('Y-m-d').'.log';
        }
        $date = date('Y-m-d H:i:s (T)');
        $fileName  = $_SERVER['DOCUMENT_ROOT'].'/'.$fileName;
        $dir = dirname( $fileName );
        if( !is_dir( $dir ) ){
            mkdir( $dir, 0777 , true );
        }
        $f = fopen($fileName, 'a+');
        if (!empty($f)) {
            $msg  = "[".$this->getUniqId()."] [$date] $logMsg".PHP_EOL;
            fwrite($f, $msg);
            fclose($f);
        }
        return $logMsg;
    }

    private $_uniqId = null;
    public function getUniqId() {
        if(empty($this->_uniqId)){
            $this->_uniqId = md5(microtime(true));
        }
        return $this->_uniqId;

    }
}