<?php
namespace Eo\Service;

/**
 * получение, обработка, и вывод данных выписки свидетельств 
 * @author Ruslan
 */
class certificateExcerptService
{
    private
      //данные документа      
      $docName = 'vipiska_certificates',
      $docType = 'docx',
      $generateDocName = 'vipiska_svidetelstvo',
      $generteDocType = 'pdf',
      //переменные для сокращения количества запросов к базе
      $certStatuses = null,
      $certSupStatuses = null,
      $certTypes = null,
      $eduLevels = null,
      $includeProgram = true,
      //Эквиваленты месяцев
      $MONTH_EQUIVALENT = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        ];

    public function setIncludeProgram($bool) {
            $this->includeProgram = $bool;
    }
        
    public function getIncludeProgram() {
        return $this->includeProgram;
    }
        
/**
 * получаем путь к документу
 * @return string
 */
    protected function getDocumentPath(){
        //local
//        return  $_SERVER['DOCUMENT_ROOT'] . '/templates/';
        //global
        return $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
    }
    
    /**
     * Получаем имя документа
     * @return string
     */
    protected function getDocumentName(){
        return $this->docName;
    }

    /**
     * получаем тип документа
     * @return string
     */
    protected function getDocumentType(){
        return $this->docType;
    }
    
    /**
     * Получает тип для сгененрированого документа
     * @return string
     */
    protected function getGenerateDocumentType(){
        return $this->generteDocType;
    }

    /**
     * Получает имя для сгененрированого документа
     * @$addStr string
     * @return string
     */
    protected function getGenerateDocumentName( $addStr = null ){
        $str = $this->generateDocName;
        
        if( !empty($addStr) ){
            $str .= $addStr;
        }
        return $str;
    }
    
    /**
     * Получаем текущую дату
     * @return string
     */
    public function now(){
        $dt = new \DateTime('now');
        return $dt->format('d.m.Y');
    }
    
    /**
     * Получаем день
     * @param string $date
     * @return string
     */
    public function getDay( $date ){
        $dt = new \DateTime($date);
        return $dt->format('d');
    }
    
    /**
     * Получаем месяц
     * @param string $date
     * @return string
     */
    public function getMonth( $date ){
        $dt = new \DateTime($date);
        $m = (int)$dt->format('m');
        $meq = $this->MONTH_EQUIVALENT;
        return $meq[$m];
    }
    
    /**
     * Получаем год
     * @param string $date
     * @return string
     */
    public function getYear( $date ){
        $dt = new \DateTime($date);
        return $dt->format('Y');
    }
    
    /**
     * Получаем объект склонялки
     * @return \Eo\Model\Licensing\Tools\Morphy
     */
    public function getMorphy() {
        return new \Eo\Model\Licensing\Tools\Morphy();
    }
    
    /**
     * Возвращает строку в дательном падеже
     * @param string $str
     * @return string
     */
    public function getDativeFromStr($str) {
        $morphy = $this->getMorphy();
        $forms = $morphy->getForms($str);
        return $forms['dative'];
    }
    
    /**
     * Получаем свидетельство по ид
     * @param string $certId
     * @return \Model\CertReestrEntities\accreditationcertificate
     */
    public function getCertificateFormId($certId) {
        return new \Model\CertReestrEntities\accreditationcertificate($certId);
    }
    
    /**
     * Получаем свидетельство по дефолтному ид
     * @param string $certId
     * @return \Model\CertReestrEntities\accreditationcertificate
     */
    public function getCertificateFormDefId($certId) {
        $cert = new \Model\CertReestrEntities\accreditationcertificate($certId);
         return $cert->getByWhere([
            'Id' => $certId
        ]);
    }
    
    /**
     * Получаем объекты приложения по ид свидетельства
     * @param string $certId
     * @return \Model\Entities\isgaCertificateSupplements
     */
    public function getCertificateSuppFromCertId($certId) {
        $certSupp = new \Model\Entities\isgaCertificateSupplements();
        return $certSupp->getAllByWhere([
            'fk_isgaCertificate' => $certId
        ]);
    }
    
    /**
     * Получаем бланк и серию из свидетельства
     * @param \Model\CertReestrEntities\accreditationcertificate( $cert
     * @return string
     */
    public function getBlankAndSerial($cert) {
        return $cert->getFieldOrSpace('SerialNumber').' '.$cert->getFieldOrSpace('FormNumber');
    }
    
    /**
     * получаем полное имя организации из свидетельства
     * @param \Model\CertReestrEntities\accreditationcertificate( $cert
     * @return string
     */
    public function getOrgFullName($cert) {
        $fullName = '';
//       $dative = $this->getDativeFromStr( $cert->getFieldOrSpace('EduOrgFullName') );
        
        if(!empty($cert->getField('EduOrgShortName'))){
            $fullName = $cert->getFieldOrSpace('EduOrgFullName').', '.$cert->getField('EduOrgShortName');
        }else{
            $fullName = $cert->getFieldOrSpace('EduOrgFullName');
        }
        return $fullName;
    }
    
    /**
     * Получаем номера приложений по данным приложений
     * @param array $supsData
     * @return string
     */
    public function getSupplementNumbers($supsData) {
        $supNum = '';
        $splitter = '';
        
        foreach ($supsData as $cs ){
            $supNum .= $splitter.$cs['сNumber'];
            $splitter = ', ';
        }
        return $supNum;
    }
    
    /**
     * Получаем массив статусов сертификата
     * @return array
     */
    public function getEduLevelsArray() {
        if( empty($this->eduLevels)){
            $this->eduLevels = [];
            $levels = new \Model\CertReestrEntities\edulevel();
            $levels = $levels->getAllByWhere(null);
            
            foreach($levels as $el){
                $this->eduLevels[$el->getField('_id')] = $el->getField('Name');         
            }
        }
        return $this->eduLevels;
    }
    
    /**
     * Получаем статус по ид
     * @param string $statusId
     * @return string
     */
    public function getEduLevelFromId($eduLevelId) {
        $levelArr = $this->getEduLevelsArray();
        $level = '';
        
        if(isset( $levelArr[ $eduLevelId])){
            $level = $levelArr[ $eduLevelId];
        }
        return $level;
    }
    
     /**
     * Получаем массив статусов сертификата
     * @return array
     */
    public function getCertStatusesArray() {
        if( empty($this->certStatuses)){
            $this->certStatuses = [];
            $status = new \Model\CertReestrEntities\certificatestatus();
            $status = $status->getAllByWhere(null);
            
            foreach($status as $el){
                $this->certStatuses[$el->getField('_id')] = $el->getField('Name');         
            }
        }
        return $this->certStatuses;
    }
    
    /**
     * Получаем статус по ид
     * @param string $statusId
     * @return string
     */
    public function getCertStatusFromId($statusId) {
        $statusArr = $this->getCertStatusesArray();
        $status = '';
        
        if(isset( $statusArr[ $statusId])){
            $status = $statusArr[ $statusId];
        }
        return $status;
    }
    
    /**
     * получаем массив статусов приложения
     * @return string
     */
    public function getCertSupStatusesArray() {
        if( empty($this->certSupStatuses)){
            $this->certSupStatuses = [];
            $status = new \Model\CertReestrEntities\certificatesupplementstatus();
            $status = $status->getAllByWhere(null);
            
            foreach($status as $el){
                $this->certSupStatuses[$el->getField('_id')] = $el->getField('Name');         
            }
        }
        return $this->certSupStatuses;
    }
    
    /**
     * Получаем статус приложения по ид
     * @param string $statusId
     * @return string
     */
    public function getCertSupStatusFromId($statusId) {
        $statusArr = $this->getCertSupStatusesArray();
        $status = '';
        
        if(isset( $statusArr[ $statusId])){
            $status = $statusArr[ $statusId];
        }
        
        return $status;
    }
    
    /**
     * Получаем массив статусов
     * @return array
     */
    public function getCertTypeArray() {
        if( empty($this->certTypes)){
            $this->certTypes = [];
            $types = new \Model\CertReestrEntities\certificatetype();
            $types = $types->getAllByWhere(null);
            
            foreach($types as $el){
                $this->certTypes[$el->getField('_id')] = $el->getField('Name');         
            }
        }
        return $this->certTypes;
    }
    
    /**
     * Получаем тип по ид
     * @param string $statusId
     * @return string
     */
    public function getCertTypeFromId($statusId) {
        $statusArr = $this->getCertTypeArray();
        $status = '';
        
        if(isset( $statusArr[ $statusId])){
            $status = $statusArr[ $statusId];
        }
        
        return $status;
    }
    
    /**
     * Получаем объект проверяющей организации по ид
     * @param type $controlOrganId
     * @return \Model\Entities\eiisControlOrgans
     */
    public function getControlOrganFromId($controlOrganId) {
        return new \Model\Entities\eiisControlOrgans($controlOrganId);
    }
    
    /**
     * Получаем имя проверяющей организации по ид
     * @param string $controlOrganId
     * @return string
     */
    public function getControlOrganNameFromId($controlOrganId) {
        $controlOrgan = $this->getControlOrganFromId($controlOrganId);
        return $controlOrgan->getFieldOrSpace('Name');
    }
    
    /**
     * Получаем объект региона по ид
     * @param string $regionId
     * @return \Model\CertReestrEntities\region
     */
    public function getRegionFromId($regionId) {
        return new \Model\CertReestrEntities\region($regionId);
    }
    
    /**
     * Получаем имя региона по ид
     * @param string $regionId
     * @return string
     */
    public function getRegionNameFromId($regionId) {
        $region = $this->getRegionFromId($regionId);
        return $region->getFieldOrSpace('Name');
    }
    
    /**
     * Получаем приложения по ид свидетельства
     * @param string $certId
     * @return \Model\CertReestrEntities\certificatesupplement
     */
    public function getSupplementFromCertId($certId){
        $certSupp = new \Model\CertReestrEntities\certificatesupplement();
        return $certSupp->getAllEntities( null, new \Zend\Db\Sql\Expression("INET_ATON(SUBSTRING_INDEX(CONCAT(certificatesupplement.Number,'.0.0.0'),'.',4))"), ['certificate_id' => $certId]);
       
    }
    
    public function uniqueArray($arr) {
        $i = 0;
        $ret = [];
        
        foreach( $arr as $val){
            $isSet = false;
            $code = $val['opcode'];
            $level = $val['oplevel'];
            
            foreach( $ret as $el){
                $tcode = $el['opcode'];
                $tlevel = $el['oplevel'];
                
                if($tcode == $code && $tlevel == $level){
                   $isSet = true; 
                }
            }
            
            if(!$isSet){
                $ret[] = $val;
            }
        }
        
        return $ret;
    }
    
    public function getProgrammFromSupId( $supid ){
        if(!$this->getIncludeProgram()){
            return [];
        }
        $programs = new \Model\CertReestrEntities\accreditedprogramm();
        $programs = $programs->getProgramsByDefaultSupId($supid);
        $programBlock = [];
        $programsToRet = [];
        $counter = 1;
        
        foreach( $programs as $pr ){
            
            if($counter == 1 ){
                $programsToRet[] = [
                    'opcount' => '№ п.п.',
                    'opcode' => 'Код УГСН',
                    'opname' => 'Наименование УГСН',
                    'oplevel' => 'Уровень образования',
                    'opcanceled' => 'Лишение',
                    'opsuspended' => 'Приостановление',
                ];
            }
            
            $program = [
                'opcount' => $counter,
                'opcode' => $pr->getFieldOrSpace('UGSCode'),
                'opname' => $pr->getFieldOrSpace('UGSName'),
                'oplevel' => $this->getEduLevelFromId($pr->getField('edulevel_id')),
                'opcanceled' => $pr->getFieldOrSpace('Suspended') ? 'Да' : 'Нет',
                'opsuspended' =>  $pr->getFieldOrSpace('Canceled') ? 'Да' : 'Нет',
            ];
            $programsToRet[] = $program;
            $counter++;
        }
        
        if(count($programsToRet)>0){
            $programBlock['opprogramTitle'] = 'Программы приложения';
            $op = $this->uniqueArray($programsToRet);
            $counter = 1;
            
            foreach( $op as $key=>$el ){
                if($key !== 0 ){
                    $op[$key]['opcount'] = $counter;
                    $counter++;
                }
            }
            
            $programBlock['opprogramData'] = $op;
            $ret = [];
            $ret[] = $programBlock; 
            return $ret;
        }
        
        return [];
    }
    
    /**
     * Получаем данные приложения
     * @param string $cert
     * @return array
     */
    public function getSupplementData($cert){
        $certId = $cert->getId();
        $certSupp = $this->getSupplementFromCertId($certId);
        $supplements = [];
        $blankSerNum = $this->getBlankAndSerial($cert);
        
        foreach ($certSupp as $cs ){
            $csId = $cs->getId();
            $dec = new \Model\CertReestrEntities\certificatedecision();
            $dec = $dec->getByWhere([
                'startedsupplement_id' => $csId
            ]);            
            $str = '';
            $programs = $this->getProgrammFromSupId($cs->getFieldOrSpace('id'));
            
            if(!empty($dec)){
                $str = $dec->getFieldOrSpace('OrderDocumentKind').' №'
                            .$dec->getFieldOrSpace('OrderDocumentNumber')
                                .' от '.$dec->getDate('DecisionDate');
            }
            
            $status = $this->getCertSupStatusFromId( $cs->getFieldOrSpace('certificatesupplementstatus_id') );
            
            $supplements[] = [
                'сNumber' => $cs->getFieldOrSpace('Number'),
                'cNumber2' => $cs->getFieldOrSpace('Number'),
                'EOFullName' => $cs->getFieldOrSpace('EduOrgFullName'),
                'DocKindName' => $str,
                'certSupStatusName' => $status,
                'certSupStatusName2' => $status,
                'SblankSerNum' => $cs->getFieldOrSpace('SerialNumber').' '.$cs->getFieldOrSpace('FormNumber'),
                'Note' => $cs->getFieldOrSpace('Note'),
                'opprograms' => $programs
            ];
        }
        
        return $supplements;
    }
    
    /**
     * Возвращает полный путь по умолчанию
     * @return string
     */
    private function getDefaultPath() {
        return $this->getDocumentPath().$this->getDocumentName().'.'.$this->getDocumentType();
    }
    
    /**
     * Получаем данные свидетельства
     * @param \Model\CertReestrEntities\accreditationcertificate $cert
     * @return array
     */
    public function getDateFromCertificate( $cert ) {
        $fullName = $this->getOrgFullName($cert);
        $dateIssue = $cert->getField('IssueDate');
        $supplements = $this->getSupplementData($cert);
        $supNum = $this->getSupplementNumbers($supplements);
        $region = $this->getRegionNameFromId( $cert->getFieldOrSpace('region_id'));
        $blankSerNum = $this->getBlankAndSerial($cert);
        $type = $this->getCertTypeFromId($cert->getField('certificatetype_id'));
        $status = $this->getCertStatusFromId($cert->getField('certificatestatus_id'));
        $controlOrgan = $cert->getFieldOrSpace('ControlOrgan');
        
        $data = [
            'EOFullName' => $cert->getFieldOrSpace('EduOrgFullName'),
            'EOFullName2' => $cert->getFieldOrSpace('EduOrgFullName'),
            'EOAddressL' => $cert->getFieldOrSpace('PostAddress'),
            'EOAddressL2' => $cert->getFieldOrSpace('PostAddress'),
            'DateNow' => $this->now(),
            'orgNames' => $fullName,
            'dateIssueD' => $this->getDay($dateIssue),
            'dateIssueM' => $this->getMonth($dateIssue),
            'monthToLastLic' => $this->getMonth($dateIssue),
            'dateIssueY' => $this->getYear($dateIssue),
            'RegNumber' => $cert->getFieldOrSpace('RegNumber'),
            'RegNumber2' => $cert->getFieldOrSpace('RegNumber'),
            'SerialNumber' => $cert->getFieldOrSpace('SerialNumber'),
            'FormNumber' => $cert->getFieldOrSpace('FormNumber'),
            'supList' => $supNum,
            'fk_Region_EOAddressL' => $region,
            'Inn' => $cert->getFieldOrSpace('EduOrgINN'),
            'GosRegNum' => $cert->getFieldOrSpace('EduOrgOGRN'),
            'EOShortName' => $cert->getFieldOrSpace('EduOrgShortName'),
            'DateIssue' => $cert->getDate('IssueDate'),
            'DateEnd' => $cert->getDate('EndDate'),
            'fk_eiisControlOrgan' => $controlOrgan,
            'DateUpdate' => $cert->getDate('CreateDate'),
            'blankSerNum' => $blankSerNum,
            'typeName' => $type,
            'statusName' => $status,
            'suplBlok' => $supplements,
        ];
        //        echo '<pre>';
//        var_dump($licData);
//        echo '</pre>';
//        die;

        return $data;
    }
    
    /**
     * Получаем остальные свидетельства
     * @param string $orgId
     * @param string $certId
     * @return \Model\CertReestrEntities\accreditationcertificate
     */
    public function getOldCertificate( $orgId, $certId) {
        $certs = new \Model\CertReestrEntities\accreditationcertificate();
        return  $certs->getToExcerpt( $orgId, $certId);
    }
    
    /**
     * Генерируем документ по ид свидетельства
     * @param string $certId
     */
    public function generate( $certId ) {
        $cert = $this->getCertificateFormDefId($certId);
        $curId = $cert->getId();
        $data = $this->getDateFromCertificate($cert);
        $data['di'] = '"'.$data['dateIssueD'].'" '.$data['dateIssueM'].' '.$data['dateIssueY'];
        unset($data['monthToLastLic']);
        unset($data['dateIssueD']);
        unset($data['dateIssueM']);
        unset($data['dateIssueY']);
        $odlCert = $this->getOldCertificate($cert->getField('EduOrgId'),$curId);
        $data['old'] = [];
        $toLastTitle = 'ДРУГИЕ СВИДЕТЕЛЬСТВА';
        
        foreach ( $odlCert as $tcert) {
            $lastData = $this->getDateFromCertificate($tcert);
            $lastData['toLastTitle'] = $toLastTitle;
            $data['old'][] = $lastData;
            $toLastTitle = '';
        }
        $templater = new \Ron\Model\Templater(
            $this->getDefaultPath(),
            $data,
            $this->getGenerateDocumentType(),
            $this->getGenerateDocumentName('_№'.$cert->getFieldOrSpace('RegNumber'))
        );
        //генерируем документ
        $templater->makeDocumentOut();
        die;
    }
      
    
}
