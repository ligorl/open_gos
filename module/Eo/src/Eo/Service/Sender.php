<?php

namespace Eo\Service;
use Model\Gateway\EntitiesTable;

class Sender{

    CONST TMP_DIRECTORY = '_karta_tmp_';

    /**
     *
     * @var array
     */
    protected $tableColumns = array(
        'eiis_Licenses' => array(
            'LicenseRegNum',
            'DateLicDoc',
            'SerDoc',
            'NumDoc',
            'fk_eiisLicenseState',
            'DateEnd',
            'fk_eiisEducationalOrganization',
        ),
        'isga_Certificates' => array(
            'fk_eiisEducationalOrganization',
            'RegNumber',
            'fk_isgaCertificateStatus',
            'fk_isgaCertificateType',
            'SerialNumber',
            'FormNumber',
            'DateEnd',
            'DateIssue',
        ),
        'eiis_LicenseStates' => array(
            'Id',
            'Name',
            'WebName',
            'IsValid',
        ),
        'isga_CertificateStatuses' => array(
            'Id',
            'Name',
            'Code',
            'DateCreate',
            'DateUpdate',
            'EiisId',
        ),
        'isga_CertificateTypes' => array(
            'Id',
            'Name',
            'Code',
        ),
    );

    /**
     *
     * @param string $tableName
     * @param string $url
     */
    public function sendTable($tableName, $url){
        try{
            $tmpFile = getcwd() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::TMP_DIRECTORY . DIRECTORY_SEPARATOR . md5(time() . rand(0, 1000) . rand(0, 1000)) . '.txt';
            $table = new EntitiesTable($tableName);
            $columns = $this->tableColumns[$tableName];
            $data = $table->select(function($select) use ($columns){
                $select->columns($columns);
            });
            $this->writeInTmpFile($data, $tmpFile);
        }catch(\Exception $e){
            $message = '';
            $message .= $e->getMessage();
            $message .= "Имя таблицы '{$tableName}'. ";
            $message .= "Имя файла '{$tmpFile}'. ";
            $message .= "Время ошибки " . date('Y-m-d H:i:s');
            throw new \Exception($message);
        }
        $result = $this->send($url, $tmpFile, $tableName);
        $this->removeTmpFile($tmpFile);
        return $result;
    }

    /**
     *
     * @param \Zend\Db\ResultSet\ResultSetInterface $data
     * @param string $tmpFile
     */
    protected function writeInTmpFile(\Zend\Db\ResultSet\ResultSetInterface $data, $tmpFile){
        $fh = fopen($tmpFile, 'a+');
        foreach($data as $row){
            $arrRow = $row->toArray();
            foreach($arrRow as &$value){
                $value = $this->clearValue($value);
            }
            unset($value);
            $serialized = serialize($arrRow);
            if(!$serialized){
                throw new \Exception("Не валидна строка таблицы. ");
            }
            fwrite($fh, $serialized . "\n");
        }
        fclose($fh);
    }

    /**
     *
     * @param string $url
     * @param string $tmpFile
     * @param string $tableName
     * @return string
     */
    protected function send($url, $tmpFile, $tableName){
        $request = curl_init($url);
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt( $request, CURLOPT_POSTFIELDS, array(
            'file' => curl_file_create(realpath($tmpFile)),
            'table' => $tableName,
            'hash' => md5_file(realpath($tmpFile)
        )));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($request);
        curl_close($request);
        return $result;
    }

    /**
     *
     * @param string | integer $value
     * @return string | integer
     */
    protected function clearValue($value){
        return str_replace(array("\r\n", "\r", "\n", "\t"), '', $value);
    }

    /**
     *
     * @param string $tmpFile
     * @return boolean
     */
    protected function removeTmpFile($tmpFile){
        if(file_exists($tmpFile)){
            return unlink($tmpFile);
        }
        return false;
    }

    protected function curlFileCreate($filename, $mimetype = '', $postname = '') {
        return "@$filename;filename="
            . ($postname ?: basename($filename))
            . ($mimetype ? ";type=$mimetype" : '');
    }
}
?>