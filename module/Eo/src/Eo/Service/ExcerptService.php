<?php

namespace Eo\Service;

class ExcerptService{
    
    public function getOrg($orgId){
        return new \Model\Entities\eiisEducationalOrganizations($orgId);
    }
    
    public function getNewExcerpt() {
        return new \Model\LodEntities\LicenseAndCertificateOrdering();
    }
    
    public function now(){
        $dt = new \DateTime('now');
        return $dt->format('Y-m-d H:i:s');
    }
    
    public function getNewLicense(){
        return new \Model\Entities\eiisLicenses();
    }
    
    public function getNewCertificate(){
        return new \Model\Entities\isgaCertificates();
    }
    
    public function addToExcerpt($fields){
        $excerpt = $this->getNewExcerpt(); 
        $excerpt->setFields($fields);
        $excerpt->save(true);
    }
    
    public function getValidLicense( $orgId) {
        $lic = $this->getNewLicense();
        return $lic->getByWhere([
            'fk_eiisEducationalOrganization' => $orgId,
            'fk_eiisLicenseState' => 'E8EE8E7F37E847A78ED1F79901270A79'
        ]);
    }
    
    public function getValidCertificate( $orgId) {
        $lic = $this->getNewCertificate();
        return $lic->getByWhere([
            'fk_eiisEducationalOrganization' => $orgId,
            'fk_isgaCertificateStatus' => 'cba8e890-429a-4d6a-b81c-0f530a7970cc'
        ]);
    }
    
    public function createExcerptLicensMode( $orgId) {
        $license = $this->getValidLicense($orgId);
        $date = $this->now();
        $fullName = $license->getFieldOrSpace('EOName');
        $inn = $license->getFieldOrSpace('EOInn');
        $objectId = $license->getField('Id');
        
        $fields = [
            'object_type' => 'license',
            'object_id' => $objectId,
            'organization_id' => $orgId,
            'date_request' => $date,
            'organization_full_title' => $fullName,
            'inn' => $inn,
        ];
        
         $this->addToExcerpt($fields);
    }
    
    public function createExcerptCertMode( $orgId) {
        $cert = $this->getValidCertificate($orgId);
        $date = $this->now();
        $fullName = $cert->getFieldOrSpace('EOFullName');
        $inn = $cert->getFieldOrSpace('Inn');
        $objectId = $cert->getField('Id');
        
        $fields = [
            'object_type' => 'certificate',
            'object_id' => $objectId,
            'organization_id' => $orgId,
            'date_request' => $date,
            'organization_full_title' => $fullName,
            'inn' => $inn,
        ];
        
        $this->addToExcerpt($fields);
    }
    
    
    public function getExcerptFromOrg( $orgId, $type, $sort, $page=1 ) {
        $onPage = 20;
        $offset = ($page-1)*$onPage;
        $excerpt = $this->getNewExcerpt();
        
        return $excerpt->getAllToPage([
                'object_type' => $type,
                'organization_id' => $orgId
            ],
            $sort,
            $onPage,
            $offset);
    }
    
    public function getLicenseTitle($licId){
        $lic = $this->getNewLicense()->getByWhere(['Id'=>$licId]);
        return '№'.$lic->getFieldOrSpace('LicenseRegNum').' от '.$lic->getDate('DateLicDoc').'г. '.$lic->getFieldOrSpace('SerDoc').' '.$lic->getFieldOrSpace('NumDoc').' ';
    }
    
    public function getAllLicenseExcerpt( $orgId, $page=1, $sort='date_request DESC') {
        $excerpts = $this->getExcerptFromOrg($orgId, 'license', $sort, $page);
        $previos = null;
        $licenseDocument = null;
        $title = '';
        $toRet = [];
        
        foreach($excerpts as $exc){
            $excLicId = $exc->getField('object_id');
            
            if($excLicId !== $previos){
                $previos = $excLicId;
                $lic = $this->getNewLicense()->getByWhere(['Id'=>$excLicId]);
                $title = '№'.$lic->getFieldOrSpace('LicenseRegNum').' от '.$lic->getDate('DateLicDoc').'г. '.$lic->getFieldOrSpace('SerDoc').' '.$lic->getFieldOrSpace('NumDoc').' ';
                $licenseDocument = $lic->getDocumentFile();
            }
            $exc->setField('title', $title);
            $exc->setField('licenseDocument', $licenseDocument );
            $toRet[] = $exc;
        }
        return $toRet;
    }
    
    public function getCountLicenseExcerpt( $orgId) {
        return $this->getNewExcerpt()->getDataCount_ArrayCondition([[
            'column' => 'organization_id="'.$orgId.'"',
            'and' => 'object_type="license"'
        ]]);
    }
    
    public function getCountCertificateExcerpt( $orgId) {
        return $this->getNewExcerpt()->getDataCount_ArrayCondition([[
            'column' => 'organization_id="'.$orgId.'"',
            'and' => 'object_type="certificate"'
        ]]);
    }
    
    public function getCertificateTitle($certId){
        $cert = $this->getNewCertificate()->getByWhere(['Id'=>$certId]);
        return '№'.$cert->getFieldOrSpace('RegNumber').' от '.$cert->getDate('DateIssue').'г. '.$cert->getFieldOrSpace('SerialNumber').' '.$cert->getFieldOrSpace('FormNumber').'';
    }
    
    public function getAllCertificateExcerpt( $orgId, $page=1, $sort='date_request DESC') {
        $excerpts = $this->getExcerptFromOrg($orgId, 'certificate', $sort, $page);
        $previos = null;
        $cert = null;
        $title = '';
        $toRet = [];
        $docFileUrl = null;
        $docFileName = null;
        
        foreach($excerpts as $exc){
            $excCertId = $exc->getField('object_id');
            
            if($excCertId !== $previos){
                $previos = $excCertId;
                $cert = $this->getNewCertificate()->getByWhere(['Id'=>$excCertId]);
                $title =  '№'.$cert->getFieldOrSpace('RegNumber').' от '.$cert->getDate('DateIssue').'г. '.$cert->getFieldOrSpace('SerialNumber').' '.$cert->getFieldOrSpace('FormNumber').'';
                $docFileUrl = $cert->getScanMaketUrlAll(  0 , 'sign_document' );
                $docFileName = $cert->getScanMaketFileNameAll(  0 , 'sign_document' );
            }
            $exc->setField('title', $title);
            $exc->setField('docFileUrl', $docFileUrl);
            $exc->setField('docFileName', $docFileName);
            $toRet[] = $exc;
        }
        return $toRet;
    }
    
    public function getCurrentLicenseQuery( $orgId ){
         $lic = $this->getValidLicense($orgId);
         $dop = 'Отправить запрос на предоставление из ИС "Реестр лицензий" выписки по лицензии ';
         return $dop.'№'.$lic->getFieldOrSpace('LicenseRegNum').' от '.$lic->getDate('DateLicDoc').'г. '.$lic->getFieldOrSpace('SerDoc').' '.$lic->getFieldOrSpace('NumDoc').'?';
    }
    
    public function getCurrentCertificateQuery( $orgId ){
         $cert = $this->getValidCertificate($orgId);
         $dop = 'Отправить запрос на предоставление из ИС "Реестр организаций, осуществляющих образоваельную деятельность по имеющим государственную аккредитацию образовательным программам" выписку по свидетельству ';
         return $dop.'№'.$cert->getFieldOrSpace('RegNumber').' от '.$cert->getDate('DateIssue').'г. '.$cert->getFieldOrSpace('SerialNumber').' '.$cert->getFieldOrSpace('FormNumber').'?';
    }
    
}
?>