<?php

namespace Eo\Service;

define("FILES_PATH", $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/');
define("SIGN_HOST", "http://192.168.66.50");
define("WSDL_PATH", SIGN_HOST . '/verify/service.svc?wsdl');

//define("WSDL_PATH", 'http://qs.cryptopro.ru/SVS/service.svc?wsdl');

//define ( "FILES_PATH", $_SERVER['DOCUMENT_ROOT'].'/public/' );
//define ( "SIGN_HOST", "http://192.168.67.141" );
//define ( "SIGN_HOST", "http://dss.cryptopro.ru" );
//define ( "WSDL_PATH", SIGN_HOST . '/verify/service.svc?wsdl');
//define ( "WSDL_PATH", 'http://dss.cryptopro.ru/SignServer/SignService.svc?wsdl');


class Sign
{

    public $soap = NULL;

    public function __construct()
    {
        // $this->soap = new \SoapClient( WSDL_PATH, array('trace' => 1) );
        //  die;
        // try{
        //   $this->soap = new \SoapClient( WSDL_PATH, array('trace' => 1) );
        /* }catch(\Exception $e){
          //print_r($e->getTraceAsString());
          $this->soap = false;
          return "Нет соединения с сервером проверки подписи!";
          die;
          } */

        $toPath = $_SERVER['DOCUMENT_ROOT'].'/'.'public/log/to_dss';
        if( !is_dir( $toPath )){
            mkdir( $toPath );
        }
        $this->logerClass = new \Eo\Model\SendMail();


    }

    private $_intResult = null;

    /**
     * инициирует соап
     *
     *
     * @return type  '' - все путем
     *               'чего-то' - описание ошибки
     */
    public function init_soap()
    {
        //$this->soap = new \Zend\Soap\Client(WSDL_PATH);
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        if (null === $this->soap) {
            try {
                //$this->soap = new \SoapClient( WSDL_PATH );
                $this->soap = new \SoapClient(WSDL_PATH, array('trace' => 1));
                $this->_intResult = '';
            }
            catch (\Exception $e) {
                 //print_r($e->getTraceAsString());die;
                $this->soap = false;
                $this->_intResult = $e->getMessage();//"Нет соединения с сервером проверки подписи!";
                $this->loging( '!!!!!>>>>>>>>>>>>ошибка при соеденении с соап' );
                $this->loging( "\t\t" . $e->getCode() .' - ' .  $e->getMessage() );
                $this->loging( "\t\t" . $e->getTraceAsString() );
                //die;
            }
        }
        return $this->_intResult;
    }

    /**
     * проверка валидности подписи файла на сервере ПП
     *
     * @param String $file_name
     * @param Boolen $is_certificate_info
     * @param type $isFileFullPath          - флаг имя файла записано полностью а не относительно
     *
     * @return Result Valid
     */

    public function valid_signature($file_name, $return_certificate_info = true, $isFileFullPath = false ,  $isExFunction = false )
    {
        $this->loging('проверка  подписи боечвая' );
        $this->loging('проверка  боечва файла '. $file_name );
        $this->loging('проверять боечва по пути '. ((int)$isFileFullPath) );
            //боевой
            $initResult = $this->init_soap();
            if (empty($initResult)) {
                $this->loging('проверять боечва дсс делаем запрос ' );
                $response = $this->send_file_to_DSS($file_name, true, $isFileFullPath , $isExFunction );
            }
            else {
                $this->loging('проверять боечва дсс инициализация соапа выдвлв чегото ' . print_r( $initResult , true ) );
                $response = array("status" => "error", "result" => $initResult);
            }
            if ($response['status'] != "error") {
                $this->loging('проверять боечва дсс ответ положителен ' .  print_r( $response , true ) );
                if ($return_certificate_info) {
                    $response['responce'] = (array_key_exists('responce', $response)) ? $response['responce'] : "";
                    //$response['certificate_info'] = $this->parse_certificate_info($response['responce']);
                    $response['certificate_info'] = $this->parse_certificate_info($response['responce']);
                    if( empty( $response['signatureInfo'] ) ){
                         $response['signatureInfo'] = array();
                    }
                    $response['signature_info'] = $this->parse_signature_info( $response['signatureInfo'] );
                    $response['certificate_info'] = array_merge(  $response['certificate_info'] , $response['signature_info'] );
                    unset($response['responce']);
                    unset( $response['signatureInfo'] );
                }
            }
            $this->loging('проверять боечва дсс ответ разбок '.  print_r( $response , true ) );

            //проверим на наличие сведений о сертификата
            if (!empty($response['status']) && 'ok' == $response['status']) {
                if (empty($response['certificate_info'])) {
                    $response['status'] = 'error';
                    $response['result'] = 'нет сведений о сертификате';
                } else {
                    if (empty($response['certificate_info']['NotBefore'])) {
                        $response['status'] = 'error';
                        $response['result'] = 'в сертификате даты начала нет';
                    }
                    if (empty($response['certificate_info']['NotAfter'])) {
                        $response['status'] = 'error';
                        $response['result'] = 'в сертификате даты окончания нет';
                    }
                    if ('ok' == $response['status']) {
                        //проверим даты действия сертификата
                        $this->loging('проверим даты действия сертификата' );
                        $dateCurent = new \DateTime();
                        $dateBegin = new \DateTime($response['certificate_info']['NotBefore']);
                        $dateEnd = new \DateTime($response['certificate_info']['NotAfter']);
                        if ($dateCurent < $dateBegin) {
                            $response['status'] = 'error';
                            $response['result'] = 'Действия сертификата еще не наступило';
                        }
                        if ($dateCurent > $dateEnd) {
                            $response['status'] = 'error';
                            $response['result'] = 'действие сертификата истекло';
                        }
                    }
                }
            }
        return $response;
    }

    /**
     * отправка файла на сервер DSS и получение ответа
     * @param String $file_name имя подписанного файла
     * @param Boolen $return_responce нужно ли возвращать ответ с сервера DSS
     * @param Boolen $isFileFullPath  - путь к проверяемому файлу полный - труе, условный - фалсе
     *
     * @return Array "status", "result"
     */
    public function send_file_to_DSS( $file_name, $return_responce = false, $isFileFullPath = false , $isExFunction = false )
    {
        $this->loging('начинаем засылку на дсс' );
        $this->loging('на дсс  подписи боечвая' );
        $this->loging('на дсс  боечва файла '. $file_name );
        $this->loging('на дсс боечва по пути '. ((int)$isFileFullPath) );

        $initResult = $this->init_soap();
        if (empty($initResult)) {
            $result = array();
            $this->loging('путь полный '.( (int)$isFileFullPath)  );
            if( $isFileFullPath ){
                $filepath = $file_name;
            }
            else{
                $file_name = str_ireplace('/uploads/', '' , $file_name );
                $this->loging('файл после обрезки '. $file_name  );
                $filepath = FILES_PATH . $file_name;
            }
            $this->loging('путь к файлу '. $filepath  );


            if (file_exists($filepath)) {
                $this->loging('проверим тип подписи'  );
                $signature_type = $this->get_type_sign($filepath);
                $this->loging('тип подписи '.$signature_type  );
                $binary_file = fread(fopen($filepath, "r"), filesize($filepath));
                if ($this->soap != FALSE) {
                    try {
                        $this->loging('делаем запрос на дсс сервер '  );
                        if( empty($isExFunction) ){
                            $this->loging("\t\t" . 'старый метод '  );
                            $answer_from_DSS = $this->soap->VerifySignature(array("signatureType" => $signature_type, "document" => $binary_file));
                        }
                        else{
                            $this->loging("\t\t" . 'Екс метод '  );
                            $answer_from_DSS = $this->soap->VerifySignatureEx(array("signatureType" => $signature_type, "document" => $binary_file));
                        }
                    }
                    catch (\Exception $fault) {
                        if( $fault instanceof \SoapFault){
                            $this->loging('запрос на дсс сервер отвалился '. print_r( $fault , true )  );
                            $this->loging('запрос на дсс сервер вернул ошибку '. $fault->faultstring  );
                            return array("status" => "error", "result" => $fault->faultstring);
                        }
                        else{
                            $this->loging('запрос на дсс сервер отвалился '. print_r( $fault , true )  );
                            $this->loging('запрос на дсс сервер вернул ошибку '. $fault->getMessage()  );
                            return array("status" => "error", "result" => $fault->getMessage() );
                        }
                    }
                    $this->loging('запрос на дсс сервер положителен '  );
                    $this->loging('ответ ' . print_r( $answer_from_DSS , true )  );
                    if ($answer_from_DSS) {
                        if( empty($isExFunction) ){
                            $result = array(
                                "status" => "ok",
                                "result" => $answer_from_DSS->VerifySignatureResult->Result,
                                "message" => $answer_from_DSS->VerifySignatureResult->Message
                            );
                        }
                        else{
                            $result = array(
                                "status"        => "ok",
                                "result"        => $answer_from_DSS->VerifySignatureExResult->Result,
                                "message"       => $answer_from_DSS->VerifySignatureExResult->Message,
                                'signatureInfo' => $answer_from_DSS->VerifySignatureExResult->SignatureInfo,
                            );
                        }
                        if ($return_responce){
                            if( empty($isExFunction) ){
                                $result['responce'] = $answer_from_DSS->VerifySignatureResult;
                            }
                            else{
                                $result['responce'] = $answer_from_DSS->VerifySignatureExResult;
                            }
                        }
                    }else {
                        $result = array("status" => "error", "result" => "DSS_dont_answer");
                    }
                } else {
                    $result = array("status" => "error", "result" => "dont_signal_from_dss");
                }
            } else {
                $result = array("status" => "error", "result" => "dont_find_file");
            }
        } else {
            $result  = array("status" => "error", "result" => $initResult);
        }
        return $result;
    }

    /**
     * парсинг информации о сертификате
     * @param Array $result_from_dss ответ от сервера DSS
     * @return array
     */
    public function parse_certificate_info($result_from_dss)
    {
        if (empty($result_from_dss))
            return array("status" => "ok", "result" => "DSS_dont_answer");
        $certificate_info = array();

        $certificate_info_object = $result_from_dss->SignerCertificateInfo->KeyValueOfCertificateInfoParamsstring1Iy7z97I;
        if (!empty($certificate_info_object)) {
            foreach ($certificate_info_object as $item) {
                $certificate_info[$item->Key] = $item->Value;
            }
        }
        $subject_info = (array_key_exists('SubjectName', $certificate_info)) ? $certificate_info['SubjectName'] : '';
        $subject_info = str_replace('ИНН', 'INN', $subject_info);
        $subject_info = str_replace('ОГРН', 'OGRN', $subject_info);
        $subject_info = str_replace('СНИЛС', 'SNILS', $subject_info);

        $subject_fields = array('SN', 'G', 'T', 'STREET', 'OU', 'CN', 'E', 'INN', 'OGRN', 'SNILS');
        $subject_fields = implode('|', $subject_fields);
        $subject = "";
        preg_match_all('@(' . $subject_fields . ')=(?:([\'"])([^\2]+?)\2|([^(' . $subject_fields . ')]+))@',
                       $subject_info, $matches, PREG_SET_ORDER);
        foreach ($matches as $m) {
            $value = $m[3] ? trim($m[3]) : trim($m[4]);
            if ($value{strlen($value) - 1} == ',')
                $value = substr($value, 0, -1);
            $subject[$m[1]] = $value;
        }

        $certificate_info['SubjectName'] = $subject;


        return $certificate_info;
    }


    /**
     * разббор параметров подписи
     * @param type $result_from_dss
     * @return type
     */
    public function parse_signature_info( $result_from_dss )
    {
        if (empty($result_from_dss)){
            //return array("status" => "ok", "result" => "DSS_dont_answer");
        }
        $certificate_info = array();

        if( ! empty($result_from_dss->KeyValueOfSignatureInfoParamsstring1Iy7z97I) ){
            $certificate_info_object = $result_from_dss->KeyValueOfSignatureInfoParamsstring1Iy7z97I;
            if (!empty($certificate_info_object)) {
                foreach ($certificate_info_object as $item) {
                    $certificate_info[$item->Key] = $item->Value;
                }
            }
        }

        return $certificate_info;
    }

    public function parse_info_user($info)
    {

    }

    public function get_soap_functions()
    {
        $this->init_soap();
        try {
            return $this->soap->__getFunctions();
        } catch (\SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})",
                          E_USER_ERROR);
        }
    }

    /**
     * получение текста ошибки
     * @param String $error_key
     * @return String text error
     */
    public function getErrorText($error_key)
    {
        $error_key_key = mb_strtolower($error_key, "UTF-8");
        $errors = array(
            "dont_find_file"       => "Не найден файл",
            "dont_signal_from_dss" => "Нет соединения с сервером проверки подписи",
            "DSS_dont_answer"      => "Сервер проверки подписи не отвечает",
            "dont_find_real_ext"   => "Неверное название загружаемого файла. Имя файла должно быть: произвольный_текст.расширение_файла.sig",
            "dont_sign_file"       => "файл не подписан ЭП",
            "invalid_file_format"  => "Недопустимый формат файла",
            's:client'             => "В PDF документе не найдено подписи",
            'decoding failed: syntax error' => 'Синтаксическая ошибка',
        );
        if (array_key_exists($error_key_key, $errors)) {
            return $errors[$error_key_key];
        } else {
            return $error_key;
        }
    }

    /**
     * проверка имени файла (чтоб было оригинальное расширение файла)
     * @param String $file_name имя файла
     * @return boolean
     */
    public function verify_name_signfile($file_name)
    {
        $file_name = mb_strtolower($file_name, "UTF-8");
        $ext_last = substr($file_name, 1 + strrpos($file_name, "."));
        /*
         * сказали пока убрать пдф
         */
        /* if($ext_last == "pdf")
          {
          return true;
          } */

        if ($ext_last == "sig") {
            $name_file_without_ext = substr($file_name, 0, strrpos($file_name, '.'));
            if (strrpos($name_file_without_ext, ".")) {
                $ext_real_file = substr($name_file_without_ext, 1 + strrpos($name_file_without_ext, "."));
                $allowed = array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx");
                if (in_array($ext_real_file, $allowed)) {
                    return true;
                } else {
                    return "invalid_file_format";
                }
            } else {
                return "dont_find_real_ext";
            }
        }

        return "dont_sign_file";
    }

    public function get_type_sign($file_name)
    {
        $file_name = mb_strtolower($file_name, "UTF-8");
        $ext = substr($file_name, 1 + strrpos($file_name, "."));

        switch ($ext) {
            case 'pdf': return "PDF";
            case 'sig': return "CMS";
            default : return false;
        }
    }

    public function get_hash_name($file_name)
    {
        $file_name = mb_strtolower($file_name, "UTF-8");
        $ext_last = substr($file_name, 1 + strrpos($file_name, "."));
        $name_file_without_ext = substr($file_name, 0, strrpos($file_name, '.'));
        $ext = "";

        if ($ext_last == "sig") {
            $ext = "." . substr($name_file_without_ext, 1 + strrpos($name_file_without_ext, "."));
        }

        return md5($file_name . time()) . $ext . '.' . $ext_last;
    }

    public function get_real_name($file_name)
    {
        return mb_substr($file_name, 0, mb_strrpos($file_name, '.', 'UTF-8'), 'UTF-8');
    }

    /**
     * выдергиваем содоержимое из подписаного файла
     * берет исходный файл, соткидывает откидываем с наименования расширение и создает такой файл,и закидываем туда файйл
     *добавил опциональность
     *
     * лог в  public/log/to_dss/send_file_to_DSS_boy_'.date('Y-m-d').'.log'
     *
     * @param type $file_name   - исходный файл, относительно '/public/uploads/'
     * @param type $options     -  опции пихаем
     *                  realFileName                - возращаемое имя
     *                  realExtFile name            - какое разширение закинуть
     *                  isFullPathFile              - флаг того, что $file_name не относительно а глобально
     *                  isGetContent                - возращать содержимое, а не заливать в файл
     *                  'debugPrintFilePath'        - флаг отладки - печать сформированого полного имени
     *                  'debugPrintAnswerFromDss'   - флаг отладки - печать полученого ответа от дсс
     *                  'debugPrintResult'          - флаг отладки - печать отдаваемого результата
     *
     * @return array    "status" => "error" - знать ошибка
     *                  "result" => описание ошибки
     *
     *                  "status" => "ok"   - путем
     *                  'file_name'     - имя нового файла,  относительно '/public/uploads/', если isFullPathFile то глобально, если возращает только контент, то только имя, без путей
     *                  'file_content'  - содержимое, если isGetContent был труе?
     */
    public function getContent($file_name, $options = array() )
    {
        $this->logingContentBoy( '' );
        $this->logingContentBoy('!!!начинаем получить родной контент ' );
        $this->logingContentBoy("\t\t".'родной контент файл исход '. $file_name);
        $real_file_name = $this->get_real_name($file_name);
        $this->logingContentBoy("\t\t".'родной контент файл обработаный '. $file_name);

        $initResult = $this->init_soap();
        if (!empty($initResult)) {
            return array("status" => "error", "result" => $initResult);
        }
        if( empty($options['isFullPathFile']) ){ //путь относителен
            $file_name = str_ireplace('/uploads/', '' , $file_name);
            $filepath = FILES_PATH . $file_name;
        }
        else{ //путь полный
            $filepath = $file_name;
        }
        $this->logingContentBoy("\t\t".'родной контент файл с путем  '. $filepath);
        if (!file_exists($filepath)) {
            return array("status" => "error", "result" => "dont_find_file");
        }
        if( !empty($options['debugPrintFilePath'])  ){
            print_r( $filepath );
        }

        $signature_type = $this->get_type_sign($filepath);
        $binary_file = fread(fopen($filepath, "r"), filesize($filepath));

        if ($this->soap != FALSE) {
            try {
                //отправим для распаковывания данные
                $this->logingContentBoy("\t\t".'отправим для распаковывания данные ');
                $this->logingContentBoy("\t\t\t\t".'выдергивание содержимогои - размер файла '.  number_format( filesize( $filepath ) , 0 , ',' , ' '  )  );
                $this->logingContentBoy("\t\t\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                ini_set('default_socket_timeout', '300');
                $params = new \stdClass();
                $params->signatureType = $signature_type;
                $params->document = $binary_file;
                $params->verifyParams = array(array('Key' => 'ExtractContent', 'Value' => 'true'));
                $this->logingContentBoy("\t\t".'выдергивание содержимогои - подготовлено, дергаем ' );
                $this->logingContentBoy("\t\t\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                $answer_from_DSS = $this->soap->GetSignersInfo($params);
                if( !empty($options['debugPrintAnswerFromDss'])  ){
                    print_r( $answer_from_DSS );
                }

            } catch (\SoapFault $fault) {
                $this->logingContentBoy('!!!!!!!!!! отправим для распаковывания данные  - отвалилось : ' . $fault->faultstring );
                $this->logingContentBoy("\t\t\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                return array("status" => "error", "result" => $fault->faultstring);
            }
            $this->logingContentBoy("\t\t".'отправим для распаковывания данные  - отввет получен');
            if ($answer_from_DSS) {
                $content = $answer_from_DSS->GetSignersInfoResult->AdditionalInfo->Content;
                $result = array("file_name" => $real_file_name);
                if( empty($options['isGetContent']) ){ //если не возращпть контент
                    //if( empty($options['isFullPathFile']) ){ //и относительный путь
                    //    $real_file_name = FILES_PATH . $real_file_name;
                    //}
                    $real_file_name = dirname( $filepath ).'/'.basename( $real_file_name );
                    file_put_contents( $real_file_name, $content );
                    $this->logingContentBoy("\t".'родной контент - файл впихан');
                    $this->logingContentBoy("\t\t\t\t".'файл впиханый - размер файла '.  number_format( filesize(  $real_file_name ) , 0 , ',' , ' '  )  );
                }
                else{
                    $result["file_name"] = basename($result["file_name"]);
                    $result["file_content"] = $content;
                    $this->logingContentBoy("\t".'родной контент - контент впихан');
                }
                $this->logingContentBoy("\t\t\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                $result["status"] = "ok";
            } else {
                $result = array("status" => "error", "result" => "DSS_dont_answer");
            }
        } else {
            $result = array("status" => "error", "result" => "dont_signal_from_dss");
        }
        if( !empty($options['debugPrintResult'])  ){
            print_r( $result );
        }
        $this->logingContentBoy("\t".'закончено');
        $this->logingContentBoy('');
        return $result;
    }

    /**
     * проверка подписи с учетом того, что  проверялка только на боевой
     * !!!! на локалке перед  тестированием смонтировать файлы документов с открытой части
     *
     * @param type $file_name                   - имя проверяемого файла, отноительно '/public/uploads/', если $isFileFullPath труе, то глобально
     * @param type $return_certificate_info     - вернуть всю инфу
     * @param type $isFileFullPath              - если тру - то путь глобально, фалсе - от файл лежит относитьельно '/public/uploads/'
     */
    public function validSignature($file_name, $return_certificate_info = true, $isFileFullPath = false  , $isExFunction = false ){
        $this->loging('проверка  сложная подписи начинаем'  );
        $this->loging("\t\t".'проверка  файла '. $file_name );
        $this->loging("\t\t".'проверять по полному пути '. ((int)$isFileFullPath) );
        $this->loging("\t\t".'проверять расшириным методом '. print_r( $isExFunction , true ) );

        $response = array("status" => "error", "result" => 'проверка не проводилась');
        $httpHost = $_SERVER['HTTP_HOST'];
        $httpPort = $_SERVER['SERVER_PORT'];
        $testHost = array( '85.192.47.4' , '85.192.47.3', 'f11',  'f11.aln',  'f11.mon.prostoy.ru' , '146.120.224.100' , '146.120.224.101', 'gos-open.loc', $_SERVER['HTTP_HOST'] );
        if( ( in_array($httpHost,  $testHost) ) &&  '8083' != $httpPort) {
            $this->loging("\t\t".'сервер тестовый'  );
            //тестовый
            $response = array("status" => "error", "result" => 'не смогло построить запрос');
            if( empty($isFileFullPath) ){ //путь относителен
                $file_name = str_ireplace('/uploads/', '' , $file_name );
                $fileNameFull = FILES_PATH.$file_name;
            }
            else{ //путь полный
                $fileNameFull = $file_name;
            }
            $fileNameFull = str_replace('//', '/', $fileNameFull);
            $fileNameFull = str_replace( '/public/uploads/uploads/', '/public/uploads/' , $fileNameFull );
            if( is_file($fileNameFull) ){
                    $this->loging("\t\t".'проверка подписи - файл есть'  );
                    $fileNameArray = (explode('/',$file_name) );
                    $fileNameArray = array_filter($fileNameArray);
                    $fileNameOnly  = end($fileNameArray);
                    $url           = 'http://'
                        .'isga.obrnadzor.gov.ru'
                        . '/service/index/test-sign-file';
                    if( !empty( $isExFunction ) ){
                        $url.= '?';
                        $url.= 'isExFunction=1';
                    }
                    $this->loging("\t\t".'делаем запром '. $url );
                        //. '/service/index/get-sign-content';
                    //$url.= '?';
                    //$url.= 'isDebugPrintFiles=1';
                    //$url.= '&';
                    //$url.= 'isDebugPrintFileNameNew=1';
                    //$url.= '&';
                    //$url.= 'isDebugPrintResult=1';
                    //через курл не получилось, чегось возращает какойто хмл, пришлось колдовать
                    /*
                    $boundary = '---------------------' . substr(md5(rand(0, 32000)), 0, 10);
                    $postData = '';
                    $postData .= '--' . $boundary . "\n";
                    $postData .= 'Content-Disposition: form-data; name="file"; filename="' . basename($fileNameFull) . '"' . "\n";
                    $postData .= 'Content-Type: text/xml' . "\n";
                    $postData .= 'Content-Transfer-Encoding: binary' . "\n\n";
                    $postData .= file_get_contents($fileNameFull) . "\n";
                    $postData .= '--' . $boundary . "\n";
                    //file_put_contents('POST', $postData);
                    $params = array(
                        'http' => array(
                            'timeout' => 15,
                            'method' => 'POST',
                            'content' => $postData,
                            'header' => array( 'Content-Type: multipart/form-data; boundary=' . $boundary  ),
                        )
                    );
                    unset( $postData );
                    $context = stream_context_create($params);
                    unset( $params );
                    //ini_set('default_socket_timeout',    7);
                    //$remote = file_get_contents($url, false, $context);
                    ini_set('allow_url_fopen', 1);
                    $this->loging('проверка подписи - отылка на сервер'  );
                    if ($remote = @fopen($url, 'rb', false, $context)) {
                        $this->loging('проверка подписи - ответ получен'  );
                        $result = @stream_get_contents($remote);
                        $response = \Zend\Json\Json::decode( $result , \Zend\Json\Json::TYPE_ARRAY );
                        fclose($remote);
                    }
                    else{
                        $this->loging('проверка подписи - ответ инвалид'  );
                        $error = error_get_last();
                        $this->loging( print_r( $error , true )  );
                        $response = array("status" => "error", "result" => $error['message'] );
                    }
                    */

                    $ch            = curl_init();
                    $data = array();
                    $data['file']  = curl_file_create( $fileNameFull );
                    //$data['value']  = 'no';
                    //$data['file']  = '@'.$fileNameFull;
                    curl_setopt($ch, CURLOPT_URL, $url);
                    //curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_NOBODY, false);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    //curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true );
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    //curl_setopt($ch, CURLOPT_ENCODING, 'utf-8');
                    //curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/17.0 Firefox/17.0');
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_VERBOSE, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: multipart/form-data;' ) );
                    //curl_setopt($ch,CURLOPT_HTTPHEADER,array("Expect:"));
                    $this->loging("\t\t".'проверка подписи - курл - отылка на сервер '  );
                    $this->loging("\t\t".'проверка подписи - файл - '.$fileNameFull  );
                    $this->loging("\t\t".'проверка подписи - размер файла - '.number_format( (int) filesize( $fileNameFull ) , 0 , ',' , ' '  )  );
                    $result        = curl_exec($ch);
                    $options       = curl_getinfo($ch);
                    $this->loging('результат '.$result);

                    if( 200 == $options['http_code'] ){
                        $this->loging('проверка подписи - курл - ответ валиден'  );
                        $response = \Zend\Json\Json::decode( $result , \Zend\Json\Json::TYPE_ARRAY );
                    }
                    else{
                        $this->loging('проверка подписи - курл - ответ кривой'.$result  );
                        $response = array("status" => "error", "result" => $result,'error'=>curl_error($ch) );
                    }
                curl_close($ch);
            }
            else{
                $this->loging('проверка подписи - файла нет в наличии'  );
                // !!!! на локалке перед  тестированием смонтировать файлы документов с открытой части
                $response = array("status" => "error", "result" => 'не найден файл в файлохранилище' );
            }
        }
        else{
            $this->loging('сервер боевой');
            $response = $this->valid_signature( $file_name , $return_certificate_info , $isFileFullPath  , $isExFunction );
            $this->loging( 'сервер боевой - ответ получен' . print_r( $response , true ) );
        }
        return $response;
    }

  /**
     * выдергиваем содоержимое из подписаного файла
     * берет исходный файл, соткидывает откидываем с наименования расширение и создает такой файл,и закидываем туда файйл
     * !!!! на локалке перед  тестированием смонтировать файлы документов с открытой части
     *
     *
     * @param type $file_name   -  исходный файл, относительно '/public/uploads/'
     * @param type $options     -  опции пихаем
     *                  realFileName                - возращаемое имя
     *                  realExtFile name            - какое разширение закинуть
     *                  isFullPathFile              - флаг того, что $file_name не относительно а глобально
     *                  isGetContent                - возращать содержимое, а не заливать в файл
     *                  'debugPrintFilePath'        - флаг отладки - печать сформированого полного имени
     *                  'debugPrintAnswerFromDss'   - флаг отладки - печать полученого ответа от дсс
     *                  'debugPrintResult'          - флаг отладки - печать отдаваемого результата
     *
     * @return array    "status" => "error" - знать ошибка
     *                  "result" => описание ошибки
     *
     *                  "status" => "ok"   - путем
     *                  'file_name'     - имя нового файла,  относительно '/public/uploads/', если isFullPathFile то глобально, если возращает только контент, то только имя, без путей
     *                  'file_content'  - содержимое, если isGetContent был труе?
     */
    public function getContentReal($file_name, $options = array() ){
        $this->logingContentReal( '' );
        $this->logingContentReal('начинаем получить реал контент' );
        $this->logingContentReal("\t\t".'получить реал контент файл '. $file_name );

        $response = array("status" => "error", "result" => 'проверка не проводилась');
        $httpHost = $_SERVER['HTTP_HOST'];
        $httpPort = $_SERVER['SERVER_PORT'];
        $testHost = array( '85.192.47.4' , '85.192.47.3', 'f11',  'f11.aln',  'f11.mon.prostoy.ru' , '146.120.224.100' , '146.120.224.101'  );
        if( ( in_array($httpHost,  $testHost) ) &&  '8083' != $httpPort) {
            $this->logingContentReal("\t\t".'начинаем получить реал контент  -  сервер тестовый' );
            //тестовый
            $response = array("status" => "error", "result" => 'не смогло построить запрос');
            if( empty($options['isFullPathFile']) ){ //путь относителен
                $fileNameFull = FILES_PATH.$file_name;
            }
            else{ //путь полный
                $fileNameFull = $file_name;
            }
            $fileNameFull = str_replace('//', '/', $fileNameFull);
            $fileNameFull = str_replace( '/public/uploads/uploads/', '/public/uploads/' , $fileNameFull );
            if( is_file($fileNameFull) ){
                    $options['realFileName'] = empty($options['realFileName'])?$fileNameFull:$options['realFileName'];
                    $options['realFileName'] = basename($options['realFileName']);
                    $fileNameArray = (explode('/',$file_name) );
                    $fileNameArray = array_filter($fileNameArray);
                    $fileNameOnly  = end($fileNameArray);
                    $url           = 'http://'
                        .'isga.obrnadzor.gov.ru'
                        . '/service/index/get-sign-content';
                        //. '/eo/send/test-sign-file';

                    /*
                    //через курл не получилось, чегось возращает какойто хмл, пришлось колдовать
                    $boundary = '---------------------' . substr(md5(rand(0, 32000)), 0, 10);
                    $postData = '';
                    $postData .= '--' . $boundary . "\n";
                    $postData .= 'Content-Disposition: form-data; name="file"; filename="' .  $options['realFileName'] . '"' . "\n";
                    $postData .= 'Content-Type: text/xml' . "\n";
                    $postData .= 'Content-Transfer-Encoding: binary' . "\n\n";
                    $postData .= file_get_contents($fileNameFull) . "\n";
                    $postData .= '--' . $boundary . "\n";
                    //file_put_contents('POST', $postData);
                    $params = array(
                        'http' => array(
                            'timeout' => 25,
                            'method' => 'POST',
                            'content' => $postData,
                            'header' => array( 'Content-Type: multipart/form-data; boundary=' . $boundary  ),
                        )
                    );
                    unset( $postData );
                    $context = stream_context_create($params);
                    unset( $params );
                    $this->loging('выдергивание содержимого - сокет - отылка на сервер '  );
                    ini_set('default_socket_timeout',    15);
                    */



                    $ch            = curl_init();
                    $data = array();
                    $data['file']  = curl_file_create( $fileNameFull );
                    curl_setopt($ch, CURLOPT_URL, $url);
                    //curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_NOBODY, false);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    //curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true );
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    //curl_setopt($ch, CURLOPT_ENCODING, 'utf-8');
                    //curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/17.0 Firefox/17.0');
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_VERBOSE, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: multipart/form-data;' ) );
                    //curl_setopt($ch,CURLOPT_HTTPHEADER,array("Expect:"));
                    $this->logingContentReal("\t\t".'выдергивание содержимогои - курл  '  );
                    $this->logingContentReal("\t\t".'выдергивание содержимогои - файл '.  $fileNameFull  );
                    $this->logingContentReal("\t\t".'выдергивание содержимогои - размер файла '.  number_format( filesize( $fileNameFull ) , 0 , ',' , ' '  )  );
                    $this->logingContentReal("\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                    ini_set('default_socket_timeout', 25);
                    $result        = curl_exec($ch);
                    $optionsSend   = curl_getinfo($ch);
                    //$mailer->mailLog('результат '.$result, $logFile);
                    curl_close($ch);

                    if( 200 == $optionsSend['http_code'] ){
                        $this->logingContentReal('проверка подписи - курл - ответ валиден' );
                        try{
                            $this->logingContentReal("\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                            //$this->logingContentReal( "\t\t".'вернуло '. print_r( $result , true ) );
                            $response = \Zend\Json\Json::decode( $result , \Zend\Json\Json::TYPE_ARRAY );
                            //$response = json_decode( $result , true );
                            //$this->logingContentReal( "\t\t".'отдекодило'. print_r( $response , true ) );
                            $this->logingContentReal("\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                            unset( $result );
                            //$this->logingContentReal('ответ '.print_r( $response , true) );
                            if( 'error' == $response["status"] ){
                                $this->logingContentReal('выденрку содержимого- курл - ответ не валиден '.print_r( $response , true)  );
                                //$response = array("status" => "error", "result" => $response['result'] );
                            }
                            else{
                                $content =  base64_decode( $response["file_content"] );
                                if( empty($options['isGetContent']) ){
                                    $this->logingContentReal("\t\t".'сохранился как файл' );
                                    if( empty($options['isFullPathFile']) ){
                                        $punFileName = FILES_PATH . $response["file_name"];
                                    }
                                    else{
                                        $punFileName = dirname($fileNameFull).'/'.basename($response["file_name"]);
                                    }
                                    $this->logingContentReal("\t\t".'файл сохранился как '.$punFileName  );
                                    file_put_contents( $punFileName, $content );
                                    unset( $response["file_content"] );
                                }
                                else{
                                    $this->logingContentReal("\t\t".'отдадим контент' );
                                    $response["file_content"] = $content ;
                                }
                            }
                        }
                        catch( \Exception $ex) {
                            $this->logingContentReal('Вывпало в ошибку "2" - '.$ex->getMessage() );
                            if( !empty( $result) ){
                                $this->logingContentReal("\t\t". $result );
                            }
                            if( !empty( $response ) ){
                                $this->logingContentReal("\t\t". print_r( $response , true) );
                            }
                            $response = array("status" => "error", "result" => $ex->getMessage() );
                        }

                    }
                    else{
                        $this->logingContentReal('выдергивание содержимого - курл - ответ кривой'.$result  );
                        $response = array("status" => "error", "result" => print_r( $result ,true ) );
                    }
                    /*
                    if ($remote = @fopen($url, 'rb', false, $context)) {
                        $this->logingContentReal('проверка подписи - сокет - ответ валиден '  );
                        //результпт получен
                        $result = @stream_get_contents($remote);
                        $response = \Zend\Json\Json::decode( $result , \Zend\Json\Json::TYPE_ARRAY );
                        unset( $result );
                        if( empty($options['isGetContent']) ){
                            if( empty($options['isFullPathFile']) ){
                                $punFileName = FILES_PATH . $response["file_name"];
                            }
                            else{
                                $punFileName = dirname($fileNameFull).'/'.basename($response["file_name"]);
                            }
                            file_put_contents( $punFileName, $content );
                            unset( $response["file_content"] );
                        }
                        else{
                            $response["file_content"] = base64_decode( $response["file_content"] );
                        }
                        fclose($remote);
                    }
                    else{
                        $this->logingContentReal('проверка подписи - сокет - ответ крвиой '  );
                        $error = error_get_last();
                        $this->logingContentReal( print_r( $error , true)  );
                        $response = array("status" => "error", "result" => $error['message'] );
                    }
                    */
            }
            else{
                // !!!! на локалке перед  тестированием смонтировать файлы документов с открытой части
                $response = array("status" => "error", "result" => 'не найден файл в файлохранилище' );
            }
        }
        else{
            $this->logingContentReal('начинаем получить рооддддной контент  -  сервер боевой' );
            $response = $this->getContent( $file_name, $options );
            $this->logingContentReal('олучить реал контент  -  боевой  - ответ получен' );
        }
        return $response;
    }


    public function getContentMulti($fileNameArray = array(), $options = array() ){
        $returnResult = array();
        $this->logingContentMulty( '' );
        $this->logingContentMulty('начинаем получить реал контент мульти' );
        $this->logingContentMulty("\t\t".'получить реал контент файл '. print_r( $fileNameArray , true ) );
        $this->logingContentMulty("\t\t".'опции '. print_r( $options , true ) );
        $fileNameFullArray = array();
        $responseResult = array(); //это то что отдадим
        try{
                $this->logingContentMulty("\t\t".'начинаем получить реал контент  мулти' );
                $curlMulti = curl_multi_init();
                $tasks = array();
                //тестовый
                foreach( $fileNameArray as $keyFile => $file_name ){
                    $this->logingContentMulty("\t\t".'начин готовить отправк  одного'  );
                    //$response = array("status" => "error", "result" => 'не смогло построить запрос');
                    if( empty($options['isFullPathFile']) ){ //путь относителен
                        $fileNameFull = FILES_PATH.$file_name;
                    }
                    else{ //путь полный
                        $fileNameFull = $file_name;
                    }
                    $fileNameFull = str_replace('//', '/', $fileNameFull);
                    $fileNameFull = str_replace( '/public/uploads/uploads/', '/public/uploads/' , $fileNameFull );
                    if( !is_file($fileNameFull) ){
                        throw new \Exception(   $this->logingContentMulty( 'не найден файл в файлохранилище '.$fileNameFull  )  );
                    }
                    $this->logingContentMulty("\t\t\t".'заливаем в курл  '  );

                    $realName = empty( $realName )?$fileNameFull:$realName;
                    $realName = basename( $realName );
                    $options['realFileName'][$keyFile] = $realName;
                    $fileName = (explode('/',$file_name) );
                    $fileName = array_filter($fileName);
                    $fileNameOnly  = end($fileName);
                    $url           = 'http://'
                        .'isga.obrnadzor.gov.ru'
                        . '/service/index/get-sign-content';

                    $ch            = curl_init();
                    $data = array();
                    $data['file']  = curl_file_create( $fileNameFull );
                    curl_setopt($ch, CURLOPT_URL, $url);
                    //curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_NOBODY, false);
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    //curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true );
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    //curl_setopt($ch, CURLOPT_ENCODING, 'utf-8');
                    //curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/17.0 Firefox/17.0');
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
                    curl_setopt($ch, CURLOPT_VERBOSE, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: multipart/form-data;' ) );
                    //curl_setopt($ch,CURLOPT_HTTPHEADER,array("Expect:"));
                    $this->logingContentMulty("\t\t\t".'готовим выдырку - курл  '  );
                    $this->logingContentMulty("\t\t\t".'готовим содержимого - файл '.  $fileNameFull  );
                    $this->logingContentMulty("\t\t\t".'готовим содержимого - размер файла '.  number_format( filesize( $fileNameFull ) , 0 , ',' , ' '  )  );
                    $this->logingContentMulty("\t\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                    $tasks[ $fileNameFull ] = $ch;
                    $tasksToId[ $fileNameFull ] = $keyFile;
                    curl_multi_add_handle($curlMulti, $ch);
                }

                $this->logingContentMulty('курл мулти отправляем' );
                // количество активных потоков
                $active = null;
                // запускаем выполнение потоков
                do {
                    $mrc = curl_multi_exec( $curlMulti , $active);
                }
                while ($mrc == CURLM_CALL_MULTI_PERFORM);
                $this->logingContentMulty('курл мулти отправлены ' . print_r( $curlMulti ,true ) );

                // выполняем, пока есть активные потоки
                while ($active && ($mrc == CURLM_OK)) {
                    // если какой-либо поток готов к действиям
                    if (curl_multi_select($curlMulti) != -1) {
                        // ждем, пока что-нибудь изменится
                        do {
                            //$this->logingContentMulty("\t\t".'есть ответ 9 '.print_r($mrc , true )  );
                            $mrc = curl_multi_exec($curlMulti, $active);
                            //$this->logingContentMulty("\t\t".'есть ответ з '.print_r($mrc , true ) );
                            $info = curl_multi_info_read($curlMulti);
                            //$this->logingContentMulty("\t\t".'получен ответ 1 -  '.print_r( $info, true )  );
                            // получаем информацию о потоке
                            //$info = curl_multi_info_read($cmh);
                            //$this->logingContentMulty("\t\t".'получен ответ 2 - '.print_r( $info , true  ) );
                            // если поток завершился
                            if ($info['msg'] == CURLMSG_DONE) {
                                $this->logingContentMulty("\t\t".'есть ответ  '  );
                                $this->logingContentMulty("\t\t\t".'есть ответ - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                                $ch = $info['handle'];
                                // ищем урл страницы по дескриптору потока в массиве заданий
                                $fileNameFull = array_search($ch, $tasks);
                                $id = $tasksToId[ $fileNameFull ];
                                $this->logingContentMulty("\t\t\t".'исходный ид '. $id  );
                                //$this->logingContentMulty("\t\t".'Это ответ  '.print_r( $url ,true )  );
                                // забираем содержимое
                                //$tasks[$url] = curl_multi_getcontent($ch);
                                $result = curl_multi_getcontent($ch);
                                //$this->logingContentMulty("\t\t".'Это ответ - 2  '.print_r( $result ,true )  );
                                $optionsSend   = curl_getinfo($ch);
                                //$this->logingContentMulty("\t\t".'Это ответ - 3  '.print_r(  $optionsSend ,true )  );

                                if( 200 == $optionsSend['http_code'] ){
                                    $this->logingContentMulty("\t\t".'есть ответ - курл мулти - ответ код валиден' );
                                    try{
                                        $response = \Zend\Json\Json::decode( $result , \Zend\Json\Json::TYPE_ARRAY );
                                        $this->logingContentMulty("\t\t\t".'выдергивание содержимогои - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                                        unset( $result );
                                        //$this->logingContentMulty('ответ '.print_r( $response , true) );
                                        if( 'error' == $response["status"] ){
                                            $this->logingContentMulty("\t\t".'выденрку содержимого- курл - ответ не валиден '.print_r( $response , true)  );
                                            throw new \Exception( 'Ошибка сервера проверки подписи: '.$response[ 'result' ] );
                                            //$response = array("status" => "error", "result" => $response['result'] );
                                        }
                                        else{
                                            $response["file_content"] =  base64_decode( $response["file_content"] );
                                            if( !empty( $options['isPutFile'] ) ){
                                                //$punFileName = dirname($fileNameFull).'/'.basename($response["file_name"]);
                                                $this->logingContentMulty("\t\t\t".'сохраняемся обязательно' );
                                                $punFileName = dirname($fileNameFull).'/'.basename($response["file_name"]);
                                                $this->logingContentMulty("\t\t\t".'файл сохранился как '.$punFileName  );
                                                file_put_contents( $punFileName, $response["file_content"] );
                                                unset( $response["file_content"]  );
                                                //unset( $response["file_content"] );
                                                //$returnResult[ $id ] = $punFileName;
                                                $responseResult['file_name'][ $id ] = str_replace( $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' , '' , $punFileName );
                                                $responseResult['file_path'][ $id ] = $punFileName;
                                                $this->logingContentMulty("\t\t\t\t".' сохраняемся обязательно- размер файла '.  number_format( filesize( $punFileName ) , 0 , ',' , ' '  )  );
                                            }
                                            elseif( empty($options['isGetContent']) ){
                                                $this->logingContentMulty("\t\t\t".'сохранился как файл' );
                                                if( empty($options['isFullPathFile']) ){
                                                    $punFileName = FILES_PATH . $response["file_name"];
                                                }
                                                else{
                                                    $punFileName = dirname($fileNameFull).'/'.basename($response["file_name"]);
                                                }
                                                $this->logingContentMulty("\t\t".'файл сохранился как '.$punFileName  );
                                                file_put_contents( $punFileName, $response["file_content"] );
                                                $this->logingContentMulty("\t\t\t\t".' сохранился как фай- размер файла '.  number_format( filesize( $punFileName ) , 0 , ',' , ' '  )  );
                                                unset( $response["file_content"] );
                                            }
                                            else{
                                                $this->logingContentMulty("\t\t\t".'отдадим контент' );
                                                $responseResult["file_content"][ $id ] = $response["file_content"] ;
                                            }
                                        }
                                        $this->logingContentMulty("\t\t".'есть ответ - ответ обработан - памяти съело '. number_format( memory_get_usage()  , 0 , ',' , ' '  ) );
                                        $this->logingContentMulty( '' );
                                    }
                                    catch( \Exception $ex) {
                                        $this->logingContentMulty('Вывпало в ошибку "3" - '.$ex->getMessage() );
                                        if( !empty( $result) ){
                                            $this->logingContentMulty("\t\t". $result );
                                        }
                                        if( !empty( $response ) ){
                                            $this->logingContentMulty("\t\t". print_r( $response ,true  ) );
                                        }
                                        throw new \Exception(  $ex->getMessage() );
                                    }
                                }
                                else{
                                    $this->logingContentMulty('выдергивание содержимого - курл - ответ кривой'.print_r( $result ,true ).print_r( $optionsSend ,true ) );
                                    throw new \Exception( 'выдергивание содержимого прошло с ошибкой' );
                                }
                                // удаляем поток из мультикурла
                                curl_multi_remove_handle($curlMulti, $ch);
                                // закрываем отдельное соединение (поток)
                                curl_close($ch);
                            }
                            else{
                                //$this->logingContentMulty("\t\t".'ожидаем  ответ - 5  '.  print_r($info , true)  );
                            }
                        }
                        while ($mrc == CURLM_CALL_MULTI_PERFORM);
                        //$this->logingContentMulty("".' курл - получене окончено' );
                    }
                    else{
                        //$this->logingContentMulty("\t\t".'ожидает ответ - 7  '  );
                    }
                }

                // закрываем мультикурл
                curl_multi_close($curlMulti);
                $responseResult[ 'status' ] = 'ok';

            $this->logingContentMulty('типа результат ------------>>>>>>>>>>>>>>>>>>>>>>>>>>>'.print_r( $responseResult ,true ) );
            $this->logingContentMulty('исходный файл '. print_r( $fileNameArray , true ) );

        //$response = array("status" => "error", "result" => 'облом') ;
        } catch (\Exception $ex) {
            $responseResult = array("status" => "error", "result" => $ex->getMessage() );
        }
        return $responseResult;
    }


    public function logingContentBoy( $string ){
        $logFileContentBoy    = 'public/log/to_dss/boy/send_file_to_DSS_boy_'.date('Y-m-d').'.log';
        $this->logerClass->mailLog( $string , $logFileContentBoy );
    }

    public function logingContentMulty( $string ){
        $logFileContentMulty  = 'public/log/to_dss/multi/send_file_to_DSS_multi_'.date('Y-m-d').'.log';
        $this->logerClass->mailLog( $string , $logFileContentMulty );
    }

    public function logingContentReal( $string ){
        $logFileContentMulty  = 'public/log/to_dss/real/send_file_to_DSS_real_'.date('Y-m-d').'.log';
        $this->logerClass->mailLog( $string , $logFileContentMulty );
    }

    public function loging( $string ){
        $logFile              = 'public/log/to_dss/send_file_to_DSS_'.date('Y-m-d').'.log';
        $this->logerClass->mailLog( $string , $logFile );
    }
    //последняя строка

}
?>
