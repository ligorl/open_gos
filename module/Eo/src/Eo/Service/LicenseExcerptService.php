<?php

namespace Eo\Service;

use Eo\Model\License;
use Model\Gateway\BaseEntity;
use Model\Gateway\LodBaseEntity;
use XMLWriter;

/**
 * получение, обработка, и вывод данных выписки лицензий 
 * @author Ruslan
 */
class LicenseExcerptService
{
    private
      $docName = 'excerpt_license',
      $docType = 'docx',
      $generateDocName = 'vipiska_license_',
      $generteDocType = 'pdf',
      $includeProgram = true,
      //Эквиваленты для номеров месяца
      $MONTH_EQUIVALENT = [
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        ];

        public function setIncludeProgram($bool) {
            $this->includeProgram = $bool;
        }
        
        public function getIncludeProgram() {
            return $this->includeProgram;
        }
    
    /**
     * Получает путь к документу шаблона
     * @return string
     */
    protected function getDocumentPath(){
        //local
//        return  $_SERVER['DOCUMENT_ROOT'] . '/templates/';
        //global
        return $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
    }
    
    /**
     * Получает имя документа
     * @return string
     */
    protected function getDocumentName(){
        return $this->docName;
    }

    /**
     * Получает тип документа
     * @return string
     */
    protected function getDocumentType(){
        return $this->docType;
    }
    
    /**
     * Получает тип для сгененрированого документа
     * @return string
     */
    protected function getGenerateDocumentType(){
        return $this->generteDocType;
    }

    /**
     * Получает имя для сгененрированого документа
     * @$addStr string
     * @return string
     */
    protected function getGenerateDocumentName( $addStr = null ){
        $str = $this->generateDocName;
        
        if( !empty($addStr) ){
            $str .= $addStr;
        }
        return $str;
    }
    
    /**
     * Получает текущую дату
     * @return string
     */
    public function now(){
        $dt = new \DateTime('now');
        return $dt->format('d.m.Y');
    }
    
    /**
     * Получает день из текущий даты
     * @param string $date
     * @return string
     */
    public function getDay( $date ){
        $dt = new \DateTime($date);
        return $dt->format('d');
    }
    
    /**
     * Получает день из текущий даты
     * @param string $date
     * @return string
     */
    public function getMonth( $date ){
        $dt = new \DateTime($date);
        $m = (int)$dt->format('m');
        $meq = $this->MONTH_EQUIVALENT;
        return $meq[$m];
    }
    
     /**
     * Получает день из текущий даты
     * @param string $date
     * @return string
     */
    public function getYear( $date ){
        $dt = new \DateTime($date);
        return $dt->format('Y');
    }
    
     /**
     * Получает объект склонялки
     * @return \Eo\Model\Licensing\Tools\Morphy(
     */
    public function getMorphy() {
        return new \Eo\Model\Licensing\Tools\Morphy();
    }
    
    /**
     * Получаем строку в дательном падеже
     * @param string $str
     * @return string
     */
    public function getDativeFromStr($str) {
        $morphy = $this->getMorphy();
        $forms = $morphy->getForms($str);
        return $forms['dative'];
    }
    
    /**
     * Получает Инициализированный объект лицензии
     * @param string $licId
     * @return \Model\LicReestrEntities\license
     */
    public function getLicenseFromId($licId) {
        return new \Model\LicReestrEntities\license($licId);
    }
    
    /**
     * Объект лицензии по дефолтному ид
     * @param string $licId
     * @return \Model\LicReestrEntities\license
     */
    public function getLicenseFromDefId($licId) {
        $lic = new \Model\LicReestrEntities\license();
        return $lic->getByWhere([
            'guid' => $licId
        ]);
    }
    
    /**
     * Получаем полное имя из объекта лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getFullName($lic){
        $str = '';
        $eoId = $lic->getField('eo_id');
        $eo = new \Model\LicReestrEntities\eo($eoId);
        
        if( !empty($eo->getFieldOrSpace('name')) ){
             $str .= $eo->getFieldOrSpace('name');
        }
        
        if( !empty($eo->getFieldOrSpace('short_name')) ){
             $str .= ', '.$eo->getFieldOrSpace('short_name');
        }
        return $str;
    }
    
    /**
     * Получаем регион по ид
     * @param string $regionId
     * @return string
     */
    public function getRegion($regionId) {
        if(empty($regionId)){
            return '';
        }
        $region = new \Model\LicReestrEntities\region($regionId);
        return $region->getFieldOrSpace('name');
    }
    
    /**
     * Получаем объект ео по ид
     * @param string $eoId
     * @return \Model\LicReestrEntities\eo
     */
    public function getEo($eoId){
        return new \Model\LicReestrEntities\eo($eoId);
    }
    
    /**
     * Получаем имя органа лицензирования по ид
     * @param string $licOrganId
     * @return string
     */
    public function getLicenseOrgan($licOrganId) {
        if(empty($licOrganId)){
            return '';
        }
        $licOrgan = new \Model\LicReestrEntities\licenseorgan($licOrganId);
        return $licOrgan->getFieldOrSpace('name');
    }
    
    /**
     * Получаем дату окончания
     * @param string $lic
     * @return string
     */
    public function getDateEnd($lic) {
        if($lic->getField('end_date') == '0000-00-00'){
            return 'Бессрочно';
        }
        return $lic->getDate('end_date');
    }
    
    /**
     * Получаем статус по ид
     * @param string $stateId
     * @return string
     */
    public function getState($stateId) {
        if(empty($stateId)){
            return '';
        }
        $state = new \Model\LicReestrEntities\licensestate($stateId);
        return $state->getFieldOrSpace('name');
    }
    
    /**
     * Получаем статус по коду
     * @param string $stateCode
     * @return string
     */
    public function getStateFromCode($stateCode) {
        if(empty($stateCode)){
            return '';
        }
        $state = new \Model\LicReestrEntities\licensestate();
        $state = $state->getByWhere([
            'code' => $stateCode
        ]);
        if(empty($state)){
            return '';
        }
        return $state->getFieldOrSpace('name');
    }
    
    /**
     * Получаем приложения к лицензии
     * @param string $licId
     * @return \Model\LicReestrEntities\supplement
     */
    public function getSupplement($licId) {
        $sup = new \Model\LicReestrEntities\supplement();
        return $sup->getAllEntities( null, new \Zend\Db\Sql\Expression("INET_ATON(SUBSTRING_INDEX(CONCAT(supplement.number,'.0.0.0'),'.',4))"), ['license_id'=>$licId]);
    }
    
    /**
     * Получаем данные decision приложения
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getSupDecision($lic){
        $licId = $lic->getField('guid');
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_attach_guid' => $licId,
            'type_guid' => 'e0e8eb9952464ea798f7e41b6a81c93f',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    /**
     * Получаем данные остановки приложения
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getSupStop($lic){
        $licId = $lic->getField('guid');
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_attach_guid' => $licId,
            'type_guid' => '1dc3e282134a4b7e9578216e6f07f096',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    /**
     * Получаем данные suspended приложения
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getSupSuspended($lic){
        $licId = $lic->getField('guid');
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_attach_guid' => $licId,
            'type_guid' => '613877a0a16b4822af2b3319b7559771',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        $decisions = $decision->getAllByWhere([
            'license_attach_guid' => $licId,
            'type_guid' => 'b69be1738ca6495cb76cef90ab3ae102',
        ]);
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
        }
        
        return $str;
    }
    
    /**
     * Получаем данные валидности приложения
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getSupValid($lic){
        $licId = $lic->getField('guid');
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_attach_guid' => $licId,
            'type_guid' => '992cdd6800d3486ab919f323d7e312f2',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    /**
     * Получаем данные приложений из rowset
     * @param \Model\LicReestrEntities\supplement $sups
     * @return array
     */
    public function getDateFromSupplement($sups) {
        $supsData = [];
        
        
        foreach ($sups as $el) {
            $eoId = $el->getField('eo_id');
            $eo = $this->getEo($eoId);
            $stateId = $el->getFieldOrSpace('state_code');
            $decision = $this->getSupDecision($el);
            $stop = $this->getSupStop($el);
            $susp = $this->getSupSuspended($el);
            $valid = $this->getSupValid($el);
            $programs = $this->getProgrammFromSupId($el->getField('guid'));
            //хот фик исправление получаемого формата адрессов
            if(mb_strpos($el->getFieldOrSpace('addresses'),';') !== false){
                $sAdress = implode('; ', explode(';',$el->getFieldOrSpace('addresses')));
            }else{
                $sAdress = $el->getFieldOrSpace('addresses');
            }
            //Ннекоторые поля дублированы т.к. генератор документа удаляет их после использования       
            $supsData[] = [
                'supplNum' => $el->getFieldOrSpace('number'),
                'SuplOrgName' => $eo->getFieldOrSpace('name'),
                'SupplDecision' => $decision,
                'SupplState' => $this->getStateFromCode($stateId),
                'supplNum2' => $el->getFieldOrSpace('number'),
                'SupplCreate_date' => $el->getDate('create_date'),
                'SupplAddress' => $sAdress,
                'stopSuppl' => $stop,
                'SupllSuspend' => $susp,
                'SupplValid' => $valid,
                'opprograms' => $programs,
            ];
        }
        return $supsData;
    }
    
    /**
     * Получаем номера из данных приложений
     * @param array $supData
     * @return string
     */
    public function getNumbersFromSupData($supData) {
        $str = '';
        $splitter = '';
        
        foreach( $supData as $el){
            $str .= $splitter.$el['supplNum'];
            $splitter = ', ';
        }
        
        return $str;
    }
    
    /**
     * Получаем адреса из данных приложений
     * @param array $supData
     * @return string
     */
    public function getAddresFromSupData($supData) {
        $str = '';
        $splitter = '';
        
        foreach( $supData as $el){
            $str .= $splitter.$el['SupplAddress'];
            $splitter = ', ';
        }
        
        return $str;
    }
    
    /**
     * Получаем данные decision лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getDecision($lic){
        $licId = $lic->getId();
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_id' => $licId,
            'type_guid' => 'e0e8eb9952464ea798f7e41b6a81c93f',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    /**
     * Получаем данные о дубликате лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getDuplicate($lic){
        $licId = $lic->getId();
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_id' => $licId,
            'type_guid' => 'e6b01d23057f44bda3c1c6d07e234f99',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    /**
     * получаем данные suspended лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getSuspended($lic){
        $licId = $lic->getId();
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_id' => $licId,
            'type_guid' => '613877a0a16b4822af2b3319b7559771',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        $decisions = $decision->getAllByWhere([
            'license_id' => $licId,
            'type_guid' => 'b69be1738ca6495cb76cef90ab3ae102',
        ]);
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
        }
        
        return $str;
    }
    
    /**
     * Получаем данные валидности лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getValid($lic){
        $licId = $lic->getId();
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_id' => $licId,
            'type_guid' => '992cdd6800d3486ab919f323d7e312f2',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    /**
     * Получаем данные остановки лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getStop($lic){
        $licId = $lic->getId();
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_id' => $licId,
            'type_guid' => '1dc3e282134a4b7e9578216e6f07f096',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    /**
     * Получаем данные аннулирования лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getAnnuled($lic){
        $licId = $lic->getId();
        $decision = new \Model\LicReestrEntities\decision();
        $decisions = $decision->getAllByWhere([
            'license_id' => $licId,
            'type_guid' => '046fe9790a3840f2bb4c600064ad725d',
        ]);
        
        $str = '';
        $splitter = '';
        
        foreach( $decisions as $el){
            $str .= $splitter.$el->getFieldOrSpace('type_name').' №'.$el->getFieldOrSpace('number').' от '.$el->getDate('date');
            $splitter = '; ';
        }
        
        return $str;
    }
    
    public function getProgrammFromSupId( $supid ){
        if(!$this->getIncludeProgram()){
            return [];
        }
        $programs = new \Model\LicReestrEntities\program();  
        $programs = $programs->getProgramsByDefaultSupId($supid);
        $programBlock = [];
        $programsToRet = [];
        $counter = 1;
        
        foreach( $programs as $pr ){
            
            if($counter == 1 ){
                $programsToRet[] = [
                    'opcount' => '№ п.п.',
                    'opcode' => 'Код ОП',
                    'opname' => 'Наименование ОП',
                    'oplevel' => 'Уровень образования',
                    'opqualification' => 'Квалификация',
                ];
            }
            
            $program = [
                'opcount' => $counter,
                'opcode' => $pr->getFieldOrSpace('code'),
                'opname' => $pr->getFieldOrSpace('name'),
                'oplevel' => $pr->getFieldOrSpace('type'),
                'opqualification' => $pr->getFieldOrSpace('qualification'),
            ];
            $programsToRet[] = $program;
            $counter++;
        }
        
        if(count($programsToRet)>0){
            $programBlock['opprogramTitle'] = 'Программы приложения';
           $programBlock['opprogramData'] = $programsToRet;
           $ret = [];
           $ret[] = $programBlock; 
           return $ret;
        }
        
        return [];
    }
    /**
     * Получаем данные из лицензии
     * @param \Model\LicReestrEntities\license $lic
     * @return type
     */
    public function getDateFromLic($lic) {
        $dateIssue = $lic->getField('issue_date');
        $day = $this->getDay($dateIssue);
        $month = $this->getMonth($dateIssue);
        $year = $this->getYear($dateIssue);
        $date = '"'.$day.'" '.$month.' '.$year.'г.';
        $eoId = $lic->getFieldOrSpace('eo_id');
        $eo = $this->getEo($eoId);
        $regionId = $eo->getFieldOrSpace('region_id');
        $licOrganId = $lic->getFieldOrSpace('license_organ_id');
        $stateId = $lic->getFieldOrSpace('license_state_id'); 
        $sups = $this->getSupplement($lic->getId());
        $supsData = $this->getDateFromSupplement($sups, $lic);
        $supsNum = $this->getNumbersFromSupData($supsData);
        $suppAdres = $this->getAddresFromSupData($supsData);
        $duplicate = $this->getDuplicate($lic);
        $decision = $this->getDecision($lic);
        $suspended = $this->getSuspended($lic);
        $valid = $this->getValid($lic);
        $stop = $this->getStop($lic);
        $annuled = $this->getAnnuled($lic);
        
        //Ннекоторые поля дублированы т.к. генератор документа удаляет их после использования        
        $data = [
            'fieldOrgAllNames' => $this->getFullName($lic),
            'LicIssue_date' => $date,
            'now' => $this->now(),
            'LicNumber' => $lic->getFieldOrSpace('number'),
            'LicSer' => $lic->getFieldOrSpace('doc_serie'),
            'LicSerNumber' => $lic->getFieldOrSpace('doc_number'),
            'suppNubers' => $supsNum,
            
            'region' => $this->getRegion($regionId),
            'license_organ' =>  $this->getLicenseOrgan($licOrganId),
            'OrgName' => $eo->getFieldOrSpace('name'),
            'OrgShortName' => $eo->getFieldOrSpace('short_name'),
            'OrgAddress' => $eo->getFieldOrSpace('address'),
            'SupplAddress' => "Место (места) осуществления образовательной деятельности указано (указаны) в приложении (приложениях) к настоящей лицензии",//$suppAdres,
            'INN' => $eo->getFieldOrSpace('inn'),
            'OGRN' => $eo->getFieldOrSpace('ogrn'),
            'KPP' => $eo->getFieldOrSpace('kpp'),
            'LicSer' => $lic->getFieldOrSpace('doc_serie'),
            'LicSerNumber' => $lic->getFieldOrSpace('doc_number'),
            'LicNumber2' => $lic->getFieldOrSpace('number'),
            'LicSer2' => $lic->getFieldOrSpace('doc_serie'),
            'LicSerNumber2' => $lic->getFieldOrSpace('doc_number'),
            'LicDecision' => $decision,
            'LicEnd_date' => $this->getDateEnd($lic),
            'license_state' => $this->getState($stateId),
            'LicDuplicate' => $duplicate,
            'LicSuspend' => $suspended,
            'LicValid' => $valid,
            'stopLic' => $stop,
            'LicAnnulled' => $annuled,  
            
            'sups' => $supsData
        ];
        return $data;
    }
    
    /**
     * Получаем остальные лицензии 
     * @param \Model\LicReestrEntities\license $lic
     * @return string
     */
    public function getLastLicense($lic) {
        $licId = $lic->getId();
        $DualEoId = $lic->getField('eo_guid');
        $explodeArr = explode ( '_' , $DualEoId );
        
        if(isset($explodeArr[1])){
           $license = new \Model\LicReestrEntities\license();
            return $license->getToExcerpt($explodeArr[1], $licId); 
        }
        return [];
    }
    
    /**
     * Возвращает полный путь по умолчанию
     * @return string
     */
    private function getDefaultPath() {
        return $this->getDocumentPath().$this->getDocumentName().'.'.$this->getDocumentType();
    }
    
    /**
     * Генерируем документ лицензии
     * @param string $licId
     */
    public function generate($licId){
        $lic = $this->getLicenseFromDefId($licId);
        $licData = $this->getDateFromLic($lic);
        $lastLicenses = $this->getLastLicense($lic);
        $licData['lastLic'] = [];
        $titleToLast = 'ДРУГИЕ ЛИЦЕНЗИИ';
        
        foreach ($lastLicenses as $lLic) {
            $lastData = $this->getDateFromLic($lLic);
            $lastData['titleToLast'] = $titleToLast;
            $licData['lastLic'][] = $lastData;
            $titleToLast = '';
        }
        $templater = new \Ron\Model\Templater(
            $this->getDefaultPath(),
            $licData,
            $this->getGenerateDocumentType(),
            $this->getGenerateDocumentName( '_№'.$lic->getFieldOrSpace('number') )
        );
        //генерируем вывод
//        echo '<pre>';
//        var_dump($licData);
//        echo '</pre>';
//        die;
        $templater->makeDocumentOut();
        die;
    }
}