<?php

namespace Eo\Service;

use Model\Gateway\EntitiesTable;

class Synchronization
{

    CONST TMP_DIRECTORY = '_karta_tmp_';

    /**
     *
     * @var array
     */
    protected $tableColumns = array(
        'eiis_Licenses'            => array(
            'LicenseRegNum',
            'DateLicDoc',
            'SerDoc',
            'NumDoc',
            'fk_eiisLicenseState',
            'DateEnd',
            'fk_eiisEducationalOrganization',
        ),
        'isga_Certificates'        => array(
            'fk_eiisEducationalOrganization',
            'RegNumber',
            'fk_isgaCertificateStatus',
            'fk_isgaCertificateType',
            'SerialNumber',
            'FormNumber',
            'DateEnd',
            'DateIssue',
        ),
        'eiis_LicenseStates'       => array(
            'Id',
            'Name',
            'WebName',
            'IsValid',
        ),
        'isga_CertificateStatuses' => array(
            'Id',
            'Name',
            'Code',
            'DateCreate',
            'DateUpdate',
            'EiisId',
        ),
        'isga_CertificateTypes'    => array(
            'Id',
            'Name',
            'Code',
        ),
    );

    public function dump_table($table_name)
    {
        $name_file = md5(time() . rand(0, 1000) . rand(0, 1000)) . '.txt';
        $tmpFile = getcwd() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::TMP_DIRECTORY . DIRECTORY_SEPARATOR . $name_file;
        $table = new EntitiesTable($table_name);
        $columns = $this->tableColumns[$table_name];
        $data = $table->select(function($select) use ($columns) {
            $select->columns($columns);
        });
        $this->writeInTmpFile($data, $tmpFile);
        return $name_file;
    }

    /**
     *
     * @param \Zend\Db\ResultSet\ResultSetInterface $data
     * @param string $tmpFile
     */
    protected function writeInTmpFile(\Zend\Db\ResultSet\ResultSetInterface $data, $tmpFile)
    {
        $fh = fopen($tmpFile, 'a+');
        foreach ($data as $row) {
            $arrRow = $row->toArray();
            foreach ($arrRow as &$value) {
                $value = $this->clearValue($value);
            }
            unset($value);
            $serialized = serialize($arrRow);
            if (!$serialized) {
                throw new \Exception("Не валидна строка таблицы. ");
            }
            fwrite($fh, $serialized . "\n");
        }
        fclose($fh);
        return TRUE;
    }

    /**
     *
     * @param string | integer $value
     * @return string | integer
     */
    protected function clearValue($value)
    {
        return str_replace(array("\r\n", "\r", "\n", "\t"), '', $value);
    }

}
?>