<?php

namespace Eo\EloquentModel;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class IsgaNotification extends EloquentModel
{

    protected $connection = 'f11_mon_open';
    protected $table = 'isga_notification';
    public $timestamps = false;
}