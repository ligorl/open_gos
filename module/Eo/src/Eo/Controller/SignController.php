<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

class SignController extends AbstractActionController
{

    public function indexAction()
    {
		ini_set("memory_limit", "1000M");
		$sign_class = new \Eo\Service\Sign();
		$sign_data = $sign_class->send_file_to_DSS('test.zip.sig', true);
		var_dump($sign_data);die;
		/*die;
        //$e = $sign_class->verify_pdf_signature( "for_test_sign_pdf.pdf" );
        //var_dump($e);die;

        if ($result["status"] == "error") {
            print $sign_class->getErrorText($result['result']);
            die;
        }

        $sign_info = $result;
        var_dump($sign_info);
        die;*/

    }

    public function getFunctionsAction()
    {
        try {
            // $soap = new \SoapClient( 'http://192.168.67.141/verify/service.svc?wsdl' );
            $soap = new \Zend\Soap\Client('http://dss.cryptopro.ru/Verify/service.svc?wsdl');
        } catch (\SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})",
                          E_USER_ERROR);
            $soap = false;
            var_dump("Нет соединения с сервером проверки подписи!");
            die;
        }
        try {
            var_dump($soap->getFunctions());
        } catch (\SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})",
                          E_USER_ERROR);
        }
        /* $sign_class = new \Eo\Service\Sign();
          print_r($sign_class->get_soap_functions());
          die; */
    }

    public function validNameAction()
    {
        $name = "nnnnn.pdf.sig";
        $sign_class = new \Eo\Service\Sign();

        $validate_name = $sign_class->get_hash_name($name);
        var_dump($validate_name);
        die;
    }

    public function getDialogSignInfoAction()
    {
        $sign_id = $this->getRequest()->getPost("sign_id", NULL);
        $signInfoFilesClass = new \Model\Entities\isgaSignInfoFiles();

        $data = $signInfoFilesClass->selectComplicated("isga_SignInfoFiles",
                                                       array('Id', 'date_verify', 'valid', "message", "certificate_info"),
                                                       array('Id' => $sign_id));


        $view = new ViewModel();
        $view->setVariables(array(
            "data" => $data->toArray()
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function unsignAction()
    {
        $sign_class = new \Eo\Service\Sign();
        $res = $sign_class->unsign("report.txt.sig");
        print_r($res);

        die;
    }

    public function infoAction()
    {
        $sign_class = new \Eo\Service\Sign();

        // $sign_class->init_soap();
        $filepath = $_SERVER['DOCUMENT_ROOT'] . '/public/' . '90df8b8e3c28333e6b702dbb48c80eec.pdf.sig';

        $sign_class->get_real_name('90df8b8e3c28333e6b702dbb48c80eec.pdf.sig');

        $signature_type = $sign_class->get_type_sign($filepath);
        $binary_file = fread(fopen($filepath, "r"), filesize($filepath));


        $params = new \stdClass();
        $params->signatureType = $signature_type;
        $params->document = $binary_file;
        $params->verifyParams = array(array('Key' => 'ExtractContent', 'Value' => 'true'));
        $answer_from_DSS = $sign_class->soap->GetSignersInfo($params);

        $content = $answer_from_DSS->GetSignersInfoResult->AdditionalInfo->Content;

        $result = file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/public/' . 'sagaida.pdf', $content);

        die;
    }



}