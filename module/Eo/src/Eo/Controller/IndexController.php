<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Eo\EqueueFormm;
use Eo\Form\EqueueForm;
use Zend\Db\ResultSet\ResultSet;
use Zend\Form\Element\DateTime;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Validator\Date;
use Zend\View\Model\ViewModel;
use Model;
use Model\LodEntities\ElectronicQueueEntry;
use Model\LodEntities\ServiceReasonCatalog;
use Model\LodEntities\ElectronicQueueReason;
use Model\LodEntities\NumberOfProgramCatalog;
use Model\LodEntities\NumberOfDocumentsCatalog;
use Model\LodEntities\IssueDocumentsAmount;
use Model\Entities\isgaApplicationReasons;
Use Model\LodEntities\LicenseReasonCatalogItem;
use Model\LodEntities\ElectronicQueueEntryToAccreditationReason;
use Model\LodEntities\ElectronicQueueEntryToLicenseReasonCatalogItem;

class IndexController extends AbstractActionController
{
    protected $isTest = false;

    public function indexAction()
    {
        //TODO временный кусок
        if (isset($_SERVER['SERVER_NAME']) && in_array($_SERVER['SERVER_NAME'],array('85.192.47.4','146.120.224.100', 'f114', 'f-11-4'))) {
            $this->isTest = true;
        }

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();

        $this->layout()->identityUser = $user;
        //Подключаем js файл

        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/eo/main.js');
        $headStuff->appendFile('/js/eo/Calculator.class.js');
        $headStuff->appendFile('/js/tipsy/jquery.tipsy.js');
        $headStuff->appendFile('/js/jquery.form.min.js');


        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        $headStuff->appendStylesheet('/js/tipsy/tipsy.css');


        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $educationOrganization ;

        return new ViewModel(array(
            "eo" => $educationOrganization,
            'isTest' =>  $this->isTest
        ));
        return new ViewModel();
    }

    public function infoAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $educationOrganization ;

        $specTable = new \Model\Gateway\EntitiesTable("isga_EducationalOrganizationSpecifics");
        $specifics = $specTable->getAllEntities()->getObjectsArray();

        $orgData = $educationOrganization->getEducationalOrganizationData();
        if($orgData == false){

            //Проверяем если по отдельности фио руководителя не задано, но задано одним полем
            if($educationOrganization->isEmptyField('ContactSecondName')
                && $educationOrganization->isEmptyField('ContactFirstName')
                && $educationOrganization->isEmptyField('ContactLastName')
                && !$educationOrganization->isEmptyField('ChargeFio')
            ){
                $fio = explode(" ",$educationOrganization->getField('ChargeFio'));
                if(isset($fio[0])) $educationOrganization->setField('ContactSecondName',$fio[0]);
                if(isset($fio[1])) $educationOrganization->setField('ContactFirstName', $fio[1]);
                if(isset($fio[2])) $educationOrganization->setField('ContactLastName', $fio[2]);
            }

            //Получаем лицензию
            $license = $educationOrganization->getLicense();

            $orgData = array(
                "Phones" => $educationOrganization->getField("Phones"),
                "Faxes" => $educationOrganization->getField("Faxes"),
                "Emails" => $educationOrganization->getField("Mails"),
                "Site" => $educationOrganization->getField('Www'),
                "ChiefSurname" => $educationOrganization->getField('ContactSecondName'),
                "ChiefName" => $educationOrganization->getField('ContactFirstName'),
                "ChiefPatronymic" => $educationOrganization->getField('ContactLastName'),
                "ChiefPosition" => $educationOrganization->getField('ChargePosition'),
                "ChiefPhones" => "",
                "ChiefFax"  => "",
                "ChiefEmail" => "",
                "PubAccr" => array(
                    array(
                        'organization' => "",
                        'date' => "",
                        'requisite' => ""
                    )
                ),
                "ProfPubAccr" => array(
                    array(
                        'organization' => "",
                        'date' => "",
                        'requisite' => ""
                    )
                ),
                "IntPubAccr" => array(
                    array(
                        'organization' => "",
                        'date' => "",
                        'requisite' => ""
                    )
                ),
                "fk_isgaEducationalOrganizationSpecific" => "0",
                "NameDatCase" => ($license != null)?$license->getField("EONameDatCase"):"",
                "NameInstrCase" => ($license != null)?$license->getField("OrgNameInstrCase"):"",
                "NameAccCase" => "",
                "BankAccountNumber" => "",
                "BankName" => "",
                "BankCorrespondentAccount" => "",
                "BankBik" => ""
            );
        } else {
            $orgData = $orgData->getFields();
        }
        $isgaOrgData = new Model\Entities\isgaEducationalOrganizationData();
        $isgaOrgData = $isgaOrgData->getByWhere(['fk_eiisEducationalOrganization' => $educationOrganization->getField('Id')]);

        $view = new ViewModel();
        $view->setVariables(array(
            "eo" => $educationOrganization,
            "specifics" => $specifics,
            "orgData" => $orgData,
            "isgaOrgData" => $isgaOrgData
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function affiliatesAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $educationOrganization ;
        $affilates = new Model\Gateway\EntitiesTable('eiis_EducationalOrganizations');
        $affilates = $affilates->getAllEntities(null,'FullName ASC','fk_eiisParentEducationalOrganization="'.$educationOrganization->getField('Id').'"');
        $view = new ViewModel();
        $view->setVariables(array(
            "eo" => $educationOrganization,
            "affilates" => $affilates));
            /*"affilates" => $educationOrganization->getAffilates()));*/
        $view->setTerminal(true);
        return $view;
    }

    public function getAffilateInfoAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $user->getEducationOrganization();

        if($this->params("id") != null){
            $id = $this->params("id");
            $educationOrganization = new Model\Entities\eiisEducationalOrganizations($id);
            $educationOrganizationData = new Model\Entities\isgaEducationalOrganizationData();
            $educationOrganizationData = $educationOrganizationData->getById($id,'fk_eiisEducationalOrganization');

            $orgData = $educationOrganization->getEducationalOrganizationData();
            if($orgData == false){

                //Проверяем если по отдельности фио руководителя не задано, но задано одним полем
                if($educationOrganization->isEmptyField('ContactSecondName')
                    && $educationOrganization->isEmptyField('ContactFirstName')
                    && $educationOrganization->isEmptyField('ContactLastName')
                    && !$educationOrganization->isEmptyField('ChargeFio')
                ){
                    $fio = explode(" ",$educationOrganization->getField('ChargeFio'));
                    $educationOrganization->setField('ContactSecondName',$fio[0]);
                    $educationOrganization->setField('ContactFirstName', $fio[1]);
                    $educationOrganization->setField('ContactLastName', $fio[2]);
                }


                $orgData = array(
                    "Phones" => $educationOrganization->getField("Phones"),
                    "Faxes" => $educationOrganization->getField("Faxes"),
                    "Emails" => $educationOrganization->getField("Mails"),
                    "Site" => $educationOrganization->getField('Www'),
                    "ChiefSurname" => $educationOrganization->getField('ContactSecondName'),
                    "ChiefName" => $educationOrganization->getField('ContactFirstName'),
                    "ChiefPatronymic" => $educationOrganization->getField('ContactLastName'),
                    "ChiefPosition" => $educationOrganization->getField('ChargePosition'),
                    "ChiefPhones" => "",
                    "ChiefFax"  => "",
                    "ChiefEmail" => "",
                    "PubAccr" => array(
                        array(
                            'organization' => "",
                            'date' => ""
                        )
                    ),
                    "ProfPubAccr" => array(
                        array(
                            'organization' => "",
                            'date' => ""
                        )
                    ),
                    "IntPubAccr" => array(
                        array(
                            'organization' => "",
                            'date' => ""
                        )
                    ),
                    "fk_isgaEducationalOrganizationSpecific" => "0",
                    "NameGenCase" => "",
                    "NameDatCase" => "",
                    "NameInstrCase" => "",
                    "NameAccCase" => "",
                    "BankAccountNumber" => "",
                    "BankName" => "",
                    "BankCorrespondentAccount" => "",
                    "BankBik" => ""
                );
            } else {
                $orgData = $orgData->getFields();
            }

            $view = new ViewModel();
            $view->setVariables(array(
                "eo" => $educationOrganization,
                "orgData" => $orgData,
                "eoData" => $educationOrganizationData
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    public function licenseAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $educationOrganization ;

        $license = $educationOrganization->getLicense();
        //print_r($license->getFields());
        $supplements = array();
        if($license!=null) $supplements = $license->getSupplements();
        $eP = new Model\Entities\eiisEduProgramTypes();

        $view = new ViewModel();
        $view->setVariables(array(
            "license" => $license,
            "supplements" => $supplements,
            "eo" => $educationOrganization,
            "ep" => $eP->getAllByWhereParseId('1 ORDER BY SortOrder ASC','Id')
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function vipiskaAction() {
        try {
            $documentDebug = $this->params()->fromQuery( 'documentDebug', '');
            $templaterName = $this->params()->fromQuery( 'templaterName', '');
            $ourDocType    = $this->params()->fromQuery( 'ourDocType', '');
            if( empty($templaterName) ){
                $documentClass = new \Islod\Model\Spravka($orgId);
            }
            if( $templaterName == 'LicenseFull' ){
                $documentClass = new \Islod\Model\License\LicenseFull($orgId);
            }
            if( $templaterName == 'Vipiska' ){
                $documentClass = new \Islod\Model\Vipiska($orgId);
            }
            if( empty($documentClass) ){
                throw new \Exception('печать документа - не найден шаблонизатор');
            }
            $documentClass->setDebug($documentDebug);
            $content = $documentClass->makeDocumentOut();
        } catch (\Exception $e) {
            echo 'Возникла ошибака';
            echo '<div id="error" style="display:none;">';
            echo $e->getMessage();
            echo PHP_EOL;
            echo $e->getTraceAsString();
            echo '</div>';
            echo '<script>window.opener.useTopHoverTitleError("Ошибка  создания документа")</script>';
            echo '<script>var error = window.document.getElementById("error");</script>';
            echo '<script>var text=window.opener.jQuery(error).text();</script>';
            echo '<script>window.opener.console.error(text);</script>';
            echo '<script>window.close();</script>';
        }
    }

    public function getLicensesupplementInfoAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $user->getEducationOrganization();

        if($this->params("id") != null){
            $id = $this->params("id");
            $ls = new Model\Entities\eiisLicenseSupplements($id);
            $eP = new Model\Entities\eiisEduProgramTypes();

            $view = new ViewModel();
            $view->setVariables(array(
                "ls" => $ls,
                "ep" => $eP->getAllByWhereParseId('1 ORDER BY SortOrder ASC','Id')
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    /**
     * открытая = главная свидетельсва - выбираем приложения
     * 
     * url:  /eo/index/get-certificatesupplement-info/ид-прил-я
     * 
     * @return ViewModel
     */
    public function getCertificatesupplementInfoAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $user->getEducationOrganization();

        if($this->params("id") != null){

            $cs = new Model\Entities\isgaCertificateSupplements($this->params("id"));
            $programsPrint = $this->certificatesSortByCodeAndLevel( $cs );

            $view = new ViewModel();
            $view->setVariables(array(
                "cs"            => $cs,
                'programsPrint' => $programsPrint,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    /**
     * открытая = главная свидетельсва
     * 
     * url:  /eo/index/certificates
     * 
     *  @return ViewModel
     */
    public function certificatesAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $educationOrganization ;

        $certificates = $educationOrganization->getCertificates("noMakets");

        $settingsClass = new \Model\Gateway\ExpertsEntitiesTable('region_settings');
        $settings = $settingsClass->getEntityById(1);

        //уберем поиск программ, а то багит, подгрузим их потом
        //prostoy://comment/23269693
        //$programsPrint = $this->certificatesSortByCodeAndLevel($certificates);
        $programsPrint = array();
        
        $view = new ViewModel();
        $view->setVariables(array(
            "certificates" => $certificates,
            "settings" => $settings,
            "eo" => $educationOrganization,
            'programsPrint' => $programsPrint,
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * открытая = главная свидетельсва - выбираем свидетельство
     * 
     * url:  /eo/index/get-certificate-info/ид-св-ва
     * 
     * @return ViewModel
     */
    public function getCertificateInfoAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $user->getEducationOrganization();

        if($this->params("id") != null){

            $firstCert = new Model\Entities\isgaCertificates($this->params("id"));

            $view = new ViewModel();
            $view->setVariables(array("firstCert" => $firstCert));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    public function saveOrgDataAction(){
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()){ // If it's ajax call
            $data = $request->getPost('fieldsData');
            if($request->getPost('affId') != null)
                $affId = $request->getPost('affId');

            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            $educationalOrganization = $user->getEducationOrganization();

            if(isset($affId)){
                $workOrg = new \Model\Entities\eiisEducationalOrganizations($affId);
                if($workOrg->getField("Id") == null){
                    echo "error";die;
                }
            } else {
                $workOrg = $educationalOrganization;
            }

            $this->layout()->identityUser = $user;
            $this->layout()->educationOrganization = $educationalOrganization;

            $workOrg->setField("ContactFirstName",$data['ChiefSurname']);
            $workOrg->setField("ContactSecondName",$data['ChiefName']);
            $workOrg->setField("ContactLastName",$data['ChiefPatronymic']);
            $workOrg->setField("ChargeFio",$data['ChiefSurname'].' '.$data['ChiefName'].' '.$data['ChiefPatronymic']);
            $workOrg->setField("ChargePosition",$data['ChiefPosition']);
            $workOrg->save();

            $orgData = $workOrg->getEducationalOrganizationData();

            if($orgData == false){
                $orgData = new \Model\Entities\isgaEducationalOrganizationData();
                $orgData->setField("fk_eiisEducationalOrganization",$workOrg->getField("Id"));
            }

            //исправляем баг несохранения
            //prostoy://comment/23165883
            if( is_array( $data['PubAccr'] ) ){
                $data['PubAccr'] = json_encode( $data['PubAccr'] );
            }
            if( is_array( $data['ProfPubAccr'] ) ){
                $data['ProfPubAccr'] = json_encode( $data['ProfPubAccr'] );
            }
            if( is_array( $data['IntPubAccr'] ) ){
                $data['IntPubAccr'] = json_encode( $data['IntPubAccr'] );
            }
            //print_r($data);
            $orgData->setFieldsSafe($data);
            $orgData->save();
        }
        $view = new ViewModel();
        $view->setVariables(array());
        $view->setTerminal(true);
        return $view;
    }

    public function calculatorAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $educationOrganization;

        //Типы заявления
        $appTypesTable = new \Model\Gateway\EntitiesTable("isga_ApplicationTypes");
        $appTypes = $appTypesTable->getAllEntities(null,"Name ASC", array(
            new \Zend\Db\Sql\Predicate\In("Id",array(
                "b30d724a-17d1-4792-8c75-8f9db2a6ce57",
                "711776cf-7871-4c90-9ed7-9669c0e0356f",
                "550b5c08-99e0-4449-a242-b448e9b1b422",
                "41152d9a-3e6a-4c6b-9a6b-53e246eb8638"
            ))
        ))->getObjectsArray();

        /*
        $firstEduProgramType = new \Model\Entities\eiisEduProgramTypes("C420030B-0B27-CE31-BAB4-CA5DDC09231E");

        $firstEduPrograms = $firstEduProgramType->getEduPrograms()->getObjectsArray();

        //сортировка программ по названию
        usort($firstEduPrograms, function($a_obj, $b_obj){
            $a = $a_obj->getField("Code");
            $b = $b_obj->getField("Code");;
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });

        $view = new ViewModel();
        $view->setVariables(array(
            "firstPrograms" => $firstEduPrograms
        ));
        $view->setTerminal(true);
        return $view;
        */

        $affiliates = $educationOrganization->getAffilates()->getObjectsArray();
        $eos = array();
        $eos[] = $educationOrganization;
        $eos = array_merge($eos,$affiliates);

        $view = new ViewModel();
        $view->setVariables(array(
            "eos" => $eos,
            "appTypes" => $appTypes,
            "firstTypeId" => "b30d724a-17d1-4792-8c75-8f9db2a6ce57"
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function getEduProgramsAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $user->getEducationOrganization();

        if($this->params("id") != null){

            $firstEduProgramType = new \Model\Entities\eiisEduProgramTypes($this->params("id"));

            $firstEduPrograms = $firstEduProgramType->getEduPrograms()->getObjectsArray();

            //сортировка программ по названию
            usort($firstEduPrograms, function($a_obj, $b_obj){
                $a = $a_obj->getField("Code");
                $b = $b_obj->getField("Code");;
                if ($a == $b) {
                    return 0;
                }
                return ($a < $b) ? -1 : 1;
            });

            //Готовим ответ в json
            $retArray = array();
            foreach($firstEduPrograms as $program){
                $retArray[] = array(
                    'Id' => $program->getField("Id"),
                    'Name' => $program->getField("Name"),
                    'Code' => $program->getField("Code"),
                    'ParentEGSId' => $program->getParentEGSId()
                );
            }
            echo json_encode($retArray);
            die;
        }
        die;
    }

    public function getApplicationsAction(){
        $request = $this->getRequest();
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $eo = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $eo;

        if ($request->isXmlHttpRequest()){
            $applications = $eo->getApplications()->getObjectsArray();

            //фильтруем только по тем типам и основаниям, что нам нужны
            /*
            $appsArray = array();
            foreach($applications as $application){
                //Если опубликовано, то пропускаем
                /*
                var_dump($application->getFields());
                echo $application->getField("fk_isgaApplicationStatus")."\n";
                if(
                    $application->getField("fk_isgaApplicationStatus") != "D1203A0D-1262-4022-B640-8E7EBF7E8621"
                    && $application->getField("fk_isgaApplicationStatus") != "075D0CAE-B0F2-4B0A-B75C-A6E452FBA03A"

                )
                    continue;
                /
                if($application->getField("fk_isgaApplicationType") == "b30d724a-17d1-4792-8c75-8f9db2a6ce57"){
                    $appsArray[] = $application;
                    continue;
                }
                if(
                    $application->getField("fk_isgaApplicationType") == "711776cf-7871-4c90-9ed7-9669c0e0356f"
                    && $application->getField("fk_isgaApplicationReason") == "67f97827-57f9-4a0c-b2c7-d9806969bef6"
                ){
                    $appsArray[] = $application;
                    continue;
                }
            }*/

            //Готовим ответ в json
            $retArray = array();
            foreach($applications  as $application){
                $type = $application->getType();
                $tmpArray=array();
                $tmpArray['Id'] = $application->getId();
                $tmpArray['TypeName' ]= $type?$type->getName():'';
                $tmpArray['DateCreate'] = $application->getDate("DateCreate");
                $retArray[] = $tmpArray;
                unset($tmpArray);
            }
            echo json_encode($retArray);
            die;
        }
        die;
    }

    public function getApplicationCalculatorDataAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization  = $user->getEducationOrganization();
        //throw new \Exception('sssssssss');

        if($this->params("id") != null){
            $application = new \Model\Entities\isgaApplications($this->params("id"));

            //Если такой заявки нет в базе
            if($application->getField("Id") == null){
                echo "{error:'true'}";
                die;
            }

            $payments= \Eo\Model\ExportPayment::getPaymentByIdApplication($this->params("id"));
            if($application->getField("Id") == null){
                echo "{error:'true'}";
                die;
            }

            try{
            $data=$payments->getData();
            }  catch (\Exception $e){
                echo json_encode(array(
                    'error'=>'true',
                    'message'=>$e->getMessage(),
                ));
                die;
            }
            if (empty($data)){
               echo "{error:'true'}";
               die;
           }

           $resultTable=array();
           foreach ($data['PAYMENT'] as $value) {
               $filialId=$value['fieldFilialId'];
               //$resultTable[$filialId]=$filialId;
               $resultTable[$filialId]['Id']=$filialId;
               $resultTable[$filialId]['ShortName']=$value['fieldFilialShortName'];
               $alreadySum=empty($resultTable[$filialId]['Sum'])
                 ?0
                 :$resultTable[$filialId]['Sum'];
               $newSum=(int)  str_replace(' ', '',$value['fieldSumNumber']);
               $filialSum= $alreadySum+$newSum;
               $resultTable[$filialId]['Sum']=$filialSum;
           }
           unset($filialId);
           unset($filialSum);

           $toTable=  array_values($resultTable);

            $reason = $application->getField('fk_isgaApplicationReason');
            if($reason == '1'){
                $mtmApplReason = new \Model\Entities\isgamtmApplicationReason();
                $mtmApplReason = $mtmApplReason->getById($application->getField("Id"),'fk_isgaApplication');
                if($mtmApplReason){
                    $reason = $mtmApplReason->getField('fk_isgaApplicationReason');
                }
            }

           $resultArray['type']=$application->getType()->getId();
           $resultArray['reason']=$reason;
           $resultArray['tableData']=$toTable;

           echo json_encode($resultArray);
           die;


            //Проверяем, если тиа - Заявление о предоставлении дубликата свидетельства
            $type = $application->getType();
            $reason = $application->getReason();
            if($type->getField("Id") == "41152d9a-3e6a-4c6b-9a6b-53e246eb8638"){
                $resultArray[] = array(
                    "Id" => "none",
                    "ShortName" =>  $type->getField("Name")." (".$reason->getField("Name").")",
                    "Sum" => "350"
                );


                $res = array(
                    "type" => $application->getField("fk_isgaApplicationType"),
                    "reason" => $application->getField("fk_isgaApplicationReason"),
                    "tableData" => $resultArray
                );
                echo json_encode($res);
                die;
            }

            if($type->getField("Id") == "550b5c08-99e0-4449-a242-b448e9b1b422" ||
            ($type->getField("Id") == "711776cf-7871-4c90-9ed7-9669c0e0356f" && $reason->getField("Id") != "67f97827-57f9-4a0c-b2c7-d9806969bef6")) {
                $resultArray[] = array(
                    "Id" => "none",
                    "ShortName" =>  $type->getField("Name")." (".$reason->getField("Name").")",
                    "Sum" => "3000"
                );

                $res = array(
                    "type" => $application->getField("fk_isgaApplicationType"),
                    "reason" => $application->getField("fk_isgaApplicationReason"),
                    "tableData" => $resultArray
                );
                echo json_encode($res);
                die;
            }

            //======Обрабатываем, подготавливаем инфо для калькулятора
            $retArray = array();

            //список связанных филиалов
            $branches = $application->getLinkedBranches();
            //Проходим по филиалам, раскидываем в массив
            foreach($branches->getObjectsArray() as $branch){
                $retArray[$branch->getField("Id")] = array(
                    "Id" =>  $branch->getField("Id"),
                    "ShortName" => $branch->getField("ShortName"),
                    "Programs" => array(),
                    "Sum" => 0
                );
            }

            //Запрашиваем связанные с заявкой обр программы
            $progams = $application->getLinkedPrograms();

            //Проходим по программам, раскидываем их по филиалам
            foreach($progams->getObjectsArray() as $program){
                $eoId = $program->getField("fk_isgaEOBranches");
                if(isset($retArray[$eoId])){
                    $retArray[$eoId]["Programs"][] = $program;
                }
            }

            //Возвращаемый массив
            $resultArray = array();
            //Теперь подсчитываем сумму, проходим по всем филиалам
            foreach($retArray as $branchData){
                //Обнуляем переменные для подсчета суммы
                $isLowP = false;
                $colMidP = 0;
                $colHighP = 0;

                $midEnlargedP = array();
                $highEnlargedP = array();

                //если Связанных программ 0, переходим к следующему филиалу
                if(count($branchData["Programs"]) == 0)
                    continue;

                foreach($branchData["Programs"] as $program){
                    $licensedProgram = $program->getLicensedProgram();
                    $eduProgramType =  $licensedProgram->getEduProgramType();
                    $eduProgram = $licensedProgram->getEduProgram();

                    //Проверяем какому типу соответствует
                    $eduLevel = $eduProgramType->getField("fk_eiisEduLevels");
                    switch($eduLevel){
                        //High
                        case "3EE753BC-6A60-422F-A740-1AEE51FB7AF0":
                            $EnlargedPId = $eduProgram->getParentEGSId();
                            if(!isset($highEnlargedP[$EnlargedPId])){
                                $highEnlargedP[$EnlargedPId] = true;
                                $colHighP+=1;
                            }
                            break;
                        //
                        //Middle СПО
                        case "21A1833F-396C-4E6E-9DD1-2D64E0348BB1":
                            $EnlargedPId = $eduProgram->getParentEGSId();
                            if(!isset($midEnlargedP[$EnlargedPId])){
                                $midEnlargedP[$EnlargedPId] = true;
                                $colMidP+=1;
                            }
                            break;
                        //Среднее общее образование
                        //2BA482B5-1212-782D-06C3-C99607D06527
                        //Основное общее образование
                        //751479E8-95C4-4FC6-BFBA-BFA65C2706C0


                        //Low Основное общее образование
                        case "495702F5-00D9-4339-8B9E-88D59CBC86B5":
                            $isLowP = true;
                            break;
                        //Начальное общее образование
                        //9C10F98F-7736-4F32-9387-AA6D193A539C

                    }
                }
                //Подсчитываем сумму
                $sum = 0;
                if($isLowP)
                    $sum += 15000;
                $sum += $colMidP * 35000;
                $sum += $colHighP * 100000;
                //Добавляем в возвращаемый массив
                $resultArray[] = array(
                    "Id" => $branchData["Id"],
                    "ShortName" => $branchData["ShortName"],
                    "Sum" => $sum
                );
            }


            $res = array(
                "type" => $application->getField("fk_isgaApplicationType"),
                "reason" => $application->getField("fk_isgaApplicationReason"),
                "tableData" => $resultArray
            );
            echo json_encode($res);
            die;
        }
        die;
    }

    /*
     * метод формирует и экспортирует платежку
     */
    public function paymentExportAction()    {
        //берем ид заявления
        $appId=$this->params("id");
        if (empty($appId)) {
            die;
        }
        //берем нужный обработчик
        $paymentDoc= \Eo\Model\ExportPayment::getPaymentByIdApplication($appId);
        if(empty($paymentDoc)){
            throw new \Exception('Не найден обработчик формирования платежки');
        }
        //формируем данные
        $data=$paymentDoc->getData();
        if (empty($data)){
            throw new \Exception('Ошибка при формирования платежки');
        }
        //берем шаблон
        $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
        $templateName = $templatePath.'Payment.docx'; //шаблон документа
        //вставляем данные в шаблон
        $result=$paymentDoc->templateMakeDoccument($data, $templateName);
        //возвращаем файл пользователю
        $fileOutName = 'Платежка.docx'; //имя выходного файла
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . '"' . $fileOutName . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . strlen($result));
        //header('Pragma: no-cache');//глюко ошибка
        header('Cache-Control: no-store, no-cache, must-revalidate');
        clearstatcache();
        echo $result;//  отправляем  данные
        die;
        //впихиваем их в шаблон
        //отправляяем пользователю
    }

    public function sendErrorAction(){
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $data = $this->params()->fromPost('data');
            $file = $this->params()->fromFiles('data');
            //Проверяем на верное заполнение
            $errors = array();
            if($data["tbEmail"] == ""){
                $errors["tbEmail"] = true;
            }
            if (!filter_var($data["tbEmail"], FILTER_VALIDATE_EMAIL)) {
                $errors["tbEmail"] = true;
            }
            if($data["taText"] == ""){
                $errors["taText"] = true;
            }
            if($data["captcha-value"] == ""){
                $errors["captcha-value"] = true;
            }
            //Проверяем капчу
            $captchaClass = new \Captcha\Captcha();
            $result = $captchaClass->validateCaptcha($data["captcha-id"], $data["captcha-value"]);
            if(!$result)
                $errors["captcha-value"] = true;
            //Если есть ошибки
            if(count($errors) > 0){
                echo json_encode(array(
                    "errors" => $errors
                ));
                die;
            } else {
                //Если был аплоад файла то перемещаем его куда надо
                $filename = null;
                if($file != null && isset($file["fFile"])){
                    $uploaddir = __DIR__.'/../../../../../public/bugs/';
                    $tmpName = $file["fFile"]["tmp_name"];
                    $ext = pathinfo($file["fFile"]["name"], PATHINFO_EXTENSION);

                    $filename = implode("",explode(".",microtime(true))).".".$ext;
                    $uploadfile = $uploaddir . $filename;

                    move_uploaded_file($tmpName, $uploadfile);
                }


                //Получаем пароль
                /*
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://agent.prostoy.ru/get_objectX.php");
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "
            <request>
                <object>get_task_password</object>
                <access_code>G7ZZ4CNj3f4CBBbQGbUNzrdtAY1gh8r3</access_code>
                <id>420279</id>
            </request>");
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 300);
                $response = curl_exec($ch);
                var_dump($response);die;
                */

                //Отправляем сообщение
                $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
                $eo = $user->getEducationOrganization();
                $mess =  "
                Образовательное учреждение: ".$eo->getField("FullName")."<br>
                E-mail: ".$data["tbEmail"]."<br>
                Сообщение: ".$data["taText"];
                if($filename != null)
                    $mess .= "<br>Скачать файл - http://f11.mon.prostoy.ru/bugs/".$filename."<br>
                Удалить файл - http://f11.mon.prostoy.ru/auth/authorization/clear-bug/".$filename."
                ";

                //Сохраняем в бд
                $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
                $educationOrganization = $user->getEducationOrganization();

                $br = new \Model\Entities\BugReports();
                $curDate = new \DateTime();
                $br->setFields(array(
                    "Email" => $data["tbEmail"],
                    "Text" => $data["taText"],
                    "File" => $filename,
                    "Date" => $curDate->format("Y-m-d"),
                    "fk_eiisEducationalOrganization" => $educationOrganization->getId()
                ));
                $br->save();

                //Отправляем в простой
                $name = "Сообщение об ошибке";
                $data = array(
                    'tid' => "420279",
                    'tpass' => "776972f945d1f016efc6a30525858271",
                    'tname' => $name,
                    //'temail' => "",
                    //'tsubj' => 'Обращение от системы управления проектами',
                    'tcom' => iconv("utf-8","windows-1251",$mess),
                    'com_type' => 'system'
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "http://agent.prostoy.ru/addComment.php");
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_NOBODY, false);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                $obj = simplexml_load_string(curl_exec($ch));
                curl_close($ch);

                //проверяем прошло ли сообщение
                $msg = (string)$obj['status'];
                if( $msg != 'OK'){
                    //Отправляем мне емэйл
                    $message = "";

                    $message = "Ошибка по отправкесообщения в задачу<br><br>";

                    $to  = "wizzymails@gmail.com" ;

                    $subject = "Error from vuzi client error send";

                    $headers  = "Content-type: text/html; charset=windows-1251 \r\n";
                    $headers .= "From: <f11.mon.prostoy.ru>";

                    mail($to, $subject, $message, $headers);
                }


                //выводим что все ок
                echo json_encode(array(
                    "success" => "true"
                ));
            }

        }
        die;
    }

    public function phpinfoAction(){
        echo phpinfo();
        die;
    }

    public function testVerifyAction(){
            $soap = new \SoapClient("http://192.168.67.141/verify/service.svc?wsdl");
            $filepath = $_SERVER['DOCUMENT_ROOT'].'/public/cert.png.sig';
            $binary_file = fread(fopen($filepath, "r"), filesize($filepath));
            $ret = $soap->VerifySignature(array("signatureType"=>"CMS",  "document"=>$binary_file));

            var_dump($ret->VerifySignatureResult);

			die;


    }

    public function testSignAction()
    {
        $sign_class = new \Eo\Service\Sign();
        $result = $sign_class->valid_signature("file_for_test_sign2.pdf.sig");
        $signer_info = $result['result']->VerifySignatureResult->SignerCertificateInfo;
        print_r(get_object_vars($signer_info));
        die;
    }

    public function helpAction()
    {
        $tab = 1;
        if ($this->params()->fromRoute()) {
            $route = $this->params()->fromRoute();
            $tab = $route['id'];
        }else{
            echo "Что то пошло не так...";
            die;
        }
        $view = new ViewModel();

        $view->setVariables(array(
                'tab' => $tab,
            ));
        $view->setTerminal(true);
        return $view;
    }

    public function morphyOrganizationAction(){
        $ret = [
            'status' => 'error'
        ];

        if ($this->params()->fromPost()) {
            $post = $this->params()->fromPost();
            $morphy = new \Eo\Model\Licensing\Tools\Morphy();
            $org = new Model\Entities\eiisEducationalOrganizations($post['id']);
            $morphy = $morphy->getForms( $org->getFieldOrSpace('FullName'));
            $ret['data'] = $morphy;
            $ret['status'] = 'ok';
        }
        return new \Zend\View\Model\JsonModel($ret);
    }
    public function certificateExcerptAction(){
        $certServ = new \Eo\Service\certificateExcerptService();
        $id = $this->params()->fromRoute('id');
        $certServ->generate($id);
        die;
    }

    public function licenseExcerptAction(){
        $certServ = new \Eo\Service\LicenseExcerptService();
        $id = $this->params()->fromRoute('id');
        $certServ->generate($id);
        die;
    }
    
    public function certificateExcerptNoProgramAction(){
        $certServ = new \Eo\Service\certificateExcerptService();
        $certServ->setIncludeProgram(false);
        $id = $this->params()->fromRoute('id');
        $certServ->generate($id);
        die;
    }
    
    public function licenseExcerptNoProgramAction(){
        $licServ = new \Eo\Service\LicenseExcerptService();
        $licServ->setIncludeProgram(false);
        $id = $this->params()->fromRoute('id');
        $licServ->generate($id);
        die;
    }

    /**
     * Метод сортировки данных по коду и по уровню образования (раньше этот метод реализовывался во view )
     * 
     * перепилено, чтоб работало автономно и дергало программы по приложению
     * 
     * prostoy://comment/23269693
     *
     * @param $certificates
     * @return array
     */
    static public function certificatesSortByCodeAndLevel($certificates)
    {
        $accredPrograms = new \Model\Entities\isgaAccreditedPrograms();
        $programsType = new \Model\Entities\eiisEduProgramTypes();

        $firstCert = null;
        $firstSupp = null;
        $programsPrint = array();
        
        if( $certificates instanceof Model\Entities\isgaCertificateSupplements ){
            $firstSupp = $certificates;
        }
        else{
            $certArray = $certificates->getObjectsArray();
            foreach($certArray as $certificate){
                if($firstCert == null) $firstCert = $certificate;
            }
            $supplements = $firstCert->getSupplements();
            $suppArray = $supplements->getObjectsArray();
            foreach($suppArray as $supplement){
                if($firstSupp == null) $firstSupp = $supplement;
            }
        }

        $programs = $firstSupp->getAccreditedPrograms()->getObjectsArray();

        foreach ($programs as $accrProg) {

            $accProgramById =  $accredPrograms->getAllById($accrProg->getField("Id"));
            foreach ($accProgramById as $program) {

                //Если нет связи с ГС, то пропускаем обработку такой аккр программы.
                //if($accrProg->isEmptyField("fk_isgaGroupSpeciality"))
                // continue;

                $programTypeById = $programsType->getAllById($program->getField('fk_eiisEduProgramType'));

                foreach ($programTypeById as $type) {
                    $ugs = $accrProg->getEnlargedGroupSpeciality();
                    $eduLevel = $accrProg->getEduLevel();
                    $programsData = array(
                        "Id" => $accrProg->getField("Id"),
                        "Код УГС" => ($ugs != null) ? $ugs->getField("Code") : "",
                        "Наименование УГС" => ($ugs != null) ? $ugs->getField("Name") : "",
                        "Код ОП" => $accrProg->getField("Code"),
                        "Наименование ОП" => $accrProg->getField("Name"),
                        "Уровень образования" => ($eduLevel != null) ? $eduLevel->getField("Name") : "",
                        'SortOrder' => $type->getField('SortOrder'),
                    );
                    $programsPrint[] = $programsData;
                }
            }
        }
        $programsPrint = array_filter($programsPrint);

        usort($programsPrint,
            function($a_obj, $b_obj) {
                $a_obr_level = $a_obj['SortOrder'];
                $b_obr_level = $b_obj['SortOrder'];

                $a = $a_obj["Код ОП"];
                $b = $b_obj["Код ОП"];

                if($a_obr_level == $b_obr_level){
                    if ($a == $b) {
                        return 0;
                    }
                    return ($a < $b) ? -1 : 1;
                }
                return ($a_obr_level < $b_obr_level) ? -1 : 1;
            }
        );
        return $programsPrint;
    }

    /**
     * Вкладка Экспертная очередь.
     *
     * @return ViewModel
     */
    public function equeueAction()
    {
        $addWhere = null;
        $where = [];

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $search = $this->params()->fromPost();


        if($search) {
            (!empty($search['queType'])) ? $where['ElectronicQueueReason.code'] = ['reception_' . $search['queType'], 'delivery_' . $search['queType']] : false;
            (!empty($search['basisTreatment'])) ? $where['ServiceReasonCatalog.id'] = $search['basisTreatment']: false;
            (!empty($search['typeTreatment'])) ? $where['ElectronicQueueReason.id'] = $search['typeTreatment']: false;
            (!empty($search['licenseReasonCatalogItem'])) ? $addWhere = $search['licenseReasonCatalogItem']: false;
        }

        $queueClassAll = new \Model\LodEntities\ElectronicQueueEntry();
        $CQueue = new \Eo\Classes\CQueue();

        $entities = $queueClassAll->getDataQueue($educationOrganization->getField('Id'), $where, $addWhere);
        $selectOption = $CQueue->getSelectOption($entities);

        usort($entities, function($a_arr, $b_arr){
            $a = strtotime($a_arr["startReceptionTime"]);
            $b = strtotime($b_arr["startReceptionTime"]);
            if ($a == $b) {
                return 0;
            }
            return ($a > $b) ? -1 : 1;
        });


        $view = new ViewModel();
        $view->setVariables(array(
            'entities' => $entities,
            'selectOption' => $selectOption,
            'i' => 0
        ));

        $view->setTerminal(true);
        return $view;
    }

    /**
     * Генерация новой записи в электронную очередь.
     *
     * @return ViewModel
     */
    public function newQueueAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $dateFree = [];
        $form = new EqueueForm('equeue');

        $view = new ViewModel();
        $view->setVariables(array(
            "genericForm"=>$form,
            'inn'       => $educationOrganization->getField('Inn'),
            'nameOrg'   => $educationOrganization->getField('FullName'),
        ));
        $view->setTerminal(true);

        return $view;

    }

    /**
     * Первый шаг регистрации в электронную очередь.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function step1Action()
    {
        $data = $this->params()->fromPost();
        $data = $this->correctionData($data);

        $form = new EqueueForm('equeue');
        $form->setData($data);

        if($form->isValidStep1()){
            $CQueue = new \Eo\Classes\CQueue();
            $stepData1 = new \Zend\Session\Container('stepData1');
            $possibleDates = new \Zend\Session\Container('possibleDates');

            $stepData1->exchangeArray($data);
            $dateTime = $CQueue->getDateTime();
            $possibleDates->exchangeArray($dateTime);

            $result['status']   = 'ok';
            $result['dateTime'] = $dateTime;
        }else{
            $result['status'] = 'error';
            $result['errors'] = $form->getMessages();
        }

        return new \Zend\View\Model\JsonModel($result);
    }

    /**
     * Второй шаг регистрации в электронную очередь.
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function step2Action()
    {
        $dataPost = $this->params()->fromPost();
        $dataPost = $this->correctionData($dataPost);

        $possibleDates = new \Zend\Session\Container('possibleDates');

        $form = new EqueueForm('equeue');
        $form->setData($dataPost);
        $form->setDates(array_keys((array)$possibleDates->getIterator()));

        if($form->isValidStep2()){
            $stepData2 = new \Zend\Session\Container('stepData2');
            $stepData2->exchangeArray($dataPost);

            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            $educationOrganization = $user->getEducationOrganization();
            $CQueue = new \Eo\Classes\CQueue();

            $entry = $CQueue->save($educationOrganization);
            $data = $CQueue->getDataMessage($entry);
            $content = $this->renderMailBody($data);
            $status = $CQueue->sendMail($content, $entry->getField('eMail'), 'Запись в электронную очередь');

            if($status){
                $result['status'] = 'ok';
                $result['content']   = $content;
            }else{
                $result['status'] = 'error-mail';
            }

        }else{
            $result['status'] = 'error-valid';
            $result['fieldError'] = $form->getMessages();
        }

        return new \Zend\View\Model\JsonModel($result);

    }

        /**
         * Заполнение html шаблона.
         *
         * @param $params
         * @param string $tpl
         *
         * @return mixed
         */
        public function renderMailBody($params, $tpl = 'eo/mail/body')
    {
        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\RendererInterface');
        $content = new \Zend\View\Model\ViewModel($params);
        $content->setTemplate($tpl);
        return $renderer->render($content);
    }


    /**
     * Очистка данных от пробелов и тегов.
     *
     * @param $data
     * @return array
     */
    public function correctionData($data)
    {
        $result = [];

        foreach ($data as $key=>$value){
            if(is_array($value)){
                $result[$key] = $this->correctionData($value);
            }elseif (is_string($value)){
                $result[$key] = trim(strip_tags($value));
            }else{
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * Вывод подробной информации экспертной очереди
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function infoEqueueAction()
    {
        if ($this->params()->fromPost())
        {
            $post = $this->params()->fromPost();
            $equeueId = $post['equeueId'];

            $reasonsId = [];
            $equeueReasons = null;
            $reasons = [];

            $equeue = new ElectronicQueueEntry($equeueId);
            $equeueReason = new ElectronicQueueReason($equeue->getField('electronicQueueReasonId'));
            $serviceReason = new ServiceReasonCatalog($equeue->getField('serviceReasonCatalogId'));
            $numberProgramCatalog = new NumberOfProgramCatalog($equeue->getField('numberOfProgramCatalogId'));
            $numberDocumentCatalog = new NumberOfDocumentsCatalog($equeue->getField('numberOfDocumentsId'));
            $issueDocumentsAmount = new IssueDocumentsAmount($equeue->getField('issueDocumentsAmountId'));

            if(substr($serviceReason->getField('appealTypeCode'),-8) === '_license')
            {
                $licendeReasonModel = new LicenseReasonCatalogItem();
                $equeueReasons = $licendeReasonModel->getAll()->getObjectsArray();
                $tmp = new ElectronicQueueEntryToLicenseReasonCatalogItem();
                foreach ($tmp->getAllByWhere(['electronicQueueEntryId' => $equeueId]) as $value) {
                    $reasonsId[] = $value->getField('licenseReasonCatalogItemId');
                }
                foreach ($equeueReasons as $k => $val){
                    if(in_array($val->getField('id'), $reasonsId)){
                        $nameReason = $val->getField('DocumentPrint');
                        if($nameReason !== '-') $reasons[] = $nameReason;
                    }
                }
            }
            else
            {
                $isgaReasonModel = new isgaApplicationReasons();
                $equeueReasons = $isgaReasonModel->getAllObjects();

                $tmp = new ElectronicQueueEntryToAccreditationReason();
                foreach ($tmp->getAllByWhere(['electronicQueueEntryId' => $equeueId]) as $value) {
                    $reasonsId[] = $value->getField('AccredApplicationReasons');
                }

                foreach ($equeueReasons as $k => $val){
                    if(in_array($val->getField('Id'), $reasonsId)){
                        $nameReason = $val->getField('Name');
                        if($nameReason !== '-') $reasons[] = $nameReason;
                    }
                }
            }

            $content = $this->renderMailBody([
                'equeue' => $equeue,
                'equeueReason' => $equeueReason,
                'serviceReason' => $serviceReason,
                'numberProgramCatalog' => $numberProgramCatalog,
                'numberDocumentCatalog' => $numberDocumentCatalog,
                'issueDocumentsAmount' => $issueDocumentsAmount,
                'reasons' => $reasons

            ], 'eo/index/info-equeue');
        }
        return new \Zend\View\Model\JsonModel(['content' => $content]);
    }
}
