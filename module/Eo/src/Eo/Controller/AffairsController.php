<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Zend\Db\Sql\Predicate\Predicate;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Islod\Model\Tools\Converter;
use Model;

class AffairsController extends AbstractActionController
{

    private $currentUserRole;

    //protected $soap;

    public function indexAction()
    {
        //Подключаем js файл
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/multiselect/pqselect.dev.js');
        $headStuff->appendFile('/js/islod/main.js');
        $headStuff->appendFile('/js/islod/services.js');
        $headStuff->appendFile('/js/islod/binds.js');
        $headStuff->appendFile('/js/islod/namespace/affairs.js');
        $headStuff->appendFile('/js/islod/namespace/application.js');
        $headStuff->appendFile('/js/islod/namespace/request.js');
        $headStuff->appendFile('/js/islod/namespace/license.js');
        $headStuff->appendFile('/js/islod/namespace/organization.js');
        $headStuff->appendFile('/js/islod/namespace/licenseSupplement.js');
        $headStuff->appendFile('/js/islod/affairs.page.js');
        $headStuff->appendFile('/js/widget/jquery.ui.widget.js');
        $headStuff->appendFile('/js/jquery.iframe-transport.js');
        $headStuff->appendFile('/js/jquery.fileupload.js');
        $headStuff->appendFile('/js/zclip/ZeroClipboard.js');

        //Подключаем стили
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        $headStuff->appendStylesheet('/css/islod/frames.css');
        $headStuff->appendStylesheet('/js/multiselect/pqselect.min.css');

        $IslodStepClass = new \Model\Gateway\LodEntitiesTable("islod_StepRon");
        $IslodSteps = $IslodStepClass->getAllEntities(null, "Orderliness ASC");

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();

        $this->layout()->identityUser = $user;
        $role = $this->getCurrentUserRole();

        return new ViewModel([
            'user'  => $user,
            'role'  => $role,
            "steps" => $IslodSteps
        ]);
    }

    public function getCurrentUserRole()
    {
        if (empty($this->currentUserRole)) {
            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            $this->currentUserRole = new Model\LodEntities\IslodUserRole($user->getField('fk_islodUserRole'));
        }
        return $this->currentUserRole;
    }

    /**
     * Возвращает право роли
     * @param type $rightsName
     * @return string
     */
    public function getRigthsStatus($rightsName)
    {
        $role = $this->getCurrentUserRole();
        return ($role->getFieldOrSpace($rightsName) == '1');
    }

    /**
     * Проверяет доступ
     */
    public function accessControl($rightsName)
    {
        if (!$this->getRigthsStatus($rightsName)) {
            echo '<div class="errorBlock"><h1>У вас нет права доступа к данным страницам и функциям сайта!</h1></div>';
            die;
        }
    }

    /*
     * Action перехода по шагам на вкладке заявления
     */
    public function stepAction()
    {
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $step = $this->params('id');
        $pagePaginator = ($this->params('id2') != NULL) ? $this->params('id2') : 1;
        $search = $this->getRequest()->getPost()->toArray();
        $getParams = $this->getRequest()->getQuery()->toArray();
        $stepRon_class = new Model\Gateway\LodEntitiesTable('islod_StepRon');
        $stepRon = $stepRon_class->getEntityById($step);
        $status = explode(',', $stepRon->getField('Statuses'));
        $buttons = explode(',', $stepRon->getField('Buttons'));
        $rolesClass = new Model\Gateway\EntitiesTable('ron_UserRole');
        $role = $rolesClass->getAllEntities(NULL, NULL, array("curator" => 1))->toArray();

        $usersClass = new Model\Gateway\EntitiesTable('ron_Users');
        $currentCurator = null;



        if (isset($getParams["curator"]) && $getParams["curator"] != null) {
            $currentCurator = $getParams["curator"];
            $user = $usersClass->getEntityById((int) $getParams["curator"]);
        } else {
            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        }
        $educationOrganization = $user->getEducationOrganization();
        $curr_role = $user->getRole();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;
        //Создаем объект для работы с таблицей
        $max_count_page = 50;

        $ApplClass = new \Model\Gateway\LodEntitiesTable("LicenseRequest");

        //Подготавливаем запрос в зависимости от шага
        $where = [];

        switch ($step) {
            case "2":
                $where[] = new \Zend\Db\Sql\Predicate\IsNull("dealId");
                break;
        }

        //Строим запрос по значениям фильтра
        if (isset($search["SearchOrg"]) && !empty($search["SearchOrg"])) {
            //подзапрос
            //Получаем список организаций
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());

            $predicate = new \Zend\Db\Sql\Where();
            $predicate->like("FullName", "%" . $search["SearchOrg"] . "%")
              ->or
              ->like("Inn", "%" . $search["SearchOrg"] . "%");

            $subselectOrgs = $sql->select();
            $subselectOrgs->from("eiis_EducationalOrganizations")
              ->columns(array("Id"))
              ->where($predicate);
            $statement = $sql->prepareStatementForSqlObject($subselectOrgs);
            $results = $statement->execute();

            //print_r($sql->getSqlstringForSqlObject($subselectOrgs));die;

            $Ids = array();
            foreach ($results as $org) {
                $Ids[] = $org["Id"];
            }
            if (count($Ids) > 0)
                $where[] = new \Zend\Db\Sql\Predicate\In("organizationId", $Ids);
        }
        if (isset($search["dtDateStart"]) && !empty($search["dtDateStart"])) {
            $predicate = new \Zend\Db\Sql\Where();
            $where[] = $predicate->greaterThan("creationDate", $search["dtDateStart"]);
        }
        if (isset($search["dtDateEnd"]) && !empty($search["dtDateEnd"])) {
            $predicate = new \Zend\Db\Sql\Where();
            $where[] = $predicate->lessThan("creationDate", $search["dtDateEnd"]);
        }
        if (isset($search["SearchType"]) && !empty($search["SearchType"])) {
            $where["licensingProcedureId"] = $search["SearchType"];
        }
        if (isset($search["SearchReasons"]) && !empty($search["SearchReasons"])) {
            //подзапрос
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $subselect = $sql->select();
            $subselect->from("LicenseReasonToLicenseRequest")
              ->columns(array("licenseRequestId"))
              ->where([
                  "licenseReasonId" => $search["SearchReasons"]
            ]);

            $where[] = new \Zend\Db\Sql\Predicate\In("id", $subselect);
        }

        //
        $requestsAll = $ApplClass->getAllEntities(array(
              "page"   => $pagePaginator,
              "onPage" => $max_count_page
            ), "number DESC, creationDate DESC", $where)->getObjectsArray();
        $requestsCount = $ApplClass->getCount($where);

        //Подгружаем типы процедур
        $proceduresTable = new Model\Gateway\LodEntitiesTable("LicensingProcedure");
        $procedures = $proceduresTable->getAllEntities(null, "code")->getObjectsArray();

        //Подгружаем причины
        $reasonsTable = new Model\Gateway\LodEntitiesTable("LicenseReasonCatalogItem");
        $reasons = $reasonsTable->getAllEntities(null, "code")->getObjectsArray();

        //
        $allPage = ceil($requestsCount / $max_count_page);
        $view = new ViewModel();
        $rights = [
            'createRequest' => $this->getRigthsStatus('createRequest'),
        ];
        $view->setVariables(array(
            'rights'         => $rights,
            "currPage"       => $pagePaginator,
            "allPage"        => $allPage,
            "requests"       => $requestsAll,
            'buttons'        => $buttons,
            'step'           => $step,
            'search'         => $search,
            "userRole"       => $curr_role,
            "currentCurator" => $currentCurator,
            "procedures"     => $procedures,
            "reasons"        => $reasons,
            "requestsCount"  => $requestsCount
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function affairsAllAction()
    {
        $rights = [
            'viewDealDataAndReason' => $this->getRigthsStatus('viewDealDataAndReason'),
        ];
        // TODO: Удаляйте точто не успользуется
        //$headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        //$getParams = $this->getRequest()->getQuery()->toArray();
        //$currentCurator = null;
        $step = $this->params('id');
        $pagePaginator = ($this->params('id2') != NULL && $this->params('id2') != 0) ? $this->params('id2') : 1;
        $stepRon_class = new Model\Gateway\LodEntitiesTable('islod_StepRon');
        $search = $this->getRequest()->getPost()->toArray();
        $stepRon = $stepRon_class->getEntityById($step);
        $statuses = explode(';', $stepRon->getField('Statuses'));
        $buttons = explode(',', $stepRon->getField('Buttons'));
        $userTable = new Model\Gateway\LodEntitiesTable('Lod_User');
        $executors = $userTable->getAllEntities(null, "lastName", array("role" => "expert"))->buffer();

        $dealStatesTable = new Model\Gateway\LodEntitiesTable('catalog_deal');
        $dealProceduresTable = new \Model\LodEntities\LicensingProcedure();
        $dealStates = $dealStatesTable->getAllEntities(null, "order ASC")->buffer();
        $dealProcedures = $dealProceduresTable->getAll()->buffer();

        /* @var $user \Model\Entities\ronUsers */
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $userRole = $user->getRole();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;

        //Создаем объект для работы с таблицей
        $max_count_page = 50;
        $where = new \Zend\Db\Sql\Where();
        $dealTabe = new \Model\Gateway\LodEntitiesTable("Deal");
        if((count($search)>0)||!($stepRon->getField('Statuses') === 'All' || empty($stepRon->getField('Statuses')))){
            $deal = new \Model\LodEntities\Deal();
            $result = $deal->getIdsDealBySearch($search,$max_count_page,$pagePaginator,$statuses,$stepRon->getField('Statuses')); 
            $where->in('id', $result["ids"]);
            $dealsCount = $result["count"];
            $paging =null;
        }else{
            $dealsCount = $dealTabe->getCount($where);
            $paging = array(
              "page"   => $pagePaginator,
              "onPage" => $max_count_page
            );
        }
        $dealAll = $dealTabe->getAllEntities($paging, array("registrationDate" => "DESC", "number" => "DESC"), $where)->getObjectsArray();
        $allPage = ceil($dealsCount / $max_count_page);
        $view = new ViewModel();
        $view->setVariables(array(
            "currPage"       => $pagePaginator,
            "allPage"        => $allPage,
            "deals"          => $dealAll,
            "dealsCount"     => $dealsCount,
            'buttons'        => $buttons,
            'step'           => $step,
            "userRole"       => $userRole,
            //"currentCurator" => $currentCurator,
            "executors"      => $executors,
            "dealStatuses"   => $dealStates,
            'dealProcedures' => $dealProcedures,
            "search"         => $search,
            'rights'         => $rights
        ));
        $view->setTerminal(true);
        return $view;
    }


    public function editAction()
    {
        $rights = [
            'createRequest' => $this->getRigthsStatus('createRequest'),
            'editDeal' => $this->getRigthsStatus('editDeal'),
            'AssignExpert'=>$this->getRigthsStatus('AssignExpert')
        ];
        $dealId = $this->params("id");
        if (empty($dealId)) {
            die;
        }
        $view = new ViewModel();
        $view->setTerminal(true);
        $deal = new Model\LodEntities\Deal($dealId);
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $expertId = $user->getIslodIdExpert();
        $allowedState = array("FINISHED","RETURNED","ISSUE","REFUSED");
        if($expertId&&!in_array($deal->getField("stateCode"),$allowedState)){
            if($deal->getField("expertId")!=$expertId){
                echo 'error';
                die;
            }
        }
        $payments = [];
        $licenseRequestIds = array();
        $licenseRequests = new Model\LodEntities\LicenseRequest();
        $licenseRequests = $licenseRequests->getAllByWhere([ 'dealId' => $dealId]);

        if ($licenseRequests) {
            foreach ($licenseRequests as $licReq) {
                $payments[$licReq->getField('id')] = $licReq->getPayments();
                $licenseRequestIds[]=$licReq->getField('id');
            }
        }

        if ($deal->getId() == null){
            die;
        }
        $procedure = $deal->getProcedure();
        $Buttons = array();
        if ($procedure) {
            $procedureId = $procedure->getField("id");
            $ButtonsClass = new Model\Gateway\LodEntitiesTable('DealStatusButtons');
            $Buttons = $ButtonsClass->getAllEntities(NULL, NULL,
                                                     array("ProcedureId LIKE '%" . $procedureId . "%'", "Status" => $deal->getField("stateCode")));
        }
        $eiisEduOrg = new \Model\Entities\eiisEducationalOrganizations($deal->getField("organizationId"));
        $prescriptions =$eiisEduOrg->getPrescriptions();
        $ExecutorClass = new Model\Gateway\LodEntitiesTable('Lod_User');
        $Executors = $ExecutorClass->getAllEntities(null, "lastName ASC", array("role" => "expert"))->buffer();
        $DocumentTypeCatalogItemClass = new Model\Gateway\LodEntitiesTable('DocumentTypeCatalogItem');
        $AllParrentType = $DocumentTypeCatalogItemClass->getAllEntities(NULL, NULL, "parent IS NULL");
        $reorganizationInfo=array();
        if(!empty($licenseRequestIds)){
            $reorganizedOrganizationInfo = new \Model\LodEntities\ReorganizedOrganizationInfo();
            $reorganizationInfo = $reorganizedOrganizationInfo->getReorgInfoForDealEdit($licenseRequestIds)->toArray();
            foreach($reorganizationInfo as $key=>$oneInfo){
                $reorganizationInfo[$key]["supplements"]=$deal->getOnlyMainOrgSupplements($oneInfo["IdOrg"]);
            }
        }
        $supplements = $deal->getOnlyMainOrgSupplements();
        $view->setVariables(
          array(
              'rights'        => $rights,
              "deal"          => $deal,
              "executors"     => $Executors,
              "Buttons"       => $Buttons,
              "AllParentType" => $AllParrentType,
              'payments'      => $payments,
              "reorganizationInfo"=>$reorganizationInfo,
              "prescriptions"=>$prescriptions,
              "supplements"=>$supplements
          )
        );

        return $view;
    }

    public function saveAction()
    {
        $this->accessControl('editDeal');
        $dealId = $this->params("id");
        $expertId = $this->params("id2");
        if (empty($dealId)) {
            die;
        }
        $deal = new Model\LodEntities\Deal($dealId);
        $deal->setField("expertId", $expertId);
        $deal->save();
        echo "ok";
        die;
    }

    public function moveToAffairsDialogAction()
    {
        $requestId = $this->params("id");
        $Request = new Model\LodEntities\LicenseRequest($requestId);
        $Deal = new Model\LodEntities\Deal();
        $Deals = $Deal->getDealsByOrganId($Request->getField("organizationId"),
                                                             $Request->getField("licensingProcedureId"));
        $view = new ViewModel();
        $view->setVariables(
          array(
              "deals"     => $Deals,
              "requestId" => $requestId,
              "currDeal"  => $Request->getField("dealId")
          )
        );
        $view->setTerminal(true);
        return $view;
    }

    public function moveToAffairsSaveAction()
    {
        $requestId = $this->params("id");
        $newDealId = $this->params("id2");
        $Request = new Model\LodEntities\LicenseRequest($requestId);
        $oldDeal = $Request->getField("dealId");
        $Requests = new Model\Gateway\LodEntitiesTable("LicenseRequest");
        $count = $Requests->getCount(array("dealId" => $oldDeal));
        if ((int) $count < 2) {
            $Deal = new Model\LodEntities\Deal($oldDeal);
            $Deal->setField("stateCode", "FAIL_REGISTERED");
            $Deal->save();
        }
        $Request->setField("dealId", $newDealId);
        $Request->save();
        echo 1;
        die;
    }

    public function outRequestOfAffairAction()
    {
        $requestId = $this->params("id");
        $Request = new Model\LodEntities\LicenseRequest($requestId);
        $oldDealId = $Request->getField("dealId");
        $oldDeal = new Model\LodEntities\Deal($oldDealId);
        $newNumb = $oldDeal->getNextNumberOfIssused();
        $orgId = $oldDeal->getField("organizationId");
        $newDeal = new Model\LodEntities\Deal();
        $id = $newDeal->saveNewDeal($requestId, $orgId, $newNumb);
        $Request->setField("dealId", $id);
        $Request->save();
        echo 1;
        die;
    }

    public function changeStatusAffairAction()
    {
        $data = $this->getRequest()->getPost()->toArray();
        $dealId = $this->params("id");
        $newState = $this->params("id2");
        $comm = "";
        if (!empty($data)) {
            $comm = $data["comment"];
        }
        $Deal = new Model\LodEntities\Deal($dealId);
        $oldState = $Deal->getField("stateCode");
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        // если финишируем дело и это прекращение осуществления ОД то делаем не валидным все что в реестре
        if($newState == "FINISHED" && $Deal->getProcedureId()=="7d2f8700fb8d43a5a428de7cfc6e2bea"){
            $reestrService = new \Islod\Service\ReestrService($dealId);
            if(!$reestrService->applyChangesDeal(true)){
                echo "Сформируйте реестр.";
                die;
            }
        }
        if ($oldState == "WAITING_REPLY" && $newState == "RECEIVE_ANSWER") {
            if(!$Deal->checkNoticeReceiveAnswerDate()){
                echo "Введите дату получения ответа на уведомление";
                die;
            }
        }
        //проверка на пустоту комментария
        if ($oldState == "REGISTERED" && $newState == "FAIL_REGISTERED" && $comm == "") {
            echo "Поле комментарий обязательно для заполнения";
            die;
        }
        // проверка на пустоту эксперта
        if ($oldState == "CHECK_LICENSE_REQUEST" && $newState == "REVIEW" && $Deal->getField("expertId") == null) {
            echo "Необходимо выбрать эксперта";
            die;
        }
        if ($newState == "PRINT" || $newState == "REFUSED") {
            if (!$this->addAllToReestr($dealId)) {
                echo "Возникла ошибка при формировании лицензий/приложений, возможно не добавлены распорядительные документы.";
                die;
            }
        }
        if ($Deal->changeStatus($newState, $comm)) {
            $dataHistory = array(
                "IdDeal"    => $dealId,
                "oldStatus" => $oldState,
                "newStatus" => $newState,
                "IdUser"    => $user->getField("Id")
            );
            $DealHistory = new Model\LodEntities\DealStatusHistory();
            $DealHistory->setFields($dataHistory);
            $DealHistory->save(1);
            echo 1;
            die;
        } else {
            echo "Возникла ошибка при сохранении дела";
            die;
        }
    }

    public function changeStatusDocumentDialogAction()
    {
        $data = $this->getRequest()->getPost()->toArray();
        $documentId = $this->params("id");
        $Document = new Model\LodEntities\document($documentId);
        $view = new ViewModel();
        $view->setVariables(
          array(
              "reqFields" => json_decode($data["requireField"]),
              "Document"  => $Document
          )
        );
        $view->setTerminal(true);
        return $view;
    }

    public function getDocumentByDealAndTypeAction()
    {
        $dealId = $this->params("id");
        $typeDoc = $this->params("id2");
        if ($typeDoc == "677b74cac8fe43a5b469f25db50ab69d") {
            $arrType = array("e6b56dff077f40bda8c1c9d67e410f85", "677b74cac8fe43a5b469f25db50ab69d", "177fabe881474796b37b30c47969e93f");
        } elseif ($typeDoc == "8c51db151d1b44339890ea6c4b3820f1") {
            $arrType = array("2204642f3cbd463bb9836d78afd0dcad", "8c51db151d1b44339890ea6c4b3820f1");
        } else {
            $arrType = array($typeDoc);
        }
        $Deal = new Model\LodEntities\Deal($dealId);
        $Documents = $Deal->getDocumentByType($arrType);
        $DocumentClass = new \Model\LodEntities\document();
        $view = new ViewModel();
        $view->setVariables(
          array(
              "Documents"     => $Documents,
              "DocumentClass" => $DocumentClass,
              "typeDoc"=>$typeDoc
          )
        );
        $view->setTerminal(true);
        return $view;
    }

    public function getDocumentDialogAction()
    {
        $dealId = $this->params("id");

        $docServ = null;
        $document = null;
        $catalogDocument = null;
        $typeCatalogItem = null;
        $catalogItem = null;
        $deal = null;
        $view = new ViewModel();
        if ($this->getRequest()->isPost()) {
            $post = $this->params()->fromPost();
            if (isset($post['id'])) {
                $notificationDocId = $post['id'];
                $document = new \Model\LodEntities\document($notificationDocId);
                $documentPrintedForm = new \Model\LodEntities\DocumentPrintedForms();
                $documentPrintedForm = $documentPrintedForm->getAllByWhere(array("documentTypeCatalogItemId" => $document->getField("documentTypeId")));
                $catalogItemsId = array_column($documentPrintedForm->toArray(), "catalogItemId");
                if (count($catalogItemsId) > 0) {
                    $catalogItem = new \Model\LodEntities\CatalogItem();
                    $catalogItem = $catalogItem->getAllByWhere(['catalog' => 'documents_reasons', "id" => $catalogItemsId]);
                    $catalogItem = Converter::getTwoCollumnArrFromResultSet(
                        $catalogItem, 'id', 'title'
                    );
                } else {
                    $catalogItem = array();
                }
                $documentType = new \Model\LodEntities\DocumentTypeCatalogItem($document->getField("documentTypeId"));
                $signatories = new Model\LodEntities\signatories();
                $signatories = $signatories->getAllByWhere(['signatureLevelCode' => $documentType->getField("signatureLevelCode")]);
                $signatories = Converter::getTwoCollumnArrFromResultSet(
                    $signatories, 'id',
                    [
                      'lastName',
                      'firstName',
                      'middleName'
                    ]
                );
                $deal = new \Model\LodEntities\Deal($document->getField("dealId"));
                $deal = $deal->getField('number');
            } else {
                $signatories = new Model\LodEntities\signatories();
                $signatories = $signatories->getAllByWhere(['signatureLevelCode' => ['3']]);
                $signatories = Converter::getTwoCollumnArrFromResultSet(
                    $signatories, 'id',
                    [
                      'lastName',
                      'firstName',
                      'middleName'
                    ]
                );
                $documentPrintedForm = new \Model\LodEntities\DocumentPrintedForms();
                $documentPrintedForm = $documentPrintedForm->getAll();
                $catalogItem = new \Model\LodEntities\CatalogItem();
                $catalogItem = $catalogItem->getAllByWhere(['catalog' => 'documents_reasons',]);
                $catalogItem = Converter::getTwoCollumnArrFromResultSet(
                    $catalogItem, 'id', 'title'
                );
                $view->setTemplate("/islod/affairs/add-document-dialog.phtml");
            }
            if (isset($post["parentType"])&&!empty($post["parentType"])) {
                if ($post["parentType"] == "677b74cac8fe43a5b469f25db50ab69d") {
                    $arrType = array("e6b56dff077f40bda8c1c9d67e410f85", "677b74cac8fe43a5b469f25db50ab69d", "177fabe881474796b37b30c47969e93f");
                } elseif ($post["parentType"] == "8c51db151d1b44339890ea6c4b3820f1") {
                    $arrType = array("2204642f3cbd463bb9836d78afd0dcad", "8c51db151d1b44339890ea6c4b3820f1");
                } else {
                    $arrType = array($post["parentType"]);
                    $strType = implode(",", $arrType);
                }
                $typeCatalogItem = new \Model\Gateway\LodEntitiesTable("DocumentTypeCatalogItem");
                $typeCatalogItem = $typeCatalogItem->getAllEntities(null, "code ASC",
                                                                    array("parent" => $arrType, "signatureLevelCode !=''"));
                $typeCatalogItem = Converter::getTwoCollumnArrFromResultSet(
                    $typeCatalogItem, 'id', 'title'
                );
            } else {
                $typeCatalogItem = new \Model\LodEntities\DocumentTypeCatalogItem();
                $typeCatalogItem = $typeCatalogItem->getAll();
                $typeCatalogItem = Converter::getTwoCollumnArrFromResultSet(
                    $typeCatalogItem, 'id', 'title'
                );
            }




            $catalogDocument = new \Model\LodEntities\catalogdocument();
            $catalogDocument = $catalogDocument->getAll();
            $catalogDocument = Converter::getTwoCollumnArrFromResultSet(
                $catalogDocument, 'origin_name', 'russian_name'
            );
        }
        $view->setVariables([
            'document'            => $document,
            'catalogItem'         => $catalogItem,
            'signatories'         => $signatories,
            'typeCatalogItem'     => $typeCatalogItem,
            'catalogDocument'     => $catalogDocument,
            'deal'                => $deal,
            'documentPrintedForm' => $documentPrintedForm
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function saveFilesAction()
    {
        $documentId = $this->params('id');
        $document = new \Model\LodEntities\document($documentId);
        $error = array();
        $SendFileClass = new \Ron\Model\SendFile();
        $code = "Document";
        $dealId = $document->getField("dealId");
        foreach ($_FILES as $key => $one_file) {
            if (empty($one_file["size"])) {
                continue;
            }
            $EiFile = new \Model\LodEntities\EiFile();
            $save_data = array();
            $name = explode('.', $one_file['name']);
            $ext = strrchr($one_file['name'], '.');
            $RealName = $one_file['name'];
            $fileHashName = md5(\Ron\Controller\ExpertiseController::translit('', $name[0]) . '_' . md5(time())) . $ext;
            $shortPathDir = '/public/uploads/synch/' . date('Y') . '_lod/' . $dealId . '/' . $code . '/';
            $mimeType = $one_file["type"];
            $result = $SendFileClass->SendFileToOpenServer($one_file,
                                                           array("path" => $shortPathDir, "name" => $fileHashName),
                                                           584288000);
            if ($result == "1") {
                $path = str_replace("/public/uploads/", "", $shortPathDir);
                $EiFile->setField('ownerId', $documentId);
                $EiFile->setField('ownerClassCode', $code);
                $EiFile->setField('filename', $RealName);
                $EiFile->setField('size', $one_file["size"]);
                $EiFile->setField('mimeType', $mimeType);
                $EiFile->setField('sourceUrl', $path . $fileHashName);
                $EiFile->setField('withFileName', 1);
                $EiFile->setField('created', date("Y-m-d G:H:i"));
                $EiFile->exchangeArray($save_data);
                $res = $EiFile->save(1);
                if ($key == "ScanCopy") {
                    $document->setField("scanCopyId", $res);
                } elseif ($key == "PrintForm") {
                    $document->setField("printFormId", $res);
                }
                $document->save();
            } else {
                $error[] = $result;
            }
        }
        $result = json_encode(array('files' => array(0 => array('errors' => $error, 'filePath' => $shortPathDir . $fileHashName, 'fileName' => $RealName))));
        echo $result;
        die;
    }

    public function saveDocumentInfoAction()
    {
        $dealId = $this->params("id");
        if ($this->getRequest()->isPost()) {
            $post = $this->params()->fromPost();
            if (isset($post['id'])) {
                $documentId = $post['id'];
                $document = new \Model\LodEntities\document($documentId);
                $post["registrationDate"] = ($post["registrationDate"] != "-" && $post["registrationDate"] != "") ? date("Y-m-d",
                                                                                                                         strtotime($post["registrationDate"])) : NULL;
                $post["sendDate"] = ($post["sendDate"] != "-" && $post["sendDate"] != "") ? date("Y-m-d",
                                                                                                 strtotime($post["sendDate"])) : NULL;
                $post["receiveDate"] = ($post["receiveDate"] != "-" && $post["receiveDate"] != "") ? date("Y-m-d",
                                                                                                          strtotime($post["receiveDate"])) : NULL;
                $post["receiveAnswerDate"] = ($post["receiveAnswerDate"] != "-" && $post["receiveAnswerDate"] != "") ? date("Y-m-d",
                                                                                                                            strtotime($post["receiveAnswerDate"])) : NULL;
                $post["returnDate"] = ($post["returnDate"] != "-" && $post["returnDate"] != "") ? date("Y-m-d",
                                                                                                       strtotime($post["returnDate"])) : NULL;
                $document->exchangeArray($post);
                $res = $document->save();
            } else {
                $document = new \Model\LodEntities\document();
                $type = new \Model\LodEntities\DocumentTypeCatalogItem($post["documentTypeId"]);
                $post["stateCode"] = "FORM";
                $document->exchangeArray($post);
                $document->setField("dealId", $dealId);
                $res = $document->save(1);
                $document->setRegNumberNotify();
                //проверим, если создаетс яуведомление, то создадим печатную копию уведомлления
                try {
                    $document->remakePrintForm();
                } catch (\Exception $exc) {
                    echo $exc->getMessage();
                    echo $exc->getTraceAsString();
                }
            }
            echo $res;
            die;
        }
    }

    public function removeDocumentFileAction()
    {
        try {
            $documentId = $this->params("id");
            $type = $this->params("id2");
            $document = new \Model\LodEntities\document($documentId);
            $idFile = $document->getField($type);
            $EiFile = new \Model\LodEntities\EiFile($idFile);
            $EiFile->delete();
            $document->setField($type, NULL);
            $document->save();
            echo "ok";
            die;
        } catch (\Exception $e) {
            echo "error: " . $e->getMessage();
            die;
        }
    }

    public function changeStatusDocumentAction()
    {
        $data = $this->getRequest()->getPost()->toArray();
        $DocumentId = $this->params("id");
        $NewState = $this->params("id2");
        foreach ($data as $key => $oneField) {
            $pos = strpos($key, "Date");
            if ($pos !== false) {
                if($data[$key]==""){
                    unset($data[$key]);
                }else{
                    $data[$key] = date("Y-m-d", strtotime($oneField));
                }
            }
        }
        $Document = new Model\LodEntities\document($DocumentId);
        if ($Document->changeStatus($NewState)) {
            $Document->exchangeArray($data);
            $Document->save();
            echo "Статус документа изменен";
            die;
        } else {
            echo "Возникла ошибка при сохранении дела";
            die;
        }
    }

    public function deleteDocumentAction()
    {
        try {
            $documentId = $this->params("id");
            $docToLic = new \Model\LodEntities\LicenseAndLicenseSupplementToDocument();
            $docLinkes = $docToLic->getAllByWhere(array("documentId"=>$documentId));
            foreach($docLinkes as $oneLink){
                $oneLink->delete();
            }
            $document = new \Model\LodEntities\document($documentId);
            $document->delete();
            echo "Документ успешно удален";
            die;
        } catch (\Exception $e) {
            echo "Ошибка при удалении документа: " . $e->getMessage();
            die;
        }
    }

    public function getHistoryReestrAction()
    {
        $rights = [
            'expertButton'              => $this->getRigthsStatus('expertButton'),
            'printButton'               => $this->getRigthsStatus('printButton'),
            'extraditionButton'         => $this->getRigthsStatus('extraditionButton'),
        ];

        $dealId = $this->params("id");
        $reestrType = $this->params("id2");
        $registryChange = new \Model\LodEntities\RegistryChange();
        $view = new ViewModel();
        if ($reestrType == "old") {
            $registryList = $registryChange->getListOfRegistryByDealAndType($dealId, "exclusionFromRegistry");
            //$registryList = $registryChange->getAllByWhere(array("dealId" => $dealId, "registryChangeType" => "exclusionFromRegistry"));
        } else {
            $registryList = $registryChange->getListOfRegistryByDealAndType($dealId, "addToRegistry");
            //$registryList = $registryChange->getAllByWhere(array("dealId" => $dealId, "registryChangeType" => "addToRegistry"));
        }
        $StatesClass = new \Model\LodEntities\catalogeiislicenses();
        $states = $StatesClass->getAll();
        $stateArr = $states->toArray();
        $view->setVariables(array(
            "rights" => $rights,
            "registryList"     => $registryList,
            "Deal"             => new \Model\LodEntities\Deal($dealId),
            "catalogEiisState" => array_column($stateArr, "russian_name", "origin_name"),
            "registryChange" =>$registryChange,
            "licensesClass" => new \Model\Entities\eiisLicenses(),
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function addAllToReestr($dealId, $recharge = false)
    {
        $Deal = new \Model\LodEntities\Deal($dealId);
        // $SettingOfHistoryReestr = new \Model\LodEntities\SettingOfHistoryReestr();
        $count = $Deal->getCountInReestr();
        $reestrService = new \Islod\Service\ReestrService($dealId);
        if (!$recharge) {
            if ($count > 0) {
                return true;
            }
        } else {
            if ($count > 0) {
                $reestrService->deleteAllChanges();
            }
        }
        return $reestrService->generateReestrInDeal();
    }

    public function dealAtWorkAction()
    {
        $id = $this->params()->fromPost('id', null);
        $userTable = new \Model\LodEntities\LodUser();

        /* @var $user \Model\Entities\ronUsers */
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        /* @var $currentExecutor \Model\LodEntities\LodUser */
        $currentExecutor = $userTable->getByWhere(['ron_User' => $user->getId()]);
        $isExpert = false;
        if ($currentExecutor && null === $id) {
            $id = $currentExecutor->getId();
            $isExpert = $currentExecutor->isExpert();
        }

        $deals = \Model\LodEntities\Deal::getAllForUser($id);
        $executors = $userTable->getAllByWhere(['role' => 'expert']);

        $view = new ViewModel([
            'deals'     => $deals,
            'executors' => $executors,
            'isExpert'  => $isExpert,
            'hasDealAccess' => $this->getRigthsStatus('viewAffairs'),
            'hasOrganizationAccess' => $this->getRigthsStatus('viewOrganizations'),
        ]);
        $view->setTerminal(true);

        return $view;
    }

   public function changeStatusReestrAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $userId = $user->getField("Id");
        $elementId = $this->params("id");
        $serviceError = 'Ошибка отправки в реестр';
        $data = $this->getRequest()->getPost()->toArray();
        $licenseIdsForReestr = array();
        $comment = empty($data['comment'])?'':$data['comment'];
        $comment = htmlentities($comment);
        if( mb_strlen($comment) > 500 ){
            die('Слишком длинный комментарий');
        }
        if ($data["stateType"] == "signCode") {
            $fieldName = "lod_signStateCode";
            $supplFieldName = "lod_signStateCode";
            $supplState = $data["nextState"];
        }
        if ($data["elemType"] == "License") {

            if ($data["stateType"] == "stateCode") {
                $fieldName = "fk_eiisLicenseState";
                $supplFieldName = "fk_eiisLicenseSupplementState";
                $mtmLicAndSupplState = array(
                    "E8EE8E7F37E847A78ED1F79901270A79" => "9416C0E6E84F4194B57DA930AD10A4D0",
                    "E30CDE3DF6E243E5AC47A42EEC2E6BE8" => "4D41481DBCA461BF5BAB7C5316D8F2D4",
                    "9464A3242CB7F716FDD1CAF43122CB45" => "5A9AAA5977024C5EAE3128DD37AC03AB",
                    "881BF0EAD521453296B47008A4373825" => "0727E76E83574B10833A25CA627FE6A0",
                    "34F4DBDB89FA94D119AF0C1ABDD61D43" => "4E09AEA87399AFFD9EC3FE6833DB4B36",
                    "C1497B3779DF4356B9CBBAC97479FAEB" => "0727E76E83574B10833A25CA627FE6A0"
                );
                $supplState = $mtmLicAndSupplState[$data["nextState"]];
            }

            $element = new \Model\Entities\eiisLicenses($elementId);
            $licenseIdsForReestr[]=$elementId;
            if ($fieldName == "fk_eiisLicenseState") {
                $oldState = $realOldState = $element->getField("lod_stateCode");
                $licenseState = new \Model\Entities\eiisLicenseStates($data["nextState"]);
                $element->setField("lod_stateCode", $licenseState->getField("LodCode"));
                $statusType = "lod_stateCode";
                $nextState = $realNextState = $licenseState->getField("LodCode");
            }else{
                $oldState = $realOldState = $element->getField("lod_signStateCode");
                $nextState = $realNextState = $data["nextState"];
                $statusType =$realStatusType = "lod_signStateCode";
                if($oldState== "ON_SIGN"&& $nextState == "SIGNED"){
                    $element->exchangeArray([
                            "fk_eiisLicenseState" => "E8EE8E7F37E847A78ED1F79901270A79",
                            "lod_stateCode"=> "VALID"
                    ]); 
                }
            }
            $element->setField($fieldName, $data["nextState"]);
            $element->save();
            
            $RegistryChange = new \Model\LodEntities\RegistryChange();
            $oneRegistry    = $RegistryChange->getByWhere(array("licenseId" => $elementId));
            
            $supplements    = new \Model\Entities\eiisLicenseSupplements();
            $supplements    = $supplements->getAllByWhere(["fk_eiisLicense" => $element->getId()]);
            foreach ($supplements as $oneSuppl) {
                if ($supplFieldName == "fk_eiisLicenseSupplementState") {
                    $oldState        = $oneSuppl->getField("lod_stateCode");
                    $supplementState = new \Model\Entities\eiisLicenseSupplementStates($supplState);
                    $oneSuppl->setField("lod_stateCode", $supplementState->getField("lodCode"));
                    $statusType      = "lod_stateCode";
                    $nextState       = $supplementState->getField("lodCode");
                } else {
                    $oldState   = $oneSuppl->getField("lod_signStateCode");
                    $statusType = "lod_signStateCode";
                    $nextState  = $data["nextState"];
                    if ($realOldState == "ON_SIGN" && $realNextState == "SIGNED") {
                        $oneSuppl->exchangeArray([
                            "fk_eiisLicenseSupplementState" => "9416C0E6E84F4194B57DA930AD10A4D0",
                            "lod_stateCode"                 => "VALID"
                        ]);
                    }
                }
                $oneSuppl->setField($supplFieldName, $supplState);
                $oneSuppl->save();
                if ($oldState != $nextState) {
                    //$this->statusChangeHistory($userId, $oneSuppl->getField("Id"), "LicenseAttach", $oldState, $nextState,$statusType);
                    if (!\Model\Entities\eiisLicenses::statusChangeHistory($userId, $oneSuppl->getField("Id"), "LicenseAttach", $oldState, $nextState, $comment, $statusType)) {
                        echo $serviceError;
                        die;
                    }
                }
            }
            if ($oldState != $nextState) {
                if (!\Model\Entities\eiisLicenses::statusChangeHistory($userId, $elementId, "License", $oldState, $nextState, $comment, $statusType)) {
                    echo $serviceError;
                    die;
                }
            }
            if ($oneRegistry) {
                $dealId        = $oneRegistry->getField("dealId");
                $allElemByDeal = $RegistryChange->getAllByWhere(array("dealId" => $dealId, "registryChangeType" => "exclusionFromRegistry"))->toArray();
                $supplIds      = array_unique(array_column($allElemByDeal, "licenseAttachId"));
                $licenseIds    = array_unique(array_column($allElemByDeal, "licenseId"));
                if (($data["nextState"] == "E8EE8E7F37E847A78ED1F79901270A79" && $oneRegistry->getField("registryChangeType") != "exclusionFromRegistry") || ($realOldState == "ON_SIGN" && $realNextState == "SIGNED" && $oneRegistry->getField("registryChangeType") != "exclusionFromRegistry")) {
                    foreach ($licenseIds as $licenseId) {
                        $licenseIdsForReestr[] = $licenseId;
                        $license               = new \Model\Entities\eiisLicenses($licenseId);
                        $oldState              = $license->getField("lod_stateCode");
                        $licenseState          = new \Model\Entities\eiisLicenseStates("9464A3242CB7F716FDD1CAF43122CB45");
                        $license->exchangeArray([
                            "fk_eiisLicenseState" => "9464A3242CB7F716FDD1CAF43122CB45",
                            "lod_stateCode"       => $licenseState->getField("LodCode")
                        ]);
                        $license->save();
                        if ($oldState != $licenseState->getField("LodCode")) {
                            //$this->statusChangeHistory($userId,$licenseId, "License", $oldState, $licenseState->getField("LodCode"),"lod_stateCode");
                            if (!\Model\Entities\eiisLicenses::statusChangeHistory($userId, $licenseId, "License", $oldState, $licenseState->getField("LodCode"), $comment, "lod_stateCode")) {
                                echo $serviceError;
                                die;
                            }
                        }
                    }
                    foreach ($supplIds as $supplId) {
                        $supplement            = new \Model\Entities\eiisLicenseSupplements($supplId);
                        $oldState              = $supplement->getField("lod_stateCode");
                        $supplementState       = new \Model\Entities\eiisLicenseSupplementStates("5A9AAA5977024C5EAE3128DD37AC03AB");
                        $licenseIdsForReestr[] = $supplement->getField("fk_eiisLicense");
                        $supplement->exchangeArray([
                            "fk_eiisLicenseSupplementState" => "5A9AAA5977024C5EAE3128DD37AC03AB",
                            "lod_stateCode"                 => $supplementState->getField("lodCode")
                        ]);
                        $supplement->save();
                        if ($oldState != $supplementState->getField("lodCode")) {
                            //$this->statusChangeHistory($userId,$supplId, "LicenseAttach", $oldState, $supplementState->getField("lodCode"),"lod_stateCode");
                            if (!\Model\Entities\eiisLicenses::statusChangeHistory($userId, $supplId, "LicenseAttach", $oldState, $supplementState->getField("lodCode"), $comment, "lod_stateCode")) {
                                echo $serviceError;
                                die;
                            }
                        }
                    }
                }
                $ReestrServise = new \Islod\Service\ReestrService($dealId);
                if ($RegistryChange->checkSupplementStatus($dealId)) {
                    $ReestrServise->applyChangesDeal();
                }
            }
            if(($realNextState!="DRAFT"&&$realNextState!="34F4DBDB89FA94D119AF0C1ABDD61D43")){
                $returnService  = \Islod\Service\ExportService::sendLicenses(array_unique($licenseIdsForReestr));
                if($returnService["response"]!=1){
                    echo $serviceError;
                    die;
                }
            }
            
            //если статус меняется на готово к выдаче
            //пошлем по почте сообщение
            if( isset($dealId) && $data["stateType"] == "signCode" && 'READY_TO_ISSUE' == $data["nextState"] ){
                \Islod\Model\Mail\licenseReady::send($dealId);
            }
            
            echo 1;
            die;
        } elseif ($data["elemType"] == "Supplement") {
            $supplement = new \Model\Entities\eiisLicenseSupplements($elementId);
            if ($data["stateType"] == "stateCode") {
                $oldState = $supplement->getField("lod_stateCode");
                $statusType = "lod_stateCode";
                $fieldName = "fk_eiisLicenseSupplementState";
                $supplementState = new \Model\Entities\eiisLicenseSupplementStates($data["nextState"]);
                $nextState =$realNextState= $supplementState->getField("lodCode");
                $supplement->setField("lod_stateCode", $supplementState->getField("lodCode"));

            }else{
                $statusType = "lod_signStateCode";
                $oldState = $supplement->getField("lod_signStateCode");
                $nextState =$realNextState= $data["nextState"];
                if($oldState == "ON_SIGN"&&$nextState == "SIGNED" ){
                    $supplement->setField("fk_eiisLicenseSupplementState", "9416C0E6E84F4194B57DA930AD10A4D0");
                }
            }
            $supplement->setField($fieldName, $data["nextState"]);
            $supplement->save();
            $licenseIdsForReestr[] = $supplement->getField("fk_eiisLicense");
            if($oldState!=$nextState){
                //$this->statusChangeHistory($userId, $elementId, "LicenseAttach", $oldState, $nextState,$statusType);
                if(!\Model\Entities\eiisLicenses::statusChangeHistory($userId, $elementId, "LicenseAttach", $oldState, $nextState, $comment,$statusType)){
                    echo $serviceError;
                    die;
                }
            }
            $RegistryChange = new \Model\LodEntities\RegistryChange();
            $oneRegistry = $RegistryChange->getByWhere(array("licenseAttachId" => $elementId));
            if ($oneRegistry) {
                $dealId=$oneRegistry->getField("dealId");
                if ($RegistryChange->checkSupplementStatus($dealId)) {
                    $allElemByDeal = $RegistryChange->getAllByWhere(array("dealId" => $dealId));
                    foreach ($allElemByDeal as $oneReestr) {
                        if ($oneReestr->getField("licenseAttachId") != null) {
                            if ($oneReestr->getField("registryChangeType") !== "addToRegistry") {
                                $supplementState = new \Model\Entities\eiisLicenseSupplementStates("5A9AAA5977024C5EAE3128DD37AC03AB");
                                $where = array("fk_eiisLicenseSupplementState" => "5A9AAA5977024C5EAE3128DD37AC03AB","lod_stateCode"=> $supplementState->getField("lodCode"));
                            } else {
                                $supplementState = new \Model\Entities\eiisLicenseSupplementStates("9416C0E6E84F4194B57DA930AD10A4D0");
                                $where = array("fk_eiisLicenseSupplementState" => "9416C0E6E84F4194B57DA930AD10A4D0","lod_stateCode"=> $supplementState->getField("lodCode"));
                            }
                            $supplement = new \Model\Entities\eiisLicenseSupplements($oneReestr->getField("licenseAttachId"));
                            $oldState = $supplement->getField("lod_stateCode");
                            $supplement->exchangeArray($where);
                            $supplement->save();
                            if($oldState!=$supplementState->getField("lodCode")){
                                //$this->statusChangeHistory($userId,$oneReestr->getField("licenseAttachId"), "LicenseAttach", $oldState, $supplementState->getField("lodCode"),"lod_stateCode");
                                if(!\Model\Entities\eiisLicenses::statusChangeHistory($userId,$oneReestr->getField("licenseAttachId"), "LicenseAttach", $oldState, $supplementState->getField("lodCode"), $comment ,"lod_stateCode")){
                                    echo $serviceError;
                                    die;
                                }
                            }
                        } elseif ($oneReestr->getField("licenseId") != null) {
                            $licenseIdsForReestr[]=$oneReestr->getField("licenseId");
                            if ($oneReestr->getField("registryChangeType") == "addToRegistry") {
                                $licenseState = new \Model\Entities\eiisLicenseStates("E8EE8E7F37E847A78ED1F79901270A79");
                                $where = array("fk_eiisLicenseState" => "E8EE8E7F37E847A78ED1F79901270A79","lod_stateCode"=> $licenseState->getField("LodCode"));
                            } else {
                                $licenseState = new \Model\Entities\eiisLicenseStates("9464A3242CB7F716FDD1CAF43122CB45");
                                $where = array("fk_eiisLicenseState" => "9464A3242CB7F716FDD1CAF43122CB45","lod_stateCode"=> $licenseState->getField("LodCode"));
                            }
                            $license = new \Model\Entities\eiisLicenses($oneReestr->getField("licenseId"));
                            $oldState = $license->getField("lod_stateCode");
                            $license->exchangeArray($where);
                            $license->save();
                            if($oldState!= $licenseState->getField("LodCode")){
                                //$this->statusChangeHistory($userId,$oneReestr->getField("licenseId"), "License", $oldState, $licenseState->getField("LodCode"),"lod_stateCode");
                                if(!\Model\Entities\eiisLicenses::statusChangeHistory($userId,$oneReestr->getField("licenseId"), "License", $oldState, $licenseState->getField("LodCode"), $comment,"lod_stateCode" )){
                                    echo $serviceError;
                                    die;
                                }
                            }
                        }
                    }
                    $ReestrServise = new \Islod\Service\ReestrService($dealId);
                    $ReestrServise->applyChangesDeal();
                }
            }
            if(($realNextState!="DRAFT"&&$realNextState!="34F4DBDB89FA94D119AF0C1ABDD61D43")){
               $returnService  = \Islod\Service\ExportService::sendLicenses($licenseIdsForReestr);
                if($returnService["response"]!=1){
                    echo $serviceError;
                    die;
                }
            }
            //если статус меняется на готово к выдаче
            //пошлем по почте сообщение
            if( $data["stateType"] == "signCode" && 'READY_TO_ISSUE' == $data["nextState"] ){
                \Islod\Model\Mail\licenseReady::send($dealId);
            }
            echo 1;
            die;
        }
        echo "Возникла ошибка при смене статуса";
        die;
    }

    public function rechangeReestrAction()
    {

        $dealId = $this->params("id");
        if ($this->addAllToReestr($dealId, true)) {
            echo 1;
            die;
        } else {
            echo "Возникла ошибка при формировании лицензий/приложений, возможно не добавлены распорядительные документы.";
            die;
        }
    }

    public function testGenerateReestrAction()
    {
        $dealId = $this->params("id");
        $reestrService = new \Islod\Service\ReestrService($dealId);
        $reestrService->generateReestrInDeal();
        die;
    }

    public function checkMaxAction()
    {
        $eiisEduOrg = new \Model\Entities\eiisEducationalOrganizations();
        $orgId = $this->params("id");
        $licId = $this->params("id2");
        $eiisLicenseSupplement = new \Model\Entities\eiisLicenseSupplements();
        $currentOrg = $eiisEduOrg->getById($orgId);
        $maxNum = $eiisLicenseSupplement->getMaxNumber($orgId, $licId);
        var_dump($maxNum);
        die;
    }

    public function getDealInfoAction()
    {
        $post = $this->getRequest()->getPost();

        $id = $post->get('id', null);
        $deal = new \Model\LodEntities\Deal($id);
        $state = $deal->getId() !== null;

        $documentTypeCatalogItemTable = new Model\Gateway\LodEntitiesTable('DocumentTypeCatalogItem');
        $parentTypes = $documentTypeCatalogItemTable->getAllEntities(NULL, NULL, "parent IS NULL");

        $viewData = [
            'state'       => $state,
            'deal'        => $deal,
            'parentTypes' => $parentTypes,
        ];

        if ($state) {

        }

        $view = new ViewModel($viewData);
        $view->setTerminal(true);
        return $view;
    }

    /**
     * сохраняем данные лицензии, или всего подряд
     * формат входа
     * $POST[имя таблицы][идЗаписи][имяПоля] = значение
     * выход
     * Json[status] = Ok    - добро
     * Json[status] = Error - установлена ошибка
     * Json['message']      - вывести в всплывалке
     * Json['console']      - выввести в консольку
     * @return \Zend\View\Model\JsonModel
     * @throws \Exception
     */
    public function saveFilialAction()
    {
        try {
            //хочу через транзакцию
            $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
            $driver = $dbAdapter->getDriver();
            $connection = $driver->getConnection();
            $connection->beginTransaction();
            $post = $this->getRequest()->getPost();
            if (empty($post)) {
                throw new \Exception('Лод - сохранение филиала - Не приехали данные ');
            }
            foreach ($post as $tableName => $tableRow) {
                if (empty($tableRow) || !is_array($tableRow)) {
                    throw new \Exception('Лод - сохранение филиала - ошибка в таблице ');
                }
                $tableClass = new \Model\Gateway\LodEntitiesTable($tableName);
                if (empty($tableClass) || !is_array($tableRow)) {
                    throw new \Exception('Лод - сохранение филиала - не найдена таблице ');
                }
                $getwey = $tableClass->getGateway();
                $prototype = $getwey->getResultSetPrototype();
                $tablesClass = $prototype->getArrayObjectPrototype();
                if (empty($tableClass) || !is_array($tableRow)) {
                    throw new \Exception('Лод - сохранение филиала - не найден класс таблице ');
                }
                $rowAll = $tablesClass->getAllByWhereParseId(
                  array($tablesClass->getIdName() => array_keys($tableRow))
                );
                if (!empty($rowAll)) {
                    foreach ($rowAll as $rowId => $rowOne) {
                        //костль - если таблича RequestOrganizationInfo,
                        //то надо проверить, есть ли еще в этом деле заявления
                        //и внести такиеже правки в такиеже филиалы
                        if ('RequestOrganizationInfo' == $tableName) {
                            $orgId = $rowOne->getFieldSafe('orgId');
                            if (empty($orgId)) {
                                throw new \Exception('Лод - сохранение филиала - не найден ид филиала у записи ' . $rowOne->getId());
                            }
                            if (!empty($rowOne)) {
                                $deal = $rowOne->getDeal();
                                if( !empty($deal) ){
                                    $reqAll = $deal->getAllRequests();
                                    if (!empty($reqAll)) {
                                        foreach ($reqAll as $reqOne) {
                                            $orgInfoAll = $reqOne->getRequestOrganizationInfoAll($orgId);
                                            if (!empty($orgInfoAll)) {
                                                foreach ($orgInfoAll as $orgInfoOne) {
                                                    $orgInfoOne->setFieldsSafe($tableRow[$rowId]);
                                                    $orgInfoOne->save();
                                                }
                                            }
                                        }
                                    }
                                    continue;
                                }                                
                            }                            
                        }
                        //$rowId = $rowOne->getId();
                        $rowOne->setFieldsSafe($tableRow[$rowId]);
                        $rowOne->save();
                    }
                }
            }
            $result['status'] = 'Ok';
            $result['message'] = 'Данные записаны';
            // $result['link'] = $resultSend;
            //$result['fileName'] = $fileName;
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            $result['status'] = 'Error';
            $result['message'] = $e->getMessage();
            $result['console'] = str_replace(PHP_EOL, '\n', $e->getTraceAsString());
        }
        return new \Zend\View\Model\JsonModel($result);
    }

    public function getSignatoriesForDocAction()
    {
        $typeId = $this->params("id");
        $documentType = new Model\LodEntities\DocumentTypeCatalogItem($typeId);
        $signatories = new Model\LodEntities\signatories();
        $signatories = $signatories->getAllByWhere(['signatureLevelCode' => $documentType->getField("signatureLevelCode")]);
        $signatories = Converter::getTwoCollumnArrFromResultSet(
            $signatories, 'id',
            [
              'lastName',
              'firstName',
              'middleName'
            ]
        );
        $viewdata = array("signatories" => $signatories);
        $view = new ViewModel($viewdata);
        $view->setTerminal(true);
        return $view;
    }

    /*public function testAction()
    { 
        $supplement = new \Model\Entities\eiisLicenseSupplements("0B0E110F-0E0E-1311-0D12-0D11120C0C0F120D0D10");
        var_dump($supplement->getMaxNumber("7186693E0A486B60561C12517E1C593D", "669099EEEE1E8B04C8CEDADA3E5316A7"));
        var_dump($supplement->getMaxNumberLic("669099EEEE1E8B04C8CEDADA3E5316A7"));die;

    }*/
    
    public function addLicenseNumberAction(){
        $licenseId = $this->params("id");
        $eiisLicense = new Model\Entities\eiisLicenses($licenseId);
        if($eiisLicense->isEmptyField("LicenseRegNum")){
            $maxNum           = $eiisLicense->getMaxNumber();
            $maxNum           = (int) $maxNum[0];
            $maxNum++;
            $eiisLicense->exchangeArray(array("LicenseRegNum"=>$maxNum));
            $eiisLicense->save();
        }
        return new \Zend\View\Model\JsonModel(1);
    }

    /**
     *  отправляет в иссторию
     *  примечание: добавил статичным, чтоб копии не плодить
     * @param type $uid - Ид пользователя
     * @param type $objectId - Ид лицензии\приложения
     * @param type $type - ENUM('LicenseAttach', 'License')
     * @param type $stateOld - старый статус
     * @param type $stateNew - новый статус статус
     * @param string $typeStatus - тип статуса 	enum('lod_stateCode', 'lod_signStateCode')
     * @return type
     */
    public static function statusChangeHistory( $uid, $objectId, $type, $stateOld, $stateNew, $typeStatus="lod_stateCode"){
        $creationDate = new \DateTime();
        $creationDate = $creationDate->format('Y-m-d');

        $statusHistory = new \Model\LodEntities\StatusHistory();
        $statusHistory ->setFields([
            'creationDate' => $creationDate,
            'sourceUserId' => $uid,
            'objectId' => $objectId,
            'objectType' => $type,
            'typeStatus'=>$typeStatus,
            'StateOld' => $stateOld,
            'StateNew' => $stateNew,
        ]);
        return  $statusHistory->save(true);
    }

    /**
     * получим запись о текущем аторизованом пользователе
     * @return type
     */
    public static function getCurentAutorisatedUser() {
        $dbAdapter = \Model\Gateway\DbAdapter::getInstance();
        $dbTableAuthAdapter  = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter($dbAdapter,
            'ron_Users','Login','Password');
        $authService = new \Auth\Storage\AuthService();;
        $authService->setAdapter($dbTableAuthAdapter);
        $authService->setStorage(new \Auth\Storage\AuthStorage('Auth'));
        $user = $authService->getIdentity();
        return $user;
    }
    public function getReasonForProcedureAction(){
        $idProcedure = $this->params("id");
        $idReason = $this->params("id2");
        $reasonClass = new \Model\LodEntities\LicenseReasonCatalogItem();
        if($idProcedure=="all"){
            $reasonsAll = $reasonClass->getAllByWhere(1);
        }else{
            $reasonsAll = $reasonClass->getAllByWhere(array("licensingProcedureId"=>$idProcedure));
        }
        
        $viewdata = array("reasonsAll" => $reasonsAll,"currentReason"=>$idReason);
        $view = new ViewModel($viewdata);
        $view->setTerminal(true);
        return $view;
    }
    public function deallocationSupplementAction(){
        $idSupplement = $this->params("id");
        $idDeal = $this->params("id2");
        $deal = new \Model\LodEntities\Deal($idDeal);
        $allRequestIds = $deal->getAllRequestIds();
        $mtmTable = new Model\Gateway\LodEntitiesTable("LicenseSupplementToLicenseRequest");
            $mtmTable->delete([
                "licenseAttachId" => $idSupplement,
                "licenseRequestId" => $allRequestIds
            ]);
        echo "ok"; 
        die;
    }
    //последняя строчка, что свн на коллизии не колбасило
}
