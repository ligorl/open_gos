<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

/**
 * контроллер обработки блока лицензий в личном кабинете
 * /eo/licensing
 */
class LicenseController extends AbstractActionController
{

    private $currentUserRole;

    public function indexAction()
    {
        //Подключаем js файл
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/moment.js');
        //$headStuff->appendFile('/js/multiselect/pqselect.dev.js');
        $headStuff->appendFile('/js/islod/main.js');
        $headStuff->appendFile('/js/islod/services.js');
        $headStuff->appendFile('/js/islod/binds.js');
        $headStuff->appendFile('/js/eo/excerpt.js');
        $headStuff->appendFile('/js/islod/namespace/application.js');
        $headStuff->appendFile('/js/islod/namespace/affairs.js');
        $headStuff->appendFile('/js/islod/namespace/license.js');
        $headStuff->appendFile('/js/islod/namespace/organization.js');
        $headStuff->appendFile('/js/islod/namespace/licenseSupplement.js');
        $headStuff->appendFile('/js/islod/namespace/users.js');
        $headStuff->appendFile('/js/islod/namespace/request.js');
        $headStuff->appendFile('/js/islod/namespace/report.js');
        $headStuff->appendFile('/js/islod/namespace/inventory.js');
        $headStuff->appendFile('/js/islod/namespace/certificate.js');
        $headStuff->appendFile('/js/islod/index.page.js');


        $headStuff->appendFile('/js/islod/RequestFrames.class.js');

        $headStuff->appendFile('/js/jquery-ui-1.10.4.custom.min.js');
        $headStuff->appendFile('/js/jquery.iframe-transport.js');
        $headStuff->appendFile('/js/widget/jquery.ui.widget.js');
        //$headStuff->appendFile('/js/jquery.iframe-transport.js');
        //$headStuff->appendFile('/js/jquery.fileupload.js');
        //$headStuff->appendFile('/js/zclip/ZeroClipboard.js');

        //$headStuff->appendFile('/js/eo/main.js');

        $headStuff->appendFile('/js/chosen/chosen.jquery.js');
        //$headStuff->appendFile('/js/multiselect/pqselect.dev.js');
        $headStuff->appendFile('/js/multiselect/pqselect.dev.js');

        //Подключаем стили
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        //$headStuff->appendStylesheet('/css/islod/frames.css');
        //$headStuff->appendStylesheet('/js/multiselect/pqselect.min.css');
        $headStuff->appendStylesheet('/css/islod_style.css');
        $headStuff->appendStylesheet('/css/license.css');
        $headStuff->appendStylesheet('/js/multiselect/pqselect.min.css');

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();

        $this->layout()->identityUser = $user;
        $rights = [
            'viewReports'          => $this->getRigthsStatus('viewReports'),
            'viewUsers'            => $this->getRigthsStatus('viewUsers'),
            'viewOrganizations'    => $this->getRigthsStatus('viewOrganizations'),
            'viewLicenses'         => $this->getRigthsStatus('viewLicenses'),
            'viewEducationProgram' => $this->getRigthsStatus('viewEducationProgram'),
            'vieweQueue'           => $this->getRigthsStatus('vieweQueue'),
            'viewCalendar'         => $this->getRigthsStatus('viewCalendar'),
            'headInterface'        => $this->getRigthsStatus('headInterface')
        ];

        return new ViewModel([
            'user'   => $user,
            'rights' => $rights
        ]);
    }

    public function getCurrentUserRole()
    {
        if (empty($this->currentUserRole)) {
            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            $this->currentUserRole = new Model\LodEntities\IslodUserRole($user->getField('fk_islodUserRole'));
        }
        return $this->currentUserRole;
    }
   
    public function excerptAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $orgId = $educationOrganization->getId();
        
        $type = $this->params()->fromRoute('id');
        $page = $this->params()->fromRoute('id2');
        
        $excerpt = new \Eo\Service\ExcerptService();
        $excerpts = [];
        $title = '';
        
        if(empty($page)){
            $page = 1;
        }
        
        if($type=='licenses'){
            $excerpts = $excerpt->getAllLicenseExcerpt($orgId, $page);
            $pages = $excerpt->getCountLicenseExcerpt($orgId);
            $title = $excerpt->getCurrentLicenseQuery($orgId);
        }
        
        if($type=='certificates'){
            $excerpts = $excerpt->getAllCertificateExcerpt($orgId);
            $pages = $excerpt->getCountCertificateExcerpt($orgId);
            $title = $excerpt->getCurrentCertificateQuery($orgId);
        }
        
        return $this->getViewModel([
            'excerpts' => $excerpts,
            'type' => $type,
            'pages' => $pages,
            'page' => $page,
            'title' => $title
        ]);
    }
    
    public function excerptThisAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $orgId = $educationOrganization->getId();
        
        $type = $this->params()->fromPost('type');
        $excerpt = new \Eo\Service\ExcerptService();
        $stat = 'error';
        
        if($type=='licenses'){
            $excerpt->createExcerptLicensMode($orgId);
            $stat = 'ok';
        }
        
        if($type=='certificates'){
            $excerpt->createExcerptCertMode($orgId);
            $stat = 'ok';
        }
        
        return new \Zend\View\Model\JsonModel([
            'status' => $stat
        ]);
    }

    /**
     * Возвращает право роли
     * @param type $rightsName
     * @return string
     */
    public function getRigthsStatus($rightsName)
    {
        $role = $this->getCurrentUserRole();
        return ($role->getFieldOrSpace($rightsName) == '1');
    }

    public function accessDeniedAction()
    {
        return $this->getViewModel([]);
    }

    /**
     * Проверяет доступ
     */
    public function accessControl($rightsName)
    {
        if (!$this->getRigthsStatus($rightsName)) {
            echo '<div class="errorBlock"><h1>У вас нет права доступа к данным страницам и функциям сайта!</h1></div>';
            die;
        }
    }

    /**
     * модуль новых заявлений
     *
     * url: /eo/license/new
     *
     * @return ViewModel
     */
    function newAction(){

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 3;
        $where = array();
        $pagePaginator = 1;

        $allPage = 1;

        //для фильтров - процедуры
        $licenseProcedureAll = new Model\LodEntities\LicensingProcedure();
        $licenseProcedureAll = $licenseProcedureAll->getAllByWhereParseId(array(
            $licenseProcedureAll->getIdName() => $licenseProcedureAll->getLicensingProcedureForOpenId()
        ));
        uasort(
            $licenseProcedureAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        //для фильтров - причины обращения
        $licenseReasonAll = new Model\LodEntities\LicenseReasonCatalogItem();
        $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId( array(
                $licenseReasonAll->getIdName() => $licenseReasonAll->getLicenseReasonForOpenId()
        ));
        uasort(
            $licenseReasonAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;
        $requestsAll = array();

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('licenseProcedureAll', $licenseProcedureAll);
        $view->setVariable('licenseReasonAll', $licenseReasonAll);
        $view->setVariable('requestsAll', $requestsAll);
        return $view;
    }

    /**
     * получаем записи о новых заявлениях
     *
     * url: /eo/license/new-page
     *
     * @return ViewModel
     */
    function newPageAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 50;
        $where = array();
        $where = new \Zend\Db\Sql\Where();

        $pagePaginator = $this->getRequest()->getPost('page',1);
        $width = $this->getRequest()->getPost('width',1366);
        if($width <= 1366) $max_count_page = 10;


        //$where[] = new \Zend\Db\Sql\Predicate\IsNull("dealId");
        $where->isNull("dealId");
        //$where[] = new \Zend\Db\Sql\Predicate\IsNull('DateSubmission');
        $where->isNull("DateSubmission");
        $where->like('organizationId', $educationOrganization->getId());


        //добавляем фильтры
        $filter = $this->getRequest()->getPost('filter','');
        //по процедуре
        if( !empty($filter["licenseProcedure"]) ){
            $where->like('licensingProcedureId',$filter["licenseProcedure"]);
            //$where[] = new \Zend\Db\Sql\Predicate\Like('licensingProcedureId',$filter["licenseProcedure"]);
        }
        //по причине
        if( !empty($filter["licenseReason"]) ){
            /*
            $licenseReasonAll = new Model\LodEntities\LicenseReasonToLicenseRequest();
            $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId(
                    array(
                        new \Zend\Db\Sql\Predicate\Like('licenseReasonId',$filter["licenseReason"]),
                    ),
                    'licenseRequestId'
            );
            $licenseReasonAll =  array_keys($licenseReasonAll);
            $where->in('id',$licenseReasonAll);
            */
            $selectIn = new \Zend\Db\Sql\Select();
            $selectIn->from('LicenseReasonToLicenseRequest');
            $selectIn->where->like('licenseReasonId',$filter["licenseReason"]);
            $selectIn->columns(array('licenseRequestId'));
            $where->in('id',$selectIn);
        }
        //под дате заполнени я с
        if( !empty($filter["fillingDateBegin"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateBegin"]));
            if( !empty($date)){
                $where->greaterThanOrEqualTo('fillingDate',$date);
            }
        }
        //под дате заполнени я по
        if( !empty($filter["fillingDateEnd"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateEnd"]));
            if( !empty($date)){
                $where->lessThanOrEqualTo('fillingDate',$date);
            }
        }


        $ApplClass = new \Model\Gateway\LodEntitiesTable("LicenseRequest");
        $requestsAll = $ApplClass->getAllEntities(array(
                "page"   => $pagePaginator,
                "onPage" => $max_count_page
            ),
            "fillingDate DESC",
            $where)->getObjectsArray();
        $requestsCount = $ApplClass->getCount($where);

        $allPage = ceil($requestsCount / $max_count_page);

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('requestsAll', $requestsAll);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('requestsCount', $requestsCount);
        return $view;
    }


    /**
     * рисуем диалог добавления заявления
     *
     * url: /eo/lisense/add
     *
     * @return ViewModel
     */
    public function addAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $org = $educationOrganization;
        //$viewData["prescriptions"] = count($org->getPrescriptions());

        //$this->accessControl('createRequest');
        //Данные используемые для первой вкладки
        $propertiesData = [];
        $viewData = [
            "propertiesData" => null,
            "isEdit" => false
        ];
        $viewData["startOrg"] = $org;

        /*
        //Если передан айдишник, запрос открыт на редактирование
        if($this->params()->fromPost("requestId") != null){
            $request = new Model\LodEntities\LicenseRequest($this->params()->fromPost("requestId"));
            $viewData["isEdit"] = true;
            $viewData["request"] = $request;

            //Предписания
            $org = $request->getEducationalOrganization();
            $viewData["prescriptions"] = count($org->getPrescriptions());
        }
        */
        /*
        if($this->params()->fromPost("organizationId") != null){
            $org = new Model\Entities\eiisEducationalOrganizations($this->params()->fromPost("organizationId"));
            $viewData["startOrg"] = $org;
            $viewData["prescriptions"] = count($org->getPrescriptions());
        }
        */
        /*
        if($this->params()->fromPost("dealId") != null){
            $dealId = $this->params()->fromPost("dealId");
            $viewData["dealId"] = $dealId;
            //если передан айдишник дела надо собрать заявления из этого дела
            //хотя бы одно и вытянуть оттуда тип процедуры
            $requestsTable = new Model\Gateway\LodEntitiesTable("LicenseRequest");
            $dealRequests = $requestsTable->getAllEntities(null,null,[
                "dealId" => $dealId
            ])->getObjectsArray();

            if(count($dealRequests)>0){
                $sampleRequest = $dealRequests[0];
                if(!$sampleRequest->isEmptyField("licensingProcedureId")) {
                    $viewData["dealLicensingProcedureId"] = $sampleRequest->getField("licensingProcedureId");
                    $lp = new Model\LodEntities\LicensingProcedure($viewData["dealLicensingProcedureId"]);
                    $viewData["licensingProcedureName"] = $lp->getField("title");
                }
            }
        }
        */

        //Тип поступления
        $aTypeTab = new \Model\Gateway\LodEntitiesTable("CatalogItem");
        $aTypes = $aTypeTab->getAllEntities(null,"title ASC",[
            "catalog" => "admissionTypes"
        ])->getObjectsArray();
        $propertiesData["admissionTypes"] = $aTypes;

        //Тип процедуры
        $licenseProcedureAll = new Model\LodEntities\LicensingProcedure();
        $lProcTab = new \Model\Gateway\LodEntitiesTable("LicensingProcedure");
        $lProcedures = $lProcTab->getAllEntities(
                null,
                "code ASC",
                array($licenseProcedureAll->getIdName() => $licenseProcedureAll->getLicensingProcedureForOpenId() )
                //array($licenseProcedureAll->getIdName() => 'eb9e2f563ef24b3aa127811a27cd7009')
        )->getObjectsArray();
        $propertiesData["LicensingProcedures"] = $lProcedures;
        $propertiesData["firstProcedure"] = $lProcedures[0];

        //Причины обращения
        $licenseReasonAll = new Model\LodEntities\LicenseReasonCatalogItem();
        $reasonsTab = new \Model\Gateway\LodEntitiesTable("LicenseReasonCatalogItem");
        $reasons = $reasonsTab->getAllEntities(
                null,
                "licensingProcedureId ASC, title ASC",
                array($licenseReasonAll->getIdName() => $licenseReasonAll->getLicenseReasonForOpenId() )
        )->getObjectsArray();
        $propertiesData["reasons"] = $reasons;
        $reasonsByProcedure = [];
        foreach($reasons as $reason){
            if(!isset($reasonsByProcedure[$reason->getField("licensingProcedureId")]))
                $reasonsByProcedure[$reason->getField("licensingProcedureId")] = [];

            $reasonsByProcedure[$reason->getField("licensingProcedureId")][] = $reason;
        }
        $propertiesData["reasonsByProcedure"] = $reasonsByProcedure;

        $viewData["propertiesData"] = $propertiesData;
        //вью
        $view = new ViewModel();
        $view->setVariables($viewData);
        $view->setTerminal(true);
        return $view;
    }


    /**
     * добавляе заявления
     *
     * url: /eo/requests/add-request
     *
     */
    public function addRequestAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            $educationOrganization = $user->getEducationOrganization();


            $request = $this->getRequest();
            $fields = $this->getRequest()->getPost()->toArray();
            $reasons = $fields["reasons"];
            unset($fields["reasons"]);

            //Если это редактирование
            $isEdit = false;
            if(isset($fields["id"])){
                $isEdit = true;
                //объект запроса
                $newRequest = new Model\LodEntities\LicenseRequest($fields["id"]);
                //удаляем все текущие причины
                $newRequest->deleteReasons();
            } else {
                //Создаем новый объект заявления
                $newRequest = new Model\LodEntities\LicenseRequest();

                //Максимальный номер
                //пока не надо, т.к. оно еще не отправилось
                //$fields["number"] = $newRequest->getMaxNumber();
            }

            //Сохраняем поля
            if($isEdit){
                $oldFields = $newRequest->getFields();
            }
            if(!$isEdit || ($isEdit && $fields["organizationId"]!=$oldFields["organizationId"])) {
                //$eiisOrg = new Model\Entities\eiisEducationalOrganizations($fields["organizationId"]);
                $eiisOrg = $educationOrganization;
                $fields["typeOfBusiness"] = $eiisOrg->getFieldSafe("fk_eiisEducationalOrganizationProperties");
                $fields["ogrn"] = $eiisOrg->getFieldSafe("GosRegNum");
                $fields["inn"] = $eiisOrg->getFieldSafe("Inn");

                $license = $eiisOrg->getLicense();
                if( !empty($license) ){
                    $fields["licenseId"] = $license->getId();
                    $fields["licenseRegistrationNumber"] = $license->getFieldSafe("LicenseRegNum");
                    $fields["licenseDate"] = $license->getFieldSafe("DateLicDoc");
                    $fields["licenseSeries"] = $license->getFieldSafe("SerDoc");
                    $fields["licenseBlankNumber"] = $license->getFieldSafe("NumDoc");
                    $fields["issuingAuthority"] = $license->getFieldSafe("lod_authorizedPersonId");
                }

                if(!$isEdit){
                    $dt = new \DateTime($fields["fillingDate"]);
                    $fields["fillingDate"] = $dt->format("Y-m-d");
                    //дата создания нот нул
                    $fields["creationDate"] = date('Y-m-d');
                    $fields["notificationRequired"] = 1;
                }
            }
            $newRequest->setFieldsSafe($fields);
            //$user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            //$newRequest->setField("registratorId",$user->getId());

            //Добавляем объект
            $newId = $newRequest->save();
            /*
            if (!$isEdit && $newId !== false && (!isset($fields["dealId"]) || $fields["dealId"]=="")) {
                $newDeal = new Model\LodEntities\Deal();
                $idDeal  = $newDeal->saveNewDeal($newId, $fields["organizationId"]);
                $newRequest->addDeal($idDeal);
            }
            */

            //Добавляем связи с причинами
            $reasonsList = explode(",", $reasons);
            $newRequest->addReasons($reasonsList);

            //Если это редактирование, то проверяем не была ли изменена организация
            if($isEdit){
                if($fields["organizationId"]!=$oldFields["organizationId"]){
                    $newRequest->clearAllFramesData();
                }
            }

            //Тут идет кусок что проверяет вдруг у этого дело были заявления и хотяб одно из них было на реорганизацию
            /*
                Таким образом, если в деле есть заявление с причиной реорганизация в форме преобразования, то последующим заявлениям из этого же дела нужно подтягивать лицензию реорганизованной организации из первого заявления, чтоб потом из этой лицензии могли выбрать исключенные программы.


                как дополнение к предыдущему комментарию, т.е. для последующих заявлений в том же деле, где первым является реорганизация в форме преобразования, при создании заявления нужно в таблице LicenseRequest прописывать licenseId, licenseRegistrationNumber, licenseDate, licenseSeries, licenseBlankNumber лицензии реорганизованной организации, выбранной в первом заявлении.

             */
            //устанавливаем флаг в false
            $hasReorg = false;
            //связанное с делом заявление
            $dealMainRequest = null;
            /*
            //смотрим если есть дело
            $deal = $newRequest->getDeal();
            if($deal!=null){
                //забираем главное заявление
                $dealMainRequest = $deal->getMainLicenseRequest();
                //Проверяем на причину реорганизация в форме преобразования
                if(($dealMainRequest->hasReasonReorgTransform() || $dealMainRequest->hasReasonReorgJoin() || $dealMainRequest->hasReasonReorgMerge()) && $dealMainRequest->getId() != $newRequest->getId()){
                    $hasReorg = true;
                }
            }
            */
            //Если это не первое заявление дела
            if(!$isEdit && $dealMainRequest != null && $dealMainRequest->getId() != $newRequest->getId()){
                //копируем часть данных из сущесвующего объекта заявления
                //Наименование, сокращенное наименование ОГРН, �?НН, КПП, организационно правовую форму
                //requestSignerLastName,  requestSignerFirstName, requestSignerMiddleName и его должность наследуем
                $requestCopyData = [
                    "requestSignerLastName" => $dealMainRequest->getField("requestSignerLastName"),
                    "requestSignerFirstName" => $dealMainRequest->getField("requestSignerFirstName"),
                    "requestSignerMiddleName" => $dealMainRequest->getField("requestSignerMiddleName"),
                    "requestSignerPosition" => $dealMainRequest->getField("requestSignerPosition")
                ];
                $newRequest->setFields($requestCopyData);
                $newRequest->save();
                // Копирование данных для орг инфо
                $orgInfo = $dealMainRequest->getMainOrganizationInfo();

                $orgFields = $orgInfo->getFields();
                unset($orgFields["id"]);
                $orgFields["requestId"] = $newRequest->getId();
                $newOrg = new Model\LodEntities\RequestOrganizationInfo();
                $newOrg->setFields($orgFields);
                $newOrg->save();
            } else

            if(!$isEdit || ($isEdit && $fields["organizationId"]!=$oldFields["organizationId"])){
                //Добавляем запись для головного вуза в RequestOrganizationInfo
                $eiisOrg = new Model\Entities\eiisEducationalOrganizations($fields["organizationId"]);

                $emailData = '';
                $phone = '';
                $educationalOrganizationDataRes = $eiisOrg->getEducationalOrganizationData();
                if(!empty($educationalOrganizationDataRes)){
                    $phone = $educationalOrganizationDataRes->getFieldOrSpace('Phones');
                    $emailData = $educationalOrganizationDataRes->getFieldOrSpace('Emails');
                }
                //колдуем с адресом почты
                if( empty($emailData) ){
                    $emailData = $eiisOrg->getFieldSafe('Mails');
                }
                if( empty($phone) ){
                    $phone = $eiisOrg->getFieldSafe('Phones');
                }

                $orgInfo = new Model\LodEntities\RequestOrganizationInfo();
                $orgInfo->setFields([
                    "requestId"         => $newId,
                    "orgId"             => $fields["organizationId"],
                    "fullTitle"         => $eiisOrg->getFieldSafe("FullName"),
                    "shortTitle"        => $eiisOrg->getFieldSafe("ShortName"),
                    "regularName"       => $eiisOrg->getFieldSafe("RegularName"),
                    "kpp"               => $eiisOrg->getFieldSafe("Kpp"),
                    "licenseePhone"     => $phone ,
                    "licenseeEmail"     => $emailData,
                    "additionalInfo"    => "base",
                    "ogrn"              => $eiisOrg->getFieldSafe("GosRegNum"),
                    "inn"               => $eiisOrg->getFieldSafe("Inn"),
                    "fk_eiisEducationalOrganizationProperties" => $eiisOrg->getFieldSafe("fk_eiisEducationalOrganizationProperties")
                ]);
                //Подтягиваем адрес
                //Для нашей организации в заявлении есть юр.адрес: в таблице OrganizationLocation этот адрес имеет тип addressType "OrganizationLocation". Так вот при создании заявления этот гуид должен записаться в RequestOrganizationInfo в поле addressId
                $orgLocTable = new Model\Gateway\LodEntitiesTable("OrganizationLocation");
                $orgLocs = $orgLocTable->getAllEntities(null, null, [
                    "organizationId" => $eiisOrg->getId(),
                    "addressType" => "OrganizationLocation"
                ])->getObjectsArray();
                if(count($orgLocs)>0){
                    $orgLoc = $orgLocs[0];
                    $orgInfo->setFieldSafe("addressId", $orgLoc->getId());
                    $orgInfo->setFieldSafe("address", $orgLoc->getField("address"));
                }

                $orgInfo->save();
            }

            //Если это новое заявление в деле, но не первое, надо скопировать все адреса
            if($dealMainRequest != null && $dealMainRequest->getId() != $newRequest->getId()){
                $orgInfo = $dealMainRequest->getMainOrganizationInfo();
                $newOrg = $newRequest->getMainOrganizationInfo();
                //Теперь подтягиваем все адреса из первого заявления для главной организации заявления
                $addrTable = new Model\Gateway\LodEntitiesTable("LicenseRequestAddress");
                //собираем адреса организации чтобы их скопировать
                $orgAddresses = $addrTable->getAllEntities(null, null, [
                    "requestOrganizationInfoId" => $orgInfo->getId()
                ]);
                //теперь все эти адреса копируем
                foreach($orgAddresses as $oa){
                    $fields = $oa->getFields();
                    unset($fields["id"]);
                    $fields["requestOrganizationInfoId"] = $newOrg->getId();
                    $lra = new Model\LodEntities\LicenseRequestAddress();
                    $lra->setFields($fields);
                    $lra->save();
                }
            }

            //Раз звезды сошлись по реорганизации, забираем все что нужно из предыдущего заявления
            //создаем запись по реорганизованной организации в новое заявление
            if($hasReorg){
                //Подтягиваем реорг организацию из главного заявления
                $reorgOrgInfo = $dealMainRequest->getOneReorganizationData();
                $reorgOrg = $reorgOrgInfo->getReorganizedOrganization();

                //добавляем инфо по реорг организации в это заявление
                $reorgFields = $reorgOrgInfo->getFields();
                unset($reorgFields["id"]);
                $reorgFields["licenseRequestId"] = $newRequest->getId();
                $newReorg = new Model\LodEntities\ReorganizedOrganizationInfo();
                $newReorg->setFields($reorgFields);
                //var_dump($reorgFields);die;
                $newReorg->save();

                //заполняем поля в заявлении по лицензии реорганизованной организации
                $license = $reorgOrg->getLicense();
                if($license != null){
                    $newRequest->setField("licenseId",$license->getId());
                    $newRequest->setField("licenseRegistrationNumber", $license->getField("LicenseRegNum"));
                    $newRequest->setField("licenseDate" ,$license->getField("DateLicDoc"));
                    $newRequest->setField("licenseSeries", $license->getField("SerDoc"));
                    $newRequest->setField("licenseBlankNumber", $license->getField("NumDoc"));
                    $newRequest->setField("issuingAuthority", $license->getField("lod_authorizedPersonId"));
                    $newRequest->save();
                }
            }


            //возвращаем
            $returnArray = array(
                "result" => "success",
                "id" => $newId
            );
        } catch(\Exception $e){
            $returnArray["msg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    /**
     * редактирование заявления
     *
     * @return type
     */
    public function editAction(){
        //$this->accessControl('createRequest');

        $requestId = $this->params("id");
        $isTest = false;
        if($requestId=="test"){
            $requestId = '57d75ee11eff4a4ba396d58116bb0073';
            $isTest = true;
        }
        //$this->params("id");
        if (empty($requestId)) {
            die;
        }

        $frame = $this->params()->fromPost("frame");

        $request = new Model\LodEntities\LicenseRequest($requestId);
        if($request->getId() == null)
            die;

        $sm =  $this->getServiceLocator();
        $requestDialog = new \Eo\Model\RequestFrames\RequestFramesController($request, $sm, $isTest, $frame);
        $dialog = $requestDialog->getDialog();
        return $dialog;
    }

    /**
     * <pre>
     * удаление заявления
     * url: /eo/license/delete/ид-заявления
     *  ответ в виде json
     *  {result: "Ok"} - все путем
     *  при ошибке
     *  {
     *      result: ""  -  все че не Ok - ошибка
     *      message: '' - выведить в титле
     *      console: '' - выведет в консольке
     *
     *  }
     * </pre>
     * @return \Zend\View\Model\JsonModel
     */
    public function deleteAction(){
        $user       = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $userRole   = $user->getRole();
        $request    = $this->getRequest();
        $method     = $request->getMethod();
        $result = array();
        if ($request->isXmlHttpRequest() && $method=='DELETE'){
            $requestId = $this->params('id');
            if( empty($requestId) ){
                $result['result'] = 'Error';
                $result['console'] = 'удаление заявления - не приежали данные';
                $result['message'] = 'Ошибка при удалении';
            }
            else{
                try{
                    $requestClass = new Model\LodEntities\LicenseRequest();
                    $requestClass = $requestClass->getByWhere(array(
                        $requestClass->getIdName() => $requestId,
                    ));
                    if( empty($requestClass) ){
                        $result['result'] = 'Error';
                        $result['console'] = 'удаление заявления - обработка данных - не найдено заявление с ид '.$requestId;
                        $result['message'] = 'Заявление не найдено';
                    }
                    else{
                        $requestClass->delete();
                        $result['result'] = 'Ok';
                    }
                } catch (\Exception $ex) {
                    $result['result'] = 'Error';
                    $result['console'] = 'удаление заявления - обработка данных '
                            .$ex->getMessage();
                    $result['message'] = 'Ошибка при удалении';
                }
            }
            return new \Zend\View\Model\JsonModel($result);
        }
        die( 'только через аякс' );
    }

    /**
     * отправляем на открутую
     * url: /eo/license/send-to-open/ид-заявления
     */
    public function sendToOpenAction(){
        $user       = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $userRole   = $user->getRole();
        $request    = $this->getRequest();
        $method     = $request->getMethod();
        $result = array();
        if ($request->isXmlHttpRequest() && $method=='POST'){
            $requestId = $this->params('id');
            if( empty($requestId) ){
                $result['result'] = 'Error';
                $result['console'] = 'отправка на открытую часть - не приежали данные';
                $result['message'] = 'Ошибка при отправке';
            }
            else{
                try{
                    $dbAdapterLod = \Model\Gateway\LodDbAdapter::getInstance();
                    $driverLod = $dbAdapterLod->getDriver();
                    $connectionLod = $driverLod->getConnection();
                    $connectionLod->beginTransaction();

                    $requestClass = new Model\LodEntities\LicenseRequest();
                    $requestClass = $requestClass->getByWhere(array(
                        $requestClass->getIdName() => $requestId,
                    ));
                    if( empty($requestClass) ){
                        $result['result'] = 'Error';
                        $result['console'] = 'отправка на открытую часть - обработка данных - не найдено заявление с ид '.$requestId;
                        $result['message'] = 'Заявление не найдено';
                    }
                    else{
                        //определяемся это новая отсылка или испраление
                        $isFirstSend = true;
                        $deal = $requestClass->getDeal();
                        if( !empty($deal) ){
                            if( 'WAITING_REPLY' == $deal->getFieldSafe('stateCode') ){
                                $isFirstSend = false;
                            }
                        }
                        //проверка на есть ли скан
                        if( $isFirstSend){
                            $signResult = $requestClass->checkSign( true );
                        }
                        else{
                            $signResult = $requestClass->checkSign( null );
                        }
                        //если с пописью порядок, то обработаем запрос
                        if( 'Ok' == $signResult ){
                            //определяемся это новая отсылка или испраление
                            if( $isFirstSend){
                                $requestClass->setFieldSafe( 'DateSubmission', date('Y-m-d') );
                                $requestClass->setFieldSafe( 'admissionType', 'd41d8cd98f00b204e9800998ecf8427e' );
                                $requestClass->save(false);
                            }
                            else{
                                //меняем статус дела
                                $deal->setNewStatus('RECEIVE_ANSWER');
                                //В документ уведомления о несоответствии записать дату получения
                                //ответа
                                $documentClass = new \Model\LodEntities\document();
                                $documentClass = $documentClass->getByWhere(array(
                                    'DocumentTypeId'    => '220e621a119846389aa7bb37709d381a',
                                    //'stateCode'         => 'SEND',
                                    'stateCode'         => 'REGISTERED',
                                    new \Zend\Db\Sql\Predicate\IsNotNull( 'sendDate ' ),
                                    'receiveAnswerDate' => null,
                                    'dealId'            => $deal->getId(),
                                ));
                                if( empty($documentClass) ){
                                    throw new \Exception('не найден отосланый документ');
                                }
                                $documentClass->setFieldSafe( 'receiveAnswerDate', date('Y-m-d') );
                                $documentClass->setFieldSafe( 'stateCode', 'RECEIVED_ANSWER' );
                                $documentClass->save( false );
                            }
                            $result['result'] = 'Ok';
                            $connectionLod->commit();
                        }
                        else{
                            $result['result'] = 'Error';
                            $result['console'] = 'отправка на открытую часть - обработка данных - проверка подписи - '.$signResult;
                            $result['message'] = ' Проверка подписи: '.$signResult;
                        }
                    }
                }
                catch (\Exception $ex) {
                    $connectionLod->rollback();
                    $result['result'] = 'Error';
                    $result['console'] = 'отправка на открытую часть - обработка данных '
                            .$ex->getMessage();
                    $result['message'] = 'Ошибка при отправке';
                }
            }
            return new \Zend\View\Model\JsonModel($result);
        }
        die( 'только через аякс' );
    }


    /**
     * модуль новых заявлений
     *
     * url: /eo/license/sended
     *
     * @return ViewModel
     */
    function sendedAction(){

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 3;
        $where = array();
        $pagePaginator = 1;

        $allPage = 1;

        //для фильтров - процедуры
        $licenseProcedureAll = new Model\LodEntities\LicensingProcedure();
        $licenseProcedureAll = $licenseProcedureAll->getAllByWhereParseId(array(
            $licenseProcedureAll->getIdName() => $licenseProcedureAll->getLicensingProcedureForOpenId()
        ));
        uasort(
            $licenseProcedureAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        //для фильтров - причины обращения
        $licenseReasonAll = new Model\LodEntities\LicenseReasonCatalogItem();
        $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId( array(
                $licenseReasonAll->getIdName() => $licenseReasonAll->getLicenseReasonForOpenId()
        ));
        uasort(
            $licenseReasonAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;
        $requestsAll = array();

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('licenseProcedureAll', $licenseProcedureAll);
        $view->setVariable('licenseReasonAll', $licenseReasonAll);
        $view->setVariable('requestsAll', $requestsAll);
        return $view;
    }


    /**
     * получаем записи о отправленых заявлениях
     *
     * url: /eo/license/sended-page
     *
     * @return ViewModel
     */
    function sendedPageAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 50;
        $where = array();
        $where = new \Zend\Db\Sql\Where();

        $pagePaginator = $this->getRequest()->getPost('page',1);
        $width = $this->getRequest()->getPost('width',1366);
        if($width <= 1366) $max_count_page = 10;

        //$where[] = new \Zend\Db\Sql\Predicate\IsNull("dealId");
        //$where[] = new \Zend\Db\Sql\Predicate\IsNull('DateSubmission');
        $where->isNotNull("LicenseRequest.DateSubmission");
        $where->like('LicenseRequest.organizationId', $educationOrganization->getId());

        $whereIn = new \Zend\Db\Sql\Where();

            $whereIn->isNull("LicenseRequest.dealId");
            $whereIn->or;

            $selectIn = new \Zend\Db\Sql\Select();
            $selectIn->from('Deal');
            $selectIn->where->like('organizationId', $educationOrganization->getId());
            $selectIn->where->notLike('stateCode', 'WAITING_REPLY');
            $selectIn->columns(array('id'));
            $whereIn->in('LicenseRequest.dealId',$selectIn);

        $where->addPredicate($whereIn);


        //добавляем фильтры
        $filter = $this->getRequest()->getPost('filter','');
        //по процедуре
        if( !empty($filter["licenseProcedure"]) ){
            $where->like('LicenseRequest.licensingProcedureId',$filter["licenseProcedure"]);
            //$where[] = new \Zend\Db\Sql\Predicate\Like('licensingProcedureId',$filter["licenseProcedure"]);
        }
        //по причине
        if( !empty($filter["licenseReason"]) ){
            /*
            $licenseReasonAll = new Model\LodEntities\LicenseReasonToLicenseRequest();
            $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId(
                    array(
                        new \Zend\Db\Sql\Predicate\Like('licenseReasonId',$filter["licenseReason"]),
                    ),
                    'licenseRequestId'
            );
            $licenseReasonAll =  array_keys($licenseReasonAll);
            $where->in('id',$licenseReasonAll);
            */
            $selectIn = new \Zend\Db\Sql\Select();
            $selectIn->from('LicenseReasonToLicenseRequest');
            $selectIn->where->like('licenseReasonId',$filter["licenseReason"]);
            $selectIn->columns(array('licenseRequestId'));
            $where->in('LicenseRequest.id',$selectIn);
        }
        //под дате заполнени я с
        if( !empty($filter["fillingDateBegin"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateBegin"]));
            if( !empty($date)){
                $where->greaterThanOrEqualTo('LicenseRequest.fillingDate',$date);
            }
        }
        //под дате заполнени я по
        if( !empty($filter["fillingDateEnd"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateEnd"]));
            if( !empty($date)){
                $where->lessThanOrEqualTo('LicenseRequest.fillingDate',$date);
            }
        }
        //под дате отправки я с
        if( !empty($filter["submissionDateBegin"]) ){
            $date = date('Y-m-d',  strtotime($filter["submissionDateBegin"]));
            if( !empty($date)){
                $where->greaterThanOrEqualTo('LicenseRequest.DateSubmission',$date);
            }
        }
        //под дате отправки я по
        if( !empty($filter["submissionDateEnd"]) ){
            $date = date('Y-m-d',  strtotime($filter["submissionDateEnd"]));
            if( !empty($date)){
                $where->lessThanOrEqualTo('LicenseRequest.DateSubmission',$date);
            }
        }


        $sortField = "LicenseRequest.fillingDate DESC";
        $sortPost = $this->getRequest()->getPost('sort','');
        if( !empty($sortPost) ){
            if( 'fillingDate' == substr($sortPost, 0, 11) ){
                $sortField = 'LicenseRequest.'.$sortPost ;
            }
            if( 'number' == substr($sortPost, 0, 6) ){
                $sortField = 'LicenseRequest.'.$sortPost ;
            }
            if( 'DateSubmission' == substr($sortPost, 0, 14) ){
                $sortField = 'LicenseRequest.'.$sortPost ;
            }
            if( 'Deal.number' == substr($sortPost, 0, 11) ){
                $sortField = $sortPost;
            }
            if( 'catalog_deal.russian_name' == substr($sortPost, 0, strlen('catalog_deal.russian_name')) ){
                $sortField = $sortPost;
            }
            if( 'LicensingProcedure.title' == substr($sortPost, 0, strlen('LicensingProcedure.title')) ){
                $sortField = $sortPost;
            }
            if( 'LicenseReasonCatalogItem.title' == substr($sortPost, 0, strlen('LicenseReasonCatalogItem.title')) ){
                $sortField = $sortPost;
            }
        }

        $reqAll = new Model\LodEntities\LicenseRequest();
        $reqAll = $reqAll->selectComplicated(
                'LicenseRequest',
                array(),
                $where,
                'id',//$group
                $sortField,
                $max_count_page,
                $pagePaginator-1,
                array(
                    'Deal',
                    'catalog_deal',
                    'LicensingProcedure',
                    'LicenseReasonToLicenseRequest',
                    'LicenseReasonCatalogItem',
                ),
                array(
                    'LicenseRequest.dealId = Deal.id',
                    'Deal.stateCode = catalog_deal.origin_name',
                    'LicenseRequest.licensingProcedureId = LicensingProcedure.id',
                    'LicenseReasonToLicenseRequest.licenseRequestId = LicenseRequest.id',
                    'LicenseReasonCatalogItem.id = LicenseReasonToLicenseRequest.licenseReasonId'
                ) ,
                array(
                    array('Deal.number'=>'number'),
                    array('catalog_deal.russian_name'=>'russian_name'),
                    array('LicensingProcedure.title'=>'title'),
                    //array(' LicenseReasonToLicenseRequest.licenseReasonId'=>'licenseReasonId'),
                    //array(' LicenseReasonCatalogItem.title'=>'title'),
                    array(' LicenseReasonCatalogItem.title'=> new \Zend\Db\Sql\Expression('GROUP_CONCAT(LicenseReasonCatalogItem.title order by LicenseReasonCatalogItem.title SEPARATOR ", ")') )
                ),//$left_column
                array('LEFT','LEFT','LEFT','INNER','LEFT')//$joinType
        );
        $requestsAll = array();
        foreach ( $reqAll as $requestsKey => $reqOne ){
            $sto =  $reqOne->getArrayCopy();
            $requestsOne = new Model\LodEntities\LicenseRequest();
            $requestsOne->setFieldsSafe($sto);
            $requestsAll[$requestsOne->getId()] = $requestsOne;
        }
        $ApplClass = new \Model\Gateway\LodEntitiesTable("LicenseRequest");
        /*
        $requestsAll = $ApplClass->getAllEntities(array(
                "page"   => $pagePaginator,
                "onPage" => $max_count_page
            ),
            $sortField,
            $where)->getObjectsArray();
        */
        $requestsCount = $ApplClass->getCount($where);

        $allPage = ceil($requestsCount / $max_count_page);

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('requestsAll', $requestsAll);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('requestsCount', $requestsCount);
        return $view;
    }

    /**
     * считаем количество записей в базе, для меню слева
     *
     * url: /eo/license/get-count
     * ответ:
     *  {
     *      "result":"Ok",      результат - Ok - путем, прочеее - ошибка
     *      "data":{            данные ответа в виде ключ значение, впихивается в
     *          "new":"22",     ul.steps_div li.btn a span.num[step="_Ключ_"]
     *          "sended":"6"    в меню вставляестяв сран по степ
     *          "reply":2
     *      }
     *  }
     *
     * @return \Zend\View\Model\JsonModel
     */
    function getCountAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        //новые
        $where = new \Zend\Db\Sql\Where();
        $where->isNull("dealId");
        //$where[] = new \Zend\Db\Sql\Predicate\IsNull('DateSubmission');
        $where->isNull("DateSubmission");
        $where->like('organizationId', $educationOrganization->getId());
        $ApplClass = new \Model\Gateway\LodEntitiesTable("LicenseRequest");
        $requestsCountNew = $ApplClass->getCount($where);

        //отправленые
        $where = new \Zend\Db\Sql\Where();
        //$where->isNull("dealId");
        //$where[] = new \Zend\Db\Sql\Predicate\IsNull('DateSubmission');
        $where->isNotNull("DateSubmission");
        $where->like('organizationId', $educationOrganization->getId());
        $whereIn = new \Zend\Db\Sql\Where();

            $whereIn->isNull("dealId");
            $whereIn->or;

            $selectIn = new \Zend\Db\Sql\Select();
            $selectIn->from('Deal');
            $selectIn->where->like('organizationId', $educationOrganization->getId());
            $selectIn->where->notLike('stateCode', 'WAITING_REPLY');
            $selectIn->columns(array('id'));
            $whereIn->in('dealId',$selectIn);

        $where->addPredicate($whereIn);

        $ApplClass = new \Model\Gateway\LodEntitiesTable("LicenseRequest");
        $requestsCountSended = $ApplClass->getCount($where);

        //заявлений с выявлеными нарушениями
        $where = new \Zend\Db\Sql\Where();
        $where->like('organizationId', $educationOrganization->getId());
        $whereIn = new \Zend\Db\Sql\Where();
            $whereIn->isNotNull("dealId");
            $selectIn = new \Zend\Db\Sql\Select();
            $selectIn->from('Deal');
            $selectIn->where->like('stateCode', 'WAITING_REPLY');
            $selectIn->columns(array('id'));
            $whereIn->in('dealId',$selectIn);
        $where->addPredicate($whereIn);

        $ApplClass = new \Model\Gateway\LodEntitiesTable("LicenseRequest");
        $requestsCountReply = $ApplClass->getCount($where);


        //уведобленичя
        $where = new \Zend\Db\Sql\Where();
        $dealAll = new \Model\LodEntities\Deal();
        $dealAll = $dealAll->getAllByWhereParseId(array(
            'organizationId' => $educationOrganization->getId(),
        ));
        $docTypeAll = new \Model\LodEntities\DocumentTypeCatalogItem();
        $docTypeAll = $docTypeAll->getAllByWhereParseId(array(
            'parent' => '20a4674be28c4b36ad69de14a0e4c8df',
        ));
        $docTypeIdAll = array_keys($docTypeAll);
        $where->in('Document.documentTypeId',$docTypeIdAll);
        $where->isNotNull("Document.sendDate");
        $dealIdArray = array_keys($dealAll);
        $where->in('Document.dealId', $dealIdArray);
        $ApplClass = new \Model\Gateway\LodEntitiesTable("document");
        $requestsCountNotify = $ApplClass->getCount($where);

        $result = array();
        $result['result'] = 'Ok';
        $result['data']['new']    = $requestsCountNew;
        $result['data']['sended'] = $requestsCountSended;
        $result['data']['reply']  = $requestsCountReply;
        $result['data']['notify']  = $requestsCountNotify;
        return new \Zend\View\Model\JsonModel($result);
    }

    /**
     * модуль новых заявлений с выявленными нарушениями
     *
     * url: /eo/license/reply
     *
     * @return ViewModel
     */
    function replyAction(){

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 3;
        $where = array();
        $pagePaginator = 1;

        $allPage = 1;

        //для фильтров - процедуры
        $licenseProcedureAll = new Model\LodEntities\LicensingProcedure();
        $licenseProcedureAll = $licenseProcedureAll->getAllByWhereParseId(array(
            $licenseProcedureAll->getIdName() => $licenseProcedureAll->getLicensingProcedureForOpenId()
        ));
        uasort(
            $licenseProcedureAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        //для фильтров - причины обращения
        $licenseReasonAll = new Model\LodEntities\LicenseReasonCatalogItem();
        $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId( array(
                $licenseReasonAll->getIdName() => $licenseReasonAll->getLicenseReasonForOpenId()
        ));
        uasort(
            $licenseReasonAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;
        $requestsAll = array();

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('licenseProcedureAll', $licenseProcedureAll);
        $view->setVariable('licenseReasonAll', $licenseReasonAll);
        $view->setVariable('requestsAll', $requestsAll);
        return $view;
    }

    /**
     * получаем записи о отправленых заявлениях
     *
     * url: /eo/license/reply-page
     *
     * @return ViewModel
     */
    function replyPageAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 50;
        $where = array();
        $where = new \Zend\Db\Sql\Where();

        $pagePaginator = $this->getRequest()->getPost('page',1);
        $width = $this->getRequest()->getPost('width',1366);
        if($width <= 1366) $max_count_page = 10;

        //$where[] = new \Zend\Db\Sql\Predicate\IsNull("dealId");
        //$where[] = new \Zend\Db\Sql\Predicate\IsNull('DateSubmission');
        //$where->isNotNull("DateSubmission");
        $where->like('organizationId', $educationOrganization->getId());


        $whereIn = new \Zend\Db\Sql\Where();

            $whereIn->isNotNull("dealId");
            //$whereIn->or;

            $selectIn = new \Zend\Db\Sql\Select();
            $selectIn->from('Deal');
            $selectIn->where->like('stateCode', 'WAITING_REPLY');
            $selectIn->columns(array('id'));
            $whereIn->in('dealId',$selectIn);

        $where->addPredicate($whereIn);

        //добавляем фильтры
        $filter = $this->getRequest()->getPost('filter','');
        //по процедуре
        if( !empty($filter["licenseProcedure"]) ){
            $where->like('licensingProcedureId',$filter["licenseProcedure"]);
            //$where[] = new \Zend\Db\Sql\Predicate\Like('licensingProcedureId',$filter["licenseProcedure"]);
        }
        //по причине
        if( !empty($filter["licenseReason"]) ){
            /*
            $licenseReasonAll = new Model\LodEntities\LicenseReasonToLicenseRequest();
            $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId(
                    array(
                        new \Zend\Db\Sql\Predicate\Like('licenseReasonId',$filter["licenseReason"]),
                    ),
                    'licenseRequestId'
            );
            $licenseReasonAll =  array_keys($licenseReasonAll);
            $where->in('id',$licenseReasonAll);
            */
            $selectIn = new \Zend\Db\Sql\Select();
            $selectIn->from('LicenseReasonToLicenseRequest');
            $selectIn->where->like('licenseReasonId',$filter["licenseReason"]);
            $selectIn->columns(array('licenseRequestId'));
            $where->in('id',$selectIn);
        }
        //под дате заполнени я с
        if( !empty($filter["fillingDateBegin"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateBegin"]));
            if( !empty($date)){
                $where->greaterThanOrEqualTo('fillingDate',$date);
            }
        }
        //под дате заполнени я по
        if( !empty($filter["fillingDateEnd"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateEnd"]));
            if( !empty($date)){
                $where->lessThanOrEqualTo('fillingDate',$date);
            }
        }
        //под дате отправки я с
        if( !empty($filter["submissionDateBegin"]) ){
            $date = date('Y-m-d',  strtotime($filter["submissionDateBegin"]));
            if( !empty($date)){
                $where->greaterThanOrEqualTo('DateSubmission',$date);
            }
        }
        //под дате отправки я по
        if( !empty($filter["submissionDateEnd"]) ){
            $date = date('Y-m-d',  strtotime($filter["submissionDateEnd"]));
            if( !empty($date)){
                $where->lessThanOrEqualTo('DateSubmission',$date);
            }
        }


        $ApplClass = new \Model\Gateway\LodEntitiesTable("LicenseRequest");
        $requestsAll = $ApplClass->getAllEntities(array(
                "page"   => $pagePaginator,
                "onPage" => $max_count_page
            ),
            "fillingDate DESC",
            $where)->getObjectsArray();
        $requestsCount = $ApplClass->getCount($where);

        $allPage = ceil($requestsCount / $max_count_page);

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('requestsAll', $requestsAll);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('requestsCount', $requestsCount);
        return $view;
    }


    /**
     * формирвоание документоы
     *
     * формируем документ и отдает ссылку на него
     * /eo/license/make-document/requestmaket
     * /eo/license/make-document/requestmaket/"+licenseId
     *  requestmaket - какой документ делать
     *      requestmaket - макет заявления
     *
     * в гет
     *     documentDebug=1      - не вормируем документ, а возращаем составной массив
     *     isDebug=1            - не вормируем документ, а возращаем составной массив
     *
     * @throws \Exception
     */
    public function makeDocumentAction(){
        $documentDebug = $this->params()->fromQuery( 'documentDebug', '');
        if( empty($documentDebug) ){
            $documentDebug = $this->params()->fromQuery( 'isDebug', '');
        }
         try {
            $document = $this->params('id');
            $docId = $this->params('id2');
            $data = array();
            $documentClass = null;
            $dealId = null;
            $code = '';
            $outType = '';
            if ('requestmaket' == strtolower($document)) {
                $documentClass = \Eo\Model\Licensing\Request\Request::RequestFabric($docId);
                $outType = 'pdf';
            }
            if( empty($documentClass) ){
                throw new \Exception('Не определен тип формируемого документа');
            }
            if( !empty($documentDebug) ){
                $documentClass->setDebug(true);
            }
            if( !empty($outType) ){
                $documentClass->setOurDocType($outType);
            }
            $documentClass->makeDocumentOut();
            die;
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo '<div id="error" style="display:none">'.$e->getCode().' '.$e->getMessage().' '.$e->getLine().PHP_EOL;
            echo $e->getTraceAsString().'</div>';
            echo '<script>console.error(document.getElementById("error").textContent);</script>';
            die;
        }

    }

    public function reMakeDocumentAction(){
        $documentDebug = $this->params()->fromQuery( 'documentDebug', '');
        if( empty($documentDebug) ){
            $documentDebug = $this->params()->fromQuery( 'isDebug', '');
        }
        try {
            //хочу через транзакцию
            $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
            $driver = $dbAdapter->getDriver();
            $connection = $driver->getConnection();
            $connection->beginTransaction();

            $document = $this->params('id');
            $docId = $this->params('id2');
            $data = array();
            $documentClass = null;
            $dealId = null;
            $code = '';

            if ('request' == strtolower($document)) {
                $documentClass = \Eo\Model\Licensing\Request\Request::RequestFabric($docId);
                $code = 'Request';
            }
            if( !empty($documentDebug) ){
                $result = $documentClass->getData();
                header('Content-Type: text/plain');
                print_r($result);
                die;
            }
            $content = $documentClass->makeDocument();
            $fileName = $documentClass->getFileOutName();
            //хрень какаято, вылазиит нотисе не знаю где
            //Notice: Use of undefined constant table - assumed 'table' in Unknown on line 0
            //не занаю что это
            //upd by Sheremet пофиксил notice  проблема была в классе EiFile в $ownerClassCodeToTable название элемента table не было взято в кавычки
            @$fileBank = new \Model\LodEntities\EiFile();
            $resultSend = $fileBank->updateByCodeIdContent($dealId, $code, $docId, $fileName, $content);
            if (empty($resultSend)) {
                throw new \Exception('Лод - формирование документов - ошибка при отправке файла');
            }
            $result['status'] = 'Ok';
            $result['message'] = 'Макет сформирован';
            $result['link'] = $resultSend;
            $result['fileName'] = $fileName;
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            $result['status'] = 'Error';
            $result['message'] = $e->getMessage();
            $result['console'] = $e->getMessage() . '\n' . str_replace(PHP_EOL, '\n', $e->getTraceAsString());
        }
        return new \Zend\View\Model\JsonModel($result);
    }


    /**
     * запихиваем произвольный файл в файлохранилище
     * url:  /eo/license/send-file
     *
     * вход в ПОСТ
     *          ownerClassCode          - код чего делать
     *          ownerId                 - ид  записи по которой впихивается
     *          dealId                  - ид дела
     *      в ФАЙЛ                      - файл, который впихивается
     *
     *
     * @throws \Exception
     */
    public function sendFileAction()
    {
        $result = array();
        try {
            $request = $this->getRequest();
            $post = $request->getPost()->toArray();

            if (empty($post['ownerClassCode']) || empty($post['ownerId']) || (empty($_FILES) && 'deleteRequestDocumentFile' !=  $post['ownerClassCode'])
            ) {
                throw new \Exception('Лицензирование - впихивание файла - Не приехали данные ');
            }
            //хочу через транзакцию
            $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
            $driver = $dbAdapter->getDriver();
            $connection = $driver->getConnection();
            $connection->beginTransaction();
            //выбираем данные из запроса
            $ownerClassCode = trim(strip_tags($post['ownerClassCode']));
            $ownerId = trim(strip_tags($post['ownerId']));
            $fileId = null;

            $docId = $ownerId;
            $document = $ownerClassCode;
            $documentClass = null;
            $documentDelete = null;
            $dealId = null;
            if (!empty($post['dealId'])) {
                $dealId = trim(strip_tags($post['dealId']));
            }
            $code = '';
            if ('requestscan' == strtolower($document)) {
                $documentClass = true;
                $code = 'RequestScan';
            }
            if ('requestdocument' == strtolower($document)) {
                $documentClass = true;
                $code = 'RequestDocument';
            }
            //заявление подписагое электронной подписью
            if ('requestsign' == strtolower($document)) {
                $documentClass = true;
                $code = 'RequestSign';
                $request = new \Model\LodEntities\LicenseRequest($ownerId);
                $dealId = $request->getDeal();
                unset( $request );
                $fileId = empty($post['fileId'])?'':$post['fileId'];
                if (empty($fileId)) {
                    throw new \Exception('Загружаемый документ не содержит ЭП');
                }
                if( 'new file' == $fileId ){
                    $fileId = true;
                }
            }
            //загоняем документы с карточки заявления
            if( 'requestfile' == strtolower($document) ){
                //ownerId - ид заявления 
                //fileId  - ид типа документа
                $documentClass = true;
                $code = 'RequestFile';
                $fileId = true;
                $docTypeId = empty($post['fileId'])?'':$post['fileId'];
                if( empty($docTypeId) ){
                    throw new \Exception('лиценнзирование - записать в файл - Не указан тип документа ');
                }
                //проверим на док тийп
                $docFile = new \Model\LodEntities\LicenseReasonDocument();
                $docFile = $docFile->getByWhere( array('DocumentTypeId' => $docTypeId) );
                if( empty($docFile) ){
                    throw new \Exception('лиценнзирование - записать в файл - - не найден тип документа');
                }
                //определимся надо подпись на документв или не надо
                $isSignCheck = (int)$docFile->getFieldSafe( 'CheckSign' );
                                                                
                $docFile = new \Model\LodEntities\LicenseRequestDocument();
                $docFile->setField( 'LicenseRequestId' , $ownerId );
                $docFile->setField( 'DocumentTypeId' , $docTypeId );
                $docFile->save(  true );
                $docId = $docFile->getId();
            }
            //заменяем документы с карточки заявления
            if( 'requestfilechange' == strtolower($document) ){
                $documentClass = true;
                $code = 'RequestFile';
                $fileId = empty($post['fileId'])?'':$post['fileId'];
                if( empty($fileId) ){
                    throw new \Exception('лиценнзирование - записать документ в файл - Не найден ');
                }
            }
            //удаляем файлы
            if( strtolower('deleteRequestDocumentFile') == strtolower($document) ){
                $documentClass = true;
                $documentDelete = true;
                @$fileBank = new \Model\LodEntities\EiFile($ownerId);
                if( $fileBank->isEmpty() ){
                    throw new \Exception('Лод - формирование документов - ошибка при выборке записи файла');
                }
                $ownerCode = $fileBank ->getFieldSafe('ownerClassCode');
                if( 'RequestFile' != $ownerCode ){
                    throw new \Exception('Лод - формирование документов - неверный тип документа записи файла');
                }
                $filePath = $fileBank ->getPathOnServer();
                $ownerId = $fileBank ->getFieldSafe('ownerId');
                @$fileBankAll = new \Model\LodEntities\EiFile();
                $fileBankAll = $fileBankAll->getAllByWhereParseId( array('ownerId' => $ownerId) );
                if( 1 == count($fileBankAll) ){
                    $reqDocClass = new Model\LodEntities\LicenseRequestDocument($ownerId);
                    if( $reqDocClass->isEmpty() ){
                        throw new \Exception('Лод - формирование документов - неверный тип записи документа ');
                    }
                    $reqDocClass->delete();
                }
                $fileBank->delete();
                @unlink( $filePath  );
            }

            if (empty($documentClass)) {
                throw new \Exception('лиценнзирование - записать в файл - Не найден формирователь');
            }
            $error = "";
            if( !empty($documentDelete ) ){


                $result['message'] = 'Файл удален';
                $result['status'] = 'Ok';
                $connection->commit();
            }
            //$SendFileClass = new \Eo\Model\SendFile();
            elseif( !empty($_FILES) && !empty($_FILES['file']) ) {
                $one_file = $_FILES['file'];
                //если с ошибкой
                if (empty($one_file["size"]) || !empty($one_file["error"])) {
                    throw new \Exception('Лицензирование - записать в файл - ошибка в файле');
                }
                //берем файл
                //$content = file_get_contents($one_file['tmp_name']);
                $content = '';
                $fileName = $one_file['name'];
                
                if( !empty($isSignCheck) ){
                    $sign_class = new \Eo\Service\Sign();
                    $tmpDir = sys_get_temp_dir();
                    $tmpDir = $tmpDir . '/'.uniqid('ftgdertgbvgyrdth_');
                    if( !mkdir( $tmpDir ) ){
                        throw new \Exception( 'проверка подписи - не смогло создать временный  каталог ' . $tmpDir );
                    }
                    $tmpFile = $tmpDir . '/' . $one_file['name'];
                    copy( $one_file['tmp_name'] , $tmpFile );
                    $sign_data = $sign_class->validSignature( $tmpFile , true , true  );
                    @unlink( $tmpFile );
                    @rmdir($tmpDir);
                    if( 'ok' != $sign_data['status'] ){
                        throw new \Exception( 'Ошибка проверка подписи: ' . $sign_class->getErrorText($sign_data['result']) );
                    }      
                    unset( $tmpDir );
                    unset( $tmpFile );
                }
                
                
                //впихиваем в файлохранилище
                @$fileBank = new \Model\LodEntities\EiFile();
                $resultSend = $fileBank->updateByCodeIdContent(
                        $dealId, $code, $docId, $fileName,
                        $content, $one_file['tmp_name'], $fileId );
                if (empty($resultSend)) {
                    throw new \Exception('Лод - формирование документов - ошибка при отправке файла');
                }
                
                
                $dateSend = '';
                //после внесения заявление подписагое электронной подписью
                if ('requestsign' == strtolower($document)) {
                    $dateSend = $fileBank->getDate('created');
                    $dateSend = empty( $dateSend )?'':' ('.$dateSend .')';
                    //после впихивания подписаного выдерим контент самой подписи
                    $sign_class = new \Eo\Service\Sign();
                    //$result_verify = $sign_class->verify_name_signfile( $fileBank->getUrl() );
                    //if( true === $result_verify){
                        $sign_data = $sign_class->validSignature( $fileBank->getUrl() );
                        //$sign_data['status'] =  'ok';
                        if( 'ok' == $sign_data['status'] ){
                            $result_content = $sign_class->getContentReal( $fileBank->getUrl(), array('isGetContent' =>  true, 'realFileName' => $fileBank->getFileName() ) );
                            if (array_key_exists('status', $result_content) && $result_content['status'] == 'ok' && !empty($result_content['file_content'])) {
                                /*
                                $applicationFiles_class->exchangeArray(['Id' => $file->getField("Id"), "isArhive" => 1]);
                                $applicationFiles_class->save();
                                $applicationFiles_class_dubl = new \Model\Entities\isgaApplicationFiles();
                                $save_unpack_file_data = [];
                                $save_unpack_file_data['RealName'] = $sign_class->get_real_name($file->getField("RealName"));
                                $save_unpack_file_data['HashName'] = $result_content['file_name'];
                                $save_unpack_file_data['fk_isgaDocumentType'] = $file->getField("fk_isgaDocumentType");
                                $save_unpack_file_data['fk_isgaApplication'] = $id_appl;
                                $applicationFiles_class_dubl->exchangeArray($save_unpack_file_data);
                                $applicationFiles_class_dubl->save(1);

                                $save_data['isArhive'] = 1;
                                */
                                @$fileScanBank = new \Model\LodEntities\EiFile();
                                $fileScanBank->updateByCodeIdContent(
                                        $dealId, 'RequestScan', $docId, $result_content['file_name'],
                                        $result_content['file_content'], '', null);
                            }
                            else{
                                throw new \Exception( $sign_class->getErrorText($result_content['result']) );
                            }
                        }
                        else{
                            throw new \Exception( $sign_class->getErrorText($sign_data['result']) );
                        }
                    //}
                }
                $result['status'] = 'Ok';
                $result['message'] = 'Файл записан';
                $result['link'] = $resultSend;
                $result['fileName'] = $fileName.$dateSend;
                $result['fileId'] = $fileBank->getId();
                if( !empty($fileScanBank) ){
                    $result['scanLink'] = $fileScanBank->getUrl();
                    $result['scanName'] = $fileScanBank->getFileName();
                }
                $connection->commit();
            }
            else{
                throw new \Exception('ничего не сделано');
            }
        } catch (\Exception $e) {
            if( !empty($connection) ){
                $connection->rollback();
            }
            $result['status'] = 'Error';
            $result['message'] = $e->getMessage();
            $result['console'] = str_replace(PHP_EOL, '\n', $e->getTraceAsString());
        }
        return  new \Zend\View\Model\JsonModel($result);
        echo json_encode($result);
        die;
    }



    /**
     * карточка лицензии
     *
     * нужно чтоб завелося персональный обработчик на кнопки
     *
     * на кнопи изменений в реестре добавил изменения в лб
     * ALTER TABLE `status_history` ADD `comment` VARCHAR(520) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'Коментарии' AFTER `StateNew`;                                                                                                                                                               
     * ALTER TABLE `ReestrStatusButton` ADD `OnClick` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'обработчик тыка по кнопке' ;
     * в таблице поле 4  ButtonType next - funct
     * в таблице поле 3  ButtonType next - funct
     * в таблице поле 3 Name Приостановить в целом - Приостановить действие
     * в таблице поле 4  TypeReestr License -  нулл
     * в таблице поле 3  OnClick - licenseSlopDoDialogAction(jQuery('#licenseForm #LicenseId').val());
     * в таблице поле 7  OnClick - licenseResumeDialogAction(jQuery('#licenseForm #LicenseId').val());
     * в таблице поле 8  OnClick - licenseResumeDialogAction(jQuery('#licenseForm #LicenseId').val());
     * в таблице поле 12 OnClick - licenseResumeDialogAction(jQuery('#licenseForm #LicenseId').val());
     * в таблице поле 5  OnClick - licenseResumeDialogAction(jQuery('#licenseForm #LicenseId').val(),'annuling');
     * в таблице поле 9  OnClick - licenseResumeDialogAction(jQuery('#licenseForm #LicenseId').val(),'annuling');
     * в таблице поле 11 OnClick - licenseResumeDialogAction(jQuery('#licenseForm #LicenseId').val(),'annuling');
     * INSERT INTO `ReestrStatusButton` ( `CurrentStatus`, `NextStatus`, `Message`, `comm`, `TypeReestr`, `Name`, `TypeCode`, `ButtonType`, `FromGroup`, `OnClick`) VALUES
     *  ( '0727E76E83574B10833A25CA627FE6A0', '9416C0E6E84F4194B57DA930AD10A4D0', 'Возобновить действие', 0, 'Supplement', 'Возобновить', 'stateCode', 'next', 'expert', 'licenseResumeDialogAction(jQuery(''#supplementInfoForm input[name=Id]'').val(),''supplement-validing'',{''isSupplement'':1});');
     * в таблице поле 10 OnClick - licenseResumeDialogAction(jQuery('#licenseForm #LicenseId').val());
     * до этого внес в рон
     *
     * в таблице поле 2,43,44  OnClick - licenseSlopDoDialogAction(jQuery('#licenseForm #LicenseId').val(), {'actionName': 'notValidCheking' });
     *
     * @return type
     */
    public function getLicenseInfoAction()
    {

        $id_lic = $this->params("id");

        $licenseModel = new \Eo\Model\Licensing\License();
        $license = $licenseModel->getLicenseInfo($id_lic);
        $licenseInfoService = new \Eo\Model\Licensing\Service\LicenseInfoService($id_lic);
        $buttons = array();//= $licenseInfoService->getLicense()->getButtons();

        $rights = [
            'changeLicenseStatus'     => $this->getRigthsStatus('changeLicenseStatus'),
            'changeLicensePostDate'   => $this->getRigthsStatus('changeLicensePostDate'),
            'changeLicenseHandleDate' => $this->getRigthsStatus('changeLicenseHandleDate'),
            'LicenseEdit'             => $this->getRigthsStatus('LicenseEdit'),
            'viewOrganizations'       => $this->getRigthsStatus('viewOrganizations'),
            'expertButton'            => $this->getRigthsStatus('expertButton'),
            'printButton'             => $this->getRigthsStatus('printButton'),
            'extraditionButton'       => $this->getRigthsStatus('extraditionButton'),
        ];

        $handDocument = [
            'url'  => '',
            'name' => ''
        ];

        if ($licenseInfoService->getDocumentById()) {
            $thandDocument = new \Model\LodEntities\document(
              $licenseInfoService->getDocumentById()->getField('documentId')
            );
            $eoFile = $thandDocument->getFileScanCopy();
            if ($eoFile) {
                $handDocument = [
                    'url'  => $eoFile->getUrl(),
                    'name' => $eoFile->getFileName()
                ];
            }
        }

        $endDocument = [
            'url'  => '',
            'name' => ''
        ];

        if ($licenseInfoService->getEndDocumentById()) {
            $tendDocument = new \Model\LodEntities\document(
              $licenseInfoService->getEndDocumentById()->getField('documentId')
            );
            $eoFile = $tendDocument->getFileScanCopy();
            if ($eoFile) {
                $endDocument = [
                    'url'  => $eoFile->getUrl(),
                    'name' => $eoFile->getFileName()
                ];
            }
        }

        return $this->getViewModel([
              "license"              => $licenseInfoService->getLicense(),
              "supplements"          => \Eo\Model\Licensing\Service\LicenseSupplementInfoService::supplementsObjectArrInfoRowPrepare(
                $licenseInfoService->getSupplements()
              ),
              'Buttons'              => $buttons,
              "LicenseState"         => $licenseInfoService->getStates(),
              "controlOrgans"        => $licenseInfoService->getControlOrgans(),
              'authorizedPersons'    => $licenseInfoService->getAuthorizedPerson(),
              'notValidLicense'      => $licenseInfoService->getNotValidLicenses(),
              'licenseSignStates'    => $licenseInfoService->getSignStates(),
              'documentsOnDelivery'  => $licenseInfoService->getDocumentOnDelivery(),
              'deal'                 => $licenseInfoService->getDealFromDoc(),
              'dealFromEndDoc'       => $licenseInfoService->getDealFromEndDoc(),
              'licenseIsGranted'     => $licenseInfoService->getLicenseGranted(),
              'licenseEndDocument'   => $licenseInfoService->getEndDocument(),
              'licenseOtherDocument' => $licenseInfoService->getOtherDocuments(),
              'licenseTitle'         => $licenseInfoService->getTitle(),
              'rights'               => $rights,
              'urlHandDoc'           => $handDocument,
              'urlEndDoc'            => $endDocument,
        ]);
    }

    /**
     * возвращает готовый инициализированный ViewModel(Для сокращения кода)
     * @param type $arr
     * @return ViewModel
     */
    public function getViewModel($arr = [])
    {
        $view = new ViewModel();
        $view->setVariables($arr);
        $view->setTerminal(true);
        return $view;
    }

    public function supplementInfoPageAction()
    {

        $suppService = null;
        $licenseService = null;
        $Buttons = null;

        $rights = [
            'supplementEdit'    => $this->getRigthsStatus('supplementEdit'),
            'expertButton'      => $this->getRigthsStatus('expertButton'),
            'printButton'       => $this->getRigthsStatus('printButton'),
            'extraditionButton' => $this->getRigthsStatus('extraditionButton'),
        ];

        if ($this->params()->fromPost()) {
            $post = $this->params()->fromPost();
            $id = $post['id'];
            $suppService = new \Eo\Model\Licensing\Service\LicenseSupplementInfoService($id);
            $licenseService = new \Eo\Model\Licensing\Service\LicenseInfoService(
              $suppService->getLicense()->getField('Id'), $suppService->getLicense()
            );
            $organizationId = $licenseService->getLicense()->getField('fk_eiisEducationalOrganization');

            $orgServ = new \Eo\Model\Licensing\Service\OrganizationService($organizationId);

            $Buttons = array();//$suppService->getSupplement()->getButtons();

            $handDocument = [
                'url'  => '',
                'name' => ''
            ];

            if ($suppService->getDocumentById() ) {
                $thandDocument = new \Model\LodEntities\document(
                  $suppService->getDocumentById()->getField('id')
                );
                $eoFile = $thandDocument->getFileScanCopy();
                if ($eoFile) {
                    $handDocument = [
                        'url'  => $eoFile->getUrl(),
                        'name' => $eoFile->getFileName()
                    ];
                }
            }

            $endDocument = [
                'url'  => '',
                'name' => ''
            ];

            if ($suppService->getEndDocumentById()) {
                $tendDocument = new \Model\LodEntities\document(
                  $suppService->getEndDocumentById()->getField('id')
                );
                $eoFile = $tendDocument->getFileScanCopy();
                if ($eoFile) {
                    $endDocument = [
                        'url'  => $eoFile->getUrl(),
                        'name' => $eoFile->getFileName()
                    ];
                }
            }
        }

        return $this->getViewModel([
              'Buttons'               => $Buttons,
              "supplement"            => $suppService->getSupplement(),
              "license"               => $suppService->getLicense(),
              'eduOrganization'       => $suppService->getEducationOrganization(),
              'licensesSignStates'    => $licenseService->getSignStates(),
              'govermentOrganization' => $suppService->getGovermentOrg(),
              'supplementStatus'      => $suppService->getAllStates(),
              'personArr'             => $suppService->getSignatories(),
              'basisForLicensing'     => $suppService->getBasisForLicensing(),
              'supplementTitle'       => $suppService->getTitle(),
              'adminDocument'         => $suppService->getAdministrativeDocument(),
              'endDocument'           => $suppService->getEndDocument(),
              'otherDocument'         => $suppService->getOtherDocuments(),
              'deal'                  => $suppService->getDeal(),
              'dealFromEndDoc'        => $suppService->getDealFromEndDoc(),
              'rights'                => $rights,
              'urlHandDoc'           => $handDocument,
              'urlEndDoc'            => $endDocument,
        ]);
    }

    /**
     * url  /eo/license/educational-programs-tab
     * @return type
     */
    public function educationalProgramsTabAction()
    {
        $eduProgService = null;

        $rights = [
            'supplementEdit' => $this->getRigthsStatus('supplementEdit'),
        ];

        if ($this->getRequest()->isPost()) {
            $post = $this->params()->fromPost();
            $supplementId = $post['id'];
            $supp = new Model\Entities\eiisLicenseSupplements($supplementId);
            $eduProgService = new \Eo\Model\Licensing\Service\EducationProgramService();
            $eduProgService->setSupplementId($supplementId);
        }
        return $this->getViewModel([
              'eduProgramsUsed'  => $eduProgService->getUsedLevelsAndData(),
              'parentLevel'      => $eduProgService->getLevelArrayIdName(),
              'parentLevelCount' => $eduProgService->programFromLevelsCount(),
              'rights'           => $rights,
              'status'           => $supp->getFieldOrSpace('lod_signStateCode')
        ]);
    }

    /**
     * url:  /eo/license/get-education-location-list
     *
     * @return type
     */
    public function getEducationLocationListAction()
    {
        $location = null;
        $rights = [
            'DelLicProgAdres' => $this->getRigthsStatus('DelLicProgAdres'),
            'supplementEdit'  => $this->getRigthsStatus('supplementEdit'),
        ];

        if ($this->params()->fromPost()) {
            $post = $this->params()->fromPost();
            $id = $post['supplementId'];
            $suppService = new \Eo\Model\Licensing\Service\LicenseSupplementInfoService($id);
            $licenseService = new \Eo\Model\Licensing\Service\LicenseInfoService(
              $suppService->getLicense()->getField('Id'), $suppService->getLicense()
            );
            $organizationId = $licenseService->getLicense()->getField('fk_eiisEducationalOrganization');

            $orgServ = new \Eo\Model\Licensing\Service\OrganizationService($organizationId);
            $location = $orgServ->getLocationFromOrganizationAndSupplement($id);
        }
        return $this->getViewModel([
              'status'   => $suppService->getSupplement()->getFieldOrSpace('lod_signStateCode'),
              'location' => $location,
              'rights'   => $rights
        ]);
    }


    /**
     * проверяем подпись
     *
     * url:  /eo/license/request-chek-sign/ид-файла для проверки
     *
     * @return \Zend\View\Model\JsonModel
     * @throws \Exception
     */

    public function requestChekSignAction() {
        $requestId = $this->params("id");
        $result = array();
        $result['status'] = 'Error';
        $isExFunction               = (boolean)$this->getRequest()->getQuery('isExFunction', false);
        try{
            //$requestClass = new Model\LodEntities\LicenseRequest();
            //$requestClass = $requestClass->getByWhere(array(
            //    $requestClass->getIdName() => $requestId,
            //));
            //if( empty($requestClass) ){
            //    throw new \Exception('не найдено заявление');
            //}
            //$isSign = $requestClass->checkSign();
            $requestClass = new Model\LodEntities\EiFile;
            $requestClass = $requestClass->getByWhere(array(
                $requestClass->getIdName() => $requestId,
            ));
            if( empty($requestClass) ){
                throw new \Exception('не найдено заявление');
            }
            $isSign = $requestClass->checkSign( $isExFunction );
            if( 'Ok' == $isSign){
                $result['status'] = 'Ok';
                $signData = $requestClass->checkSignLastResult();
                $result['signData'] = $signData['certificate_info'];
                if( empty($signData['signature_info']) ){
                    $signData['signature_info'] = array();
                }
                $result['signInfo'] = $signData['signature_info'];                
                if( !empty($result['signData']['NotBefore'])){
                    $result['signData']['dateBegin'] = date('d.m.Y',  strtotime(substr($result['signData']['NotBefore'],0,10)));
                }
                if( !empty($result['signData']['NotAfter'])){
                    $result['signData']['dateEnd']   = date('d.m.Y',strtotime(substr($result['signData']['NotAfter'] ,0,10)));
                }
            }
            else{
                throw new \Exception($isSign);
            }
        }
        catch (\Exception $e) {
            $result['status'] = 'Error';
            $result['message'] = $e->getMessage();
            //$result['console'] = str_replace(PHP_EOL, '\n', $e->getTraceAsString());
        }
        return new \Zend\View\Model\JsonModel($result);
    }


    /**
     * отдает отсортированый перечень приложений
     *
     * url:  /eo/license/get-license-supplement-sort
     *
     * @return ViewModel'
     */
    public function getLicenseSupplementSortAction()
    {
        $id_lic = $this->params()->fromPost("id");
        $Sort = $this->params()->fromPost("sort");
        if( $Sort=='Number DESC' ){
            $Sort = new \Zend\Db\Sql\Expression("INET_ATON(SUBSTRING_INDEX(CONCAT(f11_mon.eiis_LicenseSupplements.Number,'.0.0.0'),'.',4))  DESC");
        }
        if( $Sort=='Number ASC' ){
            $Sort = new \Zend\Db\Sql\Expression("INET_ATON(SUBSTRING_INDEX(CONCAT(f11_mon.eiis_LicenseSupplements.Number,'.0.0.0'),'.',4))  ASC");
        }
        $supplementTable = new \Model\Gateway\EntitiesTable("eiis_LicenseSupplements");
        $supplements = $supplementTable->getAllEntities(null, $Sort, array("fk_eiisLicense" => $id_lic));
        $view = new ViewModel();
        $view->setVariables(array(
            "supplements" => \Eo\Model\Licensing\Service\LicenseSupplementInfoService::supplementsObjectArrInfoRowPrepare(
              $supplements
            ),
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * модуль  уведоблений
     *
     * url: /eo/license/notify
     *
     * @return ViewModel
     */
    public function notifyAction(){

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 3;
        $where = array();
        $pagePaginator = 1;

        $allPage = 1;


        $currentPage = $pagePaginator;
        $pagesCount = $allPage;
        $requestsAll = array();

        //Тип документа
        $documentTypeCatalogAll = new \Model\LodEntities\DocumentTypeCatalogItem();
        $documentTypeCatalogAll = $documentTypeCatalogAll->getAllByWhereParseId(
                array('parent' => '20a4674be28c4b36ad69de14a0e4c8df',)
        );

        uasort(
            $documentTypeCatalogAll,
            function($a, $b){
                $aF = $a->getFieldSafe('title');
                $bF = $b->getFieldSafe('title');
                $aB = mb_strtolower($aF);
                $bB = mb_strtolower($bF);
                return strcmp($aB, $bB);
            }
        );

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('documentTypeCatalogAll', $documentTypeCatalogAll);
        return $view;
    }

    /**
     * получаем записи о отправленых заявлениях
     *
     * url: /eo/license/notify-page
     *
     * @return ViewModel
     */
    public function notifyPageAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 50;
        $where = array();
        $where = new \Zend\Db\Sql\Where();

        $pagePaginator = $this->getRequest()->getPost('page',1);
        $width = $this->getRequest()->getPost('width',1366);
        if($width <= 1366) $max_count_page = 10;

        $dealAll = new \Model\LodEntities\Deal();

        $dealAll = $dealAll->getAllByWhereParseId(array(
            'organizationId' => $educationOrganization->getId(),
        ));
        $docTypeAll = new \Model\LodEntities\DocumentTypeCatalogItem();
        $docTypeAll = $docTypeAll->getAllByWhereParseId(array(
            'parent' => '20a4674be28c4b36ad69de14a0e4c8df',
        ));
        $docTypeIdAll = array_keys($docTypeAll);
        $where->in('Document.documentTypeId',$docTypeIdAll);
        $where->isNotNull("Document.sendDate");
        $dealIdArray = array_keys($dealAll);
        $where->in('Document.dealId', $dealIdArray);

        //фильтры
        $filter = $this->getRequest()->getPost('filter','');
        //по процедуре
        if( !empty($filter["documentTypeId"]) ){
            $where->like('documentTypeId',$filter["documentTypeId"]);
        }
        if( !empty($filter["registrationNumber"]) ){
            $where->like('registrationNumber','%'.$filter["registrationNumber"].'%');
        }

        //порядочность сортировки
        $sortField = "Document.sendDate DESC";
        $sortPost = $this->getRequest()->getPost('sort','');
        if( !empty($sortPost) ){
            $sortField = $sortPost;
        }

        $docAll = new Model\LodEntities\document();
        $docAll = $docAll->selectComplicated(
                'Document',
                array(),
                $where,
                'id',//$group
                $sortField,
                $max_count_page,
                $pagePaginator-1,
                array(
                    'Deal',
                    'DocumentTypeCatalogItem',
                    'CatalogItem',
                    'catalog_deal',

                    'LicenseRequest',
                    'LicensingProcedure',
                    'LicenseReasonToLicenseRequest',
                    'LicenseReasonCatalogItem',
                    //'EiFile',
                ),
                array(
                    'Document.dealId = Deal.id',
                    'Document.documentTypeId = DocumentTypeCatalogItem.id',
                    'Document.documentReasonId = CatalogItem.id',
                    'Deal.stateCode = catalog_deal.origin_name',

                    'LicenseRequest.dealId = Deal.id',
                    'LicenseRequest.licensingProcedureId = LicensingProcedure.id',
                    'LicenseReasonToLicenseRequest.licenseRequestId = LicenseRequest.id',
                    'LicenseReasonCatalogItem.id = LicenseReasonToLicenseRequest.licenseReasonId',
                    //'Document.id = EiFile.ownerId and ownerClassCode = "Document" ',
                ) ,
                array(
                    array('Deal.number'=>'number'),
                    array('DocumentTypeCatalogItem.title'=>'title'),
                    array('CatalogItem.title'=>'title'),
                    array('catalog_deal.russian_name'=>'russian_name'),

                    array(),
                    array('LicensingProcedure.title'=>'title'),
                    array('LicenseReasonToLicenseRequest.licenseReasonId'=>'licenseReasonId'),
                    array('LicenseReasonCatalogItem.title'=> new \Zend\Db\Sql\Expression('GROUP_CONCAT(LicenseReasonCatalogItem.title order by LicenseReasonCatalogItem.title SEPARATOR ", ")') )
                ),//$left_column
                array('RIGHT','LEFT','LEFT','LEFT',   'LEFT','LEFT','INNER','LEFT')//$joinType

        );
        $requestsAll = array();
        foreach ( $docAll as $requestsKey => $reqOne ){
            $sto =  $reqOne->getArrayCopy();
            $requestsOne = new Model\LodEntities\document();
            $requestsOne->setFieldsSafe($sto);
            $requestsAll[$requestsOne->getId()] = $requestsOne;
        }
        $ApplClass = new \Model\Gateway\LodEntitiesTable("document");
        /*
        $requestsAll = $ApplClass->getAllEntities(array(
                "page"   => $pagePaginator,
                "onPage" => $max_count_page
            ),
            $sortField,
            $where)->getObjectsArray();
        */
        $requestsCount = $ApplClass->getCount($where);

        $allPage = ceil($requestsCount / $max_count_page);

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('requestsAll', $requestsAll);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('requestsCount', $requestsCount);
        return $view;
    }

    /*
     * лицензирование - новые
     * url:  /eo/license/all-deal
     * view: /module/Eo/view/eo/license/all-deal.phtml
     */    
    public function allDealAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $max_count_page = 3;
        $where = array();
        $pagePaginator = 1;

        $allPage = 1;

        //для фильтров - процедуры
        $licenseProcedureAll = new Model\LodEntities\LicensingProcedure();
        $licenseProcedureAll = $licenseProcedureAll->getAllByWhereParseId(array(
            $licenseProcedureAll->getIdName() => $licenseProcedureAll->getLicensingProcedureForOpenId()
        ));
        uasort(
            $licenseProcedureAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        //для фильтров - причины обращения
        $licenseReasonAll = new Model\LodEntities\LicenseReasonCatalogItem();
        $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId( array(
                $licenseReasonAll->getIdName() => $licenseReasonAll->getLicenseReasonForOpenId()
        ));
        uasort(
            $licenseReasonAll,
            function($a, $b) {
                $a = $a->getFieldHtml('title');
                $b = $b->getFieldHtml('title');
                return strcmp($a, $b);
            }
        );

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;
        $requestsAll = array();
        
        //Фильт статтус дела 
        $dealstatusAll = new Model\LodEntities\catalogdeal();
        $dealstatusAll = $dealstatusAll->getAllByWhereParseId();
        uasort(
            $dealstatusAll,
            function($a, $b) {
                $a = $a->getFieldHtml('russian_name');
                $b = $b->getFieldHtml('russian_name');
                return strcmp($a, $b);
            }
        );

        
        $currentPage = $pagePaginator;
        $pagesCount = $allPage;
        $requestsAll = array();
        
        

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        $view->setVariable('licenseProcedureAll', $licenseProcedureAll);
        $view->setVariable('licenseReasonAll', $licenseReasonAll);
        $view->setVariable('requestsAll', $requestsAll);
        $view->setVariable('dealstatusAll',  $dealstatusAll);
        return $view;    }
    
    /**
     * получаем записи о всех делах
     *
     * url:  /eo/license/all-deal-page
     * view: /module/Eo/view/eo/license/all-deal-page.phtml
     *
     * @return ViewModel
     */
    function allDealPageAction(){
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        

        $max_count_page = 50;
        $where = array();
        $where = new \Zend\Db\Sql\Where();

        $pagePaginator = $this->getRequest()->getPost('page',1);
        $width = $this->getRequest()->getPost('width',1366);
        if($width <= 1366) $max_count_page = 10;


        //$where[] = new \Zend\Db\Sql\Predicate\IsNull("dealId");
        //$where->isNull("dealId");
        //$where[] = new \Zend\Db\Sql\Predicate\IsNull('DateSubmission');
        ///$where->isNull("DateSubmission");
        $select = new \Zend\Db\Sql\Select( 'Deal' );
        $where->like('Deal.organizationId', $educationOrganization->getId());
        //добавляем фильтры
        $filter = $this->getRequest()->getPost('filter','');
        
        //по процедуре
        if( !empty($filter["licenseProcedure"]) ){
            $select->join(
                'LicenseRequest',
                'LicenseRequest.id = Deal.mainLicenseRequestId',
                array()                
            );
            $where->like( 'LicenseRequest.licensingProcedureId' ,  $filter["licenseProcedure"]  );
        }
        //по причине
        if( !empty($filter["licenseReason"]) ){
            $select->join(
                'LicenseReasonToLicenseRequest',
                'LicenseReasonToLicenseRequest.licenseRequestId = Deal.mainLicenseRequestId',
                array()                
            );
            $where->like( 'LicenseReasonToLicenseRequest.licenseReasonId' ,  $filter["licenseReason"]  );
            
            
            /*
            $licenseReasonAll = new Model\LodEntities\LicenseReasonToLicenseRequest();
            $licenseReasonAll = $licenseReasonAll->getAllByWhereParseId(
                    array(
                        new \Zend\Db\Sql\Predicate\Like('licenseReasonId',$filter["licenseReason"]),
                    ),
                    'licenseRequestId'
            );
            $licenseReasonAll =  array_keys($licenseReasonAll);
            $where->in('id',$licenseReasonAll);
            */
            //$selectIn = new \Zend\Db\Sql\Select();
            //$selectIn->from('LicenseReasonToLicenseRequest');
            //$selectIn->where->like('licenseReasonId',$filter["licenseReason"]);
            //$selectIn->columns(array('licenseRequestId'));
            //$where->in('id',$selectIn);
        }
        //под дате заполнени я с
        if( !empty($filter["fillingDateBegin"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateBegin"]));
            if( !empty($date)){
                $where->greaterThanOrEqualTo('Deal.registrationDate',$date);
            }
        }
        //под дате заполнени я по
        if( !empty($filter["fillingDateEnd"]) ){
            $date = date('Y-m-d',  strtotime($filter["fillingDateEnd"]));
            if( !empty($date)){
                $where->lessThanOrEqualTo('Deal.registrationDate',$date);
            }
        }
        if( !empty($filter["fillingDealStatus"]) ){
            $where->like('Deal.stateCode',$filter["fillingDealStatus"]);
            //$where[] = new \Zend\Db\Sql\Predicate\Like('licensingProcedureId',$filter["licenseProcedure"]);
        }
        if( !empty($filter["fillingDealNumber"]) ){
            $where->like( 'Deal.number' , '%' . $filter["fillingDealNumber"] . '%' );
            //$where[] = new \Zend\Db\Sql\Predicate\Like('licensingProcedureId',$filter["licenseProcedure"]);
        }
        $select->where( $where );


        new \Model\LodEntities\Deal();
        $deallClass = new \Model\Gateway\LodEntitiesTable("Deal");
        $requestsAll = $deallClass->getAllEntities(
            array(
                "page"   => $pagePaginator,
                "onPage" => $max_count_page
            ),
            "registrationDate DESC",
            $select
        )->getObjectsArray();
        $requestsCount = $deallClass->getCount( $select );

        $allPage = ceil($requestsCount / $max_count_page);

        $currentPage = $pagePaginator;
        $pagesCount = $allPage;

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('requestsAll', $requestsAll);
        $view->setVariable('requestsCount', $requestsCount);
        $view->setVariable('currentPage', $currentPage);
        $view->setVariable('pagesCount', $pagesCount);
        return $view;
    }
    //последняя строка
}
