<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Eo\Model\RequestFrames\RequestFrameController;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Predicate\In;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

class RequestsController extends AbstractActionController
{
    private $currentUserRole;

    public function getCurrentUserRole(){
        if( empty($this->currentUserRole)){
            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            $this->currentUserRole = new Model\LodEntities\IslodUserRole( $user->getField('fk_islodUserRole'));
        }
        return $this->currentUserRole;
    }
    
    /**
     * Возвращает право роли
     * @param type $rightsName
     * @return string
     */
    public function getRigthsStatus($rightsName){
        $role = $this->getCurrentUserRole();
        return ($role->getFieldOrSpace($rightsName) == '1');
    }

    /**
     * Проверяет доступ, и редеректит если надо
     */
    public function accessControl( $rightsName){
        if( !$this->getRigthsStatus($rightsName)){
            $this->redirect()->toRoute('eo/default',[
                'controller' => 'index',
                'action' => 'access-denied'
            ]);
        }
    }

    
    /**
     * url:  /eo/requests/add
     * view: /module/Eo/view/eo/requests/add.phtml
     * 
     * @return ViewModel
     */
    public function addAction()
    {
        //$this->accessControl('createRequest');
        //Данные используемые для первой вкладки
        $propertiesData = [];
        $viewData = [
            "propertiesData" => null,
            "isEdit" => false
        ];

        //Если передан айдишник, запрос открыт на редактирование
        if($this->params()->fromPost("requestId") != null){
            $request = new Model\LodEntities\LicenseRequest($this->params()->fromPost("requestId"));
            $viewData["isEdit"] = true;
            $viewData["request"] = $request;

            //Предписания
            $org = $request->getEducationalOrganization();
            $viewData["prescriptions"] = count($org->getPrescriptions());
        }

        if($this->params()->fromPost("organizationId") != null){
            $org = new Model\Entities\eiisEducationalOrganizations($this->params()->fromPost("organizationId"));
            $viewData["startOrg"] = $org;
            $viewData["prescriptions"] = count($org->getPrescriptions());
        }
        $viewData["isFirstRequest"] = "false";
        if($this->params()->fromPost("dealId") != null){
            $dealId = $this->params()->fromPost("dealId");
            $viewData["dealId"] = $dealId;
            //если передан айдишник дела надо собрать заявления из этого дела
            //хотя бы одно и вытянуть оттуда тип процедуры
            $deal = new Model\LodEntities\Deal($dealId);
            $dealRequest = $deal->getMainLicenseRequest();

            if($dealRequest != null){
                $viewData["isFirstRequest"] = "true";
                if(!$dealRequest->isEmptyField("licensingProcedureId")) {
                    $viewData["dealLicensingProcedureId"] = $dealRequest->getField("licensingProcedureId");
                    $lp = new Model\LodEntities\LicensingProcedure($viewData["dealLicensingProcedureId"]);
                    $viewData["licensingProcedureName"] = $lp->getField("title");
                }
                $propertiesData["requestRepresentativeLastName"] = $dealRequest->getField("requestRepresentativeLastName");
                $propertiesData["requestRepresentativeFirstName"] = $dealRequest->getField("requestRepresentativeFirstName");
                $propertiesData["requestRepresentativeMiddleName"] = $dealRequest->getField("requestRepresentativeMiddleName");
                $propertiesData["mobilePhoneNumber"] = $dealRequest->getField("mobilePhoneNumber");
                $propertiesData["eMail"] = $dealRequest->getField("eMail");
            }
        }

        //Тип поступления
        $aTypeTab = new \Model\Gateway\LodEntitiesTable("CatalogItem");
        $aTypes = $aTypeTab->getAllEntities(null,"title ASC",[
            "catalog" => "admissionTypes"
        ])->getObjectsArray();
        $propertiesData["admissionTypes"] = $aTypes;

        //Тип процедуры
        $lProcTab = new \Model\Gateway\LodEntitiesTable("LicensingProcedure");
        $lProcedures = $lProcTab->getAllEntities(null,"code ASC")->getObjectsArray();
        $propertiesData["LicensingProcedures"] = $lProcedures;
        $propertiesData["firstProcedure"] = $lProcedures[0];

        //Причины обращения
        $licenseReasonAll = new Model\LodEntities\LicenseReasonCatalogItem();
        $reasonsTab = new \Model\Gateway\LodEntitiesTable("LicenseReasonCatalogItem");
        $reasons = $reasonsTab->getAllEntities(
            null, 
            "licensingProcedureId ASC, title ASC",
            array( $licenseReasonAll->getIdName() => $licenseReasonAll->getLicenseReasonForOpenId() ) // отбираем причины только из нужных причин, чтоб на боевой откидывались не надо
        )->getObjectsArray();
        $propertiesData["reasons"] = $reasons;
        $reasonsByProcedure = [];
        foreach($reasons as $reason){
            if(!isset($reasonsByProcedure[$reason->getField("licensingProcedureId")]))
                $reasonsByProcedure[$reason->getField("licensingProcedureId")] = [];

            $reasonsByProcedure[$reason->getField("licensingProcedureId")][] = $reason;
        }
        $propertiesData["reasonsByProcedure"] = $reasonsByProcedure;


        $viewData["propertiesData"] = $propertiesData;
        //вью
        $view = new ViewModel();
        $view->setVariables($viewData);
        $view->setTerminal(true);
        return $view;
    }

    /**
     * дергаем организации
     * /islod/requests/get-organizations-select/'+page;
     *
     * @return ViewModel
     */
    public function getOrganizationsSelectAction(){
        $val = $this->params('id2');

        $page = $this->params('id');
        if (!empty($val)) {
            $search = $val;
        } else {
            $search = ' ';
        }

        $user                                  = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization                 = $user->getEducationOrganization();
        $this->layout()->educationOrganization = $educationOrganization;
        $this->layout()->identityUser          = $user;
        $organizationClass                     = new Model\Entities\eiisEducationalOrganizations();
        $sql                                   = $organizationClass->confertSearchToSql($search);
        if(!empty($sql)){
            $sql .='  AND ';
        }
        $sql .=' (`fk_eiisParentEducationalOrganization` is Null OR `fk_eiisParentEducationalOrganization`="NULL" OR `fk_eiisParentEducationalOrganization`="")';

        /*
        $stateValidId=$organizationClass->selectComplicated('eiis_LicenseStates',array('Id'),array('IsValid'=>1))->toArray();
        $stateValidId=  array_column($stateValidId, 'Id');
        if(count($stateValidId)){
            $stateValidString='"'.implode('", "', $stateValidId).'"';
            $sql .=' AND `eiis_Licenses`.`fk_eiisLicenseState` IN('. $stateValidString .')';
        }*/
        $sql .= " AND (`eiis_EducationalOrganizations`.`lod_stateCode` = 'VALID' OR `eiis_EducationalOrganizations`.`lod_stateCode` = 'DRAFT')";

        //$sql .= ' AND  eiis_Licenses.' ;
        //$countAll                              = $organizationClass->selectComplicated("eiis_EducationalOrganizations",array('Id'), '(`FullName` LIKE "%' . $search . '%" OR `ShortName` LIKE "%' . $search . '%" OR `RegularName` LIKE "%' . $search . '%" OR `Inn` ="' . $search . '")  AND (`fk_eiisParentEducationalOrganization` is Null OR `fk_eiisParentEducationalOrganization`="NULL")', NULL, NULL, NULL,NULL,"isga_EducationalOrganizationData",  "eiis_EducationalOrganizations.Id = isga_EducationalOrganizationData.fk_eiisEducationalOrganization ","fk_eiisEducationalOrganization");
        //$orgArray                              = $organizationClass->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName'), '(`FullName` LIKE "%' . $search . '%" OR `ShortName` LIKE "%' . $search . '%" OR `RegularName` LIKE "%' . $search . '%" OR `Inn` ="' . $search . '")  AND (`fk_eiisParentEducationalOrganization` is Null OR `fk_eiisParentEducationalOrganization`="NULL")', NULL, NULL, 20, $page,"isga_EducationalOrganizationData",  "eiis_EducationalOrganizations.Id = isga_EducationalOrganizationData.fk_eiisEducationalOrganization ","fk_eiisEducationalOrganization")->toArray();
        $countAll                              = $organizationClass->selectComplicated(
            "eiis_EducationalOrganizations",
            array('Id'),
            $sql
            , 'Id'
            , NULL
            , NULL
            , NULL
        );
        $orgArray                              = $organizationClass->selectComplicated(
            "eiis_EducationalOrganizations",
            array('Id', 'FullName', 'LawAddress'),
            $sql,
            'Id',
            NULL,
            20,
            $page
        )->toArray();
        $view                                  = new ViewModel();
        $maxPage=ceil(count($countAll)/20);
        $view->setVariables(array(
            "orgs" => $orgArray,
            'search' => $search,
            'nextPage' => $page+1,
            'maxPage' => $maxPage
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function orgsSelectAction(){
        //строка поиска
        $searchStr = null;
        if($this->params('id2') != null)
            $searchStr = $this->params('id2');

        //Страница
        $page = 1;
        if($this->params('id') != null)
            $page = $this->params('id');

        //таблица с организациями
        $orgsTable = new Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");

        //Подготоавливаем условия поиска
        $where = [];

    }

    /**
     * получить перечень филиалов
     * url:  /eo/requests/get-filials-select
     *
     * @return ViewModel
     */
    public function getFilialsSelectAction(){
        if($this->params()->fromPost("orgId")== null || $this->params()->fromPost("requestId")== null){
            echo "Error";
            die;
        }

        $orgId = $this->params()->fromPost("orgId");
        $org = new Model\Entities\eiisEducationalOrganizations($orgId);

        $requestId = $this->params()->fromPost("requestId");
        $request = new Model\LodEntities\LicenseRequest($requestId);

        $type = $this->params()->fromPost("type");

        //Результ массив, в который добавляем организацию
        $filialsList = array();
        //$filialsList[] = $org;

        //Массив организаций с которых нужно забирать филиалы
        //Так как если есит реорганизация то нужно и из других орг забирать
        $orgs = [];
        $orgs[] = $org;
        //Смотрим что там по реорганизациям

        //Выбираем все организации что связаны с этим делом
        $reorgTable = new Model\Gateway\LodEntitiesTable("ReorganizedOrganizationInfo");
        $suppReorgs = $reorgTable->getAllEntities(null,null, array(
            "licenseRequestId" => $request->getId()
        ));

        $orgIds = [];
        foreach($suppReorgs as $suppReorg){
            $orgIds[] = $suppReorg->getField("organizationId");
        }

        $reorgs = [];
        if(count($orgIds) > 0) {
            $EOTable = new Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");
            $reorgs = $EOTable->getAllEntities(null, null, [
                new \Zend\Db\Sql\Predicate\In("Id", $orgIds)
            ])->getObjectsArray();
        }

        $orgs = array_merge($orgs,$reorgs);

        //Проходим по всем организациям
        $filials = [];
        foreach($orgs as $itemOrg) {
            //Если это не основной филиал, а реорганизация, тоже добавляем в список филиалов
            if ($itemOrg->getId() != $orgId)
                $filials[] = $itemOrg;
        }

        $isNewFilial = $request->hasReasonOpenFilial();
        foreach($orgs as $itemOrg){
            //все филиалы организации
            $orgFilials = $itemOrg->getValidAffilates( $isNewFilial )->getObjectsArray();
            $filials = array_merge($filials,$orgFilials);
        }


        //филиалы что уже есть в заявклении
        $reqFilials = $request->getFilials();
        $reqFilialsList = [
            $org->getId() => true
        ];
        foreach($reqFilials as $reqFilial){
            $reqFilialsList[$reqFilial->getField("orgId")] = true;
        }

        //вью
        $view = new ViewModel();
        $view->setVariables([
            "filials" => $filials,
            "reqFilialsList" => $reqFilialsList,
            "type" => $type
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function getAddressSelectAction(){
        if($this->params()->fromPost("requestId")== null){
            echo "Error";
            die;
        }

        $data = $this->params()->fromPost();

        $requestId = $this->params()->fromPost("requestId");
        $request = new Model\LodEntities\LicenseRequest($requestId);

        //Смотрим какие причины у этого заявления
        //потому что если это реорганизация
        //в формах преобразования, присоединения и слияния, то там другая логика
        $reasonIds = $request->getLicenseReasonsIds();
/*
        if(in_array("518dfabe3375431684b71d2faa5c7564", $reasonIds)
            || in_array("c83201802b474c4393305b16b7c05375", $reasonIds)
            || in_array("f9893ac3e72e43b59101a8270f9eaacf", $reasonIds)){*/
            //берем организацию которая передана, забираем от нее инфо о городе
            $org = new Model\Entities\eiisEducationalOrganizations($data["orgId"]);

            //город филиала что сейчас выбран
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $subselectLocations = $sql->select();
            $locs = $subselectLocations->from("OrganizationLocation")
                ->columns(array("settlementId"))
                ->where(array(
                    "organizationId" => $org->getId(),
                    "addressType" => "OrganizationLocation"
                ));
            //айдишники все организаций и филиалов
            //организации
            $orgsId = [$data["orgId"]];

            $reorgTable = new \Model\Gateway\LodEntitiesTable("ReorganizedOrganizationInfo");
            $reorgs = $reorgTable->getEntitiesByWhere([
                "licenseRequestId" => $requestId
            ])->getObjectsArray();

            foreach($reorgs as $reorg){
                $orgsId[] = $reorg->getField("organizationId");
            }

            //+нам нужны все филиалы
            $eoTable = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");

            $filials = $eoTable->getEntitiesByWhere([
                new \Zend\Db\Sql\Predicate\In("fk_eiisParentEducationalOrganization", $orgsId),
            ])->getObjectsArray();

            foreach($filials as $filial){
                $orgsId[] = $filial->getId();
            }

            //адреса достаем
            /*
             смотри, ты отбираешь филиал по settlementId. Он присутствует только для типа OrganizationLocation. Правильно? теперь тебе надо вывести Места осуществления ОД для этого филиала - это адреса PlaceOfEducationalActivities. И уже этот тип не надо сверять по settlementId
             */
            $sqlorgs = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $orgsIdList = $sqlorgs->select();
            $orgsIdFromLocs =  $orgsIdList->from("OrganizationLocation")
                ->columns(array("organizationId"))
                ->where(array(
                    new \Zend\Db\Sql\Predicate\In("settlementId", $locs),
                    new \Zend\Db\Sql\Predicate\In("organizationId", $orgsId),
                ));


            $LocTable = new Model\Gateway\LodEntitiesTable("OrganizationLocation");
            $addressList = $LocTable->getAllEntities(null, null, [
                new \Zend\Db\Sql\Predicate\In("organizationId", $orgsIdFromLocs),
                "addressType" => "PlaceOfEducationalActivities"
            ])->getObjectsArray();

            //Проходим по списку адресов и отсеиваем те, что связаны с невалидными приложениями

            $workedAddrList = [];
            foreach($addressList as $addr){
                if($addr->isLinkedSupplementValid()){
                    $workedAddrList[] = $addr;
                }
            }
            $addressList = $workedAddrList;
        /*
        } else {
            /*было закомменчено
                    //Получаем список айди всех приложений
                        //Для этого получаем список всех филиалов организации и в приложениях ищем по полю fk_eiisBranch
                    $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
                    $subselectBranches = $sql->select();
                    $subselectBranches->from("eiis_EducationalOrganizations")
                        ->columns(array("Id"))
                        ->where(array(
                            "fk_eiisParentEducationalOrganization" => $request->getField("organizationId")
                        ));
            */
        /*
            $addressList = [];

            //Проверяем количество привязанных реорг
            $reorg = $request->getOneReorganizationData();
            if(!$reorg->isEmptyField("id")){
                $orgId = $reorg->getField("organizationId");
            } else {
                $orgInfo = new \Model\LodEntities\RequestOrganizationInfo($this->params()->fromPost("orgInfId"));
                $orgId = $orgInfo->getField("orgId");
            }



            if ($orgId != null && $orgId != "" && $orgId != "NULL") {
                $suppTable = new Model\Gateway\EntitiesTable("eiis_LicenseSupplements");


                $where = [];
                $where["lod_stateCode"] = "VALID";
                //$where[] = new \Zend\Db\Sql\Predicate\In("fk_eiisBranch",$subselectBranches);
                $where["fk_eiisBranch"] = $orgId;

                $supps = $suppTable->getAllEntities(null, null, $where)->getObjectsArray();
                $suppIds = [];
                foreach ($supps as $supp)
                    $suppIds[] = $supp->getId();

                $addressList = [];
                if (count($suppIds) > 0) {
                    // Идём в таблицу OrganizationLocationToLicenseAttach и нашему гуиду в поле licenseAttachId
                    // выбираем все записи
                    $sqlLoc = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
                    $suppLocation = $sqlLoc->select();
                    $suppLocation->from("OrganizationLocationToLicenseAttach")
                        ->columns(array("organizationLocationId"))
                        ->where(array(
                            new \Zend\Db\Sql\Predicate\In("licenseAttachId", $suppIds)
                        ));

                    $LocTable = new Model\Gateway\LodEntitiesTable("OrganizationLocation");
                    $addressList = $LocTable->getAllEntities(null, null, [
                        new \Zend\Db\Sql\Predicate\In("id", $suppLocation)
                    ])->getObjectsArray();
                }
            }
        }*/

        //на этом этапе у нас есть отобранный адресс лист, но не надо выводить адреса что уже были добавлены
        //Забираем все адреса уже добавленные
        $orgInfo = new \Model\LodEntities\RequestOrganizationInfo($this->params()->fromPost("orgInfId"));
        $currentAddresses = $orgInfo->getODPlaces($this->params()->fromPost("type"));

        $currentAddressesId = [];
        foreach($currentAddresses as $ca){
            $currentAddressesId[$ca->getField("organizationLocationId")] = true;
        }

        //Фильтруем все найденные адреса и не пропускаем уже добавленные ранее
        $returnAddressList = [];
        foreach($addressList as $al){
            if(!isset($currentAddressesId[$al->getId()]))
                $returnAddressList[] = $al;
        }

        //вью
        $view = new ViewModel();
        $view->setVariables([
            "addresses" => $returnAddressList
        ]);
        $view->setTerminal(true);
        return $view;
    }

    /**
     * сохраняет данные причин заявления
     *
     * url:  /eo/requests/add-request
     */
    public function addRequestAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $request = $this->getRequest();
            $fields = $this->getRequest()->getPost()->toArray();
            $reasons = $fields["reasons"];
            unset($fields["reasons"]);

            //Если это редактирование
            $isEdit = false;
            if(isset($fields["id"])){
                $isEdit = true;
                //объект запроса
                $newRequest = new Model\LodEntities\LicenseRequest($fields["id"]);
                //удаляем все текущие причины
                $newRequest->deleteReasons();
            } else {
                //Создаем новый объект заявления
                $newRequest = new Model\LodEntities\LicenseRequest();

                //Максимальный номер
                $fields["number"] = $newRequest->getMaxNumber();
            }

            //Сохраняем поля
            if($isEdit)
                $oldFields = $newRequest->getFields();

            if(!$isEdit || ($isEdit && $fields["organizationId"]!=$oldFields["organizationId"])) {
                $eiisOrg = new Model\Entities\eiisEducationalOrganizations($fields["organizationId"]);
                $fields["typeOfBusiness"] = $eiisOrg->getField("fk_eiisEducationalOrganizationProperties");
                $fields["ogrn"] = $eiisOrg->getField("GosRegNum");
                $fields["inn"] = $eiisOrg->getField("Inn");

                $license = $eiisOrg->getLicense();
                if($license != null){
                    $fields["licenseId"] = $license->getId();
                    $fields["licenseRegistrationNumber"] = $license->getField("LicenseRegNum");
                    $fields["licenseDate"] = $license->getField("DateLicDoc");
                    $fields["licenseSeries"] = $license->getField("SerDoc");
                    $fields["licenseBlankNumber"] = $license->getField("NumDoc");
                    $fields["issuingAuthority"] = $license->getField("lod_authorizedPersonId");
                }

                if(!$isEdit){
                    $dt = new \DateTime();
                    $fields["fillingDate"] = $dt->format("Y-m-d");
                    $fields["notificationRequired"] = 1;
                }

                //если сменилась организация, меняем ссылку на организацию и в деле
                if($isEdit) {
                    $deal = $newRequest->getDeal();
                    $deal->setField("organizationId",$fields["organizationId"]);
                    $deal->save();
                }
            }
            $newRequest->setFields($fields);
            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
            $newRequest->setField("registratorId",$user->getId());

            //Добавляем объект
            //$newId = $newRequest->save();
            $newId = $newRequest->getId();
            if (!$isEdit && $newId !== false && (!isset($fields["dealId"]) || $fields["dealId"]=="")) {
                $newDeal = new Model\LodEntities\Deal();
                $idDeal  = $newDeal->saveNewDeal($newId, $fields["organizationId"]);
                $newRequest->addDeal($idDeal);
            }

            //Добавляем связи с причинами
            $reasonsList = explode(",", $reasons);
            $newRequest->addReasons($reasonsList);

            //Если это редактирование, то проверяем не была ли изменена организация
            if($isEdit){
                if($fields["organizationId"]!=$oldFields["organizationId"]){
                    $newRequest->clearAllFramesData();
                }
            }

            //Тут идет кусок что проверяет вдруг у этого дело были заявления и хотяб одно из них было на реорганизацию
            /*
                Таким образом, если в деле есть заявление с причиной реорганизация в форме преобразования, то последующим заявлениям из этого же дела нужно подтягивать лицензию реорганизованной организации из первого заявления, чтоб потом из этой лицензии могли выбрать исключенные программы.


                как дополнение к предыдущему комментарию, т.е. для последующих заявлений в том же деле, где первым является реорганизация в форме преобразования, при создании заявления нужно в таблице LicenseRequest прописывать licenseId, licenseRegistrationNumber, licenseDate, licenseSeries, licenseBlankNumber лицензии реорганизованной организации, выбранной в первом заявлении.

             */
            //устанавливаем флаг в false
            $hasReorg = false;
            //связанное с делом заявление
            $dealMainRequest = null;
            //смотрим если есть дело
            $deal = $newRequest->getDeal();
            if($deal!=null){
                //забираем главное заявление
                $dealMainRequest = $deal->getMainLicenseRequest();
                //Проверяем на причину реорганизация в форме преобразования
                if(($dealMainRequest->hasReasonReorgTransform() || $dealMainRequest->hasReasonReorgJoin() || $dealMainRequest->hasReasonReorgMerge()) && $dealMainRequest->getId() != $newRequest->getId()){
                    $hasReorg = true;
                }
            }


            //Если это не первое заявление дела
            if(!$isEdit && $dealMainRequest != null && $dealMainRequest->getId() != $newRequest->getId()){
                //копируем часть данных из сущесвующего объекта заявления
                //Наименование, сокращенное наименование ОГРН, ИНН, КПП, организационно правовую форму
                //requestSignerLastName,  requestSignerFirstName, requestSignerMiddleName и его должность наследуем
                $requestCopyData = [
                    "requestSignerLastName" => $dealMainRequest->getField("requestSignerLastName"),
                    "requestSignerFirstName" => $dealMainRequest->getField("requestSignerFirstName"),
                    "requestSignerMiddleName" => $dealMainRequest->getField("requestSignerMiddleName"),
                    "requestSignerPosition" => $dealMainRequest->getField("requestSignerPosition")
                ];
                $newRequest->setFields($requestCopyData);
                $newRequest->save();
                // Копирование данных для орг инфо
                $orgInfo = $dealMainRequest->getMainOrganizationInfo();

                $orgFields = $orgInfo->getFields();
                unset($orgFields["id"]);
                $orgFields["requestId"] = $newRequest->getId();
                $newOrg = new Model\LodEntities\RequestOrganizationInfo();
                $newOrg->setFields($orgFields);
                $newOrg->save();
            } else

            if(!$isEdit || ($isEdit && $fields["organizationId"]!=$oldFields["organizationId"])){
                //Добавляем запись для головного вуза в RequestOrganizationInfo
                $eiisOrg = new Model\Entities\eiisEducationalOrganizations($fields["organizationId"]);

                $orgInfo = new Model\LodEntities\RequestOrganizationInfo();
                $orgInfo->setFields([
                    "requestId" => 	$newId,
                    "orgId" => $fields["organizationId"],
                    "fullTitle" => $eiisOrg->getField("FullName"),
                    "shortTitle" => $eiisOrg->getField("ShortName"),
                    "kpp" => $eiisOrg->getField("Kpp"),
                    "licenseePhone" => $eiisOrg->getField("Phones"),
                    "licenseeEmail" => $eiisOrg->getField("Mails"),
                    "additionalInfo" => "base",
                    "ogrn" => $eiisOrg->getField("GosRegNum"),
                    "inn" => $eiisOrg->getField("Inn"),
                    "fk_eiisEducationalOrganizationProperties" => $eiisOrg->getField("fk_eiisEducationalOrganizationProperties")
                ]);
                //Подтягиваем адрес
                //Для нашей организации в заявлении есть юр.адрес: в таблице OrganizationLocation этот адрес имеет тип addressType "OrganizationLocation". Так вот при создании заявления этот гуид должен записаться в RequestOrganizationInfo в поле addressId
                $orgLocTable = new Model\Gateway\LodEntitiesTable("OrganizationLocation");
                $orgLocs = $orgLocTable->getAllEntities(null, null, [
                    "organizationId" => $eiisOrg->getId(),
                    "addressType" => "OrganizationLocation"
                ])->getObjectsArray();
                if(count($orgLocs)>0){
                    $orgLoc = $orgLocs[0];
                    $orgInfo->setField("addressId", $orgLoc->getId());
                    $orgInfo->setField("address", $orgLoc->getField("address"));
                }

                $orgInfo->save();
            }

            //Если это новое заявление в деле, но не первое, надо скопировать все адреса
            if($dealMainRequest != null && $dealMainRequest->getId() != $newRequest->getId()){
                $orgInfo = $dealMainRequest->getMainOrganizationInfo();
                $newOrg = $newRequest->getMainOrganizationInfo();
                //Теперь подтягиваем все адреса из первого заявления для главной организации заявления
                $addrTable = new Model\Gateway\LodEntitiesTable("LicenseRequestAddress");
                //собираем адреса организации чтобы их скопировать
                $orgAddresses = $addrTable->getAllEntities(null, null, [
                    "requestOrganizationInfoId" => $orgInfo->getId()
                ]);
                //теперь все эти адреса копируем
                foreach($orgAddresses as $oa){
                    $fields = $oa->getFields();
                    unset($fields["id"]);
                    $fields["requestOrganizationInfoId"] = $newOrg->getId();
                    $lra = new Model\LodEntities\LicenseRequestAddress();
                    $lra->setFields($fields);
                    $lra->save();
                }
            }

            //Раз звезды сошлись по реорганизации, забираем все что нужно из предыдущего заявления
            //создаем запись по реорганизованной организации в новое заявление
            if($hasReorg){
                //Подтягиваем реорг организацию из главного заявления
                $reorgOrgInfo = $dealMainRequest->getOneReorganizationData();
                $reorgOrg = $reorgOrgInfo->getReorganizedOrganization();

                //добавляем инфо по реорг организации в это заявление
                $reorgFields = $reorgOrgInfo->getFields();
                unset($reorgFields["id"]);
                $reorgFields["licenseRequestId"] = $newRequest->getId();
                $newReorg = new Model\LodEntities\ReorganizedOrganizationInfo();
                $newReorg->setFields($reorgFields);
                //var_dump($reorgFields);die;
                $newReorg->save();

                //заполняем поля в заявлении по лицензии реорганизованной организации
                $license = $reorgOrg->getLicense();
                if($license != null){
                    $newRequest->setField("licenseId",$license->getId());
                    $newRequest->setField("licenseRegistrationNumber", $license->getField("LicenseRegNum"));
                    $newRequest->setField("licenseDate" ,$license->getField("DateLicDoc"));
                    $newRequest->setField("licenseSeries", $license->getField("SerDoc"));
                    $newRequest->setField("licenseBlankNumber", $license->getField("NumDoc"));
                    $newRequest->setField("issuingAuthority", $license->getField("lod_authorizedPersonId"));
                    $newRequest->save();
                }
            }

            //возвращаем
            $returnArray = array(
                "result" => "success",
                "id" => $newId
            );
        } catch(\Exception $e){
            $returnArray["msg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function editAction(){
        //$this->accessControl('createRequest');

        $requestId = $this->params("id");
        $isTest = false;
        if($requestId=="test"){
            $requestId = '57d75ee11eff4a4ba396d58116bb0073';
            $isTest = true;
        }
        //$this->params("id");
        if (empty($requestId)) {
            die;
        }

        $frame = $this->params()->fromPost("frame");

        $request = new Model\LodEntities\LicenseRequest($requestId);
        if($request->getId() == null){
            //die;
        }
        $sm =  $this->getServiceLocator();
        $requestDialog = new \Eo\Model\RequestFrames\RequestFramesController($request, $sm, $isTest, $frame);
        $dialog = $requestDialog->getDialog();
        return $dialog;
    }

    /**
     * Метод возвращает по филиалу уровни с колвом-программ, для левого блока с выбором филиалов и уровней
     */
    public function getFilialLevelsAction(){
        $id_org             = $this->params('id');
        $type = $this->params()->fromPost("type");

        //Подготавливаем набор уровней
        $table = new Model\Gateway\LodEntitiesTable("LicenseRequestEducationProgram");
        $where = array(
            "requestOrganizationInfoId" => $id_org,
        );
        switch($type){
            case "new-op":
                $where["typeOfActivity"] = "OPENING";
                break;
            case "current-op":
                $where["typeOfActivity"] = "ACTIVE";
                break;
            case "stop-op":
                $where["typeOfActivity"] = "CLOSING";
                break;
            case "change-op":
                $where["typeOfActivity"] = "EDITING";
                break;
        }

        $lreo = $table->getAllEntities(null, null, $where);

        $levels = [];
        foreach ($lreo as $ep) {
            if ($ep->getField("additionalInfo") == "edit_from")
                continue;

            if (!isset($levels[$ep->getField("educationLevelId")])) {
                $level = new Model\Entities\eiisEduLevels($ep->getField("educationLevelId"));

                if (!$level->isEmptyField("parentlevel")) {
                    $level = $level->getParentLevel();
                }

                if (!isset($levels[$level->getId()])) {
                    $levels[$level->getId()] = [
                        "title" => $level->getField("Name"),
                        "id" => $level->getId(),
                        "count" => 1
                    ];
                } else $levels[$level->getId()]["count"]++;

            } else
                $levels[$ep->getField("educationLevelId")]["count"]++;
        }

        $view            = new ViewModel();
        $view->setVariables(
            array(
                "eduLevels" => $levels
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    /**
     * Метод возвращает список оп по уровню и филиалу
     *
     * url:  /eo/requests/get-level-op
     */
    public function getLevelOpAction(){
        $data = $this->params()->fromPost();

        $where = [
            "requestOrganizationInfoId" => $data["filialId"]
        ];

        if(isset($data["levelId"])) {
            $level = new Model\Entities\eiisEduLevels($data["levelId"]);

            $sublevels = $level->getSublevels();
            if(count($sublevels) > 0){
                $subIds = [];
                foreach($sublevels as $slvl)
                    $subIds[] = $slvl->getId();

                $where[] = new In("educationLevelId",$subIds);
            } else
                $where["educationLevelId"] = $data["levelId"];
        }
        if(isset($data["type"])) {
            //$where[] = new \Zend\Db\Sql\Predicate\NotLike("additionalInfo", "edit_from");

            //В "Новые ОП" должны выводиться программы с typeOfActivity = OPENING.
            if(isset($data["type"])){
                switch($data["type"]){
                    case "new-op":
                        $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity","OPENING");
                        break;
                    case "current-op":
                        $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity","ACTIVE");
                        break;

                    case "stop-op":
                        $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity","CLOSING");
                        break;

                    case "change-op":
                        $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity","EDITING");
                        break;
                }
            }

            $epTable = new Model\Gateway\LodEntitiesTable("LicenseRequestEducationProgram");
            $ePrograms = $epTable->getAllEntities(null, null, $where)->getObjectsArray();
        }

        //В "Действующие ОП" - программы с typeOfActivity = ACTIVE из действующих приложений,
        //выбранных на фрейме заявления "Лицензия".
        /*
        if(isset($data["type"]) && $data["type"] == "current-op"){
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
            $subselectSupps = $sql->select();
            $subselectSupps->from("LicenseSupplementToLicenseRequest")
                ->columns(array("licenseAttachId"))
                ->where(array(
                    "licenseRequestId" => $data["requestId"]
                ));

            $epTable = new Model\Gateway\LodEntitiesTable("EducationProgram");

            $where = [];
            $where[] = new In("LicenseSupplementID",$subselectSupps);
            if(isset($data["levelId"]))
                $where["educationLevelIslodId"] = $data["levelId"];
            $where["organizationId"] = $data["filialId"];

            $pPrograms = $epTable->getAllEntities(null, null, $where)->getObjectsArray();

            $ePrograms = [];
            foreach($pPrograms as $pProgram){
                $ePrograms = $pProgram->getEduProgram();
            }
        }
        */

        //Разбиваем программы по уровням
        $eProgramsByLevel = [];
        foreach($ePrograms as $program){
            $level = $program->getEduLevel();

            if(!$level)
                continue;

            if(!isset($eProgramsByLevel[$level->getId()]))
                $eProgramsByLevel[$level->getId()] = [
                    "level" => $level,
                    "programs" => []
                ];

            $eProgramsByLevel[$level->getId()]["programs"][] = $program;
        }
/*
        $type="none";
        if(isset($data["type"]))
            $type=$data["type"];

        if($type=="change-op"){

            $retEP = array();
            foreach($ePrograms as $program){
                if(!isset($retEP[$program->getField("educationProgramCatalogItemId")]))
                    $retEP[$program->getField("educationProgramCatalogItemId")] = [
                        "edit_from" => null,
                        "edit_to"   => null
                    ];
                //Для старых оп

                if($program->isEmptyField("additionalInfo")){

                    $program->setField("additionalInfo","edit_from");
                    $program->setField("typeOfActivity","EDITING");
                    $program->save();
                    $retEP[$program->getField("educationProgramCatalogItemId")]["edit_from"] = $program;


                    $newFields =  $program->getFields();
                    $newFields["additionalInfo"] = "edit_to";

                    $eiisEduProgram = $program->getEduProgram();

                    if($eiisEduProgram != null) {
                        $newFields["changeName"] = $eiisEduProgram->getField("Name");
                        $newFields["changeCode"] = $eiisEduProgram->getField("Code");
                    }

                    unset($newFields["id"]);
                    $newProgram = new Model\LodEntities\LicenseRequestEducationProgram();
                    $newProgram->setFields($newFields);
                    $newProgram->save();
                    $retEP[$program->getField("educationProgramCatalogItemId")]["edit_to"] = $newProgram;
                }
                //Для новых филиалов
                else{
                    $retEP[$program->getField("educationProgramCatalogItemId")][$program->getField("additionalInfo")] = $program;
                }
            }
            $ePrograms = $retEP;
        }
*/

        $requestOrganizationInfo = new \Model\LodEntities\RequestOrganizationInfo();
        $requestOrganizationInfo = $requestOrganizationInfo->getByWhere(array(
            $requestOrganizationInfo->getIdName() => $data["filialId"],
        ));
        $request = $requestOrganizationInfo->getLicenseRequest();


        $view            = new ViewModel();
        $view->setVariables(
            array(
                "eProgramsByLevel" => $eProgramsByLevel,
                "type" => $data["type"],
                'request' => $request,
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    /**
     * Метод возвращает список адресов ОД
     *
     * url:  /eo/requests/get-filial-places-od/"requestOrganizationInfoId"
     *
     */
    public function getFilialPlacesOdAction(){
        $filId = $this->params('id');
        $type = $this->params()->fromPost("type");

        $where = array(
            "requestOrganizationInfoId" => $filId
        );

        switch($type){
            case "places": $where["requestAddressType"] = "ACTIVE"; break;
            case "open": $where["requestAddressType"] = "OPENING"; break;
            case "close": $where["requestAddressType"] = "CLOSING"; break;
        }

        $addressTable = new Model\Gateway\LodEntitiesTable("LicenseRequestAddress");
        $adr = $addressTable->getAllEntities(null, null, $where)->getObjectsArray();

        //выддерним заявление
        $requestOrganizationInfo = new \Model\LodEntities\RequestOrganizationInfo();
        $requestOrganizationInfo = $requestOrganizationInfo->getByWhere(array(
            $requestOrganizationInfo->getIdName() => $filId,
        ));
        $request =  $requestOrganizationInfo->getLicenseRequest();


        $view            = new ViewModel();
        $view->setVariables(
            array(
                "addresses" => $adr,
                "type"      => $type,
                'request'   => $request,
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    /**
     * Метод возвращает форму для создания филиала
     * @return ViewModel
     */
    public function getFilialCreateAction(){
        if($this->params()->fromPost("orgId")== null || $this->params()->fromPost("requestId")== null){
            echo "Error";
            die;
        }

        $data = [];
        $data["orgId"] = $this->params()->fromPost("orgId");
        $data["requestId"] = $this->params()->fromPost("requestId");

        $view            = new ViewModel();
        $view->setVariables(
            array(
                "data" => $data
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    /**
     * Метод обрабатывает форму добавления филиала, создает объект филиала
     */
    public function addFilialAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();
            $type = $this->params("id");

            $filial = new Model\LodEntities\RequestOrganizationInfo();
            $data["stateCode"] = "draft";

            if($type=="new")
                $data["additionalInfo"] = "new";

            $filial->setFields($data);
            $filial->save();

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    /**
     * Метод обрабатывает диалог выбора филиалов, создает для каждого выбранного филиала запись
     *
     * url:  /eo/requests/add-selected-filials
     */
    public function addSelectedFilialsAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            $orgFilialTable = new Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");
            $selectedFilials = $orgFilialTable->getAllEntities(null, null, array(
                new \Zend\Db\Sql\Predicate\In("Id",$data["filialIds"])
            ))->getObjectsArray();

            foreach($selectedFilials as $filial){

                $emailData = '';
                $phone = '';
                $educationalOrganizationDataRes = $filial->getEducationalOrganizationData();
                if(!empty($educationalOrganizationDataRes)){
                    $phone = $educationalOrganizationDataRes->getFieldOrSpace('Phones');
                    $emailData = $educationalOrganizationDataRes->getFieldOrSpace('Emails');
                }
                //колдуем с адресом почты
                if( empty($emailData) ){
                    $emailData = $filial->getFieldSafe('Mails');
                }
                if( empty($phone) ){
                    $phone = $filial->getFieldSafe('Phones');
                }

                $reqFilial = new Model\LodEntities\RequestOrganizationInfo();
                $reqFilial->setFields(array(
                    "requestId"         => $data["requestId"],
                    "orgId"             =>  $filial->getId(),
                    "fullTitle"         => $filial->getFieldSafe("FullName"),
                    "shortTitle"        => $filial->getFieldSafe("ShortName"),
                    "regularName"       => $filial->getFieldSafe("RegularName"),
                    "kpp"               => $filial->getFieldSafe("Kpp"),
                    "ogrn"              => $filial->getFieldSafe("GosRegNum"),
                    "inn"               => $filial->getFieldSafe("Inn"),
                    "licenseePhone"     => $phone,
                    "licenseeEmail"     => $emailData,
                    "address"           => $filial->getFieldSafe("Address"),
                    "StateCode"         => "draft",
                    "fk_eiisEducationalOrganizationProperties" => $filial->getFieldSafe("fk_eiisEducationalOrganizationProperties")
                ));

                //Подтягиваем адрес
                $orgLocTable = new Model\Gateway\LodEntitiesTable("OrganizationLocation");
                $orgLocs = $orgLocTable->getAllEntities(null, null, [
                    "organizationId" => $filial->getId(),
                    "addressType" => "OrganizationLocation"
                ])->getObjectsArray();
                if(count($orgLocs)>0){
                    $orgLoc = $orgLocs[0];
                    $reqFilial->setField("addressId", $orgLoc->getId());
                    $reqFilial->setField("address", $orgLoc->getField("address"));
                }

                //сохраняем
                $reqFilial->save();


                //дергаем дело
                $request = new Model\LodEntities\LicenseRequest($data["requestId"]);
                $deal = $request->getDeal();

                //дергаем заявления
                if(!empty($deal)){
                    $requestAll = $deal->getAllRequests();
                }
                if(!empty($requestAll)){
                    foreach ( $requestAll as $requestOne ) {
                        //дергаем филиалы
                        $filialAll = $requestOne->getRequestOrganizationInfoAll($reqFilial->getField("orgId"));
                        if(!empty($filialAll)){
                            foreach ( $filialAll as $filialOne ) {
                                //Проходим на обработку только если филиал связан с той же оргпнизацией что и этот но не с айди этого
                                if($filialOne->getId() != $reqFilial->getId()){

                                    $addrTable = new Model\Gateway\LodEntitiesTable("LicenseRequestAddress");
                                    //собираем адреса организации чтобы их скопировать
                                    $orgAddresses = $addrTable->getAllEntities(null, null, [
                                        "requestOrganizationInfoId" => $filialOne->getId()
                                    ]);
                                    //теперь все эти адреса копируем
                                    foreach($orgAddresses as $oa){
                                        $fields = $oa->getFields();
                                        unset($fields["id"]);
                                        $fields["requestOrganizationInfoId"] = $reqFilial->getId();
                                        $lra = new Model\LodEntities\LicenseRequestAddress();
                                        $lra->setFields($fields);
                                        $lra->save();
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    public function removeFilialAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $id = $this->params()->fromPost("filialId");

            $reqFilial = new Model\LodEntities\RequestOrganizationInfo($id);
            $reqFilial->delete();
            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    public function addSelectedFilialsEditAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            $orgFilialTable = new Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");
            $selectedFilials = $orgFilialTable->getAllEntities(null, null, array(
                new \Zend\Db\Sql\Predicate\In("Id",$data["filialIds"])
            ))->getObjectsArray();

            foreach($selectedFilials as $filial){
                $fields = array(
                    "requestId" => $data["requestId"],
                    "orgId" =>  $filial->getId(),
                    "fullTitle" => $filial->getField("FullName"),
                    "shortTitle" => $filial->getField("ShortName"),
                    "kpp" => $filial->getField("Kpp"),
                    "licenseePhone"  => $filial->getField("Phones"),
                    "licenseeEmail"  => $filial->getField("Mails"),
                    "address"  => $filial->getField("Address"),
                    "StateCode" => "editing"
                );

                $reqFilial = new Model\LodEntities\RequestOrganizationInfo();
                $fields["additionalInfo"] = "edit_from";
                $reqFilial->setFields($fields);
                $reqFilial->save();

                $reqFilial = new Model\LodEntities\RequestOrganizationInfo();
                $fields["additionalInfo"] = "edit_to";
                $reqFilial->setFields($fields);
                $reqFilial->save();
            }

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            $returnArray["errorMsg"] = $e->getMessage()." ".$e->getLine();
        }
        echo json_encode($returnArray);
        die;
    }

    public function removeFilialEditAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $orgId = $this->params()->fromPost("orgId");
            $reqFilialTable = new \Model\Gateway\LodEntitiesTable("RequestOrganizationInfo");
            $reqFilialTable->delete(array("orgId" => $orgId));

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    public function changeLicTitleSaveAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost("data");
            $formData = json_decode($data, true);

            //var_dump($formData);
            //Проходим по каждой форме
            foreach($formData as $fd){
                if(isset($fd["id"]))
                    $filial = new Model\LodEntities\RequestOrganizationInfo($fd["id"]);
                else
                    $filial = new Model\LodEntities\RequestOrganizationInfo();

                $filial->setFields($fd);
                $filial->save();
            }
            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            $returnArray["errorMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    /**
     * Метод обрабатывает сохранение мест осуществления ОД
     * url:  /eo/requests/places-od-save
     */
    public function placesOdSaveAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost("data");
            $formData = json_decode($data, true);

            $filialId = $this->params()->fromPost("filial");
            $type = $this->params()->fromPost("type");
            //Проходим по каждой форме
            $debugstr = "";
            foreach($formData as $fd){
                $debugstr .= "\n";
                $isNew = false;
                if(isset($fd["id"])) {
                    $debugstr .= "1";
                    $adress = new Model\LodEntities\LicenseRequestAddress($fd["id"]);
                } else {
                    $debugstr .= "2";
                    $adress = new Model\LodEntities\LicenseRequestAddress();
                    $isNew = true;
                    //генерируем гуид для новых адресов у которых не будет поля 'organizationLocationId'
                    //чтобы знать что эти адреса в разных организациях - одни и те же
                    $fd["guid"] = $adress->_generateId();
                }

                $adress->setFields($fd);

                switch($type){
                    case "places":$adress->setField("requestAddressType", "ACTIVE"); break;
                    case "open":$adress->setField("requestAddressType", "OPENING"); break;
                    case "close":$adress->setField("requestAddressType", "CLOSING"); break;
                }

                $adress->save();

                //после сохранения надо пройтись по всем заявлениям-филиалам-адресам
                //и внести новые правки


                //подготавливаем данные к массовому сохранегию адресов
                $adressData = $adress->getFields();
                unset($adressData[$adress->getIdName()]);
                unset($adressData['requestOrganizationInfoId']);
                //дергаем дело
                $debugstr .= "3";
                $deal = $adress->getDeal();

                //дергаем заявления
                if(!empty($deal)){
                    $debugstr .= "4";
                    $requestAll = $deal->getAllRequests();
                }
                if(!empty($requestAll)){
                    $debugstr .= "5";
                    //данные фильтра адресов
                    $organizationLocationId = $adress->getFieldSafe('organizationLocationId');
                    $requestAddressType     = $adress->getFieldSafe('requestAddressType');
                    $adressOrgInfo          = $adress->getRequestOrganizationInfo();
                    //Проходим по заявлениям
                    foreach ( $requestAll as $requestOne ) {
                        $debugstr .= "6";
                        //дергаем филиалы
                        $filialAll = $requestOne->getRequestOrganizationInfoAll($adressOrgInfo->getField("orgId"));
                        if(!empty($filialAll)){
                            $debugstr .= "7";
                            foreach ( $filialAll as $filialOne ) {
                                $debugstr .= "8";
                                //Если мы создаем адрес, которого не было в бд
                                if($isNew){
                                    $debugstr .= "9";
                                    //Чтобы не добавлять второй раз в тот же филиал
                                    if($adress->getField("requestOrganizationInfoId") != $filialOne->getId()){
                                        $debugstr .= "(10)";
                                        $newAdress = new Model\LodEntities\LicenseRequestAddress();
                                        $adressData['requestOrganizationInfoId'] = $filialOne->getId();
                                        $newAdress->setFields($adressData);
                                        $newAdress->save();
                                    }
                                }
                                //Если это редактирование адреса
                                else {
                                    $debugstr .= "(11)";
                                    //Если это редактирование адреса введенного вручную
                                    if($adress->isEmptyField('organizationLocationId')){
                                        $debugstr .= "(12)";
                                        $adressAll = $filialOne->getLicenseRequestAddressAll(
                                            array(
                                                'guid' => $adress->getField("guid"),
                                                'requestAddressType' => $requestAddressType,
                                            )
                                        );
                                    }
                                    //Если это редактирование адреса выбранного через диалог
                                    else {
                                        $debugstr .= "(13)";
                                        $adressAll = $filialOne->getLicenseRequestAddressAll(
                                            array(
                                                'organizationLocationId' => $organizationLocationId,
                                                'requestAddressType' => $requestAddressType,
                                            )
                                        );
                                    }

                                    //Если в этом филиалечто мы сейчас проходим уже был связанный адрес, то меняем его
                                    if (!empty($adressAll)) {
                                        $debugstr .= "(14)";
                                        foreach ($adressAll as $adressOne) {
                                            $debugstr .= "(15)";
                                            $adressOne->setFieldsSafe($adressData);
                                            $adressOne->save();
                                        }
                                    }
                                    //Если не было связанного адреса, то создаем
                                    else {
                                        $debugstr .= "(16)";
                                        $newAdress = new Model\LodEntities\LicenseRequestAddress();
                                        $adressData['requestOrganizationInfoId'] = $filialOne->getId();
                                        $newAdress->setFields($adressData);
                                        $newAdress->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //возвращаем
            $returnArray = array(
                "result" => "success",
                "debug" => $debugstr
            );
        } catch(\Exception $e){
            $returnArray["errorMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    /**
     * Метод обрабатывает удаление адресов ОД
     */
    public function removeOdAddressAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $adrId = $this->params()->fromPost("adrId");

            $adress = new Model\LodEntities\LicenseRequestAddress($adrId);

            //====теперь удаляем отовсюду этот адрес

            //
            $organizationLocationId = $adress->getFieldSafe('organizationLocationId');
            $requestAddressType     = $adress->getFieldSafe('requestAddressType');
            $adressOrgInfo          = $adress->getRequestOrganizationInfo();

            $debugstr = "1";

            //дергаем дело
            $deal = $adress->getDeal();

            if(!empty($deal))
                $requestAll = $deal->getAllRequests();

            if(!empty($requestAll)){
                $debugstr .= "2";
                foreach ( $requestAll as $requestOne ) {
                    $debugstr .= "3";
                    //дергаем филиалы
                    $filialAll = $requestOne->getRequestOrganizationInfoAll($adressOrgInfo->getField("orgId"));
                    if(!empty($filialAll)){
                        foreach ( $filialAll as $filialOne ) {
                            $debugstr .= "4";
                            if($adress->isEmptyField('organizationLocationId')){
                                $debugstr .= "5";
                                $adressAll = $filialOne->getLicenseRequestAddressAll(
                                    array(
                                        'guid' => $adress->getField("guid"),
                                        'requestAddressType' => $requestAddressType,
                                    )
                                );
                            } else {
                                $debugstr .= "6";
                                //дергаем филиалы с фильтрами
                                $adressAll = $filialOne->getLicenseRequestAddressAll(
                                    array(
                                        'organizationLocationId' => $organizationLocationId,
                                        'requestAddressType' => $requestAddressType,
                                    )
                                );
                            }
                            if(!empty($adressAll)){
                                $debugstr .= "7";
                                foreach ( $adressAll as $adressOne ) {
                                    $debugstr .= "8";
                                    $adressOne->delete();
                                }
                            }
                        }
                    }
                }
            }
            //удаляем начальный адрес
            $adress->delete();

            //возвращаем
            $returnArray = array(
                "result" => "success",
                //"debug" => $debugstr
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    public function addSelectedAddressAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            $filAddrTable = new Model\Gateway\LodEntitiesTable("OrganizationLocation");
            $addresses = $filAddrTable->getAllEntities(null, null, array(
                new \Zend\Db\Sql\Predicate\In("id",$data["addrIds"])
            ))->getObjectsArray();

            $filialAddressesTable = new Model\Gateway\LodEntitiesTable("LicenseRequestAddress");
            $cons = $filialAddressesTable->getAllEntities(null, "consecutiveNumber DESC")->getObjectsArray();
            $maxNumber = 0;
            if(count($cons)>0){
                $maxNumber = $cons[0]->getField("consecutiveNumber");
            }

            foreach($addresses as $addr){
                $fields = [
                    "requestOrganizationInfoId"	=> $data["filialId"],
                    "requestAddressType" => "",
                    "organizationLocationId" => $addr->getId(),
                    "postcode" => $addr->getField("postcode"),
                    "address" => $addr->getField("address"),
                    "consecutiveNumber" => $maxNumber++
                ];

                switch($data["type"]){
                    case "places": $fields["requestAddressType"] = "ACTIVE"; break;
                    case "open": $fields["requestAddressType"] = "OPENING"; break;
                    case "close": $fields["requestAddressType"] = "CLOSING"; break;
                }

                $newAddr = new Model\LodEntities\LicenseRequestAddress();
                $newAddr->setFields($fields);
                $newId = $newAddr->save();

                //теперь если это дело и на реорганизацию, то добавляем адресс во все заявления

                //данные фильтра адресов
                $organizationLocationId = $newAddr->getFieldSafe('organizationLocationId');
                $requestAddressType     = $newAddr->getFieldSafe('requestAddressType');
                $adressOrgInfo          = $newAddr->getRequestOrganizationInfo();
                //подготавливаем данные к массовому сохранегию адресов
                $adressData = $newAddr->getFields();
                unset($adressData[$newAddr->getIdName()]);
                unset($adressData['requestOrganizationInfoId']);
                //дергаем дело
                if(!empty($organizationLocationId) && !empty($requestAddressType)){
                    $deal = $newAddr->getDeal();
                }
                //дергаем заявления
                if(!empty($deal)){
                    $requestAll = $deal->getAllRequests();
                }
                if(!empty($requestAll)){
                    foreach ( $requestAll as $requestOne ) {
                        //дергаем филиалы
                        $filialAll = $requestOne->getRequestOrganizationInfoAll($adressOrgInfo->getField("orgId"));
                        if(!empty($filialAll)){
                            foreach ( $filialAll as $filialOne ) {
                                if($filialOne->getId() == $newAddr->getField("requestOrganizationInfoId"))
                                    continue;

                                    $newAdress = new Model\LodEntities\LicenseRequestAddress();
                                    $adressData['requestOrganizationInfoId'] = $filialOne->getId();
                                    $newAdress->setFieldsSafe($adressData);
                                    $newAdress->save();
                                    //}

                            }
                        }
                    }
                }
            }

            //возвращаем
            $returnArray = array(
                "result" => "success",
                "id" => $newId
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    /**
     * Метод возвращает диалог выбора ОП
     */
    public function getOpDialogAction(){
        if($this->params()->fromPost("orgId")== null || $this->params()->fromPost("requestId")== null){
            echo "Error";
            die;
        }

        $orgId = $this->params()->fromPost("orgId");
        $org = new Model\Entities\eiisEducationalOrganizations($orgId);

        $requestId = $this->params()->fromPost("requestId");
        $request = new Model\LodEntities\LicenseRequest($requestId);

        $type = $this->params()->fromPost("type");

        //уровни
        /*
        $levelTable = new Model\Gateway\EntitiesTable("eiis_EduLevels");
        $levels = $levelTable->getAllEntities(null, "Name ASC",array(
            "EduProgramKind" => "Основная"
        ))->getObjectsArray();
        */

        $EPTTable = new Model\Gateway\EntitiesTable("eiis_EduProgramTypes");
        $EPTypes = $EPTTable->getAllEntities(null, "Name ASC");

        //================ Собираем объект по уровням и типам
        /*
        $typesByLevelsList = array();

        //Проходим по всем уровням
        foreach($levels as $level){
            $typesByLevelsList[$level->getId()]=[
                "item" => $level->getFields(),
                "types" => array()
            ];
        }

        //Проходим по всем типам
        foreach($EPTypes as $type){
            $typesByLevelsList[$type->getField("fk_eiisEduLevels")]["types"][] = $type->getFields();
        }
*/
        //вью
        $view = new ViewModel();
        $view->setVariables([
            "type" => $type,
            "orgId" => $orgId,
            "requestId" => $requestId,
            //"typesByLevelsList" => $typesByLevelsList
            "types" => $EPTypes
        ]);
        $view->setTerminal(true);
        return $view;
    }
    /**
     * Метод возвращает диалог выбора ОП из текущих
     */
    public function selectCurrentOpDialogAction(){
        if($this->params()->fromPost("orgId")== null || $this->params()->fromPost("requestId")== null){
            echo "Error";
            die;
        }

        $orgInfoId = $this->params()->fromPost("orgId");
        $orgInfo = new Model\LodEntities\RequestOrganizationInfo($orgInfoId);
        $orgId = $orgInfo->getField("orgId");

        $requestId = $this->params()->fromPost("requestId");
        $request = new Model\LodEntities\LicenseRequest($requestId);

        $type = $this->params()->fromPost("type");


        // Т.е. на примере дела 1032 в нем 1 заявление № 1270 (0fb4c91504bb44aaa5adcf405d8f6e2c).
        // Для этого заявления в таблице LicenseSupplementToLicenseRequest есть связь с приложением
        // 4E99E7A747BEA1611CE21392131A4A64 из действующей лицензии организации. Тем самым пользователь
        // указал, что хочет приостановить программы из этого приложения.
        // Мы берём гуид этого приложения и идём в таблицу EducationProgram и предлагаем пользователю
        // на выбор программы только из этого приложения.
        $lEPList = [];
        //подзапрос
        $mtmSuppsTable = new Model\Gateway\LodEntitiesTable("LicenseSupplementToLicenseRequest");
        $mtmSupps = $mtmSuppsTable->getAllEntities(null, null, [
            "licenseRequestId" => $requestId
        ]);
        $suppIds = [];
        foreach($mtmSupps as $mtmSupp){
            $suppIds[] = $mtmSupp->getField("licenseAttachId");
        }


        if(!empty($suppIds)) {
            $orgIds = [$orgId];
            //Теперь проверяем если мы имеем дело с делом по реорганизации
            //тогда мы будем выбирать оп по фииалам что в том же городе что и выбранный
            $deal = $request->getDeal();
            if(!empty($deal)){
                $mainRequest = $deal->getMainLicenseRequest();
                //проверяем на причины
                if($mainRequest->hasReasonReorgTransform() || $mainRequest->hasReasonReorgJoin() || $mainRequest->hasReasonReorgMerge() || $mainRequest->hasReasonById('08f4049094984955ab29d17d6f178d17') || $mainRequest->hasReasonById('317c24a9fb7c42b88400432d210a01c4')){
                    $org = new Model\Entities\eiisEducationalOrganizations($orgId);
                    //город филиала что сейчас выбран
                    $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\LodDbAdapter::getInstance());
                    $subselectLocations = $sql->select();
                    $locs = $subselectLocations->from("OrganizationLocation")
                        ->columns(array("settlementId"))
                        ->where(array(
                            "organizationId" => $org->getId(),
                            "addressType" => "OrganizationLocation"
                        ));

                    //айдишники все организаций и филиалов
                    //организации
                    $orgsId = [$org->getId()];

                    $reorgTable = new \Model\Gateway\LodEntitiesTable("ReorganizedOrganizationInfo");
                    $reorgs = $reorgTable->getEntitiesByWhere([
                        "licenseRequestId" => $requestId
                    ])->getObjectsArray();

                    //айдишники реорганизаций
                    foreach($reorgs as $reorg){
                        if(!in_array($reorg->getField("organizationId"), $orgsId))
                            $orgsId[] = $reorg->getField("organizationId");
                    }

                    //+нам нужны все филиалы
                    $eoTable = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");

                    $filials = $eoTable->getEntitiesByWhere([
                        new \Zend\Db\Sql\Predicate\In("fk_eiisParentEducationalOrganization", $orgsId),
                    ])->getObjectsArray();

                    foreach($filials as $filial){
                        if(!in_array( $filial->getId(), $orgsId))
                            $orgsId[] = $filial->getId();
                    }

                    $orgLocTable = new Model\Gateway\LodEntitiesTable("OrganizationLocation");
                    $orgsIdFromLocs = $orgLocTable->getAllEntities(null, null,
                        array(
                            new \Zend\Db\Sql\Predicate\In("settlementId", $locs),
                            new \Zend\Db\Sql\Predicate\In("organizationId", $orgsId),
                        )
                    );

                    $orgIds = [];
                    foreach($orgsIdFromLocs as $loc){
                        $orgIds[] = $loc->getField("organizationId");
                    }
                }
            }

            if(!empty($orgIds)) {
                //теперь формируем запрос к приложениями фильтруем по филиалу
                $suppsTable = new Model\Gateway\EntitiesTable("eiis_LicenseSupplements");
                $subselect = $suppsTable->getAllEntities(null, null, [
                    new \Zend\Db\Sql\Predicate\In("Id", $suppIds),
                    new \Zend\Db\Sql\Predicate\In("fk_eiisBranch", $orgIds)
                ]);

                $subselectIds = [];
                foreach ($subselect as $sub) {
                    $subselectIds[] = $sub->getId();
                }

                if (!empty($subselectIds)) {
                    $lEPTable = new Model\Gateway\LodEntitiesTable("EducationProgram");
                    $lEPList = $lEPTable->getAllEntities(null, null, [
                        new \Zend\Db\Sql\Predicate\In("LicenseSupplementID", $subselectIds)
                    ])->getObjectsArray();
                }
            }
        }

        //Фильтр по уровням
        $levelsList = [];
        $levelId = [];
        foreach($lEPList as $program){
            if($program->isEmptyField("educationLevelId"))
                continue;

            $level = $program->getEduProgramLevel();
            if($level != null && !isset($levelId[$level->getId()])) {
                $levelsList[] = $level;
                $levelId[$level->getId()] = true;
            }
        }

        //Сортировка по коду
        usort($lEPList, array($this, "sortEducationProgramsByCode"));


        //вью
        $view = new ViewModel();
        $view->setVariables([
            "type" => $type,
            "orgId" => $orgId,
            "requestId" => $requestId,
            "levels" => $levelsList,
            'lodEduPogram'  => $lEPList,
            'orgInfoId' => $orgInfoId
        ]);
        $view->setTerminal(true);
        return $view;
    }

    private function sortEducationProgramsByCode($a, $b){
        $aProgram = $a->getEduProgram();
        $bProgram = $b->getEduProgram();

        $nameA = $aProgram->getField("Code");
        $nameB = $bProgram->getField("Code");
        if ($nameA == $nameB) {
            return 0;
        }
        return ($nameA < $nameB) ? -1 : 1;
    }

    public function getOpListAction(){
        $typeId = $this->params()->fromPost("typeId");

        //Таблица с оп
        $opTable = new Model\Gateway\EntitiesTable("eiis_EduPrograms");
        $ops = $opTable->getAllEntities(null,"Code ASC",[
            "fk_eiisEduProgramType" => $typeId
        ])->getObjectsArray();



        //вью
        $view = new ViewModel();
        $view->setVariables([
            "programs" => $ops
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function addOpAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();
            $type = $data["type"];
            unset($data["type"]);

            if($type=="change-op"){
                $ep = new Model\LodEntities\LicenseRequestEducationProgram();
                $ep->setFields($data);
                $ep->setField("typeOfActivity","EDITING");
                $fields["additionalInfo"] = "edit_from";
                $ep->save();

                $ep = new Model\LodEntities\LicenseRequestEducationProgram();
                $ep->setFields($data);
                $ep->setField("typeOfActivity","EDITING");
                $fields["additionalInfo"] = "edit_to";
                $ep->save();
            } else {
                switch ($type) {
                    case "new-op":
                        $data["typeOfActivity"] = "OPENING";
                        break;
                    case "current-op":
                        $data["typeOfActivity"] = "ACTIVE";
                        break;
                    case "stop-op":
                        $data["typeOfActivity"] = "CLOSING";
                        break;

                    case "change-op":
                        $data["typeOfActivity"] = "EDITING";
                        break;
                }

                $ep = new Model\LodEntities\LicenseRequestEducationProgram();
                $ep->setFields($data);
                $ep->save();
            }

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    public function addCurrentOpAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();
            $type = $data["type"];

            foreach($data["programs"] as $programId){
                $LodEducationProgram = new Model\LodEntities\EducationProgram($programId);
                $eduProgram = $LodEducationProgram->getEduProgram();

                $data = [
                    "requestOrganizationInfoId" => $data["requestOrganizationInfoId"],
                    "educationProgramCatalogItemId" => $eduProgram->getId(),
                    "educationLevelId" => $eduProgram->getField("fk_eiisEduLevels"),
                    "eduProgramTypeId" => $eduProgram->getField("fk_eiisEduProgramType"),
                    "fk_EducationProgram" => $programId
                ];
                switch ($type) {
                    case "current-op":
                        $data["typeOfActivity"] = "ACTIVE";
                        break;
                    case "stop-op":
                        $data["typeOfActivity"] = "CLOSING";
                        break;
                }
                $ep = new Model\LodEntities\LicenseRequestEducationProgram();
                $ep->setFields($data);
                $id = $ep->save();


                //вытягиваем и копируем
                //квалификации нужно подтянуть из тех, прог, что в приложении
                $mtmEPToQualTable = new Model\Gateway\LodEntitiesTable("EducationProgramToQualification");
                $mtmLinks = $mtmEPToQualTable->getAllEntities(null,null,[
                    "educationProgramId" => $programId
                ]);
                //и сохранить эти квалификации в LicenseRequestEducationProgramToQualification
                foreach($mtmLinks as $link){
                    $mtmLink = new Model\LodEntities\LicenseRequestEducationProgramToQualification();
                    $mtmLink->setFields([
                        "licenseRequestEducationProgramId" => $id,
                        "qualificationId" => $link->getField("qualificationId")
                    ]);
                    $mtmLink->save();
                }
            }

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            $returnArray["ErrorMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function addNewOpAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            //Проверяем, если такая программа уже была добавлена, то не добавляем новую
            if(Model\LodEntities\LicenseRequestEducationProgram::programExists($data["requestOrganizationInfoId"],$data["educationProgramCatalogItemId"])){
                $returnArray = array(
                    "result" => "error",
                    "error"  => "Такая программа уже была добавлена ранее"
                );
            } else {


                $ep = new Model\LodEntities\LicenseRequestEducationProgram();

                $qualificationIds = $data["qualificationId"];
                unset($data["qualificationId"]);

                $ep->setFields($data);
                $id = $ep->save();

                if(isset($qualificationIds) && !empty($qualificationIds))
                    foreach ($qualificationIds as $qualId) {
                        $mtm = new Model\LodEntities\LicenseRequestEducationProgramToQualification();
                        $mtm->setFields([
                            "licenseRequestEducationProgramId" => $id,
                            "qualificationId" => $qualId
                        ]);
                        $mtm->save();
                    }

                //возвращаем
                $returnArray = array(
                    "result" => "success",
                    "id" => $id
                );
            }
        } catch(\Exception $e){
            $returnArray["errMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    /**
     * Метод обрабатывает удаление ОП
     */
    public function removeOpAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $opId = $this->params()->fromPost("opId");

            $opTable = new \Model\Gateway\LodEntitiesTable("LicenseRequestEducationProgram");
            $opTable->delete(array("id" => $opId));

            //Удаляем связи с квалификациями
            $mtmTable = new \Model\Gateway\LodEntitiesTable("LicenseRequestEducationProgramToQualification");
            $mtmTable->delete(array("licenseRequestEducationProgramId" => $opId));

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    public function changeOpNameSaveAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost("data");
            $formData = json_decode($data, true);

            //Проходим по каждой форме
            foreach($formData as $fd){
                if(isset($fd["id"]))
                    $program = new Model\LodEntities\LicenseRequestEducationProgram($fd["id"]);
                else
                    $program = new Model\LodEntities\LicenseRequestEducationProgram();

                $program->setFields($fd);
                $program->save();
            }
            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            $returnArray["errorMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function getPaymentDialogAction(){
        $requestId = null;
        $payment = null;

        if ($this->params()->fromPost()) {
            $post = $this->params()->fromPost();
            if( !empty($post['requestId'])){
                $requestId = $post['requestId'];
            }
            if( !empty($post['LicenseRequestPaymentId'])){
                $payment = new Model\LodEntities\LicenseRequestPayment( $post['LicenseRequestPaymentId']);
            }
        }
        //вью
        $view = new ViewModel();
        $view->setVariables([
            "requestId" => $requestId,
            "payment" => $payment,
        ]);
        $view->setTerminal(true);
        return $view;
    }

    /**
     * платежые поруучения - закидываем данные
     *
     * url:  /eo/requests/add-payment
     */
    public function addPaymentAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $dbAdapter = \Model\Gateway\LodDbAdapter::getInstance();
            $driver = $dbAdapter->getDriver();
            $connection = $driver->getConnection();
            $connection->beginTransaction();

            $data = $opId = $this->params()->fromPost();

            $payment = new Model\LodEntities\LicenseRequestPayment();
            $payment->setFields($data);
            $payment->save();

            $files = $this->getRequest()->getFiles();
            if( !empty($files['scanFile']) ){
                //$firstFile = reset($files);
                //if( !empty($firstFile) ){
                    $content = '';
                    @$fileBank = new \Model\LodEntities\EiFile();
                    $resultSend = $fileBank->updateByCodeIdContent(
                        null , 'LicenseRequestPayment', $payment->getId(), $files['scanFile']['name'],
                        $content, $files['scanFile']['tmp_name']);
                //}
            }

            $file = $payment->getScanFile();
            $fileName = '';
            $fileUrl = '';
            if( !empty($file) ){
                $fileName = $file->getFileName();
                $fileUrl = $file->getUrl();
            }
            $link = '';
            if( !empty($fileUrl) ){
                 $link = '<a
                        href="'.$fileUrl.'"
                        download="'.$fileName.'"
                        target="_blank"
                    ><img src="/img/islod/file_expand.png"></a>  ';
            }


                        $paymentArray = array();
                        $paymentNum = $payment->getFieldSafe("NumDoc");
                        if(!empty($paymentNum)){
                            $paymentArray['num'] = '№ '.$paymentNum;
                        }
                        $paymentNum = $payment->getDate("Date");
                        if(!empty($paymentNum)){
                            $paymentArray['date'] = 'от '.$paymentNum. ' г.';
                        }
                        $paymentNum = $payment->getFieldSafe("Sum");
                        if(!empty($paymentNum)){
                            $paymentArray['sum'] = 'на  '.$paymentNum. ' руб.';
                        }
                        $paymentNum = $payment->getFieldSafe("Comment");
                        if(!empty($paymentNum)){
                            $paymentArray['coment'] = $paymentNum;
                        }
                        $paymentStr = implode( ' ', $paymentArray );
            //возвращаем
            $returnArray = array(
                "result" => "success",
                "row" =>
'<tr id="p_'.$payment->getId().'">
    <td>Платежное поручение</td>
    <td>'.
    $paymentStr.$link
    .'<span  style="background: none;float:right;" class="btRemove minus-icon" id="p_'.$payment->getId().'">'
        . '<span style="margin-left: -15px;">'
                . '<img class="iconFile removeFileType" onmouseover="this.src='.'\'/img/islod/delete.png\''.';" onmouseout="this.src='.'\'/img/islod/delete.png\''.';" src="/img/islod/delete.png">'
        . '</span>'
    . '</span>'
    . '<span style="background: none;float: right;" class="btEdit minus-icon" id="p_'.$payment->getId().'" >'
        . '<span style="margin-left: -15px;">'
                . '<img class=" iconFile newFileType" onmouseover="this.src='.'\'/img/islod/pencil.png\''.';" onmouseout="this.src='.'\'/img/islod/pencil.png\''.';" src="/img/islod/pencil.png">
        </span>
    </span>
    </td>
</tr>'
            );
            $connection->commit();
        } catch(\Exception $e){
            $connection->rollback();
            $returnArray = array(
                "result" => "error",
                "error" => "Произошла ошибка",
                "message" => $e->getMessage(),
                "console" => $e->getMessage(),
            );
        }
        echo json_encode($returnArray);
        die;
    }

    public function removePaymentAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $id = $this->params()->fromPost("paymentId");

            $reqPayment = new Model\LodEntities\LicenseRequestPayment($id);
            $reqPayment->delete();
            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }


    /**
     * лицензирование - карточка заявлени - сохраняем - главный фрейм
     *
     * url:  /eo/requests/save-general
     * @throws \Exception
     */
    public function saveGeneralAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost("data");
            $formData = json_decode($data, true);

            //проверим на наличие заполненость полей
            $notEmptyFiels = array(
                'orgInfo'=> array('fullTitle')
            );
            $errorDescription = array();
            foreach ( $notEmptyFiels as $formKey => $formValue ){
                foreach ( $formValue as $elementKey => $elementValue ){
                    if( empty($formData[ $formKey ][ $elementKey ]) ){
                        //$errorDescription[ $formKey ][ $elementValue ][] = 'введите значение';
                    }
                }
            }

            if( empty($errorDescription) ){
                if(isset($formData["request"]) && isset($formData["request"]["id"])){
                    $request = new Model\LodEntities\LicenseRequest($formData["request"]["id"]);
                    $request->setFields($formData["request"]);
                    $request->save();

                    unset($formData["request"]["id"]);
                    unset($formData["request"]["number"]);
                    unset($formData["request"]["creationDate"]);
                    unset($formData["request"]["fillingDate"]);

                    //Теперь проходим по всем заявениям дела
                    $deal = $request->getDeal();
                    if(!empty($deal))
                        $allRequests = $deal->getAllRequests();
                    if(!empty($allRequests)) foreach($allRequests as $reqOne){
                        //не сохраняем повторно заявление
                        if($reqOne->getId() ==  $request->getId())
                            continue;

                        $reqOne->setFields($formData["request"]);
                        $reqOne->save();
                    }
                }
                //сохраняем правки о вузе во всех заявах этого дела
                if(isset($formData["orgInfo"])){
                    if(isset($formData["orgInfo"]["id"])) {
                        $orgInfo = new Model\LodEntities\RequestOrganizationInfo($formData["orgInfo"]["id"]);
                    } else {
                        $orgInfo = new Model\LodEntities\RequestOrganizationInfo();
                    }

                    $orgInfo->setFields($formData["orgInfo"]);
                    $orgInfo->setField("additionalInfo","base");
                    $orgInfo->save();
                    //проверяем есть ли этот вуз еще в заявлениях этого дела, если ксть то правим его
                    $orgId = $formData["orgInfo"]['orgId'];
                    if(empty($orgId)){
                        throw new \Exception('Лод - сохранение головняка - не найден ид филиала у записи '.$request->getId());
                    }
                    if(empty($request)){
                        $request = new Model\LodEntities\LicenseRequest();
                        $request = $request->getByWhere(array($request->getIdName()=>$formData["orgInfo"]['requestId']));
                    }
                    if(empty($request)){
                        throw new \Exception('Лод - сохранение головняка - не найден заявление  '.$request->getId());
                    }
                    //запилка чтоб последующее не менять
                    $rowOne = $request;
                    $rowId = $request->getId();
                    $tableRow[$rowId] = $formData["orgInfo"];
                    unset($tableRow[$rowId]['id']);
                    unset($tableRow[$rowId]['requestId']);
                    //выдрано из AffairsController::saveFilialAction()
                    if(!empty($rowOne)){
                        $deal = $rowOne->getDeal();
                        //связанных дел может и не быть
                        if(isset($deal)) {
                            $reqAll = $deal->getAllRequests();
                            if (!empty($reqAll)) {
                                foreach ($reqAll as $reqOne) {
                                    $orgInfoAll = $reqOne->getRequestOrganizationInfoAll($orgId);

                                    if (!empty($orgInfoAll)) {
                                        foreach ($orgInfoAll as $orgInfoOne) {
                                            $orgInfoOne->setFieldsSafe($tableRow[$rowId]);
                                            $orgInfoOne->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //Проверяем, если это главное заявление дела, то надо в деле менять дату дела, вдруг в заявлении изменили
                if(isset($request)) {
                    $deal = $request->getDeal();
                    if (isset($deal) && isset($request)) {
                        $mainRequest = $deal->getMainLicenseRequest();
                        if ($mainRequest->getId() == $request->getId()){
                            $creationDate = $request->getField("creationDate");
                            $deal->setField("registrationDate",$creationDate);
                            $deal->save();
                        }
                }
                }
                //возвращаем
                $returnArray = array(
                    "result" => "success"
                );
            }
            else{
                $returnArray = array(
                    "result"            => "error",
                    'errorDescription'  => $errorDescription,
                );
            }
        } catch(\Exception $e){
            $returnArray["msg"] = $e->getFile()." ".$e->getLine()." ".$e->getMessage()." | ".$e->getTraceAsString();
        }
        echo json_encode($returnArray);
        die;
    }

    public function dialogSelectSupplementAction(){
        if($this->params()->fromPost("requestId")== null){
            echo "Error";
            die;
        }

        $requestId = $this->params()->fromPost("requestId");
        $request = new Model\LodEntities\LicenseRequest($requestId);

        $license = new Model\Entities\eiisLicenses($this->params()->fromPost("licenseId"));
        $licenseSupplement = $license->getValidSupplements(true);

        //текущие подключенные приложения
        $requestSupplements = $request->getSupplements();
        $reqSuppIdList = [];
        foreach($requestSupplements as $rSupp){
            $reqSuppIdList[] = $rSupp->getId();
        }

        //вью
        $view = new ViewModel();
        $view->setVariables([
            "supplements" => \Eo\Model\Licensing\Service\LicenseSupplementInfoService::supplementsObjectArrInfoRowPrepare(
                $licenseSupplement
            ),
            "reqSuppIdList" => $reqSuppIdList
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function addSelectedSupplementsAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            $LicSuppTable = new Model\Gateway\EntitiesTable("eiis_LicenseSupplements");
            $selectedSupps = $LicSuppTable->getAllEntities(null, null, array(
                new \Zend\Db\Sql\Predicate\In("Id",$data["suppIds"])
            ))->getObjectsArray();

            foreach($selectedSupps as $supp){
                $suppRequestLink = new Model\LodEntities\LicenseSupplementToLicenseRequest();
                $suppRequestLink->setFields([
                    "licenseAttachId" => $supp->getId(),
                    "licenseRequestId" => $data["requestId"]
                ]);
                $suppRequestLink->save();
            }

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    public function removeSuppAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $suppId = $this->params()->fromPost("suppId");
            $requestId = $this->params()->fromPost("requestId");

            $mtmTable = new Model\Gateway\LodEntitiesTable("LicenseSupplementToLicenseRequest");
            $mtmTable->delete([
                "licenseAttachId" => $suppId,
                "licenseRequestId" => $requestId
            ]);

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            echo json_encode($returnArray);
        }
        echo json_encode($returnArray);
        die;
    }

    /**
     * Метод сохраняет данные по реорганизации
     */
    public function reorgLicSaveAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            /* что приходит
                id:                            0B0E1110-0C0F-130F-100A-0D0D0E120A110D12130B
                organizationId:                CC52DDF46F886FF3F64A7E45846AEA0A
                reorganizedOrganizationId:     004CF827406010BB7A4E233999869102
                requestId:
             */

            $isEdit = false;
            if(isset($data["id"]) && !empty($data["id"])){
                $licReorg = new \Model\LodEntities\ReorganizedOrganizationInfo($data["id"]);
                $isEdit = true;
            } else {
                $licReorg = new \Model\LodEntities\ReorganizedOrganizationInfo();
            }
            /* Поля
                 id
                 licenseRequestId
                 organizationId
                 organizationTitle
                 reorganizationType
                 licenseId
             */

            /* reorganizationType
             ACCESSION	c83201802b474c4393305b16b7c05375	Реорганизация лицензиата в форме присоединения
             CONVERSION	518dfabe3375431684b71d2faa5c7564	Реорганизация лицензиата в форме преобразования
             MERGER	f9893ac3e72e43b59101a8270f9eaacf	Реорганизация лицензиата в форме слияния
             SEPARETION	317c24a9fb7c42b88400432d210a01c4	Реорганизация лицензиата в форме разделения
             ALLOCATION	08f4049094984955ab29d17d6f178d17	Реорганизация лицензиата в форме выделения
             */
            //==========определяем тип
            $reorgType = "";
            //
            $request = new Model\LodEntities\LicenseRequest($data["requestId"]);
            if($request->hasReasonById("c83201802b474c4393305b16b7c05375"))
                $reorgType = "ACCESSION";
            else if($request->hasReasonById("518dfabe3375431684b71d2faa5c7564"))
                $reorgType = "CONVERSION";
            else if($request->hasReasonById("f9893ac3e72e43b59101a8270f9eaacf"))
                $reorgType = "MERGER";
            else if($request->hasReasonById("317c24a9fb7c42b88400432d210a01c4"))
                $reorgType = "SEPARETION";
            else if($request->hasReasonById("08f4049094984955ab29d17d6f178d17"))
                $reorgType = "ALLOCATION";
            //--

            //реорг организация
            $reorgOrg = new Model\Entities\eiisEducationalOrganizations($data["reorganizedOrganizationId"]);

            //лицензия реорг организации
            $license = $reorgOrg->getLicense();
            $licenseId = "";
            if($license != null)
                $licenseId = $license->getId();

            $fields = [
                "licenseRequestId"      => $data["requestId"],
                "organizationId"        => $data["reorganizedOrganizationId"],
                "organizationTitle"     => $reorgOrg->getField("FullName"),
                "reorganizationType"    => $reorgType,
                "licenseId"             => $licenseId
            ];
            if(isset($data["id"]))
                $fields["id"] = $data["id"];

            $licReorg->setFields($fields);

            $licReorgId = $licReorg->save();

            //возвращаем
            $returnArray = array(
                "result" => "success",
                "id" => $licReorgId
            );
        } catch(\Exception $e){
            $returnArray["errMsg"] = $e->getLine().": ".$e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function getReorgListAction(){
        $id = $this->params()->fromPost("requestId");
        $request = new Model\LodEntities\LicenseRequest($id);

        $reorgs = $request->getReorganizationsData();
        //вью
        $view = new ViewModel();
        $view->setVariables([
            "reorgs" => $reorgs
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function addReorgAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            /* что приходит
                organizationId: reqOrgId,
                reorganizedOrganizationId: orgId,
                requestId: $("#requestId", _private.container).val()
             */

            if(isset($data["id"]) && !empty($data["id"])){
                $licReorg = new \Model\LodEntities\ReorganizedOrganizationInfo($data["id"]);
            } else {
                $licReorg = new \Model\LodEntities\ReorganizedOrganizationInfo();
            }
            /* Поля
                 id
                 licenseRequestId
                 organizationId
                 organizationTitle
                 reorganizationType
                 licenseId
             */

            /* reorganizationType
             ACCESSION	c83201802b474c4393305b16b7c05375	Реорганизация лицензиата в форме присоединения
             CONVERSION	518dfabe3375431684b71d2faa5c7564	Реорганизация лицензиата в форме преобразования
             MERGER	f9893ac3e72e43b59101a8270f9eaacf	Реорганизация лицензиата в форме слияния
             SEPARETION	317c24a9fb7c42b88400432d210a01c4	Реорганизация лицензиата в форме разделения
             ALLOCATION	08f4049094984955ab29d17d6f178d17	Реорганизация лицензиата в форме выделения
             */
            //==========определяем тип
            $reorgType = "";
            //
            $request = new Model\LodEntities\LicenseRequest($data["requestId"]);
            if($request->hasReasonById("c83201802b474c4393305b16b7c05375"))
                $reorgType = "ACCESSION";
            else if($request->hasReasonById("518dfabe3375431684b71d2faa5c7564"))
                $reorgType = "CONVERSION";
            else if($request->hasReasonById("f9893ac3e72e43b59101a8270f9eaacf"))
                $reorgType = "MERGER";
            else if($request->hasReasonById("317c24a9fb7c42b88400432d210a01c4"))
                $reorgType = "SEPARETION";
            else if($request->hasReasonById("08f4049094984955ab29d17d6f178d17"))
                $reorgType = "ALLOCATION";
            //--

            //реорг организация
            $reorgOrg = new Model\Entities\eiisEducationalOrganizations($data["reorganizedOrganizationId"]);

            //лицензия реорг организации
            $license = $reorgOrg->getLicense();
            $licenseId = "";
            if($license != null)
                $licenseId = $license->getId();

            $fields = [
                "licenseRequestId"      => $data["requestId"],
                "organizationId"        => $data["reorganizedOrganizationId"],
                "organizationTitle"     => $reorgOrg->getField("FullName"),
                "reorganizationType"    => $reorgType,
                "licenseId"             => $licenseId
            ];
            if(isset($data["id"]))
                $fields["id"] = $data["id"];

            $licReorg->setFields($fields);

            $licReorg->save();

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            $returnArray["errMsg"] = $e->getLine().": ".$e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function reorgRemoveAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $reorgId = $this->params()->fromPost("reorgId");

            $reorg = new Model\LodEntities\ReorganizedOrganizationInfo($reorgId);
            $reorg->delete();

            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            $returnArray["errMsg"] = $e->getLine().": ".$e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function reorgsSaveAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost("data");
            $formData = json_decode($data, true);

            //Проходим по каждой форме
            foreach($formData as $fd){
                if(isset($fd["id"]))
                    $reorg = new Model\LodEntities\OrganizationToReorganizedOrganization($fd["id"]);
                else
                    continue;

                $reorg->setFields($fd);
                $reorg->save();
            }
            //возвращаем
            $returnArray = array(
                "result" => "success"
            );
        } catch(\Exception $e){
            $returnArray["errorMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function getOpLevelDialogAction(){
        //Подгружаем уровни
        $levelTable = new Model\Gateway\EntitiesTable("eiis_EduLevels");
        $levels = $levelTable->getAllEntities(null, "Code ASC, Name ASC", [
            new \Zend\Db\Sql\Predicate\IsNotNull("Code")
        ]);

        //Подгружаем программы
        $data = $this->params()->fromPost();
        if(isset($data["filialId"]))
            $where = [
                "requestOrganizationInfoId" => $data["filialId"]
            ];

        if(isset($data["type"]) && ($data["type"] == "change-op" || $data["type"] == "new-op")) {
            //$where[] = new \Zend\Db\Sql\Predicate\NotLike("additionalInfo", "edit_from");

            //В "Новые ОП" должны выводиться программы с typeOfActivity = OPENING.
            if($data["type"] == "new-op"){
                $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity","OPENING");
            }
            if($data["type"] == "change-op"){
                $where[] = new \Zend\Db\Sql\Predicate\Like("typeOfActivity","EDITING");
            }

            $epTable = new Model\Gateway\LodEntitiesTable("LicenseRequestEducationProgram");
            $ePrograms = $epTable->getAllEntities(null, null, $where)->getObjectsArray();
        }

        //Разбиваем программы по уровням
        $eProgramsByLevel = [];
        foreach($ePrograms as $program){
            if($program->isEmptyField("educationLevelId"))
                continue;

            $eProgramsByLevel[$program->getField("educationLevelId")] = true;
        }

        //Проходим по уровням, подготавливаем к отображению
        $levelsTree = [];
        foreach($levels as $level){
            if(!$level->isEmptyField("parentlevel")){
                if(!isset($levelsTree[$level->getField("parentlevel")]))
                    $levelsTree[$level->getField("parentlevel")]=[
                        "item" => null,
                        "children"     => []
                    ];

                $levelsTree[$level->getField("parentlevel")]["children"][] = $level;
            } else {
                if(!isset($levelsTree[$level->getId()]))
                    $levelsTree[$level->getId()]=[
                        "item" => null,
                        "children"     => []
                    ];

                $levelsTree[$level->getId()]["item"] = $level;
            }
        }



        //вью
        $view = new ViewModel();
        $view->setVariables([
            "levelsTree" => $levelsTree,
            "eProgramsByLevel" => $eProgramsByLevel
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function createLevelProgramsAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $data = $this->params()->fromPost();

            //Получаем список уровней
            $levelTable = new Model\Gateway\EntitiesTable("eiis_EduLevels");
            $levels = $levelTable->getAllEntities(null, null, [
                new In("Id", $data["levelIds"])
            ])->getObjectsArray();

            foreach ($levels as $level) {
                if ($level->isEmptyField("fk_eiisEduProgram"))
                    continue;

                $eduProgram = new Model\Entities\eiisEduPrograms($level->getField("fk_eiisEduProgram"));

                switch ($data["type"]) {
                    case "new-op":
                        $toa = "OPENING";
                        break;
                    case "current-op":
                        $toa = "ACTIVE";
                        break;
                    case "stop-op":
                        $toa = "CLOSING";
                        break;
                    case "change-op":
                        $toa = "EDITING";
                        break;
                }

                $fields = [
                    "requestOrganizationInfoId" => $data["orgId"],
                    "typeOfActivity" => $toa,
                    "educationProgramCatalogItemId" => $eduProgram->getId(),
                    "educationLevelId" => $level->getId(),
                    "eduProgramTypeId" => $eduProgram->getField("fk_eiisEduProgramType")
                ];

                $requestProgram = new Model\LodEntities\LicenseRequestEducationProgram();
                $requestProgram->setFields($fields);
                $requestProgram->save();

                //возвращаем
                $returnArray = array(
                    "result" => "success"
                );
            }
        } catch(\Exception $e){
            $returnArray["errorMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function getEpQualificationsAction(){
        $returnArray = array(
            "result" => "error",
            "error" => "Произошла ошибка"
        );
        try {
            $returnArray = array(
                "result" => "success",
                "list" => []
            );

            $epId = $this->params()->fromPost("epId");

            $mtmQualificationsTable = new Model\Gateway\LodEntitiesTable("EducationProgramCIToQualification");
            $mtm = $mtmQualificationsTable->getAllEntities(null,null,[
                "educationProgramId" => $epId
            ]);
            $qualIds = [];
            foreach($mtm as $link)
                $qualIds[] = $link->getField("qualificationId");


            if(count($qualIds) > 0) {
                $qualificationsTable = new Model\Gateway\EntitiesTable("isga_Qualifications");
                $qualifications = $qualificationsTable->getAllEntities(null, null, [
                    new \Zend\Db\Sql\Predicate\In("Id", $qualIds)
                ]);


                foreach ($qualifications as $qual) {
                    $returnArray["list"][] = array(
                        "Id" => $qual->getId(),
                        "Name" => $qual->getField("Name")
                    );
                }
            }
        } catch(\Exception $e){
            $returnArray["errorMsg"] = $e->getMessage();
        }
        echo json_encode($returnArray);
        die;
    }

    public function getPrescriptionsByOrgAction(){
        if ($this->params("id") != null) {
            if($this->params("id")=="undefined"){
                echo "Не выбрана организация";
                die;
            }
            $org = new Model\Entities\eiisEducationalOrganizations($this->params("id"));
            $prescriptions = $org->getPrescriptions();
            $prescrCount = count($prescriptions);

            if($prescrCount == 0){
                echo '<div>
                    <img src="/img/ok-green.png"> Неснятые с контроля предписания отсутствуют
                </div>';
            } else {
                echo '<div>
                    <img src="/img/alert-red.png"> По данным ИС АКНД ОУ организация имеет '.$prescrCount.' неснятых предписания.
                    <div class="btIn btShowPrescriptions" onclick="javascript:dialog_showPrescriptions(\''.$org->getId().'\');">Посмотреть</div>
                </div>';
            }
            die;
        }
        echo "<div>Error</div>";
        die;
    }

    /**
     * дергаем фио того кто должен прийти из електронной очереди
     * также определяем причины указаные в очереди
     * url: /islod/requests/get-fio-from-equeue/ид_конторы
     *  ид_конторы - ид организации
     *
     * ответ json
     *      ['data'][]['lastName']          - фамили
     *      ['data'][]['firstName']         - имя
     *      ['data'][]['middleName']        - отчесто
     *      ['data'][]['phone']             - имя
     *      ['data'][]['email']             - почта
     *      ['data'][]['lisensingProcedureTitle']   - наименование процедулв
     *      ['data'][]['lisensingProcedureId']      - ид процедуры
     *      ['data'][]['licenseReasonAllId'][]      - пеерчень причин
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function getFioFromEqueueAction(){
        $orgId = $this->params("id");
        $orgId = htmlentities($orgId);
        if( empty($orgId) ||  'undefined' == $orgId ){
            echo "Не выбрана организация";
            die;
        }
        $orgClass = new \Model\Entities\eiisEducationalOrganizations();
        $orgClass = $orgClass->getById($orgId);
        $inn = $orgClass->getFieldHtml('Inn');
        if( empty($inn) ){
            echo "Нет инн";
            die;
        }
        $where = new \Zend\Db\Sql\Where();
        $where->like( 'organizationInn', $inn);
        $where->greaterThanOrEqualTo( 'startReceptionTime',  date('Y-m-d') );
        $where->lessThan( 'endReceptionTime',  date('Y-m-d', time()+60*60*24) );
        $where->like('electronicQueueReasonId', '0d49ba7467504c43a17600691a198f23');

        $queueClassAll = new \Model\LodEntities\ElectronicQueueEntry();
        $queueClassAll = $queueClassAll->getAllByWhereParseId( $where );

        $result = array();
        $i = 0;
        if( !empty($queueClassAll) ){
            foreach ( $queueClassAll as $key => $queueClass ) {
                if( !empty($queueClass) ){
                        $result['data'][$i]['lastName']   = $queueClass->getFieldHtml('lastName');
                        $result['data'][$i]['firstName']  = $queueClass->getFieldHtml('firstName');
                        $result['data'][$i]['middleName'] = $queueClass->getFieldHtml('middleName');
                        $result['data'][$i]['phone']      = $queueClass->getFieldHtml('mobilePhoneNumber');
                        $result['data'][$i]['email']      = $queueClass->getFieldHtml('eMail');
                }

                //дергним причины указаные при записи очередь
                if( !empty($queueClass) ){
                    $toReason = new \Model\LodEntities\ElectronicQueueEntryToLicenseReasonCatalogItem();
                    $reasonAll = $toReason
                        ->getAllByWhereParseId(
                            array( 'electronicQueueEntryId' =>  $queueClass->getId() ),
                            'licenseReasonCatalogItemId'
                        );
                }
                //найдем причины
                if( !empty($reasonAll) ){
                    $reasonAllId = array_keys($reasonAll);
                }
                if( !empty($reasonAllId ) ){
                    $reasonClass = new \Model\LodEntities\LicenseReasonCatalogItem();
                    $reasonClass = $reasonClass->getByWhere(array($reasonClass->getIdName() => reset($reasonAllId)));
                }
                if( !empty($reasonClass) ){
                    $procedureClass = new \Model\LodEntities\LicensingProcedure();
                    $procedureClass = $procedureClass->getByWhere(array(
                        $procedureClass->getIdName() => $reasonClass->getFieldSafe('licensingProcedureId')
                    ) );
                }
                //найдем процедуру
                if( !empty($procedureClass) ){
                    $procedureId = $procedureClass->getId();
                    $result['data'][$i]['lisensingProcedureTitle'] = $procedureClass->getFieldHtml('title');
                    $result['data'][$i]['lisensingProcedureId'] = $procedureId;
                    $result['data'][$i]['licenseReasonAllId'] = $reasonAllId;
                }
                $i++;
            }
        }
        //готово
        return new \Zend\View\Model\JsonModel($result);
    }

    /**
     * Метод возвращает контент для диалога с предписаниями
     */
    public function getOrgPrescriptionsAction(){
        if ($this->params("id") != null) {
            $org = new Model\Entities\eiisEducationalOrganizations($this->params("id"));
            $prescriptions = $org->getPrescriptions();

            if ($this->params("id2") != null && $this->params("id2")=='print'){
                \Ron\Model\Declaration\Prescription::printed($prescriptions,$org);
            }

            $view = new ViewModel();
            $view->setVariables(array(
                "prescriptions" => $prescriptions,
                'orgId' => $org->getId(),
            ));
            $view->setTerminal(true);
            return $view;
        }
        echo "<div>Error</div>";
        die;
    }

    public function getLicenseDataByOrgAction(){
        if ($this->params("id") != null) {
            $org = new Model\Entities\eiisEducationalOrganizations($this->params("id"));
            $license = $org->getLicense();

            $return = "Данные по лицензии не найдены";
            if($license != null){
                $return = '
                <div class="right-value" onclick="javascript:license._get_license_info(\''.$license->getId().'\';">
                    <span class="license_link" id="l_'.$license->getId().'">
                        № '.$license->getField("LicenseRegNum").' от '.$license->getDate("DateLicDoc").'г. серия '.$license->getField("SerDoc").' № '.$license->getField("NumDoc").'
                    </span>
                </div>';
            }

            echo $return;
            die;
        }
        echo "<div>Error</div>";
        die;
    }

    /**
     * Метод возвращает список мест од, чтобы обновить после сохранения
     */
    public function getPlacesOdAction(){
        echo "<div>Error</div>";
        die;
    }
}
