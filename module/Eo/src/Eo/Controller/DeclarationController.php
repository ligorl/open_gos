<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Eo\EloquentModel\IsgaNotification;
use Ron\EloquentModel\ApplicationExtraDocument;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Model;
use Eo\EloquentModel\ApplicationFileCorrupt;

class DeclarationController extends AbstractActionController
{

    public $months = array(
        '01' => 'января',
        '02' => 'февраля',
        '03' => 'марта',
        '04' => 'апреля',
        '05' => 'мая',
        '06' => 'июня',
        '07' => 'июля',
        '08' => 'августа',
        '09' => 'сентября',
        '10' => 'октября',
        '11' => 'ноября',
        '12' => 'декабря'
    );

    /*
      public $brunches = array (
      'DublicateCertificate' => "",
      'Accreditation' => "",
      'Certificate' => ""

      );
     *
     */
    /*
      public $tables = array (
      'Accreditation' => "",
      'Certificate' => ""
      );
     */
    public function indexAction()
    {
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/eo/excerpt.js');
        $headStuff->appendFile('/js/eo/main.js');
        $headStuff->appendFile('/js/tipsy/jquery.tipsy.js');
        $headStuff->appendFile('/js/eo/declaration/declaration.js');
        $headStuff->appendFile('/js/eo/declaration/declaration.deprivation.js');
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        $headStuff->appendStylesheet('/js/tipsy/tipsy.css');

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;
        $view = new ViewModel();
        return $view;
    }

    public function sentAction()
    {
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/eo/main.js');
        $search = $this->params()->fromPost();
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $id_org = $user->getField("fk_eiisEducationalOrganization");
        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;
        $application_class = new Model\Entities\isgaApplications();
        $appClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $applicationType_class = new Model\Entities\isgaApplicationTypes();
        $applicationReason_class = new Model\Entities\isgaApplicationReasons();
        $mtmApplicationReason_class = new Model\Entities\isgamtmApplicationReason();

        $applicationTypes = array();
        $applicationReasons = array();

        if ($search) {
            $str_reason = '';
            if (!empty($search["reason"])) {
                $str_reason = ' AND (isga_Applications.fk_isgaApplicationReason = "' . $search["reason"] . '" OR isga_mtm_Application_Reason.fk_isgaApplicationReason = "' . $search["reason"] . '")';
            } else {
                if (!empty($search["type"])) {
                    $reason_arr = $applicationReason_class->getAllByWhere('fk_isgaApplicationType = "' . $search["type"] . '"');
                    if ($reason_arr) {
                        $list_reason = array();
                        foreach ($reason_arr as $reasonOne) {
                            if (!in_array($reasonOne->getField('Id'), $list_reason)) {
                                array_push($list_reason, $reasonOne->getField('Id'));
                            }
                        }
                        $list_reason = '"' . implode('", "', $list_reason) . '"';
                        $str_reason = ' AND (isga_Applications.fk_isgaApplicationReason IN (' . $list_reason . ') OR isga_mtm_Application_Reason.fk_isgaApplicationReason IN (' . $list_reason . '))';
                    }
                }
            }
            $str_date = '1';
            //под дате заполнения с
            if (!empty($search["dateBegin"])) {
                $date = date('Y-m-d', strtotime($search["dateBegin"]));
                if (!empty($date)) {
                    $str_date = 'isga_Applications.DateReg >= "' . $date . '"';
                }
            }
            //под дате заполнения по
            if (!empty($search["dateEnd"])) {
                $date = date('Y-m-d', strtotime($search["dateEnd"]));
                if (!empty($date)) {
                    if ($str_date != '1')
                        $str_date .= ' AND isga_Applications.DateReg <= "' . $date . '"';
                    else
                        $str_date = 'isga_Applications.DateReg <= "' . $date . '"';
                }
            }
            $where = '`eiis_Licenses`.`fk_eiisEducationalOrganization` = "' . $id_org . '" AND
                `isga_Applications`.`fk_isgaApplicationStatus` IS NOT NULL AND
                `isga_Applications`.`fk_isgaApplicationStatus` != "2D675622-1E14-4063-AD11-B647B836746D" AND
                `isga_Applications`.`fk_isgaApplicationStatus` != "A3B952BF-628E-446C-B865-E483503241E8" AND
                `isga_Applications`.`fk_isgaApplicationReason` IS NOT NULL AND
                `isga_Applications`.`fk_isgaApplicationReason` NOT LIKE "" AND
                `isga_Applications`.`fk_isgaApplicationReason` NOT LIKE "NULL" AND
                ( ' . $str_date . ' ' . $str_reason . ' ) ';
            $joins = array(
                '0' => array(
                    'name' => 'eiis_Licenses',
                    'where' => 'isga_Applications.fk_eiisLicense = eiis_Licenses.Id',
                    'column' => array('fk_eiisEducationalOrganization')
                ),
                '1' => array(
                    'name' => 'isga_mtm_Application_Reason',
                    'where' => 'isga_Applications.Id = isga_mtm_Application_Reason.fk_isgaApplication'
                )
            );
            $applications = $application_class->selectComplicatedManyJoin(
                'isga_Applications',
                array('Id', 'DateReg', 'RegNumber', 'fk_eiisLicense',
                    'fk_isgaApplicationReason',
                    'fk_eiisEducationalOrganization',
                    'fk_isgaApplicationStatus' => new \Zend\Db\Sql\Expression('upper(fk_isgaApplicationStatus)'),
                ),
                $where, 'isga_Applications.Id', 'DateReg DESC', 100, 0, $joins
            )->toArray();
            $statusesIds = array_unique(array_column($applications, 'fk_isgaApplicationStatus'));

            $statusesArr = array();
            if ($statusesIds) {
                $statusesArrQuest = $application_class->getApplicationStatus($statusesIds);
                //заполняем ванианты статусов заявлений
                if (!empty($statusesArrQuest)) {
                    foreach ($statusesArrQuest as $statusesItem) {
                        $statusesArr[$statusesItem['Id']] = (empty($statusesItem['IsgaTextForPublication']) || strtolower($statusesItem['IsgaTextForPublication']) == 'null') ? $statusesItem['Name'] : $statusesItem['IsgaTextForPublication'];
                    }
                }
                //$statusesArr = array_column($statusesArr, 'IsgaTextForPublication', 'Id');
            } else {
                $statusesArr = array();
            }
        } else {
            $applications = $application_class->selectComplicated(
                'isga_Applications',
                array('Id'
                , 'DateReg'
                , 'RegNumber',
                    'fk_eiisLicense',
                    'fk_isgaApplicationReason',
                    'fk_eiisEducationalOrganization',
                    'fk_isgaApplicationStatus' => new \Zend\Db\Sql\Expression('upper(fk_isgaApplicationStatus)'),
                    // 'fk_isgaApplicationStatus'
                ),
                '`eiis_Licenses`.`fk_eiisEducationalOrganization` = "' . $id_org . '" AND
                `isga_Applications`.`fk_isgaApplicationStatus` IS NOT NULL AND
                `isga_Applications`.`fk_isgaApplicationStatus` != "2D675622-1E14-4063-AD11-B647B836746D" AND
                `isga_Applications`.`fk_isgaApplicationStatus` != "A3B952BF-628E-446C-B865-E483503241E8" AND
                `isga_Applications`.`fk_isgaApplicationReason` IS NOT NULL AND
                `isga_Applications`.`fk_isgaApplicationReason` NOT LIKE "" AND
                `isga_Applications`.`fk_isgaApplicationReason` NOT LIKE "NULL"',
                NULL, 'DateReg DESC', 100, 0, 'eiis_Licenses',
                'isga_Applications.fk_eiisLicense = eiis_Licenses.Id',
                array('fk_eiisEducationalOrganization')
            )->toArray();
            $statusesIds = array_unique(array_column($applications, 'fk_isgaApplicationStatus'));

            $statusesArr = array();
            if ($statusesIds) {
                $statusesArrQuest = $application_class->getApplicationStatus($statusesIds);
                //заполняем ванианты статусов заявлений
                if (!empty($statusesArrQuest)) {
                    foreach ($statusesArrQuest as $statusesItem) {
                        $statusesArr[$statusesItem['Id']] = (empty($statusesItem['IsgaTextForPublication']) || strtolower($statusesItem['IsgaTextForPublication']) == 'null') ? $statusesItem['Name'] : $statusesItem['IsgaTextForPublication'];
                    }
                }
                //$statusesArr = array_column($statusesArr, 'IsgaTextForPublication', 'Id');
            } else {
                $statusesArr = array();
            }

            if ($applications) {
                $fk_isgaApplicationReasonIds = array();
                $fk_isgaApplicationIds = array();
                $where = null;
                foreach ($applications as $application) {
                    if (!in_array($application['fk_isgaApplicationReason'], $fk_isgaApplicationReasonIds))
                        array_push($fk_isgaApplicationReasonIds, $application['fk_isgaApplicationReason']);
                    if (!in_array($application['Id'], $fk_isgaApplicationIds) && $application['fk_isgaApplicationReason'] == '1')
                        array_push($fk_isgaApplicationIds, $application['Id']);
                }
                if ($fk_isgaApplicationIds) {
                    $reasonsAppl = $mtmApplicationReason_class->selectComplicated('isga_mtm_Application_Reason',
                        array('fk_isgaApplicationReason'),
                        array('fk_isgaApplication' => $fk_isgaApplicationIds))->toArray();
                    if (!empty($reasonsAppl)) {
                        $reasons_id = array_column($reasonsAppl, 'fk_isgaApplicationReason');
                        if (!empty($reasons_id)) {
                            $fk_isgaApplicationReasonIds = array_merge($fk_isgaApplicationReasonIds, $reasons_id);
                            $fk_isgaApplicationReasonIds = array_unique($fk_isgaApplicationReasonIds);
                        }
                    }
                }
                if (!empty($fk_isgaApplicationReasonIds))
                    $where = array('Id' => $fk_isgaApplicationReasonIds);
                $applicationReasons = $applicationReason_class->getAllByWhereParseId($where, 'Id');
                $fk_isgaApplicationTypeIds = array();
                $where2 = null;
                if (!empty($applicationReasons)) {
                    foreach ($applicationReasons as $id => $reason) {
                        if (!in_array($reason->getFieldHtml('fk_isgaApplicationType'), $fk_isgaApplicationTypeIds))
                            array_push($fk_isgaApplicationTypeIds, $reason->getFieldHtml('fk_isgaApplicationType'));
                    }
                    $where2 = array('Id' => $fk_isgaApplicationTypeIds);
                }
                $applicationTypes = $applicationType_class->getAllByWhereParseId($where2, 'Id');
            }
        }
        $view = new ViewModel();
        $view->setVariables(array(
            "applications" => $applications,
            "search" => $search,
            "applicationClass" => $appClass,
            'applicationBranches' => $application_class->getApplicationBranches($applications),
            'applicationStatuses' => $statusesArr,
            "applicationTypes" => $applicationTypes,
            'applicationReasons' => $applicationReasons,
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function rectificationAction()
    {
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $page = ($this->params('id') != NULL) ? $this->params('id') : 0;
        $search = $this->params()->fromPost();
        $headStuff->appendFile('/js/eo/main.js');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $id_org = $user->getField("fk_eiisEducationalOrganization");
        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;
        $application_class = new Model\Entities\isgaApplications();
        $appClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $applicationType_class = new Model\Entities\isgaApplicationTypes();
        $applicationReason_class = new Model\Entities\isgaApplicationReasons();
        $mtmApplicationReason_class = new Model\Entities\isgamtmApplicationReason();

        $applicationTypes = array();
        $applicationReasons = array();

        if ($search) {
            $str_reason = '';
            if (!empty($search["reason"])) {
                $str_reason = ' AND (isga_Applications.fk_isgaApplicationReason = "' . $search["reason"] . '" OR isga_mtm_Application_Reason.fk_isgaApplicationReason = "' . $search["reason"] . '")';
            } else {
                if (!empty($search["type"])) {
                    $reason_arr = $applicationReason_class->getAllByWhere('fk_isgaApplicationType = "' . $search["type"] . '"');
                    if ($reason_arr) {
                        $list_reason = array();
                        foreach ($reason_arr as $reasonOne) {
                            if (!in_array($reasonOne->getField('Id'), $list_reason)) {
                                array_push($list_reason, $reasonOne->getField('Id'));
                            }
                        }
                        $list_reason = '"' . implode('", "', $list_reason) . '"';
                        $str_reason = ' AND (isga_Applications.fk_isgaApplicationReason IN (' . $list_reason . ') OR isga_mtm_Application_Reason.fk_isgaApplicationReason IN (' . $list_reason . '))';
                    }
                }
            }
            $str_date = '1';
            //под дате заполнения с
            if (!empty($search["dateBegin"])) {
                $date = date('Y-m-d', strtotime($search["dateBegin"]));
                if (!empty($date)) {
                    $str_date = 'isga_Applications.DateReg >= "' . $date . '"';
                }
            }
            //под дате заполнения по
            if (!empty($search["dateEnd"])) {
                $date = date('Y-m-d', strtotime($search["dateEnd"]));
                if (!empty($date)) {
                    if ($str_date != '1')
                        $str_date .= ' AND isga_Applications.DateReg <= "' . $date . '"';
                    else
                        $str_date = 'isga_Applications.DateReg <= "' . $date . '"';
                }
            }
            $where = 'eiis_Licenses.fk_eiisEducationalOrganization = "' . $id_org . '" AND
                (isga_Applications.fk_isgaApplicationStatus = "2D675622-1E14-4063-AD11-B647B836746D") AND
                ( ' . $str_date . ' ' . $str_reason . ' ) ';

            $joins = array(
                '0' => array(
                    'name' => 'eiis_Licenses',
                    'where' => 'isga_Applications.fk_eiisLicense = eiis_Licenses.Id',
                    'column' => array('fk_eiisEducationalOrganization')
                ),
                '1' => array(
                    'name' => 'isga_mtm_Application_Reason',
                    'where' => 'isga_Applications.Id = isga_mtm_Application_Reason.fk_isgaApplication'
                )
            );
            $applications = $application_class->selectComplicatedManyJoin(
                'isga_Applications',
                array('Id', 'DateReg', 'RegNumber', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization', 'fk_isgaApplicationStatus'),
                $where, 'isga_Applications.Id', 'DateReg DESC', 50, $page, $joins
            )->toArray();
            $AllApplications = $application_class->selectComplicatedManyJoin(
                'isga_Applications',
                array('Id', 'DateReg', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization', 'fk_isgaApplicationStatus'),
                $where, 'isga_Applications.Id', 'DateReg DESC', NULL, NULL, $joins
            )->toArray();
            $allPage = ceil(count($AllApplications) / 50);
        } else {
            $where = 'eiis_Licenses.fk_eiisEducationalOrganization = "' . $id_org . '" AND (isga_Applications.fk_isgaApplicationStatus = "2D675622-1E14-4063-AD11-B647B836746D")';

            $applications = $application_class->selectComplicated(
                'isga_Applications',
                array('Id', 'DateReg', 'RegNumber', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization', 'fk_isgaApplicationStatus'),
                $where, NULL, 'DateReg DESC', 50, $page, 'eiis_Licenses',
                'isga_Applications.fk_eiisLicense = eiis_Licenses.Id', array('fk_eiisEducationalOrganization')
            )->toArray();
            $AllApplications = $application_class->selectComplicated(
                'isga_Applications',
                array('Id', 'DateReg', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization', 'fk_isgaApplicationStatus'),
                $where, NULL, 'DateReg DESC', NULL, NULL, 'eiis_Licenses',
                'isga_Applications.fk_eiisLicense = eiis_Licenses.Id', array('fk_eiisEducationalOrganization')
            )->toArray();
            $allPage = ceil(count($AllApplications) / 50);

            if ($applications) {
                $fk_isgaApplicationReasonIds = array();
                $fk_isgaApplicationIds = array();
                $where = null;
                foreach ($applications as $application) {
                    if (!in_array($application['fk_isgaApplicationReason'], $fk_isgaApplicationReasonIds))
                        array_push($fk_isgaApplicationReasonIds, $application['fk_isgaApplicationReason']);
                    if (!in_array($application['Id'], $fk_isgaApplicationIds) && $application['fk_isgaApplicationReason'] == '1')
                        array_push($fk_isgaApplicationIds, $application['Id']);
                }
                if ($fk_isgaApplicationIds) {
                    $reasonsAppl = $mtmApplicationReason_class->selectComplicated('isga_mtm_Application_Reason',
                        array('fk_isgaApplicationReason'),
                        array('fk_isgaApplication' => $fk_isgaApplicationIds))->toArray();
                    if (!empty($reasonsAppl)) {
                        $reasons_id = array_column($reasonsAppl, 'fk_isgaApplicationReason');
                        if (!empty($reasons_id)) {
                            $fk_isgaApplicationReasonIds = array_merge($fk_isgaApplicationReasonIds, $reasons_id);
                            $fk_isgaApplicationReasonIds = array_unique($fk_isgaApplicationReasonIds);
                        }
                    }
                }
                if (!empty($fk_isgaApplicationReasonIds))
                    $where = array('Id' => $fk_isgaApplicationReasonIds);
                $applicationReasons = $applicationReason_class->getAllByWhereParseId($where, 'Id');
                $fk_isgaApplicationTypeIds = array();
                $where2 = null;
                if (!empty($applicationReasons)) {
                    foreach ($applicationReasons as $id => $reason) {
                        if (!in_array($reason->getFieldHtml('fk_isgaApplicationType'), $fk_isgaApplicationTypeIds))
                            array_push($fk_isgaApplicationTypeIds, $reason->getFieldHtml('fk_isgaApplicationType'));
                    }
                    $where2 = array('Id' => $fk_isgaApplicationTypeIds);
                }
                $applicationTypes = $applicationType_class->getAllByWhereParseId($where2, 'Id');
            }
        }
        $view = new ViewModel();
        if ($page != 0) {
            $view->setTemplate('/eo/declaration/newsearch.phtml');
        }
        $view->setVariables(array(
            "nextPage" => $page + 1,
            "allPage" => $allPage,
            "search" => $search,
            "applications" => $applications,
            "applicationClass" => $appClass,
            "applicationTypes" => $applicationTypes,
            'applicationReason' => $applicationReasons,
            'applicationBranches' => $application_class->getApplicationBranches($applications)
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * По заявлению в базе ищем замечания о несоответствии
     * закидываем в JSON массив и на выход
     */
    public function notifyCommentsGetAction()
    {
        //берем заявление
        $appId = $this->params('id');
        if (empty($appId)) {
            die;
        }
        //$notify = new \Model\Entities\isgaApplicationNotify();
        //делаем выборку нужных
        $docGroupGroup = new \Model\Entities\isgaApplicationDocumentGroupGroupStatusReason();
        $reasons = $docGroupGroup->getReasons($appId);
        $reasonsOut = array();
        if ($reasons) {
            $reasons = $reasons->toArray();
            //раскидываем результаты выборке
            foreach ($reasons as $value) {
                if (isset($value['Comment'])) {
                    $reasonsOut[]['comment'] = $value['Comment'];
                } elseif (isset($value ['fk_isgaApplicationDocumentGroupStatusReasons'])) {
                    $reasonsOut[]['reference'] = $value ['fk_isgaApplicationDocumentGroupStatusReasons'];
                } else
                    continue;
            }
        }
        echo json_encode($reasonsOut);
        die;
    }

    /**
     * отдаем вкладку форимирования несоответствий
     *
     * @return ViewModel
     */
    public function notifyTabAction()
    {

        $applicationId = $this->params('id');
        //$user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $application = \Model\Entities\isgaApplications($applicationId);
        if (!$application) {
            die;
        }
        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setVariable('applicationId', $applicationId);
        $view->setVariable('RegNumber', $application->getField('RegNumber'));
        return $view;
    }

    public function notificationAction()
    {
        $isSearch = ($this->params('id') != NULL) ? 1 : 0;
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/eo/main.js');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;

        $id_org = $user->getField("fk_eiisEducationalOrganization");
        $where = [
            'isga_Applications.fk_eiisEducationalOrganization' => $id_org,
            'isga_Notification.DateSigning IS NOT NULL'
        ];

        if ($isSearch) {
            $search = $this->params()->fromPost();
            (!empty($search['regNumber'])) ? $where[] = 'isga_Applications.RegNumber LIKE "%' . $search['regNumber'] . '%"' : false;
            (!empty($search['status'])) ? $where['isga_Applications.fk_isgaApplicationStatus'] = $search['status'] : false;

            // Дата формирования от
            if (!empty($search["dateBegin"])) {
                $date = date('Y-m-d', strtotime($search["dateBegin"]));
                (!empty($date)) ? $where[] = 'isga_Applications.DateCreate >= "' . $date . '"' : false;
            }

            // Дата формирования до
            if (!empty($search["dateEnd"])) {
                $date = date('Y-m-d', strtotime($search["dateEnd"]));
                (!empty($date)) ? $where[] = 'isga_Applications.DateCreate <= "' . $date . '"' : false;
            }

        }

        $application_class = new Model\Entities\isgaApplications();
        $applications = $application_class->selectComplicated(
            'isga_Applications',
            array('Id',
                'ApplicationNumber' => 'RegNumber',
                'fk_eiisEducationalOrganization',
                'fk_isgaApplicationStatus'
            ),
            $where,
            NULL, 'DateCreation DESC', 100, 0,
            'isga_Notification', 'isga_Applications.Id = isga_Notification.fk_isgaApplication',
            array('DateCreation', 'DateSigning', 'DateSending', 'NotifyNumber' => 'Number', 'fk_isgaNotificationTemplate')
        )->toArray();

        //отбираем нужные наименованияя типов]
        if (count($applications) > 0) {
            $notificationTamplateId = array_column($applications, 'fk_isgaNotificationTemplate');
            $notificationTamplateId = array_unique($notificationTamplateId);

            $applicationTamplets = $application_class->selectComplicated(
                'isga_NotificationTemplates',
                array('Id', 'isga_DocumentType'),
                array(
                    'Id' => $notificationTamplateId
                )
            )->toArray();
            $applicationTamplets = array_column($applicationTamplets, 'isga_DocumentType', 'Id');

            $applicationStatusId = array_column($applications, 'fk_isgaApplicationStatus');
            $applicationStatusId = array_unique($applicationStatusId);

            $applicationStatus = $application_class->selectComplicated(
                'isga_ApplicationStatuses',
                array('Id', 'IsgaTextForPublication'),
                array(
                    'Id' => $applicationStatusId
                )
            )->toArray();
            $applicationStatus = array_column($applicationStatus, 'IsgaTextForPublication', 'Id');

            $applicationsId = array_column($applications, 'Id');
            $applicationsId = array_unique($applicationsId);
            $applicationFiles = $application_class->selectComplicated(
                'isga_ApplicationFiles',
                array('fk_isgaApplication', 'fk_isgaDocumentType', 'HashName'),
                array(
                    'fk_isgaApplication' => $applicationsId
                )
            )->toArray();
            $applicationFilesArr = array();
            foreach ($applicationFiles as $file) {
                $applicationFilesArr[strtoupper($file['fk_isgaApplication'])][strtoupper($file['fk_isgaDocumentType'])] = $file['HashName'];
            }
        } else {
            $applicationStatus = array();
            $applicationFilesArr = array();
            $applicationTamplets = array();
        }
        $applicationStatus = array_change_key_case($applicationStatus, CASE_UPPER);
        $applicationTamplets = array_change_key_case($applicationTamplets, CASE_UPPER);

        $view = new ViewModel();
        // редирект на отрисовку новой таблицы.
        if ($isSearch) {
            $view->setTemplate('/eo/declaration/notification-search.phtml');
        }
        $view->setVariables(array(
            'applications' => $applications,
            'applicationStatus' => $applicationStatus,
            'applicationFiles' => $applicationFilesArr,
            'applicationTamplets' => $applicationTamplets,
        ));
        $view->setTerminal(true);

        return $view;
    }

    public function newAction()
    {
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $page = ($this->params('id') != NULL) ? $this->params('id') : 0;
        $search = $this->params()->fromPost();
        $headStuff->appendFile('/js/eo/main.js');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $id_org = $user->getField("fk_eiisEducationalOrganization");
        $application_class = new Model\Entities\isgaApplications();
        $appClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $applicationType_class = new Model\Entities\isgaApplicationTypes();
        $applicationReason_class = new Model\Entities\isgaApplicationReasons();
        $mtmApplicationReason_class = new Model\Entities\isgamtmApplicationReason();

        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;
        $applicationTypes = array();
        $applicationReasons = array();
        if ($search) {
            $str_reason = '';
            if (!empty($search["reason"])) {
                $str_reason = ' AND (isga_Applications.fk_isgaApplicationReason = "' . $search["reason"] . '" OR isga_mtm_Application_Reason.fk_isgaApplicationReason = "' . $search["reason"] . '")';
            } else {
                if (!empty($search["type"])) {
                    $reason_arr = $applicationReason_class->getAllByWhere('fk_isgaApplicationType = "' . $search["type"] . '"');
                    if ($reason_arr) {
                        $list_reason = array();
                        foreach ($reason_arr as $reasonOne) {
                            if (!in_array($reasonOne->getField('Id'), $list_reason)) {
                                array_push($list_reason, $reasonOne->getField('Id'));
                            }
                        }
                        $list_reason = '"' . implode('", "', $list_reason) . '"';
                        $str_reason = ' AND (isga_Applications.fk_isgaApplicationReason IN (' . $list_reason . ') OR isga_mtm_Application_Reason.fk_isgaApplicationReason IN (' . $list_reason . '))';
                    }
                }
            }
            $str_date = '1';
            //под дате заполнения с
            if (!empty($search["dateBegin"])) {
                $date = date('Y-m-d', strtotime($search["dateBegin"]));
                if (!empty($date)) {
                    $str_date = 'isga_Applications.DateCreate >= "' . $date . '"';
                }
            }
            //под дате заполнения по
            if (!empty($search["dateEnd"])) {
                $date = date('Y-m-d', strtotime($search["dateEnd"]));
                if (!empty($date)) {
                    if ($str_date != '1')
                        $str_date .= ' AND isga_Applications.DateCreate <= "' . $date . '"';
                    else
                        $str_date = 'isga_Applications.DateCreate <= "' . $date . '"';
                }
            }
            $where = 'eiis_Licenses.fk_eiisEducationalOrganization = "' . $id_org . '" AND (isga_Applications.fk_isgaApplicationStatus = "A3B952BF-628E-446C-B865-E483503241E8" OR isga_Applications.fk_isgaApplicationStatus IS NULL) AND ( ' . $str_date . ' ' . $str_reason . ' ) ';

            $joins = array(
                '0' => array(
                    'name' => 'eiis_Licenses',
                    'where' => 'isga_Applications.fk_eiisLicense = eiis_Licenses.Id',
                    'column' => array('fk_eiisEducationalOrganization')
                ),
                '1' => array(
                    'name' => 'isga_mtm_Application_Reason',
                    'where' => 'isga_Applications.Id = isga_mtm_Application_Reason.fk_isgaApplication'
                )
            );
            $applications = $application_class->selectComplicatedManyJoin(
                'isga_Applications',
                array('Id', 'DateCreate', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization'),
                $where, 'isga_Applications.Id', 'DateCreate DESC', 50, $page, $joins
            )->toArray();
            $AllApplications = $application_class->selectComplicatedManyJoin(
                'isga_Applications',
                array('Id', 'DateCreate', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization'),
                $where, 'isga_Applications.Id', 'DateCreate DESC', NULL, NULL, $joins
            )->toArray();
            $allPage = ceil(count($AllApplications) / 50);
        } else {
            $search = false;
            $where = 'eiis_Licenses.fk_eiisEducationalOrganization = "' . $id_org . '" AND (isga_Applications.fk_isgaApplicationStatus = "A3B952BF-628E-446C-B865-E483503241E8" OR isga_Applications.fk_isgaApplicationStatus IS NULL)';

            $applications = $application_class->selectComplicated(
                'isga_Applications',
                array('Id', 'DateCreate', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization'),
                $where, NULL, 'DateCreate DESC', 50, $page, 'eiis_Licenses',
                'isga_Applications.fk_eiisLicense = eiis_Licenses.Id', array('fk_eiisEducationalOrganization')
            )->toArray();
            $AllApplications = $application_class->selectComplicated(
                'isga_Applications',
                array('Id', 'DateCreate', 'fk_eiisLicense', 'fk_isgaApplicationReason', 'fk_eiisEducationalOrganization'),
                $where, NULL, 'DateCreate DESC', NULL, NULL, 'eiis_Licenses',
                'isga_Applications.fk_eiisLicense = eiis_Licenses.Id', array('fk_eiisEducationalOrganization')
            )->toArray();
            $allPage = ceil(count($AllApplications) / 50);

            if ($applications) {
                $fk_isgaApplicationReasonIds = array();
                $fk_isgaApplicationIds = array();
                $where = null;
                foreach ($applications as $application) {
                    if (!in_array($application['fk_isgaApplicationReason'], $fk_isgaApplicationReasonIds))
                        array_push($fk_isgaApplicationReasonIds, $application['fk_isgaApplicationReason']);
                    if (!in_array($application['Id'], $fk_isgaApplicationIds) && $application['fk_isgaApplicationReason'] == '1')
                        array_push($fk_isgaApplicationIds, $application['Id']);
                }
                if ($fk_isgaApplicationIds) {
                    $reasonsAppl = $mtmApplicationReason_class->selectComplicated('isga_mtm_Application_Reason',
                        array('fk_isgaApplicationReason'),
                        array('fk_isgaApplication' => $fk_isgaApplicationIds))->toArray();
                    if (!empty($reasonsAppl)) {
                        $reasons_id = array_column($reasonsAppl, 'fk_isgaApplicationReason');
                        if (!empty($reasons_id)) {
                            $fk_isgaApplicationReasonIds = array_merge($fk_isgaApplicationReasonIds, $reasons_id);
                            $fk_isgaApplicationReasonIds = array_unique($fk_isgaApplicationReasonIds);
                        }
                    }
                }
                if (!empty($fk_isgaApplicationReasonIds))
                    $where = array('Id' => $fk_isgaApplicationReasonIds);
                $applicationReasons = $applicationReason_class->getAllByWhereParseId($where, 'Id');
                $fk_isgaApplicationTypeIds = array();
                $where2 = null;
                if (!empty($applicationReasons)) {
                    foreach ($applicationReasons as $id => $reason) {
                        if (!in_array($reason->getFieldHtml('fk_isgaApplicationType'), $fk_isgaApplicationTypeIds))
                            array_push($fk_isgaApplicationTypeIds, $reason->getFieldHtml('fk_isgaApplicationType'));
                    }
                    $where2 = array('Id' => $fk_isgaApplicationTypeIds);
                }
                $applicationTypes = $applicationType_class->getAllByWhereParseId($where2, 'Id');
            }
        }

        $view = new ViewModel();
        if ($page != 0) {
            $view->setTemplate('/eo/declaration/newsearch.phtml');
        }

        $view->setVariables(array(
            "nextPage" => $page + 1,
            "search" => $search,
            "allPage" => $allPage,
            "applications" => $applications,
            "applicationClass" => $appClass,
            "applicationTypes" => $applicationTypes,
            'applicationReasons' => $applicationReasons,
            'applicationBranches' => $application_class->getApplicationBranches($applications)
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * открытая - акредитация - заявления - редактировать карточку заявления
     * url:  /eo/declaration/edit/ид-за-ния
     * view: /module/Eo/view/eo/declaration/edit.phtml
     *
     * @return ViewModel
     */
    public function editAction()
    {
        $id = $this->params('id');
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/eo/main.js');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->educationOrganization = $educationOrganization;
        $this->layout()->identityUser = $user;
        $id_org = $user->getField("fk_eiisEducationalOrganization");
        $currentReorg = NULL;
        $view = new ViewModel();
        $deprivation = false;
        $removal = false;
        if (!empty($id)) {
            $reasons_class = new Model\Gateway\EntitiesTable('isga_ApplicationReasons');
            $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
            $currentApplication = $applicationsClass->getEntityById($id);
            $ApplicationDocumentGroups = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentGroups');
            $MoreInfo = $currentApplication->getLinkedItem(
                'isga_ApplicationMoreInfo',
                array(
                    'fk_isgaApplication' => $currentApplication->getId(),
                    'fk_eiisEducationalOrganization' => $educationOrganization->getId(),
                )
            );
            if (empty($MoreInfo)) {
                $MoreInfo = $currentApplication->getLinkedItem(
                    'isga_ApplicationMoreInfo',
                    array(
                        'fk_isgaApplication' => $currentApplication->getField("Id")
                    )
                );
            }

            $currentType = $currentApplication->getType();
            $currentReason = $currentApplication->getNamesReasons();
            $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
                array('fk_eiisEducationalOrganization'),
                array('fk_isgaApplication' => $id))->toArray();
            $reasons = $reasons_class->getEntitiesByWhere('`fk_isgaApplicationType` = "' . $currentType->getField("Id") . '" ORDER BY `Name` ASC');

            if (count($ReorgOrganizations) > 0) {
                $idsReorg = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
                $currentReorg = $currentApplication->selectComplicated('eiis_EducationalOrganizations',
                    array('FullName'), array('Id' => $idsReorg))->toArray();
                $currentReorg = implode(',', array_column($currentReorg, 'FullName'));
            }
            $copyApplicationDoc = $ApplicationDocumentGroups->getEntitiesByWhere('`fk_isgaApplication` = "' . $id . '" AND `fk_isgaApplicationDocumentType` = "0B4B6324-5DAE-4AB8-B973-86C4A39409E1" AND `isArhive` = 0')->current();
            if ($copyApplicationDoc) {
                $copyApplicationDoc = $copyApplicationDoc->toArray();
            }
            $currentLicense = $currentApplication->getLinkedItem('eiis_Licenses',
                array('Id' => $currentApplication->getField("fk_eiisLicense")));
            $currentCert = $currentApplication->getLinkedItem('isga_Certificates',
                array('Id' => $currentApplication->getField("fk_isgaPreviousCertificate")));
            $fillialsApplication_class = new Model\Gateway\EntitiesTable('isga_mtm_Application_Branches');
            $fillialsApplication = $fillialsApplication_class->getEntitiesByWhere(array('fk_isgaApplication' => $id));
            $max_show_tab = (count($fillialsApplication)) ? 1 : 0;
            if ($currentApplication->getLinkedItem('isga_ApplicationFiles', array('fk_isgaApplication' => $id))) {
                $max_show_tab = 3;
            } elseif ($currentApplication->getLinkedItems('isga_ApplicationPrograms', array('fk_isgaApplication' => $id))) {
                $max_show_tab = 2;
            }
            if (isset($idsReorg) && count($idsReorg) > 0) {
                $textReorgIds = implode(',', $idsReorg);
            } else {
                $textReorgIds = '';
            }
            $deprivation = $currentApplication->checkReasonInApplication("39788d86-0f59-489d-b43d-e4557aa6d466");
            $removal = $currentApplication->checkReasonInApplication("512C17E5-CC59-4373-B6CB-C0C07F93DACF");
            $view->setVariables(array(
                "reasons" => $reasons,
                "currentCert" => $currentCert,
                "currentLicense" => $currentLicense,
                "copyApplicationDoc" => $copyApplicationDoc,
                "currentType" => $currentType,
                "currentReason" => $currentReason,
                "currentReorg" => $currentReorg,
                "idsReorg" => $textReorgIds,
                "moreInfo" => $MoreInfo,
                "application" => $currentApplication,
                "fillials" => array(),
                'idorg' => $id_org,
                'type' => isset($_POST['type']) ? $_POST['type'] : '',
                'max_show_tab' => $max_show_tab,
                'deprivation' => $deprivation,
                'removal' => $removal,
            ));
        } else {
            $copyApplicationDoc = NULL;
            $currentApplication = NULL;
            $MoreInfo = NULL;
            $currentType = NULL;
            $currentReason = NULL;
            $types_class = new Model\Gateway\EntitiesTable('isga_ApplicationTypes');
            $types = $types_class->getEntitiesByWhere('`OldType` = 0 ORDER BY `Id` DESC');
            $certificate_class = new Model\Gateway\EntitiesTable('isga_Certificates');
            $idOrgDashes = $educationOrganization->fillIdDashes($id_org);
            $certificates = $certificate_class->getAllEntities(null, 'DateEnd DESC',
                array('fk_eiisEducationalOrganization ' => array($id_org, $idOrgDashes)));
            $licenses_class = new Model\Gateway\EntitiesTable('eiis_Licenses');
            $licenses = $licenses_class->getAllEntities(null, 'DateEnd DESC',
                array('fk_eiisEducationalOrganization' => array($id_org, $idOrgDashes)));
            $fillialsApplication_class = new Model\Gateway\EntitiesTable('isga_mtm_Application_Branches');
            $fillialsApplication = $fillialsApplication_class->getEntitiesByWhere(array('fk_isgaApplication' => $id_org));
            $fillials = array();
            foreach ($fillialsApplication as $oneApplication) {
                $fillials[] = $oneApplication->getLinkedItem('eiis_EducationalOrganizations',
                    array('Id' => $oneApplication->getField("fk_eiisEducationalOrganizations")));
            }
            $view->setVariables(array("deprivation" => $deprivation, "types" => $types, "certificates" => $certificates, 'licenses' => $licenses, "fillials" => $fillials, 'idorg' => $id_org, 'type' => isset($_POST['type']) ? $_POST['type'] : ''));
        }

        $view->setTerminal(true);
        return $view;
    }

    /**
     * ОТправка заявки на вкладку отправленные
     *
     * url:  /eo/declaration/send-to-send/ид-зая-ния
     */
    public function sendToSendAction()
    {

        $id = $this->params('id');
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id);
        if (isset($_REQUEST['type']) && @$_REQUEST['type'] == 'rectification') {
            $ApplicationDocumentGroups = new Model\Entities\isgaApplicationDocumentGroups();
            $docScanCopy = $ApplicationDocumentGroups->getByWhere(
                'fk_isgaApplicationDocumentType = "0B4B6324-5DAE-4AB8-B973-86C4A39409E1" AND
                fk_isgaApplication = "' . $id . '" AND
                isArhive="0"
                ORDER BY CreatedDate DESC'
            );
            $notifications_class = new Model\Entities\isgaNotification();
            $notification = $notifications_class->getByWhere(array(
                "fk_isgaApplication" => $id,
                "fk_isgaNotificationTemplate" => '5C66BDA8-BD63-4962-91DB-BDCBA893D53F'
            ));
            if (empty($docScanCopy)) {
                $signed = false;
            } else {
                if ($docScanCopy->isEmptyField('CreatedDate')) {
                    $signed = false;
                } else {
                    $timeCopy = strtotime($docScanCopy->getField('CreatedDate'));
                    $timeNotification = strtotime($notification->getField('DateCreation'));
                    if ($timeNotification > $timeCopy) {
                        $signed = false;
                    } else {
                        $signed = true;
                    }
                }
            }
        } else {
            $signed = $currentApplication->checkSignedDeclaration();
        }
        //$signed = false;//отладочнй
        if ($signed) {
            if (isset($currentApplication) && $currentApplication != NULL) {
                if (isset($_REQUEST['type']) && @$_REQUEST['type'] == 'rectification') {
                    /*$currentApplication->setNewStatus("0FE99A13-98BB-4235-97B2-B65819AB34AF",date('Y-m-d H:i:s'));*/
                    $currentApplication->setNewStatus("4465AC20-13CF-45A5-8D84-4FD1C6C15E07", date('Y-m-d H:i:s'));
                    $currentApplication->exchangeArray(array('Id' => $id, 'fk_isgaApplicationStatus' => "4465AC20-13CF-45A5-8D84-4FD1C6C15E07", 'DateSubmission' => date("Y-m-d")));
                } else {
                    $currentApplication->exchangeArray(array('Id' => $id, 'fk_isgaApplicationStatus' => "38D871AC-BF1D-469E-AFC1-7A6CD55EF1B6", 'DateSubmission' => date("Y-m-d")));
                }
                $currentApplication->save();
                echo 'Ok';
                die;
            } else {
                echo 'Error';
                die;
            }
        } else {
            if ($currentApplication->getField("fk_isgaApplicationStatus") != "2D675622-1E14-4063-AD11-B647B836746D" && $currentApplication->getField("fk_isgaApplicationStatus") != "6c184c34-cc7b-4117-91f7-f0533d5caa23") {
                $currentApplication->exchangeArray(array('Id' => $id, 'fk_isgaApplicationStatus' => "A3B952BF-628E-446C-B865-E483503241E8"));
                $currentApplication->save();
            }
            echo 'Unsigned';
            die;
        }
    }

    public function ApplInfo($id)
    {
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id);
        $signed = $currentApplication->getSignedDeclaration();
        $type = $currentApplication->getType();
        $branch = $currentApplication->getLinkedBranchesAppl();
        $data['Signed'] = $signed->toArray();

        $data['Branches'] = $branch->toArray();
        foreach ($data['Branches'] as $key => $oneBranch) {
            $data['Branches'][$key]['fk_eiisEOBranch'] = $currentApplication->fillIdDashes($oneBranch['fk_eiisEOBranch']);
        }
        $data['Files'] = $currentApplication->getLinkedFiles()->toArray();
        $data['Programs'] = $currentApplication->getLinkedPrograms()->toArray();
        foreach ($data['Programs'] as $key => $oneProgram) {
            $data['Programs'][$key]['fk_eiisLicensedPrograms'] = $currentApplication->fillIdDashes($oneProgram['fk_eiisLicensedPrograms']);
            $data['Programs'][$key]['fk_isgaEOBranches'] = $currentApplication->fillIdDashes($oneProgram['fk_isgaEOBranches']);
        }
        $data['MoreInfo'] = $currentApplication->getLinkedMoreInfo()->toArray();
        $data['Application'] = $currentApplication->toArray();
        $data['Application']['fk_isgaApplicationType'] = $type->getField('Id');
        $data['Application']['fk_eiisEducationalOrganization'] = $currentApplication->fillIdDashes($data['Application']['fk_eiisEducationalOrganization']);
        $data['Application']['fk_eiisLicense'] = $currentApplication->fillIdDashes($data['Application']['fk_eiisLicense']);
        $curl = curl_init();
        $url = 'http://f11ron.mon.prostoy.ru/ron/declaration/add-declaration-other';

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'all=' . json_encode($data));
        $answer = curl_exec($curl);
        echo $answer;
        die;
    }

    public function getFormSignedAction()
    {
        $id = $this->params('id');
        $type = $this->params('id2');
        $view = new ViewModel();
        $view->setVariables(array(
            "id_appl" => $id,
            "type" => $type,
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * Загрузка подписаного файла
     * проверка подписей всех файлоу, выдергиваем подписаные данные, оригинал отпровляем в архив
     *
     * url:  /eo/declaration/save-sign-file/ид-зая-ния
     *
     */
    public function saveSignFileAction()
    {
        $id_appl = $this->params('id');
        $error = "";
        $applicationDocumentGroups_class = new \Model\Entities\isgaApplicationDocumentGroups();
        $sign_class = new \Eo\Service\Sign();
        $short_path = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/';
        $one_file = $_FILES['sign_file'];
        $name_info_file = $one_file['name'];
        $save_data = array();

        $logFile = 'public/log/send_file_to_open_server.log';
        $logerClass = new \Eo\Model\SendMail();
        $logerClass->mailLog('Начинаем', $logFile);
        $logerClass->mailLog('имя приеховшего файла ' . $name_info_file, $logFile);


        $httpHost = $_SERVER['HTTP_HOST'];
        $logerClass->mailLog("\t\t" . 'сервер ' . $httpHost, $logFile);

        $need_sign_valid = true;

        if ($one_file['size'] != 0) {
            if (isset($one_file) && $one_file['error'] == 0) {
                if ($one_file['size']) {

                    $logerClass->mailLog("\t\t" . 'проверка типа подписи ', $logFile);
                    //проверим является ли файл PDF || подписанным SIG, а также проверка имени файла
                    $result_verify = $sign_class->verify_name_signfile($name_info_file);

                    if ($result_verify === true) {
                        $logerClass->mailLog("\t\t" . 'подпись как надо ', $logFile);
                        $nameInfoFilePath = 'synch/' . date('Y') . '/' . $id_appl . '/open/';//папку на серверк куда закинуть файл
                        $nameInfoFilePathFull = $short_path . $nameInfoFilePath;
                        $logerClass->mailLog("\t\t" . "\t\t" . 'путь куда заливать ' . $nameInfoFilePath, $logFile);
                        $logerClass->mailLog("\t\t" . "\t\t" . 'путь куда заливать полный ' . $nameInfoFilePathFull, $logFile);
                        if (!is_dir($nameInfoFilePathFull)) { // если ее нет создадим
                            if (!mkdir($nameInfoFilePathFull, 0777, true)) {
                                echo json_encode(array("error" => "Не смогло создать папку в файлохранилище "));
                                die;
                            }
                        }
                        $name_info_file_hash = $nameInfoFilePath . $sign_class->get_hash_name($name_info_file);
                        $logerClass->mailLog("\t\t" . 'хеш имя файла ' . $name_info_file_hash, $logFile);
                        if (move_uploaded_file($one_file['tmp_name'], $short_path . $name_info_file_hash)) {
                            $logerClass->mailLog("\t\t" . 'файл залит на диск ', $logFile);

                            if ($need_sign_valid) {
                                /*                                 * *отправим файл на сервер проверки подписи и получим данные сертификата и валидности */
                                $sign_data = $sign_class->validSignature($name_info_file_hash);
                            } else {
                                $sign_data = ["status" => "ok"];
                            }
                            if ($sign_data['status'] == 'ok') {
                                $logerClass->mailLog("\t\t" . 'подпись првавильная ', $logFile);

                                if ($need_sign_valid) {
                                    /*                                     * **соберем информацию о пидписи файла*** */
                                    $signInfoFilesClass = new \Model\Entities\isgaSignInfoFiles();
                                    $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
                                    $id_org = $user->getField("fk_eiisEducationalOrganization");
                                    $educationOrganization = $user->getEducationOrganization();
                                    $organization_inn = trim($educationOrganization->getField('Inn'));
                                    $certificate_inn = trim($sign_data["certificate_info"]['SubjectName']["INN"]);
                                    $save_sign_info = array(
                                        "fk_isgaUserId" => $user->getField("Id"),
                                        "date_verify" => Date("Y-m-d H:i:s"),
                                        "valid" => ($sign_data['result']) ? 1 : 0,
                                        //"valid"            => 1,
                                        'message' => $sign_data['message'],
                                        "certificate_info" => json_encode($sign_data['certificate_info'])
                                    );
                                } else {
                                    $save_sign_info = ['valid' => 1];
                                }

                                /*                                 * проверим принадлежит ли сертификат подписи пользователю загрузившему файл */
                                /* if($organization_inn != $certificate_inn)
                                  {
                                  $save_sign_info['valid'] = 0;
                                  $save_sign_info['message'] .= "ИНН сертификата подписи не совпадает с ИНН подписывающего";
                                  } */

                                if ($save_sign_info['valid'] == 1) {
                                    $logerClass->mailLog("\t\t" . 'перзаем данные из подписи ', $logFile);
                                    //извлекаем файл из подписи
                                    $result_content = $sign_class->getContentReal($name_info_file_hash, array('isGetContent' => true));

                                    if (array_key_exists('status', $result_content) && $result_content['status'] == 'ok') {
                                        if (empty($result_content['file_content'])) {
                                            if (empty($result_content['error'])) {
                                                $result_content['error'] = '';
                                            }

                                            $result_content['error'] = $logerClass->mailLog('нет содержимого файла  4 ', $logFile);
                                            $logerClass->mailLog(print_r($result_content, true), $logFile);
                                            echo json_encode($result_content);
                                            die;
                                        }
                                        $logerClass->mailLog("\t\t" . 'данные выдернуты ', $logFile);
                                        //путь куда
                                        $filePathName = '/public/uploads/';
                                        $extractFilePath = /*$filePathName.*/
                                            $nameInfoFilePath . $result_content['file_name'];
                                        $extractFileName = $sign_class->get_real_name($name_info_file);
                                        $extractFilePathFull = $nameInfoFilePathFull . $result_content['file_name'];
                                        //сохраним извлеченный файл
                                        file_put_contents($extractFilePathFull, $result_content['file_content']);
                                        unset($result_content['file_content']);

                                        $logerClass->mailLog("\t\t" . "\t\t" . 'имя  извлеченноо файла ' . $extractFileName, $logFile);
                                        $logerClass->mailLog("\t\t" . "\t\t" . 'хеш извлеченной файла ' . $extractFilePath, $logFile);


                                        $signedFilePath = /*$filePathName.*/
                                            $name_info_file_hash;
                                        $signedFileName = $name_info_file;
                                        $logerClass->mailLog("\t\t" . "\t\t" . 'имя  подписаного файла ' . $signedFileName, $logFile);
                                        $logerClass->mailLog("\t\t" . "\t\t" . 'хеш подписаного файла ' . $signedFilePath, $logFile);

                                        //запхнем в связную табличку
                                        //извленченный
                                        $applicationDocumentGroups_class_duble = new \Model\Entities\isgaApplicationDocumentGroups();
                                        $save_unpack_file_data = [];
                                        $save_unpack_file_data['FtpDirectoryUrl'] = $extractFilePath;
                                        $save_unpack_file_data['RealFileName'] = $extractFileName;
                                        $save_unpack_file_data['fk_isgaApplicationDocumentType'] = '0B4B6324-5DAE-4AB8-B973-86C4A39409E1';
                                        $save_unpack_file_data['fk_isgaApplication'] = $id_appl;
                                        $applicationDocumentGroups_class_duble->exchangeArray($save_unpack_file_data);
                                        $logerClass->mailLog('извлеченный фйла записан', $logFile);
                                        //$applicationDocumentGroups_class_duble->save(1);

                                        //подписанный( исходный )
                                        $save_data = array();
                                        $save_data['isArhive'] = 1;
                                        //$name_info = $name_info_file . "::" . $name_info_file_hash;
                                        $save_data['FtpDirectoryUrl'] = $signedFilePath;
                                        $save_data['RealFileName'] = $signedFileName;
                                        $save_data['fk_isgaApplicationDocumentType'] = '0B4B6324-5DAE-4AB8-B973-86C4A39409E1';
                                        $save_data['fk_isgaApplication'] = $id_appl;
                                        $applicationDocumentGroups_class->exchangeArray($save_data);
                                        $logerClass->mailLog('подписаный  фйла записан', $logFile);
                                        //$applicationDocumentGroups_class->save(1);


                                        $logerClass->mailLog('открепим все файлы по данной акредитации', $logFile);
                                        $applicationFiles_class = new \Model\Gateway\EntitiesTable('isga_ApplicationFiles');
                                        $files_application = $applicationFiles_class->getEntitiesByWhere(array(
                                            'fk_isgaApplication' => $id_appl,
                                            'isArhive' => 0,
                                            new \Zend\Db\Sql\Predicate\Like('RealName', '%.sig'),
                                        ))->getObjectsArray();
                                        $logerClass->mailLog('найдено ' . count($files_application), $logFile);
                                        $fileCounter = 1;
                                        $fileNameArray = array();
                                        $logerClass->mailLog('подготовка выдергиванию содержимое из файла ', $logFile);
                                        foreach ($files_application as $key => $file) {
                                            $applicationFiles_class = new \Model\Entities\isgaApplicationFiles();
                                            $logerClass->mailLog("\t\t" . 'подготовка выдергиванию содержимое из файла ' . $fileCounter++, $logFile);
                                            $logerClass->mailLog("\t\t" . 'изначальное имя' . $file->getFieldSafe("RealName"), $logFile);
                                            $logerClass->mailLog("\t\t" . 'изначальный хеш' . $file->getFieldSafe("HashName"), $logFile);
                                            $fileNameArray[$file->getId()] = $file->getField("HashName");
                                        }

                                        $logerClass->mailLog('к выдергиванию подготовлено', $logFile);
                                        $exceptResult = $sign_class->getContentMulti($fileNameArray, array('isPutFile' => true));
                                        $logerClass->mailLog('выдернуто ' . print_r($exceptResult, true), $logFile);
                                        //$result_content = $sign_class->getContentReal( $file->getField("HashName") , array('isGetContent' => true ) );

                                        if (strtolower($exceptResult['status']) != 'ok') {
                                            $prot = uniqid('prot-');
                                            echo json_encode(array("error" => 'ошибка  при распоковке файлов prot ' . $prot));
                                            $logerClass->mailLog('отвал при извлечении файлов из подписий ' . $prot, $logFile);
                                            die;
                                        }


                                        // переделка под мульти курл
                                        foreach ($files_application as $key => $file) {
                                            // делаем его архивным подписаный
                                            $pathFrom = $file->getFieldSafe("HashName");
                                            $exceptResult['file_name'];
                                        }


                                        $fileCounter = 1;
                                        foreach ($files_application as $key => $file) {
                                            //$result_content = $sign_class->getContentReal( $file->getField("HashName") , array('isGetContent' => true ) );
                                            //$result_content = $exceptResult[ $file->getId() ];
                                            $result_content['status'] = 'ok';
                                            //$exceptResultFileName =
                                            if (array_key_exists('status', $result_content) && $result_content['status'] == 'ok') {
                                                //if( empty( $result_content['file_name'] ) ){
                                                //    if( empty($result_content['error']) ){
                                                //        $result_content['error'] = '';
                                                //    }
                                                //    $result_content['error'] = $logerClass->mailLog('нет содержимого файла 3  '.$result_content['error']  , $logFile );
                                                //    //$result_content['error'] = $logerClass->mailLog( 'нет содержимого файла  ' );
                                                //    $logerClass->mailLog( print_r( $result_content ,true )  , $logFile );
                                                //    echo json_encode( result_content );
                                                //    die;
                                                //}

                                                $logerClass->mailLog("\t\t" . 'подготавливаем запись о файле ' . $fileCounter++, $logFile);


                                                //Впихнем извлеченный файл
                                                //$pathFrom = $file->getFieldSafe("HashName");
                                                //$pathFromArray = explode('/' , $pathFrom);
                                                //array_pop( $pathFromArray );
                                                //$pathTo = implode( '/' , $pathFromArray );

                                                //$extractFilePath = $pathTo.'/'.$result_content['file_name'];
                                                //$extractFilePathFull = $short_path.$extractFilePath;

                                                //сохраним извлеченное
                                                //file_put_contents( $extractFilePathFull, $result_content['file_content'] );
                                                //unset( $result_content['file_content'] );

                                                // делаем его архивным подписаный
                                                $files_application[$key]->setField("isArhive", 1);
                                                $extractFileReal = $sign_class->get_real_name($file->getField("RealName"));
                                                $extractFilePath = $exceptResult['file_name'][$file->getId()];

                                                $logerClass->mailLog("\t\t\t\t" . 'выдернутое имя: ' . $extractFileReal, $logFile);
                                                $logerClass->mailLog("\t\t\t\t" . 'выдернутый хеш: ' . $extractFilePath, $logFile);

                                                $applicationFiles_class_dubl[$key] = new \Model\Entities\isgaApplicationFiles();
                                                $save_unpack_file_data = [];
                                                $save_unpack_file_data['RealName'] = $extractFileReal;
                                                $save_unpack_file_data['HashName'] = $extractFilePath;
                                                $save_unpack_file_data['fk_isgaDocumentType'] = $file->getField("fk_isgaDocumentType");
                                                $save_unpack_file_data['fk_isgaApplication'] = $id_appl;
                                                $applicationFiles_class_dubl[$key]->exchangeArray($save_unpack_file_data);
                                                //$applicationFiles_class_dubl->save(1);
                                                $logerClass->mailLog("\t\t" . ' файл добавлен', $logFile);
                                                $logerClass->mailLog("\t\t" . '', $logFile);
                                                //$save_data['isArhive'] = 1;
                                            } else {
                                                echo json_encode(array("error" => 'ошибка ' . $result_content['result'] . ' при распоковке файла ' . $file->getFieldSafe('RealName')));
                                                die;
                                            }

                                        }


                                        if ($need_sign_valid) {
                                            /*                                         * сохраним данные о подписи* */
                                            $save_sign_info["fk_isgaAppplicationFileId"] = $applicationDocumentGroups_class->getField('Id');
                                            $signInfoFilesClass->exchangeArray($save_sign_info);
                                            //$res_sign = $signInfoFilesClass->save(1);
                                        }


                                        //сохраняем все в базу
                                        $logerClass->mailLog('сохраняем все в базу', $logFile);
                                        $applicationDocumentGroups_class_duble->save(1);
                                        $applicationDocumentGroups_class->save(1);
                                        foreach ($files_application as $key => $file) {
                                            $file->save();
                                            if (!empty($applicationFiles_class_dubl[$key])) {
                                                $applicationFiles_class_dubl[$key]->save(1);
                                            }
                                        }

                                        $logerClass->mailLog('', $logFile);
                                        echo json_encode(array("status" => 'send'));
                                        die;
                                    } else {
                                        echo json_encode(array("error" => "получение содержимого из подписи: " . $result_content['result']));
                                        die;
                                    }
                                } else {
                                    echo json_encode(array("error" => "Неверная подпись. Ответ сервера проверки подписи: " . $save_sign_info['message']));
                                    die;
                                }
                            } else {
                                $error = $sign_class->getErrorText($sign_data['result']);
                                echo json_encode(array("error" => $error));
                                die;
                            }
                            die;
                        } else {
                            echo json_encode(array("error" => "Не удалось загрузить файл, проверьте права доступа"));
                            die;
                        }
                    } else {
                        $error = $sign_class->getErrorText($result_verify);
                        if ('Файл не подписан сертификатом' == $error) {
                            $error = 'Файл не подписан электронной подписью';
                        }
                        echo json_encode(array("error" => $error));
                        die;
                    }
                } else {
                    echo json_encode(array("error" => "Размер файла превышает 10 Мб"));
                    die;
                }
            } else {
                echo json_encode(array("error" => "Не удалось загрузить файл."));
                die;
            }
        } else {
            echo json_encode(array("error" => "Не удалось загрузить файл"));
            die;
        }
    }

    public function saveFileSignedAction()
    {
        $id_appl = $this->params('id');
        $allowed = array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx", "sig");
        $error = "";
        $applicationDocumentGroups_class = new \Model\Entities\isgaApplicationDocumentGroups();
        if (isset($_FILES['signed'])) {
            $one_file = $_FILES['signed'];
            $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
            $name = explode('.', $one_file['name']);
            if ($one_file['size'] != 0) {
                if (isset($one_file) && $one_file['error'] == 0) {
                    $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
                    $name = explode('.', $one_file['name']);
                    if ($one_file['size'] < 10 * 1024 * 1024) {
                        if (in_array(mb_strtolower($ext), $allowed)) {
                            $ext = strrchr($one_file['name'], '.');
                            $name_info_file = $one_file['name'];
                            $name_info_file_hash = md5($this->translit('', $name[0]) . '_' . md5(time())) . $ext;
                            move_uploaded_file($one_file['tmp_name'],
                                $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' . $name_info_file_hash);
                            $name_info = $name_info_file . "::" . $name_info_file_hash;
                            $save_data['FtpDirectoryUrl'] = '/public/uploads/' . $name_info_file_hash;
                            $save_data['RealFileName'] = $name_info_file;
                            $save_data['fk_isgaApplicationDocumentType'] = '0B4B6324-5DAE-4AB8-B973-86C4A39409E1';
                            $save_data['fk_isgaApplication'] = $id_appl;
                            $applicationDocumentGroups_class->exchangeArray($save_data);
                            $applicationDocumentGroups_class->save(1);
                        } else {
                            $error = "Недопустимый формат файла.";
                        }
                    } else {
                        $error = "Размер файла превышает 10 Мб";
                    }
                } else {
                    $error = "Ошибка загрузки файла";
                }
            }
        } else {
            $error = "Ошибка загрузки файла";
        }
        if ($error != "") {
            echo '<script>parent.useTopHoverTitle("' . $error . '", 10000)</script>';
        } else {
            echo '<script>parent.sendToSend(); parent.hidePopupWin();</script>';
            die;
        }
    }

    public function saveFilweSignedAction()
    {
        $id_appl = $this->params('id');
        $allowed = array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx", "sig");
        $error = "";
        $applicationDocumentGroups_class = new \Model\Entities\isgaApplicationDocumentGroups();
        $one_file = $_FILES['signed'];

        $sign_class = new \Eo\Service\Sign();
        $short_path = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/';

        if ($one_file['size'] != 0) {
            if (isset($one_file) && $one_file['error'] == 0) {

                if ($one_file['size'] < 10 * 1024 * 1024) {
                    $name_info_file = $one_file['name'];
                    //проверим является ли файл PDF || подписанным SIG, а также проверка имени файла
                    $result_verify = $sign_class->verify_name_signfile($name_info_file);
                    if ($result_verify === true) {
                        $name_info_file_hash = $sign_class->get_hash_name($name_info_file);

                        if (!file_exists($short_path)) {
                            if (!mkdir($short_path, 0777, true)) {
                                echo json_encode(array("error" => "Не удалось создать каталог, проверьте права доступа"));
                                die;
                            }
                        }
                        if (move_uploaded_file($one_file['tmp_name'], $short_path . $name_info_file_hash)) {

                            /*                             * *отправим файл на сервер проверки подписи и получим данные сертификата и валидности */
                            $sign_data = $sign_class->valid_signature($name_info_file_hash);

                            if ($sign_data['status'] == 'ok') {
                                $signInfoFilesClass = new \Model\Entities\isgaSignInfoFiles();
                                $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
                                $id_org = $user->getField("fk_eiisEducationalOrganization");
                                $educationOrganization = $user->getEducationOrganization();
                                $organization_inn = trim($educationOrganization->getField('Inn'));
                                $certificate_inn = trim($sign_data["certificate_info"]['SubjectName']["INN"]);

                                $save_sign_info = array(
                                    "fk_isgaUserId" => $user->getField("Id"),
                                    "date_verify" => Date("Y-m-d H:i:s"),
                                    // "valid"                         =>  ($sign_data['result']) ? 1 : 0,
                                    "valid" => 1,
                                    'message' => $sign_data['message'],
                                    "certificate_info" => json_encode($sign_data['certificate_info'])
                                );

                                /*                                 * проверим принадлежит ли сертификат подписи пользователю загрузившему файл */
                                /* if($organization_inn != $certificate_inn)
                                  {
                                  $save_sign_info['valid'] = 0;
                                  $save_sign_info['message'] .= "ИНН сертификата подписи не совпадает с ИНН подписывающего";
                                  } */

                                if ($save_sign_info['valid'] == 1) {
                                    $applicationDocumentGroups_class->exchangeArray($save_data);
                                    $applicationDocumentGroups_class->save(1);

                                    $save_data["fk_isgaAppplicationFileId"] = $applicationDocumentGroups_class->getField('Id');
                                    $save_data['FtpDirectoryUrl'] = '/public/uploads/' . $name_info_file_hash;
                                    $save_data['RealFileName'] = $name_info_file;
                                    $save_data['fk_isgaApplicationDocumentType'] = '0B4B6324-5DAE-4AB8-B973-86C4A39409E1';
                                    $save_data['fk_isgaApplication'] = $id_appl;
                                    /*                                     * сохраним данные о подписи* */
                                    $signInfoFilesClass->exchangeArray($save_sign_info);
                                    $res_sign = $signInfoFilesClass->save(1);
                                } else {
                                    $error = $sign_data['message'];
                                }
                            } else {
                                $error = $sign_class->getErrorText($sign_data['result']);
                            }
                        } else {
                            $error = "Не удалось загрузить файл, проверьте права доступа";
                        }
                    } else {
                        $error = $sign_class->getErrorText($result_verify);
                    }
                } else {
                    $error = "Размер файла превышает 10 Мб";
                }
            } else {
                $error = "Ошибка загрузки файла";
            }
        } else {
            $error = "Ошибка загрузки файла";
        }

        if ($error != "") {
            echo '<script>parent.useTopHoverTitle(\'' . $error . '\', 10000)</script>';
        } else {
            echo '<script>parent.sendToSend(); parent.hidePopupWin();</script>';
            die;
        }
    }

    public function getApplicationReasonAction()
    {
        $id = $this->params('id');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $reasons_class = new Model\Gateway\EntitiesTable('isga_ApplicationReasons');

        $httpHost = $_SERVER['HTTP_HOST'];
        $testHost = '85.192.47.4';
        /*if($httpHost != $testHost ){
            $where = array("fk_isgaApplicationType"=>$id,'Id NOT IN ("39788d86-0f59-489d-b43d-e4557aa6d466","512C17E5-CC59-4373-B6CB-C0C07F93DACF")');
        }else{*/
        $where = array("fk_isgaApplicationType" => $id);
        /*}*/
        $reasons = $reasons_class->getAllEntities(null, 'Name ASC', $where);
        $view = new ViewModel();
        $view->setVariables(array("reasons" => $reasons));
        $view->setTerminal(true);
        return $view;
    }

    public function generateXmlRegionAction()
    {
        $regionsClass = new Model\Gateway\EntitiesTable('eiis_Regions');
        $regions = $regionsClass->getAllEntities(NULL, 'Name ASC');
        foreach ($regions as $key => $value) {
            echo '<option innerHtml="' . $value->getField('Name') . '" value="' . $value->getField('Name') . '"/>';
        }
        die;
    }

    public function getApplicationFillialsAction()
    {
        $id = $this->params('id');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id);
        /*$reasonsIds = $currentApplication->getIdsReasons();
         if (in_array("67f97827-57f9-4a0c-b2c7-d9806969bef6",$reasonsIds)
          || in_array('24e060cb-6384-4009-b9ce-93a775678732',$reasonsIds)
          || in_array('f3923905-c349-4f87-af80-8660361537dc',$reasonsIds)
          || in_array('64771BDC-27C5-4C83-B93D-CB725435658B',$reasonsIds)
          ) { */
        $License = $currentApplication->getLinkedItem('eiis_Licenses',
            array('Id' => $currentApplication->getField("fk_eiisLicense")));
        $LicenseSupplements = $License->getLinkedItems('eiis_LicenseSupplements',
            array('fk_eiisLicense' => $License->getField("Id"), 'fk_eiisBranch IS NOT NULL'));
        $currOrg = $currentApplication->getLinkedItem('eiis_EducationalOrganizations',
            array('Id' => $currentApplication->getField("fk_eiisEducationalOrganization")));
        $fillialsAll = array();
        $fillialsAll[0]['Id'] = $currOrg->getField('Id');
        $fillialsAll[0]['Name'] = $currOrg->getField('RegularName');
        $i = 1;
        foreach ($LicenseSupplements as $oneSuplement) {
            $stateSuplement = $oneSuplement->getLinkedItem('eiis_LicenseSupplementStates',
                array('Id' => $oneSuplement->getField("fk_eiisLicenseSupplementState")));
            if ($stateSuplement->getField('IsValid')) {
                $fillialName = $oneSuplement->getLinkedItem('eiis_EducationalOrganizations',
                    array('Id' => $oneSuplement->getField("fk_eiisBranch")));
                $fillialsAll[$i]['Id'] = $oneSuplement->getField('fk_eiisBranch');
                $fillialsAll[$i]['Name'] = $fillialName->getField('RegularName');
                $i++;
            }
        }
        /* } else {
          $Certificate            = $currentApplication->getCertificate();
          $CertificateSupplements = $Certificate->getLinkedItems('isga_CertificateSupplements', array('fk_isgaCertificate' => $Certificate->getField("Id"), 'fk_eiisEducationalOrganization IS NOT NULL'));
          $currOrg                = $currentApplication->getEducationalOrganization();
          $fillialsAll            = array();
          $fillialsAll[0]['Id']   = $currOrg->getField('Id');
          $fillialsAll[0]['Name'] = $currOrg->getField('RegularName');
          $i                      = 1;
          foreach ($CertificateSupplements as $oneSuplement) {
          $stateSuplement = $oneSuplement->getStatusCode();
          if ($stateSuplement == 'InForce'//действующие
          ||$stateSuplement == 'Resumed'//возобновленные
          ) {
          $fillialName                    = $oneSuplement->getEducationalOrganization();//$oneSuplement->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $org_id));
          $filialOrg=$oneSuplement->getEducationalOrganization();
          $filialId= $filialOrg->getId();
          if($filialId==$fillialsAll[0]['Id']){
          continue;//пропускаем главный, как уже вписаный
          }
          $fillialsAll[$i]['Id']          = $filialId;
          $fillialsAll[$i]['Name']        = $fillialName->getField('RegularName');
          $i++;
          }
          }
          } */
        $fillialsApplication_class = new Model\Gateway\EntitiesTable('isga_mtm_Application_Branches');
        $fillialsApplication = $fillialsApplication_class->getEntitiesByWhere(array('fk_isgaApplication' => $id));
        $array_id = array();
        foreach ($fillialsApplication as $key => $value) {
            $array_id[] = $value->getField('fk_eiisEOBranch');
        }
        $view = new ViewModel();
        $view->setVariables(array("check_fill" => $array_id, "fillials" => $fillialsAll, 'application' => $id));
        $view->setTerminal(true);
        return $view;
    }

    public function getCertificatesByOrgAction()
    {
        $applClass = new \Model\Entities\isgaApplications();
        $id_org = $this->params('id');
        $idOrgDashes = $applClass->fillIdDashes($id_org);
        $certificate_class = new Model\Gateway\EntitiesTable('isga_Certificates');
        $certificates = $certificate_class->getAllEntities(null, 'DateEnd DESC',
            array('fk_eiisEducationalOrganization ' => array($id_org, $idOrgDashes)));
        $view = new ViewModel();
        $view->setVariables(array("certificates" => $certificates));
        $view->setTerminal(true);
        return $view;
    }

    public function saveFileAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->educationOrganization = $educationOrganization;
        $this->layout()->identityUser = $user;
        $id_appl = $this->params('id');
        $allowed = array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx", "sig");
        $error = "";
        $prev_key = 1;
        $massFiles = array();

        foreach ($_FILES as $key => $one_file) {
            $applicationFiles_class = new \Model\Entities\isgaApplicationFiles();
            $save_data = array();
            $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
            $name = explode('.', $one_file['name']);
            if ($one_file['size'] != 0) {
                if (isset($one_file) && $one_file['error'] == 0) {
                    $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
                    $name = explode('.', $one_file['name']);
                    if ($one_file['size'] < 10 * 1024 * 1024) {
                        if (in_array(mb_strtolower($ext), $allowed)) {
                            $ext = strrchr($one_file['name'], '.');
                            $name_info_file = $one_file['name'];
                            $name_info_file_hash = md5($this->translit('', $name[0]) . '_' . md5(time())) . $ext;
                            move_uploaded_file($one_file['tmp_name'],
                                $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' . $name_info_file_hash);
                            $name_info = $name_info_file . "::" . $name_info_file_hash;
                            $save_data['HashName'] = $name_info_file_hash;
                            $save_data['RealName'] = $name_info_file;
                            $save_data['fk_isgaDocumentType'] = substr($key, 0, -2);
                            $save_data['fk_isgaApplication'] = $id_appl;
                            $applicationFiles_class->exchangeArray($save_data);
                            $applicationFiles_class->save(1);
                        } else {
                            $error = "Недопустимый формат файла.";
                        }
                    } else {
                        $error = "Размер файла превышает 10 Мб";
                    }
                } else {
                    $error = "Ошибка загрузки файла";
                }
            }
        }
        $new_names['fk_isgaApplication'] = $id_appl;
        $applicationFiles_class = new \Model\Gateway\EntitiesTable('isga_ApplicationFiles');
        $old = $applicationFiles_class->getEntitiesByWhere(array('fk_isgaApplication' => $id_appl))->current();
        if (isset($old) && !empty($old)) {
            $new_names['Id'] = $old->getField('Id');
        }

        if ($error != "") {
            $result['error'] = $error;
            $text = 'error';
        } else {
            $text = " ";
            $result = 5;
        }
        echo '<script>parent.handleResponse(\'' . json_encode($result) . '\',\'' . $text . '\')</script>';
        die;
    }

    public function saveDeclarationAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->educationOrganization = $educationOrganization;
        $this->layout()->identityUser = $user;
        $save_type = $this->params()->fromPost('save_type');
        $applicationReason_class = new Model\Entities\isgamtmApplicationReason();
        switch ($save_type) {
            case 'add1':
                $data = $_POST;
                unset($data['save_type']);
                $application_class = new Model\Entities\isgaApplications();
                $data[0]['DateCreate'] = date('Y-m-d');
                $applicationReasons = array();
                if (isset($data[0]["fk_isgaApplicationReason"]))
                    $applicationReasons = $data[0]["fk_isgaApplicationReason"];
                $data[0]["fk_isgaApplicationReason"] = '1';
                $application_class->exchangeArray($data[0]);
                if (isset($data[0]['Id'])) {
                    $application_class->save();
                    $res = $data[0]['Id'];
                } else {
                    $application_class->setField('Barcode', $application_class->generateBarcode());
                    $res = $application_class->save(1);
                    $res = $application_class->getId();
                }
                $dataReason = array();
                if (count($applicationReasons)) {
                    $applicationReason_class->deleteWhere(array('fk_isgaApplication' => $res));
                    foreach ($applicationReasons as $oneReason) {
                        $applicationReason_classNew = new Model\Entities\isgamtmApplicationReason();
                        $dataReason["fk_isgaApplicationReason"] = $oneReason;
                        $dataReason["fk_isgaApplication"] = $res;
                        $applicationReason_classNew->exchangeArray($dataReason);
                        $applicationReason_classNew->save(1);
                    }
                }

                $data[1]['fk_isgaApplication'] = $res;
                //мореИнфо теперь привязывваеться к филиалу
                if (empty($data[1]['fk_eiisEducationalOrganization'])) {
                    $data[1]['fk_eiisEducationalOrganization'] = $educationOrganization->getId();
                }
                $moreinfo_class = new Model\Entities\isgaApplicationMoreInfo();
                $moreinfo_class = $moreinfo_class->getLinkedItem(
                    'isga_ApplicationMoreInfo',
                    array(
                        'fk_isgaApplication' => $data[1]['fk_isgaApplication'],
                        'fk_eiisEducationalOrganization' => $data[1]['fk_eiisEducationalOrganization']
                    )
                );
                if (empty($moreinfo_class)) {
                    $moreinfo_class = new Model\Entities\isgaApplicationMoreInfo();
                    $moreinfo_class = $moreinfo_class->getLinkedItem(
                        'isga_ApplicationMoreInfo',
                        array(
                            'fk_isgaApplication' => $data[1]['fk_isgaApplication'],
                            'fk_eiisEducationalOrganization' => 0,
                        )
                    );
                }
                $isMoreNew = false;
                if (empty($moreinfo_class)) {
                    $moreinfo_class = new Model\Entities\isgaApplicationMoreInfo();
                    $isMoreNew = true;
                }
                //if (!empty($data[1]['DateReg'])) {
                //    $data[1]['DateReg'] = date('Y-m-d', strtotime($data[1]['DateReg']));
                //}
                //else {
                //    $data[1]['DateReg'] = null;
                //}
                //if (!empty($data[1]['DateEnd'])) {
                //    $data[1]['DateEnd'] = date('Y-m-d', strtotime($data[1]['DateEnd']));
                //}
                //else {
                //    $data[1]['DateEnd'] = null;
                //}
                if (!isset($data[1]['Received'])) {
                    $data[1]['Received'] = 2;
                }
                $moreinfo_class->exchangeArray($data[1]);
                $moreinfo_class->save($isMoreNew);

                $arrayOfStructures = array();
                if (isset($data['ListOfIdStruturs']))
                    $arrayOfStructures = explode(',', $data['ListOfIdStruturs']);
                $application_org_class = new Model\Entities\isgamtmApplicationOrganizations();
                $application_org_class->deleteWhere(array('fk_isgaApplication' => $data[1]['fk_isgaApplication']));
                foreach ($arrayOfStructures as $key => $oneStructure) {
                    if ($oneStructure != '') {
                        $application_org_class->exchangeArray(array('fk_isgaApplication' => $data[1]['fk_isgaApplication'], 'fk_eiisEducationalOrganization' => $oneStructure));
                        $application_org_class->save(1);
                    }
                }
                /*Запихиваем наш вуз*/
                $applicationBranches_class = new Model\Entities\isgamtmApplicationBranches();
                $applicationBranches_class = $applicationBranches_class->getByWhere(
                    array(
                        'fk_eiisEOBranch' => $educationOrganization->getField('Id'),
                        "fk_isgaApplication" => $data[1]['fk_isgaApplication']
                    )
                );
                if (!$applicationBranches_class) {
                    $applicationBranches_class = new Model\Entities\isgamtmApplicationBranches();
                    $applicationBranches_class->exchangeArray(
                        array(
                            'fk_eiisEOBranch' => $educationOrganization->getField('Id'),
                            "fk_isgaApplication" => $data[1]['fk_isgaApplication']
                        )
                    );
                    $applicationBranches_class->save(1);
                }

                echo '<script>parent.handleResponse(1,"' . $data[1]['fk_isgaApplication'] . '")</script>';
                die;
            case 'add2':
                $data = $_POST;
                $applicationBranches_class = new Model\Entities\isgamtmApplicationBranches();
                unset($data['save_type']);
                $id_application = $data['id_application'];
                unset($data['id_application']);
                $branches = array();
                $applicationBranches = $applicationBranches_class->getAllByWhere(array('fk_isgaApplication' => $id_application));
                if (is_array($data)) {
                    foreach ($data as $one) {
                        $applicationBranches_class = new Model\Entities\isgamtmApplicationBranches();
                        $applicationBranches_class = $applicationBranches_class->getByWhere(
                            array(
                                'fk_eiisEOBranch' => $one,
                                "fk_isgaApplication" => $id_application
                            )
                        );
                        if (!$applicationBranches_class) {
                            $applicationBranches_class = new Model\Entities\isgamtmApplicationBranches();
                            $applicationBranches_class->exchangeArray(array(
                                'fk_eiisEOBranch' => $one,
                                "fk_isgaApplication" => $id_application
                            ));
                            $applicationBranches_class->save(1);
                        }
                        array_push($branches, $one);
                    }
                } else {
                    $applicationBranches_class = $applicationBranches_class->getByWhere(
                        array(
                            'fk_eiisEOBranch' => $data,
                            "fk_isgaApplication" => $id_application
                        )
                    );
                    if (!$applicationBranches_class) {
                        $applicationBranches_class = new Model\Entities\isgamtmApplicationBranches();
                        $applicationBranches_class->exchangeArray(array('fk_eiisEOBranch' => $data, "fk_isgaApplication" => $id_application));
                        $applicationBranches_class->save(1);
                    }
                    array_push($branches, $data);
                }
                foreach ($applicationBranches as $applicationBranche) {
                    if (!in_array($applicationBranche->getField('fk_eiisEOBranch'), $branches)) {
                        $applicationBranche->delete();
                    }
                }
                echo 'ok';
                die;
            case 'add3':
                $data = $_POST;
                $applicationBranches_class = new Model\Entities\isgaMtmApplicationBranches();
                unset($data['save_type']);
                $id_application = $data['id_application'];
                unset($data['id_application']);
                $applicationBranches_class->delete();
                foreach ($data as $one) {
                    $applicationBranches_class->exchangeArray(array('fk_eiisEOBranch' => $one, "fk_isgaApplication" => $id_application));
                    $applicationBranches_class->save(1);
                }
                echo '<script>parent.handleResponse(2,' . $id_application . ')</script>';
                die;
        }
    }


    /**
     * откртая - акредитаци я- карточка заявлени - филиалы - сохранить
     * сохрнаяет инву о лицензии гос тайны
     *
     * url:   /eo/declaration/save-declaration-license
     * в посту какието лицензии
     */
    public function saveDeclarationLicenseAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->educationOrganization = $educationOrganization;
        $this->layout()->identityUser = $user;
        $data = $_POST;
        $licenses = $data['license'];
        foreach ($licenses as $eduOrg => $values) {
            $moreInfo = new Model\Entities\isgaApplicationMoreInfo();
            $moreInfo = $moreInfo->getByWhere(array(
                'fk_isgaApplication' => $data['idAppl'],
                'fk_eiisEducationalOrganization' => $eduOrg,
            ));
            $isMoreInfoNew = false;
            if (empty($moreInfo)) {
                $moreInfo = new Model\Entities\isgaApplicationMoreInfo();
                $isMoreInfoNew = true;
            }
            if (empty($values['RegNumber'])) {
                $values['RegNumber'] = '';
            }
            if (empty($values['SiriesNumber'])) {
                $values['SiriesNumber'] = '';
            }
            if (empty($values['DateReg'])) {
                $values['DateReg'] = null;
            } else {
                $values['DateReg'] = date('Y-m-d', strtotime($values['DateReg']));
            }
            if (empty($values['DateEnd'])) {
                $values['DateEnd'] = null;
            } else {
                $values['DateEnd'] = date('Y-m-d', strtotime($values['DateEnd']));
            }
            $moreInfo->setFieldSafe('fk_isgaApplication', $data['idAppl']);
            $moreInfo->setFieldSafe('fk_eiisEducationalOrganization', $eduOrg);
            $moreInfo->setFieldSafe('RegNumber', $values['RegNumber']);
            $moreInfo->setFieldSafe('SiriesNumber', $values['SiriesNumber']);
            $moreInfo->setFieldSafe('DateReg', $values['DateReg']);
            $moreInfo->setFieldSafe('DateEnd', $values['DateEnd']);
            if ($isMoreInfoNew) {
                $moreInfo->save(true);
            } else {
                $moreInfo->save(false);
            }
        }
        echo 'ok';
        die;
    }

    /**
     * удаление документа
     * @param {Integer} file_id ид удаляемого файла
     * @param {Integer} table_flag value =
     *      1 - удаляем из таблицы isga_ApplicationFiles,
     *      2 - из таблицы isga_ApplicationDocumentGroups,
     *      3 - дропаем из поля recall в таблице isgaApplications
     * @param {Integer} inArhive =
     *      1 - не удаляем а отправляем в архив
     */
    public function deleteApplicationFilesAction()
    {
        $data = $_POST;
        $response = array();
        //проверим все ли параметры дошли
        $require_keys = array("table_flag", "file_id");
        foreach ($require_keys as $key) {
            if (!array_key_exists($key, $data) || empty($data[$key])) {
                $response['response'] = 'error';
                $response['message'] = 'Произошла ошибка, обратитесь к администратору';
                print json_encode($response);
                die;
            }
        }

        //проверим наличие файла в системе
        $path = "";
        $document_obj = "";
        switch ($data['table_flag']) {
            case '1':
                {
                    $document_obj = new Model\Entities\isgaApplicationFiles($data['file_id']);
                    if (!$document_obj) {
                        $response['response'] = 'error';
                        $response['message'] = 'Файл не найден в системе, обратитесь к администратору';
                        print json_encode($response);
                        die;
                    }
                    $path = $document_obj->getField("HashName");
                    break;
                }
            case '2':
                {
                    $document_obj = new Model\Entities\isgaApplicationDocumentGroups($data['file_id']);

                    if (!$document_obj) {
                        $response['response'] = 'error';
                        $response['message'] = 'Файл не найден в системе, обратитесь к администратору';
                        print json_encode($response);
                        die;
                    }

                    $path = $document_obj->getField("FtpDirectoryUrl");
                    break;
                }
            case '3':
                {
                    $appl = new Model\Entities\isgaApplications($data['file_id']);

                    if (!$appl) {
                        $response['response'] = 'error';
                        $response['message'] = 'Файл не найден в системе, обратитесь к администратору';
                        print json_encode($response);
                        die;
                    } else {
                        $path = $appl->getFieldSafe('RecallApplicationScan');
                        $appl->setFields(array("RecallApplicationScan" => NULL, "RecallApplicationScanFileName" => NULL));
                        $appl->save();
                    }
                    if (!empty($path) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $path)) {
                        unlink($path);
                    }
                    $response['response'] = 'ok';
                    print json_encode($response);
                    die;
                }
        }
        if (!empty($data['inArhive'])) {
            $document_obj->setField('isArhive', 1);
            $document_obj->save();
        } else {
            $document_obj->delete();
            if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $path)) {
                unlink($path);
            }
        }
        $response['response'] = 'ok';

        print json_encode($response);
        die;
    }

    /**
     * открытая - акредитация - заявления - карточка заявления - филиалы
     * url:  /eo/declaration/get-active-fillials/ид-заявлени
     *
     * @return ViewModel
     */
    public function getActiveFillialsAction()
    {
        $id = $this->params('id');
        $fillialsApplication_class = new Model\Gateway\EntitiesTable('isga_mtm_Application_Branches');
        $fillialsApplication = $fillialsApplication_class->getEntitiesByWhere(array('fk_isgaApplication' => $id));
        $view = new ViewModel();
        $filialsView = $this->getApplicationFillialsAction();
        $filials = $filialsView->getVariable('fillials');
        $filials = array_column($filials, 'Id');
        $filials = array_unique($filials);
        $filialsCount = count($filials);
        unset($filialsView);
        unset($filials);
        $view->setVariables(array(
            "fillials" => $fillialsApplication,
            'filialsCount' => $filialsCount,
            "sent" => isset($_POST['sent']) ? 1 : 0,
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * открытая - акредитация - заявления - карточка заявления - филиалы
     * url:  /eo/declaration/get-active-fillials-new/ид-заявлени
     *
     * @return ViewModel
     */
    public function getActiveFillialsNewAction()
    {
        $id = $this->params('id');
        $fillialsApplication_class = new Model\Gateway\EntitiesTable('isga_mtm_Application_Branches');
        $fillialsApplication = $fillialsApplication_class->getEntitiesByWhere(array('fk_isgaApplication' => $id));
        $view = new ViewModel();
        $filialsView = $this->getApplicationFillialsAction();
        $filials = $filialsView->getVariable('fillials');
        $filials = array_column($filials, 'Id');
        $filials = array_unique($filials);
        $filialsCount = count($filials);
        unset($filialsView);
        unset($filials);
        $view->setVariables(array(
            "idAppl" => $id,
            "fillials" => $fillialsApplication,
            'filialsCount' => $filialsCount,
            "sent" => isset($_POST['sent']) ? 1 : 0,
            'fillialsApplication_class' => $fillialsApplication_class
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function setNewAddressAction()
    {
        $id = $this->params()->fromPost('id');
        $address = $this->params()->fromPost('value');
        $fillialsApplication_class = new Model\Entities\isgamtmApplicationBranches($id);
        $fillialsApplication_class->setField('ApplicationAdres', $address);
        if ($fillialsApplication_class->save()) {
            echo 'ok';
        } else {
            echo 'error';
        }
        die;
    }

    public function getFillialsProgramsAction()
    {
        $id = $this->params('id');
        if (!$this->params('id2')) {
            $only_EP = $this->params('id2');
        } else {
            $only_EP = false;
        }

        $fillialsApplication_class = new Model\Gateway\EntitiesTable('isga_mtm_Application_Branches');
        $fillialsApplication = $fillialsApplication_class->getEntitiesByWhere(array('fk_isgaApplication' => $id));
        $application = new \Model\Entities\isgaApplications($id);
        $applicationIsDeny = $application->isOrderDeny();
        if ($applicationIsDeny) {
            $application->setProgramChekedInFalse();
        }
        //головной вуз пихаем первым
        $fillialsApplicationArray = array();
        $i = 1;
        //$application=new \Model\Entities\isgaApplications($id);
        $orgId = $application->getEducationalOrgId();
        $fillialsApplication->buffer();
        foreach ($fillialsApplication as $currentFilial) {
            $filId = $currentFilial->getField('fk_eiisEOBranch');
            if ($filId == $orgId) {
                $fillialsApplicationArray[0] = $currentFilial;
            } else {
                $fillialsApplicationArray[$i++] = $currentFilial;
            }
        }
        ksort($fillialsApplicationArray);
        $fillialsApplication = $fillialsApplicationArray;
        $view = new ViewModel();
        $view->setVariables(array(
                "fillials" => $fillialsApplication,
                'applicationIsDeny' => $applicationIsDeny,
                'onlyEP' => $only_EP
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    public function getEducationalProgramsAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $applicationProgramsClass = new Model\Gateway\EntitiesTable('isga_ApplicationPrograms');
        $ApplicationPrograms = $applicationProgramsClass->getEntitiesByWhere(array('fk_isgaEOBranches' => $id_org, 'fk_isgaApplication' => $id_application));

        $bufer = $ApplicationPrograms->buffer();

        // получение кодов УГС
        $ugs_codes = array();
        if (count($bufer)) {
            $programsIds = array();
            foreach ($bufer as $app_i) {
                $programsIds[$app_i->getField('Id')] = $app_i->getField('fk_eiisLicensedPrograms');
            }
            $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
            $currentApplication = $applicationsClass->getEntityById($id_application);
            $programs = $currentApplication->selectComplicated("eiis_LicensedPrograms",
                array('Id', 'fk_eiisEduProgram'),
                array('Id' => array_values($programsIds)))->toArray();
            $eduProgramIds = array_column($programs, 'fk_eiisEduProgram', 'Id');
            $eduPrograms = $currentApplication->selectComplicated("eiis_EduPrograms", array('Id', 'fk_eiisGS'),
                array('Id' => array_values($eduProgramIds)))->toArray();
            $isgaGSIds = array_column($eduPrograms, 'fk_eiisGS', 'Id');
            $isgaGSArray = $currentApplication->selectComplicated("isga_GroupSpecialities",
                array('Id', 'Name', 'Code', 'fk_isgaEGS'),
                array('Id' => array_values($isgaGSIds)))->toArray();
            $isgaEGSIds = array_column($isgaGSArray, 'fk_isgaEGS', 'Id');
            $isgaEGSArray = $currentApplication->selectComplicated("isga_EnlargedGroupSpecialities",
                array('Id', 'Name', 'Code'),
                array('Id' => array_values($isgaEGSIds)))->toArray();
            $isgaEGSArrayCode = array_column($isgaEGSArray, 'Code', 'Id');
            foreach ($bufer as $app_i) {
                $ugs_codes[$app_i->getField('Id')] = $isgaEGSArrayCode[$isgaEGSIds[$isgaGSIds[$eduProgramIds[$programsIds[$app_i->getField('Id')]]]]];
            }
        }

        $view = new ViewModel();
        $view->setVariables(array(
            "applicationPrograms" => $bufer,
            "ugs_codes" => $ugs_codes
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function addEducationProgramAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();

        if ($currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57' || $currentApplication->getType()->getField("Id") == strtoupper('b30d724a-17d1-4792-8c75-8f9db2a6ce57')) {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '67f97827-57f9-4a0c-b2c7-d9806969bef6') {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } elseif ($id_org == $currentApplication->getField('fk_eiisEducationalOrganization') && count($ReorgOrganizations) > 0 && $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '64771BDC-27C5-4C83-B93D-CB725435658B') {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            $prevCerticateReorg = $currentApplication->selectComplicated('isga_Certificates', array('Id'),
                array('fk_eiisEducationalOrganization' => $ReorgOrganizations, 'fk_isgaCertificateStatus' => 'CBA8E890-429A-4D6A-B81C-0F530A7970CC'))->toArray();
            $prevCerticateReorg = array_column($prevCerticateReorg, 'Id');
            array_push($prevCerticateReorg, $currentApplication->getField("fk_isgaPreviousCertificate"));
            array_push($ReorgOrganizations, $id_org);
            $eduProgramTypeIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $prevCerticateReorg, $ReorgOrganizations
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '691a08f0-a0d0-45a2-83e6-754df11fc697') {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } else {
            $eduProgramTypeIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $currentApplication->getField("fk_isgaPreviousCertificate"), $id_org
            );
        }
        if (count($eduProgramTypeIds) > 0) {
            $eduProgramTypes = $currentApplication->selectComplicated(
                "eiis_EduProgramTypes", array('fk_eiisEduLevels'),
                array(
                    'Id' => $eduProgramTypeIds,
                    'fk_eiisEduLevels IS NOT NULL',
                    "fk_eiisEduLevels != ''"
                )
            )->toArray();
        } else {
            $eduProgramTypes = array();
        }
        if (count($eduProgramTypes) > 0) {
            $eduLevelsIDs = array_unique(array_column($eduProgramTypes, 'fk_eiisEduLevels'));
            $eduLevels = $currentApplication->selectComplicated(
                "eiis_EduLevels", array('Id', 'Name', 'ShortName'), array('Id' => $eduLevelsIDs)
            )->toArray();
        } else {
            $eduLevels = array();
        }
        $organizationClass = new Model\Gateway\EntitiesTable('eiis_EducationalOrganizations');
        $currentOrganization = $organizationClass->getEntityById($id_org);
        $view = new ViewModel();
        $view->setVariables(array(
            "id_application" => $id_application,
            "eduLevels" => $eduLevels,
            "currOrganization" => $currentOrganization
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function editEducationProgramAction()
    {
        // тут похожая логика из addEducationProgramAction
        die;
        $view = new ViewModel();
        $view->setVariables(array("id_application" => $id_application, "programmTypes" => $programmTypes->toArray(), "currOrganization" => $currentOrganization));
        $view->setTerminal(true);
        return $view;
    }

    public function getEduProgramTypesByIdLevelAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        /* $id_level           = $this->params('id3'); */
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();
        if ($currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57') {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '67f97827-57f9-4a0c-b2c7-d9806969bef6') {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } elseif ($id_org == $currentApplication->getField('fk_eiisEducationalOrganization') && count($ReorgOrganizations) > 0 && $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '64771BDC-27C5-4C83-B93D-CB725435658B') {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            $prevCerticateReorg = $currentApplication->selectComplicated('isga_Certificates', array('Id'),
                array('fk_eiisEducationalOrganization' => $ReorgOrganizations, 'fk_isgaCertificateStatus' => 'CBA8E890-429A-4D6A-B81C-0F530A7970CC'))->toArray();
            $prevCerticateReorg = array_column($prevCerticateReorg, 'Id');
            array_push($prevCerticateReorg, $currentApplication->getField("fk_isgaPreviousCertificate"));
            array_push($ReorgOrganizations, $id_org);
            $eduProgramTypeIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $prevCerticateReorg, $ReorgOrganizations
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '691a08f0-a0d0-45a2-83e6-754df11fc697') {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } else {
            $eduProgramTypeIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $currentApplication->getField("fk_isgaPreviousCertificate"), $id_org
            );
        }
        $eduProgramTypes = $currentApplication->selectComplicated(
            "eiis_EduProgramTypes", array('Id', 'Name'),
            array('Id' => $eduProgramTypeIds, 'fk_eiisEduLevels !=""'/* => $id_level */)
        );
        $view = new ViewModel();
        $view->setVariables(array("eduProgramTypes" => $eduProgramTypes->toArray()));
        $view->setTerminal(true);
        return $view;
    }

    public function getDataUgsAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $id_program_type = $this->params('id3');
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();
        if ($currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57') {
            $eduProgramIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'fk_eiisEduProgram', $id_program_type
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '67f97827-57f9-4a0c-b2c7-d9806969bef6') {
            $eduProgramIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'fk_eiisEduProgram', $id_program_type
            );
        } elseif ($id_org == $currentApplication->getField('fk_eiisEducationalOrganization') && count($ReorgOrganizations) > 0 && $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '64771BDC-27C5-4C83-B93D-CB725435658B') {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            $prevCerticateReorg = $currentApplication->selectComplicated('isga_Certificates', array('Id'),
                array('fk_eiisEducationalOrganization' => $ReorgOrganizations, 'fk_isgaCertificateStatus' => 'CBA8E890-429A-4D6A-B81C-0F530A7970CC'))->toArray();
            $prevCerticateReorg = array_column($prevCerticateReorg, 'Id');
            array_push($prevCerticateReorg, $currentApplication->getField("fk_isgaPreviousCertificate"));
            array_push($ReorgOrganizations, $id_org);
            $eduProgramIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $prevCerticateReorg, $ReorgOrganizations, 'fk_eiisEduProgram', $id_program_type
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '691a08f0-a0d0-45a2-83e6-754df11fc697') {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } else {
            $eduProgramIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $currentApplication->getField("fk_isgaPreviousCertificate"), $id_org, 'fk_eiisEduProgram',
                $id_program_type
            );
        }

        $eduProgramsClass = new Model\Entities\eiisEduPrograms();
        $eduPrograms = $eduProgramsClass->selectComplicated(
            "eiis_EduPrograms", array('Id', 'fk_eiisUGS'), array('Id' => $eduProgramIds, 'fk_eiisUGS IS NOT NULL')
        )->toArray();

        $isgaEGSIds = array_column($eduPrograms, 'fk_eiisUGS');
        $enlargedGroupSpecialitiesClass = new Model\Entities\isgaEnlargedGroupSpecialities();
        $enlargedGroupSpecialities = $enlargedGroupSpecialitiesClass->selectComplicated(
            "isga_EnlargedGroupSpecialities", array('Id', 'Code', 'Name'), array('Id' => $isgaEGSIds)
        )->toArray();
        $view = new ViewModel();
        $view->setVariables(array("enlargedGroupSpecialities" => $enlargedGroupSpecialities));
        $view->setTerminal(true);
        return $view;
    }

    public function getEduProgramByIdUgsAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $id_program_type = $this->params('id3');
        $id_egs = $this->params('id4');

        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();
        if ($currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57') {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '67f97827-57f9-4a0c-b2c7-d9806969bef6') {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } elseif ($id_org == $currentApplication->getField('fk_eiisEducationalOrganization') && count($ReorgOrganizations) > 0 && $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '64771BDC-27C5-4C83-B93D-CB725435658B') {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            $prevCerticateReorg = $currentApplication->selectComplicated('isga_Certificates', array('Id'),
                array('fk_eiisEducationalOrganization' => $ReorgOrganizations, 'fk_isgaCertificateStatus' => 'CBA8E890-429A-4D6A-B81C-0F530A7970CC'))->toArray();
            $prevCerticateReorg = array_column($prevCerticateReorg, 'Id');
            array_push($prevCerticateReorg, $currentApplication->getField("fk_isgaPreviousCertificate"));
            array_push($ReorgOrganizations, $id_org);
            $licensedProgramsIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $prevCerticateReorg, $ReorgOrganizations, 'Id', $id_program_type
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && $currentApplication->getField("fk_isgaApplicationReason") == '691a08f0-a0d0-45a2-83e6-754df11fc697') {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } else {
            $licensedProgramsIds = $appClass->getLicensedProgramsByCertificateFieldIds(
                $currentApplication->getField("fk_isgaPreviousCertificate"), $id_org, 'Id', $id_program_type
            );
        }
        $eiisLicensedProgramsClass = new Model\Entities\eiisLicensedPrograms();

        $eduPrograms = $eiisLicensedProgramsClass->selectComplicated(
            "eiis_EduPrograms", array('Id'), array('fk_eiisUGS' => $id_egs)
        )->toArray();
        $eduProgramIds = array_column($eduPrograms, 'Id');

        $licensedPrograms = $eiisLicensedProgramsClass->selectComplicated(
            "eiis_LicensedPrograms", array('Id', 'Name', 'Code', 'Period'),
            array(
                'Id' => $licensedProgramsIds,
                'fk_eiisEduProgram' => $eduProgramIds,
                'fk_eiisEduProgramType' => $id_program_type,
            ), NULL, 'Code ASC'
        );

        $view = new ViewModel();
        $view->setVariables(array(
            "licensedPrograms" => $licensedPrograms
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function saveApplicationProgramAction()
    {
        $request = $this->getRequest();
        $applicationProgramsClass = new Model\Entities\isgaApplicationPrograms();
        $applicationProgramsClass->exchangeArray($request->getPost()->toArray());
        echo $applicationProgramsClass->save(1);
        die;
    }

    public function deleteDeclarationAction()
    {
        $id_application = $this->params('id');
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');

        $currentApplication = $applicationsClass->getEntityById($id_application);

        $MoreInfo = $currentApplication->getLinkedItem('isga_ApplicationMoreInfo',
            array('fk_isgaApplication' => $id_application));
        if ($MoreInfo != false)
            $MoreInfo->delete();

        $ReorgOrganizations = $currentApplication->getLinkedItems('isga_mtm_Application_Organizations',
            array('fk_isgaApplication' => $id_application))->current();
        if ($ReorgOrganizations != false)
            $ReorgOrganizations->deleteWhere(array('fk_isgaApplication' => $id_application));

        $Files = $currentApplication->getLinkedItems('isga_ApplicationFiles',
            array('fk_isgaApplication' => $id_application))->current();
        if ($Files != false)
            $Files->deleteWhere(array('fk_isgaApplication' => $id_application));

        $Programs = $currentApplication->getLinkedItems('isga_ApplicationPrograms',
            array('fk_isgaApplication' => $id_application))->current();
        if ($Programs != false)
            $Programs->deleteWhere(array('fk_isgaApplication' => $id_application));

        $Branches = $currentApplication->getLinkedItems('isga_mtm_Application_Branches',
            array('fk_isgaApplication' => $id_application))->current();
        if ($Branches != false)
            $Branches->deleteWhere(array('fk_isgaApplication' => $id_application));

        if ($currentApplication != false)
            $currentApplication->delete();
        die;
    }

    public function deleteApplicationProgramAction()
    {
        $id_program = $this->params('id');
        $applicationProgramsClass = new Model\Entities\isgaApplicationPrograms();
        $applicationProgramsClass->setFields(array('Id' => $id_program));
        $applicationProgramsClass->delete();
        die;
    }

    public function deleteOldFileAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;
        $idFile = $this->params('id');
        $applicationFiles_class = new Model\Gateway\EntitiesTable('isga_ApplicationFiles');
        $currFile = $applicationFiles_class->getEntityById($idFile);
        $filename = $currFile->getField('HashName');
        $filepath = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/';


        //delete info from sign table
        $sign_info_class = new Model\Entities\isgaSignInfoFiles();
        $sign_info_class->deleteWhere(array('fk_isgaAppplicationFileId' => $idFile));

        if (file_exists($filepath . $filename)) {
            unlink($filepath . $filename);
            $currFile->delete();
        } else {
            echo 'error';
            $currFile->delete();
        }
        echo $idFile;
        die;
    }

    public function getApplicationExtraDocumentFileAction()
    {

        $id = $this->params('id');
        $applicationExtraDocument = ApplicationFileCorrupt::where('id',$id)->get()->first();
        if(!empty($applicationExtraDocument)){

        }
        $path = $applicationExtraDocument->HashName;
        $response = new \Zend\Http\Response\Stream();
        $fileName = explode("/",$path);

        $response->setStream(fopen($path, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename(end($fileName)));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($applicationExtraDocument->RealName) . '"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($path),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);

        return $response;

    }

    public function getApplicationCurruptAction()
    {

        $id = $this->params('id');
        $applicationExtraDocument = ApplicationFileCorrupt::where('id',$id)->get()->first();
        if(!empty($applicationExtraDocument)){

        }
        $path = $applicationExtraDocument->HashName;
        $response = new \Zend\Http\Response\Stream();
        $fileName = explode("/",$path);

        $response->setStream(fopen($path, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename(end($fileName)));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($applicationExtraDocument->RealName) . '"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($path),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);

        return $response;

    }



    public function applicationFilesAction()
    {
        $id_appl = $this->params('id');
        $applicationExtraDocument = ApplicationExtraDocument::where('fk_isgaApplication',$id_appl)->get()->toArray();
        $IsgaNotification = IsgaNotification::where('fk_isgaApplication',$id_appl)->get();
        $complicatedSelect = new Model\Entities\isgamtmDocumentReason();
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/ajaxupload.js');

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();

        $this->layout()->identityUser = $user;
        $this->layout()->educationOrganization = $educationOrganization;
        $orgData = $educationOrganization->getEducationalOrganizationData();
        $orgData = ($orgData !== null && $orgData !== false) ? $orgData->toArray() : array();
        //Если нет записи в таблице или не задана спецификация, то задаем спецификацию
        if (!isset($orgData['fk_isgaEducationalOrganizationSpecific']) || empty($orgData['fk_isgaEducationalOrganizationSpecific']) || $orgData['fk_isgaEducationalOrganizationSpecific'] == null)
            $orgData['fk_isgaEducationalOrganizationSpecific'] = "1";

        $application_class = new Model\Gateway\EntitiesTable('isga_Applications');
        $currAppl = $application_class->getEntityById($id_appl);
        $idsReasons = $currAppl->getIdsReasons();
        $mtm_DocumentType_class = new Model\Gateway\EntitiesTable('isga_mtm_Document_Reason');
        $Documents = $complicatedSelect->selectComplicated('isga_mtm_Document_Reason',
            array('fk_isgaDocumentTypes', 'Sorting', 'Obligation'),
            array('isga_mtm_Document_Reason.fk_isgaEducationalOrganizationSpecifics' => $orgData['fk_isgaEducationalOrganizationSpecific'], 'isga_mtm_Document_Reason.fk_isgaApplicationReasons' => $idsReasons[0]),
            NULL, 'isga_mtm_Document_Reason.Sorting ASC', NULL, NULL,
            'isga_DocumentTypes',
            'isga_mtm_Document_Reason.fk_isgaDocumentTypes = isga_DocumentTypes.Id',
            array('Id', 'Name'))->toArray();
        $currFile = $complicatedSelect->selectComplicated(
            "isga_ApplicationFiles",
            array('Id', 'RealName', 'HashName', 'fk_isgaDocumentType', 'isArhive'),
            array('fk_isgaApplication' => $id_appl),
            NULL,
            NULL,
            NULL,
            NULL,
            "isga_SignInfoFiles",
            "isga_SignInfoFiles.fk_isgaAppplicationFileId = isga_ApplicationFiles.Id",
            array("sign_id" => "Id", "valid" => "valid", 'date_verify' => 'date_verify', 'certificate_info' => 'certificate_info'),
            "left"
        );
        /* $currFile = $complicatedSelect->selectComplicated("isga_ApplicationFiles",
                                                           array('Id', 'RealName', 'HashName', 'fk_isgaDocumentType'),
                                                           array('fk_isgaApplication' => $id_appl));*/

        $appStatus = $currAppl->getFieldOrSpace('fk_isgaApplicationStatus');

        /* получим дополнительные документы из таблицы isga_ApplicationDocumentGroups */
        $application_document_groups_class = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentGroups');
        $application_document_obj = $application_document_groups_class->getEntitiesByWhere(array("fk_isgaApplication" => $id_appl));
        $app_doc_docs = array();
        $isgaApplicationDocumentType = array();
        $application_document_types = array();
        foreach ($application_document_obj as $ado) {
            $isgaApplicationDocumentType[] = $ado->getField('fk_isgaApplicationDocumentType');
            $ftp_directory = $ado->getField('FtpDirectoryUrl');
            if (!empty($ftp_directory)) {
                $app_doc_docs[mb_strtolower($ado->getField('fk_isgaApplicationDocumentType'), 'UTF-8')][] = array(
                    "FtpDirectoryUrl" => $ftp_directory,
                    "RealFileName" => $ado->getField('RealFileName'),
                    "Id" => $ado->getField('Id'),
                    'isArhive' => $ado->getFieldOrSpace('isArhive'),
                    'CreatedDate' => $ado->getFieldOrSpace('CreatedDate'),
                );
            }
        }
        if (count($app_doc_docs) > 0) {
            $application_document_types_class = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentTypes');
            $application_document_types_obj = $application_document_types_class->getEntitiesByWhere(array("id" => $isgaApplicationDocumentType));
            foreach ($application_document_types_obj as $adt) {
                $application_document_types[mb_strtolower($adt->getField('Id'), 'UTF-8')] = $adt->getField('Name');
            }
        }
        //получим массив типов документов которые не входят в диапазон причинных связей
        $document_types = array();
        foreach ($Documents as $doc_reason) {
            $document_types[] = $doc_reason['fk_isgaDocumentTypes'];
        }
        $application_types = array();
        $oldDoc = $currFile->toArray();
        foreach ($oldDoc as $docs) {
            if (!in_array($docs['fk_isgaDocumentType'], $document_types)) {
                $application_types[] = $docs['fk_isgaDocumentType'];
            }
        }
        $application_types_without_reason = array();
        if (count($application_types) > 0) {
            $document_types_class = new Model\Gateway\EntitiesTable('isga_DocumentTypes');
            $document_types_obj = $document_types_class->getEntitiesByWhere(array("Id" => $application_types));
            foreach ($document_types_obj as $dt) {
                $application_types_without_reason[mb_strtolower($dt->getField('Id'), 'UTF-8')] = $dt->getField('Name');
            }
        }

        //TODO get data from DB

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $max_count_uploaded_files = $user->getField("max_count_upload_files");
        $max_count_uploaded_files = $max_count_uploaded_files ? $max_count_uploaded_files : 5;

           $files = ApplicationExtraDocument::where(['expert_id' => $user->getId()])->get();

           $extra = ApplicationFileCorrupt::where(['fk_isgaApplication' => $id_appl])->get();
        $view = new ViewModel();
        $application_file_corrupt = ApplicationFileCorrupt::where('fk_isgaApplication', $id_appl)->get();
//        var_dump($extra->toArray());
//        die;
        $view->setVariables(
            array(
                'extra' => $extra->toArray(),
                'application_file_corrupt' => $application_file_corrupt,
                'files' => $files,
                'userId' => $user->getId(),
                "id_appl" => $id_appl,
                'appStatus' => $appStatus,
                "cur_appl" => $currAppl,
                'arrayDocs' => $Documents,
                'oldDoc' => $oldDoc,
                "dop_docs" => $app_doc_docs,
                'application_document_types' => $application_document_types,
                'application_types_without_reason' => $application_types_without_reason,
                'userRole' => $user->getRole(),
                "typeSend" => isset($_REQUEST['type']) ? $_REQUEST['type'] : false,
                'RecallScan' => array("path" => $currAppl->getField('RecallApplicationScan'), "name" => $currAppl->getField('RecallApplicationScanFileName')),
                'max_count_uploaded_files' => $max_count_uploaded_files,
                'applicationExtraDocument' => $applicationExtraDocument
            ));
        $view->setTerminal(true);
        return $view;
    }

    public function getEduOrgAction()
    {
        $val = $this->params('id2');

        $page = $this->params('id');
        if (!empty($val)) {
            $search = $val;
        } else {
            $search = ' ';
        }
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization = $user->getEducationOrganization();
        $this->layout()->educationOrganization = $educationOrganization;
        $this->layout()->identityUser = $user;
        $organizationClass = new Model\Entities\eiisEducationalOrganizations();
        $countAll = $organizationClass->selectComplicated("eiis_EducationalOrganizations", array('Id'),
            '`FullName` LIKE "%' . $search . '%" OR `ShortName` LIKE "%' . $search . '%" OR `RegularName` LIKE "%' . $search . '%" OR `Inn` ="' . $search . '"');
        $orgArray = $organizationClass->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName'),
            '`FullName` LIKE "%' . $search . '%" OR `ShortName` LIKE "%' . $search . '%" OR `RegularName` LIKE "%' . $search . '%" OR `Inn` ="' . $search . '"',
            NULL, NULL, 20, $page);
        $view = new ViewModel();
        $maxPage = ceil(count($countAll) / 20);
        $view->setVariables(array("orgs" => $orgArray->toArray(), 'search' => $search, 'nextPage' => $page + 1, 'maxPage' => $maxPage));
        $view->setTerminal(true);
        return $view;
    }

    //Экспорт данных в .doc docx pdf odt xml файл
    public function exportDataAction()
    {
        //ini_set('error_reporting', E_ALL);
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors', 1);
        $newDate = $this->params('id3');
        $exportIn = array('doc', 'docx', 'pdf', 'odt');
        $outDocType = $this->params('id');
        $application_id = $this->params('id2');
        if (!isset($outDocType) || !isset($application_id)) {
            echo 'Не указан документ или тип докуента';
            die;
            return false;
        }
        $outDocType = strtolower($outDocType);
        if (in_array($outDocType, $exportIn)) {
            $application = \Eo\Model\ExportDocument\ExportDocument::getDocumentByIdApplication($application_id);
            if (!$application) {
                die ('Не найдено заяление с таким ид');
                return false;
            }
            $array_data = $application->getData();

            if (!empty($newDate)) {
                //возьмем сервис сертификатов - для удобства конвертации даты
                $certServ = new \Eo\Service\certificateExcerptService();
                $array_data['fieldDateDay'] = $certServ->getDay($newDate);
                $array_data['fieldDateMounth'] = $certServ->getMonth($newDate);
                $array_data['fieldDateYear'] = $certServ->getYear($newDate);
            }

            $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
            $templateName = $array_data['templType'] . '.docx'; //шаблон документа
            $fileOutName = ucfirst($array_data['templName'] . ' от '
                . date('d.m.Y'/*, strtotime($array_data['dateCreate'])*/)
                . '.' . $outDocType); //имя выходного файла
            $result = $application->templateMakeDoccument(
                $array_data,
                $templatePath . $templateName,
                $outDocType
            );
        } elseif ($outDocType == 'xml') {
            $xml = \Eo\Model\ExportXml::getXml($application_id);
            $result = $xml->getXmlContent();
            $fileOutName = 'Заявление от ' . $xml->getFormData('DateCreate') . '.xml'; //имя выходного файла
        } else {
            die ('Не указан тип документа');
            return false;
        }
        //возвращаем файл пользователю
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . '"' . $fileOutName . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . strlen($result));
        //header('Pragma: no-cache');//глюко ошибка
        header('Cache-Control: no-store, no-cache, must-revalidate');
        clearstatcache();
        echo $result; //  отправляем  данные
        die;
    }

    //Вставляем текущую дату в шаблон
    public function editDateAction($content)
    {
        $content = preg_replace("/date_day/", date('d'), $content);
        $content = preg_replace("/date_month/", $this->months[date('m')], $content);
        $content = preg_replace("/date_year/", date('Y'), $content);
        return $content;
    }

    static public function translit($url, $title)
    {
        if ($url == '') {
            $url = $title;
        }
        $space = '-';
        $transl = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h',
            'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => $space, 'ы' => 'y', 'ь' => $space, 'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            'А' => 'a', 'Б' => 'b', 'В' => 'v', 'Г' => 'g', 'Д' => 'd', 'Е' => 'e', 'Ё' => 'e', 'Ж' => 'zh',
            'З' => 'z', 'И' => 'i', 'Й' => 'j', 'К' => 'k', 'Л' => 'l', 'М' => 'm', 'Н' => 'n',
            'О' => 'o', 'П' => 'p', 'Р' => 'r', 'С' => 's', 'Т' => 't', 'У' => 'u', 'Ф' => 'f', 'Х' => 'h',
            'Ц' => 'c', 'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'sh', 'Ъ' => $space, 'Ы' => 'y', 'Ь' => $space, 'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya',
            ' ' => $space, '_' => $space, '`' => $space, '~' => $space, '!' => $space, '@' => $space,
            '#' => $space, '$' => $space, '%' => $space, '^' => $space, '&' => $space, '*' => $space,
            '(' => $space, ')' => $space, '-' => $space, '\=' => $space, '+' => $space, '[' => $space,
            ']' => $space, '\\' => $space, '|' => $space, '/' => $space, '.' => $space, ',' => $space,
            '{' => $space, '}' => $space, '\'' => $space, '"' => $space, ';' => $space, ':' => $space,
            '?' => $space, '<' => $space, '>' => $space, '№' => $space);

        return strtr($url, $transl);
    }

    public function testAction()
    {
        $application_class = new Model\Entities\isgaApplications();
        $res = $application_class->getBarcode();
        var_dump($res);
        die;
    }

    public function backAction()
    {
        $applicationId = $this->params('id');
        if (!isset($applicationId)) {
            die;
        }
        $request = $this->getRequest();
        $post = $request->getPost();
        if (isset($post->action) || $post->action == 'save') {
            $applicationBackClass = new \Model\Entities\isgaApplicationBack();
            $historyClass = new \Model\Entities\isgaHistoryStatus();
            $applicationsClass = new \Model\Entities\isgaApplications($applicationId);

            $saveData['fk_isgaApplication'] = $applicationId;
            //$saveData['Reason'] = $post->backReasonText;
            $saveData['DateCreate'] = date('Y-m-d');
            $applicationBackClass->exchangeArray($saveData);
            $result = $applicationBackClass->save(1);
            $saveHistory = array(
                'StatusOld' => $applicationsClass->getField('fk_isgaApplicationStatus'),
                'StatusNew' => 'FD365DDE-2B56-4CE2-A241-356DCCFB3C14',
                'fk_isgaApplication' => $applicationId,
            );
            $historyClass->exchangeArray($saveHistory);
            $historyClass->save(1);
            $applicationsClass->exchangeArray(array('fk_isgaApplicationStatus' => 'FD365DDE-2B56-4CE2-A241-356DCCFB3C14'));
            $applicationsClass->save();
            if ($result) {
                echo 'Ok';
            } else {
                echo 'error';
            }
            die;
        }
        $applicationBackEntities = new \Model\Gateway\EntitiesTable('isga_ApplicationBack');
        $applicationBackData = $applicationBackEntities->getEntityById($applicationId, 'fk_isgaApplication');
        $view = new ViewModel();
        $view->setTerminal(true);
        if ($applicationBackData) {
            $backFile = $applicationBackData->getLinkedItem(
                'isga_ApplicationFiles',
                array('fk_isgaApplication' => $applicationId,
                    'fk_isgaDocumentType' => 'BC56DC56-C34A-4556-ABCD-AB45D6CD3CDA',
                )
            );
            $view->setVariables(array(
                'applicationBack' => $applicationBackData,
                'backFile' => $backFile,
            ));
            //$request = $this->getRequest();
            //$post = $request->getPost();
            //echo 'available';
            //die;
        }
        return $view;
    }

    /* Новый блок выбора ОП */
    public function getEduProgramTypeAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();
        if ($currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57' || in_array('f3923905-c349-4f87-af80-8660361537dc',
                $currentApplication->getIdsReasons())) {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('67f97827-57f9-4a0c-b2c7-d9806969bef6',
                $currentApplication->getIdsReasons())) {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } elseif ($id_org == $currentApplication->getField('fk_eiisEducationalOrganization') && count($ReorgOrganizations) > 0 && $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('64771BDC-27C5-4C83-B93D-CB725435658B',
                $currentApplication->getIdsReasons())) {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            /* $prevCerticateReorg = $currentApplication->selectComplicated('isga_Certificates', array('Id'), array('fk_eiisEducationalOrganization' => $ReorgOrganizations, 'fk_isgaCertificateStatus' => 'CBA8E890-429A-4D6A-B81C-0F530A7970CC'))->toArray();
              $prevCerticateReorg = array_column($prevCerticateReorg, 'Id');
              array_push($prevCerticateReorg, $currentApplication->getField("fk_isgaPreviousCertificate")); */
            array_push($ReorgOrganizations, $id_org);
            /* $eduProgramTypeIds  = $appClass->getLicensedProgramsByCertificateFieldIds(
              $prevCerticateReorg, $ReorgOrganizations
              ); */
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $ReorgOrganizations
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('691a08f0-a0d0-45a2-83e6-754df11fc697',
                $currentApplication->getIdsReasons())) {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
        } else {
            $eduProgramTypeIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org
            );
            /* $eduProgramTypeIds = $appClass->getLicensedProgramsByCertificateFieldIds(
              $currentApplication->getField("fk_isgaPreviousCertificate"), $id_org
              ); */
        }
        $eduProgramTypes = array();
        if (!empty($eduProgramTypeIds))
            $eduProgramTypes = $currentApplication->selectComplicated(
                "eiis_EduProgramTypes", array('Id', 'Name'), array('Id' => $eduProgramTypeIds, 'fk_eiisEduLevels !=""')
            )->toArray();
        $applicationProgramsClass = new Model\Gateway\EntitiesTable('isga_ApplicationPrograms');

        $view = new ViewModel();
        $view->setVariables(
            array(
                "eduProgramTypes" => $eduProgramTypes,
                "applicationProgramsClass" => $applicationProgramsClass,
                "id_org" => $id_org,
                "id_application" => $id_application
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    /**
     * открытая = акредитация - заявления - карточка заявленгия - программы - табличка программ - заполняем уровки программ
     *
     * url:  /eo/declaration/get-ugs-by-level/ид-за-ния/ид-орг-ции/ид-типа-прог-мы
     * view: /module/Eo/view/eo/declaration/get-ugs-by-level.phtml
     *
     * @return ViewModel
     */
    public function getUgsByLevelAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $id_program_type = $this->params('id3');
        if ($id_program_type == 'undefined')
            $id_program_type = NULL;
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();
        if (
            $currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57' ||
            in_array('f3923905-c349-4f87-af80-8660361537dc', $currentApplication->getIdsReasons())
        ) {
            $eduProgramIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'fk_eiisEduProgram', $id_program_type, 1
            );
        } elseif (
            $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' &&
            in_array('67f97827-57f9-4a0c-b2c7-d9806969bef6', $currentApplication->getIdsReasons())
        ) {
            $eduProgramIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'fk_eiisEduProgram', $id_program_type, 1
            );
        } elseif (
            $id_org == $currentApplication->getField('fk_eiisEducationalOrganization') &&
            count($ReorgOrganizations) > 0 &&
            $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' &&
            in_array('64771BDC-27C5-4C83-B93D-CB725435658B', $currentApplication->getIdsReasons())
        ) {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            /* $prevCerticateReorg = $currentApplication->selectComplicated('isga_Certificates', array('Id'), array('fk_eiisEducationalOrganization' => $ReorgOrganizations, 'fk_isgaCertificateStatus' => 'CBA8E890-429A-4D6A-B81C-0F530A7970CC'))->toArray();
              $prevCerticateReorg = array_column($prevCerticateReorg, 'Id');
              array_push($prevCerticateReorg, $currentApplication->getField("fk_isgaPreviousCertificate")); */
            array_push($ReorgOrganizations, $id_org);
            $eduProgramIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $ReorgOrganizations, 'fk_eiisEduProgram',
                $id_program_type, 1
            );
        } elseif (
            $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' &&
            in_array('691a08f0-a0d0-45a2-83e6-754df11fc697', $currentApplication->getIdsReasons())
        ) {
            $eduProgramIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'fk_eiisEduProgram', $id_program_type, 1
            );
        } else {
            $eduProgramIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'fk_eiisEduProgram', $id_program_type, 1
            );
        }

        $eduProgramsClass = new Model\Entities\eiisEduPrograms();
        if (empty($eduProgramIds)) {
            $eduPrograms = array();
        } else {
            $eduPrograms = $eduProgramsClass->selectComplicated(
                "eiis_EduPrograms", array('Id', 'fk_eiisUGS'), array('Id' => $eduProgramIds, 'fk_eiisUGS IS NOT NULL')
            )->toArray();
        }

        $isgaEGSIds = array_column($eduPrograms, 'fk_eiisUGS');
        $enlargedGroupSpecialitiesClass = new Model\Entities\isgaEnlargedGroupSpecialities();
        if (empty($isgaEGSIds)) {
            $enlargedGroupSpecialities = array();
        } else {
            $enlargedGroupSpecialities = $enlargedGroupSpecialitiesClass->selectComplicated(
                "isga_EnlargedGroupSpecialities", array('Id', 'Code', 'Name'), array('Id' => $isgaEGSIds), null,
                "Code ASC"
            )->toArray();
        }

        $appClass = new \Model\Entities\isgaApplications($id_application);
        $appIsShortPath = $appClass->isShortPath();

        $applicationProgramsClass = new Model\Gateway\EntitiesTable('isga_ApplicationPrograms');
        $view = new ViewModel();
        $view->setVariables(array(
            "enlargedGroupSpecialities" => $enlargedGroupSpecialities,
            'applicationProgramsClass' => $applicationProgramsClass,
            "org" => $id_org,
            "programtype" => $id_program_type,
            "sent" => isset($_POST['sent']) ? 1 : 0,
            "application" => $id_application,
            'appIsShortPath' => $appIsShortPath,
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function getDirectByUgsAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $id_program_type = $this->params('id3');
        $id_egs = $this->params('id4');

        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();
        if ($currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57') {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('67f97827-57f9-4a0c-b2c7-d9806969bef6',
                $currentApplication->getIdsReasons()) || in_array('f3923905-c349-4f87-af80-8660361537dc',
                $currentApplication->getIdsReasons())) {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } elseif ($id_org == $currentApplication->getField('fk_eiisEducationalOrganization') && count($ReorgOrganizations) > 0 && $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('64771BDC-27C5-4C83-B93D-CB725435658B',
                $currentApplication->getIdsReasons())) {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            /* $prevCerticateReorg  = $currentApplication->selectComplicated('isga_Certificates', array('Id'), array('fk_eiisEducationalOrganization' => $ReorgOrganizations, 'fk_isgaCertificateStatus' => 'CBA8E890-429A-4D6A-B81C-0F530A7970CC'))->toArray();
              $prevCerticateReorg  = array_column($prevCerticateReorg, 'Id');
              array_push($prevCerticateReorg, $currentApplication->getField("fk_isgaPreviousCertificate")); */
            array_push($ReorgOrganizations, $id_org);
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $ReorgOrganizations, 'Id'
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('691a08f0-a0d0-45a2-83e6-754df11fc697',
                $currentApplication->getIdsReasons())) {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } else {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        }
        $eiisLicensedProgramsClass = new Model\Entities\eiisLicensedPrograms();

        $eduPrograms = $eiisLicensedProgramsClass->selectComplicated(
            "eiis_EduPrograms", array('Id', 'Code', 'fk_eiisEduProgramType'), array('fk_eiisUGS' => $id_egs)
        )->toArray();
        $Codes = array_column($eduPrograms, 'Code');
        $licensedPrograms = $eiisLicensedProgramsClass->selectComplicated(
            "eiis_LicensedPrograms", array('Id', 'Name', 'Code', 'Period'),
            array(
                'Id' => $licensedProgramsIds,
                'Code' => $Codes,
                'fk_eiisEduProgramType' => $id_program_type,
            ), array('Code', 'Name'), 'Code ASC'
        );

        $view = new ViewModel();
        $view->setVariables(array(
            "licensedPrograms" => $licensedPrograms,
            "id_egs" => $id_egs
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * рон-заявлени - карточка заявлени - программы
     * рон-заявлени - карточка заявлени - образдовательные программы для лишения
     * рон-заявлени - карточка заявлени -  исключения ОП
     *
     * добавление программ
     *
     * url:  /ron/declaration/save-checked-direct
     */
    public function saveCheckedDirectAction()
    {
        $request = $this->getRequest();
        $data = $request->getPost()->toArray();
        $applicationProgramsClass = new Model\Entities\isgaApplicationPrograms();
        $sortMaxProgramm = $applicationProgramsClass->getByWhere(
            'fk_isgaEnlargedGroupSpecialities = "' . $data['ugs_id'] . '" AND
            fk_isgaApplication = "' . $data['appl'] . '" AND
            fk_isgaEOBranches = "' . $data['org'] . '" AND
            fk_eiisEduProgramTypes = "' . $data['level'] . '"
            ORDER BY SortOP DESC'
        );
        if (!empty($sortMaxProgramm)) {
            $sort = (int)$sortMaxProgramm->getField('SortOP') + 1;
        } else {
            $sort = 0;
        }
        foreach ($data['direct'] as $oneDir) {
            $oneArr = array(
                'fk_isgaEnlargedGroupSpecialities' => $data['ugs_id'],
                'fk_isgaApplication' => $data['appl'],
                'fk_eiisLicensedPrograms' => $oneDir,
                'fk_isgaEOBranches' => $data['org'],
                'fk_eiisEduProgramTypes' => $data['level'],
                'SortOP' => $sort
            );
            if ($data["PrLish"]) {
                $oneArr["PrLish"] = (int)$data["PrLish"];
                $oneArr["Checked"] = 0;
            }
            $applicationProgramsClass = new Model\Entities\isgaApplicationPrograms();
            $applicationProgramsClass->setFieldsSafe($oneArr);
            $applicationProgramsClass->save(1);
        }
        echo 'Ok';
        die;
    }

    public function saveAllDirectAction()
    {
        $request = $this->getRequest();
        $data = $request->getPost()->toArray();
        $id_application = $data['id_appl'];
        $id_org = $data['id_org'];
        $arr_level = $data['arr_level'];

        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations', array('fk_eiisEducationalOrganization'), array('fk_isgaApplication' => $id_application))->toArray();
        if ($currentApplication->getType()->getField("Id") == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57') {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('67f97827-57f9-4a0c-b2c7-d9806969bef6', $currentApplication->getIdsReasons()) || in_array('f3923905-c349-4f87-af80-8660361537dc', $currentApplication->getIdsReasons())) {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } elseif ($id_org == $currentApplication->getField('fk_eiisEducationalOrganization') && count($ReorgOrganizations) > 0 && $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('64771BDC-27C5-4C83-B93D-CB725435658B', $currentApplication->getIdsReasons())) {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            array_push($ReorgOrganizations, $id_org);
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $ReorgOrganizations, 'Id'
            );
        } elseif ($currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' && in_array('691a08f0-a0d0-45a2-83e6-754df11fc697', $currentApplication->getIdsReasons())) {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        } else {
            $licensedProgramsIds = $appClass->getLicensedProgramsFieldIds(
                $currentApplication->getField("fk_eiisLicense"), $id_org, 'Id'
            );
        }

        $eiisLicensedProgramsClass = new Model\Entities\eiisLicensedPrograms();

        $licensedPrograms = $eiisLicensedProgramsClass->getAllByWhere(
            'Id IN ("' . implode('","', $licensedProgramsIds) . '") AND
            fk_eiisEduProgramType IN ("' . implode('","', $arr_level) . '")
            GROUP BY Code'
        );
        foreach ($licensedPrograms as $oneProgram) {
            $oneArr = array(
                'fk_isgaEnlargedGroupSpecialities' => $oneProgram->getUgsId(),
                'fk_isgaApplication' => $id_application,
                'fk_eiisLicensedPrograms' => $oneProgram->getField('Id'),
                'fk_isgaEOBranches' => $id_org,
                'fk_eiisEduProgramTypes' => $oneProgram->getField('fk_eiisEduProgramType')
            );
            $applicationProgramsClass = new Model\Entities\isgaApplicationPrograms();
            $applicationProgramsClass->exchangeArray($oneArr);
            $applicationProgramsClass->save(1);
        }
        echo 'Ok';
        die;
    }


    /**
     * открытая - аккредитация - заялвения - карточка заявления - программы - табличка программ, выбранные программы
     *
     * url:  /eo/declaration/get-program-by-ugs/ид-за-ния/ид-орг-ции/ид-типа-прог-мы/ид-угс
     * view: /module/Eo/view/eo/declaration/get-program-by-ugs.phtml
     */
    public function getProgramByUgsAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $id_program_type = $this->params('id3');
        $id_egs = $this->params('id4');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $applicationProgramsClass = new Model\Gateway\EntitiesTable('isga_ApplicationPrograms');
        $where = array(
            'fk_isgaApplication' => $id_application,
            'fk_isgaEOBranches' => $id_org,
            'fk_isgaEnlargedGroupSpecialities' => $id_egs,
            'fk_eiisEduProgramTypes' => $id_program_type
        );

        $programs = $applicationProgramsClass->getAllEntities(null, null, $where);

        $appClass = new \Model\Entities\isgaApplications($id_application);
        $appIsShortPath = $appClass->isShortPath();

        $view = new ViewModel();
        $view->setVariables(array(
            "programs" => $programs,
            "id_program_type" => $id_program_type,
            "sent" => isset($_POST['sent']) ? 1 : 0,
            "userRole" => $user->getRole(),
            'appIsShortPath' => $appIsShortPath,
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function saveFieldProgramApplicationAction()
    {
        $id_application_program = $this->params('id');
        $request = $this->getRequest();
        $data = $request->getPost()->toArray();
        $applicationProgramsClass = new Model\Entities\isgaApplicationPrograms($id_application_program);
        $applicationProgramsClass->setFieldsSafe($data);
        $applicationProgramsClass->save();
        echo 'Ok';
        die;
    }

    public function deleteProgramApplicationAction()
    {
        $id_application_program = $this->params('id');
        $applicationProgramsClass = new Model\Entities\isgaApplicationPrograms($id_application_program);
        $applicationProgramsClass->delete();
        echo 'Ok';
        die;
    }

    /**
     * сохранение файла с левого сервера
     *
     * _$POST[isChange] - eесли не отсутствует 0 false то заменяем
     */
    public function fileUploadServiceAction()
    {
        $logFile = 'public/log/fileUploadService.log';
//        $logFile = 'log/fileUploadService.log'; // Для локалки

        $mailer = new \Exchange\Controller\IndexController();

        $data = $this->getRequest()->getPost();
        $files = $this->getRequest()->getFiles()->toArray();
        $one_file = array_shift($files);
        $allowed = explode(",", $data["allow_ext"]);
        $mailer->mailLog('начинаем обработку файла', $logFile);
        if (isset($one_file) && $one_file['error'] == 0) {
            $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
            if ($one_file['size'] != 0 && $one_file['size'] < $data["maxsize"]) {
                if (in_array(mb_strtolower($ext), $allowed)) {
                    $mailer->mailLog('хотим по пути  ' . $data["path"], $logFile);
                    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $data["path"])) {
                        if (!mkdir($_SERVER['DOCUMENT_ROOT'] . $data["path"], 0777, true)) {
                            echo $mailer->mailLog("Не удалось создать каталог, проверьте права доступа", $logFile);
                            die;
                        }
                    }
                    $mailer->mailLog('сохранить  ' . $data["path"] . $data["name"], $logFile);
                    if (empty($data['isChange']) && is_file($_SERVER['DOCUMENT_ROOT'] . $data["path"] . $data["name"])) {
                        echo $mailer->mailLog('Уже есть такой файл', $logFile);
                        die;
                    }
                    $mailer->mailLog('сохраняем в ' . $_SERVER['DOCUMENT_ROOT'] . $data["path"] . $data["name"],
                        $logFile);

                    if (move_uploaded_file($one_file['tmp_name'],
//                                           $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $data["name"])) { // Для локалки
                        $_SERVER['DOCUMENT_ROOT'] . $data["path"] . $data["name"])) {
                        if (!empty($data['onLoad'])) {
                            $mailer->mailLog('Есть онлоад', $logFile);
                            $mailer->mailLog(print_r($data['onLoad'], true), $logFile);
                            try {
                                $this->onLoad(json_decode($data['onLoad'], true));
                            } catch (\Exception $exc) {
                                echo $mailer->mailLog('Ошибка при разделе онЛоад ' . $exc->getMessage(), $logFile);
                                $mailer->mailLog($exc->getTraceAsString(), $logFile);
                                die;
                            }
                        } else {
                            $mailer->mailLog('нет онлоад', $logFile);
                        }
                        $mailer->mailLog('сохранено', $logFile);
                        echo 1;
                        die;
                    } else {
                        echo $mailer->mailLog("Не удалось загрузить файл, проверьте права доступа", $logFile);
                        die;
                    }
                } else {
                    echo $mailer->mailLog("Недопустимый формат файла.", $logFile);
                    die;
                }
            } else {
                echo $mailer->mailLog("Размер файла превышает допустимый размер, или файл пустой", $logFile);
                die;
            }
        } else {
            echo $mailer->mailLog("Ошибка загрузки файла", $logFile);
            die;
        }
        echo $mailer->mailLog("Возникла ошибка при загрузке файла.", $logFile);
        die;
    }

    public function deleteFileAction()
    {
        $result = 'не определенная ошибка';
        $logFile = 'public/log/fileDeleteService.log';
        $mailer = new \Exchange\Controller\IndexController();

        $path = "public/uploads/" . $_POST["path"];
        //типа безопасность, чтоб назад не отошло
        $path = str_replace('../', '/', $path);
        $path = str_replace('/..', '/', $path);
        $path = str_replace('//', '/', $path);

        $mailer->mailLog(" хотим удалить файл по пути: " . $path, $logFile);
        //удаление файла
        if (file_exists($path)) {
            $mailer->mailLog("такой файл есть ", $logFile);
            try {
                $resultDelete = @unlink($path);
                if ($resultDelete) {
                    $mailer->mailLog("удалено ", $logFile);
                } else {
                    $mailer->mailLog("ушло в осадок ", $logFile);
                }
                $result = 1;
            } catch (Exception $e) {
                $result = $e->getMessage();
            }
        } else {
            $result = 1;
            $mailer->mailLog("такого файла нету ", $logFile);
        }
        $mailer->mailLog("результат выполнения ", $result);
        echo $result;
        die;
    }

    /**
     * проверяем лог и удаляем записи которым более месяца
     */
    public function logEntityDeleteClearAction()
    {
        $fileName = 'public/log/entitiy_delete.log';
        $fileName = $_SERVER['DOCUMENT_ROOT'] . '/' . $fileName;
        if (!is_file($fileName)) {
            die('Нет файла лога');
        }
        $handle = fopen($fileName, "r+");
        if (empty($handle)) {
            die('Не открывается файл лога');
        }
        //БЛОКИРОВКА ФАЙЛА
        if (!flock($handle, LOCK_EX | LOCK_NB)) {
            die('Не блокируется');
        }
        $date = date('Y-m-d', strtotime('-1 month'));
        $result = '';
        while (($string = fgets($handle)) !== false) {
            $strLen = strlen($string);
            $dateValue = substr($string, 0, 10);
            if ($dateValue < $date) {
                continue;
            }
            $result .= $string/* .PHP_EOL */
            ;
        }
        $resultLen = strlen($result);
        ftruncate($handle, 0); //УДАЛЯЕМ СОДЕРЖИМОЕ ФАЙЛА
        fseek($handle, 0);
        if (fwrite($handle, $result) === FALSE) {
            die('ошибка при записи');
        }
        fflush($handle); //очищение файлового буфера и запись в файл
        flock($handle, LOCK_UN); //СНЯТИЕ БЛОКИРОВКИ
        fclose($handle);
        die('Отработано');
    }


    /**
     * сохранение дополнительных файлов
     * Переделал под новый загрузчик файлов Sheremet
     */
    public function saveDopFilesAction()
    {
        $id_appl = $this->params('id');
        $error = array();
        $prev_key = 1;
        $massFiles = array();
        $sign_class = new \Eo\Service\Sign();

        $httpHost = $_SERVER['HTTP_HOST'];
        $testHosts = array('85.192.47.4', '146.120.224.100');
        if (in_array($httpHost, $testHosts)) {
            $need_sign_valid = false;
        } else {
            $need_sign_valid = true;
        }

        foreach ($_FILES as $key => $one_file) {
            if (empty($one_file["size"])) {
                continue;
            }

            $save_data = array();
            $name_info_file = $one_file['name'];

            //проверим является ли файл PDF || подписанным SIG, а также проверка имени файла
            $result_verify = $sign_class->verify_name_signfile($name_info_file);
            if ($result_verify === true) {
                $name_info_file_hash = $sign_class->get_hash_name($name_info_file);
                $DocumentType = new \Model\Entities\isgaApplicationDocumentTypes($key);
                $short_path = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' . date('Y') . '/' . $id_appl . '/' . $DocumentType->getField('Code') . '/';

                if (!file_exists($short_path)) {
                    if (!mkdir($short_path, 0777, true)) {
                        echo json_encode(array("error" => "Не удалось создать каталог, проверьте права доступа"));
                        die;
                    }
                }
                if (move_uploaded_file($one_file['tmp_name'], $short_path . $name_info_file_hash)) {

                    $save_data['FtpDirectoryUrl'] = str_replace($_SERVER['DOCUMENT_ROOT'] . "/public/uploads/", "", $short_path) . $name_info_file_hash;
                    if ($need_sign_valid) {
                        //                         * *отправим файл на сервер проверки подписи и получим данные сертификата и валидности
                        $sign_data = $sign_class->valid_signature($save_data['FtpDirectoryUrl']);
                    } else {
                        $sign_data = ["status" => "ok"];
                    }

                    if ($sign_data['status'] == 'ok') {
                        $applicationFiles_class = new \Model\Entities\isgaApplicationDocumentGroups();

                        $save_data['RealFileName'] = $name_info_file;
                        $save_data['fk_isgaApplicationDocumentType'] = $key;
                        $save_data['fk_isgaApplication'] = $id_appl;
                        $save_data['CreatedDate'] = date("Y-m-d H:i:s");

                        $applicationFiles_class->exchangeArray($save_data);
                        $applicationFiles_class->save(1);

                        if ($need_sign_valid) {
                            /*                             * **соберем информацию о пидписи файла*** */
                            $signInfoFilesClass = new \Model\Entities\isgaSignInfoFiles();
                            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
                            $id_org = $user->getField("fk_eiisEducationalOrganization");
                            $educationOrganization = $user->getEducationOrganization();
                            $organization_inn = trim($educationOrganization->getField('Inn'));
                            $certificate_inn = trim($sign_data["certificate_info"]['SubjectName']["INN"]);

                            $save_sign_info = array(
                                "fk_isgaAppplicationFileId" => $applicationFiles_class->getField('Id'),
                                "fk_isgaUserId" => $user->getField("Id"),
                                "date_verify" => Date("Y-m-d H:i:s"),
                                "valid" => ($sign_data['result']) ? 1 : 0,
                                // "valid"                         =>  1,
                                'message' => $sign_data['message'],
                                "certificate_info" => json_encode($sign_data['certificate_info'])
                            );

                            /*                            * сохраним данные о подписи*/
                            $signInfoFilesClass->exchangeArray($save_sign_info);
                            $res_sign = $signInfoFilesClass->save(1);
                        } else {
                            $save_sign_info = ["valid" => 1];
                        }
                        $result = json_encode(array('files' => array(0 => array('errors' => $error, 'filePath' => '/downloadfile.php?name=' . $applicationFiles_class->getField('HashName'), 'fileId' => $applicationFiles_class->getField('Id'), 'valid' => $save_sign_info['valid'], 'fileName' => $applicationFiles_class->getField('RealName')))));
                    } else {
                        $error = $sign_class->getErrorText($sign_data['result']);
                        $result = json_encode(array('files' => array(0 => array('errors' => array($error)))));
                    }
                } else {
                    echo json_encode(array("error" => "Не удалось загрузить файл, проверьте права доступа"));
                    die;
                }
            } else {
                $error = $sign_class->getErrorText($result_verify);
                $result = json_encode(array('files' => array(0 => array('errors' => array($error)))));
            }
        }
        print $result;
        die;
    }

    /**
     * сохранение файла
     */
    public function saveOneFileAction()
    {
        ini_set("memory_limit", "900M");
        $id_appl = $this->params('id');
        $error = array();
        $prev_key = 1;
        $massFiles = array();
        $sign_class = new \Eo\Service\Sign();
        $short_path = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/synch/' . date('Y') . '/' . $id_appl . '/open/';

        $need_sign_valid = true; // всегда проверяем

        foreach ($_FILES as $key => $one_file) {

            $applicationFiles_class = new \Model\Entities\isgaApplicationFiles();
            $save_data = array();
            $name_info_file = $one_file['name'];

            //проверим является ли файл PDF || подписанным SIG, а также проверка имени файла
            $result_verify = $sign_class->verify_name_signfile($name_info_file);
            if ($result_verify === true) {
                $name_info_file_hash = $sign_class->get_hash_name($name_info_file);

                if (!file_exists($short_path)) {
                    if (!mkdir($short_path, 0777, true)) {
                        echo json_encode(array("error" => "Не удалось создать каталог, проверьте права доступа"));
                        die;
                    }
                }

                if (@move_uploaded_file($one_file['tmp_name'], $short_path . $name_info_file_hash)) {

                    $save_data['HashName'] = str_replace($_SERVER['DOCUMENT_ROOT'] . "/public/uploads/", "", $short_path) . $name_info_file_hash;
                    if ($need_sign_valid) {
                        /*                         * *отправим файл на сервер проверки подписи и получим данные сертификата и валидности */
                        $sign_data = $sign_class->validSignature($save_data['HashName']);

                    } else {
                        $sign_data = ["status" => "ok"];
                    }

                    if ($sign_data['status'] == 'ok') {
                        $name_info = $name_info_file . "::" . $name_info_file_hash;
                        $save_data['RealName'] = $name_info_file;
                        $save_data['fk_isgaDocumentType'] = $key;
                        $save_data['fk_isgaApplication'] = $id_appl;
                        $applicationFiles_class->exchangeArray($save_data);
                        $res = $applicationFiles_class->save(1);

                        if ($need_sign_valid) {
                            /*                             * **соберем информацию о пидписи файла*** */
                            $signInfoFilesClass = new \Model\Entities\isgaSignInfoFiles();
                            $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
                            $id_org = $user->getField("fk_eiisEducationalOrganization");
                            $educationOrganization = $user->getEducationOrganization();
                            $organization_inn = trim($educationOrganization->getField('Inn'));
                            $certificate_inn = trim($sign_data["certificate_info"]['SubjectName']["INN"]);

                            $save_sign_info = array(
                                "fk_isgaAppplicationFileId" => $applicationFiles_class->getField('Id'),
                                "fk_isgaUserId" => $user->getField("Id"),
                                "date_verify" => Date("Y-m-d H:i:s"),
                                "valid" => ($sign_data['result']) ? 1 : 0,
                                // "valid"                         =>  1,
                                'message' => $sign_data['message'],
                                "certificate_info" => json_encode($sign_data['certificate_info'])
                            );

                            /*                             * проверим принадлежит ли сертификат подписи пользователю загрузившему файл */
                            /* if($organization_inn != $certificate_inn)
                              {
                              $save_sign_info['valid'] = 0;
                              $save_sign_info['message'] .= "ИНН сертификата подписи не совпадает с ИНН подписывающего";
                              } */

                            /*                             * сохраним данные о подписи* */
                            $signInfoFilesClass->exchangeArray($save_sign_info);
                            $res_sign = $signInfoFilesClass->save(1);
                        } else {
                            $save_sign_info = ["valid" => 1];
                        }
                        $result = json_encode(array('files' => array(0 => array('errors' => $error, 'filePath' => '/downloadfile.php?name=' . $applicationFiles_class->getField('HashName'), 'fileId' => $applicationFiles_class->getField('Id'), 'valid' => $save_sign_info['valid'], 'fileName' => $applicationFiles_class->getField('RealName')))));
                    } else {
                        $error = $sign_class->getErrorText($sign_data['result']);
                        $result = json_encode(array('files' => array(0 => array('errors' => array($error)))));
                    }
                } else {
                    echo json_encode(array("error" => "Не удалось загрузить файл, проверьте права доступа"));
                    die;
                }
            } else {
                $error = $sign_class->getErrorText($result_verify);
                $result = json_encode(array('files' => array(0 => array('errors' => array($error)))));
            }
        }
        print $result;
        die;
    }

    /**
     *
     * @param array $params
     * @return type
     */
    protected function onLoad(array $params)
    {
        return $this->forward()->dispatch($params['controller'], $params['options']);
    }

    /**
     * отдает справочник стандартных причин
     * (для выпадающего меню причин)
     */
    public function notifyCommentsReferenceAction()
    {
        //берем ид заявы
        $appId = $this->params('id');
        //Дергаем гроур
        $docGroup = new \Model\Entities\isgaApplicationDocumentGroups();
        $docGroup->getByApplicationId($appId);
        if (!$docGroup->getByApplicationId($appId)) {
            throw new Exception('Не найдено docGroup  по ТД заявлинея "' . $appId . '"');
        }
        //дергнуть  гроуп гроуп, удалить старые
        $documentGroudId = $docGroup->getField('Id');
        $docGroupStatusId = $docGroup->getField('fk_ApplicationDocumentGroupStatus');
        $where = array(
            'fk_isgaApplicationDocumentGroupStatus' => $docGroupStatusId,
        );
        $reasons = new \Model\Entities\isgaApplicationDocumentGroupStatusReason();
        $result = $reasons->getAllArray($where);
        echo json_encode($result);
        die;
    }

    /*
     * Получение количества заявок на всех шагах.
     */
    public function getCountAllStepAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $id_org = $user->getField("fk_eiisEducationalOrganization");
        $application_class = new Model\Entities\isgaApplications();

        $where = 'eiis_Licenses.fk_eiisEducationalOrganization = "' . $id_org . '" AND (isga_Applications.fk_isgaApplicationStatus = "A3B952BF-628E-446C-B865-E483503241E8" OR isga_Applications.fk_isgaApplicationStatus IS NULL)';

        $new = $application_class->selectComplicated(
            'isga_Applications', array('count' => new \Zend\Db\Sql\Expression('COUNT(isga_Applications.Id)')), $where,
            NULL, NULL, NULL, NULL, 'eiis_Licenses',
            'isga_Applications.fk_eiisLicense = eiis_Licenses.Id',
            array()
        )->toArray();
        $sent = $application_class->selectComplicated(
            'isga_Applications', array('count' => new \Zend\Db\Sql\Expression('COUNT(isga_Applications.Id)')),
            '`eiis_Licenses`.`fk_eiisEducationalOrganization` = "' . $id_org . '" AND
            `isga_Applications`.`fk_isgaApplicationStatus` IS NOT NULL AND
            `isga_Applications`.`fk_isgaApplicationStatus` != "2D675622-1E14-4063-AD11-B647B836746D" AND
            `isga_Applications`.`fk_isgaApplicationStatus` != "A3B952BF-628E-446C-B865-E483503241E8" AND
            `isga_Applications`.`fk_isgaApplicationReason` IS NOT NULL AND
            `isga_Applications`.`fk_isgaApplicationReason` NOT LIKE "" AND
            `isga_Applications`.`fk_isgaApplicationReason` NOT LIKE "NULL"',
            NULL, NULL, NULL, NULL, 'eiis_Licenses',
            'isga_Applications.fk_eiisLicense = eiis_Licenses.Id',
            array()
        )->toArray();
        $rectification = $application_class->selectComplicated(
            'isga_Applications', array('count' => new \Zend\Db\Sql\Expression('COUNT(isga_Applications.Id)')),
            'eiis_Licenses.fk_eiisEducationalOrganization = "' . $id_org . '" AND (isga_Applications.fk_isgaApplicationStatus = "2D675622-1E14-4063-AD11-B647B836746D")',
            NULL, NULL, NULL, NULL, 'eiis_Licenses',
            'isga_Applications.fk_eiisLicense = eiis_Licenses.Id',
            array()
        )->toArray();
        $notification = $application_class->selectComplicated(
            'isga_Applications', array('count' => new \Zend\Db\Sql\Expression('COUNT(isga_Applications.Id)')),
            '`isga_Applications`.`fk_eiisEducationalOrganization` = "' . $id_org . '" AND isga_Notification.DateSigning IS NOT NULL',
            NULL, NULL, NULL, NULL,
            'isga_Notification',
            'isga_Applications.Id = isga_Notification.fk_isgaApplication',
            array()
        )->toArray();
        $count['new'] = $new[0]['count'];
        $count['sent'] = $sent[0]['count'];
        $count['rectification'] = $rectification[0]['count'];
        $count['notification'] = $notification[0]['count'];
        echo json_encode($count);
        die;
    }

    /**
     * рон-заявлени - карточка заявлени - образдовательные программы для лишения
     * рон-заявлени - карточка заявлени - исключения ОП
     *
     * дергаем филиалов
     *
     * url:  /eo/declaration/get-fillials-programs-deprivation/ид-чегото
     *
     * @return ViewModel
     */
    public function getFillialsProgramsDeprivationAction()
    {
        $id = $this->params('id');
        if (!$this->params('id2')) {
            $only_EP = $this->params('id2');
        } else {
            $only_EP = false;
        }
        /*$tabName            = $this->getRequest()->getQuery('tabName','deprivation');*/
        $tabName = $this->params()->fromPost('tabName', 'deprivation');
        $tabName = strtolower($tabName);
        $prLish = 1;
        if (!empty(\Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName])) {
            $prLish = \Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName];
        }

        $fillialsApplication_class = new Model\Gateway\EntitiesTable('isga_mtm_Application_Branches');
        $fillialsApplication = $fillialsApplication_class->getEntitiesByWhere(array('fk_isgaApplication' => $id));
        $fillialsApplication->buffer();

        //$filialsIdAll =  $fillialsApplication->getObjectsArray();
        $application = $appClass = new \Model\Entities\isgaApplications($id);
        $prevCertId = $appClass->getFieldSafe('fk_isgaPreviousCertificate');
        $filialsIdAll = $fillialsApplication->toArray();
        $filialsIdAll = array_column($filialsIdAll, 'fk_eiisEOBranch');
        unset($fillialsApplication);
        $appClass = new \Model\Entities\isgaApplications();
        $fillialsApplication = $appClass->getProgramsIdsDeprivation(
            $prevCertId,
            $filialsIdAll,
            array('fk_eiisEducationalOrganization' => new \Zend\Db\Sql\Expression("`isga_CertificateSupplements`.`fk_eiisEducationalOrganization`")),
            null,
            null,
            $prLish
        );
        if (!empty($fillialsApplication)) {
            $fialialAll = new Model\Entities\eiisEducationalOrganizations();
            $fillialsApplication = $fialialAll->getAllByWhereParseId(array($fialialAll->getIdName() => $fillialsApplication));
            unset($fialialAll);
        }


        //$application = new \Model\Entities\isgaApplications($id);
        $applicationIsDeny = $application->isOrderDeny();
        if ($applicationIsDeny) {
            $application->setProgramChekedInFalse();
        }
        //головной вуз пихаем первым
        $fillialsApplicationArray = array();
        $fillialsApplicationArrayMain = array();
        $i = 1;
        //$application=new \Model\Entities\isgaApplications($id);
        $orgId = $application->getEducationalOrgId();

        foreach ($fillialsApplication as $currentFilial) {
            if ($currentFilial instanceof Model\Entities\eiisEducationalOrganizations) {
                $fillial = $currentFilial;
                $filId = $currentFilial->getId();
            } else {
                $filId = $currentFilial->getField('fk_eiisEOBranch');
                $fillial = $currentFilial->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $filId));
            }
            if (empty($fillial)) continue;
            if ($filId == $orgId) {
                $fillialsApplicationArrayMain[0]['base'] = $currentFilial;
                $fillialsApplicationArrayMain[0]['org'] = $fillial;
            } else {
                $fillialsApplicationArray[$fillial->getId()]['base'] = $currentFilial;
                $fillialsApplicationArray[$fillial->getId()]['org'] = $fillial;
            }
        }
        //  отсортировка
        uasort(
            $fillialsApplicationArray,
            function ($a, $b) {
                $ind1 = $a['org']->getFieldSafe('ShortName');
                $ind1 = empty($ind1) ? $a['org']->getFieldSafe('RegularName') : $ind1;
                $ind1 = mb_strtolower($ind1);

                $ind2 = $b['org']->getFieldSafe('ShortName');
                $ind2 = empty($ind2) ? $b['org']->getFieldSafe('RegularName') : $ind2;
                $ind2 = mb_strtolower($ind2);
                return strcmp($ind1, $ind2);
            }
        );
        $fillialsApplicationArray = array_merge($fillialsApplicationArrayMain, $fillialsApplicationArray);

        $fillialsApplication = $fillialsApplicationArray;
        $view = new ViewModel();
        $view->setVariables(array(
                "fillials" => $fillialsApplication,
                'applicationIsDeny' => $applicationIsDeny,
                'onlyEP' => $only_EP,
                'prLish' => $prLish,
                'applicationId' => $application->getId(),
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    /**
     * акредитция -заявлени - карточка заявлени - образдовательные программы для лишения
     * акредитация-заявлени - карточка заявлени -  исключения ОП
     *
     *  получаем типы  программ у заявления
     *
     *  url: /eo/declaration/get-edu-program-type-deprivation/ид-чего-то?tabName=
     *
     *  в гет
     *      нету        -
     *      deprivation - лишение программ
     *
     * дергаем типы программ
     *
     * @return ViewModel
     */
    public function getEduProgramTypeDeprivationAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $tabName = $this->params()->fromPost('tabName', 'deprivation');
        $tabName = strtolower($tabName);
        $prLish = 1;
        if (!empty(\Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName])) {
            $prLish = \Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName];
        }
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $eduProgramTypeIds = $appClass->getProgramsIdsDeprivation(
            $currentApplication->getField("fk_isgaPreviousCertificate"),
            $id_org,
            "fk_eiisEduProgramType",
            null,
            null,
            $prLish
        );
        $eduProgramTypes = array();
        if (!empty($eduProgramTypeIds))
            $eduProgramTypes = $currentApplication->selectComplicated(
                "eiis_EduProgramTypes", array('Id', 'Name'), array('Id' => $eduProgramTypeIds, 'fk_eiisEduLevels !=""')
            )->toArray();
        $applicationProgramsClass = new Model\Gateway\EntitiesTable('isga_ApplicationPrograms');
        $view = new ViewModel();
        $view->setVariables(
            array(
                "eduProgramTypes" => $eduProgramTypes,
                "applicationProgramsClass" => $applicationProgramsClass,
                "id_org" => $id_org,
                "id_application" => $id_application,
                'prLish' => $prLish,
            )
        );
        $view->setTerminal(true);
        return $view;
    }

    /**
     * акредитация -заявлени - карточка заявлени - образдовательные программы для лишения
     * акредитация -заявлени - карточка заявлени -  исключения ОП
     *
     * получаем усы по уровням
     *
     * url:  /eo/declaration/get-ugs-by-level-deprivation/ид-чего-то
     *
     * @return ViewModel
     */
    public function getUgsByLevelDeprivationAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $id_program_type = $this->params('id3');
        $tabName = $this->params()->fromPost('tabName', 'deprivation');
        $tabName = strtolower($tabName);
        $prLish = 1;
        if (!empty(\Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName])) {
            $prLish = \Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName];
        }

        if ($id_program_type == 'undefined') {
            $id_program_type = NULL;
        }
        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications($id_application);
        $ReorgOrganizations = $appClass->selectComplicated(
            'isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application)
        )->toArray();
        $isgaEGSIds = $appClass->getProgramsIdsDeprivation(
            $currentApplication->getField("fk_isgaPreviousCertificate"),
            $id_org,
            'fk_isgaEnlargedGroupSpeciality', //'id',
            $id_program_type,
            null,
            $prLish
        );
        /*
        if( !empty($isgaEGSIds) ){
            $progAll = new \Model\Entities\isgaAccreditedPrograms();
            $progAll = $progAll->getAllByWhereParseId( array($progAll->getIdName() => $isgaEGSIds) );
            $isgaEGSIds = array();
            foreach( $progAll as $key => $progOne ){
                $egsClass = $progOne->getEGS();
                if( empty($egsClass) ){
                    $egsClass = $progOne->getEnlargedGroupSpeciality();
                }
                if( !empty($egsClass) ){
                    $isgaEGSIds[] = $egsClass->getId();
                }
            }
        }
        */
        $isHasNullUgs = false;
        $enlargedGroupSpecialitiesClass = new Model\Entities\isgaEnlargedGroupSpecialities();
        if (empty($isgaEGSIds)) {
            $enlargedGroupSpecialities = array();
        } else {

            foreach ($isgaEGSIds as $value) {
                if ($value === null || 'null' === strtolower($value) || 'void' === strtolower($value)) {
                    $isHasNullUgs = true;
                    break;
                }
            }
            $enlargedGroupSpecialities = $enlargedGroupSpecialitiesClass->selectComplicated(
                "isga_EnlargedGroupSpecialities", array('Id', 'Code', 'Name'), array('Id' => $isgaEGSIds), null,
                "Code ASC"
            )->toArray();
            if ($isHasNullUgs) {
                $enlargedGroupSpecialities[] = array(
                    'Id' => 'null',
                    'Code' => '',
                    'Name' => 'без деления на угс',
                );
            }
        }
        $applicationProgramsClass = new Model\Gateway\EntitiesTable('isga_ApplicationPrograms');
        $view = new ViewModel();
        $view->setVariables(array(
            "enlargedGroupSpecialities" => $enlargedGroupSpecialities,
            'applicationProgramsClass' => $applicationProgramsClass,
            "org" => $id_org,
            "programtype" => $id_program_type,
            "sent" => (isset($_POST['sent']) && @$_POST['sent'] == 'sent') ? 1 : 0,
            "application" => $id_application,
            'prLish' => $prLish,
            'isHasNullUgs' => $isHasNullUgs,
        ));
        $view->setTerminal(true);
        return $view;
    }


    /**
     * рисует диалог выбора программ для исклюыени/ лишения
     *
     * url:  /eo/declaration/get-direct-by-ugs-deprivation/
     * @return ViewModel
     */
    public function getDirectByUgsDeprivationAction()
    {
        $id_application = $this->params('id');
        $id_org = $this->params('id2');
        $id_program_type = $this->params('id3');
        $id_egs = $this->params('id4');

        $tabName = $this->params()->fromPost('tabName', 'deprivation');
        $tabName = strtolower($tabName);
        $prLish = 1;
        if (!empty(\Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName])) {
            $prLish = \Model\Entities\isgaApplicationPrograms::$lishenieCodeToId[$tabName];
        }

        $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id_application);
        $appClass = new Model\Entities\isgaApplications();
        $eiisLicensedProgramsClass = new Model\Entities\eiisLicensedPrograms();

        $ReorgOrganizations = $currentApplication->selectComplicated('isga_mtm_Application_Organizations',
            array('fk_eiisEducationalOrganization'),
            array('fk_isgaApplication' => $id_application))->toArray();
        $fieldNeed = array('IdProgram' => 'Id', 'Name', 'Code', 'Period' => 'EduNormativePeriod');
        $addWhere = new \Zend\Db\Sql\Where();
        $addWhere->isNull('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended');
        $addWhere->or;
        $addWhere->like('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended', 'null');
        $addWhere->or;
        $addWhere->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended', 0);
        $addWhere->or;
        $addWhere->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsSuspended', '');
        if (1 == $prLish) {
            $addWhere2 = new \Zend\Db\Sql\Where();
            $addWhere2->isNotNull('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled');
            $addWhere2->and;
            $addWhere2->notLike('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled', 'null');
            $addWhere2->and;
            $addWhere2->notEqualTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled', 0);
            $addWhere2->and;
            $addWhere2->notEqualTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled', '');
        }
        if (2 == $prLish) {
            $addWhere2 = new \Zend\Db\Sql\Where();
            $addWhere2->or;
            $addWhere2->isNull('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled');
            $addWhere2->or;
            $addWhere2->like('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled', 'null');
            $addWhere2->or;
            $addWhere2->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled', 0);
            $addWhere2->or;
            $addWhere2->equalTo('isga_AccreditedPrograms.fk_isgaOrderDocumentsCanceled', '');
        }
        $additionalCondition = array(

            'fk_eiisEduProgramType' => $id_program_type,
            "fk_isgaCertificateSupplementStatus" => array("4c3c4932-d4c8-4524-83c5-0b5039403a09", '67f82aa6-edf8-4115-bd3f-8a4f348782c0', '14b2d4a4-c96b-4e9a-b65e-dab6480d8865'),
            $addWhere,
            $addWhere2,
        );
        if ('null' == strtolower($id_egs)) {
            $addWhere3 = new \Zend\Db\Sql\Where();
            $addWhere3->or;
            $addWhere3->isNull('isga_AccreditedPrograms.fk_isgaEnlargedGroupSpeciality');
            $addWhere3->or;
            $addWhere3->like('isga_AccreditedPrograms.fk_isgaEnlargedGroupSpeciality', 'null');
            $addWhere3->or;
            $addWhere3->like('isga_AccreditedPrograms.fk_isgaEnlargedGroupSpeciality', 'void');
            $addWhere3->or;
            $addWhere3->equalTo('isga_AccreditedPrograms.fk_isgaEnlargedGroupSpeciality', '');
            $additionalCondition[] = $addWhere3;
        } else {
            $additionalCondition['fk_isgaEnlargedGroupSpeciality'] = $id_egs;
        }
        if (
            $id_org == $currentApplication->getField('fk_eiisEducationalOrganization') &&
            count($ReorgOrganizations) > 0 &&
            $currentApplication->getType()->getField("Id") == '711776cf-7871-4c90-9ed7-9669c0e0356f' &&
            in_array('64771BDC-27C5-4C83-B93D-CB725435658B', $currentApplication->getIdsReasons())
        ) {
            $ReorgOrganizations = array_column($ReorgOrganizations, 'fk_eiisEducationalOrganization');
            array_push($ReorgOrganizations, $id_org);
            $accreditedPrograms = $appClass->getAccreditedPrograms($currentApplication->getField("fk_isgaPreviousCertificate"), $ReorgOrganizations, $fieldNeed, $additionalCondition);

        } else {
            $accreditedPrograms = $appClass->getAccreditedPrograms($currentApplication->getField("fk_isgaPreviousCertificate"), $id_org, $fieldNeed, $additionalCondition);
        }
        $view = new ViewModel();
        $view->setVariables(array(
            "licensedPrograms" => $accreditedPrograms,
            "id_egs" => $id_egs
        ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * акредитация - заявлени - карточка заявлени - образдовательные программы для лишения
     * акредитация - заявлени - карточка заявлени -  исключения ОП
     *
     * сами программы
     *
     * url:  /eo/declaration/get-program-by-ugs-deprivation/ид-чего-то
     *
     * @return ViewModel
     */
    public function setApplicationExtraDocumentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            if (isset($_FILES['file'])) {

                $explode = explode('.', $_FILES['file']['name']);
                $exp = "";
                for ($i = 1; $i < count($explode); $i++) {
                    $exp = $exp . "." . $explode[$i];
                }

                $hash = md5($_FILES['file']['name'] . date("Y-m-d H:i:s:ms"));

                $post = $request->getPost()->toArray();
                parse_str($post['form'], $resultPost);
                $post = $resultPost;

                $temp = $_FILES['file']['tmp_name'];
                $path = $_SERVER['DOCUMENT_ROOT'] . "/temp/" . time();
                $isDir = mkdir($path, 0777, true);
                $sign_class = new \Eo\Service\Sign();
                $name_info_file_hash = $sign_class->get_hash_name($_FILES['file']['name']);
                $fullPath = $path . "/" . $name_info_file_hash;
                move_uploaded_file($temp, $fullPath);
                //$save_data['HashName'] = $fullPath;

//                return new JsonModel([
//                    'answer' => "ok",
//                    'tmp' => $fullPath,
//                    'name' => $_FILES['file']['name'],
//                    'status' => $sign_data['status'],
//                    'certificate_info' => $sign_data['certificate_info']
//
//                ]);


                $path = 'module/GetDocumets/extraDocument/';
                $newfile = $path . $post['expert_id'] . '/' . $hash . $exp;

                if (!file_exists('module/GetDocumets')) {
                    mkdir('module/GetDocumets/', 0777, true);
                }
                if (!file_exists($path)) {
                    mkdir('module/GetDocumets/extraDocument/', 0777, true);
                }
                if (!file_exists($path . $post['expert_id'])) {
                    mkdir($path . $post['expert_id'] . '/', 0777, true);
                }

                if (!copy($fullPath, $newfile)) {
                    return false;
                }


                $newExtra = new ApplicationExtraDocument();
                $newExtra->fk_isgaApplication = $post['app'];
                $newExtra->fk_isgaDocumentType = $post['type'];
                $newExtra->RealName = $_FILES['file']['name'];
                $newExtra->HashName = $newfile;
                $newExtra->dateCreate =date('Y-m-d H:m:s');
                $newExtra->expert_id = $post['expert_id'];
                $newExtra->eduProgramSelectNew = $post['program'];

                if ($newExtra->save()) {
                    return new JsonModel([
                        'realName' =>  $_FILES['file']['name'],
                        'newfile' => $newfile,
                        'answer' => "ok",
                    ]);
                }

            }


        }
        return new JsonModel(['answer' => "error"]);
    }

    public function downloadImages()
    {


        $user = $this->getAuthService()->getIdentity();
        $file = $this->params('id');
        $path = 'module/GetDocumets/Files/';
        $newfile = $path . $user->getId() . '/' . $file . '.docx';
        $response = new \Zend\Http\Response\Stream();

        $response->setStream(fopen($newfile, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($file));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($newfile) . '"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($newfile),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);

        return $response;


    }


}