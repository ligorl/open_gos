<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

class ConsoleController extends AbstractActionController
{
    private $isTest = false;

    public function indexAction()
    {

    }

    public function updateLicensesAction(){

        $partsOfWork = [
            "TempDelete"        => false,
            "IsUpdated"         => true,
            "UpdateFromEIIS"    => false,
            "temp_fLicenses"    => true,
            "temp_fSupplements" => true,
            "temp_fDirections"  => true
        ];

        set_time_limit(0);

        $logger = new \Model\Logger\Logger("fupdate",true);
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Начинаем скрипт получения данных с ФБДРЛ");

        $adapter = Model\Gateway\LicReestrDbAdapter::getInstance();

        //Чистим бд от того что там уже есть
        if($partsOfWork["TempDelete"]) {
            $logger->Log("Чистим темп таблицы бд");
            echo "\n <br> Чистим темп таблицы бд";


            $statement = $adapter->query("TRUNCATE TABLE `temp_fLicenses`");
            $statement->execute();

            $statement = $adapter->query("TRUNCATE TABLE `temp_fSupplements`");
            $statement->execute();

            $statement = $adapter->query("TRUNCATE TABLE `temp_fDirections`");
            $statement->execute();
        }

        if($partsOfWork["IsUpdated"]) {
            $logger->Log("Устанавливаем все is_updated в 0");
            echo "\n <br> Устанавливаем все is_updated в 0";

            $statement = $adapter->query("UPDATE `license` SET `is_updated`=0 WHERE 1");
            $statement->execute();
            $statement = $adapter->query("UPDATE `program` SET `is_updated`=0 WHERE 1");
            $statement->execute();
            $statement = $adapter->query("UPDATE `supplement` SET `is_updated`=0 WHERE 1");
            $statement->execute();
        }

        //Получаем данные с ЕИИС
        if($partsOfWork["UpdateFromEIIS"]) {

            $params = null;
            if($this->isTest)
                $params = [
                    "wsdl" => "http://eiis-production.srvdev.ru/IntegrationService/BaseService.asmx?wsdl"
                ];

            $logger->Log("Получаем данные EIIS.F_LICENSES");
            echo "\n <br> Получаем данные EIIS.F_LICENSES";
            $serviceEntities = new \Model\EiisService\ServiceEntities($params);
            $serviceEntities->getEntitiesFromService("EIIS.F_LICENSES", 100);

            $logger->Log("Получаем данные EIIS.F_SUPPLEMENTS");
            echo "\n <br> Получаем данные EIIS.F_SUPPLEMENTS";
            $serviceEntities = new \Model\EiisService\ServiceEntities($params);
            $serviceEntities->getEntitiesFromService("EIIS.F_SUPPLEMENTS", 100);

            $logger->Log("Получаем данные EIIS.F_DIRECTIONS");
            echo "\n <br> Получаем данные EIIS.F_DIRECTIONS";
            $serviceEntities = new \Model\EiisService\ServiceEntities($params);
            $serviceEntities->getEntitiesFromService("EIIS.F_DIRECTIONS", 110);
        }

        //Проходим по таблицам и парсим блоками по 5к

        try {
            //таблица temp_fLicenses
            if($partsOfWork["temp_fLicenses"]) {
                $licTable = new Model\Gateway\LicReestrEntitiesTable("temp_fLicenses");
                $count = $licTable->getCount();
                $pages = ceil($count / 5000);
                $logger->Log("Обработка temp_fLicenses, страниц: " . $pages);
                echo "\n <br> Обработка temp_fLicenses, страниц: " . $pages;
                for ($page = 1; $page <= $pages; $page++) {
                    $logger->Log("страница " . $page);
                    echo "\n <br> страница " . $page;
                    $items = $licTable->getAllEntities([
                        "page" => $page,
                        "onPage" => 5000
                    ], "Id ASC");
                    foreach ($items as $item) {
                        //Пропускаем все лицензии выданные органом Федеральная служба в сфере образования
                        if (strtolower($item->getField("LicOrganFk")) == strtolower("BFB891F1F234F61C11835CDEAF8E9CA0"))
                            continue;

                        //Получаем запись с таким же айди, вдруг она есть в бд
                        $license = Model\LicReestrEntities\license::getLicenseByGUID($item->getId());
                        //если нет, создаем новый объект
                        $isNew = false;
                        if ($license == null) {
                            $license = new Model\LicReestrEntities\license();
                            $isNew = true;
                        }

                        //Теперь парсим поля и раскидываем по текущей бд
                        $license->setField("guid", $item->getId());

                        //Организация
                        $schoolId = $item->getField("SchoolFk");
                        if(!$item->isEmptyField("IndividualFk"))
                            $schoolId = $item->getField("IndividualFk");

                        $license->setField("eo_guid", $schoolId);
                        $eo = Model\LicReestrEntities\eo::getOrgByGUID($license->getField("guid").'_'.$schoolId);

                        if ($eo == null){
                            $eo = new Model\LicReestrEntities\eo();
                            $eo->setField("guid", $license->getField("guid").'_'.$schoolId);
                        }

                        $eo->setField("name", $item->getField("FullOrgName"));
                        $eo->setField("short_name", $item->getField("ShortOrgName"));
                        if(!$item->isEmptyField("IndividualFk")){
                            $eo->setField("name", $item->getField("NameIP"));
                            $eo->setField("short_name", $item->getField("NameIP"));
                        }

                        $eo->setField("ogrn", $item->getField("OGRN"));
                        $eo->setField("inn", $item->getField("INN"));
                        $eo->setField("kpp", $item->getField("KPP"));
                        $eo->setField("address", $item->getField("Address"));
                        $eo->setField("source", "ФБДРЛ");

                        //Регион
                        $region = Model\LicReestrEntities\region::getRegionByGUID($item->getField("RegionFk"));
                        if ($region != null) {
                            $eo->setField("region_id", $region->getId());
                        }
                        $eo->save();
                        $license->setField("eo_id", $eo->getId());

                        //Орган выдавший лицензию
                        $licOrgan = Model\LicReestrEntities\licenseorgan::getOrgByGUID($item->getField("LicOrganFk"));
                        if ($licOrgan == null && !$item->isEmptyField("LicOrgan")) {
                            $licOrgan = new Model\LicReestrEntities\licenseorgan();
                            $licOrgan->setField("guid", $item->getField("LicOrganFk"));
                            $licOrgan->setField("name", $item->getField("LicOrgan"));
                            $licOrgan->setField("source", "ФБДРЛ");
                            $licOrgan->save();
                            $license->setField("license_organ_id", $licOrgan->getId());
                        }
                        if ($licOrgan != null)
                            $license->setField("license_organ_id", $licOrgan->getId());

                        //Статус
                        $licState = Model\LicReestrEntities\licensestate::getStateByCode($item->getField("LicStatusFk"));
                        if ($licState != null) {
                            $license->setField("license_state_id", $licState->getId());
                        }

                        //Другие поля
                        $license->setField("number", $item->getField("LicNumber"));
                        $license->setField("doc_serie", $item->getField("LicBlankSer"));
                        $license->setField("doc_number", $item->getField("LicBlankNum"));
                        $license->setField("issue_date", $item->getField("LicDate"));
                        $license->setField("end_date", $item->getField("LicEndDate"));
                        $license->setField("is_actual", 1);
                        //if($isNew)
                        $license->setField("source", "ФБДРЛ");
                        $license->setField("is_updated", 1);
                        $license->setField("file_xml",$item->getField("LicenseDocXML"));
                        $license->setField("file_sign",$item->getField("SignFile"));
                        $license->save();

                        //добавляем/редактируем документы
                        $createDoc = $license->getStartDoc();
                        if($createDoc == null)
                            $createDoc = new \Model\LicReestrEntities\decision();
                        $createDoc->setField("license_id", $license->getId());
                        $createDoc->setField("license_guid", $license->getField("guid"));
                        $createDoc->setField("type_guid", "e0e8eb9952464ea798f7e41b6a81c93f");
                        $createDoc->setField("type_name", $item->getField("DocTypeName"));
                        $createDoc->setField("date", $item->getField("DocDate"));
                        $createDoc->setField("number", $item->getField("DocNumber"));
                        $createDoc->setField("is_actual", 1);
                        $createDoc->setField("source", "ФБДРЛ");
                        $createDoc->save();

                        if (!$item->isEmptyField("TerminateBase") && !$item->isEmptyField("TerminateDate")) {
                            $terminateDoc = $license->getEndDoc();
                            if($terminateDoc == null)
                                $terminateDoc = new \Model\LicReestrEntities\decision();
                            $terminateDoc->setField("license_id", $license->getId());
                            $terminateDoc->setField("license_guid", $license->getField("guid"));
                            $terminateDoc->setField("type_guid", "1dc3e282134a4b7e9578216e6f07f096");
                            $terminateDoc->setField("type_name", "Распоряжение");
                            $terminateDoc->setField("date", $item->getField("TerminateDate"));
                            $terminateDoc->setField("number", $item->getField("TerminateBase"));
                            $terminateDoc->setField("is_actual", 1);
                            $terminateDoc->setField("source", "ФБДРЛ");
                            $terminateDoc->save();

                            $license->setField("is_terminated", 1);
                            $license->save();
                        }
                    }
                }
                $logger->Log("Конец обработки temp_fLicenses");
                echo "\n <br> Конец обработки temp_fLicenses";
            }

            //таблица temp_fSupplements
            if($partsOfWork["temp_fSupplements"]) {
                $suppTable = new Model\Gateway\LicReestrEntitiesTable("temp_fSupplements");
                $count = $suppTable->getCount();
                $pages = ceil($count / 5000);
                $logger->Log("Обработка temp_fSupplements, страниц: " . $pages);
                echo "\n <br> Обработка temp_fSupplements, страниц: " . $pages;
                for ($page = 1; $page <= $pages; $page++) {
                    $logger->Log("страница " . $page);
                    echo "\n <br> страница " . $page;
                    $items = $suppTable->getAllEntities([
                        "page" => $page,
                        "onPage" => 5000
                    ], "Id ASC");
                    foreach ($items as $item) {

                        //Пропускаем все приложения свящанные с лицензией выданной органом Федеральная служба в сфере образования
                        $license = \Model\LicReestrEntities\license::getLicenseByGUID($item->getField("LicenseFk"));
                        if ($license != null) {
                            $licOrg = $license->getLicOrgan();
                            if ($licOrg != null && strtolower($licOrg->getField("guid")) == strtolower("BFB891F1F234F61C11835CDEAF8E9CA0"))
                                continue;
                        }

                        //Получаем запись с таким же айди, вдруг она есть в бд
                        $supplement = Model\LicReestrEntities\supplement::getSupplementByGUID($item->getId());
                        //если нет, создаем новый объект
                        $isNew = false;
                        if ($supplement == null) {
                            $supplement = new Model\LicReestrEntities\supplement();
                            $isNew = true;
                        }

                        //Теперь парсим поля и раскидываем по текущей бд
                        $supplement->setField("guid", $item->getId());

                        //Организация
                        $schoolId = $item->getField("SchoolFk");
                        if(!$item->isEmptyField("IndividualFk"))
                            $schoolId = $item->getField("IndividualFk");

                        $supplement->setField("eo_guid", $schoolId);
                        $eo = Model\LicReestrEntities\eo::getOrgByGUID( $supplement->getField("guid")."_".$schoolId);
                        if ($eo == null) {
                            $eo = new Model\LicReestrEntities\eo();
                            $eo->setField("guid", $supplement->getField("guid")."_".$schoolId);
                        }

                        if(!$item->isEmptyField("FullOrgName")){
                            $eo->setField("name", $item->getField("FullOrgName"));
                            $eo->setField("short_name", $item->getField("ShortOrgName"));
                        }
                        $eo->setField("address", $item->getField("Address"));
                        //if($isNew)
                        $eo->setField("source", "ФБДРЛ");
                        $eo->setField("is_updated", 1);
                        $eo->save();
                        $supplement->setField("eo_id", $eo->getId());

                        //Лицензия
                        $supplement->setField("license_guid", $item->getField("LicenseFk"));
                        if ($license != null)
                            $supplement->setField("license_id", $license->getId());

                        //Статус
                        $supplement->setField("state_code", $item->getField("SupStatusFk"));
                        $licState = Model\LicReestrEntities\licensestate::getStateByCode($item->getField("SupStatusFk"));
                        if ($licState != null) {
                            $supplement->setField("state_name", $licState->getField("name"));
                        }

                        //Другие поля
                        $supplement->setField("number", $item->getField("SupNumber"));
                        $supplement->setField("doc_serie", $item->getField("SupBlankSer"));
                        $supplement->setField("doc_number", $item->getField("SupBlankNum"));
                        $supplement->setField("addresses", $item->getField("ImplAddresses"));
                        $supplement->setField("source", "ФБДРЛ");
                        $supplement->setField("is_actual", 1);
                        $supplement->setField("is_updated", 1);
                        $supplement->setField("file_xml",$item->getField("SupDocXML"));
                        $supplement->setField("file_sign",$item->getField("SignFile"));
                        $supplement->save();

                        //Если новое приложение добавляем документы
                        if ($isNew){
                            $createDoc = new \Model\LicReestrEntities\decision();
                            $createDoc->setField("license_attach_guid", $supplement->getField("guid"));
                            $createDoc->setField("type_guid", "e0e8eb9952464ea798f7e41b6a81c93f");
                            $createDoc->setField("type_name", $item->getField("DocTypeName"));
                            $createDoc->setField("date", $item->getField("DocDate"));
                            $createDoc->setField("number", $item->getField("DocNumber"));
                            $createDoc->setField("is_actual", 1);
                            $createDoc->setField("source", "ФБДРЛ");
                            $createDoc->save();
                        }
                        if (!$item->isEmptyField("TerminateBase") && !$item->isEmptyField("TerminateDate") && $supplement->getField("is_terminated") == 0) {
                            $terminateDoc = new \Model\LicReestrEntities\decision();
                            $terminateDoc->setField("license_attach_guid", $supplement->getField("guid"));
                            $terminateDoc->setField("type_guid", "1dc3e282134a4b7e9578216e6f07f096");
                            $terminateDoc->setField("type_name", "Распоряжение");
                            $terminateDoc->setField("date", $item->getField("TerminateDate"));
                            $terminateDoc->setField("number", $item->getField("TerminateBase"));
                            $terminateDoc->setField("is_actual", 1);
                            $terminateDoc->setField("source", "ФБДРЛ");
                            $terminateDoc->save();

                            $supplement->setField("is_terminated", 1);
                            $supplement->save();
                        }
                    }
                }
                $logger->Log("Конец обработки temp_fSupplements");
                echo "\n <br> Конец обработки temp_fSupplements";
            }

            //таблица temp_fDirections
            if($partsOfWork["temp_fDirections"]) {
                $dirTable = new Model\Gateway\LicReestrEntitiesTable("temp_fDirections");
                $count = $dirTable->getCount();
                $pages = ceil($count / 5000);
                $logger->Log("Обработка temp_fDirections, страниц: " . $pages);
                echo "\n <br> Обработка temp_fDirections, страниц: " . $pages;
                for ($page = 1; $page <= $pages; $page++) {
                    $logger->Log("страница " . $page);
                    echo "\n <br> страница " . $page;
                    $items = $dirTable->getAllEntities([
                        "page" => $page,
                        "onPage" => 5000
                    ], "Id ASC");
                    foreach ($items as $item) {

                        //Пропускаем все связанные с приложениями связанные с лицензией выданной органом Федеральная служба в сфере образования


                        $supplement = \Model\LicReestrEntities\supplement::getSupplementByGUID($item->getField("SupplementFk"));
                        if ($supplement != null) {
                            $license = \Model\LicReestrEntities\license::getLicenseByGUID($supplement->getField("license_guid"));
                            if ($license != null)
                                $licOrg = $license->getLicOrgan();
                            if ($licOrg != null && strtolower($licOrg->getField("guid")) == strtolower("BFB891F1F234F61C11835CDEAF8E9CA0"))
                                continue;
                        }

                        //Получаем запись с таким же айди, вдруг она есть в бд
                        $program = Model\LicReestrEntities\program::getProgramByGUID($item->getId());
                        //если нет, создаем новый объект
                        $isNew = false;
                        if ($program == null) {
                            $program = new Model\LicReestrEntities\program();
                            $isNew = false;
                        }

                        //Теперь парсим поля и раскидываем по текущей бд
                        $program->setField("guid", $item->getId());

                        //Приложение
                        $program->setField("supplement_guid", $item->getField("SupplementFk"));
                        if ($supplement != null)
                            $program->setField("supplement_id", $supplement->getId());

                        //Другие поля
                        $program->setField("level", $item->getField("Level"));
                        $program->setField("code", $item->getField("Code"));
                        $program->setField("name", $item->getField("Name"));
                        $program->setField("type", $item->getField("Kind"));
                        $program->setField("qualification", $item->getField("Qualification"));
                        $program->setField("source", "ФБДРЛ");
                        $program->setField("is_actual", 1);
                        $program->setField("is_updated", 1);
                        $program->save();

                    }
                }
                $logger->Log("Конец обработки temp_fDirections");
                echo "\n <br> Конец обработки temp_fDirections";
            }

        } catch(\Exception $e){
            $logger->Log("Ошибка:".$e->getMessage());
            echo "\n <br> Ошибка:".$e->getMessage();
        }

        if($partsOfWork["IsUpdated"]) {
            $logger->Log("Задаем не актуальность всем не обработанным сущностям");
            echo "\n <br> Задаем не актуальность всем не обработанным сущностям";


            $statement = $adapter->query("UPDATE `license` SET `is_actual`=0 WHERE `is_updated`=0 and source is not null");
            $statement->execute();
            $statement = $adapter->query("UPDATE `program` SET `is_actual`=0 WHERE `is_updated`=0 and source is not null");
            $statement->execute();
            $statement = $adapter->query("UPDATE `supplement` SET `is_actual`=0 WHERE `is_updated`=0 and source is not null");
            $statement->execute();
        }

        $logger->Log("Конец обработки");
        echo "\n <br> Конец обработки";
        die;
    }


    public function updateLicenseSupplementsAction(){
        //$serviceEntities = new \Model\EiisService\ServiceEntities();
        //$serviceEntities->getEntitiesFromService("EIIS.F_DIRECTIONS", 140);
        die;
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Приложения к лицензиям юолбше не обновляются, мы источник");
        /*
        $logger->Log("Старт скрипта получения Приложений к Лицензиям - updateLicenseSupplements");


        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.LICENSE_APPS", 60);
        */
        die;
    }

    public function updateCertificateSupplementsAction(){
        /*
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Приложений к Сертификатам - updateCertificateSupplements");


        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.ISGA.CERTIFICATE_SUPPLEMENTS", 60);
        */
        die;
    }

    public function updateSchoolsAction(){
        /*
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Обр Организаций - updateSchools");

        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.SCHOOLS", 60);
        */
        die;
    }

    public function updateCertificatesAction(){
        //die;

        $partsOfWork = [
            "TempDelete"        => true,
            "IsUpdated"         => true,
            "UpdateFromEIIS"    => true,
            "temp_fCertificates"=> true,
            "temp_fSupplements" => true,
            "temp_fDirections"  => true
        ];


        set_time_limit(0);

        $logger = new \Model\Logger\Logger("fupdate",true);
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Начинаем скрипт получения данных с ФБДРА");

        $adapter = Model\Gateway\CertReestrDbAdapter::getInstance();

        //Чистим бд от того что там уже есть
        if($partsOfWork["TempDelete"]) {
            $logger->Log("Чистим темп таблицы бд");
            echo "\n <br> Чистим темп таблицы бд";

            $statement = $adapter->query("TRUNCATE TABLE `temp_fCertificates`");
            $statement->execute();

            $statement = $adapter->query("TRUNCATE TABLE `temp_fSupplements`");
            $statement->execute();

            $statement = $adapter->query("TRUNCATE TABLE `temp_fDirections`");
            $statement->execute();
        }

        if($partsOfWork["IsUpdated"]) {
            $logger->Log("Устанавливаем все is_updated в 0");
            echo "\n <br> Устанавливаем все is_updated в 0";

            $statement = $adapter->query("UPDATE `accreditationcertificate` SET `is_updated`=0 WHERE 1");
            $statement->execute();
            $statement = $adapter->query("UPDATE `accreditedprogramm` SET `is_updated`=0 WHERE 1");
            $statement->execute();
            $statement = $adapter->query("UPDATE `certificatesupplement` SET `is_updated`=0 WHERE 1");
            $statement->execute();
        }

        //Получаем данные с ЕИИС
        if($partsOfWork["UpdateFromEIIS"]) {


            $params = null;
            if($this->isTest)
                $params = [
                    "wsdl" => "http://eiis-production.srvdev.ru/IntegrationService/BaseService.asmx?wsdl"
                ];

            $logger->Log("Получаем данные EIIS.F_CERTIFICATES");
            echo "\n\n <br> Получаем данные EIIS.F_CERTIFICATES";
            $serviceEntities = new \Model\EiisService\ServiceEntities($params);
            $serviceEntities->getEntitiesFromService("EIIS.F_CERTIFICATES", 100);

            $logger->Log("Получаем данные EIIS.F_CER_SUPPLEMENTS");
            echo "\n\n <br> Получаем данные EIIS.F_CER_SUPPLEMENTS";
            $serviceEntities = new \Model\EiisService\ServiceEntities($params);
            $serviceEntities->getEntitiesFromService("EIIS.F_CER_SUPPLEMENTS", 100);

            $logger->Log("Получаем данные EIIS.F_CER_DIRECTIONS");
            echo "\n\n <br> Получаем данные EIIS.F_CER_DIRECTIONS";
            $serviceEntities = new \Model\EiisService\ServiceEntities($params);
            $serviceEntities->getEntitiesFromService("EIIS.F_CER_DIRECTIONS", 100);
        }

        //Проходим по таблицам и парсим блоками по 5к

        try {
            //таблица temp_fCertificates
            if($partsOfWork["temp_fCertificates"]) {
                $certTable = new Model\Gateway\CertReestrEntitiesTable("temp_fCertificates");
                $count = $certTable->getCount();
                $pages = ceil($count / 5000);
                $logger->Log("Обработка temp_fCertificates, страниц: " . $pages);
                echo "\n <br> Обработка temp_fCertificates, страниц: " . $pages;
                for ($page = 1; $page <= $pages; $page++) {
                    $logger->Log("страница " . $page);
                    echo "\n <br> страница " . $page;
                    $items = $certTable->getAllEntities([
                        "page" => $page,
                        "onPage" => 5000
                    ], "Id ASC");
                    foreach ($items as $item) {
                        //Пропускаем все свидетельства выданные органом Федеральная служба в сфере образования
                        if (strtolower($item->getField("AkkredOrganFK")) == strtolower("BFB891F1F234F61C11835CDEAF8E9CA0"))
                            continue;

                        //Получаем запись с таким же айди, вдруг она есть в бд
                        $certificate = Model\CertReestrEntities\accreditationcertificate::getByGUID($item->getId());

                        //если нет, создаем новый объект
                        $isNew = false;
                        if ($certificate == null) {
                            $certificate = new Model\CertReestrEntities\accreditationcertificate();
                            $isNew = true;
                        } else {
                            if($certificate->getField("source") != "ФБДРА")
                                continue;
                        }


                        //Теперь парсим поля и раскидываем по текущей бд
                        $certificate->setField("Id", $item->getId());

                        $certificate->setField("EduOrgFullName", $item->getField("FullOrgName"));
                        $certificate->setField("EduOrgOGRN", $item->getField("OGRN"));
                        $certificate->setField("EduOrgINN", $item->getField("INN"));
                        $certificate->setField("RegionId", $item->getField("RegionFk"));
                        $certificate->setField("RegNumber", $item->getField("SvidNumber"));
                        $certificate->setField("IssueDate", $item->getField("SvidDate"));
                        $certificate->setField("EndDate", $item->getField("SvidEndDate"));
                        $certificate->setField("SerialNumber", $item->getField("SvidBlankSer"));
                        $certificate->setField("FormNumber", $item->getField("SvidBlankNum"));
                        $certificate->setField("file_xml",$item->getField("SvidDocXML"));
                        $certificate->setField("file_sign",$item->getField("SignFile"));


                        $certificateStatus = \Model\CertReestrEntities\certificatestatus::getByName($item->getField("SvidStatus"));
                        if($certificateStatus) {
                            $certificate->setField("certificatestatus_id", $certificateStatus->getField("Id"));
                            $certificate->setField("CertificateStatusId", $certificateStatus->getField("Id"));
                        }

                        $schoolId = $item->getField("SchoolFk");
                        if(!$item->isEmptyField("IndividualFk"))
                            $schoolId = $item->getField("IndividualFk");


                        $certificate->setField("ControlOrgan", $item->getField("AkkredOrgan"));
                        $certificate->setField("IndividualEmployeeLastName", $item->getField("NameIP"));
                        $certificate->setField("PostAddress", $item->getField("Address"));
                        $certificate->setField("EduOrgId", $schoolId);
                        $certificate->setField("CertificateTypeId", "352123ca-6a2b-4ded-900b-dc5dee7f596d");
                        $certificate->setField("Active", 1);
                        $certificate->setField("is_updated", 1);

                        if($isNew)
                            $certificate->setField("source", "ФБДРА");

                        $region = \Model\CertReestrEntities\region::getByName($item->getField("Region"));
                        if($region)
                            $certificate->setField("RegionId", $region->getField("Id"));

                        //Организация
                        $eo = Model\CertReestrEntities\educationorganization::getByGUID($schoolId);
                        if ($eo == null) {
                            $eo = new Model\CertReestrEntities\educationorganization();
                            $eo->setField("Id", $schoolId);
                            $eo->setField("source", "ФБДРА");
                        }
                        if($eo->getField("source") == "ФБДРА" ) {

                            $eo->setField("FullName", $item->getField("FullOrgName"));
                            if(!$item->isEmptyField("IndividualFk")){
                                $eo->setField("FullName", $item->getField("NameIP"));
                            }


                            $eo->setField("HeadName", $item->getField("OrgRukFio"));
                            $eo->setField("HeadPost", $item->getField("OrgRukPost"));
                            $eo->setField("OGRN", $item->getField("OGRN"));
                            $eo->setField("INN", $item->getField("INN"));
                            $eo->setField("Phone", $item->getField("Phone"));
                            $eo->setField("Fax", $item->getField("Fax"));
                            $eo->setField("Email", $item->getField("Mail"));
                            $eo->setField("WebSite", $item->getField("WebSite"));
                            if ($region)
                                $eo->setField("RegionId", $region->getField("Id"));
                            $eo->setField("PostAddress", $item->getField("Address"));
                            $eo->save();
                        }

                        $certificate->setField("eduorg_id", $eo->getId());

                        $certificate->save();

                        //Если новое свидетельство, добавляем документ
                        if ($isNew && !$item->isEmptyField("DocTypeName") && !$item->isEmptyField("DocNumber")){
                            $doc = new \Model\CertReestrEntities\certificatedecision();
                            $doc->setField("certificate_id", $certificate->getId());
                            $doc->setField("CertificateId", $item->getId());
                            $doc->setField("OrderDocumentKind", $item->getField("DocTypeName"));
                            $doc->setField("OrderDocumentNumber", $item->getField("DocNumber"));
                            $doc->setField("DecisionDate", $item->getField("DocDate"));
                            $doc->setField("source", "ФБДРА");
                            $doc->save();
                        }
                    }
                }
                $logger->Log("Конец обработки temp_fCertificates");
                $logger->Log("");
                echo "\n <br> Конец обработки temp_fCertificates\n\n";
            }

            //таблица temp_fSupplements
            if($partsOfWork["temp_fSupplements"]) {
                $suppTable = new Model\Gateway\CertReestrEntitiesTable("temp_fSupplements");
                $count = $suppTable->getCount();
                $pages = ceil($count / 5000);
                $logger->Log("Обработка temp_fSupplements, страниц: " . $pages);
                echo "\n <br> Обработка temp_fSupplements, страниц: " . $pages;
                for ($page = 1; $page <= $pages; $page++) {
                    $logger->Log("страница " . $page);
                    echo "\n <br> страница " . $page;
                    $items = $suppTable->getAllEntities([
                        "page" => $page,
                        "onPage" => 5000
                    ], "Id ASC");
                    foreach ($items as $item) {
                        //Пропускаем все приложения выданные органом Федеральная служба в сфере образования
//echo "\n n1";
                        $certificate = Model\CertReestrEntities\accreditationcertificate::getByGUID($item->getField("CertificateFk"));
//echo "\n n2";
                        if (!$certificate || strtolower($certificate->getField("ControlOrgan")) == strtolower("Федеральная служба по надзору в сфере образования и науки"))
                            continue;
//echo "\n n3";
                        //Получаем запись с таким же айди, вдруг она есть в бд
                        $supplement= Model\CertReestrEntities\certificatesupplement::getByGUID($item->getId());
//echo "\n n4";
                        //если нет, создаем новый объект
                        $isNew = false;
                        if ($supplement == null) {
                            $supplement = new Model\CertReestrEntities\certificatesupplement();
                            $isNew = true;
                        } else {
                            if($supplement->getField("source") != "ФБДРА")
                                continue;
                        }
//echo "\n n5";
                        //Теперь парсим поля и раскидываем по текущей бд
                        $supplement->setField("id", $item->getId());
//echo "\n n7";
                        $supplement->setField("CertificateId", $item->getField("CertificateFk"));

                        $certificate = \Model\CertReestrEntities\accreditationcertificate::getByGUID($item->getField("CertificateFk"));

                        $schoolId = $item->getField("SchoolFk");
                        if(!$item->isEmptyField("IndividualFk"))
                            $schoolId = $item->getField("IndividualFk");


                        $supplement->setField("certificate_id", $certificate->getId());

                        $supplement->setField("Number", $item->getField("SupNumber"));
                        $supplement->setField("IssueDate", $item->getField("SupDate"));
                        $supplement->setField("SerialNumber", $item->getField("SupBlankSer"));
                        $supplement->setField("FormNumber", $item->getField("SupBlankNum"));
                        $supplement->setField("EduOrgFullName", $item->getField("FullOrgName"));
                        $supplement->setField("file_xml",$item->getField("SupDocXML"));
                        $supplement->setField("file_sign",$item->getField("SignFile"));
                        $supplement->setField("is_updated", 1);
//echo "\n n8";
                        $supplement->setField("CertificateSupplementStatusId", $item->getField("SupStatusFk"));

                        $supplementstate = \Model\CertReestrEntities\certificatesupplementstatus::getByGUID($item->getField("SupStatusFk"));
                        if($supplementstate)
                            $supplement->setField("certificatesupplementstatus_id", $supplementstate->getId());

                        $supplement->setField("EduOrgId", $schoolId);
                        //Организация
                        $eo = Model\CertReestrEntities\educationorganization::getByGUID($schoolId);
                        if ($eo == null) {
                            $eo = new Model\CertReestrEntities\educationorganization();
                            $eo->setField("Id", $schoolId);
                            $eo->setField("source", "ФБДРА");
                        }
                        if($eo->getField("source") == "ФБДРА" ) {
                            if (!$item->isEmptyField("FullOrgName"))
                                $eo->setField("FullName", $item->getField("FullOrgName"));
                            $eo->save();
                        }

                        $supplement->setField("eduorg_id", $eo->getId());
                        if($isNew)
                            $supplement->setField("source", "ФБДРА");

                        //var_dump($supplement->getFields());
                        $supplement->save();

//echo "\n n10";
                        //Если новое свидетельство, добавляем документ
                        if ($isNew && !$item->isEmptyField("DocTypeName") && !$item->isEmptyField("DocNumber")){
                            $doc = new \Model\CertReestrEntities\certificatedecision();
                            $doc->setField("certificate_id", $certificate->getId());
                            $doc->setField("CertificateId", $certificate->getField("Id"));
                            $doc->setField("OrderDocumentKind", $item->getField("DocTypeName"));
                            $doc->setField("OrderDocumentNumber", $item->getField("DocNumber"));
                            $doc->setField("DecisionDate", $item->getField("DocDate"));
                            $doc->setField("startedsupplement_id", $supplement->getId());
                            $doc->setField("StartedSupplementId", $supplement->getField("id"));
                            $doc->setField("StartedSupplementNumber", $supplement->getField("Number"));
                            $doc->setField("StartedSupplementFormNumber", $supplement->getField("FormNumber"));
                            $doc->setField("StartedSupplementSerialNumber", $supplement->getField("SerialNumber"));
                            $doc->setField("StartedSupplementIssueDate", $supplement->getField("IssueDate"));
                            $doc->setField("source", "ФБДРА");
                            $doc->save();
                        }
//echo "\n n11";
                    }
                }
                $logger->Log("Конец обработки temp_fSupplements");
                $logger->Log("");
                echo "\n <br> Конец обработки temp_fSupplements\n\n";
            }

            //таблица temp_fDirections
            if($partsOfWork["temp_fDirections"]) {
                $dirTable = new Model\Gateway\CertReestrEntitiesTable("temp_fDirections");
                $count = $dirTable->getCount();
                $pages = ceil($count / 5000);
                $logger->Log("Обработка temp_fDirections, страниц: " . $pages);
                echo "\n <br> Обработка temp_fDirections, страниц: " . $pages;
                for ($page = 1; $page <= $pages; $page++) {
                    $logger->Log("страница " . $page);
                    echo "\n <br> страница " . $page;
                    $items = $dirTable->getAllEntities([
                        "page" => $page,
                        "onPage" => 5000
                    ], "Id ASC");
                    foreach ($items as $item) {
                        //Пропускаем все приложения выданные органом Федеральная служба в сфере образования
                        $supplement = Model\CertReestrEntities\certificatesupplement::getByGUID($item->getField("SupplementFk"));
                        $certificate = null;
                        if($supplement)
                            $certificate = $supplement->getCertificate();

                        if (!$certificate || strtolower($certificate->getField("ControlOrgan")) == strtolower("Федеральная служба по надзору в сфере образования и науки"))
                            continue;

                        //Получаем запись с таким же айди, вдруг она есть в бд
                        $direction = Model\CertReestrEntities\accreditedprogramm::getByGUID($item->getId());
                        //если нет, создаем новый объект
                        $isNew = false;
                        if ($direction  == null) {
                            $direction  = new Model\CertReestrEntities\accreditedprogramm();
                            $isNew = true;
                        } else {
                            if($certificate->getField("source") != "ФБДРА")
                                continue;
                        }

                        //Теперь парсим поля и раскидываем по текущей бд
                        $direction->setField("Id", $item->getId());

                        $direction->setField("CertificateSupplementId", $item->getField("SupplementFk"));

                        $edulevel = \Model\CertReestrEntities\edulevel::getByName($item->getField("Level"));
                        if($edulevel) {
                            $direction->setField("edulevel_id", $edulevel->getId());
                            $direction->setField("EduLevelId", $edulevel->getField("Id"));
                        }

                        $direction->setField("UGSCode", $item->getField("CodeUgsn"));
                        $direction->setField("UGSName", $item->getField("NameUgsn"));
                        $direction->setField("ProgrammCode", $item->getField("CodeNp"));
                        $direction->setField("ProgrammName", $item->getField("NameNp"));
                        $direction->setField("is_updated", 1);
                        if($isNew)
                            $direction->setField("source", "ФБДРА");

                        switch($item->getField("DirectionStatus")){
                            case "Приостановлено":
                                $direction->setField("Suspended", "1");
                                break;
                            case "Лишение":
                                $direction->setField("Canceled", "1");
                                break;
                        }
                        $direction->save();

                        /*
                        _id
                        programmtype_id
                        programmkind_id

                        edusublevel_id
                        certificatesupplement_id
                        EduNormativePeriod
                        Qualification
                        OKSOCode
                        EduProgrammTypeId
                        EduProgrammKindId

                        EduSublevelId

                        IsDeleted
                         */

                    }
                }
                $logger->Log("Конец обработки temp_fDirections");
                $logger->Log("");
                echo "\n <br> Конец обработки temp_fDirections\n\n";
            }

        } catch(\Exception $e){
            $logger->Log("Ошибка:".$e->getMessage());
            echo "\n <br> Ошибка:".$e->getMessage();
        }

        if($partsOfWork["IsUpdated"]) {
            $logger->Log("Задаем не актуальность всем не обработанным сущностям");
            echo "\n <br> Задаем не актуальность всем не обработанным сущностям";


            $statement = $adapter->query("DELETE FROM `accreditationcertificate` WHERE `is_updated`=0 and source is not null");
            $statement->execute();
            $statement = $adapter->query("DELETE FROM `accreditedprogramm` WHERE `is_updated`=0 and source is not null");
            $statement->execute();
            $statement = $adapter->query("DELETE FROM `certificatesupplement` WHERE `is_updated`=0 and source is not null");
            $statement->execute();
        }

        $logger->Log("Конец получения");
        echo "\n <br> Конец получения";

        die;
    }

    public function updateEduProgramsAction(){
        /*
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Обр Программ - updateEduPrograms");

        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.EDUPROGRAMS", 60, true);
        $serviceEntities->getEntitiesFromService("ISLOD.NEW_EDUPROGRAMS", 60, true);
        */
        die;
    }

    public function updateUGSAction(){
        /*
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Укрупненных груп специальностей - updateUGS");

        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.UGS", 60);
        */
        die;
    }

    public function updateAccrProgramsAction(){
        /*
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Аккр Программ - updateAccrPrograms");

        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.ISGA.ACCREDITED_PROGRAMS", 260, true);

        //Запускаем скрипт, что
        $logger->Log("Старт скрипта задания Аккр Программам связей");

        //проходим по всем аккр програмам
        $table = new Model\Gateway\EntitiesTable("isga_AccreditedPrograms");
        $progs = $table->getAllEntities();

        $eduProgramsTable = new Model\Gateway\EntitiesTable("eiis_EduPrograms");

        $num = 0;
        $n = 0;

        foreach($progs as $prog){
            $n++;
            if(!$prog->isEmptyField("fk_isgaGroupSpeciality") || !$prog->isEmptyField("isga_EnlargedGroupSpeciality"))
                continue;

            if($prog->isEmptyField("Code"))
                continue;

            $code = $prog->getField("Code");

            $ep = $eduProgramsTable->getAllEntities(null,null,array(
                "Code" => $code
            ));

            $ep = $ep->getObjectsArray();

            if(count($ep) >0){
                $prog->setField("fk_isgaEnlargedGroupSpeciality", $ep[0]->getField("fk_eiisUgs"));
                if($prog->isEmptyField("fk_eiisEduProgram")){
                    $prog->setField("fk_eiisEduProgram", $ep[0]->getField("Id"));
                }
                $prog->save(false, true);
                $num++;
            }
        }
        $logger->Log("Отработало ".$num." записей из ".$n);

        $logger->Log("Конец работы скрипта");
        */
        die;
    }

    public function updateLicProgramsAction(){
        /*
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Лиц Программ - updateLicPrograms");

        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.LICENSED_PROGRAMS", 260, true);
        */
        die;
    }

    public function updateApplicationsAction(){
        /*
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Заявлений - updateApplications");

        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.ISGA.APPLICATIONS", 60);
        */
        die;
    }


    public function serviceCreatePackageAction(){
        $logger = new \Model\Logger\Logger("service",true);
        $logger->Log("Старт скрипта создания пакета в отдельном потоке");
        $packageId = $this->getRequest()->getParam('packageId');
        //$logger->Log($packageId);
        $sh = new \Service\Model\ServiceHelper();
        $sh->createPackage($packageId);
    }

    /**
     * Консольныq экшн, что будет обрабатывать работу по формированию
     */
    public function threadWorkAction(){
        /*
        $adapter = Model\Gateway\LicReestrDbAdapter::getInstance();

        $statement = $adapter->query("TRUNCATE TABLE `temp_fSupplements`");
        $statement->execute();

        //Получаем данные с ЕИИС
        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $sericeEntities->getEntitiesFromService("EIIS.F_SUPPLEMENTS", 100);
        die;*/
        /*
        //Таблица с пакетами
        $packageTable = new \Model\Gateway\EntitiesTable("isgaservice_Package");
        //помечаем новые пакеты,
        //Получаем новые пакеты
        $newPackages = $packageTable->getAllEntities(null,null,array(
            "State" => "New"
        ))->getObjectsArray();

        //меняем статус у всех этих пакетов
        foreach($newPackages as $np){
            $np->setField("State","Working");
            $np->save();
        }

        if(count($newPackages)>0){
            foreach($newPackages as $np){
                $sh = new \Service\Model\ServiceHelper();
                $sh->createPackage($np->getId());
            }
        }*/
    }
}