<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Zend\Db\ResultSet\ResultSet;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

class DbController extends AbstractActionController
{
    private function transferData($oldEntitiesTable, $newTableName, $transferArray){
        $oldEntities = $oldEntitiesTable->getAllEntities();
        $num = 0;
        foreach($oldEntities as $oldEntity){
            $oldFields = $oldEntity->getFields();
            $newFields = array();
            foreach($transferArray as $newKey => $oldKey){
                $newFields[$newKey] = $oldFields[$oldKey];
            }
            $newEntity = new Model\Gateway\PBaseEntity($newTableName);
            $newEntity->setFields($newFields);
            $newEntity->save(true);
            $num++;
        }
        echo "<br>Создано: ".$num."<br>";
    }

    private function transferComplexData($oldEntitiesTable, $newTableName, $transferArray){
        $oldEntities = $oldEntitiesTable->getAllEntities();
        $idArray = array();

        $k = 0;
        foreach($oldEntities as $oldEntity){
            $oldFields = $oldEntity->getFields();
            if(!isset($idArray[$oldFields["Id"]]))
                $idArray[$oldFields["Id"]] = array();

            $idArray[$oldFields["Id"]][] = $oldFields;
        }

        echo "done";

        $otArray = array();

        foreach($idArray as $arr){
            $returnArr = array();
            foreach($arr as $fields){
                foreach($fields as $key => $value){
                    if(!empty($value) || !isset($returnArr[$key]))
                        $returnArr[$key] = $value;
                }
            }
            $otArray[] = $returnArr;
        }
        unset($idArray);
        echo "otArray: ".count($otArray)."<br>";

        /*
        foreach($otArray as $arr){
            $echoarr = array();
            foreach($arr as $field)
                $echoarr[] = $field;
            echo implode(", ",$echoarr)."<br>";
        }*/
        $num = 0;
        foreach($otArray as $oldFields){
            $newFields = array();
            foreach($transferArray as $newKey => $oldKey){
                $newFields[$newKey] = $oldFields[$oldKey];
            }
            $newEntity = new Model\Gateway\PBaseEntity($newTableName);
            $newEntity->setFields($newFields);
            try{
                $newEntity->save();
            } catch (\Exception $e){
                echo $e->getMessage();
            }
            unset($newEntity);
            $num++;
            echo $num."<br>";
        }
        echo "<br>Создано: ".$num."<br>";
    }

    private function transferComplexDataById($oldEntitiesTable, $newTableName, $transferArray){
        $Ids = $oldEntitiesTable->getDistinctIds();

        $num = 0;
        foreach($Ids as $Id){
            $Id = $Id["Id"];
            $arr = $oldEntitiesTable->getEntitiesByWhere(array("Id" => $Id));
            $returnArr = array();
            foreach($arr as $item){
                $fields = $item->getFields();
                foreach($fields as $key => $value){
                    if(!empty($value) || !isset($returnArr[$key]))
                        $returnArr[$key] = $value;
                }
            }

            $newFields = array();
            foreach($transferArray as $newKey => $oldKey){
                $newFields[$newKey] = $returnArr[$oldKey];
            }
            $newEntity = new Model\Gateway\PBaseEntity($newTableName);
            $newEntity->setFields($newFields);

/*
                $echoarr = array();
                foreach($newFields as $field)
                    $echoarr[] = $field;
                echo implode(", ",$echoarr)."<br>";
*/

            try{
                $newEntity->save(true);
            } catch (\Exception $e){
                echo $e->getMessage();
            }
            unset($newEntity);
            $num++;
            echo $num."<br>";

        }
        echo "<br>Создано: ".$num."<br>";
    }

    private function showTable($tableName){
        $table = new Model\Gateway\PEntitiesTable($tableName);
        $entities = $table->getAllEntities();
        foreach($entities as $entity){
            $fields = $entity->getFields();
            $arr = array();
            foreach($fields as $field){
                $arr[] = $field;
            }
            echo implode(", ", $arr)."<br>";
        }
    }

    public function indexAction()
    {
        //phpinfo();die;
        echo "index";die;
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        //$this->showTable("isga_CertificateSupplementStatuses");

        //$this->CertificateSupplementStatuses();
        //$this->ControlOrgans();
        //$this->Countries();
        //$this->EduLevels();
        //$this->EduProgramTypes();
        //$this->EduDirectories();
        //$this->EduPrograms();
        //$this->EducationalOrganizationKinds();
        //$this->EducationalOrganizationProperties();
        //$this->EducationalOrganizationTypes();
        //$this->EducationalOrganizations();
        //$this->FedOkr();
        //$this->LicenseStates();
        //$this->LicenseSupplementStates();
        //$this->LicenseSupplements();
        //$this->LicensedPrograms();
        //$this->Licenses();
        //$this->Regions();
        //$this->AccreditationKinds();
        //$this->AccreditedPrograms();
        //$this->ApplicationReasons();
        //$this->ApplicationStatuses();
        //$this->ApplicationTypes();
        //$this->Applications();
        //$this->CertificateStatuses();
        //$this->CertificateSupplements();
        //$this->CertificateTypes();
        //$this->Certificates();
        //=============================
        //$this->CheckStatuses();
        //$this->CollegeStatuses();
        //$this->Colleges();
        //$this->EduStandards();
        //$this->EnlargedGroupSpecialities();
        //$this->EnlargedGroupSpecialityEditions();
        //$this->FounderTypes();
        //$this->Founders();
        //$this->GroupSpecialities();
        //$this->OrderDocumentKinds();
        //$this->OrderDocumentStatuses();
        //$this->OrderDocumentTypes();
        //$this->OrderDocuments();
        //$this->Qualifications();
        //$this->mtm_Founders_EducationalOrganizations();

        //$this->FormDocumentGroupStatus();
        //$this->FormDocumentGroup();
        //$this->FormDocumentGroupGroupStatusReason();
        //$this->FormDocumentGroupStatusReason();
        //$this->FormDocumentType();
        //$this->Notification();
        $this->NotificationTemplate();
        die;
        return new ViewModel();
    }

    private function CertificateSupplementStatuses(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("CertificateSupplementStatus");
        $this->transferData($oldCS, "isga_CertificateSupplementStatuses",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code"
        ));
    }

    private function ControlOrgans(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("ControlOrgan");
        $this->transferComplexData($oldCS, "eiis_ControlOrgans",array(
            "Id" => "Id",
            "Name" => "FullName",
            "fk_eiisRegions" => "LAddressRegionId"
        ));
    }

    private function Countries(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Country");
        $this->transferComplexData($oldCS, "eiis_Countries",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code3" => "EiisId"
        ));
    }

    private function EduLevels(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalProgramLevel");
        $this->transferComplexData($oldCS, "eiis_EduLevels",array(
            "Id" => "Id",
            "Name" => "Name",
            "ShortName" => "ShortName",
            "EduProgramKind" => "Code"
        ));
    }

    private function EduProgramTypes(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalProgramType");
        $this->transferComplexData($oldCS, "eiis_EduProgramTypes",array(
            "Id" => "Id",
            "Name" => "Name"
        ));
    }

    private function EduDirectories(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalProgramSublevel");
        $this->transferComplexData($oldCS, "eiis_EduDirectories",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code",
            "fk_eiisEduLevels" => "EducationalProgramLevelId"
        ));
    }

    private function EduPrograms(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalProgram");
        $this->transferComplexData($oldCS, "eiis_EduPrograms",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code",
            "fk_eiisEduProgramType" => "EducationalProgramTypeId",
            "fk_isgaGS" => "SpecialitySubgroupId",
            "fk_isgaQualification" => "QualificationId",
            "QualificationGrade" => "QualificationGrade",
            "Period" => "EduNormativePeriod",
            "RangeTariff" => "WageGradeRange",
            "fk_isgaEduStandard" => "EducationalProgramStandardId",
            "EduNote" => "Note",
            "fk_eiisEduDirectory" => "EducationalProgramSublevelId"
        ));
    }

    private function EducationalOrganizationKinds(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalOrganizationKind");
        $this->transferComplexData($oldCS, "eiis_EducationalOrganizationKinds",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code",
            "fk_eiisEducationalOrganizationType" => "EducationalOrganizationTypeId"
        ));
    }

    private function EducationalOrganizationProperties(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("OrgForm");
        $this->transferComplexData($oldCS, "eiis_EducationalOrganizationProperties",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code"
        ));
    }

    private function EducationalOrganizationTypes(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalOrganizationType");
        $this->transferComplexData($oldCS, "eiis_EducationalOrganizationTypes",array(
            "Id" => "Id",
            "Name" => "Name",
            "EOSubTypeCode" => "Code"
        ));
    }

    private function EducationalOrganizations(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalOrganization");
        $this->transferComplexData($oldCS, "eiis_EducationalOrganizations",array(
            "Id" => "Id",
            "IslodGuid" => "IslodId",
            "FullName" => "FullName",
            "ShortName" => "ShortName",
            "RegularName" => "RegularName",
            "fk_eiisEducationalOrganizationProperties" => "OrgFormId",
            "fk_eiisEducationalOrganizationType" => "EducationalOrganizationTypeId",
            "fk_eiisEducationalOrganizationKind" => "EducationalOrganizationKindId",
            "Branch" => "IsBranch",
            "fk_eiisParentEducationalOrganization" => "HeadEducationalOrganizationId",
            "LawAddress" => "LAddress",
            "LawPostIndex" => "LAddressPostalCode",
            "fk_eiisLawCountry" => "CountryId",
            "fk_eiisLawRegion" => "AddressRegionId",
            "LawCity" => "LAddressTown",
            "LawStreet" => "LAddressStreet",
            "LawHouse" => "LAddressHouseNumber",
            "Address" => "PAddress",
            "PostIndex" => "PAddressPostalCode",
            "fk_eiisCountry" => "CountryId",
            "fk_eiisRegion" => "AddressRegionId",
            "TownName" => "PAddressTown",
            "Street" => "PAddressStreet",
            "House" => "PAddressHouseNumber",
            "Phones" => "Phone",
            "Faxes" => "Fax",
            "Mails" => "Email",
            "Www" => "Webpage",
            "GosRegNum" => "Ogrn",
            "Inn" => "Inn",
            "Kpp" => "Kpp",
            "ChargePosition" => "HeadPost",
            "ContactFirstName" => "HeadFirstName",
            "ContactSecondName" => "HeadLastName",
            "ContactLastName" => "HeadPatronymic",
            "Outdated" => "Deleted"
        ));
    }

    private function FedOkr(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FederalDistrict");
        $this->transferComplexData($oldCS, "eiis_FedOkr",array(
            "Id" => "Id",
            "Name" => "Name",
            "ShortName" => "ShortName"
        ));
    }

    private function LicenseStates(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("LicenseStatus");
        $this->transferComplexData($oldCS, "eiis_LicenseStates",array(
            "Id" => "Id",
            "Name" => "Name",
            "IsValid" => "IsValid"
        ));
    }

    private function LicenseSupplementStates(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("LicenseSupplementStatus");
        $this->transferComplexData($oldCS, "eiis_LicenseSupplementStates",array(
            "Id" => "Id",
            "Name" => "Name",
            "IsValid" => "IsValid"
        ));
    }

    private function LicenseSupplements(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("LicenseSupplement");
        $this->transferComplexDataById($oldCS, "eiis_LicenseSupplements",array(
            "Id" => "Id",
            "fk_eiisLicense" => "LicenseId",
            "Number" => "Number",
            "SerDoc" => "SerialNumber",
            "NumDoc" => "FormNumber",
            "fk_eiisLicenseSupplementState" => "LicenseSupplementStatusId",
            "DateEnd" => "EndDate",
            "CommentSup" => "Comment",
            "fk_eiisControlOrgan" => "ControlOrganId",
            "fk_eiisBranch" => "EducationalOrganizationId",
            "ImplAddr" => "InstitutionAddress",
            "DateReg" => "IssueDate"
        ));
    }

    private function LicensedPrograms(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("LicensedEducationalProgram");
        $this->transferComplexDataById($oldCS, "eiis_LicensedPrograms",array(
            "Id" => "Id",
            "Name" => "Name",
            "fk_eiisEduDirectory" => "EducationalProgramSublevelId",
            "Period" => "EduNormativePeriod",
            "fk_eiisLicenseSupplement" => "LicenseSupplementId",
            "fk_eiisEduProgram" => "EducationalProgramId",
            "OKSO" => "OksoCode",
            "Code" => "Code",
            "fk_eiisEduProgramType" => "EducationalProgramTypeId",
            "QualificationGrade" => "QualificationGrade",
            "fk_isgaQualification" => "QualificationId"
        ));
    }

    private function Licenses(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("License");
        $this->transferComplexDataById($oldCS, "eiis_Licenses",array(
            "Id" => "Id",
            "fk_eiisRegion" => "RegionId",
            "LicenseRegNum" => "RegNumber",
            "SerDoc" => "SerialNumber",
            "NumDoc" => "FormNumber",
            "fk_eiisLicenseState" => "LicenseStatusId",
            "IsTermless" => "IsTermless",
            "DateEnd" => "EndDate",
            "fk_eiisControlOrgan" => "ControlOrganId",
            "fk_eiisEducationalOrganization" => "EducationalOrganizationId",
            "DateReg" => "IssueDate"
        ));
    }

    private function Regions(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Region");
        $this->transferComplexDataById($oldCS, "eiis_Regions",array(
            "Id" => "Id",
	        "Name" => "Name",
            "fk_eiisFedOkr" => "FederalDistrictId",
            "Code" => "Code"
        ));
    }

    private function AccreditationKinds(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("AccreditationKind");
        $this->transferComplexDataById($oldCS, "isga_AccreditationKinds",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code"
        ));
    }


    private function AccreditedPrograms(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("CertificateSupplementEducationalProgram");
        $this->transferComplexDataById($oldCS, "isga_AccreditedPrograms",array(
            "Id" => "Id",
            "DynamicColumns" => "DynamicColumns",
	        "fk_eiisLicensedProgram" => "LicensedEducationalProgramId",
	        "fk_isgaCertificateSupplement" => "CertificateSupplementId"
        ));
    }

    private function ApplicationReasons(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormReason");
        $this->transferComplexDataById($oldCS, "isga_ApplicationReasons",array(
            "Id" => "Id",
            "fk_isgaApplicationType" => "FormTypeId",
	        "Code" => "Code",
	        "Name" => "Name",
	        "Ordering" => "Ordering",
	        "ShortName" => "ShortName",
	        "NamePrepositionalCase" => "NamePrepositionalCase",
	        "ShowCollege" => "ShowCollege",
	        "AllowSendToRaa" => "AllowSendToRaa",
	        "ShowPreviousCertificate" => "ShowPreviousCertificate",
	        "GroupCode" => "GroupCode"
        ));
    }

    private function ApplicationStatuses(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormStatus");
        $this->transferComplexDataById($oldCS, "isga_ApplicationStatuses",array(
            "Id" => "Id",
	        "Code" => "Code",
	        "Name" => "Name",
	        "PublishName" => "PublishName",
	        "Description" => "Description",
	        "Published" => "Published",
            "ShowCollege" => "ShowCollege",
            "ShowCertificate" => "ShowCertificate",
            "AllowSelectCollege" => "AllowSelectCollege",
            "AllowEditFields" => "AllowEditFields",
            "AllowEditFormMembers" => "AllowEditFormMembers",
            "AllowEditDocuments" => "AllowEditDocuments",
            "AllowEditAccreditedPrograms" => "AllowEditAccreditedPrograms",
            "AllowDelete" => "AllowDelete",
            "AllowReverseTransitions" => "AllowReverseTransitions",
            "IsgaAllowPublication" => "IsgaAllowPublication",
            "IsgaTextForPublication" => "IsgaTextForPublication"
        ));
    }

    private function ApplicationTypes(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormType");
        $this->transferComplexDataById($oldCS, "isga_ApplicationTypes",array(
             "Id" => "Id",
             "Code" => "Code",
             "Name" => "Name",
             "ShortName" => "ShortName",
             "NameAccusativeCase" => "NameAccusativeCase",
             "NameGenitiveCase" => "NameGenitiveCase"
        ));
    }

    private function Applications(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Form");
        $this->transferComplexDataById($oldCS, "isga_Applications",array(
             "Id" => "Id",
             "RegNumber" => "RegNumber",
             "DateSubmission" => "SubmissionDate",
             "DateReg" => "RegDate",
             "fk_isgaApplicationStatus" => "FormStatusId",
             "fk_isgaPreviousCertificate" => "PrevCertificateId",
             "ApplicantPersonFullName" => "DeclarerPersonName",
             "AcceptanceTransferActNumber" => "AcceptanceTransferActNumber",
             "DateAcceptanceTransferAct" => "AcceptanceTransferActDate",
             "RecallApplicationRegNumber" => "RecallApplicationRegNumber",
             "RecallApplicationScanFileName" => "RecallApplicationScanFileName",
             "DateRecall" => "RecallDate",
             "DateRecallDocumentsSend" => "RecallDocumentsSendDate",
             "fk_isgaApplicationType" => "FormTypeId",
             "fk_eiisLicense" => "LicenseId",
        ));
    }

    private function CertificateStatuses(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("CertificateStatus");
        $this->transferComplexDataById($oldCS, "isga_CertificateStatuses",array(
            "Id" => "Id",
            "Code" => "Code",
	        "Name" => "Name"
        ));
    }

    private function CertificateSupplements(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("CertificateSupplement");
        $this->transferComplexDataById($oldCS, "isga_CertificateSupplements",array(
             "Id" => "Id",
             "fk_isgaCertificate" => "CertificateId",
	         "fk_isgaCertificateSupplementStatus" => "CertificateSupplementStatusId",
             "DateIssue" => "IssueDate",
             "FormNumber" => "BlankNumber",
             "SerialNumber" => "SerialNumber",
             "Note" => "Note",
             "fk_isgaAccreditationKind" => "AccreditationKindId",
             //"SupplementFile" => "CertificateSupplementDraftFile",
             "SupplementFileName" => "CertificateSupplementDraftFileName",
	         "DateIssueToApplicant" => "IssueToApplicantDate",
	         "DateCheck" => "CheckDate",
	         "DatePrint" => "PrintDate",
	         "fk_isgaCheckStatus" => "CheckStatusId",
	         "DateReadyToIssue" => "ReadyToIssueDate",
	         "fk_isgaOrderDocument" => "OrderDocumentId",
        ));
    }

    private function CertificateTypes(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("CertificateType");
        $this->transferComplexDataById($oldCS, "isga_CertificateTypes",array(
            "Id" => "Id",
	        "Code" => "Code",
	        "Name" => "Name"
        ));
    }

    private function Certificates(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Certificate");
        $this->transferComplexDataById($oldCS, "isga_Certificates",array(
            "Id" => "Id",
            "RegNumber" => "RegNumber",
            "DateIssue" => "IssueDate",
            "DateEnd" => "EndDate",
            "FormNumber" => "BlankNumber",
            "SerialNumber" => "SerialNumber",
            "fk_eiisControlOrgan" => "ControlOrganId",
            "fk_oisgaApplication" => "FormId",
            "fk_eiisEducationalOrganization" => "legacy_EducationalOrganizationId",
            "fk_isgaCertificateStatus" => "CertificateStatusId",
            "fk_isgaCertificateType" => "CertificateTypeId",

            "Note" => "Note",
            "CertificateFile" => "CertificateDraftFile",
            "CertificateFileName" => "CertificateDraftFileName",
            "DateIssueToApplicant" => "IssueToApplicantDate",
            "DateCheck" => "CheckDate",
            "DateReadyToIssue" => "ReadyToIssueDate",
            "DatePrint" => "PrintDate",
            "fk_isgaCheckStatus" => "CheckStatusId",
            "fk_isgaOrderDocument" => "OrderDocumentId",
        ));
    }

    private function CheckStatuses(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("AccredDocumentCheckStatus");
        $this->transferComplexDataById($oldCS, "isga_CheckStatuses",array(
            "Id" => "Id",
            "Code" => "Code",
            "Name" => "Name"
        ));
    }

    private function CollegeStatuses(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("CollegeStatus");
        $this->transferComplexDataById($oldCS, "isga_CollegeStatuses",array(
            "Id" => "Id",
            "Name" => "Name"
        ));
    }

    private function Colleges(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("College");
        $this->transferComplexDataById($oldCS, "isga_Colleges",array(
            "Id" => "Id",
            "Number" => "Number",
	        "DateCollege" => "CollegeDate",
	        "Address" => "Address",
            "DateProtocol" => "ProtocolDate",
	        "fk_isgaCollegeStatus" => "CollegeStatusId",
            "XmlPackageFileName" => "XmlPackageFileName"
        ));
    }

    private function EduStandards(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalProgramStandard");
        $this->transferComplexDataById($oldCS, "isga_EduStandards",array(
            "Id" => "Id",
            "Code" => "Code",
            "Name" => "Name",
            "Abbreviation" => "Abbreviation"
        ));
    }

    private function EnlargedGroupSpecialities(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("SpecialityGroup");
        $this->transferComplexDataById($oldCS, "isga_EnlargedGroupSpecialities",array(
            "Id" => "Id",
            "Code" => "Code",
            "Name" => "Name",
            "fk_oisgaEGSEdition" => "SpecialityGroupEditionId"
        ));
    }

    private function EnlargedGroupSpecialityEditions(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("SpecialityGroupEdition");
        $this->transferComplexDataById($oldCS, "isga_EnlargedGroupSpecialityEditions",array(
            "Id" => "Id",
            "Name" => "Name"
        ));
    }

    private function FounderTypes(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FounderType");
        $this->transferComplexDataById($oldCS, "isga_FounderTypes",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code"
        ));
    }

    private function Founders(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Founder");
        $this->transferComplexDataById($oldCS, "isga_Founders",array(
             "Id" => "Id",
             "fk_isgaFounderType" => "FounderTypeId",
             "OrganizationFullName" => "OrganizationFullName",
             "OrganizationShortName" => "OrganizationShortName",
             "LastName" => "LastName",
             "FirstName" => "FirstName",
             "Patronymic" => "Patronymic",
             "Phones" => "Phone",
             "Faxes" => "Fax",
             "Emails" => "Email",
             "Ogrn" => "Ogrn",
             "Inn" => "Inn",
             "Kpp" => "Kpp",
             "LawAddress" => "LAddress",
             "fk_eiisLawRegion" => "LAddressRegionId",
             "LawDistrict" => "LAddressDistrict",
             "LawTown" => "LAddressTown",
             "LawStreet" => "LAddressStreet",
             "LawHouseNumber" => "LAddressHouseNumber",
             "LawPostalCode" => "LAddressPostalCode",
             "PAddress" => "PAddress",
             "fk_eiisPRegion" => "PAddressRegionId",
             "PDistrict" => "PAddressDistrict",
             "PTown" => "PAddressTown",
             "PStreet" => "PAddressStreet",
             "PHouseNumber" => "PAddressHouseNumber",
             "PPostalCode" => "PAddressPostalCode",
        ));
    }

    private function GroupSpecialities(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("SpecialitySubgroup");
        $this->transferComplexDataById($oldCS, "isga_GroupSpecialities",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code",
            "fk_isgaEGS" => "SpecialityGroupId"
        ));
    }

    private function OrderDocumentKinds(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("OrderDocumentKind");
        $this->transferComplexDataById($oldCS, "isga_OrderDocumentKinds",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code",
            "fk_isgaOrderDocumentType" => "OrderDocumentTypeId"
        ));
    }

    private function OrderDocumentStatuses(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("OrderDocumentStatus");
        $this->transferComplexDataById($oldCS, "isga_OrderDocumentStatuses",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code"
        ));
    }

    private function OrderDocumentTypes(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("OrderDocumentType");
        $this->transferComplexDataById($oldCS, "isga_OrderDocumentTypes",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code"
        ));
    }

    private function OrderDocuments(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("OrderDocument");
        $this->transferComplexDataById($oldCS, "isga_OrderDocuments",array(
            "Id" => "Id",
            "fk_isgaOrderDocumentKind" => "OrderDocumentKindId",
	        "fk_isgaOrderDocumentStatus" => "OrderDocumentStatusId",
	        "Number" => "Number",
	        "DateSign" => "SignDate",
	        "fk_isgaCollege" => "CollegeId",
	        "AddressFileFtp" => "AddressFileFtp"
        ));
    }

    private function Qualifications(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Qualification");
        $this->transferComplexDataById($oldCS, "isga_Qualifications",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code"
        ));
    }

    private function FormDocumentGroupStatus(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormDocumentGroupStatus");
        $this->transferComplexDataById($oldCS, "isga_ApplicationDocumentGroupStatuses",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code",
            "Ordering" => "Ordering",
            "HasReasons" => "HasReasons"
        ));
    }

    private function FormDocumentGroup(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormDocumentGroup");
        $this->transferComplexDataById($oldCS, "isga_ApplicationDocumentGroups",array(
            "Id" => "Id",
            "fk_isgaApplication" => "FormId",
            "fk_isgaApplicationDocumentType" => "FormDocumentTypeId",
            "fk_ApplicationApplicant" => "FormApplicantId",
            "fk_ApplicationDocumentGroupStatus" => "FormDocumentGroupStatusId",
            "FtpDirectoryUrl" => "FtpDirectoryUrl"
        ));
    }

    private function FormApplicant(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormApplicant");
        $this->transferComplexDataById($oldCS, "isga_mtm_Application_Branches",array(
            "Id" => "Id",
            "fk_isgaApplication" => "FormId",
            "fk_eiisEOBranch" => "EducationalOrganizationId"
        ));
    }

    private function FormReason(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormFormReason");
        $this->transferComplexDataById($oldCS, "isga_mtm_Application_Reason",array(
            "Id" => "Id",
            "fk_isgaApplication" => "FormId",
            "fk_isgaApplicationReason" => "FormReasonId"
        ));
    }

    private function FormDocumentGroupGroupStatusReason(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormDocumentGroupGroupStatusReason");
        $this->transferComplexDataById($oldCS, "isga_ApplicationDocumentGroupGroupStatusReason",array(
            "Id" => "Id",
            "fk_isgaApplicationDocumentGroup" => "FormDocumentGroupId",
            "fk_isgaApplicationDocumentGroupStatus" => "FormDocumentGroupStatusId",
            "fk_isgaApplicationDocumentGroupStatusReasons" => "FormDocumentGroupStatusReasonId",
            "Comment" => "Comment"
        ));
    }

    private function FormDocumentGroupStatusReason(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormDocumentGroupStatusReason");
        $this->transferComplexDataById($oldCS, "isga_ApplicationDocumentGroupStatusReason",array(
            "Id" => "Id",
            "Name" => "Name"
        ));
    }

    private function FormDocumentType(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormDocumentType");
        $this->transferComplexDataById($oldCS, "isga_ApplicationDocumentTypes",array(
            "Id" => "Id",
            "Name" => "Name",
            "Code" => "Code",
            "IsForApplicants" => "IsForApplicants",
            "IsForFilialsOnly" => "IsForFilialsOnly",
            "Ordering" => "Ordering",
            "FtpDirectoryName" => "FtpDirectoryName",
            "ForEachApplicant" => "ForEachApplicant",
            "Readonly" => "Readonly",
            "Checking" => "Checking",
            "AutoPublishing" => "AutoPublishing"
        ));
    }

    private function Notification(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Notification");
        $this->transferComplexDataById($oldCS, "isga_Notification",array(
            "Id" => "Id",
            "fk_isgaApplication" => "FormId",
            "fk_isgaNotificationTemplate" => "NotificationTemplateId",
            "Number" =>  "Number",
            "DateCreation" => "CreationDate",
            "DateSigning" => "SigningDate",
            "ApplicationRegNumber" => "FormRegNumber",
            "RejectionReason" => "RejectionReason",
            "AddresseeName" => "AddresseeName",
            "AddresseePost" => "AddresseePost",
            "DestinationAddress" => "DestinationAddress",
            "AddresseeEMail" => "AddresseeEMail",
            "SendingType" => "SendingType",
            "DateSending" => "SendingDate",
            "SendingComment" => "SendingComment",
            "DateReturn" => "ReturnDate",
            "ReturnComment" => "ReturnComment",
            "DateDelivery" => "DeliveryDate",
            "AnswerLetterNumber" => "AnswerLetterNumber",
            "AnswerComment" => "AnswerComment",
            "DateAnswerReceipt" => "AnswerReceiptDate",
            "AuthorizedUserComment" => "AuthorizedUserComment",
            "DateNotification" => "NotificationDate",
            "MissingDocuments" => "MissingDocuments",
            "NonconformityNotificationNumber" => "NonconformityNotificationNumber",
            "DateNonconformityNotification" => "NonconformityNotificationDate",
            "DateNonconformityNotificationDelivery" => "NonconformityNotificationDeliveryDate",
            "fk_isgaRefusalType" => "RefusalTypeId"
        ));
    }

    private function NotificationTemplate()
    {
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("NotificationTemplate");
        $this->transferComplexDataById($oldCS, "isga_NotificationTemplates", array(
            "Id" => "Id",
            "fk_ApplicationStatus" => "FormStatusId",
            "FileName" => "FileName",
            "FileData" => "FileData",
            "DatetimeUploadTime" => "UploadTime"
        ));
    }

    private function mtm_Founders_EducationalOrganizations(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("EducationalOrganizationFounder");
        $this->transferComplexData($oldCS, "isga_mtm_Founders_EducationalOrganizations",array(
            "fk_eiisEducationalOrganization" => "EducationalOrganizationId",
            "fk_isgaFounder" => "FounderId"
        ));
    }

    private function scriptCertificates(){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        //Скрипт, что заполняет бд дополнительными данными

        //заполняем связь с обр учреждением для всех сертификатов

        //проходим по всем обр учреждениям
        $table = new Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");
        $orgs = $table->getAllEntities();
        $num = 0;
        $norgs = 0;
        foreach($orgs as $org){
            echo "Орг ".++$norgs."<br>";

            //Получаем список сертификатов, несколько извращенным способом

            $eoTable = new \Model\Gateway\EntitiesTable("isga_Certificates");
            $tg = $eoTable->getGateway();

            //Строим запрос на получение всех связанных филиалов

            //Подзапрос 1, Лицензии
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
            $subselectLicenses = $sql->select();

            $subselectLicenses->from('eiis_Licenses')
                ->columns(array('Id'))
                ->where(array('fk_eiisEducationalOrganization' => $org->getField("Id")));

            //Подзапрос 2, Заявки
            $sql2 = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
            $subselectApps = $sql2->select();

            $subselectApps->from('isga_Applications')
                ->columns(array('Id'))
                ->where->in('fk_eiisLicense', $subselectLicenses);

            //Запрос
            $resultSet = $tg->select(function(\Zend\Db\Sql\Select $select) use ($subselectApps, $tg){
                $select
                    ->where->in('fk_oisgaApplication', $subselectApps);

                //$sql = $tg->getSql();
                //print_r($sql->getSqlstringForSqlObject($select));die;
            });

            //А теперь проходим по этому списку сертификатов и заполням айдишниками
            foreach($resultSet as $cert){
                var_dump($cert->getField("fk_eiisEducationalOrganization"));

                if($cert->getField("fk_eiisEducationalOrganization") == "" || $cert->getField("fk_eiisEducationalOrganization") == null){
                    $cert->setField("fk_eiisEducationalOrganization", $org->getField("Id"));
                    $cert->save();
                    $num++;
                }
            }
            echo $num."<br>";
        }
        echo "done";
        die;
    }

    private function scriptApps(){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        //Скрипт, что заполняет бд дополнительными данными

        //заполняем связь с обр учреждением для всех заявлений

        //проходим по всем обр учреждениям
        $table = new Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");
        $orgs = $table->getAllEntities();
        $num = 0;
        $norgs = 0;
        foreach($orgs as $org){
            echo "Орг ".++$norgs."<br>";

            //Получаем список заявлений
            $eoTable = new \Model\Gateway\EntitiesTable("isga_Applications");
            $tg = $eoTable->getGateway();

            //Строим запрос на получение всех связанных филиалов

            //Подзапрос 1, Лицензии
            $sql = new \Zend\Db\Sql\Sql(\Model\Gateway\DbAdapter::getInstance());
            $subselectLicenses = $sql->select();

            $subselectLicenses->from('eiis_Licenses')
                ->columns(array('Id'))
                ->where(array('fk_eiisEducationalOrganization' => $org->getField("Id")));


            //Запрос
            $resultSet = $tg->select(function(\Zend\Db\Sql\Select $select) use ($subselectLicenses, $tg){
                $select
                    ->where->in('fk_eiisLicense', $subselectLicenses);

                //$sql = $tg->getSql();
                //print_r($sql->getSqlstringForSqlObject($select));die;
            });

            //А теперь проходим по этому списку сертификатов и заполням айдишниками
            foreach($resultSet as $app){
                var_dump($app->getField("fk_eiisEducationalOrganization"));

                if($app->getField("fk_eiisEducationalOrganization") == "" || $app->getField("fk_eiisEducationalOrganization") == null){
                    $app->setField("fk_eiisEducationalOrganization", $org->getField("Id"));
                    $app->save();
                    $num++;
                }
            }
            echo $num."<br>";
        }
        echo "done";
        die;
    }

    private function scriptAccreditedPrograms(){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        //Скрипт, что заполняет бд дополнительными данными

        //проходим по всем аккр програмам
        $table = new Model\Gateway\EntitiesTable("isga_AccreditedPrograms");
        $progs = $table->getAllEntities(array(
            "page" => 1,
            "onPage" => 10
        ));
        $num = 0;
        foreach($progs as $prog){
            if($num==10)
                break;
            //Если поле задано, то пропускаем
            if($prog->getField("fk_eiisLicensedProgram") != "" && $prog->getField("fk_eiisLicensedProgram") != null)
                continue;

            echo "<br>=====================================<br>";
            var_dump($prog->getProgramData());
            //получаем связанный isga_CertificateSupplements
            //$certSupp = $prog->getCertificateSupplement();
            $num++;
        }
    }

    /**
     * Скрипт заполняет поле со связью с Лицензиями для таблицы с заявлениями
     */
    private function scriptFillApplicationLicenseLink($page){
        //Проходим по всем заявлениям

        //Пока найдем для определенного заявления
        //$app = new Model\Entities\isgaApplications("04b66245-22b6-4c03-82c7-fa9711280810");
        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $table = new Model\Gateway\EntitiesTable("isga_Applications");
        $apps = $table->getAllEntities(array(
            "onPage" => 250,
            "page" => $page
        ));

        $num = 0;
        $napps = 0;

        foreach($apps as $app){
            if($app->getField("fk_eiisLicense") != "" && isset($this->fields["fk_eiisLicense"])){
                continue;
            }

            $napps++;

            $cert = $app->getCertificate();
            if($cert == null)
                continue;
            $suppCerts = $cert->getSupplements()->getObjectsArray();
            if(count($suppCerts) > 0){
                $suppCert = $suppCerts[0];
                $accrPrograms = $suppCert->getAccreditedPrograms()->getObjectsArray();
                if(count($accrPrograms) > 0){
                    $accrProgram = $accrPrograms[0];
                    $licProgram = $accrProgram->getLicensedProgram();
                    if($licProgram == null)
                        continue;

                    $licSupp = $licProgram->getLicenseSupplement();
                    $lic = $licSupp->getLicense();
                    $licenseId = $lic->getField("Id");

                    if(isset($licenseId) && $licenseId != "" && strtolower($licenseId) != "null"){
                        $app->setField("fk_eiisLicense", $licenseId);
                        $app->save();
                    }
                }
            }

        }
        echo "page".$page." - ".$napps." ";
        echo "done";
    }

    /**
     * Скрипт заполняет поле со связью с Лицензиями со старой бд
     */
    private function scriptSetApplicationLicenseLinkFromOldBase($page){
        //$oldCS = new Model\Gateway\SqlSrvEntitiesTable("isga_Applications");
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Form");

        $n = 0;
        $oldApps = $oldCS->getAllEntities(
            /*array(
            "onPage" => 500,
            "page" => $page
        )*/);

        foreach($oldApps as $oldApp){
            $n++;
            $oldAppId = $oldApp->getField("Id");
            $newApp = new Model\Entities\isgaApplications($oldAppId);
            if($newApp != null && $newApp->getField("Id") != null){
                $newApp->setField("fk_eiisLicense", $newApp->stripIdDashes($oldApp->getField("LicenseId")));
                $newApp->save();
            }
        }
        echo $n;
        echo " done";
    }

    /**
     * Метод переносит таблицу со связями между организациями и учредителями в новую бд
     */
    private function transferMtmFoundersEOFromOldBase(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("isga_mtm_founders_educationalorganizations");
        $oldMtms = $oldCS->getAllEntities();

        $newCS = new Model\Gateway\EntitiesTable("isga_mtm_Founders_EducationalOrganizations");


        $n = 0;
        foreach($oldMtms as $oldMtm){
            $n++;
            $newRows = $newCS->getAllEntities(null,null,array(
                "fk_eiisEducationalOrganization" => $oldMtm->getField("fk_eiisEducationalOrganization"),
                "fk_isgaFounder" => $oldMtm->getField("fk_isgaFounder")
            ));

            $newRows = $newRows->getObjectsArray();
            if(count($newRows) > 0)
                continue;

            $newMtm = new Model\Entities\isgamtmFoundersEducationalOrganizations();
            $newMtm->setField("fk_eiisEducationalOrganization", $newMtm->stripIdDashes($oldMtm->getField("fk_eiisEducationalOrganization")));
            $newMtm->setField("fk_isgaFounder", $oldMtm->getField("fk_isgaFounder"));
            $newMtm->save();
        }

        echo $n;
        echo " done";
    }

    /**
     * Метод заполняет связь с группами специальносетй для аккредитованных программ
     */
    private function scriptSetGSLinkForAccrPrograms(){
        //проходим по всем аккр програмам
        $table = new Model\Gateway\EntitiesTable("isga_AccreditedPrograms");
        $progs = $table->getAllEntities();

        $eduProgramsTable = new Model\Gateway\EntitiesTable("eiis_EduPrograms");

        $num = 0;
        $n = 0;

        foreach($progs as $prog){
            $n++;
            if(!$prog->isEmptyField("fk_isgaGroupSpeciality") || !$prog->isEmptyField("isga_EnlargedGroupSpeciality"))
                continue;

            if($prog->isEmptyField("Code"))
                continue;

            $code = $prog->getField("Code");

            $ep = $eduProgramsTable->getAllEntities(null,null,array(
               "Code" => $code
            ));

            $ep = $ep->getObjectsArray();


            if(count($ep) >0){
                $prog->setField("fk_isgaEnlargedGroupSpeciality", $ep[0]->getField("fk_eiisUgs"));
                if($prog->isEmptyField("fk_eiisEduProgram")){
                    $prog->setField("fk_eiisEduProgram", $ep[0]->getField("Id"));
                }

                $prog->save();
                $num++;
            }
        }
        echo $n." - ".$num;
        echo " done";
    }

    private function scriptSetOrderDocumentsForApps(){
        //Таблица заявлений
        $table = new Model\Gateway\EntitiesTable("isga_Applications");
        $apps = $table->getAllEntities();

        $num = 0;
        $n = 0;
        foreach($apps as $app){
            $n++;

            if($app->isEmptyField("fk_isgaOrderDocument")){
                $oldCS = new Model\Gateway\SqlSrvEntitiesTable("FormOrderDocument");
                $oldApps = $oldCS->getEntitiesByWhere(array(
                    "FormId" => strtoupper($app->getId())
                ))->getObjectsArray();

                //var_dump($oldApps[0]->getFields());//[0]->getField("OrderDocumentId");
                if(count($oldApps) > 0){
                    $app->setField("fk_isgaOrderDocument",$oldApps[0]->getField("OrderDocumentid"));
                    $app->save(false, true);
                    $num++;
                }
            }
            //die;

        }
        echo $n." - ".$num;
        echo " done";
    }

    private function scriptAddNewApps(){
        $oldCS = new Model\Gateway\SqlSrvEntitiesTable("Form");
        $oldApps = $oldCS->getAllEntities();

        $n = 0;
        foreach($oldApps as $oldApp){

            $newApp = new \Model\Entities\isgaApplications($oldApp->getField("Id"));
            if($newApp->getField("Id") == null){
                $app2 = new \Model\Entities\isgaApplications2();

                $transferArray = array(
                    "Id" => "Id",
                    "DateSubmission" => "SubmissionDate",
                    "DateReg" => "RegDate",
                    "fk_isgaApplicationStatus" => "FormStatusId",
                    "ApplicantPersonFullName" => "DeclarerPersonName",
                    "ApplicantPersonFirstName" => "DeclarerPersonFirstName",
                    "ApplicantPersonPatronymic" => "DeclarerPersonPatronymic",
                    "fk_eiisLicense" => "LicenseId",
                    "RegNumber" => "RegNumber",
                    "fk_isgaPreviousCertificate" => "PrevCertificateId",
                    "AcceptanceTransferActNumber" => "AcceptanceTransferActNumber",
                    "DateAcceptanceTransferAct" => "AcceptanceTransferActDate",
                    "RecallApplicationRegNumber" => "RecallApplicationRegNumber",
                    "RecallApplicationScanFileName" => "RecallApplicationScanFileName",
                    "DateRecall" => "RecallDate",
                    "DateRecallDocumentsSend" => "RecallDocumentsSendDate"
                );

                $oldFields = $oldApp->getFields();
                $newFields = array();
                foreach($transferArray as $newKey => $oldKey){
                    $newFields[$newKey] = $oldFields[$oldKey];
                }
                $app2->setFields($newFields);
                $app2->save(true);
                $n++;
            }
        }

        echo $n;
        echo " done";
    }

    public function scriptAction(){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        //$this->OrderDocuments();
        //$this->scriptSetOrderDocumentsForApps();
        $this->scriptAddNewApps();
        die;
    }
}
