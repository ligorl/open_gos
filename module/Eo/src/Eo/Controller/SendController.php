<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

use Model\Gateway\EntitiesTable;

class SendController extends AbstractActionController
{
    private $logsHandlerUrl = 'http://sync.kartavuzov.ru/application/log/add-logs';
    private $sendToUrl = 'http://sync.kartavuzov.ru/application/synchronization/accept-data';

    protected $tables = array(
        'eiis_Licenses',
        'isga_Certificates',
        'eiis_LicenseStates',
        'isga_CertificateStatuses',
        'isga_CertificateTypes',
    );


    public function sendAction(){
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        set_time_limit(0);
        $start = microtime(true);
        $sender = new \Eo\Service\Sender();
        $logs = array();
        foreach($this->tables as $table){
            try{
                $response = $sender->sendTable($table, $this->sendToUrl);
            }catch(\Exception $e){
                $response = $e->getMessage();
            }
            \Zend\Debug\Debug::Dump($response);
            $this->handlerResponse($response, $logs);
        }
        if($logs){
            echo $this->sendLog($logs);
        }
        $time = microtime(true) - $start;
        printf('Скрипт работал %.4F сек.', $time);
        exit();
    }

    /**
     *
     * @param string $response
     */
    protected function handlerResponse($response, &$logs){
        if($this->isJson($response)){
            $result = json_decode($response);
            if($result->status !== 'ok'){
                $logs[] = $result->message;
            }
            return true;
        }
        $logs[] = $response;
        return true;
    }

    /**
     *
     * @param String $string
     * @return Boolean
     */
    protected function isJson($string){
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     *
     * @param array $logs
     * @return string
     */
    protected function sendLog(array $logs){
        $request = curl_init($this->logsHandlerUrl);
        curl_setopt( $request, CURLOPT_POSTFIELDS, array('messages' => json_encode($logs)));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($request);
        curl_close($request);
        return $result;
    }
}