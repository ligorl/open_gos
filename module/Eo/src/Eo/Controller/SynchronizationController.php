<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Eo\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

class SynchronizationController extends AbstractActionController {


    public function indexAction()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        set_time_limit(0);
        $start = microtime(true);
        
        $sync_class = new \Eo\Service\Synchronization();
        $dump_table = $sync_class->dump_table('eiis_Licenses');
        print $dump_table."<br>";
        $time = microtime(true) - $start;
        printf('Скрипт работал %.4F сек.', $time);
        
        die;
    }
    
   

}


