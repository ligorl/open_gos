<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(

            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'eo' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/eo',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Eo\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]][/:id][/:id2][/:id3][/:id4]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action'        => 'index',
                            ),
                        ),
                    ),
                    'send' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/send[/:action][/:id]',
                            'defaults' => array(
                                'controller' => 'Eo\Controller\Send',
                                'action' => 'index'
                            )
                        )
                    ),
                    'licenseExcerpt' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/index/license-excerpt[/:id]',
                            'defaults' => array(
                                'controller' => 'Eo\Controller\Index',
                                'action' => 'licenseExcerpt'
                            )
                        )
                    ),
                     'certificateExcerpt' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/index/certificate-excerpt[/:id]',
                            'defaults' => array(
                                'controller' => 'Eo\Controller\Index',
                                'action' => 'certificateExcerpt'
                            )
                        )
                    ),
                    'licenseExcerptNoProgram' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/index/license-excerpt-no-program[/:id]',
                            'defaults' => array(
                                'controller' => 'Eo\Controller\Index',
                                'action' => 'licenseExcerptNoProgram'
                            )
                        )
                    ),
                     'certificateExcerptNoProgram' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/index/certificate-excerpt-no-program[/:id]',
                            'defaults' => array(
                                'controller' => 'Eo\Controller\Index',
                                'action' => 'certificateExcerptNoProgram'
                            )
                        )
                    ),
                ),
            ),
        ),
    ),

    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'updateLicenses' => array(
                    'options' => array(
                        'route'    => 'updateLicenses',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateLicenses'
                        )
                    )
                ),
                'updateSchools' => array(
                    'options' => array(
                        'route'    => 'updateSchools',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateSchools'
                        )
                    )
                ),
                'updateCertificates' => array(
                    'options' => array(
                        'route'    => 'updateCertificates',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateCertificates'
                        )
                    )
                ),
                'updateAccrPrograms' => array(
                    'options' => array(
                        'route'    => 'updateAccrPrograms',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateAccrPrograms'
                        )
                    )
                ),
                'updateEduPrograms' => array(
                    'options' => array(
                        'route'    => 'updateEduPrograms',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateEduPrograms'
                        )
                    )
                ),
                'updateUGS' => array(
                    'options' => array(
                        'route'    => 'updateUGS',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateUGS'
                        )
                    )
                ),
                'updateLicenseSupplements' => array(
                    'options' => array(
                        'route'    => 'updateLicenseSupplements',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateLicenseSupplements'
                        )
                    )
                ),
                'updateCertificateSupplements' => array(
                    'options' => array(
                        'route'    => 'updateCertificateSupplements',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateCertificateSupplements'
                        )
                    )
                ),
                'updateLicPrograms' => array(
                    'options' => array(
                        'route'    => 'updateLicPrograms',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateLicPrograms'
                        )
                    )
                ),
                'updateApplications' => array(
                    'options' => array(
                        'route'    => 'updateApplications',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'updateApplications'
                        )
                    )
                ),
                'serviceCreatePackage' => array(
                    'options' => array(
                        'route'    => 'serviceCreatePackage <packageId>',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'serviceCreatePackage'
                        )
                    )
                ),
                'threadWork' => array(
                    'options' => array(
                        'route'    => 'threadWork',
                        'defaults' => array(
                            'controller' => 'Eo\Controller\Console',
                            'action' => 'threadWork'
                        )
                    )
                )
            )
        )
    ),

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Eo\Controller\Index' => 'Eo\Controller\IndexController',
            'Eo\Controller\Declaration' => 'Eo\Controller\DeclarationController',
            'Eo\Controller\Sign' => 'Eo\Controller\SignController',
            'Eo\Controller\Synchronization' => 'Eo\Controller\SynchronizationController',
            'Eo\Controller\Db' => 'Eo\Controller\DbController',
            'Eo\Controller\Console' => 'Eo\Controller\ConsoleController',
            'Eo\Controller\Send' => 'Eo\Controller\SendController',
            'Eo\Controller\License' => 'Eo\Controller\LicenseController',
            'Eo\Controller\Requests' => 'Eo\Controller\RequestsController',
            'Eo\Controller\Affairs' => 'Eo\Controller\AffairsController',
            
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/eo'           => __DIR__ . '/../view/layout/layout.phtml',

            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
        'invokables'=> array(
            'pages' => 'Eo\View\Helper\Pages',
        )
    ),
);
