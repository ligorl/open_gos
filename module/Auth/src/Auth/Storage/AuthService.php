<?php
//module/SanAuth/src/SanAuth/Model/MyAuthStorage.php
namespace Auth\Storage;

use Zend\Authentication;

class AuthService extends Authentication\AuthenticationService
{
    private $user = null;

    public function getIdentity()
    {
        $storage = $this->getStorage();

        if ($storage->isEmpty()) {
            return null;
        }

        $userId = $storage->read();

        if(is_object($userId) && !is_string($userId)){
            $logger = new \Model\Logger\Logger("obj");
            $logger->Log(print_r(debug_backtrace(),true));
            return $userId;
        }

        $user = new \Model\Entities\isgaUsers($userId);
        return $user;
    }
}