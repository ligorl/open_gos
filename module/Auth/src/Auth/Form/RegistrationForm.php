<?php
namespace Auth\Form;

use Zend\Form\Form;

class RegistrationForm extends Form
{
    private function prepareFilter(){
        $formFilter = new \Zend\InputFilter\InputFilter();
        $formFilter->add(array(
            'name'     => 'name',
            'required'   => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'=>'NotEmpty',
                    "break_chain_on_failure"=>true,
                    'options'=>array(
                        'message'=>'Поле не может быть пустым',
                    )
                ),
                array(
                    'name'    => 'StringLength',
                    "break_chain_on_failure"=>true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 100,
                        'message'=>'Имя не может быть короче 2 знаков',
                    ),
                ),

            ),
        ));
        $formFilter->add(array(
            'name'     => 'inn',
            'required'   => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'=>'NotEmpty',
                    "break_chain_on_failure"=>true,
                    'options'=>array(
                        'message'=>'Поле не может быть пустым',
                    )
                )
            ),
        ));
        $formFilter->add(array(
            'name'       => 'email',
            'required'   => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'=>'NotEmpty',
                    "break_chain_on_failure"=>true,
                    'options'=>array(
                        'message'=>'Поле не может быть пустым',
                    )
                ),
                array(
                    'name'    => 'StringLength',
                    "break_chain_on_failure"=>true,
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 4,
                        'max'      => 100,
                        'message'=>'Email не может быть короче 4 знаков',

                    ),

                ),
                array('name'=>'EmailAddress','options'=>array('message'=>'Не верный формат Email'))
            ),
        ));
        $this->setInputFilter($formFilter);
    }

    private function prepareFields(){
        parent::__construct('registration');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Полное наименование ОО',
            ),
        ));

        $this->add(array(
            'name' => 'inn',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'ИНН',
            ),
        ));

        $model = new \Model\Gateway\EntitiesTable('isga_EducationalOrganizationSpecifics');
        $specs = $model->getAllEntities();
        $arr = array();
        foreach($specs as $spec){
            $arr[$spec->getField('Id')] = $spec->getField('Name');
        }

        $this->add(array(
            'name' => 'spec',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
                'label' => 'Специфика организации',
                'value_options' => $arr,
             )
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'E-mail',
            ),
        ));

        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Контактный телефон',
            ),
        ));

        $this->add(array(
            'name' => 'fio',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'ФИО контактного лица',
            ),
        ));

        $this->add(array(
            'name' => 'file',
            'type' => 'Zend\Form\Element\File',
            'attributes' => array(
            ),
            'options' => array(
                'label' => 'Копия заявления (не на гос. аккредитацию)',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Зарегистрироваться',
                'id' => 'submitbutton',
                'class' => 'btn btn-success'
            ),
        ));

    }

    public function __construct($name = null)
    {
        $this->prepareFields();
        $this->prepareFilter();
    }
}