<?php
namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

//use Model\Entities\;


// for the form
use Zend\Form\Element;

use Auth\Form\RegistrationForm;


class RegistrationController extends AbstractActionController
{
    public function indexAction()
    {
        $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');

        $model = new \Model\Gateway\EntitiesTable('isga_EducationalOrganizationSpecifics');
        $specs = $model->getAllEntities();

        return new ViewModel(array(
            "specs" => $specs,
        ));
    }
    public function saveAction()
    {
        $status = 'ok';
        $bool = true;
        $error = array();
        $allowed = array("gif", "jpg", "png", "jpeg", "pdf");
        //Получение данных и их проверка
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $_POST;
            if(empty($data['name']) || trim($data['name']) == ''){
                $bool = false;
                $error['name'] = 'Укажите полное наименование ОО';
            }
            if(empty($data['inn']) || trim($data['inn']) == ''){
                $bool = false;
                $error['inn'] = 'Укажите ИНН';
            }
            if(empty($data['email']) || trim($data['email']) == ''){
                $bool = false;
                $error['email'] = 'Укажите E-mail';
            }
            if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
                $error['email'] = 'Укажите валидный E-mail';
                $bool = false;
            }
            if(empty($data['phone']) || trim($data['phone']) == ''){
                $bool = false;
                $error['phone'] = 'Укажите контактный телефон';
            }
            if(empty($data['fio']) || trim($data['fio']) == ''){
                $bool = false;
                $error['fio'] = 'Укажите ФИО';
            }


            if ( isset($_FILES['file']) && $_FILES['file']['error'] == 0 ) {
                #определяем расширение файла
                $ext = substr($_FILES['file']['name'], 1 + strrpos($_FILES['file']['name'], "."));
                if($_FILES["file"]["size"] > 1024*1024*2){
                    $error['file'] = "Файл превышает максимальный размер в 2Mb";
                    $bool = false;
                }elseif (!in_array(mb_strtolower($ext), $allowed)) {
                    $error['file'] = "Недопустимое расширение файла";
                    $bool = false;
                }else{
                    $ext = strrchr($_FILES['file']['name'],'.');
                    $name_info_file = $_FILES['file']['name'];
                    $name_info_file_hash = md5(time().$name_info_file).$ext;
                    move_uploaded_file( $_FILES['file']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$name_info_file_hash);
                    $data['file'] = $name_info_file_hash;
                }
            }else{
                $error['file'] = "Прикрепите копию заявления";
                $bool = false;
            }
            //проверка валидности формы
            if ($bool) {
                $model = new \Model\Entities\isgaUserRegRequest();
                $model->exchangeArray($data);
                $model->save(1);
            }else{
                $status = 'error';
            }
        }
        $res = "<script>";
        $res .= "var msg = new Object;";
        foreach($error as $key => $value){
            $res .= 'msg.'.$key.' = "'.$value.'";';
        }
        $res .= "var error = '$status';";
        $res .= 'window.parent.handleResponse(error,msg);';
        $res .= "</script>";

        echo $res;
        die;
    }
}