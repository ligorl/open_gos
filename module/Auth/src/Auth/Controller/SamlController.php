<?php
namespace Auth\Controller;

use Model\Entities\eiisEducationalOrganizations;
use Model\Entities\isgaUsers;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;


/*
define("TOOLKIT_PATH", __DIR__. '/../../../../../vendor/onelogin/php-saml/');
require_once(TOOLKIT_PATH . '_toolkit_loader.php');
require_once(TOOLKIT_PATH . 'settings.php');
require_once(TOOLKIT_PATH . 'advanced_settings.php');
*/
//extend the SSOLogin Library

//require_once(__DIR__. '/../../../../../vendor/sso/simplesamlphp/SSOLogin.php');
require_once (__DIR__. '/../../../../../vendor/esia/EsiaOmniAuth.php');
class SamlController extends AbstractActionController
{

    private $authservice = null;
    private $storage = null;

    private function getConfig(){
        if( \Eo\Model\Export::isTestServer() )
            return array(
                'site'            => "https://esia-portal1.test.gosuslugi.ru/",
                'logout_uri'  => "http://85.192.47.4/auth/saml/logout-service-redirect",
                'auth_uri'     => "http://85.192.47.4/auth/saml/assertion-consumer",
                'client_id'     => "OBRN10",
                'scope'         => "fullname email mobile usr_org",
                'cert_path'    => __DIR__. "/../../../../../vendor/esia/keys/isga.obrnadzor.gov.ru.crt",
                'pkey_path'  => __DIR__. "/../../../../../vendor/esia/keys/rootProstoyCA.key",
                'openssl_utility_path' => ''
            );
        else
            return array(
                'site'            => "https://esia-portal1.test.gosuslugi.ru/",
                'logout_uri'  => "https://isga.obrnadzor.gov.ru/auth/saml/logout-service-redirect",
                'auth_uri'     => "https://isga.obrnadzor.gov.ru/auth/saml/assertion-consumer",
                'client_id'     => "OBRN10",
                'scope'         => "fullname email mobile usr_org",
                'cert_path'    => __DIR__. "/../../../../../vendor/esia/keys/isga.obrnadzor.gov.ru.crt",
                'pkey_path'  => __DIR__. "/../../../../../vendor/esia/keys/rootProstoyCA.key",
                'openssl_utility_path' => ''
            );
    }

    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
            $this->authservice->setStorage(new \Zend\Authentication\Storage\Session('Auth'));
        }

        return $this->authservice;
    }

    public function esiaLoginAction(){
        $esia = new \EsiaOmniAuth($this->getConfig());
        $esia->create(); //redirected to esia login page
        die;
    }

    private function createUser($email, $username, $orgId){
        $user = new \Model\Entities\isgaUsers();
        $user->setField("Email",$email);
        $user->setField("Login",$email);
        $user->setField("Password",md5(time()));

        $user->setField("Name", $username);
        $user->setField("Role", "user");
        $user->setField("Source", "ЕСИА");
        $user->setField("WasLogin", 1);
        $user->setField("fk_eiisEducationalOrganization", $orgId);
        $user->save();
        $user = \Model\Entities\isgaUsers::getByEmail($email);

        return $user;
    }



    private function loginUser($user){
        //Если есть авторизируемся
        $authService = $this->getAuthService();

        //check authentication...
        $authService->getAdapter()
            ->setIdentity($user->getField("Login"))
            ->setCredential($user->getField("Password"));

        $authResult = $this->getAuthService()->authenticate();
        if ($authResult->isValid()) {
            ////$session = new Container('esia');
            //$session->getManager()->destroy();

            $resultRow = $this->getAuthService()->getAdapter()->getResultRowObject();

            $user = new \Model\Entities\isgaUsers();

            $user->setFields((array)$resultRow);
            $user->checkLogin();
            $authService->getStorage()->write($user->getId());

            $time = 60 * 10; // 10 минут
            if( \Eo\Model\Export::isTestServer() ){
                //если сервер тествый
                $time = 60  * 60 * 24 * 1; // 1 суток
            }

            $sessionManager = new \Zend\Session\SessionManager();
            $sessionManager->rememberMe($time);

            if ($user->getRole() == 'expert') {
                return $this->redirect()->toRoute('ron/default', array('controller' => 'index', 'action' => 'index'));
                exit;
            } else {
                return $this->redirect()->toRoute('eo/default', array('controller' => 'index', 'action' => 'index'));
                exit;
            }
        } else {
            return $this->redirect()->toRoute('auth');
        }
    }

    public function testAction(){

    }

    private function createUserRequest($params){
        $user = new \Model\Entities\isgaUserRegRequest();
        $user->setFields(array(
            "name" => $params["org_name"],
            "ogrn" => $params["org_ogrn"],
            "source" => "ЕСИА",
            "email"  => $params["usermail"],
            "fio"  => $params["username"],
            "file" => $params["file"]
        ));
        $user->save();
    }

    public function requestAction(){
        $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');
    }

    private function saveFile(){
        $bool = true;
        $error = "";
        $status = 'ok';
        $filename = null;

        $allowed = array("gif", "jpg", "png", "jpeg", "pdf");
        //Получение данных и их проверка
        if ( isset($_FILES['file']) && $_FILES['file']['error'] == 0 ) {
            #определяем расширение файла
            $ext = substr($_FILES['file']['name'], 1 + strrpos($_FILES['file']['name'], "."));
            if($_FILES["file"]["size"] > 1024*1024*2){
                $error = "Файл превышает максимальный размер в 2Mb";
                $bool = false;
            }elseif (!in_array(mb_strtolower($ext), $allowed)) {
                $error = "Недопустимое расширение файла";
                $bool = false;
            }else{
                $ext = strrchr($_FILES['file']['name'],'.');
                $name_info_file = $_FILES['file']['name'];
                $name_info_file_hash = md5(time().$name_info_file).$ext;
                move_uploaded_file( $_FILES['file']['tmp_name'],$_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$name_info_file_hash);
                $filename = $name_info_file_hash;
            }
        }else{
            $error = "Прикрепите копию заявления";
            $bool = false;
        }

        //проверка валидности формы
        if (!$bool)
            $status = 'error';

        $ret = array(
            "status"     => $status,
            "filename"   => $filename,
            "error"      => $error
        );
        return $ret;
    }

    public function assertionConsumerAction(){
        $session = new Container('esia');

        $fileError = null;
        if($this->params()->fromPost("isform")!=null){
            if(!isset($session->isAnswer) || !$session->isAnswer)
                return $this->redirect()->toRoute('auth');

            $params = $this->params()->fromPost();
            //TODO:
            //$params["org_type"] = "ourOrg";
            //$params["org_id"] = "5C36243EF6C0547A9171AC4A5EDB8C54";

            //Если была выбрана организация с имеющимcя юзером, то под ним и заходим
            if(isset($params["org_type"]) && $params["org_type"] == "ourOrg"){
                $ourOrgs = $session->view->orgsWithUser;
                $ourOrg = null;
                //Забираем орг из сессии, чтобы быть уверенным что эта орг точно из сессии
                foreach($ourOrgs as $org){
                    if($org->ourOrgId == $params["org_id"]){
                        $ourOrg = $org;
                        break;
                    }
                }

                //TODO:
                //$ourOrg = new \Model\Entities\eiisEducationalOrganizations("5C36243EF6C0547A9171AC4A5EDB8C54");

                if($ourOrg == null)
                    return $this->redirect()->toRoute('auth');

                $user = \Model\Entities\isgaUsers::getByOrgId($ourOrg->ourOrgId);
                if($user != null)
                    return $this->loginUser($user);
                else
                    return $this->redirect()->toRoute('auth');
            } else {
                $noOrgs = $session->view->noOrgs;
                $noOrg = null;
                //Забираем орг из сессии, чтобы быть уверенным что эта орг точно из сессии
                foreach($noOrgs as $org){
                    if($org->ogrn == $params["org_id"]){
                        $noOrg = $org;
                        break;
                    }
                }
                if($noOrg==null)
                    return $this->redirect()->toRoute('auth');

                $fileData = $this->saveFile();
                if($fileData["status"] == 'ok'){
                    $config = array(
                        "org_name" =>$noOrg->fullName,
                        "org_ogrn" =>$noOrg->ogrn,
                        "usermail" => $session->email,
                        "username" => $session->username,
                        "file"     => $fileData["filename"]
                    );
                    $this->createUserRequest($config);
                    return $this->redirect()->toRoute('auth/default', array('controller' => 'saml', 'action' => 'request'));
                } else {
                    $fileError = $fileData["error"];
                }
            }

        }

        if($this->params()->fromPost("isform")==null){
            //Приходит ответ от ЕИИС сервиса, разбираем

            error_reporting( E_ERROR );
            $esia = new \EsiaOmniAuth($this->getConfig());
            $token = $esia->get_token($_GET['code']);
            $info = $esia->get_info($token);

            $session->isAnswer = true;

            //$logger = new \Model\Logger\Logger("samluser");
            //$logger->Log(print_r($info,true));

    //Для локального теста
/*
            $info["user_contacts"] = '{"stateFacts":["hasSize"],"size":2,"eTag":"784F597BF9EACDC4BCFEAA7528D766E73CD26DE2","elements":[{"stateFacts":["Identifiable"],"eTag":"4A43D99ED128D272EC1E53C1A5DBCCA8314B6245","id":14249732,"type":"MBT","vrfStu":"VERIFIED","vrfValStu":"VERIFYING","value":"+7(000)0000004","verifyingValue":"+7(900)0000004","isCfmCodeExpired":true},{"stateFacts":["Identifiable"],"eTag":"59670135EB09F9A373D81579337DCE9708BBFB65","id":14216751,"type":"EML","vrfStu":"VERIFIED","value":"EsiaTest004@yandex.ru"}]}';
            $info["user_info"] = '{"stateFacts":["EntityRoot"],"eTag":"17076127BF95B3B2E447C176BCE4D9D6EAEFF5BC","firstName":"Имя004","lastName":"Фамилия004","middleName":"Отчество004","trusted":true,"updatedOn":1507033907,"status":"REGISTERED","verifying":false,"rIdDoc":3559,"containsUpCfmCode":false}';
            $info["user_orgs"] = '{"stateFacts":["hasSize"],"size":2,"elements":[{"stateFacts":["Identifiable"],"oid":1000298922,"fullName":"ОРГАНИЗАЦИЯ 1181280564","ogrn":"2000000000002","branchName":"Тестовый филиал для тестирования взаимодействия","chief":false},{"stateFacts":["Identifiable"],"oid":1000299543,"fullName":"Общий Тестовый ЮЛ (НЕ Обновлять)","ogrn":"1234567890123","chief":true}]}';
*/
            //Разбираем данные и сохраняем в сессию
            $userinfo = json_decode($info["user_info"]);
            $session->userinfo = $userinfo;

            $username = $userinfo->lastName. " ".$userinfo->firstName." ".$userinfo->middleName;
            $session->username = $username;

            $user_contacts = json_decode($info["user_contacts"]);
            $session->user_contacts = $user_contacts;

            $user_orgs = json_decode($info["user_orgs"]);
            $session->user_orgs = $user_orgs;

            //определяем емэйл
            $email=null;
            foreach( $user_contacts->elements as $contact){
                if($contact->type == "EML"){
                    $email = $contact->value;
                    break;
                }
            }
            $session->email = $email;
        } else {
            $email = $session->email;
            $user_contacts = $session->user_contacts;
            $username = $session->username;
            $userinfo = $session->userinfo;
            $user_orgs = $session->user_orgs;
        }

        //Смотрим, если пользователь не проверенный, редиректим
        if(!$userinfo->trusted){
            return $this->redirect()->toRoute('auth/default', array('controller'=>'saml', 'action'=>'notrust'));
            exit;
        }

        //Смотрим, если пользователь без email редиректим
        if($email == null || $email==""){
            return $this->redirect()->toRoute('auth/default', array('controller'=>'saml', 'action'=>'noemail'));
            exit;
        }



        //Если пользователь есть среди пользователей которые подали заявку, кидаем на страницу с заявкой
        $reqUsers = \Model\Entities\isgaUserRegRequest::getByEmail($email);
        if($reqUsers != null && $reqUsers->getField("status") != "done")
            return $this->redirect()->toRoute('auth/default', array('controller' => 'saml', 'action' => 'request'));

        //Проходим по списку, определяем есть ли эта организация у нас в бд
        $outOrgCount = 0;
        $lastOrgId = null;
        $orgsList = array();
        foreach($user_orgs->elements as $org){
            //Если эта организация филиал, пропускаем ее
            if(isset($org->branchName))
                continue;
            //Проверяем, есть ли организация в нашей бд
            $ourOrg = eiisEducationalOrganizations::geyByOGRN($org->ogrn);
            if($ourOrg!=null){
                $org->ourOrg = true;
                $org->ourOrgId = $ourOrg->getId();
                $orgsList[] = $org;
                //Проверяем, если для этой организации уже есть учетка
                $userOrg = \Model\Entities\isgaUsers::getByOrgId($ourOrg->getId());
                if($userOrg!=null){
                    $org->hasUser = true;
                    $lastOrgId = $ourOrg->getId();
                    $outOrgCount++;
                }
            }
        }

        //Если не пришло списка связанных организаций, отправляем на страничку
        if(count($orgsList)==0){
            return $this->redirect()->toRoute('auth/default', array('controller'=>'saml', 'action'=>'noorg'));
        }

        //Если есть организации c учеткой в нашей бд и таких организаций = 1, то авторизируемся под учеткой этой организации
        $user = null;
        if($outOrgCount==1){

            $user = isgaUsers::getByOrgId($lastOrgId);
        }


        if($user != null) {
            //var_dump($user->getFields());die;
            return $this->loginUser($user);
        }

        //Этот этап - это когда пользователя нет, и автоматом организацию ему привязать не смогли
        $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');
        $this->getServiceLocator()->get("viewhelpermanager")->get('headScript')->appendFile('/js/jquery.min.js');

        //Проходим по организациям и разделяем найденные у нас(с юзером) и не найденные
        $orgsWithUser = [];
        $noOrgs = [];

        foreach($user_orgs->elements as $org){
            if(isset($org->hasUser))
                $orgsWithUser[] = $org;
            else
                if(isset($org->ourOrg))
                    $noOrgs[] = $org;
        }
        $session->view = new \stdClass();
        $session->view->orgsWithUser = $orgsWithUser;
        $session->view->noOrgs = $noOrgs;

        return new ViewModel(array(
            'ourOrgs' => $orgsWithUser,
            'noOrgs' => $noOrgs,
            "fileError" => $fileError
        ));
    }

    public function noorgAction(){
        $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');
    }

    public function notrustAction(){
        $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');
    }

    public function noemailAction(){
        $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');
    }

    public function logoutAction(){
        $esia = new \EsiaOmniAuth($this->getConfig());
        $esia->request_logout();
        die;

    }

    public function logoutServiceRedirectAction(){
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        return $this->redirect()->toRoute('auth');
        die;
    }

    public function getSessionStorage()
    {
        if (! $this->storage) {
            $this->storage = $this->getServiceLocator()
                ->get('Auth\Storage\AuthStorage');
        }

        return $this->storage;
    }


}