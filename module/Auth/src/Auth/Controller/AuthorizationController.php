<?php
namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Storage\SessionStorage;
use Zend\View\Model\ViewModel;

use Model\Entities;

// for the form
use Zend\Form\Element;

use Auth\Form\LoginForm;
use Auth\Form\LoginFilter;

use Zend\Authentication\AuthenticationService;

require_once (__DIR__. '/../../../../../lib/esia/EsiaOmniAuth.php');
class AuthorizationController extends AbstractActionController
{
    protected $storage;
    protected $authservice;
    protected $form;

    private function getConfig(){
        if( \Eo\Model\Export::isTestServer() )
            return array(
                'site'            => "https://esia-portal1.test.gosuslugi.ru/",
                'logout_uri'  => "http://85.192.47.4/auth/saml/logout-service-redirect",
                'auth_uri'     => "http://85.192.47.4/auth/saml/assertion-consumer",
                'client_id'     => "OBRN10",
                'scope'         => "fullname email mobile usr_org",
                'cert_path'    => __DIR__. "/../../../../../vendor/esia/keys/isga.obrnadzor.gov.ru.crt",
                'pkey_path'  => __DIR__. "/../../../../../vendor/esia/keys/rootProstoyCA.key",
                'openssl_utility_path' => ''
            );
        else
            return array(
                'site'            => "https://esia-portal1.test.gosuslugi.ru/",
                'logout_uri'  => "https://isga.obrnadzor.gov.ru/auth/saml/logout-service-redirect",
                'auth_uri'     => "https://isga.obrnadzor.gov.ru/auth/saml/assertion-consumer",
                'client_id'     => "OBRN10",
                'scope'         => "fullname email mobile usr_org",
                'cert_path'    => __DIR__. "/../../../../../vendor/esia/keys/isga.obrnadzor.gov.ru.crt",
                'pkey_path'  => __DIR__. "/../../../../../vendor/esia/keys/rootProstoyCA.key",
                'openssl_utility_path' => ''
            );
    }

    private function createUser($email, $username, $orgId){
        $user = new \Model\Entities\isgaUsers();
        $user->setField("Email",$email);
        $user->setField("Login",$email);
        $user->setField("Password",md5(time()));

        $user->setField("Name", $username);
        $user->setField("Role", "user");
        $user->setField("Source", "ЕСИА");
        $user->setField("fk_eiisEducationalOrganization", $orgId);
        $user->save();

        $user=new \Model\Entities\isgaUsers();
        $user = $user->getByEmail($email);

        return $user;
    }

    public function indexAction()
    {

/*
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        return $this->redirect()->toRoute('auth/default', array('controller'=>'authorization', 'action'=>'tech'));
*/
        $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');
        //$logger = new \Model\Logger\Logger("auth", true);



        $isEsia = false;
        if($this->params()->fromQuery("esia") !== null){
            $isEsia = true;
        }



            $authService = $this->getAuthService();
        //var_dump($this->params()->fromQuery());die;
	    //$logger->log("========================");
	    //$logger->log(print_r($this->params()->fromQuery(),true));

        if (!$authService->hasIdentity()) {
            $this->getSessionStorage()->forgetMe();
            $this->getAuthService()->clearIdentity();

            $form = $this->getForm();
            $messages = null;

            $request = $this->getRequest();
            $get = $this->params()->fromQuery();
            $isSaml = false;
            //if(isset($get["type"]) && $get["type"]=="saml")
            //    $isSaml = true;
            //$user = $this->createUser("1","2","5C36243EF6C0547A9171AC4A5EDB8C54");


            //$logger->log("p1");
            //$logger->log(print_r($isSaml,true));
            if ($request->isPost() || $isSaml) {
                $sl = $this->getServiceLocator();
                $form->setInputFilter(new LoginFilter($sl));

                $data = $request->getPost();
                if($isSaml) $data = $get;
                /*$data = [
                    "login" => $user->getField("Login"),
                    "password" => $user->getField("Password")
                ];*/
                //$logger->log(print_r($data,true));

                $form->setData($data);
                //var_dump($form->isValid());die;

                if ($form->isValid()){
                    //check authentication...
                    $pass= md5($data['password']);
                    if($isSaml) $pass = $data['password'];

                    $this->getAuthService()->getAdapter()
                        ->setIdentity($data['login'])
                        ->setCredential($pass);

                    $authResult = $this->getAuthService()->authenticate();
                    //var_dump($authResult->isValid());die;
                    //$logger->log("p2");
                    //$logger->log(print_r($authResult->isValid(),true));
                    if ($authResult->isValid()) {
                        $identity = $authResult->getIdentity();
                        $resultRow = $this->getAuthService()->getAdapter()->getResultRowObject();
                        $user = new \Model\Entities\isgaUsers();

                        $user->setFields((array)$resultRow);
                        //$logger->log($user->getFields());
                        /*if($user->getRole() != 'expert' && $user->getField('Login') != 'eouser1'){
                            $this->getSessionStorage()->forgetMe();
                            $this->getAuthService()->clearIdentity();
                            return $this->redirect()->toRoute('auth/default', array('controller'=>'authorization', 'action'=>'tech'));
                        }*/
                        //$logger->log("p3");
                        $user->checkLogin();

                        //Задаем время жизни сессии в 14 дней
                        //$this->getSessionStorage()->setRememberMe(1);
                        //set storage again
                        //$this->getAuthService()->setStorage($this->getSessionStorage());

                        $authService->getStorage()->write($user->getId());
                        $time = 60 * 10; // 10 минут
                        if( \Eo\Model\Export::isTestServer() ){
                            //если сервер тествый
                            $time = 60  * 60 * 24 * 1; // 1 суток
                        }
                        //$logger->log("p4");
                        $sessionManager = new \Zend\Session\SessionManager();
                        $sessionManager->rememberMe($time);
                        //echo "hi";die;
                        //$logger->log("p5");
                        //$logger->Log("auth hasidentity:");
                        //$logger->Log(print_r($authService->hasIdentity(),true));
                        if($user->getRole() == 'expert') {
                            //$logger->log("p6");
                            return $this->redirect()->toRoute('ron/default', array('controller' => 'index', 'action' => 'index'));
                            exit;
                        } else {
                            //$logger->log("p7");
                            return $this->redirect()->toRoute('eo/default', array('controller' => 'index', 'action' => 'index'));
                            exit;
                        }
                    }else{
                        $messages = "Неверный логин или пароль";
                    }
                }
            }
            return new ViewModel(array(
                'error' => 'Введены неверные данные для авторизации',
                'form'	=> $form,
                'messages' => $messages,
                'isEsia' => $isEsia
            ));
		}else{
            if($authService->hasIdentity()){
                $user = $authService->getIdentity();

                /*if($user->getField('Login') != 'eouser1'){
                    $this->getSessionStorage()->forgetMe();
                    $this->getAuthService()->clearIdentity();
                    return $this->redirect()->toRoute('auth/default', array('controller' => 'authorization', 'action' => 'tech'));
                }*/
                if($user->getRole() == 'expert')
                    return $this->redirect()->toRoute('ron/default', array('controller'=>'index', 'action'=>'index'));
                else
                    return $this->redirect()->toRoute('eo/default', array('controller'=>'index', 'action'=>'index'));
            }
        }
    }


    public function logoutAction()
    {

        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();

       /* if(!\Eo\Model\Export::isTestServer()) {
            $esia = new \EsiaOmniAuth($this->getConfig());
            $esia->request_logout();
        }*/


        return $this->redirect()->toRoute('auth/default', array('controller' => 'authorization', 'action' => 'index'));
    }

    public function deactivatedAction(){
        /*
        $this->layout()->deactivated = true;
        return new ViewModel(array(
        ));
        */
    }

    public function techAction(){
        return new ViewModel(array(
        ));
    }



    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
            $this->authservice->setStorage(new \Zend\Authentication\Storage\Session('Auth'));

            //$logger = new \Model\Logger\Logger("auth", true);
            //$logger->Log("Auth");
            //$logger->Log($this->authservice->getStorage()->getNamespace());

            //$this->authservice->setStorage(new \Zend\Authentication\Storage\Session('Auth'));
        }

        return $this->authservice;
    }

    public function getAuthService2()
    {
        $this->authservice = $this->getServiceLocator()->get('AuthService2');
        return $this->authservice;
    }

    public function getSessionStorage()
    {
        if (! $this->storage) {
            $this->storage = $this->getServiceLocator()
                ->get('Auth\Storage\AuthStorage');
        }

        return $this->storage;
    }

    public function loginAction()
    {
        //if already login, redirect to success page
        if ($this->getAuthService()->hasIdentity()){
            return $this->redirect()->toRoute('success');
        }

        $form       = $this->getForm();

        return array(
            'form'      => $form,
            'messages'  => $this->flashmessenger()->getMessages()
        );
    }

    public function getForm()
    {
        if (! $this->form) {
            $this->form = new LoginForm();
        }

        return $this->form;
    }

    public function clearBugAction(){
        if($this->params("id") != null && strlen($this->params("id")) == 18){
            $filepath = __DIR__.'/../../../../../public/bugs/';
            $file = $filepath.$this->params("id");
            if(file_exists($file)){
                unlink($file);
                echo "Файл успешно удален";die;
            } else {
                echo "Файл не найден. Возможно он был удален ранее.";die;
            }

        }
        echo "Неверная ссылка";
        die;
    }

}