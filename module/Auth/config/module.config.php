<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'Auth' => __DIR__ . '/../public',
            ),
        ),
    ),
    'service_manager' => array(
    ),
    'translator' => array(
        'locale' => 'ru_RU',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '/%s/Zend_Validate.php',
                'text_domain' => 'default'
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Auth\Controller\Authorization',
                        'action'     => 'index',
                    ),
                ),
            ),
            'auth' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/auth',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Auth\Controller',
                        'controller'    => 'Authorization',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]][/:id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'action'        => 'index',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/auth'           => __DIR__ . '/../view/layout/layout.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Auth\Controller\Registration' => 'Auth\Controller\RegistrationController',
            'Auth\Controller\Authorization' => 'Auth\Controller\AuthorizationController',
            'Auth\Controller\Saml' => 'Auth\Controller\SamlController',
        ),
    ),

    /*
    'doctrine' => array(
        // 1) for Authentication
        'authentication' => array( // this part is for the Auth adapter from DoctrineModule/Authentication
            'orm_default' => array(
                'object_manager' => 'Doctrine\ORM\EntityManager',
                // object_repository can be used instead of the object_manager key
                'identity_class' => 'Model\Entities\User',
                'identity_property' => 'login',
                'credential_property' => 'password',
                'credential_callable' => function(\Model\Entities\User $user, $passwordGiven) {
                    if ($user->getPassword() == md5(
                        "aFGQ475SDsdfsaf2342". $passwordGiven)
                        //&& $user->getActive() == 1
                    ) {
                        return true;
                    }
                    else {
                        return false;
                    }
                },
            ),
        ),
    ),*/
    //Сдвиг для пароля, добавляется перед пользовательским паролем при добавлении
    //в базу и всех проверках, чтобы если злоумышленник получил доступ к хэшам
    //паролей, затруднить расшифровку паролей
    "passSalt" => "aFGQ475SDsdfsaf2342",
);
