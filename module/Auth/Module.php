<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Auth;

use Zend\I18n\Translator\Translator;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as DbTableAuthAdapter;
use Zend\Authentication\Storage;
use Zend\Authentication\AuthenticationService;
use Model\Gateway;

class Module
{

    public function init(\Zend\ModuleManager\ModuleManager $moduleManager)
    {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch',
                              function($e) {
            $controller = $e->getTarget();
            if (is_readable(__DIR__ . '/view/layout/layout.phtml')) {
                $controller->layout('layout/auth');
            }
        }, 100);
    }

    public function onBootstrap($e)
    {
        $sm = $e->getApplication()->getServiceManager();
        /* $headLink = $sm->get('viewhelpermanager')->get('headLink');
          $helper = $sm->get('viewhelpermanager')->get('basePath');
          $path = $helper('css/auth.css');
          $headLink->appendStylesheet($path);
         */

        $translator = new \Zend\Mvc\I18n\Translator(new Translator());
//        $translator = new Translator();
        $translator->addTranslationFile(
          'phpArray', 'vendor/zendframework/zendframework/resources/languages/ru/Zend_Validate.php', 'default', 'ru_RU'
        );
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Auth\Storage\AuthStorage' => function($sm) {
                    return new \Auth\Storage\AuthStorage('Auth');
                },
                'AuthService' => function($sm) {
                    //My assumption, you've alredy set dbAdapter
                    //and has users table with columns : user_name and pass_word
                    //that password hashed with md5
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'isga_Users', 'Login', 'Password');
                    //$dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter,
                    //    'User','Login','Password', 'MD5(?)');

                    $authService = new \Auth\Storage\AuthService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('Auth\Storage\AuthStorage'));

                    return $authService;
                },
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}