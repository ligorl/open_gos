<?php

namespace Confirm\Model;

/**
 * готовим данные для печати Формуляр согласия эксперта
 */
class expertiseExpertConsent
{
    public $expertise;
    public $commissionMember;
    public $cahirMain;

    public function __construct($expertiseId,$expertId,$organizationId) {
        $expertise = new \Model\ExpertEntities\expertise();
        $expertise = $expertise->getById($expertiseId);
        if (empty($expertise)){
            throw new Exception('Не найдено экспертизы');
        }
        $this->expertise = $expertise;
        
        $commissionMember = $expertise->getCommissionMember(array(
            'expert_id'      => $expertId,
            'institution_id' => $organizationId,            
        ));
        $this->commissionMember = $commissionMember->getObjectsArray();   
        
        $this->cahirMain = $cahirMain = $expertise->getChairman(array(
                'expert_id'      => $expertId,
                //'institution_id' => $organizationId,            
            ))->getObjectsArray();            
    }
    
    /*
${fieldEduOrgName}
${fieldExpertiseDateBegin}
${fieldExpertiseDateEnd}
    ${tableProgramCode}
    ${tableProgramName}
    ${tableProgramPeriod}
    ${tableLeveEdu}
${fieldCuratorFio}
${fieldCuratorEmail}
${fieldCuratorTelefon}
 */
    public function getData(){
        $result = array();        
        $expertise = $this->expertise;
        
        $commissionMember = $this->commissionMember;    
        
        $result['fieldExpertiseDateBegin'] = $expertise->getDate('create_timestamp');
        $result['fieldExpertiseDateEnd'] = $expertise->getDate('end_timestamp');
        $result['fieldLeav'] = $expertise->getField('no_leav');

        $result['_tableProgramCode']=array();
        //$commissionMember=$commissionMember->getObjectsArray();
        if (!empty($commissionMember)){                        
            foreach ($commissionMember as  $oneMember) {
                $onerogramTable = array();
                $appProgram = $oneMember->getApplicationProgram();
                $licProgram = $appProgram->getLicensedProgram();
                $baseProgram = $licProgram;
                
                $programLevel = $baseProgram->getEduProgramLevel();
                $onerogramTable['tableLeveEdu'] = $programLevel->getField('Name');
                $onerogramTable['tableProgramCode'] = $appProgram->getCode();
                $onerogramTable['tableProgramName'] = $appProgram->getName();
                $onerogramTable['tableProgramPeriod'] = $appProgram->getPeriod();
                $result['_tableProgramCode'][]=$onerogramTable;
            }
            $user   = reset($commissionMember)->getUser();
            $eduOrg = reset($commissionMember)->getEducationalOrganization();            
        }
        
        if(empty($eduOrg)){
            $chairMain = reset($this->cahirMain);
            if(!empty($chairMain)){
                $eduOrg = $chairMain->getEducationalOrganization();                
            }
        }
        
        if(empty($eduOrg)){
            $eduOrg = new \Model\Entities\eiisEducationalOrganizations();
        }
        $result['fieldEduOrgName'] = $eduOrg->getFieldOrSpace('FullName');
        $result['fieldEduOrgAdress'] = $eduOrg->getLAddress();

        //если куратора нет, посмотрим в руководителях
        if(empty($user)){                         
            if(!empty($this->cahirMain)){                
                $cahirMain = reset($this->cahirMain);
                $user = $cahirMain->getUser();    
            }
        }                        
        
        if(empty($user)){            
            $user = new \Model\Entities\ronUsers();
        }          
        
        $result['fieldCuratorFio'] = $user->getFieldOrSpace('Name');
        $result['fieldCuratorEmail'] = $user->getFieldOrSpace('Email');
        $result['fieldCuratorTelefon'] = $user->getFieldOrSpace('Telefon'); 
        
        //если экспертиза без вывезда, то добавим хитрый текст
        //${blokNotTravel}  ${/blokNotTravel}
        $result['blokNotTravel'] = array();
        $noLeav = (int)$expertise->getFieldSafe('no_leav');
        if( $noLeav ){
            $result['blokNotTravel'] = array(1);
        }
        
        return $result;
    }

    public function getTemplateName(){
        return 'confirm/ExpertConsent.docx';
    }

    public function getDocumentName() {
        return 'Формуляр согласия эксперта';
    }
}


