<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Confirm\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * контраллер подтверждения согласия эксперта
 *
 * главный шаблон
 * /module/Confirm/view/layout/layout.phtml             - главный шаблон
 * диалоги
 * /module/Confirm/view/confirm/expert/waiting.phtml    - диалог выбор согласие/отказ
 * /module/Confirm/view/confirm/expert/confirm.phtml    - получение подтверждения
 * /module/Confirm/view/confirm/expert/confirmed.phtml  - согласие получено
 * /module/Confirm/view/confirm/expert/refusing.phtml   - диалог отказывания
 * /module/Confirm/view/confirm/expert/refused.phtml    - отказ получен
 * /module/Confirm/view/confirm/expert/index.phtml      - попало не туда
 * /module/Confirm/view/error/except.phtml              - выпало в ошибку
 *
 *
 */
class ExpertController extends AbstractActionController
{

    public function __construct(){
        $this->mailerClass = new \Eo\Model\SendMail();
        //$headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        //$headStuff->appendStylesheet('/css/confirm/main.css');
    }

    public function defaultAction()
    {

    }


    /**
     * сюда скиним ссылку на проверку
     *
     * @return string
     */
    public function indexAction()
    {
        $this->addCss();
        $view = new ViewModel();
        return $view;
    }

    /**
     * Проверяем ожидает ли экспертом согласие
     *
     * @return ViewModel
     */
    public function waitingAction()
    {
        $this->addCss();
        $this->loging( 'ожидание решение эксперта'   );
        $this->checkContacting( 'AgreementSend' );
        $contacting = $this->getContactingFromParam();

        $urlId = $this->params('id');
        $expert = $this->getExpert();
        $expertise = $this->getExpertise();
        $eduOrg = $this->getOrganisation();

        $uniqId = uniqid( '' , true ).uniqid( '' , true ).uniqid( '' , true ).uniqid( '' , true );
        $uniqId = str_replace( '.', '-' , $uniqId );
        $session = new \Zend\Session\Container( 'contacting' );
        //$session->getManager()->destroy();
        $session->confirm_session = $uniqId ;
        $time = 60 * 10; // 10 минут
        if( \Eo\Model\Export::isTestServer() ){
            //если сервер тествый
            $time = 60  * 60 * 24 * 1; // 1 суток
        }
        $sessionManager = new \Zend\Session\SessionManager();
        $sessionManager->rememberMe($time);
        
        $programAll = $contacting->getProgrammAll();
        $chairMain  = $contacting->getChairMain();

        $view = new ViewModel();
        $view->setVariable('fio'            , $expert->getFio() );
        $view->setVariable('vuzName'        , $eduOrg->getFieldSafe( 'RegularName' ) );
        $view->setVariable('vuzAdres'       , $eduOrg->getAddressLaw() );
        $view->setVariable('expertiseBegin' , $expertise->getDate('create_timestamp') );
        $view->setVariable('expertiseEnd'   , $expertise->getDate('end_timestamp') );
        $view->setVariable('formSession'    , $uniqId );
        $view->setVariable('contactingUniq' , $urlId  );
        $view->setVariable( 'programAll'    , $programAll  );
        $view->setVariable( 'chairMain'     , $chairMain  );
        return $view;
    }

    public function confirmAction(){
        $this->addCss();

        $this->loging( 'подтверждение согласие'   );
        $this->checkContacting( 'AgreementSend' );
        $contacting = $this->getContactingFromParam();

        $urlId = $this->params('id');
        $expert = $this->getExpert();
        $expertise = $this->getExpertise();
        $eduOrg = $this->getOrganisation();

        $sessionForm = $this->getRequest()->getPost( 'formSession' );
        $session = new \Zend\Session\Container( 'contacting' );
        //$session->getManager()->destroy();
        //$session->confirm_session = $uniqId ;
        if( empty($sessionForm) || $sessionForm != $session->confirm_session ){
            return $this->getErrorView( 'не совпали сессии' );
        }


        $sessionForm = $this->getRequest()->getPost( 'formSession' );
        $session = new \Zend\Session\Container( 'contacting' );
        //$session->getManager()->destroy();
        //$session->confirm_session = $uniqId ;
        if( empty($sessionForm) || $sessionForm != $session->confirm_session ){
            return $this->getErrorView( 'не совпали сессии' );
        }

        //проверим не пришел ли файл
        $file = $this->getRequest()->getFiles( 'confirmFile' );
        if( !empty($file) ){
            //return $this->redirect()->toRoute('confirm', array('controller'=>'expert', 'action'=>'comfired'));
            //приехал файл соглаия, запихнем файл в хранилище
            if( empty( $file['size'] )  || !empty( $file['error'] ) ) {
                return $this->getErrorView( 'Ошибка в файле' );
            }

            $name                   = explode('.', $file['name']);
            $ext                    = strrchr( $file['name'], '.');
            $name_info_file         = $file['name'];
            $name_info_file_hash    = md5( \Eo\Controller\DeclarationController::translit('', $name[0]) . '_' . md5(  microtime(true) ) ) . $ext;

            $appId                  = $expertise->getFieldSafe( 'applications_id' );
            $expDocType             = new \Model\ExpertEntities\documenttype(35);
            $SendFileClass          = new \Eo\Model\SendFile();

            $short_path_dir = 'synch/' . date('Y') . '/' . $appId . '/' . $expDocType->getFieldSafe('code') . '/';
            $result        = $SendFileClass->SendFileToOpenServer($file, array("path" => '/public/uploads/' .$short_path_dir, "name" => $name_info_file_hash),10485760);
            if($result != "1"){
                return $this->getErrorView( 'ошибака загрузки файла '. $result );
            }
            $contacting->setFieldSafe( 'date_answer'       , date('Y-m-d') );
            $contacting->setFieldSafe( 'HashName'          , $short_path_dir.$name_info_file_hash );
            $contacting->setFieldSafe( 'RealName'          , $file['name'] );
            $contacting->setFieldSafe( 'contacting_result' , 'СonsentConfirm' );
            $contacting->save( false );

            return $this->redirect()->toUrl( '/confirm/expert/confirmed/'.$contacting->getId() );
        }

        $view = new ViewModel();
        //$view->setTerminal(true);
        $view->setVariable('fio'            , $expert->getFio() );
        $view->setVariable('vuzName'        , $eduOrg->getFieldSafe( 'RegularName' ) );
        $view->setVariable('vuzAdres'       , $eduOrg->getAddressLaw() );
        $view->setVariable('expertiseBegin' , $expertise->getDate('create_timestamp') );
        $view->setVariable('expertiseEnd'   , $expertise->getDate('end_timestamp') );
        $view->setVariable('formSession'    , $session->confirm_session );
        $view->setVariable('contactingUniq' , $urlId  );
        return $view;
    }

    public function formularAction(){
        $this->checkContacting( 'AgreementSend' );
        $this->loging( 'получить формуляр согласия'   );
        $contacting = $this->getContactingFromParam();

        $expert = $this->getExpert();
        $expertise = $this->getExpertise();
        $eduOrg = $this->getOrganisation();

        $sessionForm = $this->getRequest()->getPost( 'formSession' );
        $session = new \Zend\Session\Container( 'contacting' );
        //$session->getManager()->destroy();
        //$session->confirm_session = $uniqId ;
        if( empty($sessionForm) || $sessionForm != $session->confirm_session ){
            return $this->getErrorView( 'не совпали сессии' );
        }

        $ourDocType='docx';
        $consetDoc = new \Confirm\Model\expertiseExpertConsent( $expertise->getId() , $expert->getId() , $eduOrg->getId() );
        $arrayData = $consetDoc->getData();
        $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
        $templateName = $templatePath.$consetDoc->getTemplateName(); //шаблон документа
        $ourDocName=$consetDoc->getDocumentName();
        $templater= new \Eo\Model\Templater($templateName,$arrayData,$ourDocType,$ourDocName );
        if(empty($param)){
            $templater->makeDocumentOut();
            die;
        }
        throw new \Exception( 'не смогло сформироать документ' );
    }

    public function confirmedAction(){
        $this->addCss();
        $this->loging( 'согласился'   );
        $this->checkContacting( 'СonsentConfirm' );

        $view = new ViewModel();
        return $view;
    }

    public function refusingAction(){
        $this->addCss();

        $this->loging( 'получение отказа'   );
        $this->checkContacting( 'AgreementSend' );
        $contacting = $this->getContactingFromParam();

        $urlId = $this->params('id');
        $expert = $this->getExpert();
        $expertise = $this->getExpertise();
        $eduOrg = $this->getOrganisation();

        $sessionForm = $this->getRequest()->getPost( 'formSession' );
        $session = new \Zend\Session\Container( 'contacting' );
        //$session->getManager()->destroy();
        //$session->confirm_session = $uniqId ;
        if( empty($sessionForm) || $sessionForm != $session->confirm_session ){
            return $this->getErrorView( 'не совпали сессии' );
        }

        $sessionForm = $this->getRequest()->getPost( 'formSession' );
        $session = new \Zend\Session\Container( 'contacting' );
        //$session->getManager()->destroy();
        //$session->confirm_session = $uniqId ;
        if( empty($sessionForm) || $sessionForm != $session->confirm_session ){
            return $this->getErrorView( 'не совпали сессии' );
        }

        //проверим не пришел ли файл
        $reason = $this->getRequest()->getPost( 'reason' );
        if( !empty($reason) ){
            //приехала причина отказа
            $contacting->setFieldSafe( 'date_answer' , date('Y-m-d') );
            $contacting->setFieldSafe( 'description' , $reason );
            $contacting->setFieldSafe( 'contacting_result' , 'СonsentRefused' );
            $contacting->save( false );
            return $this->redirect()->toUrl('/confirm/expert/refusing/'.$contacting->getId() );
        }

        $view = new ViewModel();
        $view->setVariable('fio'            , $expert->getFio() );
        $view->setVariable('vuzName'        , $eduOrg->getFieldSafe( 'RegularName' ) );
        $view->setVariable('vuzAdres'       , $eduOrg->getAddressLaw() );
        $view->setVariable('expertiseBegin' , $expertise->getDate('create_timestamp') );
        $view->setVariable('expertiseEnd'   , $expertise->getDate('end_timestamp') );
        $view->setVariable('formSession'    , $session->confirm_session );
        $view->setVariable('contactingUniq' , $urlId  );
        return $view;

    }

    public function refusedAction(){
        $this->addCss();
        $this->loging( 'отказался'   );
        $this->checkContacting( 'СonsentRefused' );

        $view = new ViewModel();
        return $view;
    }

    public function getErrorView( $message ){
        $view = new ViewModel();
        $view->setVariable('message',  $message );
        $view->setTemplate( 'confirm/error.phtml' );
        return $view;
    }

    protected function addCss(){
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        $headStuff->appendStylesheet('/css/confirm/main.css');
    }

    private $_contacting = 0 ;
    protected function getContactingFromParam(){
        if( 0 === $this->_contacting ){
            $hasError = false;
            $contactingId = $this->params('id');
            if( empty($contactingId) ){
                throw new \Exception( 'Не указан полный путь' );
            }
            $this->_contacting = new \Model\ExpertEntities\contacting();
            $this->_contacting = $this->_contacting->getById( $contactingId , 'uniqUrlId' );
            if( empty( $this->_contacting ) ){
                $hasError = true;
            }
            else{
                if( $this->_contacting->isEmpty() ){
                     $hasError = true;
                }
            }
            if( $hasError ){

                if( \Eo\Model\Export::isTestServer() ){
                    $hasError = false;
                    $this->_contacting = new \Model\ExpertEntities\contacting( $contactingId );
                    if( empty( $this->_contacting ) ){
                        $hasError = true;
                    }
                    else{
                        if( $this->_contacting->isEmpty() ){
                            $hasError = true;
                        }
                    }
                }
            }
            if( $hasError ){
                throw new \Exception( 'Не найдена запись' );
            }
        }
        return $this->_contacting;
    }

    private $_expert = 0;
    protected function getExpert(){
        if( 0 === $this->_expert ){
            $this->_expert = $this->getContactingFromParam()->getExpert();
            if( empty( $this->_expert ) ){
                throw new \Exception( 'Эксперт не найден' );
            }
        }
        return $this->_expert;
    }

    private $_expertise = 0;
    protected function getExpertise(){
        if( 0 === $this->_expertise ){
            $this->_expertise = $this->getContactingFromParam()->getExpertise();
            if( empty( $this->_expertise ) ){
                throw new \Exception( 'Экспертиза не найдена' );
            }
        }
        return $this->_expertise;
    }

    private $_eduOrg = 0;
    protected function getOrganisation(){
        if( 0 === $this->_eduOrg ){
            $this->_eduOrg = $this->getContactingFromParam()->getOrganisation();
            if( empty( $this->_eduOrg ) ){
                throw new \Exception( 'не найдена организация' );
            }
        }
        return $this->_eduOrg;
    }

    protected function checkContacting( $itStatus ){               
        $urlId = $this->params('id');
        $contactingClass = $this->getContactingFromParam();
        
        $this->loging( 'контактинг ид ' . $contactingClass->getId()  );
        $this->loging( 'контактинг урлид ' . $contactingClass->getFieldSafe( 'uniqUrlId' )  );
        //проверим время экспертизы
        $curentDate = date( 'Y-m-d' );
        $expertise = $this->getExpertise();
        $dateEnd = substr( $expertise->getFieldsafe( 'end_timestamp' ) , 0 , 10 );
        if( strcmp( $curentDate , $dateEnd ) > 0 ){
            throw new \Exception( $this->loging( 'Экспертиза уже окончена' ) );
        }
        $dateBegin = substr( $expertise->getFieldsafe( 'create_timestamp' ) , 0 , 10 );
        if( strcmp( $curentDate , $dateBegin ) >= 0 ){
            throw new \Exception( $this->loging( 'Экспертиза уже началась' ) );
        }

        $actionStatus = $contactingClass->getFieldSafe( 'contacting_result' );
        $this->loging( 'контактинг статус ' . $actionStatus  );

        if( 'СonsentConfirm' != $itStatus && 'СonsentConfirm' == $actionStatus ){
            $this->loging( 'переход на уже подписано '   );
            return $this->redirect()->toUrl('/confirm/expert/confirmed/'.$urlId);
        }
        if( 'СonsentRefused' != $itStatus && 'СonsentRefused' == $actionStatus ){
            $this->loging( 'переход на отказано '   );
            return $this->redirect()->toUrl('/confirm/expert/refused/'.$urlId);
        }
        if( 'AgreementSend' != $itStatus && 'AgreementSend' == $actionStatus ){
            $this->loging( 'переход на выбор выборсогласия'   );
            return $this->redirect()->toUrl('/confirm/expert/waiting/'.$urlId);
        }
        if( $itStatus != $actionStatus ){
            throw new \Exception( $this->loging( 'Согласие не в отправке' ) );
        }
        return true;
    }

    public function loging( $massage ){
        $logFile = 'public/log/contacting/contacting_'.date('Y-m-d').'.log';
        return $this->mailerClass->mailLog( $massage  , $logFile );
    }

    //последняя строка

}