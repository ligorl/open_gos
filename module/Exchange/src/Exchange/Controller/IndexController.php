<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Exchange\Controller;

use Zend\Db\ResultSet\ResultSet;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

class IndexController extends AbstractActionController
{
    public function indexAction(){
        $data = $this->getRequest()->getPost();
        $data->data = base64_decode($data->data);

        $exchanger = new \Model\SyncService\SyncModule();

        //Проверяем если запрос составлен верно
        if($data == null || !isset($data->connect)){
            echo "Неверный запрос";
            die;
        }

        //Проверяем если пароль передан верно
        $checkResult = $exchanger->checkPass($data->connect);
        if(!$checkResult){
            echo "Неверный пароль";
            die;
        }

        //Обрабатываем запрос
        $exchanger->parseRequest($data);

        die;
    }

    public function mailAction(){
        $data = $this->getRequest()->getPost();
        $regPass = md5(date("m.d.y")."j9jdK%%s");

        if($data["rp"]!=$regPass){
            echo "Неверный пароль";
            die;
        }

        /**
         * Куски и наработки для отправки смтп, вдруг еще пригодится
         */
/*
        function MailSmtp($reciever, $subject, $content, $headers, $debug = 0) {
            if($debug){
                $logger = new \Model\Logger\Logger("mail.log");
            }

            $smtp_server = '192.168.66.7'; // адрес SMTP-сервера
            $smtp_port = 25; // порт SMTP-сервера
            //$smtp_user = 'isga_sys@msk.nica.ru'; // Имя пользователя для авторизации на SMTP-сервере
            //$smtp_password = 'Qwerty123'; // Пароль для авторизации на SMTP-сервере
            $mail_from = 'isga_sys@msk.nica.ru'; // Ящик, с которого отправляется письмо

            $sock = fsockopen($smtp_server,$smtp_port,$errno,$errstr,30);

            $str = fgets($sock,512);
            if (!$sock) {
                if($debug)
                    $logger->Log("Socket is not created");
                exit(1);
            }

            smtp_msg($sock, "HELO " . $_SERVER['SERVER_NAME'], $debug);
            //smtp_msg($sock, "AUTH LOGIN");
            //smtp_msg($sock, base64_encode($smtp_user));
            //smtp_msg($sock, base64_encode($smtp_password));
            smtp_msg($sock, "MAIL FROM: <" . $mail_from . ">", $debug);
            smtp_msg($sock, "RCPT TO: <" . $reciever . ">", $debug);
            smtp_msg($sock, "DATA", $debug);

            $headers = "Subject: " . $subject . "\r\n" . $headers;

            $data = $headers . "\r\n\r\n" . $content . "\r\n.";

            smtp_msg($sock, $data, $debug);
            smtp_msg($sock, "QUIT", $debug);

            fclose($sock);
        }

        function smtp_msg($sock, $msg, $debug = 0) {
            if($debug){
                $logger = new \Model\Logger\Logger("mail.log");
            }
            if (!$sock) {
                if($debug)
                    $logger->Log("Broken socket!");
                exit(1);
            }

            if($debug){
                $logger->Log("Send from us:\n".nl2br(htmlspecialchars($msg)));
            }
            fputs($sock, "$msg\r\n");
            $str = fgets($sock, 512);
            if (!$sock) {
                if($debug){
                    $logger->Log("Socket is down");
                }
                exit(1);
            }
            else {
                if($debug){
                    $logger->Log("Got from server:\n".nl2br(htmlspecialchars($str)));
                }

                $e = explode(" ", $str);
                $code = array_shift($e);
                $str = implode(" ", $e);

                if ($code > 499) {
                    if($debug) {
                        $logger->Log("Problems with SMTP conversation.\nCode: " . $code . "\nMessage:  " . $str);
                    }
                    exit(1);
                }
            }
        }
*/


        /**
         * Вариант smtp через zend
         */
        /*
        $smtpOptions = new \Zend\Mail\Transport\SmtpOptions(array(
            'name' => '192.168.66.7',
            'host' => '192.168.66.7',
            'port' => 25,
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'isga_sys@msk.nica.ru',
                'password' => 'Qwerty123',
            ),
        ));


        $transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
        $transport->send($message);
        */

        $userLogin = $data["login"];
        $pass = $data["pass"];
        $email = $data["mail"];
        $orgName = empty($data['orgName'])?'':$data['orgName'].'<br><br>';

        //Отправляем емэйл
        $messageText ="
Добрый день!
<br><br>"
.$orgName.
"Ваша заявка на получение доступа к информационной системе сопровождения государственной аккредитации была принята и обработана. В течение ближайшего времени Ваш аккаунт будет активирован. Адрес для входа в личный кабинет: <a href='http://isga.obrnadzor.gov.ru/'>http://isga.obrnadzor.gov.ru/</a>
<br><br>
Ваши персональные данные:<br>
Логин: <b>".$userLogin."</b><br>
Пароль: <b>".$pass."</b><br><br>
Это письмо было создано автоматически и не требует ответа.";
/*
        $messageText = iconv('UTF-8','windows-1251',"Добрый день!

Ваша заявка на получение доступа к информационной системе сопровождения государственной аккредитации была принята и обработана. В течение ближайшего времени Ваш аккаунт будет активирован. Адрес для входа в личный кабинет: http://isga.obrnadzor.gov.ru/


Ваши персональные данные:
Логин: ".$userLogin."
Пароль: ".$pass."

Это письмо было создано автоматически и не требует ответа.");
*/

        $to  = $email ;

        $subject = "Регистрация в системе ИСГА";

        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= 'From: isga_sys@msk.nica.ru';


        $html = new \Zend\Mime\Part( iconv('UTF-8','windows-1251',$messageText));
        $html->type = "text/html";

        $body = new \Zend\Mime\Message();
        $body->setParts(array($html));

        $message = new \Zend\Mail\Message();

        $message->setBody($body);
        $message->setSender(iconv('UTF-8','windows-1251',"isga_sys"));
        $message->setFrom( iconv('UTF-8','windows-1251',"isga_sys"));
        $message->addTo($to);
        $message->setSubject(iconv('UTF-8','windows-1251',$subject));
        $message->setEncoding("windows-1251");;

        //$transport = new \Zend\Mail\Transport\Sendmail();
        //$transport->send($message);

        $smtpOptions = new \Zend\Mail\Transport\SmtpOptions(array(
            'name' => '192.168.66.240',
            'host' => '192.168.66.240',
            'port' => 25,
        ));
        $transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
        try {
            $transport->send($message);
        } catch (\Exception $e) {
            mail($to, $subject, $messageText, $headers);
        }
        mail("isga.sys@yandex.ru", $subject, $messageText, $headers, "-f isga_sys@msk.nica.ru");



/*
        require "../../../../../vendor/PHPMailer-master/PHPMailerAutoload.php";
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
//$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = '192.168.66.7';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = false;                               // Enable SMTP authentication

        $mail->Port = 25;                                    // TCP port to connect to

        $mail->setFrom('КИС ГА isga_sys@msk.nica.ru');
        $mail->addAddress($to);            // Name is optional

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = $subject;
        $mail->Body    = $messageText;
        $mail->

        $logger = new \Model\Logger\Logger("newmail.log");
        if(!$mail->send()){
            $logger->Log('Message could not be sent.');
            $logger->Log('Mailer Error: ' . $mail->ErrorInfo);
        } else {
            $logger->Log('Message has been sent');
        }
*/
        //mail($to, $subject, $messageText, $headers, "-f isga_sys@msk.nica.ru");
        //MailSmtp($to, $subject, $messageText, $headers, 1);
        die;
    }


    /**
     * проксируем отсылку почты по хитрому
     * с вложениями
     *
     *          $leter['mail']['To']                      - куда
     *          $leter['mail']['ToName']                  - куда красиваое имя, не обязательно
     *          $leter['mail']['rp']                      - секретный пароль чтоб лишние не лезли
     *          $leter['mail']['Body']                    - само письмо
     *          $leter['mail']['Sumject']                 - тема
     *          $leter['mail']['Attach'][*]['Name']       - название вложеного файла
     *          $leter['mail']['Attach'][*]['Content']    - содержимое вложеного файла
     *          $leter['debug']['sendOnly']               - отослать не отдавать форму, старое
     *          $leter['debug']['printBody']              - возращает тело письма, не обязательно
     *          $leter['debug']['printLeter']             - возращает весь текст письми, не обязательно
     *          $leter['debug']['work']                   - должен совпадать с $leter['mail']['To'] иначе на тестовый адрес
     *          $leter['debug']['log']                    - комент в лог
     *          $leter['mail']['AttachUpload'][*]['Path'] - путь к файлу на этом сервере
     *          $leter['mail']['AttachUpload'][*]['Name'] - как надо его звать
     *          Файлы - принимает как вложения
     *
     *          результат
     *              'Ok' - слава богу
     *              иное - чегото отвалилось
     *
     */
    public function mailProxyAction(){
        $this->mailLog('Начинаем отправку письма');
        @$this->mailLog('От '.$_SERVER['REMOTE_ADDR']);
        $post = $this->getRequest()->getPost();
        if ( !empty($post['debug']['log']) ) {
            if ( is_string($post['debug']['log']) ) {
                $this->mailLog($post['debug']['log']);
            }
            if ( is_array($post['debug']['log']) ){
                foreach ( $post['debug']['log'] as $value ) {
                    $this->mailLog($value);
                }
            }
        }
        if(empty($post) || empty($post['mail'])){
            echo 'Данные не приехали';
            $this->mailLog('Данные не приехали');
            die;
        }
        $regPass = md5(date("m.d.y")."j9jdK%%s");
        $mail = $post['mail'];
        if(empty($mail["rp"])||$mail["rp"]!=$regPass){
            echo "Неверный проходной пароль";
            $this->mailLog('Неверный проходной пароль');
            die;
        }
        if(empty($mail['To'])){
            echo "Нет получателя";
            $this->mailLog('Нет получателя');
            die;
        }
        $this->mailLog('хотим отправить на '.$mail['To']);
        $isDebug = true;
        $to = 'test-isga@mail.ru';
        if(!empty($post['debug']['work']) && $post['debug']['work']==$mail['To']){
            $to  = $mail['To'] ;
            $isDebug = false;
        }
        $this->mailLog('отправка на реально '.$to);
        $this->mailLog('режим отладки '.$isDebug);
        $validator = new \Zend\Validator\EmailAddress();
        //if (!$validator->isValid($to)) {
        //    echo 'Почтовый адрес '.$to.' не почтовый адрес ';
        //    $this->mailLog('Почтовый адрес '.$to.' не почтовый адрес ');
        //    die;
        //}
        $toName = null;
        if(!empty($mail['ToName'])){
            $toName  = $mail['ToName'] ;
        }
        if(empty($mail['Sumject'])){
            $subject = '';
        } else {
            $subject = $mail['Sumject'];
        }
        if(empty($mail['Body'])){
            $messageText ='';
        } else {
            $messageText = $mail['Body'];
        }
            $headers  = "MIME-Version: 1.0\r\n";
            //$headers .= "Content-type: text/html; charset=utf-8 \r\n";
            $headers .= "Content-type: text/plain; charset=utf-8\r\n";
            $headers .= 'From: isga_sys@msk.nica.ru';

            $html = new \Zend\Mime\Part($messageText);
            //$html->type = \Zend\Mime\Mime::TYPE_TEXT;
            $html->type = \Zend\Mime\Mime::TYPE_HTML;
            $html->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
            $html->charset = 'utf-8';

            $message = new \Zend\Mail\Message();
            //$message->setSender(array('isga_sys@msk.nica.ru'=>'isga_sys'));
            $message->addTo($to,$toName);
            if(!empty($subject)){
                $message->setSubject($subject);
            }
            $message->setEncoding("utf-8");
            $message->setFrom('isga_sys@msk.nica.ru','isga_sys');

            $mimeMessage = new \Zend\Mime\Message();
            $mimeMessage ->addPart($html);

            $this->mailLog('главны блок добавлен ');
            $this->mailLog('долливаем файлы ');

            if(isset($_FILES['mail'])&& !empty($_FILES['mail']['name'][0])){
                $this->mailLog('    прикрепляем файлы из присланых как файлы');
                foreach ($_FILES['mail']['name'] as $key => $one_file) {
                    $this->mailLog('        прикрепляем файл');
                    if ($_FILES['mail']['size'][$key]=0){
                        //echo 'нет длины';
                        $this->mailLog('        нет длины');
                        continue;
                    }
                    if($_FILES['mail']['error'][$key] != 0){
                        echo 'ошибка в файле';
                        $this->mailLog('        ошибка в файле');
                        continue;
                    }

                    $fileContent = file_get_contents($_FILES['mail']['tmp_name'][$key]);
                    $attachment = new \Zend\Mime\Part($fileContent);
                    $attachment->type = $_FILES['mail']['type'][$key];
                    $attachment->filename = $_FILES['mail']['name'][$key];
                    $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
                    $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                    $this->mailLog('        файл '.$attachment->filename );

                    $mimeMessage->addPart( $attachment);
                    $this->mailLog('        файл добавлен');
                }
            }

            //если в посте файлы
            if(!empty($post['mail']['Attach'])){
                $this->mailLog('    прикрепляем файлы в запросе');
                foreach ( $post['mail']['Attach'] as $key => $value ) {
                    $this->mailLog('        прикрепляем файл');
                    if(empty($value['Name'])){
                        echo 'Нет Имени файла';
                        $this->mailLog('        Нет Имени файла');
                        die;
                    }
                    if(empty($value['Content'])){
                        echo 'Нет содержимого файла';
                        $this->mailLog('        Нет содержимого файла');
                        die;
                    }
                    $attachment = new \Zend\Mime\Part($value['Content']);
                    $attachment->type = \Zend\Mime\Mime::TYPE_OCTETSTREAM;
                    $attachment->filename = $value['Name'];
                    $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
                    $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                    $this->mailLog('        файл '.$attachment->filename );

                    $mimeMessage->addPart( $attachment);
                    $this->mailLog('        Файл добавлен');
                }
            }

            //Если отправляемые файлы на сервере в хранилище
            if(!empty($post['mail']['AttachUpload'])){
                $this->mailLog('    Собираем файлы с сервера');
                foreach ( $post['mail']['AttachUpload'] as $key => $value ) {
                    if(empty($value['Path'])){
                        $this->mailLog('        нет пути');
                        continue;
                    }
                    $hashName = $value['Path'];
                    $realName = $value['Name'];
                    //если нет реального сойдет и кривое
                    if(empty($realName)){
                        $hashNameArray = explode('/', $hashName);
                        $hashNameArray = array_filter($hashNameArray);
                        $realName = end($hashNameArray);
                    }
                    $path = $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$hashName;
                    // проверим на наличие файла
                    $isFile = is_file($path );
                    if(!$isDebug && !$isFile){
                        echo 'Файл '.$realName.' как '.$hashName.' отсутствует';
                        $this->mailLog('        Файл '.$realName.' как '.$hashName.' отсутствует');
                        die;
                    }
                    If(!$isFile){
                        $fileContent = 'Заглушка вместо '.$path;
                        $this->mailLog('        Заглушка вместо '.$path);
                    } else {
                        $fileContent = file_get_contents($path);
                    }
                    $attachment = new \Zend\Mime\Part($fileContent);
                    $attachment->type = \Zend\Mime\Mime::TYPE_OCTETSTREAM;
                    $attachment->filename = $realName;
                    $attachment->disposition = \Zend\Mime\Mime::DISPOSITION_ATTACHMENT;
                    $attachment->encoding = \Zend\Mime\Mime::ENCODING_BASE64;
                    $this->mailLog('        файл '.$attachment->filename );

                    $mimeMessage->addPart( $attachment);
                    $this->mailLog('        файл добавлен');
                }
            }

            $message->setBody($mimeMessage);

            if(isset($post['debug']['printBody'])){
                header('Content-Type: text/plain');
                echo $message->getBodyText();
                die;
            }
            if(isset($post['debug']['printLeter'])){
                header('Content-Type: text/plain');
                echo $message->toString();
                die;
            }
            if(isset($post['debug']['printPart'])){
                header('Content-Type: text/plain');
                $mimeMessage->getParts();
                die;
            }
            $this->mailLog('готовим к отправке');
            $smtpOptions = new \Zend\Mail\Transport\SmtpOptions(array(
                    'name' => '192.168.66.240',
                    'host' => '192.168.66.240',
                    //'name' => 'localhost',
                    //'host' => 'localhost',
                    //'name' => 'mx.nica.ru',
                    //'host' => 'mx.nica.ru',
                    'port' => 25,
            ));

            $transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
                //ini_set("SMTP","server.aln" );
                //ini_set("smtp_port","25");
                //ini_set('sendmail_from', 'server.aln');
                //$subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
                //$messageText =  $message->getBodyText();
                //mail($to, $subject, $messageText, $headers);
            try {
                $this->mailLog('отправляем первым сервером');
                $transport->send($message);
            } catch (\Exception $e) {
                $this->mailLog('отправить первым сервером не удалось ');
                $this->mailLog('Первый сервер отсылки не исправен '."".$e->getMessage());
                $smtpOptions = new \Zend\Mail\Transport\SmtpOptions(array(
                    'name' => 'localhost',
                    'host' => 'localhost',
                    //'name' => '192.168.66.240',
                    //'host' => '192.168.66.240',
                    //'connection_config' => array(
                    //    'username' => 'root',
                    //    'password' => 'z1x2c3v4',
                    //    //'ssl'      => 'tls',
                    //),
                    'port' => 25,
                ));
                $transport = new \Zend\Mail\Transport\Smtp($smtpOptions);
                try {
                    $this->mailLog('отправляем вторым сервером');
                    $transport->send($message);
                } catch (\Exception $e) {
                    echo 'Второй сервер отсылки не исправен '."".$e->getMessage();
                    $this->mailLog('Сервер отсылки не исправен '."\n".$e->getMessage());
                    $this->mailLog('отправка провалилась');
                    $this->mailLog('');
                    die;
                }
                //ini_set("SMTP","server.aln" );
                //ini_set("smtp_port","25");
                //ini_set('sendmail_from', 'server.aln');
                //$subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
                //mail($to, $subject, $messageText, $headers);
            }
            //mail("isga.sys@yandex.ru", $subject, $messageText, $headers, "-f isga_sys@msk.nica.ru");
            //если не отладка то пошлем копию
            if(!$isDebug ){
                $toCopy = 't.lubinets@msk.nica.ru';
                $toCopy = 'isga_experts@mail.ru';
                $toCopyName = 'копия письма';
                //$toCopy = 'test_isga@mail.ru';
                $this->mailLog('отправлено на '.$to);
                $this->mailLog('отправим копию на '.$toCopy);
                //$to = $message->getTo()->get();
                $message->setTo($toCopy,$toCopyName);
                //$subject = $message->getSubject();
                $subject = 'копия отправленного на '.$to.' '.$subject;
                $message->setSubject($subject);
                try {
                    $transport->send($message);
                } catch (\Exception $e) {
                    echo 'Сервер отсылки не исправен '."\n".$e->getMessage();
                    $this->mailLog('Сервер отсылки копии не исправен '."\n".$e->getMessage());
                    die;
                }
                $this->mailLog('копия отправлена');
            }
            echo 'Ok';
            $this->mailLog('Ok');
            $this->mailLog('');
            die;
    }

    public function eiisAction(){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities();
        $serviceEntities->getEntitiesFromService("EIIS.SCHOOL_TYPES",40);
        echo "done";
        die;
    }


    public function setOrgIdAction(){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        $programsTable = new \Model\Gateway\LodEntitiesTable("EducationProgram");
        $programs = $programsTable->getAllEntities();

        $n = 0;
        foreach($programs as $program){
            if($n % 1000 == 0)
                echo $n."<br>";
            $n++;
            if($program != null && $program->getId() != null) {
                $pSupp = $program->gelLicenseSupplement();
                if($pSupp!=null) {
                    $program->setField("orgId_fromSupplement", $pSupp->getField("fk_eiisBranch"));
                    $program->save(false, true);
                }
            }
        }
        echo "done";die;
    }

    /**
     * логирование
     *
     * @param type $logMsg
     * @param string $fileName
     */
    function mailLog( $logMsg, $fileName ='public/log/mail.log') {
        if( 'public/log/mail.log' == $fileName ){
            $fileName ='public/log/mail/mail-'.date('Y-m-d').'.log';
        }
        $date = date('Y-m-d H:i:s (T)');
        $fileName  = $_SERVER['DOCUMENT_ROOT'].'/'.$fileName;
        //проверка на есть ли файл реально, если что строим к нему путь
        if( !is_file($fileName) ){
            $fileDir = dirname( $fileName );
            if( !is_dir($fileDir) ){
                if( @ !mkdir($fileDir, 0777, true) ){
                    throw new \Exception('не удалось создать путь к логу');
                }
            }
        }
        //запишем в файл
        $f = fopen($fileName, 'a+');
        if (!empty($f)) {
            $msg  = "[".$this->getUniqId()."] [$date] $logMsg".PHP_EOL;
            fwrite($f, $msg);
            fclose($f);
        }
        return $logMsg;

    }

    private $_uniqId = null;
    public function getUniqId() {
        if(empty($this->_uniqId)){
            $this->_uniqId = md5(microtime(true));
        }
        return $this->_uniqId;

    }

}
