<?php
namespace Service\Model;
use Zend\Log\Logger;

/**
 * Класс сервиса
 * @package Service\Model
 */
class Service{
    private $isDebug = false;
    private $isTest = false;

    /**
     * @param string $login
     * @param string $password
     * @return string
     */
    public function GetSessionId($login, $password){
        $logger = new \Model\Logger\Logger();
        if($login == "isga_service_user" && $password == "s4g0b7c9") {
            try {
                //сохраняем айди в бд
                $serviceIdEntity = new \Model\Entities\isgaserviceSessionId();
                $sessionId = $serviceIdEntity->save(true, true);
                //возвращаем айди
                return "<session id='" . $sessionId . "'/>";
            }catch(\Exception $e){
                $logger = new \Model\Logger\Logger();
                $logger->log($e->getMessage());
            }
        } else {;
            return "0321";
        }
    }

    /**
     * @param string $sessionId
     * @param string $objectCode
     * @param boolean $historyCreate
     * @param boolean $documentInclude
     * @param string $filter
     * @return string
     */
    public function CreatePackage($sessionId, $objectCode, $historyCreate = false, $documentInclude = false, $filter = ""){
        $logger = new \Model\Logger\Logger("service",true);
        $logger->Log("=================");
        try{
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("=================");
            $logger->Log($objectCode);
            //проверяем идентификатор сессии
            if(!$this->_checkSessionId($sessionId)){
                return "0322";
            }

            //Проверяем $objectCode на соответствие, чтобы такой был
            $sh = new \Service\Model\ServiceHelper();
            if(!$sh->hasObjectCode($objectCode))
                return "034";

            //создаем пакет
            $logger->Log("Создаем пакет");
            $package = new \Model\Entities\isgaservicePackage();
            $package->setFields(array(
                "fk_isgaserviceSessionId" => $sessionId,
                "ObjectCode" => $objectCode
            ));


            //Подсчитываем его количество частей
            $logger->Log("Подсчитываем количество частей");
            $corrObj = $sh->getObjectData($objectCode);

            //Объект для работы с таблицей
            if(isset($corrObj["db"]) && $corrObj["db"] == "lod")
                $table = new \Model\Gateway\LodEntitiesTable($corrObj["table"]);
            else
                $table = new \Model\Gateway\EntitiesTable($corrObj["table"]);

            //Подсчитываем количество частей
            //если селект в объекте прописан
            $logger->Log("до count");
            if(isset($corrObj["count"])){
                $adapter = $table->getGateway()->getAdapter();
                $resultsCount = $adapter->query($corrObj["count"], $adapter::QUERY_MODE_EXECUTE)->current();

                $allCount = $resultsCount["count"];
            }
            //если селект в объекте не прописан
            else if(isset($corrObj["onlyOur"]) && $corrObj["onlyOur"]){
                $allCount = $table->getCount(array(
                    new \Zend\Db\Sql\Predicate\Expression("CHAR_LENGTH(Id) > 40")
                ));
            } else {
                $where = [];
                if(isset($corrObj["where"]))
                    $where = $corrObj["where"];

                $allCount = $table->getCount($where);
            }

            $onPart = 1500;

            $parts = intval($allCount/$onPart);
            if($parts*$onPart<$allCount)
                $parts++;

            $logger->Log("allcount: ".$allCount);
            $logger->Log("Частей: ".$parts);

            $package->setField("Capacity",$parts);
            $package->setField("State","New");
            $packageId = $package->save(true, true);

            //sleep(100);


            if(!$this->isDebug) {
                $logger->Log("Запускаем обработку в новом потоке");
/*
                $cmd = "/usr/bin/php " . $_SERVER['DOCUMENT_ROOT'] . "/public/index.php serviceCreatePackage " . $packageId;
                $output = exec($cmd);

                $logger->Log("Если запуск что-то вернул, выводим ниже:");
                $logger->Log($output);
  */

                $curl = curl_init();

                $destination_url = "http://".$_SERVER['SERVER_NAME']."/service/index/start-package?packageId=".$packageId;

                $logger->Log($destination_url);

                curl_setopt($curl, CURLOPT_URL, $destination_url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($curl, CURLOPT_TIMEOUT, 1);

                $res = curl_exec($curl);
                $logger->Log(print_r($res,true));
                curl_close($curl);

/*
                $query = "GET http://".$_SERVER['SERVER_NAME']."/service/index/start-package?packageId=".$packageId."  HTTP/1.0\r\n";
                //$fp=fopen($query,"r");
                //fclose($fp);

                $fsock = fsockopen($_SERVER['SERVER_NAME'], 80);
                fputs($fsock, $query);
                fputs($fsock, "Host: $this->server\r\n");
                fputs($fsock, "\r\n");
                stream_set_blocking($fsock, 0);
                stream_set_timeout($fsock, 3600);
                fclose($fsock);
*/
/*
                $post_data = http_build_query(array(
                    "packageId" => $packageId
                ));
                $logger->Log("1");
                $logger->Log("http://".$_SERVER['SERVER_NAME'] . ":80");
                $s = stream_socket_client(
                    $_SERVER['SERVER_NAME'] . ":80",
                    $errno,
                    $errstr,
                    20,
                    STREAM_CLIENT_ASYNC_CONNECT | STREAM_CLIENT_CONNECT
                );
                $logger->Log($errno);
                $logger->Log($errstr);
                $logger->Log("2");
                if ($s) {
                    $logger->Log("3");
                    $http_message = "POST /service/index/start-package HTTP/1.0\r\nHost: " . $_SERVER['SERVER_NAME'] . "\r\n";
                    fwrite($s, $http_message);
                    fwrite($s, "Content-type: application/x-www-form-urlencoded\r\n");
                    fwrite($s, "Content-length: " . strlen($post_data) . "\r\n");
                    fwrite($s, "Connection: close\r\n\r\n");
                    fwrite($s, $post_data);
                    fclose($s);
                } else {
                    $logger->Log("100");
                    return false;
                }

*/
            } else {
                $sh = new \Service\Model\ServiceHelper();
                $sh->createPackage($packageId);
            }

            /*
            $sh = new \Service\Model\ServiceHelper();
            $sh->createPackage($packageId);
*/
            $logger->Log("Возвращение результата:");
            $logger->Log(print_r($packageId, true));
            //возвращаем результат

            return "<package id='".$packageId."'/>";
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service", true);
            $logger->Log($e->getLine()." - ".$e->getMessage());
        }
    }

    /**
     * @param string $sessionId
     * @param string $packageId
     * @return string
     */
    public function GetPackageMeta($sessionId, $packageId){
        //проверяем идентификатор сессии
        if(!$this->_checkSessionId($sessionId)){
            return "0322";
        }

        //подгружаем пакет
        $package = new \Model\Entities\isgaservicePackage($packageId);

        //проверяем пакет
        if($package->isEmptyField("Id")){
            return "0541";
        }
        if( $package->getField("State") != "Done"){
            return "053";
        }

        return "<package capacity='".$package->getField("Capacity")."' id='".$package->getId()."' />";
    }

    /**
     * @param string $sessionId
     * @param string $packageId
     * @param int $part
     * @return string
     */
    public function GetPackage($sessionId, $packageId, $part){
        //проверяем идентификатор сессии
        if(!$this->_checkSessionId($sessionId)){
            return "0322";
        }

        //подгружаем пакет
        $package = new \Model\Entities\isgaservicePackage($packageId);

        //проверяем пакет
        if($package->isEmptyField("Id")){
            return "0541";
        }

        //Таблица с частями
        $partsTable = new \Model\Gateway\EntitiesTable("isgaservice_PackagePart");
        $part = $partsTable->getEntitiesByWhere(array(
            "fk_isgaservicePackage" => $packageId,
            "PartNumber" => $part
        ))->getObjectsArray();

        if(count($part) == 0)
            return "0542";

        $part = $part[0];

        //Открываем файл и возвращаем его содержимое
        //$logger =new \Model\Logger\Logger("service", true);
        //$logger->Log($_SERVER["DOCUMENT_ROOT"] . '/service_tmp/'.$part->getId().'.tmp');
        $filecontent = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/service_tmp/'.$part->getId().'.tmp');
        return $filecontent;
    }

    /**
     * @param string $sessionId
     * @param string $packageId
     * @return string
     */
    public function SetOk($sessionId, $packageId){
        $logger = new \Model\Logger\Logger("service",true);
        //проверяем идентификатор сессии
        if(!$this->_checkSessionId($sessionId)){
            return "0322";
        }

        try {
            //подгружаем пакет
            $package = new \Model\Entities\isgaservicePackage($packageId);

            //проверяем пакет
            if ($package->isEmptyField("Id")) {
                return "0541";
            }

            //Получаем все части пакета
            $partsTable = new \Model\Gateway\EntitiesTable("isgaservice_PackagePart");
            $parts = $partsTable->getAllEntities(null, null, array(
                "fk_isgaservicePackage" => $packageId
            ))->getObjectsArray();

            //Проходим по всем частям
            foreach ($parts as $part) {
                //Удаляем файлы
                @unlink($_SERVER["DOCUMENT_ROOT"] . '/service_tmp/' . $part->getId() . '.tmp');
                //Удаляем часть
                $part->delete(true);
            }
            //удаляем пакет
            $package->delete(true);
            return "";
        } catch(\Exception $e){

            $logger->Log("Ошибка: ".$e->getCode()." ".$e->getTraceAsString()." ".$e->getMessage());
        }
    }



    /**
     * @param $sessionId
     * @return bool
     */
    private function _checkSessionId($sessionId){
        //Проверяем если в бд есть такой идентификатор сессии
        $sesIdEntity = new \Model\Entities\isgaserviceSessionId($sessionId);
        return !$sesIdEntity->isEmptyField("Id");
    }


}