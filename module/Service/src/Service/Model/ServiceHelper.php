<?php
namespace Service\Model;
use Model\Entities\eiisLicenseStates;
use Model\Gateway\LodEntitiesTable;
use Model\Logger\Logger;

/**
 * Класс помощника для сервиса, преобразовывает сущности у нужному выходу
 * @package Service\Model
 */
class ServiceHelper{
    private $num = 0;
    private $filepath = "/home/f11/domains/f11";
//'e:\\WebServers\\home\\vuzi\\www\\';
    private $svidPeriod = [];
    private $current = 0;
    private $part = 0;
    //'f:\\WebServers\\home\\vuzi\\www\\'; //;
    //==========================================Обработка сущностей

    //список сущностей
    private $_correspondences = array(
        "EIIS.EDULEVELS" => array(
            "table" => "eiis_EduLevels",
            "fields" => array(
                "SYS_GUID"          => "Id",
                "ID"                => "Id",
                "NAME"              => "Name",
                "SHORTNAME"         => "ShortName",
                "EIIS_EDULEVEL_FK"  => "parentlevel"
            )
        ),
        "EIIS.EDUPROGRAM_TYPES" => array(
            "table" => "eiis_EduProgramTypes",
            "fields" => array(
                "SYS_GUID"                  => "Id",
                "ID"                        => "Id",
                "NAME"                      => "Name",
                "SHORTNAME"                 => "ShortName",
                "EIIS_EDUPROGRAM_TYPE_FK"   => ""
            )
        ),
        "ISGA.UGS_EDITIONS" => array(
            "table" => "isga_EnlargedGroupSpecialityEditions",
            "fields" => array(
                'ID'                            => "Id",
                'NAME'                          => "Name"
            )
        ),
        "ISGA.UGS" => array(
            "table" => "isga_EnlargedGroupSpecialities",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "STANDART" => "Standart",
                "ACTUAL" => "isActual"
            )
        ),
        "ISGA.GS" => array(
            "table" => "isga_GroupSpecialities",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "UGS_FK" => "fk_isgaEGS"
            )
        ),
        "ISGA.EDU_STANDARDS" => array(
            "table" => "isga_EduStandards",
            "fields" => array(
                "ID" => "Id",
                "NAME" => "Name",
                "ABBREVIATION" => "Abbreviation"
            )
        ),
        "ISGA.QUALIFICATIONS" => array(
            "table" => "isga_Qualifications",
            "fields" => array(
                "ID" => "Id",
                "Code" => "Code",
                "Name" => "Name",
                "Actual" => "IsActual"
            )
        ),
        "ISGA.EDUPROGRAMS" => array(
            "table" => "eiis_EduPrograms",
            "fields" => array(
                "ID" => "Id",
                "GS_FK" => "fk_eiisGS",
                "UGS_FK" =>	"fk_eiisUgs",
                "QUALIFICATION_FK" =>	"fk_isgaQualification",
                "EDU_STANDARD_FK" =>	"fk_eiisEduStandard"
            )
        ),
        //"ISGA.ADAPTERS",
        //"ISGA.SUBLEVELS",
        "ISGA.SCHOOLS" => array(
            "table" => "eiis_EducationalOrganizations",
            "fields" => array(
                "ID" =>	"Id",
                "LAW_POST_INDEX"=>	"LawPostIndex",
                "LAW_CITY_NAME"=>	"LawCity",
                "LAW_STREET"=>	"LawStreet",
                "LAW_HOUSE"=>	"LawHouse",
                "POST_INDEX"=>	"PostIndex",
                "TOWN_NAME"=>	"TownName",
                "STREET"=>	"Street",
                "HOUSE"=>	"House",
                "CONTACT_FIRST_NAME"=>	"ContactFirstName",
                "CONTACT_SECOND_NAME"=>	"ContactSecondName",
                "CONTACT_LAST_NAME"=>	"ContactLastName",
                "FOUNDERS"=>""
            )
        ),
        //"ISGA.SCHOOLS_ADDITIONAL",
        "ISGA.FOUNDER_TYPES" => array(
            "table" => "isga_FounderTypes",
            "fields" => array(
                "ID" =>	"Id",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.FOUNDERS"=> array(
            "table" => "isga_Founders",
            "fields" => array(
                "ID" =>	"Id",
                "TYPE_FK" =>	"fk_isgaFounderType",
                "ORGANIZATION_FULLNAME" =>	"OrganizationFullName",
                "ORGANIZATION_SHORTNAME" =>	"OrganizationShortName",
                "LASTNAME" =>	"LastName",
                "FIRSTNAME" =>	"FirstName",
                "PATRONYMIC" =>	"Patronymic",
                "PHONES" =>	"Phones",
                "FAXES" =>	"Faxes",
                "EMAILS" =>	"Emails",
                "OGRN" =>	"Ogrn",
                "INN" =>	"Inn",
                "KPP" =>	"Kpp",
                "L_ADDRESS" =>	"LawAddress",
                "L_ADDRESS_REGION_FK" =>	"fk_eiisLawRegion",
                "L_ADDRESS_DISTRICT" =>	"LawDistrict",
                "L_ADDRESS_TOWN" =>	"LawTown",
                "L_ADDRESS_STREET" =>	"LawStreet",
                "L_ADDRESS_HOUSE_NUMBER" =>	"LawHouseNumber",
                "L_ADDRESS_POSTAL_CODE" =>	"LawPostalCode",
                "P_ADDRESS"	 =>"PAddress",
                "P_ADDRESS_REGION_FK" =>	"fk_eiisPRegion",
                "P_ADDRESS_DISTRICT" =>	"PDistrict",
                "P_ADDRESS_TOWN" =>	"PTown",
                "P_ADDRESS_STREET" =>	"PStreet",
                "P_ADDRESS_HOUSE_NUMBER" =>	"PHouseNumber",
                "P_ADDRESS_POSTAL_CODE" =>	"PPostalCode"
            )
        ),
        "ISGA.FOUNDERS_EDU_INSTITUTIONS" => array(
            "table" => "isga_mtm_Founders_EducationalOrganizations",
            "fields" => array(
                "ID" =>	"Id",
                "EDU_INSTITUTION_FK" =>	"fk_eiisEducationalOrganization",
                "FONDER_FK" =>	"fk_isgaFounder"
            )
        ),
        "EIIS.ISGA.ACCREDITATION_KINDS"=> array(
            "table" => "isga_AccreditationKinds",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"Code",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.EDU_OBJECT_CHECK_STATUSES"=> array(
            "table" => "isga_EduObjectCheckStatuses",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.ORDER_DOCUMENT_KINDS"=> array(
            "table" => "isga_OrderDocumentKinds",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"Code",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.ORDER_DOCUMENT_STATUS"=> array(
            "table" => "isga_OrderDocumentStatuses",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"Code",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.ORDER_DOCUMENTS"=> array(
            "table" => "isga_OrderDocuments",
            "fields" => array(
                "ID"=>	"Id",
                "ORDERDOCUMENTKINDID"=>	"fk_isgaOrderDocumentKind",
                "ORDERDOCUMENTSTATUSID"	=>"fk_isgaOrderDocumentStatus",
                "NUMBER"=>	"Number",
                "SIGNDATE"=>	"DateSign"
            ),
            "dateFields" => array(
                "DateSign"
            )
        ),
        "ISGA.APPLICATION_TYPES" => array(
            "table" => "isga_ApplicationTypes",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name",
                "SHORT_NAME"=>	"ShortName"
            )
        ),
        "ISGA.APPLICATION_STATUSES"=> array(
            "table" => "isga_ApplicationStatuses",
            "fields" => array(
                "ID"	=>"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name",
                "PUBLISH_NAME"=>	"PublishName",
                "DESCRIPTION"=>	"Description",
                "PUBLISHED"=>	"Published"
            )
        ),
        "ISGA.APPLICATIONS"=> array(
            "table" => "isga_Applications",
            "fields" => array(
                "ID"=>	"Id",
                "REG_NUMBER"=>	"RegNumber",
                "SUBMISSION_DATE"=>	"DateSubmission",
                "REG_DATE"=>	"DateReg",
                "STATUS_FK"	=>"fk_isgaApplicationStatus",
                "APPLICATION_TYPE_FK"=>	"",
                "PREVIOUS_CERTIFICATE_FK"	=>"fk_isgaPreviousCertificate",
                "APPLICANT_PERSON_FULL_NAME"	=>"ApplicantPersonFullName",
                "APPLICANT_PERSON_FIRSTNAME"=>	"ApplicantPersonFirstName",
                "APPLICANT_PERSON_LASTNAME"=>	"ApplicantPersonLastName",
                "APPLICANT_PERSON_PATRONYMIC"=>	"ApplicantPersonPatronymic",
                "DECLARED_ORG_FORM_FK"=>	"fk_eiisEOProperty_Declared",
                "DECLARED_TYPE_FK"=>	"fk_eiisEOType_Declared",
                "DECLARED_KIND_FK"=>	"fk_eiisEOKind_Declared",
                "DECLARED_REGULAR_NAME"=>	"DeclaredRegularName",
                "DECLARED_SHORT_NAME"=>	"DeclaredShortName",
                "ACCEPTANCE_TRANSFER_ACT_NUMBER"=>	"AcceptanceTransferActNumber",
                "ACCEPTANCE_TRANSFER_ACT_DATE"=>	"DateAcceptanceTransferAct",
                "RECALL_APPLICATION_REG_NUMBER"	=>"RecallApplicationRegNumber",
                "RECALL_DATE"=>	"DateRecall",
                "RECALL_DOCUMENTS_SEND_DATE"=>	"DateRecallDocumentsSend",
                "LICENSEID"=>	"fk_eiisLicense"
            ),
            "function" => "applications",
            "dateFields" => array(
                "DateSubmission",
                "DateReg",
                "DateAcceptanceTransferAct",
                "DateRecall",
                "DateRecallDocumentsSend"
            )
        ),
        //"ISGA.APPLICATION_STATISTICS",
        //"ISGA.APPLICATION_HISTORY",
        "ISGA.APPLICANTS"=> array(
            "table" => "isga_mtm_Application_Organizations",
            "fields" => array(
                "ID" => "Id",
                "APPLICATION_FK" => "fk_isgaApplication",
                "SCHOOL_FK" => "fk_eiisEducationalOrganization",
                "IS_DECLARER" => ""
            )
        ),
        "ISGA.APPLICATIONS_ORDERS"=> array(
            "table" => "isga_Applications",
            "fields" => array(
                "ID" => "Id",
                "APPLICATION_FK" => "Id",
                "ORDER_DOCUMENT_FK" => "fk_isgaOrderDocument"
            )
        ),
        "ISGA.CERTIFICATE_TYPES"=> array(
            "table" => "isga_CertificateTypes",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name"
            )
        ),
        "ISGA.CERTIFICATE_STATUSES"=> array(
            "table" => "isga_CertificateStatuses",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name"
            )
        ),
        "ISGA.CERTIFICATES"=> array(
            "table" => "isga_Certificates",
            "fields" => array(
                "ID"=>	"Id",
                "REESTR_ID"=>	"GUID",
                "REG_NUMBER"=>	"RegNumber",
                "ISSUE_DATE"=>	"DateIssue",
                "END_DATE"=>	"DateEnd",
                "FORM_NUMBER"=>	"FormNumber",
                "SERIAL_NUMBER"	=>"SerialNumber",
                "INSTITUTION_FK"=>	"fk_eiisEducationalOrganization",
                "INSTITUTION_FULL_NAME"	=>"EOFullName",
                "INSTITUTION_SHORT_NAME"=>	"EOShortName",
                "INN"=>	"Inn",
                "OGRN"=> "",
                "APPLICATION_FK"=>	"fk_oisgaApplication",
                "STATUS_FK"=>	"fk_isgaCertificateStatus",
                "TYPE_FK"=>	"fk_isgaCertificateType",
                "INSTITUTION_L_ADDRESS"=>	"EOAddressL",
                "CONTROL_ORGAN_HEAD_FULL_NAME"=>	"ControlOrganHeadFullName",
                "CONTROL_ORGAN_HEAD_POST"=>	"ControlOrganHeadPost",
                "DECISION_DOCUMENT_FK"=>	"fk_isgaOrderDocument",
                "ORDER_DOCUMENT_NUMBER"=>	"OrderDocumentNumber",
                "ORDER_DOCUMENT_SIGN_DATE"=>	"DateOrderDocumentSign",
                "ORDER_DOCUMENT_KIND_FK"=>	"fk_OrderDocumentKind",
                "ISSUE_TO_APPLICANT_DATE"=>	"DateIssueToApplicant",
                "CHECK_DATE"=>	"DateCheck",
                "READY_TO_ISSUE_DATE"=>	"DateReadyToIssue",
                "PRINT_DATE"=>	"DatePrint",
                "CHECK_STATUS_FK"=>	"fk_isgaCheckStatus",
                "INSTITUTION_P_ADDRESS" => "",
                "EDUCATION_TYPE" => "",
                "NEW_UPDATE_DATE" => ""
            ),
            "function" => "certificates",
            "dateFields" => array(
                "DateIssue",
                "DateEnd",
                "DateOrderDocumentSign",
                "DateIssueToApplicant",
                "DateCheck",
                "DateReadyToIssue",
                "DatePrint"
            )
            //,"onlyOur" => true
        ),
        "ISGA.CERTIFICATE_SUPPLEMENT_STATUSES"=> array(
            "table" => "isga_CertificateSupplementStatuses",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name"
            )
        ),
        "ISGA.CERTIFICATE_SUPPLEMENTS"=> array(
            "table" => "isga_CertificateSupplements",
            "fields" => array(
                "ID"=>	"Id",
                "CERTIFICATE_FK"=>	"fk_isgaCertificate",
                "INSTITUTION_FK"=>	"fk_eiisEducationalOrganization",
                "INSTITUTION_FULL_NAME"	=>"EOFullName",
                "INSTITUTION_SHORT_NAME"=>	"EOShortName",
                "OGRN" => "",
                "INN"=>	"EOInn",
                "NUMBER"=>	"Number",
                "STATUS_FK"=>	"fk_isgaCertificateSupplementStatus",
                "DECISION_DOCUMENT_FK"=>	"fk_isgaOrderDocument",
                "ISSUE_DATE"=>	"DateIssue",
                "FORM_NUMBER"	=>"FormNumber",
                "SERIAL_NUMBER"	=>"SerialNumber",
                "INSTITUTION_L_ADDRESS"	=>"EOAddressL",
                "CONTROL_ORGAN_HEAD_FULL_NAME"	=>"ControlOrganHeadFullName",
                "CONTROL_ORGAN_HEAD_POST"=>	"ControlOrganHeadPost",
                "ORDER_DOCUMENT_NUMBER"=>	"OrderDocumentNumber",
                "ORDER_DOCUMENT_SIGN_DATE"	=>"DateOrderDocumentSign",
                "APPLICATION_FK"	=>"fk_isgaApplication",
                "ORDER_DOCUMENT_KIND_FK"=>	"fk_isgaOrderDocumentKind",
                "ACCREDITATION_KIND_FK"	=>"fk_isgaAccreditationKind",
                "ISSUE_TO_APPLICANT_DATE"	=>"DateIssueToApplicant",
                "CHECK_DATE"=>	"DateCheck",
                "PRINT_DATE"	=>"DatePrint",
                "CHECK_STATUS_FK"=>	"fk_isgaCheckStatus",
                "READY_TO_ISSUE_DATE"=>	"DateReadyToIssue",
                "INSTITUTION_P_ADDRESS" => "",
                "INSTITUTION_P_ADDRESS_POSTAL_CODE" => "",
                "EDUCATION_TYPE" => ""
            ),
            "function" => "certificate_supplements",
            "dateFields" => array(
                "DateIssue",
                "DateOrderDocumentSign",
                "DateIssueToApplicant",
                "DateCheck",
                "DatePrint",
                "DateReadyToIssue"
            )
            //,"onlyOur" => true
        ),
        "ISGA.ACCREDITED_PROGRAMS"=> array(
            "table" => "isga_AccreditedPrograms",
            "fields" => array(
                "ID" => "Id",
                "APPLICATION_FK" => "fk_oisgaApplication",
                "LICENSED_PROGRAM_FK" => "fk_eiisLicensedProgram",
                "CERTIFICATE_SUPPLEMENT_FK" => "fk_isgaCertificateSupplement",
                "SCHOOL_FK" => "fk_eiisEducationalOrganization",
                "CODE" => "Code",
                "NAME" => "Name",
                "EDUPROGRAM_FK" => "fk_eiisEduProgram",
                "EDUPROGRAM_TYPE_FK" => "fk_eiisEduProgramType",
                "OKSO_CODE" => "OksoCode",
                "START_YEAR" => "StartYear",
                "EDU_NORMATIVE_PERIOD" => "EduNormativePeriod",
                "FORM_DEGREE" => "FormDegree",
                "QUALIFICATION_CODE" => "",
                "QUALIFICATION_NAME" => "",
                "QUALIFICATION_FK" => "fk_isgaQualification",
                "GS_CODE" => "",
                "GS_NAME" => "",
                "GS_FK" => "fk_isgaGroupSpeciality",
                "UGS_CODE" => "",
                "UGS_NAME" => "",
                "UGS_FK" => "fk_isgaEnlargedGroupSpeciality",
                "IS_ACCREDITED" => "",
                "EDULEVEL_NAME" => "",
                "EDUSUBLEVEL_NAME" => "",
                "EDULEVEL_FK" => "",
                "IS_UGS" => "",
                "SUSPENDED_PARTIALLY" => ""
            ),
            "function" => "accredited_programs"
        ),
        "EIIS.ISGA.APPLICATION_REASONS"=> array(
            "table" => "isga_ApplicationReasons",
            "fields" => array(
                "SYS_GUID"=> "Id",
                "NAME"=>	 "Name"
            )
        ),

        // ========================== ЛОД сущности
        "ISLOD.EDULEVELS" => array(
            "table" => "eiis_EduLevels",
            "fields" => array(
                "SYS_GUID" => "Id",
                "ID" => "Code",
                "NAME" => "Name",
                "SHORTNAME" => "ShortName",
                "EIIS_EDULEVEL_FK" => "Id",
                "ACTUAL" => ""
            ),
            "function" => "islod_edulevels"
        ),

        "ISLOD.EDU_PROGRAM_TYPES" => array(
            "table" => "eiis_EduProgramTypes",
            "fields" => array(
                "SYS_GUID" => "Id",
                "ID" => "EduProgramSubtype",
                "NAME" => "Name",
                "SHORTNAME" => "ShortName"
            )
        ),

        "ISLOD.UGS" => array(
            "table" => "isga_EnlargedGroupSpecialities",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "STANDART" => "Standart",
                "ACTUAL" => "isActual"
            ),
            "function" => "islod_ugs"
        ),

        "ISLOD.NEW_EDUPROGRAMS" => array(
            "table" => "eiis_EduPrograms",
            "fields" => array(
                "SYS_GUID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "EDULEVELFK" => "fk_eiisEduLevels",
                "EDUPROGRAMTYPEFK" => "fk_eiisEduProgramType",
                "STANDARD_TYPE" => "fk_eiisEduProgramType",
                "UGSCODE" => "UgsCode",
                "UGSNAME" => "UgsName",
                "UGS_FK" => "fk_eiisUgs",
                "PERIOD" => "Period",
                "ACTUAL" => ""
            ),
            "function" => "islod_eduprograms"
        ),

        "ISLOD.BASIS_TYPE_DOC" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "idField" => "id",
            "fields" => array(
                "SYS_GUID" => "id",
                "ID" => "code",
                "NAME" => "title"
            )
        ),

        "ISLOD.LICENSE_STATUSES" => array(
            "table" => "eiis_LicenseStates",
            "idField" => "Id",
            "fields" => array(
                "SYS_GUID" => "LodCode",
                "NAME" => "Name"
            )
        ),

        "ISLOD.LICENSE_SIGN_STATUSES" => array(
            "db"    => "lod",
            "table" => "eiis_sign_states",
            "idField" => "id",
            "fields" => array(
                "SYS_GUID" => "origin_name",
                "NAME" => "russian_name"
            )
        ),



        "ISLOD.LICENSES" => array(
            "table" => "eiis_Licenses",
            "where" => [
                "fk_eiisLicenseState IS NOT NULL AND fk_eiisLicenseState !='' AND fk_eiisLicenseState != '34F4DBDB89FA94D119AF0C1ABDD61D43' AND fk_eiisEducationalOrganization != '' AND fk_eiisEducationalOrganization IS NOT NULL"
            ],
            "fields" => array(
                "SYS_GUID"                  => "Id",
                "LICENSE_REG_NUM"           => "LicenseRegNum",
                "SERDOC"                    => "SerDoc",
                "NUMDOC"                    => "NumDoc",
                "LICENSE_STATUSESFK"        => "",
                "LICENSE_SIGN_STATUSESFK"   => "lod_signStateCode",
                "IS_TERMLESS"               => "IsTermless",
                "DATA_END"                  => "DateEnd",
                "RBD_SCHOOLFK"              => "fk_eiisEducationalOrganization",
                "SCHOOLNAME"                => "EOName",
                "SHORTNAME"                 => "EOShortName",
                "LAWADDRESS"                => "EOLawAddress",
                "GOSREGNUM"                 => "EOGosRegNum",
                "INN"                       => "EOInn",
                "BASISTYPEDOCFK"            => "",
                "NUMLICDOC"                 => "NumLicDoc",
                "DATELICDOC"                => "DateLicDoc",
                "MANAGERFIO"                => "ManagerFio",
                "POST"                      => "Post",
                "LICENSESTATFK"             => "",
                "LICENSEOLDFK"              => "",
                "DOC_STOPFK"                => "",
                "REASON_OF_SUSPENSION"      => "",
                "DATE_OF_SUSPENSION"        => "",
                "ADM_SUSP_ORDERS"           => "",
                "ORG_SUSP_DECISIONS"        => "",
                "COURT_REVOKING_DECISIONS"  => "",
                "DOUBLE"                    => "0",
                "NUM_DATE_DUPL_ISSUE"       => "",
                "DATE_DUPL"                 => "",
                "COPY_LICENSE_STATFK"       => ""
            ),
            "dateFields" => array(
                "DateEnd",
                "DateLicDoc"
            ),
            "function" => "islod_licenses"
        ),

        "ISLOD.SUPPLEMENT_STATUSES" => array(
            "table" => "eiis_LicenseSupplementStates",
            "fields" => array(
                "SYS_GUID" => "lodCode",
                "NAME" => "Name"
            )
        ),

        "ISLOD.SUPPLEMENT_SIGN_STATUSES" => array(
            "db"    => "lod",
            "table" => "eiis_sign_states",
            "fields" => array(
                "SYS_GUID" => "origin_name",
                "NAME" => "russian_name"
            )
        ),

        "ISLOD.SUPPLEMENTS" => array(
            "table" => "eiis_LicenseSupplements",
            "where" => [
                "fk_eiisLicenseSupplementState IS NOT NULL AND fk_eiisLicenseSupplementState !='' AND fk_eiisLicenseSupplementState != '4E09AEA87399AFFD9EC3FE6833DB4B36' AND fk_eiisBranch  != '' AND fk_eiisBranch IS NOT NULL"
            ],
            "fields" => array(
                "SYS_GUID"                      => "Id",
                "LICENSEFK"                     => "fk_eiisLicense",
                "NUMBER"                        => "Number",
                "SER_DOC"                       => "SerDoc",
                "NUMBER_DOC"                    => "NumDoc",
                "SUPPLEMENT_STATUSFK"           => "",
                "SUPPLEMENT_SIGN_STATUSESFK"    => "lod_signStateCode",
                "DATE_END"                      => "DateEnd",
                "MANAGERFIO"                    => "ManagerFio",
                "POST"                          => "Post",
                "BANCHFK"                       => "fk_eiisBranch",
                "SCHOOLNAME"                    => "EOName",
                "SHORTNAME"                     => "EOShortName",
                "LAWADDRESS"                    => "EOLawAddress",
                "IMPL_ADDR"                     => "",
                "BASISTYPEDOCFK"                => "lod_basisForLicensing",
                "NUMLICDOC"                     => "NumLicDoc",
                "DATELICDOC"                    => "DateLicDoc",
                "LICENSE_STATFK"                => "fk_eiisLicenseState",
                "SUPPLEMENTOLDFK"               => "fk_eiisLicenseSupplementOld",
                "DOC_STOPFK"                    => "",
                "REASON_OF_SUSPENSION"          => "",
                "DATE_OF_SUSPENSION"            => "",
                "DOUBLE"                        => "Duplicate",
                "DUPLICATE_ORDER"               => "DuplicateOrder",
                "DATE_DUPL"                     => "",
                "DUP_LIC_STATFK"                => "fk_eiisLicenseState_Duplicate"
            ),
            "dateFields" => array(
                "DateEnd",
                "DateLicDoc"
            ),
            "function" => "islod_supplements"
        ),

        "ISLOD.LICENSED_PROGRAMS" => array(
            "db"    => "lod",
            "table" => "EducationProgram",
            "fields" => array(
                "SYS_GUID"              => "id",
                "CODE"                  => "",
                "NAME"                  => "",
                "EDULEVELFK"            => "educationLevelId",
                "EDUPROGRAMTYPEFK"      => "",
                "STANDARD_TYPE"         => "",
                "PERIOD"                => "",
                "EDUPROGRAMFK"          => "EduProgramsID",
                "NEW_EDUPROGRAMFK"      => "EduProgramsID",
                "SUPPLEMENTSFK"         => "LicenseSupplementID"
            ),
            "function" => "islod_licensed_programs"
        ),

        "ISLOD.QUALIFICATIONS" => array(
            "table" => "isga_Qualifications",
            "fields" => array(
                "SYS_GUID" => "Id",
                "CODE"     => "Code",
                "NAME"     => "Name",
                "ACTUAL"   => "IsActual"
            )
        ),

        "ISLOD.LICENSED_PROGRAMS_2_QUALIFICATIONS" => array(
            "db"    => "lod",
            "table" => "EducationProgramToQualification",
            "idField" => "id",
            "fields" => array(
                "SYS_GUID"          => "id",
                "EDUPROGRAMFK"      => "educationProgramId",
                "QUALIFICATIONFK"   => "qualificationId"
            )
        ),

        "ISLOD.SCHOOL_KINDS" => array(
            "table" => "eiis_EducationalOrganizationKinds",
            "fields" => array(
                "SYS_GUID"              => "Id",
                "SCHOOL_KINDCODE"       => "Code",
                "SCHOOL_KINDNAME"       => "Name",
                "SCHOOL_SUBTYPECODEFK"  => "fk_eiisEducationalOrganizationType"
            )
        ),

        "ISLOD.SCHOOL_SUBTYPES" => array(
            "table" => "eiis_EducationalOrganizationTypes",
            "fields" => array(
                "SYS_GUID"  => "Id",
                "SCHOOL_SUBTYPENAME" => "Name"
            )
        ),

        "ISLOD.RBDC_SCHOOL_SUBPROPERTIES" => array(
            "table" => "eiis_EducationalOrganizationProperties",
            "where" => [
                "parent IS NOT NULL"
            ],
            "fields" => array(
                "SYS_GUID"              => "Id",
                "SCHOOLPROPERTYCODE"    => "Code",
                "SCHOOLPROPERTYNAME"    => "Name",
                "SCHOOLPROPERTYFK"      => "parent"
            )
        ),

        "ISLOD.RBDC_SCHOOLPROPERTIES" => array(
            "table" => "eiis_EducationalOrganizationProperties",
            "where" => [
                "parent IS NULL"
            ],
            "fields" => array(
                "SYS_GUID"              => "Id",
                "SCHOOLPROPERTYCODE"    => "Code",
                "SCHOOLPROPERTYNAME"    => "Name"
            )
        ),

        "EIIS.SCHOOLPROPERTIES" => array(
            "table" => "eiis_EducationalOrganizationProperties",
            "fields" => array(
                "SYS_GUID"              => "Id",
                "SCHOOLPROPERTYCODE"    => "Code",
                "SCHOOLPROPERTYNAME"    => "Name",
                "SCHOOLPROPERTYFK"      => "parent"
            )
        ),

        "ISLOD.RBDC_REGIONS" => array(
            "table" => "eiis_Regions",
            "fields" => array(
                "SYS_GUID"      => "Id",
                "REGION"        => "Code",
                "REGIONNAME"    => "Name",
                "FED_OKR_FK"    => "fk_eiisFedOkr"
            )
        ),

        "ISLOD.FED_OKR_RF" => array(
            "table" => "eiis_FedOkr",
            "fields" => array(
                "SYS_GUID"  => "Id",
                "NAME"      => "Name",
                "SHORTNAME" => "ShortName"
            )
        ),

        "ISLOD.SCHOOL_STATUS" => array(
            "db"    => "lod",
            "table" => "catalog_organization",
            "fields" => array(
                "SYS_GUID" => "origin_name",
                "NAME" => "russian_name"
            )
        ),

        "ISLOD.RBD_SCHOOLS" => array(
            "table" => "eiis_EducationalOrganizations",
            "where" => [
                "lod_stateCode != 'DRAFT'"
            ],
            "fields" => array(
                "SYS_GUID"              => "Id",
                "REGULARNAME"           => "RegularName",
                "SCHOOLNAME"            => "FullName",
                "SHORTNAME"             => "ShortName",
                "SCHOOLPROPERTYFK"      => "",
                "SCHOOLSUBPROPERTYFK"   => "",
                "SCHOOLTYPEFK"          => "fk_eiisEducationalOrganizationType",
                "SCHOOLKINDFK"          => "fk_eiisEducationalOrganizationKind",
                "BRANCH"                => "Branch",
                "PARENTSCHOOLFK"        => "fk_eiisParentEducationalOrganization",
                "RENAME"                => "",
                "GOSREGNUM"             => "GosRegNum",
                "INN"                   => "Inn",
                "KPP"                   => "Kpp",
                "REGION_LOCATION"       => "fk_eiisRegion",
                "LAWADDRESS"            => "LawAddress",
                "ADDRESS"               => "Address",
                "CITY"                  => "",
                "IMPL_ADDR"             => "",
                "CHARGEPOSITION"        => "",
                "CHARGEFIO"             => "ChargeFio",
                "PHONES"                => "Phones",
                "FAXS"                  => "Faxes",
                "MAILS"                 => "Mails",
                "WWW"                   => "Www",
                "REGION_COMPETENCE"     => "",
                "ISSTRONG"              => "isStrong",
                "ISRELIGION"            => "isReligion",
                "INSIDE_SCHOOL"         => "",
                "OUTSIDE_SCHOOL"        => "",
                "STARTACTIVITYDATE"     => "",
                "STOPACTIVITYDATE"      => "",
                "STOP_STATFK"           => "",
                "STATUSFK"              => "lod_stateCode",
                "COMMENT"               => "",
                "ISFU"                  => "federalUniversity",
                "ISNRU"                 => "nationalResearchUniversity"
            ),
            "function" => "islod_schools"
        ),

        "ISLOD.LICENSE_STAT_SUBTYPES" => array(
            "db"    => "lod",
            "table" => "LicensingProcedure",
            "fields" => array(
                "SYS_GUID"  => "id",
                "ID"        => "code",
                "NAME"      => "title"
            )
        ),

        "ISLOD.STATE_STATUSES" => array(
            "db"    => "lod",
            "table" => "catalog_deal",
            "fields" => array(
                "SYS_GUID"  => "origin_name",
                "ID"        => "id",
                "NAME"      => "russian_name"
            )
        ),

        "EIIS.LICENSE_STATS" => array(
            "db"    => "lod",
            "table" => "LicenseRequest",
            "idField" => "id",
            "fields" => array(
                "SYS_GUID"          => "id",
                "APPLICANT"         => "organizationId",
                "SCHOOLFK"          => "organizationId",
                "STAGE"             => "",
                "STATUS"            => "",
                "LICENSING_PROCEDURE" => "licensingProcedureId",
                "REGNUM_TEXT"       => "number",
                "REGDATE"           => "fillingDate",
                "EXPERT"            => "",
                "SCHOOLNAME"        => "",
                "SHORTNAME"         => "",
                "SCHOOLPROPERTYFK"  => "",
                "SCHOOLTYPEFK"      => "",
                "SCHOOLKINDFK"      => "",
                "LAWADDRESS"        => "",
                "ADDRESS"           => "",
                "GOSREGNUM"         => "",
                "INN"               => "",
                "KPP"               => "",
                "BENEFICIARY"       => "",
                "BEN_SHORTNAME"     => "",
                "BENEFECIARY_ADDRESS" => "",
                "BEN_IMPL_ADDRESS"  => "",
                "OLD_LICENSEFK"     => "",
                "RENEW_LICENSEFK"   => ""
            ),
            "function" => "islod_license_request"
        ),

        "ISLOD.LICENSE_STAT" => array(
            "db"    => "lod",
            "table" => "LicenseRequest",
            "idField" => "id",
            "fields" => array(
                "SYS_GUID"          => "id",
                "APPLICANT"         => "organizationId",
                "SCHOOLFK"          => "organizationId",
                "STAGE"             => "",
                "STATUS"            => "",
                "LICENSING_PROCEDURE" => "licensingProcedureId",
                "REGNUM_TEXT"       => "number",
                "REGDATE"           => "fillingDate",
                "EXPERT"            => "",
                "SCHOOLNAME"        => "",
                "SHORTNAME"         => "",
                "SCHOOLPROPERTYFK"  => "",
                "SCHOOLTYPEFK"      => "",
                "SCHOOLKINDFK"      => "",
                "LAWADDRESS"        => "",
                "ADDRESS"           => "",
                "GOSREGNUM"         => "",
                "INN"               => "",
                "KPP"               => "",
                "BENEFICIARY"       => "",
                "BEN_SHORTNAME"     => "",
                "BENEFECIARY_ADDRESS" => "",
                "BEN_IMPL_ADDRESS"  => "",
                "OLD_LICENSEFK"     => "",
                "RENEW_LICENSEFK"   => ""
            ),
            "function" => "islod_license_request"
        ),

        "ISLOD.NOTICE_TYPES" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "idField" => "id",
            "where" => [
                "parent" => "20a4674be28c4b36ad69de14a0e4c8df"
            ],
            "fields" => array(
                "SYS_GUID"  => "id",
                "ID"        => "code",
                "NAME"      => "title"
            )
        ),

        "ISLOD.NOTICES" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent = '20a4674be28c4b36ad69de14a0e4c8df')"
            ],
            "fields" => array(
                "SYS_GUID"          => "id",
                "NUMBER"            => "registrationNumber",
                "NOTICE_TYPE"       => "documentTypeId",
                "STATUS"            => "stateCode",
                "LICENSE_STATFK"    => "",
                "SENDING_DATE"      => "sendDate",
                "RECIEVING_DATE"    => "receiveDate"
            ),
            "function" => "islod_notices",
            "dateFields" => array(
                "sendDate",
                "receiveDate"
            )
        ),

        "ISLOD.DOCUMENT_STATUSES" => array(
            "db"    => "lod",
            "table" => "catalog_document",
            "idField" => "id",
            "fields" => array(
                "SYS_GUID"  => "id",
                "CODE"      => "origin_name",
                "NAME"      => "russian_name"
            )
        ),

        "ISLOD.DOCUMENT_KINDS" => array(
            "db"    => "lod",
            "table" => "CatalogItem",
            "idField" => "id",
            "where" => [
                "catalog" => "documentDecisionTypes"
            ],
            "fields" => array(
                "SYS_GUID"  => "id",
                "NAME"      => "title"
            )
        ),

        "ISLOD.ORDER_RESOLUTIONS" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "idField" => "id",
            "where" => [
                "parent" => ["177fabe881474796b37b30c47969e93f", "e6b56dff077f40bda8c1c9d67e410f85"]
            ],
            "fields" => array(
                "SYS_GUID"  => "id",
                "ID"        => "code",
                "NAME"      => "title"
            )
        ),

        "ISLOD.ORDERS" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent IN('177fabe881474796b37b30c47969e93f', 'e6b56dff077f40bda8c1c9d67e410f85'))"
            ],
            "fields" => array(
                "SYS_GUID"          => "id",
                "ORDER_NUMBER"      => "registrationNumber",
                "ORDER_DATE"        => "registrationDate",
                "RESOLUTION"        => "documentTypeId",
                "STATUS"            => "stateCode",
                "ORDER_DISPOSAL"    => "",
                "STATFK"            => ""

            ),
            "function" => "islod_orders",
            "dateFields" => array(
                "registrationDate"
            )
        ),

        "ISLOD.NOTICE_WITH_STAT" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent = '20a4674be28c4b36ad69de14a0e4c8df')",
                "dealId IS NOT NULL"
            ],
            "function" => "islod_notice_with_stat",
            "fields" => array(
                "SYS_GUID" => "",
                "LICENSE_STATFK" => "",
                "NOTICESFK" => "id"
            )
        ),

        "ISLOD.ADMISSION_TYPES" => array(
            "db"    => "lod",
            "table" => "CatalogItem",
            "idField" => "id",
            "where" => [
                "catalog" => "admissionTypes"
            ],
            "fields" => array(
                "SYS_GUID"  => "id",
                "NAME"      => "title"
            )
        ),

        "ISLOD.ORDER_WITH_STAT" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent IN('177fabe881474796b37b30c47969e93f', 'e6b56dff077f40bda8c1c9d67e410f85'))",
                "dealId IS NOT NULL"
            ],
            "function" => "islod_order_with_stat",
            "fields" => array(
                "SYS_GUID" => "",
                "LICENSE_STATFK" => "",
                "ORDERSFK" => "id"
            )
        ),

        "ISLOD.EMPLOYEES" => array(
            "db"    => "lod",
            "table" => "Lod_User",
            "idField" => "uuid",
            "fields" => array(
                "SYS_GUID" => "uuid",
                "FIRSTNAME" => "firstName",
                "MIDDLENAME" => "middleName",
                "LASTNAME" => "lastName",
                "INITIALS" => ""
            )
        ),

        "ISLOD.HOLIDAYS" => array(
            "db"    => "lod",
            "table" => "Calendar",
            "idField" => "id",
            "where" => [
                "status" => "free"
            ],
            "fields" => array(
                "SYS_GUID" => "id",
                "NAME" => "status",
                "DATE" => "date"
            )
        ),

        "ISLOD.SCHOOLS_ADDITIONAL" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "where" => [
                "lod_stateCode != 'DRAFT'"
            ],
            "fields" => array(
                "ID"                => "Id",
                "IS_GOS"            => "",
                "PROPERTY"          => "",
                "SCHOOL_CATEGORY"   => "",
                "SCHOOL_AGR_TYPE"   => "",
                "LICENSE_INFO"      => "",
                "LICENSE_STATUS"    => ""
            ),
            "function" => "islod_schools_additional",
        ),

        "ISLOD.APPLICATION_STATISTICS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lr.id, lr.organizationId, mlr.licenseReasonId,
count(*) as total,
COUNT(CASE WHEN d.stateCode = 'REFUSED' THEN 1 END) AS refused,
CASE
   when mlr.licenseReasonId IN('518dfabe3375431684b71d2faa5c7564', 'a382cea16f1c4cf9a729d4f8d0d8a8a0') then 'получение лицензии, переоформление при преобразовании'
   when mlr.licenseReasonId IN('0ba32b48a6384b5c95dcb630f34c930d', '2b6b9edcdf394ea9b3b67c9ac8a23b7e', '33b935940d1a4c5383ee72accbdf4ee9', '3e5b5f379598446ea2833f4acd2ba01b', 'c83201802b474c4393305b16b7c05375', 'f9893ac3e72e43b59101a8270f9eaacf', 'fb71a4f02e7d4f07be60fcc43fe2e9a2', 'fd77ff8629184ae89d18858d2b4d7a50', '08f4049094984955ab29d17d6f178d17', '317c24a9fb7c42b88400432d210a01c4', '469c8a48d80a4fe4912d834143de72ee') then 'переоформление 10 дней'
   when mlr.licenseReasonId IN('6d8c38b1ab79486eb8f826943b723f4c', '9ccbaed80f8248c9b1af9ff6f5bf02c9', 'aac9fc11db3d4682a3e80a64dabc1ede') then 'переоформление 45 дней'
   when mlr.licenseReasonId IN('1c3acd333a204d5288277d2c2fd3509b', 'efb3b9bf460c4a8aae48b9dd5e5d5d84', '34bee06f79144d5584668ba9fad809f8') then 'выдача дубликата, копии'
   when mlr.licenseReasonId IN('42807cbc9a9c419ba7ebc374652ed68a', 'b4bfaa7993784256890dc19a8deb7e88') then 'прекращение од'
   ELSE 'не определено'
END as indicator
FROM f11_mon_lod.LicenseRequest lr
inner join f11_mon_lod.LicenseReasonToLicenseRequest mlr on mlr.licenseRequestId = lr.id
left join f11_mon_lod.Deal d on d.id = lr.dealId
group by lr.id, indicator",
            "count" => "SELECT count(*) as count
FROM
(SELECT lr.id, lr.organizationId, mlr.licenseReasonId,
CASE
   when mlr.licenseReasonId IN('518dfabe3375431684b71d2faa5c7564', 'a382cea16f1c4cf9a729d4f8d0d8a8a0') then 'получение лицензии, переоформление при преобразовании'
   when mlr.licenseReasonId IN('0ba32b48a6384b5c95dcb630f34c930d', '2b6b9edcdf394ea9b3b67c9ac8a23b7e', '33b935940d1a4c5383ee72accbdf4ee9', '3e5b5f379598446ea2833f4acd2ba01b', 'c83201802b474c4393305b16b7c05375', 'f9893ac3e72e43b59101a8270f9eaacf', 'fb71a4f02e7d4f07be60fcc43fe2e9a2', 'fd77ff8629184ae89d18858d2b4d7a50', '08f4049094984955ab29d17d6f178d17', '317c24a9fb7c42b88400432d210a01c4', '469c8a48d80a4fe4912d834143de72ee') then 'переоформление 10 дней'
   when mlr.licenseReasonId IN('6d8c38b1ab79486eb8f826943b723f4c', '9ccbaed80f8248c9b1af9ff6f5bf02c9', 'aac9fc11db3d4682a3e80a64dabc1ede') then 'переоформление 45 дней'
   when mlr.licenseReasonId IN('1c3acd333a204d5288277d2c2fd3509b', 'efb3b9bf460c4a8aae48b9dd5e5d5d84', '34bee06f79144d5584668ba9fad809f8') then 'выдача дубликата, копии'
   when mlr.licenseReasonId IN('42807cbc9a9c419ba7ebc374652ed68a', 'b4bfaa7993784256890dc19a8deb7e88') then 'прекращение од'
   ELSE 'не определено'
END as indicator
FROM f11_mon_lod.LicenseRequest lr
inner join f11_mon_lod.LicenseReasonToLicenseRequest mlr on mlr.licenseRequestId = lr.id
group by lr.id, indicator) t",
            "fields" => array(
                "ID"                    => "",
                "SCHOOL_FK"             => "",
                "INDICATOR"             => "",
                "VALUE_1_FILED"         => "",
                "VALUE_2_FAILURE"       => "",
                "VALUE_3_REVOCATION"    => ""
            ),
            "function" => "islod_application_statistics",
        ),

        "ISLOD.SCHOOLS_PROGRAMS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lp.*, ep.*, st.Name as StandartName, q.Name as QualName, el.Name as ELName
FROM f11_mon_lod.EducationProgram lp
LEFT JOIN f11_mon.eiis_EduPrograms ep ON ep.Id = lp.EduProgramsID
LEFT JOIN f11_mon.isga_EduStandards st ON st.Id = ep.fk_eiisEduStandard
LEFT JOIN f11_mon_lod.EducationProgramToQualification epq ON lp.id = epq.educationProgramId
LEFT JOIN f11_mon.isga_Qualifications q ON q.Id = epq.qualificationId
LEFT JOIN f11_mon.eiis_EduLevels el ON el.Id = ep.fk_eiisEduLevels WHERE lp.LicenseSupplementID IN (
    SELECT s.Id
    FROM f11_mon.eiis_LicenseSupplements s
    WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)",
            "count" => "SELECT count(*) as count
FROM f11_mon_lod.EducationProgram lp
WHERE lp.LicenseSupplementID IN (
   SELECT s.Id
   FROM f11_mon.eiis_LicenseSupplements s
   WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)",
            "fields" => array(
                "ID" => "",
                "SCHOOL_FK" => "",
                "SUPPLEMENT_FK" => "",
                "LICENSED_PROGAM_FK" => "",
                "EDUPROGRAM_FK" => "",
                "LICENSE_STATUS" => "Лицензирована",
                "STANDART" => "",
                "CODE" => "",
                "NAME" => "",
                "QUALIFICATION" => "",
                "EDULEVEL_FK" => "",
                "EDULEVEL_NAME" => "",
                "COMMENT" => "",
                "UGS_CODE" => "",
                "UGS_NAME" => "",
                "UGS_FK" => ""
            ),
            "function" => "islod_schools_programs",
        ),

        "ISLOD.SCHOOLS_EDUFORMS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lp.organizationId, ep.fk_eiisEduProgramType,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then 'ВО - бакалавриат'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then 'ВО - специалитет'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then 'ВО - магистратура'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then 'ВО - подготовка кадров высшей квалификации'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then 'Дополнительное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then 'Среднее профессиональное образование'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then 'Начальное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then 'Профессиональное обучение'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then 'Дополнительное образование детей и взрослых'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then 'Общее образование'

   when ep.fk_eiisEduProgramType IN('13D0A5AA05D64AF395A3F1E14F910BC5') then 'Неопределенная форма обучения (ВПО)'

   ELSE 'Неопределенная форма обучения'
END as ef_Name,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then '01'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then '02'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then '03'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then '04'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then '05'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then '06'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then '07'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then '08'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then '09'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then '10'
   when ep.fk_eiisEduProgramType IN('13D0A5AA05D64AF395A3F1E14F910BC5') then '11'
   ELSE '12'
END as ef_Id

FROM f11_mon_lod.EducationProgram lp
LEFT JOIN f11_mon.eiis_EduPrograms ep ON ep.Id = lp.EduProgramsID
WHERE lp.LicenseSupplementID IN (
   SELECT s.Id
   FROM f11_mon.eiis_LicenseSupplements s
   WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)
AND lp.organizationId is not null
GROUP BY organizationId, ef_Id",
            "count" => "SELECT count(*) as count
FROM (SELECT lp.organizationId, ep.fk_eiisEduProgramType,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then 'ВО - бакалавриат'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then 'ВО - специалитет'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then 'ВО - магистратура'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then 'ВО - подготовка кадров высшей квалификации'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then 'Дополнительное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then 'Среднее профессиональное образование'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then 'Начальное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then 'Профессиональное обучение'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then 'Дополнительное образование детей и взрослых'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then 'Общее образование'
   when ep.fk_eiisEduProgramType IN('13D0A5AA05D64AF395A3F1E14F910BC5') then 'Неопределенная форма обучения (ВПО)'
   ELSE 'Неопределенная форма обучения'
END as ef_Name,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then '01'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then '02'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then '03'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then '04'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then '05'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then '06'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then '07'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then '08'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then '09'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then '10'
   when ep.fk_eiisEduProgramType IN('13D0A5AA05D64AF395A3F1E14F910BC5') then '11'
   ELSE '12'
END as ef_Id

FROM f11_mon_lod.EducationProgram lp
LEFT JOIN f11_mon.eiis_EduPrograms ep ON ep.Id = lp.EduProgramsID
WHERE lp.LicenseSupplementID IN (
   SELECT s.Id
   FROM f11_mon.eiis_LicenseSupplements s
   WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)
AND lp.organizationId is not null
GROUP BY organizationId, ef_Id) t",
            "fields" => array(
                "ID" => "",
                "SCHOOL_FK" => "",
                "EDUFORM_FK" => "",
                "NAME" => ""
            ),
            "function" => "islod_schools_eduforms"
        ),

        "ISLOD.LICENSING_PROCEDURE" => array(
            "db"    => "lod",
            "table" => "LicensingProcedure",
            "fields" => array(
                "SYS_GUID"  => "id",
                "LICENSINGPROCEDURE"      => "title"
            )
        ),
        "ISLOD.DOCUMENT_TYPES" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "fields" => array(
                "SYS_GUID"  => "id",
                "DOCUMENTTYPES"      => "title"
            )
        ),
        "ISLOD.NOTICE_STATUS" => array(
            "db"    => "lod",
            "table" => "catalog_document",
            "fields" => array(
                "SYS_GUID"  => "origin_name",
                "DOCUMENTTYPES"      => "russian_name"
            )
        ),

        /** ====================== Реестр ================================ **/

        "EIIS.F_LICENSES" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lic.guid as lic_guid, lic.license_organ_id, lic.issue_date as lic_create_date, lic.number, lic.doc_serie, lic.doc_number, lic.end_date,
  lorg.name as lorg_name, lorg.guid as lic_organ_guid,
  eo.*,
  reg.name as reg_name, reg.guid as reg_guid,
  st.code as lic_state_id, st.service_name as lic_state_name
FROM lic_reestr.license lic
LEFT JOIN lic_reestr.license_organ lorg ON lorg._id = lic.license_organ_id
LEFT JOIN lic_reestr.eo eo ON eo._id = lic.eo_id
LEFT JOIN lic_reestr.region reg ON reg._id = eo.region_id
LEFT JOIN lic_reestr.license_state st ON st._id = lic.license_state_id
WHERE lic.license_state_id != 2 AND lic.source is NULL
GROUP BY lic.guid
",
            "count" => "SELECT count(*) as count FROM lic_reestr.license lic WHERE lic.license_state_id != 2 AND lic.source is NULL",
            "fields" => array(
                "ID" => "",
                "LicOrganFk" => "",
                "LicOrgan" => "",
                "FullOrgName" => "",
                "ShortOrgName" => "",
                "NameIP" => "",
                "OGRN" => "",
                "INN" => "",
                "KPP" => "",
                "RegionFk" => "",
                "Region" => "",
                "Address" => "",
                "LicDate" => "",
                "LicNumber" => "",
                "LicBlankSer" => "",
                "LicBlankNum" => "",
                "LicEndDate" => "",
                "IsDubl" => "",
                "DocTypeName" => "",
                "DocNumber" => "",
                "DocDate" => "",
                "LicStatusFk" => "",
                "LicStatus" => "",
                "AdmSuspInfo" => "",
                "SuspResumpDecisions" => "",
                "TerminateBase" => "",
                "TerminateDate" => "",
                "InspectionInfo" => "",
                "AnnulDecision" => "",
                "SchoolFk" => "",
                "VipiskaFile" => ""
            ),
            "function" => "reestr_licenses",
        ),
        "EIIS.F_SUPPLEMENTS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT sup.*,
eo.name as eo_name, eo.short_name as eo_short_name, eo.address as eo_address
FROM lic_reestr.supplement sup
LEFT JOIN lic_reestr.eo eo ON eo._id = sup.eo_id
WHERE sup.state_code != 'DRAFT' AND sup.source is NULL
GROUP BY sup._id",
            "count" => "SELECT count(*) as count FROM lic_reestr.supplement sup WHERE sup.state_code != 'DRAFT'  AND sup.source is NULL",
            "fields" => array(
                "ID"                => "",
                "LicenseFk"         => "",
                "SupNumber"         => "",
                "SupBlankSer"       => "",
                "SupBlankNum"       => "",
                "FullOrgName"       => "",
                "ShortOrgName"      => "",
                "Address"           => "",
                "ImplAddresses"     => "",
                "DocTypeName"       => "",
                "DocNumber"         => "",
                "DocDate"           => "",
                "IsDubl"            => "",
                "SupStatusFk"       => "",
                "SupStatus"         => "",
                "TerminateBase"     => "",
                "TerminateDate"     => "",
                "SchoolFk"          => ""
            ),
            "function" => "reestr_supplements",
        ),

        "EIIS.F_DIRECTIONS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT pr.*
FROM lic_reestr.program pr WHERE pr.source is NULL",
            "count" => "SELECT count(*) as count FROM lic_reestr.program pr WHERE pr.source is NULL",
            "fields" => array(
                "ID"                => "",
                "SupplementFk"      => "",
                "LevelFk"           => "",
                "Level"             => "",
                "Code"              => "",
                "Name"              => "",
                "Kind"              => "",
                "Qualification"     => "",
                "ProgramFk"         => ""
            ),
            "function" => "reestr_programs",
        ),

        "EIIS.F_CERTIFICATES" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT c.*, r.Name as regName, cs.serviceName as statusName, cs.Id as statusId,
eo.HeadName as eo_HeadName, eo.HeadPost as eo_HeadPost, eo.Phone as eo_Phone, eo.Fax as eo_Fax, eo.Email as eo_Email, eo.PostAddress as eo_PostAddress, eo.WebSite as Www
FROM reestr_new.accreditationcertificate c
LEFT JOIN reestr_new.educationorganization eo ON eo._id = c.eduorg_id
LEFT JOIN reestr_new.region r ON r.Id = c.RegionId
LEFT JOIN reestr_new.certificatestatus cs ON cs.Id = c.CertificateStatusId
WHERE c.source is NULL",
            "count" => "SELECT count(*) as count FROM reestr_new.accreditationcertificate ce WHERE ce.source is NULL",
            "fields" => array(
                "ID"            => "",
                "AkkredOrgan"   => "",
                "AkkredOrganFK" => "",
                "FullOrgName"   => "",
                "NameIP"        => "",
                "OrgRukFio"     => "",
                "OrgRukPost"    => "",
                "OGRN"          => "",
                "INN"           => "",
                "Phone"         => "",
                "Fax"           => "",
                "Mail"          => "",
                "WebSite"       => "",
                "RegionFk"      => "",
                "Region"        => "",
                "Address"       => "",
                "SvidNumber"    => "",
                "SvidDate"      => "",
                "SvidEndDate"   => "",
                "SvidBlankSer"  => "",
                "SvidBlankNum"  => "",
                "DocTypeName"   => "",
                "DocNumber"     => "",
                "DocDate"       => "",
                "SvidStatusFK"  => "",
                "SvidStatus"    => "",
                "OrgULFio"      => "",
                "OrgULPost"     => "",
                "SchoolFk"      => "",
                "SvidPeriod"    => "",
                "VipiskaFile"    => ""
            ),
            "function" => "reestr_certificates",
        ),
        "EIIS.F_CER_SUPPLEMENTS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT c.*, cs.serviceName as StatusName
FROM reestr_new.certificatesupplement c
LEFT JOIN reestr_new.certificatesupplementstatus cs ON cs.Id = c.CertificateSupplementStatusId
WHERE c.source is NULL",
            "count" => "SELECT count(*) as count FROM reestr_new.certificatesupplement cs WHERE cs.source is NULL",
            "fields" => array(
                "ID"                => "",
                "CertificateFk"     => "",
                "SupNumber"         => "",
                "SupDate"           => "",
                "SupBlankSer"       => "",
                "SupBlankNum"       => "",
                "FullOrgName"       => "",
                "DocTypeName"       => "",
                "DocNumber"         => "",
                "DocDate"           => "",
                "SupStatus"         => "",
                "SupStatusFk"       => "",
                "SchoolFk"          => ""
            ),
            "function" => "reestr_certificate_supplements",
        ),
        "EIIS.F_CER_DIRECTIONS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT ap.*, el.Name as levelName
FROM reestr_new.accreditedprogramm ap
LEFT JOIN reestr_new.edulevel el ON el.Id = ap.EduLevelId
WHERE ap.source is NULL",
            "count" => "SELECT count(*) as count FROM reestr_new.accreditedprogramm ap WHERE ap.source is NULL",
            "fields" => array(
                "ID"                => "",
                "SupplementFk"      => "",
                "Level"             => "",
                "CodeUgsn"          => "",
                "NameUgsn"          => "",
                "CodeNp"            => "",
                "NameNp"            => "",
                "DirectionStatus"   => ""
            ),
            "function" => "reestr_accr_programs",
        )
    );
    //список сущностей
    private $_correspondences_new = [
        // ====================== Реестр =========================== //

        "EIIS.F_LICENSES" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lic.guid as lic_guid, lic.license_organ_id, lic.issue_date as lic_create_date, lic.number, lic.doc_serie, lic.doc_number, lic.end_date,
  lorg.name as lorg_name, lorg.guid as lic_organ_guid,
  eo.*,
  reg.name as reg_name, reg.guid as reg_guid,
  st.code as lic_state_id, st.service_name as lic_state_name
FROM lic_reestr.license lic
LEFT JOIN lic_reestr.license_organ lorg ON lorg._id = lic.license_organ_id
LEFT JOIN lic_reestr.eo eo ON eo._id = lic.eo_id
LEFT JOIN lic_reestr.region reg ON reg._id = eo.region_id
LEFT JOIN lic_reestr.license_state st ON st._id = lic.license_state_id
WHERE lic.license_state_id != 2 AND lic.source is NULL
GROUP BY lic.guid
",
            "count" => "SELECT count(*) as count FROM lic_reestr.license lic WHERE lic.license_state_id != 2 AND lic.source is NULL",
            "fields" => array(
                "ID" => "",
                "LicOrganFk" => "",
                "LicOrgan" => "",
                "FullOrgName" => "",
                "ShortOrgName" => "",
                "NameIP" => "",
                "OGRN" => "",
                "INN" => "",
                "KPP" => "",
                "RegionFk" => "",
                "Region" => "",
                "Address" => "",
                "LicDate" => "",
                "LicNumber" => "",
                "LicBlankSer" => "",
                "LicBlankNum" => "",
                "LicEndDate" => "",
                "IsDubl" => "",
                "DocTypeName" => "",
                "DocNumber" => "",
                "DocDate" => "",
                "LicStatusFk" => "",
                "LicStatus" => "",
                "AdmSuspInfo" => "",
                "SuspResumpDecisions" => "",
                "TerminateBase" => "",
                "TerminateDate" => "",
                "InspectionInfo" => "",
                "AnnulDecision" => "",
                "SchoolFk" => "",
                "VipiskaFile" => ""
            ),
            "function" => "reestr_licenses",
        ),
        "EIIS.F_SUPPLEMENTS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT sup.*,
eo.name as eo_name, eo.short_name as eo_short_name, eo.address as eo_address
FROM lic_reestr.supplement sup
LEFT JOIN lic_reestr.eo eo ON eo._id = sup.eo_id
WHERE sup.state_code != 'DRAFT' AND sup.source is NULL
GROUP BY sup._id",
            "count" => "SELECT count(*) as count FROM lic_reestr.supplement sup WHERE sup.state_code != 'DRAFT'  AND sup.source is NULL",
            "fields" => array(
                "ID"                => "",
                "LicenseFk"         => "",
                "SupNumber"         => "",
                "SupBlankSer"       => "",
                "SupBlankNum"       => "",
                "FullOrgName"       => "",
                "ShortOrgName"      => "",
                "Address"           => "",
                "ImplAddresses"     => "",
                "DocTypeName"       => "",
                "DocNumber"         => "",
                "DocDate"           => "",
                "IsDubl"            => "",
                "SupStatusFk"       => "",
                "SupStatus"         => "",
                "TerminateBase"     => "",
                "TerminateDate"     => "",
                "SchoolFk"          => ""
            ),
            "function" => "reestr_supplements",
        ),
        "EIIS.F_DIRECTIONS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT pr.*
FROM lic_reestr.program pr WHERE pr.source is NULL",
            "count" => "SELECT count(*) as count FROM lic_reestr.program pr WHERE pr.source is NULL",
            "fields" => array(
                "ID"                => "",
                "SupplementFk"      => "",
                "LevelFk"           => "",
                "Level"             => "",
                "Code"              => "",
                "Name"              => "",
                "Kind"              => "",
                "Qualification"     => "",
                "ProgramFk"         => ""
            ),
            "function" => "reestr_programs",
        ),

        "EIIS.F_CERTIFICATES" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT c.*, r.Name as regName, cs.serviceName as statusName, cs.Id as statusId,
eo.HeadName as eo_HeadName, eo.HeadPost as eo_HeadPost, eo.Phone as eo_Phone, eo.Fax as eo_Fax, eo.Email as eo_Email, eo.PostAddress as eo_PostAddress, eo.WebSite as Www
FROM reestr_new.accreditationcertificate c
LEFT JOIN reestr_new.educationorganization eo ON eo._id = c.eduorg_id
LEFT JOIN reestr_new.region r ON r.Id = c.RegionId
LEFT JOIN reestr_new.certificatestatus cs ON cs.Id = c.CertificateStatusId
WHERE c.source is NULL",
            "count" => "SELECT count(*) as count FROM reestr_new.accreditationcertificate ce WHERE ce.source is NULL",
            "fields" => array(
                "ID"            => "",
                "AkkredOrgan"   => "",
                "AkkredOrganFK" => "",
                "FullOrgName"   => "",
                "NameIP"        => "",
                "OrgRukFio"     => "",
                "OrgRukPost"    => "",
                "OGRN"          => "",
                "INN"           => "",
                "Phone"         => "",
                "Fax"           => "",
                "Mail"          => "",
                "WebSite"       => "",
                "RegionFk"      => "",
                "Region"        => "",
                "Address"       => "",
                "SvidNumber"    => "",
                "SvidDate"      => "",
                "SvidEndDate"   => "",
                "SvidBlankSer"  => "",
                "SvidBlankNum"  => "",
                "DocTypeName"   => "",
                "DocNumber"     => "",
                "DocDate"       => "",
                "SvidStatusFK"  => "",
                "SvidStatus"    => "",
                "OrgULFio"      => "",
                "OrgULPost"     => "",
                "SchoolFk"      => "",
                "SvidPeriod"    => "",
                "VipiskaFile"   => ""
            ),
            "function" => "reestr_certificates",
        ),
        "EIIS.F_CER_SUPPLEMENTS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT c.*, cs.serviceName as StatusName
FROM reestr_new.certificatesupplement c
LEFT JOIN reestr_new.certificatesupplementstatus cs ON cs.Id = c.CertificateSupplementStatusId
WHERE c.source is NULL",
            "count" => "SELECT count(*) as count FROM reestr_new.certificatesupplement cs WHERE cs.source is NULL",
            "fields" => array(
                "ID"                => "",
                "CertificateFk"     => "",
                "SupNumber"         => "",
                "SupDate"           => "",
                "SupBlankSer"       => "",
                "SupBlankNum"       => "",
                "FullOrgName"       => "",
                "DocTypeName"       => "",
                "DocNumber"         => "",
                "DocDate"           => "",
                "SupStatus"         => "",
                "SupStatusFk"       => "",
                "SchoolFk"          => ""
            ),
            "function" => "reestr_certificate_supplements",
        ),
        "EIIS.F_CER_DIRECTIONS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT ap.*, el.Name as levelName
FROM reestr_new.accreditedprogramm ap
LEFT JOIN reestr_new.edulevel el ON el.Id = ap.EduLevelId
WHERE ap.source is NULL",
            "count" => "SELECT count(*) as count FROM reestr_new.accreditedprogramm ap WHERE ap.source is NULL",
            "fields" => array(
                "ID"                => "",
                "SupplementFk"      => "",
                "Level"             => "",
                "CodeUgsn"          => "",
                "NameUgsn"          => "",
                "CodeNp"            => "",
                "NameNp"            => "",
                "DirectionStatus"   => ""
            ),
            "function" => "reestr_accr_programs",
        ),

        // ====================== ИСЛОД =========================== //

        "ISLOD.EDULEVELS" => array(
            "table" => "eiis_EduLevels",
            "fields" => array(
                "ID"          => "Id",
                "CODE"                => "Code",
                "NAME"              => "Name",
                "SHORTNAME"         => "ShortName",
                "EIIS_EDULEVEL_FK"  => "parentlevel"
            )
        ),
        "ISLOD.EDU_PROGRAM_TYPES" => array(
            "table" => "eiis_EduProgramTypes",
            "fields" => array(
                "ID"                  => "Id",
                "CODE"                        => "code",
                "NAME"                      => "Name",
                "SHORTNAME"                 => "ShortName"
            )
        ),
        "ISLOD.UGS" => array(
            "table" => "isga_EnlargedGroupSpecialities",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "STANDART" => "Standart",
                "ACTUAL" => "isActual"
            )
        ),
        "ISLOD.EDUPROGRAMS" => array(
            "table" => "eiis_EduPrograms",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "EDULEVELFK" => "fk_eiisEduLevels",
                "EDUPROGRAMTYPEFK" => "fk_eiisEduProgramType",
                "STANDARD_TYPE" => "fk_eiisEduProgramType",
                "UGSCODE" => "UgsCode",
                "UGSNAME" => "UgsName",
                "UGS_FK" => "fk_eiisUgs",
                "PERIOD" => "Period",
                "ACTUAL" => ""
            ),
            "function" => "islod_eduprograms"
        ),
        "ISLOD.BASEDOC_TYPES" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "idField" => "id",
            "fields" => array(
                "ID" => "id",
                "CODE" => "code",
                "NAME" => "title"
            )
        ),
        "ISLOD.LICENSE_STATUSES" => array(
            "table" => "eiis_LicenseStates",
            "idField" => "Id",
            "fields" => array(
                "ID" => "LodCode",
                "NAME" => "Name"
            )
        ),
        "ISLOD.LICENSE_SIGN_STATUSES" => array(
            "db"    => "lod",
            "table" => "eiis_sign_states",
            "idField" => "id",
            "fields" => array(
                "ID" => "origin_name",
                "NAME" => "russian_name"
            )
        ),
        "ISLOD.LICENSES" => array(
            "table" => "eiis_Licenses",
            "where" => [
                "fk_eiisLicenseState IS NOT NULL AND fk_eiisLicenseState !='' AND fk_eiisLicenseState != '34F4DBDB89FA94D119AF0C1ABDD61D43' AND fk_eiisEducationalOrganization != '' AND fk_eiisEducationalOrganization IS NOT NULL"
            ],
            "fields" => array(
                "ID"                  => "Id",
                "LICENSE_REG_NUM"           => "LicenseRegNum",
                "SERDOC"                    => "SerDoc",
                "NUMDOC"                    => "NumDoc",
                "LICENSE_STATUSESFK"        => "",
                "LICENSE_SIGN_STATUSESFK"   => "lod_signStateCode",
                "IS_TERMLESS"               => "IsTermless",
                "DATA_END"                  => "DateEnd",
                "SCHOOL_FK"              => "fk_eiisEducationalOrganization",
                "SCHOOLNAME"                => "EOName",
                "SHORTNAME"                 => "EOShortName",
                "LAWADDRESS"                => "EOLawAddress",
                "GOSREGNUM"                 => "EOGosRegNum",
                "INN"                       => "EOInn",
                "BASEDOC_TYPE_FK"            => "",
                "NUMLICDOC"                 => "NumLicDoc",
                "DATELICDOC"                => "DateLicDoc",
                "MANAGERFIO"                => "ManagerFio",
                "POST"                      => "Post",
                "LICENSE_STAT_FK"             => "",
                "LICENSE_OLD_FK"              => "",
                "BASEDOC_TYPE_STOP_FK"                => "",
                "REASON_OF_SUSPENSION"      => "",
                "DATE_OF_SUSPENSION"        => "",
                "ADM_SUSP_ORDERS"           => "",
                "ORG_SUSP_DECISIONS"        => "",
                "COURT_REVOKING_DECISIONS"  => "",
                "DOUBLE"                    => "0",
                "NUM_DATE_DUPL_ISSUE"       => "",
                "DATE_DUPL"                 => "",
                "COPY_LICENSE_STATFK"       => ""
            ),
            "dateFields" => array(
                "DateEnd",
                "DateLicDoc"
            ),
            "function" => "islod_licenses"
        ),
        "ISLOD.LICENSE_APP_STATUSES" => array(
            "table" => "eiis_LicenseSupplementStates",
            "fields" => array(
                "ID" => "lodCode",
                "NAME" => "Name"
            )
        ),
        "ISLOD.LICENSE_APP_SIGN_STATUSES" => array(
            "db"    => "lod",
            "table" => "eiis_sign_states",
            "fields" => array(
                "ID" => "origin_name",
                "NAME" => "russian_name"
            )
        ),
        "ISLOD.LICENSE_APPS" => array(
            "table" => "eiis_LicenseSupplements",
            "where" => [
                "fk_eiisLicenseSupplementState IS NOT NULL AND fk_eiisLicenseSupplementState !='' AND fk_eiisLicenseSupplementState != '4E09AEA87399AFFD9EC3FE6833DB4B36' AND fk_eiisBranch  != '' AND fk_eiisBranch IS NOT NULL"
            ],
            "fields" => array(
                "ID"                      => "Id",
                "LICENSE_FK"                     => "fk_eiisLicense",
                "NUMBER"                        => "Number",
                "SER_DOC"                       => "SerDoc",
                "NUMBER_DOC"                    => "NumDoc",
                "LICENSE_APP_STATUS_FK"           => "",
                "LICENSE_APP_SIGN_STATUSE_FK"    => "lod_signStateCode",
                "DATE_END"                      => "DateEnd",
                "MANAGERFIO"                    => "ManagerFio",
                "POST"                          => "Post",
                "SCHOOL_FK"                       => "fk_eiisBranch",
                "SCHOOLNAME"                    => "EOName",
                "SHORTNAME"                     => "EOShortName",
                "LAWADDRESS"                    => "EOLawAddress",
                "IMPL_ADDR"                     => "",
                "BASEDOC_TYPE_FK"                => "lod_basisForLicensing",
                "NUMLICDOC"                     => "NumLicDoc",
                "DATELICDOC"                    => "DateLicDoc",
                "LICENSE_STAT_FK"                => "fk_eiisLicenseState",
                "LICENSE_APP_OLD_FK"               => "fk_eiisLicenseSupplementOld",
                "BASEDOC_TYPE_STOP_FK"                    => "",
                "REASON_OF_SUSPENSION"          => "",
                "DATE_OF_SUSPENSION"            => "",
                "DOUBLE"                        => "Duplicate",
                "DUPLICATE_ORDER"               => "DuplicateOrder",
                "DATE_DUPL"                     => "",
                "DUP_LICENSE_STAT_FK"                => "fk_eiisLicenseState_Duplicate"
            ),
            "dateFields" => array(
                "DateEnd",
                "DateLicDoc"
            ),
            "function" => "islod_supplements"
        ),
        "ISLOD.LICENSED_PROGRAMS" => array(
            "db"    => "lod",
            "table" => "EducationProgram",
            "fields" => array(
                "ID"              => "id",
                "CODE"                  => "",
                "NAME"                  => "",
                "EDULEVEL_FK"            => "educationLevelId",
                "EDU_PROGRAM_TYPE_FK"      => "",
                "STANDARD"         => "",
                "PERIOD"                => "",
                "EDU_PROGRAM_FK"          => "EduProgramsID",
                "NEW_EDUPROGRAM_FK"      => "EduProgramsID",
                "LICENSE_APP_FK"         => "LicenseSupplementID"
            ),
            "function" => "islod_licensed_programs"
        ),
        "ISLOD.QUALIFICATIONS" => array(
            "table" => "isga_Qualifications",
            "fields" => array(
                "ID" => "Id",
                "CODE"     => "Code",
                "NAME"     => "Name",
                "ACTUAL"   => "IsActual"
            )
        ),
        "ISLOD.LICENSED_PROGRAMS_2_QUALIFICATIONS" => array(
            "db"    => "lod",
            "table" => "EducationProgramToQualification",
            "idField" => "id",
            "fields" => array(
                "ID"          => "id",
                "LICENSED_PROGRAM_FK"      => "educationProgramId",
                "QUALIFICATION_FK"   => "qualificationId"
            )
        ),
        "ISLOD.SCHOOL_KINDS" => array(
            "table" => "eiis_EducationalOrganizationKinds",
            "fields" => array(
                "ID"              => "Id",
                "CODE"       => "Code",
                "NAME"       => "Name",
                "SCHOOL_TYPE_FK"  => "fk_eiisEducationalOrganizationType"
            )
        ),
        "ISLOD.SCHOOL_TYPES" => array(
            "table" => "eiis_EducationalOrganizationTypes",
            "fields" => array(
                "ID"  => "Id",
                "NAME" => "Name"
            )
        ),
        "ISLOD.SCHOOLPROPERTIES" => array(
            "table" => "eiis_EducationalOrganizationProperties",
            "where" => [
                "parent IS NOT NULL"
            ],
            "fields" => array(
                "ID"              => "Id",
                "CODE"    => "Code",
                "NAME"    => "Name",
                "SCHOOLPROPERTY_FK"      => "parent"
            )
        ),
        "ISLOD.ORG_FORMS" => array(
            "table" => "eiis_EducationalOrganizationProperties",
            "where" => [
                "parent IS NULL"
            ],
            "fields" => array(
                "ID"              => "Id",
                "CODE"    => "Code",
                "NAME"    => "Name"
            )
        ),
        "ISLOD.REGIONS" => array(
            "table" => "eiis_Regions",
            "fields" => array(
                "ID"      => "Id",
                "CODE"        => "Code",
                "NAME"    => "Name",
                "FED_OKR_FK"    => "fk_eiisFedOkr"
            )
        ),
        "ISLOD.FED_OKR" => array(
            "table" => "eiis_FedOkr",
            "fields" => array(
                "ID"  => "Id",
                "NAME"      => "Name",
                "SHORTNAME" => "ShortName"
            )
        ),
        "ISLOD.SCHOOL_STATUSES" => array(
            "db"    => "lod",
            "table" => "catalog_organization",
            "fields" => array(
                "ID" => "origin_name",
                "NAME" => "russian_name"
            )
        ),
        "ISLOD.SCHOOLS" => array(
            "table" => "eiis_EducationalOrganizations",
            "where" => [
                "lod_stateCode != 'DRAFT'"
            ],
            "fields" => array(
                "ID"              => "Id",
                "REGULARNAME"           => "RegularName",
                "SCHOOLNAME"            => "FullName",
                "SHORTNAME"             => "ShortName",
                "SCHOOLPROPERTY_FK"      => "fk_eiisEducationalOrganizationProperties",
                "SCHOOL_TYPE_FK"          => "fk_eiisEducationalOrganizationType",
                "SCHOOL_KIND_FK"          => "fk_eiisEducationalOrganizationKind",
                "BRANCH"                => "Branch",
                "PARENT_SCHOOL_FK"        => "fk_eiisParentEducationalOrganization",
                "RENAME"                => "",
                "GOSREGNUM"             => "GosRegNum",
                "INN"                   => "Inn",
                "KPP"                   => "Kpp",
                "REGION_FK"       => "fk_eiisRegion",
                "LAWADDRESS"            => "LawAddress",
                "ADDRESS"               => "Address",
                "CITY"                  => "",
                "IMPL_ADDR"             => "",
                "CHARGEPOSITION"        => "",
                "CHARGEFIO"             => "ChargeFio",
                "PHONES"                => "Phones",
                "FAXS"                  => "Faxes",
                "MAILS"                 => "Mails",
                "WWW"                   => "Www",
                "REGION_COMPETENCE"     => "",
                "ISSTRONG"              => "isStrong",
                "ISRELIGION"            => "isReligion",
                "STARTACTIVITYDATE"     => "",
                "STOPACTIVITYDATE"      => "",
                "STOP_LICENSE_STAT_FK"           => "",
                "SCHOOL_STATUS_FK"              => "lod_stateCode",
                "COMMENT"               => "",
                "ISFU"                  => "federalUniversity",
                "ISNRU"                 => "nationalResearchUniversity"
            ),
            "function" => "islod_schools"
        ),
        "ISLOD.LICENSE_REQ_TYPES" => array(
            "db"    => "lod",
            "table" => "LicensingProcedure",
            "fields" => array(
                "ID"  => "id",
                "CODE"        => "code",
                "NAME"      => "title"
            )
        ),
        "ISLOD.LICENSE_STAT_STATUSES" => array(
            "db"    => "lod",
            "table" => "catalog_deal",
            "fields" => array(
                "ID"  => "origin_name",
                "CODE"      => "id",
                "NAME"      => "russian_name"
            )
        ),
        "ISLOD.LICENSE_STAT" => array(
            "db"    => "lod",
            "table" => "LicenseRequest",
            "idField" => "id",
            "fields" => array(
                "ID"          => "id",
                "APPLICANT_FK"         => "organizationId",
                "SCHOOL_FK"          => "organizationId",
                "STAGE"             => "",
                "STATUS"            => "",
                "LICENSING_PROCEDURE_FK" => "licensingProcedureId",
                "REGNUM_TEXT"       => "number",
                "REGDATE"           => "fillingDate",
                "EXPERT"            => "",
                "SCHOOLNAME"        => "",
                "SHORTNAME"         => "",
                "SCHOOLPROPERTY_FK"  => "",
                "SCHOOL_TYPE_FK"      => "",
                "SCHOOL_KIND_FK"      => "",
                "LAWADDRESS"        => "",
                "ADDRESS"           => "",
                "GOSREGNUM"         => "",
                "INN"               => "",
                "KPP"               => "",
                "BENEFICIARY"       => "",
                "BEN_SHORTNAME"     => "",
                "BENEFECIARY_ADDRESS" => "",
                "BEN_IMPL_ADDRESS"  => "",
                "OLD_LICENSE_FK"     => "",
                "RENEW_LICENSE_FK"   => ""
            ),
            "function" => "islod_license_request"
        ),
        "ISLOD.NOTICE_TYPES" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "idField" => "id",
            "where" => [
                "parent" => "20a4674be28c4b36ad69de14a0e4c8df"
            ],
            "fields" => array(
                "ID"  => "id",
                "CODE"        => "code",
                "NAME"      => "title"
            )
        ),
        "ISLOD.LICENSE_ORDER_RESOLUTIONS" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "idField" => "id",
            "where" => [
                "parent" => ["177fabe881474796b37b30c47969e93f", "e6b56dff077f40bda8c1c9d67e410f85"]
            ],
            "fields" => array(
                "ID"  => "id",
                "CODE"      => "code",
                "NAME"      => "title"
            )
        ),
        "ISLOD.ORDERS" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent IN('177fabe881474796b37b30c47969e93f', 'e6b56dff077f40bda8c1c9d67e410f85'))"
            ],
            "fields" => array(
                "ID"          => "id",
                "ORDER_NUMBER"      => "registrationNumber",
                "ORDER_DATE"        => "registrationDate",
                "DOCUMENT_TYPES_FK"        => "documentTypeId",
                "DOCUMENT_STATUS_FK"            => "stateCode",
                "DOCUMENT_KIND_FK"    => "",
                "LICENSE_STAT_FK"            => ""

            ),
            "function" => "islod_orders",
            "dateFields" => array(
                "registrationDate"
            )
        ),
        "ISLOD.ORDER_WITH_STAT" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent IN('177fabe881474796b37b30c47969e93f', 'e6b56dff077f40bda8c1c9d67e410f85'))",
                "dealId IS NOT NULL",
                "dealId NOT like ''"
            ],
            "function" => "islod_order_with_stat",
            "fields" => array(
                "ID" => "",
                "LICENSE_STAT_FK" => "",
                "ORDER_FK" => "id"
            )
        ),
        "ISLOD.HOLIDAYS" => array(
            "db"    => "lod",
            "table" => "Calendar",
            "idField" => "id",
            "where" => [
                "status" => "free"
            ],
            "fields" => array(
                "ID" => "id",
                "NAME" => "status",
                "DATE" => "date"
            )
        ),
        "ISLOD.LICENSING_PROCEDURE" => array(
            "db"    => "lod",
            "table" => "LicensingProcedure",
            "fields" => array(
                "ID"  => "id",
                "NAME"      => "title"
            )
        ),
        "ISLOD.REASON_TO_LICENSE_STATS" => array(
            "db"    => "lod",
            "table" => "LicenseRequest",
            "idField" => "id",
            "fields" => array(
                "ID"          => "id",
                "LICENSE_STATS_FK"  => "id",
                "LICENSE_REQ_TYPE_FK" => "licensingProcedureId",
            )
        ),
        "ISLOD.DOCUMENT_TYPES" => array(
            "db"    => "lod",
            "table" => "DocumentTypeCatalogItem",
            "fields" => array(
                "ID"  => "id",
                "NAME"      => "title"
            )
        ),
        "ISLOD.NOTICE_STATUS" => array(
            "db"    => "lod",
            "table" => "catalog_document",
            "fields" => array(
                "ID"  => "origin_name",
                "NAME"      => "russian_name"
            )
        ),
        "ISLOD.DOCUMENT_KINDS" => array(
            "db"    => "lod",
            "table" => "CatalogItem",
            "idField" => "id",
            "where" => [
                "catalog" => "documentDecisionTypes"
            ],
            "fields" => array(
                "ID"  => "id",
                "NAME"      => "title"
            )
        ),
        "ISLOD.ADMISSION_TYPES" => array(
            "db"    => "lod",
            "table" => "CatalogItem",
            "idField" => "id",
            "where" => [
                "catalog" => "admissionTypes"
            ],
            "fields" => array(
                "ID"  => "id",
                "NAME"      => "title"
            )
        ),
        "ISLOD.DOCUMENT_STATUSES" => array(
            "db"    => "lod",
            "table" => "catalog_document",
            "idField" => "id",
            "fields" => array(
                "ID"  => "id",
                "CODE"      => "origin_name",
                "NAME"      => "russian_name"
            )
        ),
        "ISLOD.SCHOOLS_REORGANIZATIONS" => array(
            "db"    => "lod",
            "table" => "OrganizationToReorganizedOrganization",
            "select" => "SELECT mtm.*, rt.title as reorganizationType
FROM f11_mon_lod.OrganizationToReorganizedOrganization mtm
LEFT JOIN (SELECT distinct(mtm.dealId), ri.reorganizationType FROM f11_mon_lod.OrganizationToReorganizedOrganization mtm LEFT JOIN f11_mon_lod.ReorganizedOrganizationInfo ri ON ri.organizationId = mtm.reorganizedOrganizationId WHERE ReorganizationType is not null) t1 ON t1.dealId = mtm.dealId
LEFT JOIN f11_mon_lod.ReorganizationType rt ON rt.id = t1.reorganizationType",
            "count" => "SELECT count(*) as count
FROM f11_mon_lod.OrganizationToReorganizedOrganization mtm
LEFT JOIN (SELECT distinct(mtm.dealId), ri.reorganizationType FROM f11_mon_lod.OrganizationToReorganizedOrganization mtm LEFT JOIN f11_mon_lod.ReorganizedOrganizationInfo ri ON ri.organizationId = mtm.reorganizedOrganizationId WHERE ReorganizationType is not null) t1 ON t1.dealId = mtm.dealId
LEFT JOIN f11_mon_lod.ReorganizationType rt ON rt.id = t1.reorganizationType",
            "idField" => "id",
            "fields" => array(
                "ID"  => "id",
                "SCHOOL_FK"      => "organizationId",
                "REORGANIZED_SCHOOL_FK"      => "reorganizedOrganizationId",
                "REORGANIZATION_TYPE_FK"      => "reorganizationType"
            )
        ),
        "ISLOD.REORGANIZATION_TYPE" => array(
            "db"    => "lod",
            "table" => "ReorganizationType",
            "idField" => "id",
            "fields" => array(
                "ID"  => "id",
                "NAME" => "title"
            )
        ),


        "EIIS.SCHOOLS_ADDITIONAL" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "where" => [
                "lod_stateCode != 'DRAFT'"
            ],
            "fields" => array(
                "ID"                => "Id",
                "IS_GOS"            => "",
                "PROPERTY"          => "",
                "SCHOOL_AGR_TYPE"   => "",
                "LICENSE_INFO"      => "",
                "LICENSE_STATUS"    => "",
                "LICENSE_FK"        => "",
                "STATUS"            => "",
                "KINDNAME"          => ""

            ),
            "function" => "islod_schools_additional",
        ),
        "ISLOD.APPLICATION_STATISTICS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lr.id, lr.organizationId, mlr.licenseReasonId,
count(*) as total,
COUNT(CASE WHEN d.stateCode = 'REFUSED' THEN 1 END) AS refused,
CASE
   when mlr.licenseReasonId IN('518dfabe3375431684b71d2faa5c7564', 'a382cea16f1c4cf9a729d4f8d0d8a8a0') then 'получение лицензии, переоформление при преобразовании'
   when mlr.licenseReasonId IN('0ba32b48a6384b5c95dcb630f34c930d', '2b6b9edcdf394ea9b3b67c9ac8a23b7e', '33b935940d1a4c5383ee72accbdf4ee9', '3e5b5f379598446ea2833f4acd2ba01b', 'c83201802b474c4393305b16b7c05375', 'f9893ac3e72e43b59101a8270f9eaacf', 'fb71a4f02e7d4f07be60fcc43fe2e9a2', 'fd77ff8629184ae89d18858d2b4d7a50', '08f4049094984955ab29d17d6f178d17', '317c24a9fb7c42b88400432d210a01c4', '469c8a48d80a4fe4912d834143de72ee') then 'переоформление 10 дней'
   when mlr.licenseReasonId IN('6d8c38b1ab79486eb8f826943b723f4c', '9ccbaed80f8248c9b1af9ff6f5bf02c9', 'aac9fc11db3d4682a3e80a64dabc1ede') then 'переоформление 45 дней'
   when mlr.licenseReasonId IN('1c3acd333a204d5288277d2c2fd3509b', 'efb3b9bf460c4a8aae48b9dd5e5d5d84', '34bee06f79144d5584668ba9fad809f8') then 'выдача дубликата, копии'
   when mlr.licenseReasonId IN('42807cbc9a9c419ba7ebc374652ed68a', 'b4bfaa7993784256890dc19a8deb7e88') then 'прекращение од'
   ELSE 'не определено'
END as indicator
FROM f11_mon_lod.LicenseRequest lr
inner join f11_mon_lod.LicenseReasonToLicenseRequest mlr on mlr.licenseRequestId = lr.id
left join f11_mon_lod.Deal d on d.id = lr.dealId
group by lr.id, indicator",
            "count" => "SELECT count(*) as count
FROM
(SELECT lr.id, lr.organizationId, mlr.licenseReasonId,
CASE
   when mlr.licenseReasonId IN('518dfabe3375431684b71d2faa5c7564', 'a382cea16f1c4cf9a729d4f8d0d8a8a0') then 'получение лицензии, переоформление при преобразовании'
   when mlr.licenseReasonId IN('0ba32b48a6384b5c95dcb630f34c930d', '2b6b9edcdf394ea9b3b67c9ac8a23b7e', '33b935940d1a4c5383ee72accbdf4ee9', '3e5b5f379598446ea2833f4acd2ba01b', 'c83201802b474c4393305b16b7c05375', 'f9893ac3e72e43b59101a8270f9eaacf', 'fb71a4f02e7d4f07be60fcc43fe2e9a2', 'fd77ff8629184ae89d18858d2b4d7a50', '08f4049094984955ab29d17d6f178d17', '317c24a9fb7c42b88400432d210a01c4', '469c8a48d80a4fe4912d834143de72ee') then 'переоформление 10 дней'
   when mlr.licenseReasonId IN('6d8c38b1ab79486eb8f826943b723f4c', '9ccbaed80f8248c9b1af9ff6f5bf02c9', 'aac9fc11db3d4682a3e80a64dabc1ede') then 'переоформление 45 дней'
   when mlr.licenseReasonId IN('1c3acd333a204d5288277d2c2fd3509b', 'efb3b9bf460c4a8aae48b9dd5e5d5d84', '34bee06f79144d5584668ba9fad809f8') then 'выдача дубликата, копии'
   when mlr.licenseReasonId IN('42807cbc9a9c419ba7ebc374652ed68a', 'b4bfaa7993784256890dc19a8deb7e88') then 'прекращение од'
   ELSE 'не определено'
END as indicator
FROM f11_mon_lod.LicenseRequest lr
inner join f11_mon_lod.LicenseReasonToLicenseRequest mlr on mlr.licenseRequestId = lr.id
group by lr.id, indicator) t",
            "fields" => array(
                "ID"                    => "",
                "SCHOOL_FK"             => "",
                "INDICATOR"             => "",
                "VALUE_1_FILED"         => "",
                "VALUE_2_FAILURE"       => "",
                "VALUE_3_REVOCATION"    => ""
            ),
            "function" => "islod_application_statistics",
        ),
        "ISLOD.SCHOOLS_PROGRAMS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lp.*, ep.*, st.Name as StandartName, q.Name as QualName, el.Name as ELName
FROM f11_mon_lod.EducationProgram lp
LEFT JOIN f11_mon.eiis_EduPrograms ep ON ep.Id = lp.EduProgramsID
LEFT JOIN f11_mon.isga_EduStandards st ON st.Id = ep.fk_eiisEduStandard
LEFT JOIN f11_mon.isga_Qualifications q ON q.Id = ep.fk_eiisQualification
LEFT JOIN f11_mon.eiis_EduLevels el ON el.Id = ep.fk_eiisEduLevels
WHERE lp.LicenseSupplementID IN (
   SELECT s.Id
   FROM f11_mon.eiis_LicenseSupplements s
   WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)",
            "count" => "SELECT count(*) as count
FROM f11_mon_lod.EducationProgram lp
WHERE lp.LicenseSupplementID IN (
   SELECT s.Id
   FROM f11_mon.eiis_LicenseSupplements s
   WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)",
            "fields" => array(
                "ID" => "",
                "SCHOOL_FK" => "",
                "LICENSE_APP_FK" => "",
                "LICENSED_PROGAM_FK" => "",
                "EDUPROGRAM_FK" => "",
                "IS_ACCREDITED" => "",
                "STANDART" => "",
                "CODE" => "",
                "NAME" => "",
                "QUALIFICATION" => "",
                "EDULEVEL_FK" => "",
                "EDULEVEL_NAME" => "",
                "COMMENT" => "",
                "UGS_CODE" => "",
                "UGS_NAME" => "",
                "UGS_FK" => ""
            ),
            "function" => "islod_schools_programs",
        ),
        "ISLOD.SCHOOLS_EDUFORMS" => array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "select" => "SELECT lp.organizationId, ep.fk_eiisEduProgramType,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then 'ВО - бакалавриат'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then 'ВО - специалитет'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then 'ВО - магистратура'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then 'ВО - подготовка кадров высшей квалификации'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then 'Дополнительное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then 'Среднее профессиональное образование'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then 'Начальное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then 'Профессиональное обучение'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then 'Дополнительное образование детей и взрослых'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then 'Общее образование'
   when ep.fk_eiisEduProgramType IN('13D0A5AA05D64AF395A3F1E14F910BC5') then 'Неопределенная форма обучения (ВПО)'
   ELSE 'Неопределенная форма обучения'
END as ef_Name,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then '01'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then '02'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then '03'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then '04'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then '05'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then '06'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then '07'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then '08'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then '09'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then '10'
   when ep.fk_eiisEduProgramType IN('13D0A5AA05D64AF395A3F1E14F910BC5') then '11'
   ELSE '12'
END as ef_Id

FROM f11_mon_lod.EducationProgram lp
LEFT JOIN f11_mon.eiis_EduPrograms ep ON ep.Id = lp.EduProgramsID
WHERE lp.LicenseSupplementID IN (
   SELECT s.Id
   FROM f11_mon.eiis_LicenseSupplements s
   WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)
AND lp.organizationId is not null
GROUP BY organizationId, ef_Id",
            "count" => "SELECT count(*) as count
FROM (SELECT lp.organizationId, ep.fk_eiisEduProgramType,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then 'ВО - бакалавриат'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then 'ВО - специалитет'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then 'ВО - магистратура'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then 'ВО - подготовка кадров высшей квалификации'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then 'Дополнительное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then 'Среднее профессиональное образование'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then 'Начальное профессиональное образование'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then 'Профессиональное обучение'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then 'Дополнительное образование детей и взрослых'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then 'Общее образование'

   ELSE 'Неопределенная форма обучения'
END as ef_Name,

CASE
   when ep.fk_eiisEduProgramType IN('0ce573cd1b3c40a2b8ec7f27f324ef57', 'C420030B0B27CE31BAB4CA5DDC09231E') then '01'
   when ep.fk_eiisEduProgramType IN('DEEE7F041B7EF674C33FDE54E4BF3FE8', '84f6f21b78c541a4bd2c2c1f2e4c0149') then '02'
   when ep.fk_eiisEduProgramType IN('11B8054B2D5F1438B505F2D5BF9E860E', '1f1d7259f12b489d81f6bcc03ea65b33') then '03'
   when ep.fk_eiisEduProgramType IN('0BCC5F7C61635866AB7E6CA42773F19A',
                                    '682A57F08BDABC627B50EB03E85797BD',
                                    'b630f1417a1049749e3a1a1547114d92',
                                    '35ed5431d561408ba476d19987399357',
                                    '6AD5D02F3C03CB687A542F39861347C3',
                                    'E4C71A88524E704E212E3966055CC9CC',
                                    'bd5d76f0b3df4d5da44e89d4781acca9',
                                    '31B103158B1FA54BCABF20431EB41AAA',
                                    '8a629055f05a4be6bbdbb9b5cc7e0044',
                                    '18A29655FBF3FE519AEC02F7FA8DDD99',
                                    '3823dda97f6b4251903e0d825ffea9cb',
                                    'D3D5CAD6ED6B4222A8595A16C9C6A5EB',
                                    'B6959ED0226242B6F1956AB59DFEE134',
                                    'E8360D8EA1083119B002FA83CE068091',
                                    '3CA6D96D8D16537BC8512BE3DBFD19B2') then '04'

   when ep.fk_eiisEduProgramType IN('217FE155E20A629BD577E3C0B33495BC',
                                    'C7A3D7110266AF75BC392A8319B4BAA2',
                                    '9f506b19835b4396a2af9d79b368c77f') then '05'

   when ep.fk_eiisEduProgramType IN('6f57657915f54cb5bddc7fc8645ff0a3',
                                    'A113E1BFE7C01B4F6698CDD3446F8AAE',
                                    '39cb27b87b3543daa53d3b4a208f70ae',
                                    '9150E09FD37D5465892DA3470E0CA815',
                                    'FC8E716E07E2325E3174FD83E19590FF',
                                    '50B384084A3F44F8BCEE63C1287768F5',
                                    '8fc836efa7a34867ae0b6476c8abda28') then '06'

   when ep.fk_eiisEduProgramType IN('3CC93BDEE4344E21BECE0A721E2A07AD') then '07'

   when ep.fk_eiisEduProgramType IN('EC2540E890586C33A9D056FEC1DCCCF4',
                                    '59bb10f883e848cda4bb82179e3a90fb',
                                    'B0954E0FB4EB6C50C35FA392C24F58B8',
                                    '27624209E12033C7DD3F7265D3A8CD8F',
                                    'c1897dd5579140348ecad0ad412a1c87',
                                    '1999e129c8e8403488a591d974dad6fc',
                                    '3231A2121D6BE4092F80993C9109CCDC',
                                    'AE0F1B1FA8803E861AF1BB0F04FE6324',
                                    'de5a9c0b5d444478a124f8025a02e1ba',
                                    'E6E88D95E5C7D5B250C02F64EA2D1F42',
                                    '9F3468C0E67EB035A9912EE3B4A6D50E') then '08'

   when ep.fk_eiisEduProgramType IN('05954A2AACE87216F446F92333F7DCE9',
                                    'FD8F8DE3AA814598B6FB1833DE8C1D12') then '09'

   when ep.fk_eiisEduProgramType IN('7E6176CC0E8B40E5806F4DB3559DE9FC',
                                    'e02c2c478f054bd1b9d7b46ba3ceae66',
                                    '18737366D87A4D7B9D0954533BE6A265',
                                    '3f640c0e91da4711bb062c8abb252f87',
                                    '7f9f383930fd433abfd8f3a3aea1551c',
                                    'C1FC793BA5EA4DC9A3A7C90BF47EB5FC',
                                    '4fd1513bdf8845e982b34896825c91b3',
                                    '5E30E928A3B9A62E2D6B53DCB80F8D2B') then '10'
   ELSE '11'
END as ef_Id

FROM f11_mon_lod.EducationProgram lp
LEFT JOIN f11_mon.eiis_EduPrograms ep ON ep.Id = lp.EduProgramsID
WHERE lp.LicenseSupplementID IN (
   SELECT s.Id
   FROM f11_mon.eiis_LicenseSupplements s
   WHERE s.fk_eiisLicenseSupplementState = '9416C0E6E84F4194B57DA930AD10A4D0' OR s.fk_eiisLicenseSupplementState = '0727E76E83574B10833A25CA627FE6A0'
)
AND lp.organizationId is not null
GROUP BY organizationId, ef_Id) t",
            "fields" => array(
                "ID" => "",
                "SCHOOL_FK" => "",
                "EDUFORM_FK" => "",
                "NAME" => ""
            ),
            "function" => "islod_schools_eduforms"
        ),


        // ====================== ИСГА =========================== //
        "EIIS.COUNTRIES" => array(
            "table" => "eiis_Countries",
            "fields" => array(
                "ID"        => "Id",
                "NAME"      => "Name",
                "FULLNAME"  => "FullName",
                "CODE2"     => "Code2",
                "CODE3"     => "Code3",
            )
        ),
        "EIIS.EDUPROGRAMS" => array(
            "table" => "eiis_EduPrograms",
            "fields" => array(
                "ID"                => "Id",
                "CODE"              => "Code",
                "NAME"              => "Name",
                "EDULEVELFK"        => "fk_eiisEduLevels",
                "EDUPROGRAMTYPEFK"  => "fk_eiisEduProgramType",
                "GS_FK"             => "",
                "STANDARD_TYPE"     => "fk_eiisEduProgramType",
                "UGSCODE"           => "UgsCode",
                "UGSNAME"           => "UgsName",
                "UGS_FK"            => "fk_eiisUgs",
                "QUALIFICATIONCODE" => "QualificationCode",
                "QUALIFICATIONNAME" => "QualificationName",
                "QUALIFICATION_FK"  => "fk_eiisQualification",
                "QUALIFICATIONGRADE"=> "",
                "EDU_STANDARD_FK"   => "fk_eiisEduStandard",
                "PERIOD"            => "Period",
                "ACTUAL"            => "actual"
            ),
            "function" => "isga_eduprograms"
        ),
        "EIIS.SCHOOL_KINDS" => array(
            "table" => "eiis_EducationalOrganizationKinds",
            "fields" => array(
                "ID"              => "Id",
                "CODE"       => "Code",
                "NAME"       => "Name",
                "SCHOOL_TYPEFK"  => "fk_eiisEducationalOrganizationType"
            )
        ),
        "ISGA.APPLICATION_REASONS"=> array(
            "table" => "isga_ApplicationReasons",
            "fields" => array(
                "ID"=> "Id",
                "NAME"=>	 "Name"
            )
        ),
        "ISGA.EDU_STANDARDS" => array(
            "table" => "isga_EduStandards",
            "fields" => array(
                "ID" => "Id",
                "NAME" => "Name",
                "ABBREVIATION" => "Abbreviation"
            )
        ),
        "ISGA.ACCREDITATION_KINDS"=> array(
            "table" => "isga_AccreditationKinds",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"Code",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.ORDER_DOCUMENT_KINDS"=> array(
            "table" => "isga_OrderDocumentKinds",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"Code",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.ORDER_DOCUMENT_STATUS"=> array(
            "table" => "isga_OrderDocumentStatuses",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"Code",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.ORDER_DOCUMENTS"=> array(
            "table" => "isga_OrderDocuments",
            "fields" => array(
                "ID"=>	"Id",
                "ORDERDOCUMENTKINDID"=>	"fk_isgaOrderDocumentKind",
                "ORDERDOCUMENTSTATUSID"	=>"fk_isgaOrderDocumentStatus",
                "NUMBER"=>	"Number",
                "SIGNDATE"=>	"DateSign"
            ),
            "dateFields" => array(
                "DateSign"
            )
        ),
        "ISGA.APPLICATION_TYPES" => array(
            "table" => "isga_ApplicationTypes",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name",
                "SHORT_NAME"=>	"ShortName"
            )
        ),
        "ISGA.APPLICATION_STATUSES"=> array(
            "table" => "isga_ApplicationStatuses",
            "fields" => array(
                "ID"	=>"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name",
                "PUBLISH_NAME"=>	"PublishName",
                "DESCRIPTION"=>	"Description",
                "PUBLISHED"=>	"Published"
            )
        ),
        "ISGA.APPLICATIONS"=> array(
            "table" => "isga_Applications",
            "fields" => array(
                "ID"=>	"Id",
                "REG_NUMBER"=>	"RegNumber",
                "SUBMISSION_DATE"=>	"DateSubmission",
                "REG_DATE"=>	"DateReg",
                "APPLICATION_STATUS_FK"	=>"fk_isgaApplicationStatus",
                "APPLICATION_TYPE_FK"=>	"",
                "PREVIOUS_CERTIFICATE_FK"	=>"fk_isgaPreviousCertificate",
                "APPLICANT_PERSON_FULL_NAME"	=>"ApplicantPersonFullName",
                "APPLICANT_PERSON_FIRSTNAME"=>	"ApplicantPersonFirstName",
                "APPLICANT_PERSON_LASTNAME"=>	"ApplicantPersonLastName",
                "APPLICANT_PERSON_PATRONYMIC"=>	"ApplicantPersonPatronymic",
                "DECLARED_SCHOOLPROPERTY_FK"=>	"fk_eiisEOProperty_Declared",
                "DECLARED_SCHOOL_TYPE_FK"=>	"fk_eiisEOType_Declared",
                "DECLARED_SCHOOL_KIND_FK"=>	"fk_eiisEOKind_Declared",
                "DECLARED_REGULAR_NAME"=>	"DeclaredRegularName",
                "DECLARED_SHORT_NAME"=>	"DeclaredShortName",
                "ACCEPTANCE_TRANSFER_ACT_NUMBER"=>	"AcceptanceTransferActNumber",
                "ACCEPTANCE_TRANSFER_ACT_DATE"=>	"DateAcceptanceTransferAct",
                "RECALL_APPLICATION_REG_NUMBER"	=>"RecallApplicationRegNumber",
                "RECALL_DATE"=>	"DateRecall",
                "RECALL_DOCUMENTS_SEND_DATE"=>	"DateRecallDocumentsSend",
                "LICENSE_FK"=>	"fk_eiisLicense"
            ),
            "function" => "applications",
            "dateFields" => array(
                "DateSubmission",
                "DateReg",
                "DateAcceptanceTransferAct",
                "DateRecall",
                "DateRecallDocumentsSend"
            )
        ),
        "ISGA.APPLICATION_STATISTICS"=> array(
            "table" => "isga_Applications",
            "idField" => "Id",
            "select" => "SELECT t.fk_eiisEducationalOrganization as school, t.Name as ReasonName, count(*) as total,
count(CASE when t.fk_isgaApplicationStatus = '6b98c800-6a54-481c-ab0d-ce9abec4d336' THEN 1 END) as withdrawn,
count(CASE when t.fk_isgaApplicationStatus = '0d5bdef0-8d14-46bb-8c8a-0e5f8c1f6d18' THEN 1 END) as refused
FROM (SELECT a.Id, a.fk_isgaApplicationStatus, a.fk_eiisEducationalOrganization, ar.Name  FROM `isga_Applications` a
LEFT JOIN isga_mtm_Application_Reason mr ON a.Id = mr.fk_isgaApplication
LEFT JOIN isga_ApplicationReasons ar ON mr.fk_isgaApplicationReason = ar.Id
WHERE ar.Name not like '-' AND ar.Name not like '' AND a.fk_eiisEducationalOrganization is not null
GROUP BY a.Id) t
GROUP BY school, ReasonName",
            "count" => "SELECT count(*) as count
FROM
(SELECT t.fk_eiisEducationalOrganization as school, t.Name as ReasonName, count(*) as total,
count(CASE when t.fk_isgaApplicationStatus = '6b98c800-6a54-481c-ab0d-ce9abec4d336' THEN 1 END) as withdrawn,
count(CASE when t.fk_isgaApplicationStatus = '0d5bdef0-8d14-46bb-8c8a-0e5f8c1f6d18' THEN 1 END) as refused
FROM (SELECT a.Id, a.fk_isgaApplicationStatus, a.fk_eiisEducationalOrganization, ar.Name  FROM `isga_Applications` a
LEFT JOIN isga_mtm_Application_Reason mr ON a.Id = mr.fk_isgaApplication
LEFT JOIN isga_ApplicationReasons ar ON mr.fk_isgaApplicationReason = ar.Id
WHERE ar.Name not like '-' AND ar.Name not like '' AND a.fk_eiisEducationalOrganization is not null
GROUP BY a.Id) t
GROUP BY school, ReasonName) t2",
            "fields" => array(
                "ID"                    => "",
                "SCHOOL_FK"             => "",
                "INDICATOR"             => "",
                "VALUE_1_FILED"         => "",
                "VALUE_2_FAILURE"       => "",
                "VALUE_3_REVOCATION"    => ""
            ),
            "function" => "isga_application_statistics"
        ),
        "ISGA.APPLICATION_HISTORY"=> array(
            "table" => "isga_Applications",
            "idField" => "Id",
            "select" => "SELECT a.Id, a.fk_isgaApplicationStatus, a.fk_eiisEducationalOrganization, a.RegNumber, a.DateReg, ar.Name, aps.Name AS StatusName
FROM  `isga_Applications` a
LEFT JOIN isga_mtm_Application_Reason mr ON a.Id = mr.fk_isgaApplication
LEFT JOIN isga_ApplicationReasons ar ON mr.fk_isgaApplicationReason = ar.Id
LEFT JOIN isga_ApplicationStatuses aps ON a.fk_isgaApplicationStatus = aps.Id
WHERE ar.Name NOT LIKE  '-'
AND ar.Name NOT LIKE  ''
AND a.fk_eiisEducationalOrganization IS NOT NULL
GROUP BY a.Id",
            "count" => "SELECT count(*) as count
FROM
(SELECT a.Id, a.fk_isgaApplicationStatus, a.fk_eiisEducationalOrganization, ar.Name, aps.Name AS StatusName
FROM  `isga_Applications` a
LEFT JOIN isga_mtm_Application_Reason mr ON a.Id = mr.fk_isgaApplication
LEFT JOIN isga_ApplicationReasons ar ON mr.fk_isgaApplicationReason = ar.Id
LEFT JOIN isga_ApplicationStatuses aps ON a.fk_isgaApplicationStatus = aps.Id
WHERE ar.Name NOT LIKE  '-'
AND ar.Name NOT LIKE  ''
AND a.fk_eiisEducationalOrganization IS NOT NULL
GROUP BY a.Id) t2",
            "fields" => array(
                "ID"                => "",
                "SCHOOL_FK"         => "",
                "APLICATION_TYPE"   => "",
                "REG_NUMBER"        => "",
                "REG_DATE"          => "",
                "APLICATION_STATUS" => ""
            ),
            "function" => "isga_application_history"
        ),
        "ISGA.APPLICANTS"=> array(
            "table" => "isga_mtm_Application_Organizations",
            "fields" => array(
                "ID" => "Id",
                "APPLICATION_FK" => "fk_isgaApplication",
                "SCHOOL_FK" => "fk_eiisEducationalOrganization",
                "IS_DECLARER" => ""
            )
        ),
        "ISGA.CERTIFICATE_TYPES"=> array(
            "table" => "isga_CertificateTypes",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name"
            )
        ),
        "ISGA.CERTIFICATE_STATUSES"=> array(
            "table" => "isga_CertificateStatuses",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name"
            )
        ),
        "ISGA.CERTIFICATES"=> array(
            "table" => "isga_Certificates",
            "fields" => array(
                "ID"=>	"Id",
                "REESTR_ID"=>	"GUID",
                "REG_NUMBER"=>	"RegNumber",
                "ISSUE_DATE"=>	"DateIssue",
                "END_DATE"=>	"DateEnd",
                "FORM_NUMBER"=>	"FormNumber",
                "SERIAL_NUMBER"	=>"SerialNumber",
                "SCHOOL_FK"=>	"fk_eiisEducationalOrganization",
                "SCHOOL_FULL_NAME"	=>"EOFullName",
                "SCHOOL_SHORT_NAME"=>	"EOShortName",
                "INN"=>	"Inn",
                "OGRN"=> "",
                "APPLICATION_FK"=>	"fk_oisgaApplication",
                "CERTIFICATE_STATUS_FK"=>	"fk_isgaCertificateStatus",
                "CERTIFICATE_TYPE_FK"=>	"fk_isgaCertificateType",
                "SCHOOL_L_ADDRESS"=>	"EOAddressL",
                "CONTROL_ORGAN_HEAD_FULL_NAME"=>	"ControlOrganHeadFullName",
                "CONTROL_ORGAN_HEAD_POST"=>	"ControlOrganHeadPost",
                "ORDER_DOCUMENT_FK"=>	"fk_isgaOrderDocument",
                "ORDER_DOCUMENT_NUMBER"=>	"OrderDocumentNumber",
                "ORDER_DOCUMENT_SIGN_DATE"=>	"DateOrderDocumentSign",
                "ORDER_DOCUMENT_KIND_FK"=>	"fk_OrderDocumentKind",
                "ISSUE_TO_APPLICANT_DATE"=>	"DateIssueToApplicant",
                "CHECK_DATE"=>	"DateCheck",
                "READY_TO_ISSUE_DATE"=>	"DateReadyToIssue",
                "PRINT_DATE"=>	"DatePrint",
                "EDU_OBJECT_CHECK_STATUS_FK"=>	"fk_isgaCheckStatus",
                "SCHOOL_P_ADDRESS" => "",
                "EDUCATION_TYPE" => "",
                "NEW_UPDATE_DATE" => ""
            ),
            "function" => "certificates",
            "dateFields" => array(
                "DateIssue",
                "DateEnd",
                "DateOrderDocumentSign",
                "DateIssueToApplicant",
                "DateCheck",
                "DateReadyToIssue",
                "DatePrint"
            )
            //,"onlyOur" => true
        ),
        "ISGA.CERTIFICATE_SUPPLEMENT_STATUSES"=> array(
            "table" => "isga_CertificateSupplementStatuses",
            "fields" => array(
                "ID"=>	"Id",
                "CODE"=>	"Code",
                "NAME"=>	"Name"
            )
        ),
        "ISGA.CERTIFICATE_SUPPLEMENTS"=> array(
            "table" => "isga_CertificateSupplements",
            "fields" => array(
                "ID"=>	"Id",
                "CERTIFICATE_FK"=>	"fk_isgaCertificate",
                "SCHOOL_FK"=>	"fk_eiisEducationalOrganization",
                "SCHOOL_FULL_NAME"	=>"EOFullName",
                "SCHOOL_SHORT_NAME"=>	"EOShortName",
                "OGRN" => "",
                "INN"=>	"EOInn",
                "NUMBER"=>	"Number",
                "CERTIFICATE_SUPPLEMENT_STATUS_FK"=>	"fk_isgaCertificateSupplementStatus",
                "ORDER_DOCUMENT_FK"=>	"fk_isgaOrderDocument",
                "ISSUE_DATE"=>	"DateIssue",
                "FORM_NUMBER"	=>"FormNumber",
                "SERIAL_NUMBER"	=>"SerialNumber",
                "SCHOOL_L_ADDRESS"	=>"EOAddressL",
                "CONTROL_ORGAN_HEAD_FULL_NAME"	=>"ControlOrganHeadFullName",
                "CONTROL_ORGAN_HEAD_POST"=>	"ControlOrganHeadPost",
                "ORDER_DOCUMENT_NUMBER"=>	"OrderDocumentNumber",
                "ORDER_DOCUMENT_SIGN_DATE"	=>"DateOrderDocumentSign",
                "APPLICATION_FK"	=>"fk_isgaApplication",
                "ORDER_DOCUMENT_KIND_FK"=>	"fk_isgaOrderDocumentKind",
                "ACCREDITATION_KIND_FK"	=>"fk_isgaAccreditationKind",
                "ISSUE_TO_APPLICANT_DATE"	=>"DateIssueToApplicant",
                "CHECK_DATE"=>	"DateCheck",
                "PRINT_DATE"	=>"DatePrint",
                "EDU_OBJECT_CHECK_STATUS_FK"=>	"fk_isgaCheckStatus",
                "READY_TO_ISSUE_DATE"=>	"DateReadyToIssue",
                "SCHOOL_P_ADDRESS" => "",
                "SCHOOL_P_ADDRESS_POSTAL_CODE" => "",
                "EDUCATION_TYPE" => ""
            ),
            "function" => "certificate_supplements",
            "dateFields" => array(
                "DateIssue",
                "DateOrderDocumentSign",
                "DateIssueToApplicant",
                "DateCheck",
                "DatePrint",
                "DateReadyToIssue"
            )
            //,"onlyOur" => true
        ),
        "ISGA.ACCREDITED_PROGRAMS"=> array(
            "table" => "isga_AccreditedPrograms",
            "where" => [
                "IsNotAccredited != 1"
            ],
            "fields" => array(
                "ID" => "Id",
                "APPLICATION_FK" => "fk_oisgaApplication",
                "LICENSED_PROGRAM_FK" => "fk_eiisLicensedProgram",
                "CERTIFICATE_SUPPLEMENT_FK" => "fk_isgaCertificateSupplement",
                "SCHOOL_FK" => "fk_eiisEducationalOrganization",
                "CODE" => "Code",
                "NAME" => "Name",
                "EDUPROGRAM_FK" => "fk_eiisEduProgram",
                "EDUPROGRAM_TYPE_FK" => "fk_eiisEduProgramType",
                "OKSO_CODE" => "OksoCode",
                "START_YEAR" => "StartYear",
                "EDU_NORMATIVE_PERIOD" => "EduNormativePeriod",
                "FORM_DEGREE" => "FormDegree",
                "QUALIFICATION_CODE" => "",
                "QUALIFICATION_NAME" => "",
                "QUALIFICATION_FK" => "fk_isgaQualification",
                "GS_CODE" => "",
                "GS_NAME" => "",
                "GS_FK" => "fk_isgaGroupSpeciality",
                "UGS_CODE" => "",
                "UGS_NAME" => "",
                "UGS_FK" => "fk_isgaEnlargedGroupSpeciality",
                "EDULEVEL_NAME" => "",
                "EDUSUBLEVEL_NAME" => "",
                "EDULEVEL_FK" => "",
                "IS_UGS" => "",
                "SUSPENDED_PARTIALLY" => ""
            ),
            "function" => "accredited_programs"
        ),
        "ISGA.APP_TO_APPLICATION_REASONS"=> array(
            "table" => "isga_mtm_Application_Reason",
            "fields" => array(
                "ID"              => "Id",
                "APPLICATION_FK"     => "fk_isgaApplication",
                "APPLICATION_REASON_FK"   => "fk_isgaApplicationReason"
            )
        ),
        "ISGA.FOUNDERS"=> array(
            "table" => "isga_Founders",
            "fields" => array(
                "ID" =>	"Id",
                "FOUNDER_TYPE_FK" =>	"fk_isgaFounderType",
                "ORGANIZATION_FULLNAME" =>	"OrganizationFullName",
                "ORGANIZATION_SHORTNAME" =>	"OrganizationShortName",
                "LASTNAME" =>	"LastName",
                "FIRSTNAME" =>	"FirstName",
                "PATRONYMIC" =>	"Patronymic",
                "PHONES" =>	"Phones",
                "FAXES" =>	"Faxes",
                "EMAILS" =>	"Emails",
                "OGRN" =>	"Ogrn",
                "INN" =>	"Inn",
                "KPP" =>	"Kpp",
                "L_ADDRESS" =>	"LawAddress",
                "L_ADDRESS_COUNTRY_FK" => "fk_eiisCountry_L",
                "L_ADDRESS_REGION_FK" =>	"fk_eiisLawRegion",
                "L_ADDRESS_DISTRICT" =>	"LawDistrict",
                "L_ADDRESS_TOWN" =>	"LawTown",
                "L_ADDRESS_STREET" =>	"LawStreet",
                "L_ADDRESS_HOUSE_NUMBER" =>	"LawHouseNumber",
                "L_ADDRESS_POSTAL_CODE" =>	"LawPostalCode",
                "P_ADDRESS"	 =>"PAddress",
                "P_ADDRESS_COUNTRY_FK" => "fk_eiisCountry_P",
                "P_ADDRESS_REGION_FK" =>	"fk_eiisPRegion",
                "P_ADDRESS_DISTRICT" =>	"PDistrict",
                "P_ADDRESS_TOWN" =>	"PTown",
                "P_ADDRESS_STREET" =>	"PStreet",
                "P_ADDRESS_HOUSE_NUMBER" =>	"PHouseNumber",
                "P_ADDRESS_POSTAL_CODE" =>	"PPostalCode"
            )
        ),
        "ISGA.FOUNDERS_EDU_INSTITUTIONS" => array(
            "table" => "isga_mtm_Founders_EducationalOrganizations",
            "fields" => array(
                "ID" =>	"Id",
                "SCHOOL_FK" =>	"fk_eiisEducationalOrganization",
                "FOUNDER_FK" =>	"fk_isgaFounder"
            )
        ),
        "ISGA.FOUNDER_TYPES" => array(
            "table" => "isga_FounderTypes",
            "fields" => array(
                "ID" =>	"Id",
                "NAME" =>	"Name"
            )
        ),
        "ISGA.SCHOOLSPECIFIC"=> array(
            "table" => "isga_EducationalOrganizationSpecifics",
            "fields" => array(
                "ID"  => "Id",
                "NAME"      => "Name",
            )
        ),
        "ISGA.ACCREDITED_UGS"=> array(
            "table" => "isga_AccreditedPrograms",
            "idField" => "Id",
            "select" => "SELECT ap.fk_isgaEnlargedGroupSpeciality, el.Name, ap.fk_eiisEducationalOrganization, eg.Name as ugsName, eg.Code FROM f11_mon.isga_AccreditedPrograms ap
LEFT JOIN f11_mon.isga_CertificateSupplements cs on  ap.fk_isgaCertificateSupplement = cs.Id
LEFT JOIN f11_mon.eiis_EduProgramTypes et on ap.fk_eiisEduProgramType = et.Id
LEFT JOIN f11_mon.eiis_EduLevels el on et.fk_eiisEduLevels = el.Id
LEFT JOIN f11_mon.isga_EnlargedGroupSpecialities eg on ap.fk_isgaEnlargedGroupSpeciality = eg.Id

WHERE cs.fk_isgaCertificateSupplementStatus IN
    	('4C3C4932-D4C8-4524-83C5-0B5039403A09',
        '67F82AA6-EDF8-4115-BD3F-8A4F348782C1',
        'C9DFA661-3E08-42D6-9CDC-4187FB32128C',
        '67F82AA6-EDF8-4115-BD3F-8A4F348782C0')
AND ap.fk_isgaEnlargedGroupSpeciality is not null
AND  et.fk_eiisEduLevels IS NOT NULL
GROUP BY ap.fk_isgaEnlargedGroupSpeciality, el.Name, ap.fk_eiisEducationalOrganization
ORDER BY ap.fk_isgaEnlargedGroupSpeciality",
            "count"  => "SELECT count(*) as count FROM (SELECT ap.fk_isgaEnlargedGroupSpeciality, el.Name, ap.fk_eiisEducationalOrganization, eg.Name as ugsName, eg.Code FROM f11_mon.isga_AccreditedPrograms ap
LEFT JOIN f11_mon.isga_CertificateSupplements cs on  ap.fk_isgaCertificateSupplement = cs.Id
LEFT JOIN f11_mon.eiis_EduProgramTypes et on ap.fk_eiisEduProgramType = et.Id
LEFT JOIN f11_mon.eiis_EduLevels el on et.fk_eiisEduLevels = el.Id
LEFT JOIN f11_mon.isga_EnlargedGroupSpecialities eg on ap.fk_isgaEnlargedGroupSpeciality = eg.Id

WHERE cs.fk_isgaCertificateSupplementStatus IN
    	('4C3C4932-D4C8-4524-83C5-0B5039403A09',
        '67F82AA6-EDF8-4115-BD3F-8A4F348782C1',
        'C9DFA661-3E08-42D6-9CDC-4187FB32128C',
        '67F82AA6-EDF8-4115-BD3F-8A4F348782C0')
AND ap.fk_isgaEnlargedGroupSpeciality is not null
AND  et.fk_eiisEduLevels IS NOT NULL
GROUP BY ap.fk_isgaEnlargedGroupSpeciality, el.Name, ap.fk_eiisEducationalOrganization
ORDER BY ap.fk_isgaEnlargedGroupSpeciality) as t",
            "fields" => array(
                "ID"                => "",
                "SCHOOL_FK"         => "fk_eiisEducationalOrganization",
                "UGS_CODE"          => "Code",
                "UGS_NAME"          => "ugsName",
                "EDULEVEL_NAME"     => "Name",
            ),
            "function" => "isga_accredited_ugs",
        ),
        "EIIS.ISGA.SCHOOLS_ADDITIONAL  "=> array(
            "table" => "eiis_EducationalOrganizations",
            "idField" => "Id",
            "fields" => array(
                "ID"                => "Id",
                "AKKR1_INFO"        => "",
                "AKKR1_STATUS"      => "",
                "AKKR1_FK"          => "",
                "AKKR2_INFO"        => "",
                "AKKR2_STATUS"      => "",
                "AKKR2_FK"          => ""
            ),
            "function" => "isga_schools_additional",
        ),
        "ISGA.UGS" => array(
            "table" => "isga_EnlargedGroupSpecialities",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "STANDART" => "Standart",
                "ACTUAL" => "isActual"
            )
        ),
        "ISGA.DOCUMENT_REASON" => array(
            "table" => "isga_mtm_Document_Reason",
            "fields" => array(
                "ID"                                        => "Id",
                "FK_APPLICATION_REASONS"                    => "fk_isgaApplicationReasons",
                "FK_SCHOOLSPECIFIC"                         => "fk_isgaEducationalOrganizationSpecifics",
                "FK_DOCUMENTS_TYPES"                        => "fk_isgaDocumentTypes"
            )
        ),
        "ISGA.DOCUMENTS_TYPES" => array(
            "table" => "isga_DocumentTypes",
            "fields" => array(
                "ID"                                        => "Id",
                "NAME"                                      => "Name"
            )
        ),
        "ISGA.EDU_OBJECT_CHECK_STATUSES"=> array(
            "table" => "isga_EduObjectCheckStatuses",
            "fields" => array(
                "ID" =>	"Id",
                "NAME" =>	"Name"
            )
        ),
    ];


    private $old_correspondences = array(
        "ISGA.QUALIFICATIONS" => array(
            "table" => "isga_Qualifications",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "ACTUAL" => "IsActual"
            )
        ),
        "ISGA.UGS_EDITIONS" => array(
            "table" => "isga_EnlargedGroupSpecialityEditions",
            "fields" => array(
                'ID'                            => "Id",
                'NAME'                          => "Name"
            )
        ),
        "ISGA.UGS_EDITIONS" => array(
            "table" => "isga_EnlargedGroupSpecialityEditions",
            "fields" => array(
                'ID'                            => "Id",
                'NAME'                          => "Name"
            )
        ),
        "ISGA.GS" => array(
            "table" => "isga_GroupSpecialities",
            "fields" => array(
                "ID" => "Id",
                "CODE" => "Code",
                "NAME" => "Name",
                "UGS_FK" => "fk_isgaEGS"
            )
        ),
        "ISGA.EDUPROGRAMS" => array(
            "table" => "eiis_EduPrograms",
            "fields" => array(
                "ID" => "Id",
                "GS_FK" => "fk_eiisGS",
                "UGS_FK" =>	"fk_eiisUgs",
                "QUALIFICATION_FK" =>	"fk_isgaQualification",
                "EDU_STANDARD_FK" =>	"fk_eiisEduStandard"
            )
        ),
        //"ISGA.ADAPTERS",
        //"ISGA.SUBLEVELS",
        "ISGA.SCHOOLS" => array(
            "table" => "eiis_EducationalOrganizations",
            "fields" => array(
                "ID" =>	"Id",
                "LAW_POST_INDEX"=>	"LawPostIndex",
                "LAW_CITY_NAME"=>	"LawCity",
                "LAW_STREET"=>	"LawStreet",
                "LAW_HOUSE"=>	"LawHouse",
                "POST_INDEX"=>	"PostIndex",
                "TOWN_NAME"=>	"TownName",
                "STREET"=>	"Street",
                "HOUSE"=>	"House",
                "CONTACT_FIRST_NAME"=>	"ContactFirstName",
                "CONTACT_SECOND_NAME"=>	"ContactSecondName",
                "CONTACT_LAST_NAME"=>	"ContactLastName",
                "FOUNDERS"=>""
            )
        ),
        //"ISGA.SCHOOLS_ADDITIONAL",



        "ISGA.EDU_OBJECT_CHECK_STATUSES"=> array(
            "table" => "isga_EduObjectCheckStatuses",
            "fields" => array(
                "ID" =>	"Id",
                "CODE" =>	"",
                "NAME" =>	"Name"
            )
        ),
        //"ISGA.APPLICATION_STATISTICS",
        //"ISGA.APPLICATION_HISTORY",
        "ISGA.APPLICATIONS_ORDERS"=> array(
            "table" => "isga_Applications",
            "fields" => array(
                "ID" => "Id",
                "APPLICATION_FK" => "Id",
                "ORDER_DOCUMENT_FK" => "fk_isgaOrderDocument"
            )
        ),

        // ========================== ЛОД сущности
        "ISLOD.EDULEVELS" => array(
            "table" => "eiis_EduLevels",
            "fields" => array(
                "SYS_GUID" => "Id",
                "ID" => "Code",
                "NAME" => "Name",
                "SHORTNAME" => "ShortName",
                "EIIS_EDULEVEL_FK" => "Id",
                "ACTUAL" => ""
            ),
            "function" => "islod_edulevels"
        ),


        "EIIS.LICENSE_STATS" => array(
            "db"    => "lod",
            "table" => "LicenseRequest",
            "idField" => "id",
            "fields" => array(
                "SYS_GUID"          => "id",
                "APPLICANT"         => "organizationId",
                "SCHOOLFK"          => "organizationId",
                "STAGE"             => "",
                "STATUS"            => "",
                "LICENSING_PROCEDURE" => "licensingProcedureId",
                "REGNUM_TEXT"       => "number",
                "REGDATE"           => "fillingDate",
                "EXPERT"            => "",
                "SCHOOLNAME"        => "",
                "SHORTNAME"         => "",
                "SCHOOLPROPERTYFK"  => "",
                "SCHOOLTYPEFK"      => "",
                "SCHOOLKINDFK"      => "",
                "LAWADDRESS"        => "",
                "ADDRESS"           => "",
                "GOSREGNUM"         => "",
                "INN"               => "",
                "KPP"               => "",
                "BENEFICIARY"       => "",
                "BEN_SHORTNAME"     => "",
                "BENEFECIARY_ADDRESS" => "",
                "BEN_IMPL_ADDRESS"  => "",
                "OLD_LICENSEFK"     => "",
                "RENEW_LICENSEFK"   => ""
            ),
            "function" => "islod_license_request"
        ),
        "ISLOD.NOTICES" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent = '20a4674be28c4b36ad69de14a0e4c8df')"
            ],
            "fields" => array(
                "SYS_GUID"          => "id",
                "NUMBER"            => "registrationNumber",
                "NOTICE_TYPE"       => "documentTypeId",
                "STATUS"            => "stateCode",
                "LICENSE_STATFK"    => "",
                "SENDING_DATE"      => "sendDate",
                "RECIEVING_DATE"    => "receiveDate"
            ),
            "function" => "islod_notices",
            "dateFields" => array(
                "sendDate",
                "receiveDate"
            )
        ),

        "ISLOD.NOTICE_WITH_STAT" => array(
            "db"    => "lod",
            "table" => "document",
            "idField" => "id",
            "where" => [
                "documentTypeId IN (SELECT id FROM DocumentTypeCatalogItem WHERE parent = '20a4674be28c4b36ad69de14a0e4c8df')",
                "dealId IS NOT NULL"
            ],
            "function" => "islod_notice_with_stat",
            "fields" => array(
                "SYS_GUID" => "",
                "LICENSE_STATFK" => "",
                "NOTICESFK" => "id"
            )
        ),
        "ISLOD.EMPLOYEES" => array(
            "db"    => "lod",
            "table" => "Lod_User",
            "idField" => "uuid",
            "fields" => array(
                "SYS_GUID" => "uuid",
                "FIRSTNAME" => "firstName",
                "MIDDLENAME" => "middleName",
                "LASTNAME" => "lastName",
                "INITIALS" => ""
            )
        ),
    );

    private function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * @param $packageId
     */
    public function createPackage($packageId){
        try {
            $logger = new \Model\Logger\Logger("service", true);


            //Получаем объект с описанием пакета
            $logger->Log("Получаем объект с описанием пакета");
            $package = new \Model\Entities\isgaservicePackage($packageId);

            //Получаем тип
            $logger->Log("Получаем тип");
            $objectCode = $package->getField("ObjectCode");
            $logger->Log($objectCode);
            $corrObj = $this->_correspondences[$objectCode];

            //Объект для работы с таблицей
            $logger->Log("Объект для работы с таблицей");
            //$table = new \Model\Gateway\EntitiesTable($corrObj["table"]);

            //количество частей
            $logger->Log("количество частей");
            $parts = $package->getField("Capacity");

            $logger->log("parts:" . $parts);

            $logger->Log("Начинаем работать с частями");
            //==============код что запускает обработку пакетов через сокеты

            //Подготавливаем массив с параметрами для скрипта, в качестве параметров, будет айди пакета и номер части
            $params = array();
            for ($part = 1; $part <= $parts; $part++) {
                $params[] = array(
                    "part" => $part,
                    "packageId" => $packageId
                );
            }

            //Запускаем многопоточно генерацию файлов
            $logger->Log("количество ".count($params));
            $logger->Log("Запускаем многопоточно генерацию файлов");

            $mt = new \Service\Model\MultiThreading(30);
            $mt->setScriptName("service/index/mt");
            $mt->setParams($params);
            $mt->execute();
            $logger->Log("Конец многопоточной обработки");
            //====================

            $package->setField("State", "Done");
            $logger->Log("State: Done");
            try {
                $package->save(false, true);
            } catch (\Exception $e) {
                $logger->Log($e->getLine() . " - " . $e->getMessage());
            }
            $logger->Log("Конец обработки частей");
        } catch(\Exception $e){
            $logger = new \Model\Logger\Logger("service", true);
            $logger->Log($e->getLine()." - ".$e->getMessage());
        }
    }

    /**
     * Метод подготавливает часть пакета данных
     * @param $packageId
     * @param $part
     */
    public function createPackagePart($packageId, $part){
        $this->part = $part;
        try {
            $start = microtime(true);
            $onPart = 1500;
            $logger = new \Model\Logger\Logger("service", true);

            $package = new \Model\Entities\isgaservicePackage($packageId);
            $objectCode = $package->getField("ObjectCode");
            $corrObj = $this->_correspondences[$objectCode];

            //Объект для работы с таблицей
            if (isset($corrObj["table"]) && isset($corrObj["db"]) && $corrObj["db"] == "lod")
                $table = new \Model\Gateway\LodEntitiesTable($corrObj["table"]);
            else
                $table = new \Model\Gateway\EntitiesTable($corrObj["table"]);

            $logger->Log("Часть: " . $part);
            //Создаем в бд запись о части
            $packagePartObj = new \Model\Entities\isgaservicePackagePart();
            $packagePartObj->setFields(array(
                "fk_isgaservicePackage" => $packageId,
                "PartNumber" => $part
            ));
            $packagePartObj->save(true, true);
            //Запрашиваем часть данных

            $order = "Id ASC";
            if (isset($corrObj["idField"]))
                $order = $corrObj["idField"] . " ASC";

            //Если прописан весь селект
            if (isset($corrObj["select"])){
                $adapter = \Model\Gateway\DbAdapter::getInstance();
                $page = $part-1;
                $offset = $page*1500;
                $query = $corrObj["select"]." LIMIT 1500 OFFSET ".$offset;
                $results = $adapter->query($query, $adapter::QUERY_MODE_EXECUTE);
                $data = $results;
                //die;
            }
            //Если отдаем только наши сущности:
            else if (isset($corrObj["onlyOur"]) && $corrObj["onlyOur"]) {
                $data = $table->getAllEntities(array(
                    "page" => $part,
                    "onPage" => $onPart
                ), $order, array(
                    new \Zend\Db\Sql\Predicate\Expression("CHAR_LENGTH(Id) > 40")
                ))->getObjectsArray();
            } else {
                $where = [];
                if (isset($corrObj["where"]))
                    $where = $corrObj["where"];

                $data = $table->getAllEntities(array(
                    "page" => $part,
                    "onPage" => $onPart
                ), $order, $where)->getObjectsArray();
            }

            $logger->log("data count:" . count($data));

            //Обходим по всем данным, генерируем xml представление данных
            $xml = "<object>";
            foreach ($data as $item) {
                $xml .= $this->_generateItemXML($item, $corrObj, $part);
            }
            $xml .= "</object>";

            /*
            if($corrObj["function"] == "reestr_certificates"){
                $logger = new \Model\Logger\Logger("service",true);
                $logger->Log(print_r($this->svidPeriod, true));
            }*/

            $total = microtime(true) - $start;
            $logger->log($package->getField("ObjectCode").": Part " . $part . " Время: ".$total." Файл:" . $packagePartObj->getId() . '.tmp');


            $fp = fopen($this->filepath . '/service_tmp/' . $packagePartObj->getId() . '.tmp', 'w');
            fwrite($fp, $xml);
            fclose($fp);
            $total = microtime(true) - $start;
            $logger->log($package->getField("ObjectCode").": Part " . $part ." Ended: ".$total);
        } catch (\Exception $e){
            $logger = new \Model\Logger\Logger("service", true);
            $logger->Log($e->getLine()." - ".$e->getMessage());
        }
    }

    public function createExcelFile($objectCode, $onlyOur){
        return;
        $corrObj = $this->_correspondences[$objectCode];

        //Объект для работы с таблицей
        $table = new \Model\Gateway\EntitiesTable($corrObj["table"]);

        //количество частей
        $entities = $table->getAllEntities(null, null, array(
            new \Zend\Db\Sql\Predicate\Expression("CHAR_LENGTH(Id) > 40")
        ));

        //Обходим по всем данным, генерируем xml представление данных
        $xml = "<object>";
        foreach($entities as $item){
            $xml .= $this->_generateItemArray($item, $corrObj);
        }
        $xml .= "</object>";

        $fp = fopen($this->filepath . '/service_tmp/'.$packagePartObj->getId().'.tmp', 'w');
        fwrite($fp,$xml);
        fclose($fp);
    }

    /**
     * Метод генерирует xml представление объекта
     *
     * @param $item
     * @param $corrObj
     * @return string
     */
    private function _generateItemXML($item, $corrObj, $part){
        $corrFields = $corrObj["fields"];
        $xmlFields = array();
        if(isset($corrObj["select"]))
            $itemFields = $item;
        else
            $itemFields = $item->getFields();

        $returnXml = "<row>";

        //Проходим по полям
        foreach($corrFields as $key => $value){
            $xmlFields[$key] = "";
            if(isset($itemFields[$value])) {
                $xmlFields[$key] = $itemFields[$value];
            }
        }

        //смотрим, если определена функция, то выполняем ее
        $logger = new \Model\Logger\Logger("service", true);
        if(isset($corrObj["function"]) && method_exists($this,"func_".$corrObj["function"])) {
                $xmlFields = $this->{"func_" . $corrObj["function"]}($xmlFields, $item);
            //Если в функции произошла ошибка
            if($xmlFields == null){
                $logger->Log("Ошибка в функции подготовки объекта: ".$corrObj["function"]);
                die;
            }
        }

        //Проходим по получившимуся массиву с xml полями
        foreach($xmlFields as $key => $value){
            //Если значение даты в таком виде заменяем на null
            if($value=="0000-00-00")
                $value = "NULL";

            if($value=="0000-00-00 00:00:00")
                $value = "NULL";

            //Если значение даты пустая строка, то заменяем на null
            if($value=="")
                $value = "NULL";

            if($key == "ID")
                $returnXml.="<primary code='".$key."'><![CDATA[".$value."]]></primary>";
            else
                $returnXml.="<column code='".$key."'><![CDATA[".$value."]]></column>";
        }

        $returnXml.="</row>";
        return $returnXml;
    }

    /**
     * Метод генерирует массив представление объекта
     *
     * @param $item
     * @param $corrObj
     * @return string
     */
    private function _generateItemArray($item, $corrObj){
        $corrFields = $corrObj["fields"];
        $xmlFields = array();
        $itemFields = $item->getFields();

        $returnArray = array();

        //Проходим по полям
        foreach($corrFields as $key => $value){
            $xmlFields[$key] = "";
            if(isset($itemFields[$value]))
                $xmlFields[$key] = $itemFields[$value];
        }

        //смотрим, если определена функция, то выполняем ее
        if(isset($corrObj["function"]) && method_exists($this,"func_".$corrObj["function"])) {
            $xmlFields = $this->{"func_" . $corrObj["function"]}($xmlFields, $item);
        }

        //Проходим по получившимуся массиву с xml полями
        foreach($xmlFields as $key => $value){
            $returnArray[] = $value;
        }
        return $returnArray;
    }

    public function hasObjectCode($objectCode){
        return isset($this->_correspondences[$objectCode]);
    }

    public function getObjectData($objectCode){
        if(isset($this->_correspondences[$objectCode]))
            return $this->_correspondences[$objectCode];
        else
            return null;
    }

    /**=========================================================
     *  Функции для работы с полями
     *=========================================================*/

    private function func_certificates($fields, $item){
        $org = $item->getEducationalOrganization();
        if($org != null) {
            $fields["OGRN"] = $org->getField("GosRegNum");
            $fields["INSTITUTION_P_ADDRESS"] = $org->getField("Address");
        }

        return $fields;
    }

    private function func_certificate_supplements($fields, $item){
        $org = $item->getEducationalOrganization();
        if($org != null) {
            $fields["OGRN"] = $org->getField("GosRegNum");
            $fields["INSTITUTION_P_ADDRESS"] = $org->getField("Address");
            $fields["INSTITUTION_P_ADDRESS_POSTAL_CODE"] = $org->getField("PostIndex");
        }
        return $fields;
    }

    private function func_applications($fields, $item){
        $logger = new \Model\Logger\Logger();

        try {
            $reasons = $item->getReasons();
            if (count($reasons) > 0) {
                $reason = $reasons[0];
                $fields["APPLICATION_TYPE_FK"] = $reason->getField("fk_isgaApplicationType");
            }
        } catch (\Exception $e){
            $logger->log($e->getMessage());
        }

        return $fields;
    }

    private function func_accredited_programs($fields, $item){
        $fields["QUALIFICATION_CODE"] = $item->getQualificationCode();
        $fields["QUALIFICATION_NAME"] = $item->getQualificationName();

        $gs = $item->getGroupSpeciality();
        if($gs != null){
            $fields["GS_CODE"] = $gs->getField("Code");
            $fields["GS_NAME"] = $gs->getField("Name");
        }

        $ugs = $item->getEnlargedGroupSpeciality();
        if($ugs != null){
            $fields["UGS_CODE"] = $ugs->getField("Code");
            $fields["UGS_NAME"] = $ugs->getField("Name");
        }

        $eduLevel = $item->getEduLevel();
        if($eduLevel != null){
            $fields["EDULEVEL_NAME"] = $eduLevel->getField("Name");
            $fields["EDULEVEL_FK"] = $eduLevel->getId();
        }

        if(isset($fields["IS_ACCREDITED"]))
            if($item->getField("IsNotAccredited")=="1"){
                $fields["IS_ACCREDITED"] = "0";
            } else{
                $fields["IS_ACCREDITED"] = "1";
            }

        if($item->getField("IsSpecialitiesGroup")=="1"){
            $fields["IS_UGS"] = "1";
        } else{
            $fields["IS_UGS"] = "0";
        }

        if($item->isEmptyField("fk_isgaOrderDocumentsSuspended")){
            $fields["SUSPENDED_PARTIALLY"] = "0";
        } else{
            $fields["SUSPENDED_PARTIALLY"] = "1";
        }

        return $fields;
    }


    private function func_isga_accredited_ugs($fields, $item){
        //$logger = new \Model\Logger\Logger("service",true);
        $fields["ID"] = md5($item["fk_eiisEducationalOrganization"].$item["fk_isgaEnlargedGroupSpeciality"]);
        return $fields;
    }

    private function func_isga_application_statistics($fields, $item){
        $fields["ID"] = md5($item["school"].$item["ReasonName"]);
        $fields["SCHOOL_FK"] = $item["school"];
        $fields["INDICATOR"] = $item["ReasonName"];
        $fields["VALUE_1_FILED"] = $item["total"];
        $fields["VALUE_2_FAILURE"] = $item["refused"];
        $fields["VALUE_3_REVOCATION"] = $item["withdrawn"];
        return $fields;
    }

    private function func_isga_application_history($fields, $item){
        $fields["ID"] = md5($item["fk_eiisEducationalOrganization"].$item["Name"]);
        $fields["SCHOOL_FK"] = $item["fk_eiisEducationalOrganization"];
        $fields["APLICATION_TYPE"] = $item["Name"];
        $fields["REG_NUMBER"] = $item["RegNumber"];
        $fields["REG_DATE"] = $item["DateReg"];
        $fields["APLICATION_STATUS"] = $item["StatusName"];

        return $fields;
    }

    //ИСЛОД функции

    private function func_islod_edulevels($fields, $item){
        $fields["ACTUAL"] = "0";
        if(!$item->isEmptyField("Code"))
            $fields["ACTUAL"] = "1";

        return $fields;
    }

    private function func_islod_ugs($fields, $item){
        $edition = $item->getEdition();
        if($edition != null) {
            $fields["STANDART"] = $edition->getField("Name");
        }

        return $fields;
    }

    private function func_islod_eduprograms($fields, $item){
        $notActual = "0";
        if(!$item->isEmptyField("actual") && $item->getField("actual") == "1")
            $notActual = "1";

        $fields["ACTUAL"] = $notActual;

        return $fields;
    }

    private function func_islod_license_request($fields, $item){
        try {
            $logger = new \Model\Logger\Logger("service",true);

            $orgInfo = $item->getMainOrganizationInfo();
            $org = $item->getEducationalOrganization();

            if ($orgInfo) {
                $fields["SCHOOLNAME"] = $orgInfo->getField("fullTitle");
                $fields["SHORTNAME"] = $orgInfo->getField("shortTitle");
                $fields["SCHOOLPROPERTYFK"] = $orgInfo->getField("fk_eiisEducationalOrganizationProperties");
                $fields["ADDRESS"] = $orgInfo->getField("address");
                $fields["GOSREGNUM"] = $orgInfo->getField("ogrn");
                $fields["INN"] = $orgInfo->getField("inn");
                $fields["KPP"] = $orgInfo->getField("kpp");
                $fields["BENEFICIARY"] = $orgInfo->getField("fullTitle");
                $fields["BEN_SHORTNAME"] = $orgInfo->getField("shortTitle");
                $fields["BENEFECIARY_ADDRESS"] = $orgInfo->getField("address");
            }
            if ($org) {
                $fields["SCHOOLTYPEFK"] = $org->getField("fk_eiisEducationalOrganizationType");
                $fields["SCHOOLKINDFK"] = $org->getField("fk_eiisEducationalOrganizationKind");
                $fields["LAWADDRESS"] = $org->getField("LawAddress");
            }
            $deal = $item->getDeal();
            if ($deal != null) {
                $fields["STAGE"] = $deal->getField("stateCode");
                $fields["EXPERT"] = $deal->getField("expertId");
            }
            $fields["OLD_LICENSEFK"] = $item->getField("licenseId");
            $regTable = new \Model\Gateway\LodEntitiesTable("RegistryChange");
            $response = $regTable->getEntitiesByWhere([
                "licenseId" => $item->getField("licenseId")
            ]);
            if (count($response) > 0) {
                $fields["RENEW_LICENSEFK"] = $item->getField("licenseId");
            }
            //адреса
            if ($orgInfo) {
                $addreses = $orgInfo->getAdresWithPostArray();
                $values = array_values($addreses);
                $fields["BEN_IMPL_ADDRESS"] = implode("; ", $values);
            }
            return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license_request");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_islod_notices($fields, $item){
        $deal = $item->getDeal();
        if($deal)
            $fields["LICENSE_STATFK"] = $deal->getField("mainLicenseRequestId");
        return $fields;
    }

    private function func_islod_orders($fields, $item){
        $deal = $item->getDeal();
        if($deal) {
            if(isset($fields["STATFK"])) $fields["STATFK"] = $deal->getField("mainLicenseRequestId");
            if(isset($fields["LICENSE_STAT_FK"])) $fields["LICENSE_STAT_FK"] = $deal->getField("mainLicenseRequestId");
        }
        return $fields;
    }

    private function func_islod_notice_with_stat($fields, $item){
        $deal = $item->getDeal();

        $fields["LICENSE_STATFK"] = $deal->getField("mainLicenseRequestId");
        $fields["SYS_GUID"] = md5($fields["NOTICESFK"].$fields["LICENSE_STATFK"]);

        return $fields;
    }

    private function func_islod_order_with_stat($fields, $item){
        //$log = new \Model\Logger\Logger("serv",true);
        //$log->Log("====================");
        $deal = $item->getDeal();
        //$log->Log("1");
        if(isset($fields["LICENSE_STATFK"])) $fields["LICENSE_STATFK"] = $deal->getField("mainLicenseRequestId");
        if(isset($fields["LICENSE_STAT_FK"])) $fields["LICENSE_STAT_FK"] = $deal->getField("mainLicenseRequestId");
        //$log->Log("2");
        if(isset($fields["SYS_GUID"])) $fields["SYS_GUID"] = md5($fields["ORDERSFK"].$fields["LICENSE_STATFK"]);
        if(isset($fields["ID"] )) $fields["ID"] = md5($fields["ORDER_FK"].$fields["LICENSE_STAT_FK"]);
        //$log->Log("3");
        return $fields;
    }

    private function func_islod_licenses($fields, $item){
        try {
            $licStatus = $item->getStatus();
            if($licStatus!=null)
                $fields["LICENSE_STATUSESFK"] = $licStatus->getField("LodCode");

            $doc = $item->getDocumentFromId($item->getId());
            if($doc) {
                $fields["BASISTYPEDOCFK"] = $doc->getField('documentTypeId');
            }
            $deal = null;
            if ($doc && !empty($doc->getFieldOrSpace('dealId'))) {
                $deal = new \Model\LodEntities\Deal(
                    $doc->getFieldOrSpace('dealId')
                );
                if($deal!=null && $deal->getId()!=null){
                    $licRequest = $deal->getMainLicenseRequest();
                    if($licRequest!=null) {
                        $fields["LICENSESTATFK"] = $licRequest->getId();
                        $fields["LICENSEOLDFK"] = $licRequest->getField("licenseId");
                        //проверка на дупликат

                        if($licRequest->getField("isDuplicate") == 1){
                            $fields["DOUBLE"] = "1";
                            $fields["NUM_DATE_DUPL_ISSUE"] = "";
                            $fields["DATE_DUPL"] = "";
                            $fields["COPY_LICENSE_STATFK"] = $licRequest->getId();
                        }
                    }
                }
            }
            $endDoc = $item->getEndDocumentFromId($item->getId());
            if($endDoc) {
                $fields["DOC_STOPFK"] = $endDoc->getField("documentTypeId");
                $fields["REASON_OF_SUSPENSION"] = $endDoc->getField("registrationNumber");
                $fields["DATE_OF_SUSPENSION"] = $endDoc->getField("registrationDate");

                $docTypeId = $endDoc->getField('documentTypeId');
                $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem($docTypeId);
                $docType = $catalogItem->getField('title');
                $docRegDate = "";
                $docRegNumber = $endDoc->getField('registrationNumber');
                if( !$endDoc->isEmptyField("registrationDate")){
                    $docRegDate = $endDoc->getDate('registrationDate');
                    $docRegDate = ' от ' . $docRegDate . 'г.';
                }
                if( !empty($docRegNumber)){
                    $docRegNumber = ' №' . $docRegNumber;
                }
                $fields["ADM_SUSP_ORDERS"] = $docType . $docRegDate . $docRegNumber;
            }
            //
            $documents = $item->getOtherDocumentFromId($item->getId());
            $documentArr = [];
            foreach ($documents as $document) {
                $docType = '';
                $docRegDate = '';
                $docRegNumber = '';
                $link = '';
                if ($document != null) {
                    $rawDoc = $document;
                    $document = $document->toArray();
                    $docTypeId = $document['documentTypeId'];
                    $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem($docTypeId);
                    $docType = $catalogItem->getField('title');
                    $docRegDate = 'registrationDate';
                    $docRegNumber = $document['registrationNumber'];
                } else {
                    $license = $item;
                    $docType = $license->getFieldOrSpace('fk_eiisBaseDocTypes');
                    $docRegDate = 'DateLicDoc';
                    $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
                }
                switch (true) {
                    case!$rawDoc->isEmptyField($docRegDate):
                        $docRegDate = $rawDoc->getDate($docRegDate);
                        $docRegDate = ' от ' . $docRegDate . 'г.';
                    case!empty($docRegNumber):
                        $docRegNumber = ' №' . $docRegNumber;
                    default: break;
                }
                $documentArr[] = $docType . $docRegDate . $docRegNumber . $link;
            }
            $fields["ORG_SUSP_DECISIONS"] = implode("; ",$documentArr);
            return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license_request");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_islod_supplements($fields, $item){
        try {
            $suppStatus = $item->getStatusObject();
            if($suppStatus!=null)
                $fields["SUPPLEMENT_STATUSFK"] = $suppStatus->getField("lodCode");

            $locAddrList = $item->getLocationAdress();
            $addrList = [];
            if(count($locAddrList) > 0)
                foreach($locAddrList as $id => $addr){
                    $addrList[] = $addr->getField("postcode").", ".$addr->getField("address");
                }
            $fields["IMPL_ADDR"] = implode("; ", $addrList);
            $doc = $item->getDocumentFromId( $item->getId());
            if($doc != null) {
                $fields["BASISTYPEDOCFK"] = $doc->getField('documentTypeId');
            }
            $endDoc = $item->getEndDocumentFromId($item->getId());
            if($endDoc) {
                $fields["DOC_STOPFK"] = $endDoc->getField("documentTypeId");
                $fields["REASON_OF_SUSPENSION"] = $endDoc->getField("registrationNumber");
                $fields["DATE_OF_SUSPENSION"] = $endDoc->getField("registrationDate");
            }
            return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license_request");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_islod_schools($fields, $item){
        try {
            if (isset($fields["SCHOOLSUBPROPERTYFK"]) && !$item->isEmptyField("fk_eiisEducationalOrganizationProperties")) {
                $property = new \Model\Entities\eiisEducationalOrganizationProperties($item->getField("fk_eiisEducationalOrganizationProperties"));
                if ($property != null && $property->getId()!=null) {
                    if ($property->isEmptyField("parent")) {
                        $fields["SCHOOLPROPERTYFK"] = $property->getId();
                        $fields["SCHOOLSUBPROPERTYFK"] = "";
                    } else {
                        $fields["SCHOOLPROPERTYFK"] = $property->getField("parent");
                        $fields["SCHOOLSUBPROPERTYFK"] = $property->getId();
                    }
                }
            }
            $fields["REGION_COMPETENCE"] = "0";

            $orgLocTable = new \Model\Gateway\LodEntitiesTable("OrganizationLocation");

            $orgLocs = $orgLocTable->getAllEntities(null, null, [
                "organizationId" => $item->getId(),
                "addressType" => "OrganizationLocation"
            ])->getObjectsArray();

            if (count($orgLocs) > 0) {
                $orgLoc = $orgLocs[0];
                if (!$orgLoc->isEmptyField("settlementId")) {
                    $settlement = new \Model\LodEntities\Settlement($orgLoc->getField("settlementId"));
                    $fields["CITY"] = $settlement->getField("title");
                }
            }

            //
            $licenses = $item->getAllLicenses();
            $names = [];

            foreach ($licenses as $lic) {
                if (!in_array($lic->getField("EOName"), $names))
                    $names[] = $lic->getField("EOName");

            }

            $fields["RENAME"] = implode("; ", $names);
            //
            $addresses = [];
            $license = $item->getLicense();

            if ($license && $license->isValid()) {

                $supps = $license->getSupplements();

                foreach ($supps as $supp) {

                    if ($supp->isValid()) {

                        $addresses = array_merge($addresses, $supp->getLocationAdress());
                    }
                }
            }
            $addrStringList = [];
            foreach ($addresses as $addr) {
                $addrStringList[] = $addr->getField("postcode") . ", " . $addr->getField("address");
            }
            $fields["IMPL_ADDR"] = implode("; ", $addrStringList);
            //
            if(isset($fields["INSIDE_SCHOOL"])) {
                $table = new \Model\Gateway\LodEntitiesTable("OrganizationToReorganizedOrganization");
                $entities = $table->getEntitiesByWhere([
                    "reorganizedOrganizationId" => $item->getId()
                ])->getObjectsArray();
                if (count($entities) > 0) {
                    $ent = $entities[0];
                    $fields["INSIDE_SCHOOL"] = $ent->getField("organizationId");
                }
            }
            //
            /*
            $suppTable = new \Model\Gateway\EntitiesTable("eiis_LicenseSupplements");
            $suppTable->getAllEntities(null, )
                $lsups = $this->getLinkedItems(,array(
                    "fk_eiisBranch" => $this->fields["Id"]
                ));
            */
            //
            $license = $item->getLicense();
            if($license) {
                $endDoc = $license->getEndDocumentFromId($item->getId());
                if ($endDoc) {
                    $fields["STOPACTIVITYDATE"] = $endDoc->getField("registrationDate");
                    $deal = $endDoc->getDeal();
                    if ($deal)
                        $fields["STOP_STATFK"] = $deal->getField("mainLicenseRequestId");
                }
            }
            //должность
            if(!$item->isEmptyField("ChargePosition")){
                $post = new \Model\LodEntities\employeepost($item->getField("ChargePosition"));
                if($post)
                    $fields["CHARGEPOSITION"] = $post->getField("title");
            }

            return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_license_request");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_islod_schools_additional($fields, $item){
        try {
            $name = $item->getField("FullName");

            if (strpos(strtolower($name), 'государств') !== false && strpos(strtolower($name), 'негосударств') === false) {
                $fields["IS_GOS"] = "1";
            } else {
                $fields["IS_GOS"] = "0";
            }

            $fields["PROPERTY"] = $item->getOrgFormName();
    //--
            if(isset($fields["SCHOOL_CATEGORY"])) {
                $fields["SCHOOL_CATEGORY"] = "Обычная организация";
                if ($item->getField("isStrong") == "1")
                    $fields["SCHOOL_CATEGORY"] = "Военная организация";
                else if ($item->getField("isReligion") == "1")
                    $fields["SCHOOL_CATEGORY"] = "Религиозная организация";
            }


            switch($item->getField("fk_eiisEducationalOrganizationType")){
                case "4e27741606344006ac8a67c91a87d320":
                case "45FE134B2CCD1699C83C3DE6AABE0F85":
                    $fields["SCHOOL_AGR_TYPE"] = "Образовательная организация высшего образования";
                    break;
                case "21d0e2d45a2b404aae4d95a368f45443":
                case "001587DD0F0449F8DF5C66C6A715E308":
                    $fields["SCHOOL_AGR_TYPE"] = "Научная организация";
                    break;
                case "4FABD21CFE54438F9D7786AF0535B5D7":
                case "1F637C9D1AB1B4F791EDF33AF1EE0413":
                case "76f9e36d13084c1182539a5370694bdd":
                    $fields["SCHOOL_AGR_TYPE"] = "Организация среднего профессионального образования";
                    break;
                case "FBB1A5649C83725E96E11EC488C9120C":
                case "72C3BB75D35502D559B8897130218F92":
                case "66bee8d3283e429e97f84148d11c15ad":
                case "5f81d860bc4b49b492f0fd10e8b1234b":
                    $fields["SCHOOL_AGR_TYPE"] = "Образовательная организация дополнительного профессионального образования";
                    break;
                case "2203AD89BFAA447487D5DE5D37CE5F5F":
                case "2c9ac2188d924ede8e66e7a3eefb81ce":
                case "2E228BC664AF0AA9076060194BE04C2E":
                    $fields["SCHOOL_AGR_TYPE"] = "Общеобразовательная организация";
                    break;
                default:
                    $fields["SCHOOL_AGR_TYPE"] = "Другие";
                    break;
            }


            $license = $item->getLicense();

            if($license){
                $fields["LICENSE_INFO"] = "Лицензия ";
                if(!$license->isEmptyField("LicenseRegNum"))
                    $fields["LICENSE_INFO"] .= '№'.$license->getField('LicenseRegNum')." ";
                if(!$license->isEmptyField("DateLicDoc"))
                    $fields["LICENSE_INFO"] .= 'от '.$license->getDate("DateLicDoc");
                $fields["LICENSE_STATUS"] = $license->getStatusName();

                if(isset($fields["LICENSE_FK"])) $fields["LICENSE_FK"] = $license->getId();
            }
            if(isset($fields["STATUS"])){
                if($item->getField("lod_stateCode") == "VALID") $fields["STATUS"] = "Действует";
                else $fields["STATUS"] = "Не действует";
            }
            if(isset($fields["KINDNAME"])) $fields["KINDNAME"] = $item->getEducationalOrganizationKindName();
            return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Error: func_islod_schools_additional");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_islod_application_statistics($fields, $item){

        $fields["ID"] = md5($item["organizationId"].$item["indicator"]);
        $fields["SCHOOL_FK"] = $item["organizationId"];
        $fields["INDICATOR"] = $item["indicator"];
        $fields["VALUE_1_FILED"] = $item["total"];
        $fields["VALUE_2_FAILURE"] = $item["refused"];
        $fields["VALUE_3_REVOCATION"] = "0";
        return $fields;
    }

    private function func_islod_schools_eduforms($fields, $item){
        $fields["SCHOOL_FK"] = $item["organizationId"];
        $fields["EDUFORM_FK"] = $item["ef_Id"];
        $fields["NAME"] = $item["ef_Name"];

        $fields["ID"] = md5($item["organizationId"].$fields["EDUFORM_FK"]);
        return $fields;
    }

    private function func_islod_schools_programs($fields, $item){
        $fields["ID"] = md5($item["id"].$item["organizationId"].$item["QualName"]);
        $fields["SCHOOL_FK"] = $item["organizationId"];

        if(isset($fields["SUPPLEMENT_FK"])) $fields["SUPPLEMENT_FK"] = $item["LicenseSupplementID"];
        if(isset($fields["LICENSE_APP_FK"])) $fields["LICENSE_APP_FK"] = $item["LicenseSupplementID"];

        $fields["LICENSED_PROGAM_FK"] = $item["id"];
        $fields["EDUPROGRAM_FK"] = $item["EduProgramsID"];

        if(isset($fields["LICENSE_STATUS"])) $fields["LICENSE_STATUS"] = $item["EduProgramsID"];

        $fields["STANDART"] = $item["StandartName"];
        $fields["CODE"] = $item["Code"];
        $fields["NAME"] = $item["Name"];
        $fields["QUALIFICATION"] = $item["QualName"];
        $fields["EDULEVEL_FK"] = $item["educationLevelId"];
        $fields["EDULEVEL_NAME"] = $item["ELName"];
        $fields["COMMENT"] = "";
        $fields["UGS_CODE"] = $item["UgsCode"];
        $fields["UGS_NAME"] = $item["UgsName"];
        $fields["UGS_FK"] = $item["fk_eiisUgs"];

        //"IS_ACCREDITED"
        $fields["IS_ACCREDITED"] = "0";
        $accreditedProgram = \Model\Entities\isgaAccreditedPrograms::getByLicensedProgramFk($item["id"]);
        if($accreditedProgram != null){
            if($accreditedProgram->isCertificateSupplementValid())
                $fields["IS_ACCREDITED"] = "1";
        }

        return $fields;
    }

    private function func_islod_licensed_programs($fields, $item){
        $eduProgram = $item->getEduProgram();
        if($eduProgram != null){
            $fields["CODE"] = $eduProgram->getField("Code");
            $fields["NAME"] = $eduProgram->getField("Name");
            $fields["EDUPROGRAMTYPEFK"] = $eduProgram->getField("fk_eiisEduProgramType");
            $fields["PERIOD"] = $eduProgram->getField("Period");

            $standard = new \Model\Entities\isgaEduStandards($eduProgram->getField("fk_eiisEduStandard"));

            if($standard->getId()!= null) {
                $fields["STANDARD_TYPE"] = $standard->getField("Name");
            }
        }
        return $fields;
    }

    //Реестр функции

    private function func_reestr_licenses($fields, $item){
        try {
        $fields["ID"]                   = $item["lic_guid"];

        $isgaLicense = new \Model\Entities\eiisLicenses($item["lic_guid"]);

        $fields["LicOrganFk"]           = $item["lic_organ_guid"];

        $fields["LicOrgan"]             = $item["lorg_name"];

        $fields["FullOrgName"]          = $item["name"];
        $fields["ShortOrgName"]         = $item["short_name"];
        $fields["NameIP"]               = "";
        $fields["OGRN"]                 = $item["ogrn"];
        $fields["INN"]                  = $item["inn"];
        $fields["KPP"]                  = $item["kpp"];
        $fields["RegionFk"]             = $item["reg_guid"];
        $fields["Region"]               = $item["reg_name"];
        $fields["Address"]              = $item["address"];
        $fields["LicDate"]              = $item["lic_create_date"];
        $fields["LicNumber"]            = $item["number"];
        $fields["LicBlankSer"]          = $item["doc_serie"];
        $fields["LicBlankNum"]          = $item["doc_number"];
        $fields["LicEndDate"]           = $item["end_date"];
        $fields["VipiskaFile"]       = "http://isga.obrnadzor.gov.ru/eo/index/license-excerpt-no-program/".$item["lic_guid"];

        if($isgaLicense->getField("Duplicate") == "1")
            $fields["IsDubl"] = "1";
        else
            $fields["IsDubl"] = "0";


        $fields["DocNumber"] = $isgaLicense->getField("NumLicDoc");
        $fields["DocDate"] = $isgaLicense->getField("DateLicDoc");

        //док
        $doc = $isgaLicense->getDocumentFromId($isgaLicense->getId());
        if($doc) {
            $docType = new \Model\LodEntities\DocumentTypeCatalogItem($doc->getField('documentTypeId'));
            if($docType) {
                if ($docType->getField("signatureLevelCode") == "1")
                    $fields["DocTypeName"] = "Приказ";
                else if ($docType->getField("signatureLevelCode") == "2")
                    $fields["DocTypeName"] = "Распоряжение";
            }
            $fields["DocNumber"] = $doc->getField("registrationNumber");
            $fields["DocDate"] = $doc->getField("registrationDate");
        }

        $fields["LicStatusFk"] = $item["lic_state_id"];
        $fields["LicStatus"] = $item["lic_state_name"];


        $endDoc = $isgaLicense->getEndDocumentFromId($isgaLicense->getId());
        if($endDoc) {
            $fields["TerminateBase"] = $endDoc->getField("registrationNumber");
            $fields["TerminateDate"] = $endDoc->getField("registrationDate");

            $docTypeId = $endDoc->getField('documentTypeId');
            $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem($docTypeId);
            $docType = $catalogItem->getField('title');
            $docRegDate = "";
            $docRegNumber = $endDoc->getField('registrationNumber');
            if( !$endDoc->isEmptyField("registrationDate")){
                $docRegDate = $endDoc->getDate('registrationDate');
                $docRegDate = ' от ' . $docRegDate . 'г.';
            }
            if( !empty($docRegNumber)){
                $docRegNumber = ' №' . $docRegNumber;
            }
            $fields["AdmSuspInfo"] = $docType . $docRegDate . $docRegNumber;
        }

        //
        $documents = $isgaLicense->getOtherDocumentFromId($isgaLicense->getId());
        $documentArr = [];
        foreach ($documents as $document) {
            $docType = '';
            $docRegDate = '';
            $docRegNumber = '';
            $link = '';
            if ($document != null) {
                $rawDoc = $document;
                $document = $document->toArray();
                $docTypeId = $document['documentTypeId'];
                $catalogItem = new \Model\LodEntities\DocumentTypeCatalogItem($docTypeId);
                $docType = $catalogItem->getField('title');
                $docRegDate = 'registrationDate';
                $docRegNumber = $document['registrationNumber'];
            } else {
                $license = $item;
                $docType = $license->getFieldOrSpace('fk_eiisBaseDocTypes');
                $docRegDate = 'DateLicDoc';
                $docRegNumber = $license->getFieldOrSpace('NumLicDoc');
            }
            switch (true) {
                case!$rawDoc->isEmptyField($docRegDate):
                    $docRegDate = $rawDoc->getDate($docRegDate);
                    $docRegDate = ' от ' . $docRegDate . 'г.';
                case!empty($docRegNumber):
                    $docRegNumber = ' №' . $docRegNumber;
                default: break;
            }
            $documentArr[] = $docType . $docRegDate . $docRegNumber . $link;
        }
        $fields["SuspResumpDecisions"] = implode("; ",$documentArr);

        $fields["InspectionInfo"] = "";



        $fields["AnnulDecision"] = "";
        $fields["SchoolFk"] = $item["guid"];
        $partsOrgId = explode("_",$item["guid"]);
        if(count($partsOrgId)==2)
            $fields["SchoolFk"] = $partsOrgId[1];

        return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_reestr_supplements($fields, $item){
        try {
        $fields["ID"]                = $item["guid"];
        $fields["LicenseFk"]         = $item["license_guid"];
        $fields["SupNumber"]         = $item["number"];
        $fields["SupBlankSer"]       = $item["doc_serie"];
        $fields["SupBlankNum"]       = $item["doc_number"];
        $fields["FullOrgName"]       = $item["eo_name"];
        $fields["ShortOrgName"]      = $item["eo_short_name"];
        $fields["Address"]           = $item["eo_address"];
        $fields["ImplAddresses"]     = $item["addresses"];

        $supplement = new \Model\Entities\eiisLicenseSupplements($item["guid"]);


        $doc = $supplement->getDocumentFromId( $supplement->getId());

        if($doc != null) {
            $docType = new \Model\LodEntities\DocumentTypeCatalogItem($doc->getField('documentTypeId'));
            if($docType) {
                if($docType->getField("signatureLevelCode")=="1")
                    $fields["DocTypeName"] = "Приказ";
                else if($docType->getField("signatureLevelCode")=="2")
                    $fields["DocTypeName"] = "Распоряжение";
                else $fields["DocTypeName"] = $docType->getField("title");
            }
        }
        $fields["DocNumber"]         = $supplement->getField("NumLicDoc");
        $fields["DocDate"]           = $supplement->getField("DateLicDoc");
        if($supplement->getField("Duplicate") == "1")
            $fields["IsDubl"] = "1";
        else
            $fields["IsDubl"] = "0";
        $fields["SupStatusFk"] = $item["state_code"];
        $fields["SupStatus"]   = $item["state_name"];
        if($item["state_name"] == null){
            switch($item["state_code"]){
                case "VALID":
                    $fields["SupStatus"]   =  'Действует';
                    break;
                case "NOT_VALID":
                    $fields["SupStatus"]   =  'Не действует';
                    break;
                case "SUSPENDED":
                    $fields["SupStatus"]   =  'Приостановлено';
                    break;
                case "ANNULLED":
                    $fields["SupStatus"]   =  'Аннулировано';
                    break;
                case "DRAFT":
                    $fields["SupStatus"]   =  'Проект';
                    break;
            }
        }
        /*
        $suppStatus = $supplement->getStatusObject();
        if($suppStatus!=null) {
            $fields["SupStatusFk"] = $suppStatus->getField("lodCode");
            $fields["SupStatus"]   = $suppStatus->getField("Name");
        }
        */
        $endDoc = $supplement->getEndDocumentFromId($supplement->getId());
        if($endDoc) {
            $fields["TerminateBase"] = $endDoc->getField("registrationNumber");
            $fields["TerminateDate"] = $endDoc->getField("registrationDate");
        }

        $fields["SchoolFk"] = $item["eo_guid"];
        $partsOrgId = explode("_",$item["eo_guid"]);
        if(count($partsOrgId)==2)
            $fields["SchoolFk"] = $partsOrgId[1];

        return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_reestr_programs($fields, $item){
        try{
        $fields["ID"]                = $item["guid"];
        $fields["SupplementFk"]      = $item["supplement_guid"];

        $lodLicProgram = new \Model\LodEntities\EducationProgram($item["guid"]);

        $fields["LevelFk"]           = $lodLicProgram->getField("educationLevelId");
        $fields["Level"]             = $item["level"];
        $fields["Code"]              = $item["code"];
        $fields["Name"]              = $item["name"];
        $fields["Kind"]              = $item["type"];
        $fields["Qualification"]     = $item["qualification"];
        $fields["ProgramFk"]         = $lodLicProgram->getField("EduProgramsID");

        return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_reestr_certificates($fields, $item){
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
//$logger = new \Model\Logger\Logger("service",true);
//$logger->Log($this->part.":".++$this->current);
        try {
//$logger = new \Model\Logger\Logger("service",true);
//$logger->Log("======");
//$logger->Log("1");
            $fields["ID"]            = $item["Id"];


            $fields["FullOrgName"]   = $item["EduOrgFullName"];
            $fields["NameIP"]        = "";
            $fields["OrgRukFio"]     = $item["eo_HeadName"];
            $fields["OrgRukPost"]    = "";
            //должность
            if($item["eo_HeadPost"]!="" && $item["eo_HeadPost"]!=null){
                $post = new \Model\LodEntities\employeepost($item["eo_HeadPost"]);
                if($post)
                    $fields["OrgRukPost"] = $post->getField("title");
            }


            $fields["OGRN"]          = $item["EduOrgOGRN"];
            $fields["INN"]           = $item["EduOrgINN"];
            $fields["Phone"]         = $item["eo_Phone"];
            $fields["Fax"]           = $item["eo_Fax"];
            $fields["Mail"]          = $item["eo_Email"];
            $fields["WebSite"]       = $item["Www"];
            $fields["VipiskaFile"]       = "http://isga.obrnadzor.gov.ru/eo/index/certificate-excerpt-no-program/".$item["Id"];

            $fields["RegionFk"]      = $item["RegionId"];
            $fields["Region"]        = $item["regName"];
//$logger->Log("5");
            $fields["Address"]       = $item["eo_PostAddress"];
            $fields["SvidNumber"]    = $item["RegNumber"];

            $fields["SvidDate"]      = $item["IssueDate"];
            $fields["SvidEndDate"]   = $item["EndDate"];

            $fields["SvidBlankSer"]  = $item["SerialNumber"];
            $fields["SvidBlankNum"]  = $item["FormNumber"];

            $fields["SvidStatusFK"]  = $item["statusId"];
            $fields["SvidStatus"]    = $item["statusName"];

            $fields["SchoolFk"]      = $item["EduOrgId"];

            $fields["AkkredOrgan"] = $item["ControlOrgan"];
            $controlOrgan = \Model\Entities\eiisControlOrgans::getByName($item["ControlOrgan"]);
            if($controlOrgan!=null){
                $fields["AkkredOrganFK"] = $controlOrgan->getId();
            }

//$logger->Log("6");
            $certificate = new \Model\Entities\isgaCertificates($item["Id"]);
            if($certificate->getId()==null){
                $certificate = \Model\Entities\isgaCertificates::getByGuid($item["Id"]);
            }


            if($certificate != null) {
                $orderKind = new \Model\Entities\isgaOrderDocumentKinds($certificate->getField("fk_OrderDocumentKind"));
//$logger->Log("6.1");
                if ($orderKind) {
//$logger->Log("6.2");
                    $fields["DocTypeName"] = $orderKind->getOrderDocumentType();
//$logger->Log("6.3");
                }
//$logger->Log("7");
                $fields["DocNumber"] = $certificate->getField("OrderDocumentNumber");
                $fields["DocDate"] = $certificate->getField("DateOrderDocumentSign");

                $fields["OrgULFio"] = $certificate->getField("ControlOrganHeadFullName");
                $fields["OrgULPost"] = $certificate->getField("ControlOrganHeadPost");

//$logger->Log("7.1");
                $fields["SvidPeriod"] = $certificate->getCertificatePeriod();

                /*
                if($fields["SvidPeriod"] == ""){
                    $logger2 = new \Model\Logger\Logger("service_noPeriod",true);
                    $logger2->Log($item["Id"]);
                }*/

//$logger->Log("8");
                /*
                if (!isset($this->svidPeriod[$fields["SvidPeriod"]]))
                    $this->svidPeriod[$fields["SvidPeriod"]] = 0;
                $this->svidPeriod[$fields["SvidPeriod"]]++;
                */
            } /*else {
                $logger2 = new \Model\Logger\Logger("service_nocert",true);
                $logger2->Text(',"'.$item["Id"].'"');
            }*/

//$logger->Log("9");
            return $fields;
        } catch(\Exception $e) {
            $logger = new \Model\Logger\Logger("service",true);
            $logger->Log("================================");
            $logger->Log("Line: ".$e->getLine()." Error:".$e->getMessage());
            $logger->Log("--------------------------------");
            return null;
        }
    }

    private function func_reestr_certificate_supplements($fields, $item){
        $fields["ID"]                = $item["id"];
        $fields["CertificateFk"]     = $item["CertificateId"];
        $fields["SupNumber"]         = $item["Number"];
        $fields["SupDate"]           = $item["IssueDate"];
        $fields["SupBlankSer"]       = $item["SerialNumber"];
        $fields["SupBlankNum"]       = $item["FormNumber"];
        $fields["FullOrgName"]       = $item["EduOrgFullName"];

        $supplement = new \Model\Entities\isgaCertificateSupplements($item["id"]);
        if($supplement) {
            $orderType = new \Model\Entities\isgaOrderDocumentKinds($supplement->getField("fk_isgaOrderDocumentKind"));
            if ($orderType)
                $fields["DocTypeName"] = $orderType->getOrderDocumentType();

            $fields["DocNumber"]         = $supplement->getField("OrderDocumentNumber");
            $fields["DocDate"]           = $supplement->getField("DateOrderDocumentSign");
        }

        $fields["SupStatus"]         = $item["StatusName"];

        $fields["SupStatusFk"]       = $item["CertificateSupplementStatusId"];
        $fields["SchoolFk"]          = $item["EduOrgId"];
        return $fields;
    }

    private function func_reestr_accr_programs($fields, $item){
        $fields["ID"]                = $item["Id"];
        $fields["SupplementFk"]      = $item["CertificateSupplementId"];
        $fields["Level"]             = $item["levelName"];
        $fields["CodeUgsn"]          = $item["UGSCode"];
        $fields["NameUgsn"]          = $item["UGSName"];
        $fields["CodeNp"]            = $item["ProgrammCode"];
        $fields["NameNp"]            = $item["ProgrammName"];

        $fields["DirectionStatus"]   = "Действует";
        if($item["Suspended"] == 1)
            $fields["DirectionStatus"]   = "Приостановлено";
        if($item["Canceled"] == 1)
            $fields["DirectionStatus"]   = "Лишение";

        return $fields;
    }
}