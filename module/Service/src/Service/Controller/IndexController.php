<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Service\Controller;

use Service\Model\ServiceHelper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    private $serviceLink =
    //"http://85.192.47.4/service";
    "http://isga.obrnadzor.gov.ru/service";
    //"http://10.0.10.10/service";
    //"http://vuzi/service";

    private $wsdlLink =
    //"http://85.192.47.4/wsdl_test.xml";
    "http://isga.obrnadzor.gov.ru/wsdl.xml";
    //"http://10.0.10.10/wsdl.xml";
    //"http://vuzi/wsdl_home.xml"; //"http://isga.obrnadzor.gov.ru/wsdl.xml";
    //"http://eiis.obrnadzor.gov.ru/integrationServices/BaseService.asmx?wsdl";
    //"http://eiis-production.srvdev.ru/IntegrationService/BaseService.asmx?wsdl";

    public function indexAction()
    {
        ini_set("memory_limit", "10000M");
        set_time_limit(0);
        ini_set('soap.wsdl_cache_enabled',0);
        ini_set('soap.wsdl_cache_ttl',0);

        $logger = new \Model\Logger\Logger();
        $logger->Log($_SERVER['REMOTE_ADDR']);

        if (array_key_exists('wsdl', $this->request->getQuery()) || array_key_exists('WSDL', $this->request->getQuery())) {

            try {
                $soapAutoDiscover = new \Zend\Soap\AutoDiscover(new \Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence());
                $soapAutoDiscover->setBindingStyle(array('style' => 'document'));
                $soapAutoDiscover->setOperationBodyStyle(array('use' => 'literal'));
                $soapAutoDiscover->setClass('\Service\Model\Service');
                $soapAutoDiscover->setUri($this->serviceLink);//);
                //so this is:
                //header("Content-Type: text/xml");
                $soapAutoDiscover->handle();//generate()->toXml();

            } catch(\Exception $e){
                echo $e->getMessage();
            }
            die;
        } else {
            try {

                //Пробуем получить контент тайп, если контент тайп
                $request = $this->getRequest();
                $contentType = $request->getHeaders("Content-Type");
                //проверяем на совпадение с application/soap+xml, если да, то это обращение к сервису
                if(isset($contentType->value) && strpos($contentType->value, "text/html") == false){

                    $service = new \Service\Model\Service();
                    $soap = new \Zend\Soap\Server($this->wsdlLink);
                    $soap->setObject(new \Zend\Soap\Server\DocumentLiteralWrapper($service));

                    $return = $soap->handle();
                    $logger->Log($contentType->value);
                    die;

                }
                //Иначе отображаем хтмл страницу, что мол это сервис и он работает
                else {
                    $this->getServiceLocator()->get('viewhelpermanager')->get('headLink')->appendStylesheet('/css/auth.css');
                }

            } catch (\Exception $e){
                $logger = new \Model\Logger\Logger();
                $logger->Log($e->getMessage() ." ".$e->getTraceAsString());
            }
        }
    }

    public function testAction(){
        die;
        $logger = new \Model\Logger\Logger("service", true);
        $logger->Log("Старт скрипта создания пакета в отдельном потоке");
        ini_set('soap.wsdl_cache_enabled',0);
        ini_set('soap.wsdl_cache_ttl',0);
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors',1);
        ini_set('display_startup_errors', 1);
        ini_set('default_socket_timeout', 720);
        set_time_limit(0);
        //Инициируем клиент
        //try {
            $client = new \Model\EiisService\ServiceClient(array(
                "wsdl" => "http://isga.obrnadzor.gov.ru/service?wsdl",
                          //"http://vuzi/service?wsdl",
                          //"http://85.192.47.4/service?wsdl",//
                          //"http://eiis-production.srvdev.ru/IntegrationService/BaseService.asmx?wsdl",
                "login" => "isga_service_user",
                "pass" => "s4g0b7c9",
                "debug" => true
            ));
            //$client->GetSessionId();
        $start = microtime(true);
        $packetId = $client->CreatePackage("ISLOD.SCHOOLS_REORGANIZATIONS");
        $total = microtime(true) - $start;
        var_dump("Время:".$total);
            //$packetId = $client-> CreatePackage("ISLOD.SUPPLEMENTS");
            //var_dump($client->);
            //$packetId = $client->CreatePackage("ISGA.CERTIFICATES");
            //$packetId = $client->CreatePackage("ISGA.ACCREDITED_PROGRAMS");
        sleep(100);
$parts = $client->GetPackageMeta($packetId);
            //echo "<br>parts:".$parts;
$response = $client->GetPackage($packetId, "1", true);
            //echo "<textarea>".$response."</textarea>";
            //$response = $client->GetPackage($packetId, "1", true);
            //echo "<textarea>".$response."</textarea>";
            //$client->SetOk($packetId);
/*
        } catch (\SoapFault $e){
            //var_dump($e);
            echo "<br>=====================<br>";
            var_dump($e->getMessage());
            echo "<br>=====================<br>";
            var_dump($e->getTraceAsString());
        }*/
        die;
    }

    public function mtAction(){
        $params = $this->params()->fromQuery();
        if(isset($params["packageId"]) && isset($params["part"])){
            $sh = new ServiceHelper();
            $sh->createPackagePart($params["packageId"],$params["part"]);
            echo "done";
            die;
        }
        echo "error"; die;
    }

    public function startPackageAction(){
        set_time_limit(0);
        $params = $this->params()->fromQuery();
        if(isset($params["packageId"])) {
            $packageId = $params['packageId'];
            $sh = new \Service\Model\ServiceHelper();
            $sh->createPackage($packageId);
            die;
        }
        echo "error"; die;
    }

    public function getDssFileAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            //получаем данные из POST запроса
            $filename = $_POST['filename'];
            $filecontent = $_POST['filecontent'];
            //декодируем содержимое файла
            $decoded = base64_decode($filecontent);
            //сохраняем его на диске
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/public/' . $filename, $decoded);
            $realpath = realpath('./public/');
            echo sprintf("Подписанный файл '%s' успешно сохранён в директории '%s'", $filename, $realpath);


        }else { echo 'неверный http метод'; }

        die;
    }

    public function expertExchangeAction(){
        if($this->params("id")!=null) {
            $Ids =  explode(",",$this->params("id"));
            $exchanger = new \Model\SyncService\SyncExpertModule();
            $exchanger->startExchange($Ids);
        }
        die;
    }

    public function ntAction(){
        $se = new \Model\EiisService\ServiceEntities([
            'wsdl' => 'http://isga.obrnadzor.gov.ru/service?wsdl',
            'debug' => true,
            'login' => 'isga_service_user',
            'pass' => 's4g0b7c9'
        ]);

        $se->getEntitiesFromService("ISGA.SCHOOLS", 120);
        die;
    }

    /**
     * проверим подпись заслоного файла
     * в файле 'file' - файл который проверяем
     *
     * url:  /service/index/test-sign-file
     *
     * @return \Zend\View\Model\JsonModel
     *      'status'    -   ok    -  добро
     *      'status'    -   error -  с ошибкой
     *      'result'    -   описание ошибки
     */
    public function testSignFileAction(){
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);

        $result = array();
        $isDebugPrintFiles          = (boolean)$this->getRequest()->getQuery('isDebugPrintFiles', false);
        $isDebugPrintFileNameNew    = (boolean)$this->getRequest()->getQuery('isDebugPrintFileNameNew', false);
        $isDebugPrintResult         = (boolean)$this->getRequest()->getQuery('isDebugPrintResult', false);
        $isExFunction               = (boolean)$this->getRequest()->getQuery('isExFunction', true);
        $fileName                   = $this->getRequest()->getQuery( 'fileName' );
        //подготовка логирования
        $toPath = $_SERVER['DOCUMENT_ROOT'].'/'.'public/log/sign_validing';
        if( !is_dir( $toPath )){
            mkdir( $toPath ,0777 , true );
        }
        $logFile = 'public/log/sign_validing/sign_validing_'.date('Y-m-d').'.log';
        $logerClass = new \Eo\Model\SendMail();
        //print_r( $fileName );
        //print_r( $isExFunction );
        //print_r( $_GET );
        //die;
        $logerClass->mailLog('', $logFile );
        $logerClass->mailLog('', $logFile );
        $logerClass->mailLog('начинаем ', $logFile );
        $logerClass->mailLog("\t\t".'параметр - расширеный '. (int)$isExFunction , $logFile );
        try{
            $result[ 'status' ] = 'error';
            $result[ 'result' ] = 'Не приехал файл';
            $files = $this->getRequest()->getFiles();
            if( $isDebugPrintFiles ){
                print_r($files);
            }
            if( !empty($files['file']) ){
                $logerClass->mailLog( "\t\t".'проверяем из файла '. print_r( $files['file'] , true ) , $logFile );
                $fileOne = $files['file'];
                $tmpName = $fileOne['tmp_name'];
                $uniq = uniqid();
                $tmpNameNew = sys_get_temp_dir ().'/'.$uniq.'_'.$fileOne['name'];
                if( $isDebugPrintFileNameNew ){
                    print_r($tmpNameNew);
                }
                $logerClass->mailLog( "\t\t".'переименуем в  файла '. $tmpNameNew , $logFile );
                if( rename($tmpName,  $tmpNameNew) ){
                    $sign_class = new \Eo\Service\Sign();
                    $result = $sign_class->validSignature( $tmpNameNew, true, true , $isExFunction );
                    @unlink($tmpNameNew);
                    $logerClass->mailLog( "\t\t".'проверено, резултат файла '. print_r( $result , true ) , $logFile );
                }
                else{
                    $result[ 'result' ] = 'не смогло перенести файл';
                }
            }
            else{
                $logerClass->mailLog( "\t\t".'проверяем из хранилища '. print_r( $fileName , true ) , $logFile );
                if( !empty( $fileName ) ){
                    $sign_class = new \Eo\Service\Sign();
                    $result = $sign_class->validSignature( $fileName , true , false , $isExFunction);
                }
                $logerClass->mailLog( "\t\t".'проверено, резултат хранилища '. print_r( $result , true ) , $logFile );
            }
            $logerClass->mailLog( 'отработка окончена ' , $logFile );
        }
        catch( \Exception $exc ){
            $result[ 'status' ] = 'error';
            $result[ 'result' ] = $exc->getMessage();
            $logerClass->mailLog( 'выпало в ошибку ' , $logFile );
        }
        if( $isDebugPrintResult ){
            print_r($result);
        }
        $logerClass->mailLog("\t\t".'выдвет отвтет '. print_r( $result , true ) , $logFile );
        return new \Zend\View\Model\JsonModel( $result );
    }

    /**
     * выдергиваем содержимое из подписаного файл
     * в файле 'file' - файл который проверяем
     *
     * в get
     *      isReturnFile        - вернуть сам файл, а не жсон
     *
     * url:  /service/index/get-sign-content
     *
     * @return \Zend\View\Model\JsonModel
     *      'status'        -   ok    -  добро
     *      'file_name'     - имя нового файла
     *      'file_content'  - содержимое, если isGetContent был труе, завернуто в бейс64
     *
     *      'status'    -   error -  с ошибкой
     *      'result'    -   описание ошибки
     */
    public function getSignContentAction(){
        $isReturnFile          = (int)$this->getRequest()->getQuery('isReturnFile', false);
        $fileName              = $this->getRequest()->getQuery( 'fileName' );
        //подготовка логирования
        $toPath = $_SERVER['DOCUMENT_ROOT'].'/'.'public/log/sign_extract';
        if( !is_dir( $toPath )){
            mkdir( $toPath );
        }
        $logFile = 'public/log/sign_extract/sign_extract_'.date('Y-m-d').'.log';
        $logerClass = new \Eo\Model\SendMail();

        ini_set('error_reporting', 0);
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);

        $logerClass->mailLog("\t\t".'параметр - вернуть файл '. (int)$isReturnFile , $logFile );
        $logerClass->mailLog("\t\t".'параметр - имя файл '. $fileName , $logFile );
        $logerClass->mailLog("\t\t".'параметр - гет '. print_r( $_GET , true ) , $logFile );
        $result = array();
        try{
            $logerClass->mailLog('Начинаем ' , $logFile );
            $result[ 'status' ] = 'error';
            $result[ 'result' ] = 'Не приехал файл';
            $files = $this->getRequest()->getFiles();
            if( !empty($files['file']) ){
                $fileOne = $files['file'];
                $tmpName = $fileOne['tmp_name'];
                $logerClass->mailLog("\t\t".'получено '.print_r( $fileOne , true ) , $logFile );
                $uniq = uniqid();
                $pathToFaile = sys_get_temp_dir ().'/lodtemp/'.$uniq;
                $logerClass->mailLog("\t\t".'временный файл  '.$pathToFaile, $logFile );
                if( !mkdir( $pathToFaile , 0777 , true )){
                    throw new \Exception('не смогло создать временную папку');
                }
                $tmpNameNew = $pathToFaile.'/'.$fileOne['name'];
                if( rename($tmpName,  $tmpNameNew) ){
                    $sign_class = new \Eo\Service\Sign();
                    $logerClass->mailLog("\t\t".'проверяем  ' , $logFile );
                    $result = $sign_class->getContentReal(
                        $tmpNameNew,
                        array(
                            'isFullPathFile'    =>  true,
                            'isGetContent'      =>  true,
                            //'debugPrintFilePath' => true,
                            //'debugPrintAnswerFromDss' => true,
                            //'debugPrintResult' => true,
                        )
                    );
                    $logerClass->mailLog("\t\t".'результат получен  ', $logFile );
                    if( $isReturnFile ){
                        echo $result["file_content"];
                        die;
                    }
                    else{
                        $result["file_content"] = empty($result["file_content"])?'':base64_encode( $result["file_content"] );
                    }
                    @unlink($tmpNameNew);
                    @rmdir($pathToFaile);
                }
                else{
                    throw new Exception( $logerClass->mailLog( 'вне смогло перенести файл  ', $logFile ) );
                }
            }
            if( !empty( $fileName ) ){
                $logerClass->mailLog("\t\t".'обработа по имени   ' . $fileName  , $logFile );
                $sign_class = new \Eo\Service\Sign();
                $result = $sign_class->getContentReal( $fileName ,array( 'isGetContent' => 1 ) );
                $logerClass->mailLog("\t\t".'вернуло чего -то  ' . print_r( array_keys( $result ) , true ) , $logFile ) ;
                //$logerClass->mailLog("\t\t".'вернуло чего -то  '  . print_r($result , true ) , $logFile  ) ;
                if( $isReturnFile ){
                    echo $result["file_content"];
                    die;
                }
            }
        }
        catch( \Exception $exc ){
            $result[ 'status' ] = 'error';
            $result[ 'result' ] = $exc->getMessage();
            $logerClass->mailLog(' ошибка при ыполнении ', print_r(  $result, true ) );
            if( $isReturnFile ){
                echo $result[ 'result' ];
                die();
            }
        }
        $logerClass->mailLog("\t\t".'вернуть только файл   ' . (int) $isReturnFile , $logFile ) ;
        //print_r($result ,true );
        echo json_encode( $result );
        die;
        //облом, это сламмалось
        return new \Zend\View\Model\JsonModel( $result );
    }


    /**
     * извлечение подписей из файлов у отправленных
     *
     * url:  /service/index/extract-document-from-application
     */
    public function extractDocumentFromApplicationAction(){
        //ищем заявления не отправленные и  с нарушениями

        $select = new \Zend\Db\Sql\Select();
        $select->from( 'isga_Applications' );
        $select->columns( array() );

        $whereInner = new \Zend\Db\Sql\Where();
        //$where->from( 'isga_Applications' );
        $whereInner->isNotNull( 'fk_isgaApplicationStatus' );
        $whereInner->or;
        $whereInner->notEqualTo( 'fk_isgaApplicationStatus' , '' );
        //$where->from( 'isga_Applications' );
        $whereInner->or;
        $whereInner->like( 'fk_isgaApplicationStatus' ,   'A3B952BF-628E-446C-B865-E483503241E8' ); //Требуется загрузка копии заявления
        $whereInner->or;
        $whereInner->like( 'fk_isgaApplicationStatus' ,   '2D675622-1E14-4063-AD11-B647B836746D' ); //Ожидание ответа от заявителя

        $where = new \Zend\Db\Sql\Where();
        $where->addPredicate( $whereInner );
        $where->equalTo( 'isga_ApplicationFiles.isArhive' , 0 );
        $where->like( 'isga_ApplicationFiles.RealName' , '%.sig' );

        $select->where( $where );


        $select->join(
            'isga_ApplicationFiles' ,
            'isga_ApplicationFiles.fk_isgaApplication = isga_Applications.Id '

        );
        $select->limit( 10 );
        $appClass = new \Model\Entities\isgaApplications();
        $appArray = $appClass->getAllByWhereParseId( $select );
        if( !empty($appArray) ){
            foreach( $appArray as $value ){
                print_r( $value->getFields() );
            }
        }


        die;

        //переходит на
        //4465AC20-13CF-45A5-8D84-4FD1C6C15E07 - Досыл заявления
        //38D871AC-BF1D-469E-AFC1-7A6CD55EF1B6 - Ожидаются документы
        //
        //если эти то
        //2D675622-1E14-4063-AD11-B647B836746D - Уведомление о несоответствии подписано
        //6c184c34-cc7b-4117-91f7-f0533d5caa23 - Уведомление о несоответствии получено заявителем
        //на эти
        //A3B952BF-628E-446C-B865-E483503241E8 - Требуется загрузка копии заявления
        //
        //есть еще
        //2D675622-1E14-4063-AD11-B647B836746D - Ожидание ответа от заявителя
        //
        //$select->where( array(
            //'fk_isgaApplicationStatus'
        //) );

    }





    /**
     * сохранение файла с реестра лицензий
     *
     * url:  /service/index/file-upload-service
     *
     * _$POST[isChange] - eесли не отсутствует 0 false то заменяем
     * _$POST[colorCode] - хитрый код для лишние не лезли
     *      bf35c9bcd1731e972c7764e1965f07e826d79531029042bde02664e2c51698b7
     *  _$POST[testCode] - хитрый код для лишние не лезли
     *      md5( имяфайла.текущая_дата('Y-m-d') )
     *  _$POST[ownerId]  - ид сущьности
     *  файлы[file]      - файл для загрузки
     *
     */
    public function fileUploadServiceAction()
    {
        try{
            $logFile = 'public/log/fileUploadReestr.log';
            $mailer = new \Exchange\Controller\IndexController();

            $files = $this->getRequest()->getFiles()->toArray();
            //$data = $this->getRequest()->getPost();

            if( empty( $files[ 'file' ] ) ){
                throw new \Exception( 'нету файла' );
            }

            $data = $this->getRequest()->getPost();
            $one_file = $files[ 'file' ];
            if( empty($data[ 'colorCode' ]) ){
                throw new \Exception( 'не пришел хитры код' );
            }
            if( 'bf35c9bcd1731e972c7764e1965f07e826d79531029042bde02664e2c51698b7' !=   $data[ 'colorCode' ] ){
                throw new \Exception(  $mailer->mailLog( ' хитрыq код не верен' , $logFile ) );
            }
            if( empty($data[ 'ownerId' ]) ){
                throw new \Exception(  $mailer->mailLog( 'не указана сущность' , $logFile ) );
            }
            //if( empty($data[ 'path' ]) ){
            //    throw new \Exception(  $mailer->mailLog( 'не указана путь к файлу' , $logFile ) );
            //}
            if( empty($data[ 'testCode' ]) ){
                throw new \Exception(  $mailer->mailLog( 'нет тест кода' , $logFile ) );
            }
            if( md5(  $one_file["name"] . date('Y-m-d') ) != $data[ 'testCode' ] ){
                throw new \Exception(  $mailer->mailLog( 'не совпал тест код' , $logFile ) );
            }
            $data["maxsize"] = 100*1024*1024; //максимальынй размер



            //разрешенные типы
            $allowed = array( "pdf", "doc", "xls","xml", "xlsx", "doc", "docx", "rar", "zip",'sig');

            $mailer->mailLog('начинаем обработку файла', $logFile , $logFile);
            if ( $one_file['error'] != 0) {
                throw new \Exception( $mailer->mailLog("Ошибка загрузки файла ".$one_file['error'] , $logFile) );
            }
            if ( empty( $one_file['size'] ) ) {
                throw new \Exception( $mailer->mailLog("нулевой размер файла " , $logFile) );
            }
            if ( $one_file['size'] >  $data["maxsize"] ) {
                throw new \Exception( $mailer->mailLog("файл   больше чем  " . number_format( $data["maxsize"] , 0 , ',' , ' ' ) . ' байт' , $logFile) );
            }


            $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
            if( !in_array(mb_strtolower($ext), $allowed) ){
                throw new \Exception( $mailer->mailLog("недопустимый тип файлов  " . $ext , $logFile) );
            }
            //формируем путь к файлу
            $mailer->mailLog('присланный путь  ' . $data["path"], $logFile);
            $data["path"]   = '';
            $data["path"]   = preg_replace('/[^a-zA-ZА-Яа-я0-9.]/i', "", $data["path"]);
            $data["path"]   = '/uploads/synch/' . date( 'Y' ) . '_reestr/'.$data[ 'ownerId' ] .'/' ;
            $fullPath       = $_SERVER['DOCUMENT_ROOT'] . '/public/'. $data["path"] ;
            $fullPath       = str_replace( '//' , '/' ,  $fullPath );
            $fullPath       = str_replace( '/public/public/' , '/public/' ,  $fullPath );

            if (!file_exists( $fullPath ) ) {
                if ( @!mkdir( $fullPath , 0777, true)) {
                    throw new \Exception(  $mailer->mailLog("Не удалось создать каталог, проверьте права доступа", $logFile) );
                }
            }

            $mailer->mailLog('сохранить  ' . $data["path"] . $one_file["name"], $logFile);
            if (empty($data['isChange']) && is_file( $fullPath . $one_file["name"]) ){
                throw new \Exception(  $mailer->mailLog('Уже есть такой файл', $logFile) );
            }
            $mailer->mailLog('сохраняем в ' . $fullPath . $one_file["name"], $logFile);

            if (move_uploaded_file($one_file['tmp_name'], $fullPath . $one_file["name"]) ){
                if (!empty($data['onLoad'])) {
                    $mailer->mailLog('Есть онлоад', $logFile);
                    $mailer->mailLog( print_r($data['onLoad'],true ), $logFile );
                    try{
                        $this->onLoad( json_decode($data['onLoad'], true) );
                    }
                    catch( \Exception $exc ){
                        echo $mailer->mailLog('Ошибка при разделе онЛоад '.$exc->getMessage(), $logFile);
                        $mailer->mailLog( $exc->getTraceAsString() , $logFile);
                        die;
                    }
                }
                else{
                    $mailer->mailLog('нет онлоад', $logFile);
                }
                $mailer->mailLog('сохранено', $logFile);
                echo 'Ok';
                die;
            } else {
                throw new \Exception(  $mailer->mailLog("Не удалось загрузить файл, проверьте права доступа", $logFile) );
            }

        }catch( \Exception $ex){
            echo $mailer->mailLog( $ex->getMessage() , $logFile);
        }
        die;
    }
    //последняя строка

}