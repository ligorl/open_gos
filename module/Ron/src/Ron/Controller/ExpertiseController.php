<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Ron\Controller;

use Model;
use Ron\Classes\Expertise;
use Ron\Controller\Word;
use Ron\EloquentModel;
use Ron\EloquentModel\ApplicationExtraDocument;
use Ron\EloquentModel\DocumentOrder;
use Ron\EloquentModel\EduProgramm;
use Ron\EloquentModel\IsgaApplication;
use Ron\EloquentModel\LicensedProgram;
use Ron\Model\Templater;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Swift;
use Ron\EloquentModel\eiisEduDirectory;



class ExpertiseController extends AbstractActionController
{
    protected $authservice;

    public function getAuthService()
    {
        if (!$this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }

    /*
      * Главный Action выводит левое меню футер шапку и шаблон
      */
    public function indexAction()
    {
        //Подключаем js файл
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/ron/main.js');
        $headStuff->appendFile('/js/ron/expertise.page.js');
        $headStuff->appendFile('/js/moment.js');
        $headStuff->appendFile('/js/tipsy/jquery.tipsy.js');

        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        $headStuff->appendStylesheet('/js/tipsy/tipsy.css');

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();


        $this->layout()->identityUser = $user;

        return new ViewModel();

    }

    public function expertiseAction()
    {

        $user = $this->getAuthService()->getIdentity();
        $this->layout()->identityUser = $user;
        $id = $user->getField('fk_eiisEducationalOrganization');

        $model_org = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");

        $contactingClass = new Model\ExpertEntities\contacting();
        $contacting = $contactingClass->selectComplicated(
            "contacting",
            array('expertise_id'),
            array('expert_id = "' . $id . '"', 'contacting_result NOT IN ("СonsentDenided","Отказ")'),
            'expertise_id'
        )->toArray();
        $expertises = false;
        $chairmansByExpertise = false;

        if ($contacting) {
            $expertises_id = array_column($contacting, 'expertise_id');
            $expertisesIdArray = $expertises_id;
            //$expertises_id = '"'.implode('","',$expertises_id).'"';
            $expertiseClass = new Model\ExpertEntities\expertise();
            $expertises = $expertiseClass->selectComplicated(
                "expertise",
                array('organization_id', 'id', 'accreditation_number', 'accreditation_procedure_type', 'create_timestamp', 'end_timestamp'),
                array(
                    'expertise.id' => $expertises_id,
                    'expertise_document.document_type_id' => 37,
                    'expertise_document.expert_id' => $id,
                    new \Zend\Db\Sql\Predicate\IsNotNull('expertise_document.traffic_date'),
                ),
                /*
                'expertise.id IN ('.$expertises_id.') AND
                    expertise_document.document_type_id="9" AND
                    expertise_document.fk_status != "613183d2-33b1-4f93-89d4-8ba9f158c467" AND
                    expertise_document.expert_id = "'.$id.'"',
                */
                /*array('id' => $expertises_id,'create_timestamp <= "'.date('Y-m-d').' 00:00:00"','end_timestamp >= "'.date('Y-m-d').' 00:00:00"'),*/
                'expertise.id',
                'create_timestamp DESC',
                null,
                null,
                'expertise_document',
                'expertise_document.expertise_id = expertise.id',
                'fk_status',
                'left'
            )->toArray();

//foreach ($expertises as $trueRule){
//    foreach ($chairmans as ){
//
//    }
//
//}
            //получаем руководителей для каждой программы
            $chairmans = array();
            if (!empty($expertisesIdArray)) {
                $chairmans = new \Model\ExpertEntities\chairman();
                $chairmans = $chairmans->getAllByWhere(array(
                    "expertise_id" => array_values($expertisesIdArray),
                    "expert_id" => $id,
                ));
            }
            //Распределяем руководителей по оо
            $chairmansByExpertise = array();
            if (!empty($chairmans)) {
                foreach ($chairmans as $chairman) {
                    $chairmansByExpertise[$chairman->getField("expertise_id")] = 1;
                }
            }
        }

        $view = new ViewModel();
        $view->setVariables(array(
            "expertises" => $expertises,//$items->getObjectsArray(),
            "orgs" => $model_org,
            "chairmans" => $chairmansByExpertise,
        ));
        $view->setTerminal(true);
        return $view;
    }

    public function viewExpertiseAction()
    {
        $id = $this->params('id');
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;
        $view = new ViewModel();
        if (!empty($id)) {
            $model_expertise = new \Model\Gateway\ExpertsEntitiesTable("expertise");
            $applicationsClass = new Model\Gateway\EntitiesTable('isga_Applications');
            $model_org = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");

            $currentExpertise = $model_expertise->getEntityById($id);

            $org = $model_org->getEntityById($currentExpertise->getField('organization_id'));

            $currentApplication = $applicationsClass->getEntityById($currentExpertise->getField('applications_id'));
            $currentType = array();
            $currentReason = array();
            $currentLicense = array();
            $currentCert = array();
            if ($currentApplication) {
                $currentType = $currentApplication->getType();
                $currentReason = $currentApplication->getNamesReasons();
                $currentLicense = $currentApplication->getLinkedItem('eiis_Licenses', array('Id' => $currentApplication->getField("fk_eiisLicense")));
                $currentCert = $currentApplication->getLinkedItem('isga_Certificates', array('Id' => $currentApplication->getField("fk_isgaPreviousCertificate")));
            }
            $ApplicationDocumentGroups = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentGroups');
            $copyApplicationDoc = $ApplicationDocumentGroups->getEntitiesByWhere('`fk_isgaApplication` = "' . $currentExpertise->getField('applications_id') . '" AND `fk_isgaApplicationDocumentType` = "0B4B6324-5DAE-4AB8-B973-86C4A39409E1" AND `FtpDirectoryUrl` IS NOT NULL AND isArhive = "0"')->current();
            if ($copyApplicationDoc) {
                $copyApplicationDoc = $copyApplicationDoc->toArray();
            }
            $expDocument = new \Model\ExpertEntities\expertisedocument();
            $expDocument = $expDocument->getByWhere(array(
                'expertise_id' => $currentExpertise->getId(),
                'document_type_id' => 8,
            ));

            //Выборка данных из isgaEducationalOrganizationData по organization_id
            $where = $currentExpertise->getField('organization_id');
            $eduOrgData = new Model\Entities\isgaEducationalOrganizationData();
            $eduOrgDataByOrgId = $eduOrgData->getEduOrgData($where);

            $view->setVariables(array(
                'user' => $user->getId(),
                "expertise" => $currentExpertise,
                "org" => $org,
                "eduOrgDataByOrgId" => $eduOrgDataByOrgId,
                /*"institution"    => $institution,
                'accr_numb' => $accr_numb,
                'idorg' => $id_org,
                'nameOrg' => $name_org,*/
                "application" => $currentApplication,
                "currentCert" => $currentCert,
                "currentLicense" => $currentLicense,
                "currentType" => $currentType,
                "currentReason" => $currentReason,
                "copyApplicationDoc" => $copyApplicationDoc,
                'expertiseDocumentOrder' => $expDocument,
            ));
        }

        $view->setTerminal(true);
        return $view;
    }

    public function getExpertiseDocumentFileAction(){
        $result = ['answer'=>"error"];
        $expertiseId = $this->params('id');
        $expertiseDocument = EloquentModel\ExpertiseDocument::where(['expertise_id'=>$expertiseId,'document_type_id'=>71])->get()->last();
        if(!empty($expertiseDocument)){
            $expertiseDocument = $expertiseDocument->getAttributes();
            $expertiseDocumentFile = EloquentModel\ExpertiseDocumentFile::where('expertise_document_id',$expertiseDocument['id'])->get()->first();
            if(!empty($expertiseDocumentFile)){
                $result = [
                    'answer'=>"ok",
                    'RealName'=>$expertiseDocumentFile->RealName,
                    'HashName'=>$expertiseDocumentFile->HashName,
                    'expertise_document_id'=>$expertiseDocumentFile->expertise_document_id,
                    'expertise_document_file_id' => $expertiseDocumentFile->getAttributes()['id']
                ];
            }
        }
        return new JsonModel($result);

    }

    /**
     * вкладка Состав группы
     *
     * @return ViewModel
     */
    public function getExpertiseExspertsGroupAction()
    {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $user_id = $user->getField('fk_eiisEducationalOrganization');
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            //$data = $this->params()->fromPost();
            $post = $this->params()->fromPost();
            if (empty($post['expertise'])) {
                die;
            }
            $id = $post['expertise'];
            $isChairman = false;

            $expertise_model = new \Model\ExpertEntities\expertise();
            $contactingModel = new \Model\ExpertEntities\contacting();
            $members = new \Model\ExpertEntities\commissionmember();
            $chairmanModel = new \Model\Gateway\ExpertsEntitiesTable('chairman');
            //$commModel = new \Model\Gateway\ExpertsEntitiesTable('commission_member');
            //выбирем ид вуза
            $member = $members->getEntityByWhere('expert_id="' . $user_id . '" AND expertise_id="' . $id . '"');
            if (!empty($member['institution_id'])) {
                $org_id = $member['institution_id'];
                $chairmans = $chairmanModel->getEntityByWhere('expert_id="' . $user_id . '" AND expertise_id="' . $id . '" AND institution_id="' . $org_id . '"');
                if (!empty($chairmans)) {
                    $isChairman = true;
                }
            } else {
                $chairmans = $chairmanModel->getEntityByWhere('expert_id="' . $user_id . '" AND expertise_id="' . $id . '"');
                if (empty($chairmans)) {
                    die('не смогло опредлить вуз эксперта');
                } else {
                    $org_id = $chairmans->getFieldOrSpace('institution_id');
                    $isChairman = true;
                }
            }

            //эксперты которые согласились
            $experts = $contactingModel->getConfirmExperts($id);
            $confExperts = explode(',', implode(',', $experts));

            $where = array('expert_id' => $confExperts, 'expertise_id' => $id, 'institution_id' => $org_id);
            // руководители
            $chairmans = $chairmanModel->getEntityByWhere($where);

            if ($chairmans) {
                unset($experts[$chairmans->getField('expert_id')]);
            }
            $confExperts = explode(',', implode(',', $experts));
            $where = array('expert_id' => $confExperts, 'expertise_id' => $id, 'institution_id' => $org_id);
            // эксперты
            //$members = $commModel->getAllEntities(null,null,$where,false,'expert_id');
            //$members = $members -> getObjectsArray();
            $members = $members->getAllByWhereParseId($where, 'expert_id');

            $view = new ViewModel();
            $view->setVariables(array(
                'isChairman' => $isChairman,
                'chairmans' => $chairmans,
                'members' => $members,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    /**
     * вкладка Состав коммисии
     *
     * @return ViewModel
     */
    public function getExpertiseExspertsViewAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            //$data = $this->params()->fromPost();
            $post = $this->params()->fromPost();
            if (empty($post['expertise'])) {
                die;
            }
            $isExpertChairMain = false;
            $expertiseId = $post['expertise'];
            $user = $this->getAuthService()->getIdentity();
            $this->layout()->identityUser = $user;
            $id = $user->getField('fk_eiisEducationalOrganization');

            $model_exp = new \Model\Gateway\ExpertsEntitiesTable("expertise");
            $model_com = new \Model\Gateway\ExpertsEntitiesTable("commission_member");
            $expertise = $model_exp->getEntityByWhere('id = "' . $expertiseId . '"');


            //текущий филиал
            $curr = $model_com->getEntityByWhere(
                'expertise_id = "' . $expertiseId . '" AND expert_id = "' . $id . '"'
            );
            if (!empty($curr)) {
                $appId = $curr->getField('institution_id');
            }

            //Выбираем руководителя
            $chairmans = $expertise->getChairman();
            $chairmans = $chairmans->getObjectsArray();
            $chairmansByInstitution = array();
            if ($chairmans) {
                foreach ($chairmans as $chairman) {
                    if (!$chairman->isConfirm()) {
                        continue;
                    }
                    $cExpert = new \Model\ExpertEntities\expert($chairman->getField("expert_id"));
                    if ($cExpert->getId() == null) {
                        continue;
                    }
                    $chairmansByInstitution[$chairman->getField("institution_id")] = $cExpert->getFio();
                    if (empty($appId) && $id == $chairman->getField("expert_id")) {
                        $appId = $chairman->getField('institution_id');
                    }
                    if ($chairman->getField("expert_id") == $id) {
                        $isExpertChairMain = true;
                    }
                }
            }
            //список експертов в группе
            $chairmanModel = new \Model\Gateway\ExpertsEntitiesTable('chairman');
            $chairmans = $chairmanModel->getEntityByWhere('expert_id="' . $id . '" AND expertise_id="' . $expertiseId . '"');
            $isChairman = false;
            if (!empty($chairmans)) {
                $isChairman = true;
            }
            $contactingModel = new \Model\ExpertEntities\contacting();
            $experts = $contactingModel->getConfirmExperts($expertiseId);
            $confExperts = explode(',', implode(',', $experts));


            //состав комиссииarray('expert_id' => $id)
            $commission = new \Model\ExpertEntities\commissionmember();
            $commission = $commission->getAllByWhere(array(
                'expertise_id' => $expertiseId,
                'expert_id' => $isChairman ? $confExperts : $id,
            ));

            //выццепим куратора
            $curator = null;
            $userFile = [];
            if (!empty($commission)) {
                foreach ($commission as $commissionOne) {

                    $curator = $commissionOne->getUser();
                    if (!empty($curator)) {
                        break;
                    }
                }
            }
            if (empty($curator)) {
                if (!empty($chairmans)) {
                    foreach ($chairmans as $chairmanOne) {
                        $curator = $chairmanOne->getUser();
                        if (!empty($curator)) {
                            break;
                        }
                    }
                }
            }
            $userFile = [];
            if ($commission) {
                foreach ($commission as $commiss) {
                    $application = $commiss->getApplicationProgram();
                    $programmId = $application->getId();
//                    $result =[];
//                    $expertiseDocument = EloquentModel\ExpertiseDocument::where(['expertise_program_id'=>$programmId,'document_type_id'=>29,'expert_id'=>$id])->get()->first();
//                    if(!empty($expertiseDocument)){
//                        $expertiseDocument = $expertiseDocument->getAttributes();
//                        $expertiseDocumentFile = EloquentModel\ExpertiseDocumentFile::where('expertise_document_id',$expertiseDocument['id'])->get()->first();
//                        if(!empty($expertiseDocumentFile)){
//                            $result = [
//                                'RealName'=>$expertiseDocumentFile->RealName,
//                                'HashName'=>$expertiseDocumentFile->HashName,
//                                'expertise_document_id'=>$expertiseDocumentFile->expertise_document_id,
//                                'expertise_document_file_id' => $expertiseDocumentFile->getAttributes()['id']
//                            ];
//                        }
//                    }
//
//                    $userFile[$programmId] = $result;
                    if ($application) {
                        $ugs = $application->getEGS();
                        $programm = $application->getLicensedProgram();
                        $programmType = $application->getEduProgramType();
                    }
                }
            }
            $orgTable = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");
            $contacting = new \Model\Gateway\ExpertsEntitiesTable("contacting");
            $expDoc = new \Model\Gateway\ExpertsEntitiesTable("expertise_document");
            $expert = new \Model\ExpertEntities\expert();
            $expert = $expert->getById($id);
            $view = new ViewModel();
            $eduProgram = EduProgramm::where('Id', $programm->getField('fk_eiisEduProgram'))->get();

            $licensedProgram = LicensedProgram::where('Id', $application->getField('fk_eiisLicensedPrograms'))->get();

            $applicationId = IsgaApplication::where('Id', $application->getField('fk_isgaApplication'))->get();
//            $docum/**/etData = DocumentOrder::where('Id', $applicationId->toArray()[0]['Id'])->get();
            $nameDirectory = eiisEduDirectory::where('id', $eduProgram->toArray()[0]['fk_eiisEduDirectory'])->get();
                $documetData = DocumentOrder::where('Id', $applicationId->toArray()[0]['fk_isgaOrderDocument'])->get()->first();
            if(empty($documetData)){
               $documetData['DateSign'] = '';
               $documetData['Number'] = '';
            }

            $view->setVariables(array(
                'nameDirectory' => count($nameDirectory->toArray())>0?$nameDirectory->toArray()[0]:null,
                'documentData' => $documetData,
                'licensedProgram' => $licensedProgram->toArray()[0],
                'eduProgram' => $eduProgram->toArray()[0],
                'appId' => $appId,
                'answer' => $contacting->getEntityByWhere(array(
                    'expert_id' => $id,
                    'expertise_id' => $expertiseId,
                    'contacting_result' => array('СonsentConfirm', 'Согласие'),
                )),
                'expertise_document' => $expDoc->getEntityByWhere(array(
                    'expert_id' => $id,
                    'expertise_id' => $expertiseId,
                    'document_type_id' => '9',
                )),
                'organization' => $orgTable->getEntityById($appId),
                'commission' => $commission,
                'chairmansByInstitution' => $chairmansByInstitution,
                'isExpertChairMain' => $isExpertChairMain,
                'curator' => $curator,
                'expert' => $expert,
                'userId' => $user->getId(),
                'expertiseId' =>$expertiseId,
//                'userFile' => $userFile

            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }


    public function documentsAction()
    {
        $user = $this->getAuthService()->getIdentity();
        $this->layout()->identityUser = $user;
        $id = $user->getField('fk_eiisEducationalOrganization');

        $id_appl = $this->params('id');

        if (!$id_appl) {
            die;
        }

        $complicatedSelect = new Model\Entities\isgamtmDocumentReason();

        $application_class = new Model\Gateway\EntitiesTable('isga_Applications');
        $currAppl = $application_class->getEntityById($id_appl);
        $educationOrganization_class = new Model\Gateway\EntitiesTable('eiis_EducationalOrganizations');
        if (!empty($currAppl)) {
            $educationOrganization = $educationOrganization_class->getEntityById($currAppl->getField('fk_eiisEducationalOrganization'));
            $idsReasons = $currAppl->getIdsReasons();

        }
        if (!empty($educationOrganization)) {
            $orgData = $educationOrganization->getEducationalOrganizationData()->toArray();
        }
        $mtm_DocumentType_class = new Model\Gateway\EntitiesTable('isga_mtm_Document_Reason');
        if (!empty($orgData) && !empty($idsReasons)) {
            $Documents = $complicatedSelect->selectComplicated(
                'isga_mtm_Document_Reason',
                array('fk_isgaDocumentTypes', 'Sorting', 'Obligation'),
                array('isga_mtm_Document_Reason.fk_isgaEducationalOrganizationSpecifics' => $orgData['fk_isgaEducationalOrganizationSpecific'],
                    'isga_mtm_Document_Reason.fk_isgaApplicationReasons' => $idsReasons[0]
                ),
                NULL,
                'isga_mtm_Document_Reason.Sorting ASC',
                NULL,
                NULL,
                'isga_DocumentTypes',
                'isga_mtm_Document_Reason.fk_isgaDocumentTypes = isga_DocumentTypes.Id',
                array('Id', 'Name')
            )->toArray();
        } else {
            $Documents = array();
        }
        $currFile = $complicatedSelect
            ->selectComplicated(
                "isga_ApplicationFiles",
                array(
                    'Id',
                    'RealName',
                    'HashName',
                    'fk_isgaDocumentType'),
                array(
                    'fk_isgaApplication' => $id_appl,
                    'isArhive' => 0
                )
            );
        /*получим дополнительные документы из таблицы isga_ApplicationDocumentGroups*/
        $application_document_groups_class = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentGroups');
        $application_document_obj = $application_document_groups_class->getEntitiesByWhere(array("fk_isgaApplication" => $id_appl));
        $app_doc_docs = array();
        $isgaApplicationDocumentType = array();
        $application_document_types = array();
        foreach ($application_document_obj as $ado) {
            $isgaApplicationDocumentType[] = $ado->getField('fk_isgaApplicationDocumentType');
            $ftp_directory = $ado->getField('FtpDirectoryUrl');
            if (!empty($ftp_directory) && file_exists($_SERVER['DOCUMENT_ROOT'] . $ftp_directory)) {
                $app_doc_docs[mb_strtolower($ado->getField('fk_isgaApplicationDocumentType'), 'UTF-8')][] = array(
                    "FtpDirectoryUrl" => $ftp_directory,
                    "RealFileName" => $ado->getField('RealFileName'),
                    "Id" => $ado->getField('Id'),
                );
            }

        }
        if (count($app_doc_docs) > 0) {
            $application_document_types_class = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentTypes');
            $application_document_types_obj = $application_document_types_class->getEntitiesByWhere(array("id" => $isgaApplicationDocumentType));
            foreach ($application_document_types_obj as $adt) {
                $application_document_types[mb_strtolower($adt->getField('Id'), 'UTF-8')] = $adt->getField('Name');
            }
        }
        //получим массив типов документов которые не входят в диапазон причинных связей
        $document_types = array();
        foreach ($Documents as $doc_reason) {
            $document_types[] = $doc_reason['fk_isgaDocumentTypes'];
        }
        $application_types = array();
        $oldDoc = $currFile->toArray();
        foreach ($oldDoc as $docs) {
            if (!in_array($docs['fk_isgaDocumentType'], $document_types)) {
                $application_types[] = $docs['fk_isgaDocumentType'];
            }
        }
        $application_types_without_reason = array();
        if (count($application_types) > 0) {
            $document_types_class = new Model\Gateway\EntitiesTable('isga_DocumentTypes');
            $document_types_obj = $document_types_class->getEntitiesByWhere(array("Id" => $application_types));
            foreach ($document_types_obj as $dt) {
                $application_types_without_reason[mb_strtolower($dt->getField('Id'), 'UTF-8')] = $dt->getField('Name');
            }
        }

        $view = new ViewModel();
        $view->setVariables(
            array(
                "id_appl" => $id_appl,
                'arrayDocs' => $Documents,
                'oldDoc' => $oldDoc,
                "dop_docs" => $app_doc_docs,
                'application_document_types' => $application_document_types,
                'application_types_without_reason' => $application_types_without_reason,
                'userRole' => $user->getRole(),
                'educationOrganization' => $educationOrganization,
                /*
                'RecallScan'                       => array(
                    "path"=>$currAppl->getField('RecallApplicationScan'),
                    "name"=>$currAppl->getField('RecallApplicationScanFileName')
                ),
                */
            )
        );
        $view->setTerminal(true);
        return $view;
    }


    /**
     * рисуем вкладку документы эксперта
     *
     * @return ViewModel
     */
    public function getExpertiseExpertDocumentedDialogAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $expertiseId = $this->params('id');
            $user = $this->getAuthService()->getIdentity();
            $expertId = $user->getField('fk_eiisEducationalOrganization');
            if (empty($expertiseId) || empty($expertId)) {
                echo 'не пришли данные  экспертиза';
                die;
            }
            //$post = $this->params()->fromPost();
            $model_exp = new \Model\Gateway\ExpertsEntitiesTable('expertise');
            $expertise = $model_exp->getEntityByWhere('id = "' . $expertiseId . '"');

            $expDocTypePersonalIdArray = array();
            $expDocTypeAllIdArray = array();
            $expDocTypeIdArray = array();
            $expDocIdArray = array();
            $expDocIdArray62 = array();

            //проверим, есть ли у эксперта программы
            $comissionAll = new \Model\ExpertEntities\commissionmember();
            $comissionAll = $comissionAll->getAllByWhere(array(
                'expertise_id' => $expertiseId,
                'expert_id' => $expertId,
            ));
            $isProgram = false;
            if (!empty($comissionAll)) {
                $isProgram = true;
            }
            unset($comissionAll);
            //проверим а не руководитель он
            $chairAll = new \Model\ExpertEntities\chairman();
            $chairAll = $chairAll->getByWhere(array(
                'expertise_id' => $expertiseId,
                'expert_id' => $expertId,
            ));
            $isChair = false;
            if (!empty($chairAll)) {
                $isChair = true;
            }

            $expDocumentArray = array();
            //сообираем документы конкретного эксперта
            $expDocument = $expertise->getExpertiseDocument(array(
                'expert_id' => $expertId,
                new \Zend\Db\Sql\Predicate\IsNotNull('open_date'),
                new \Zend\Db\Sql\Predicate\IsNotNull('traffic_date'),
            ));
            $hasOrder = false;
            if (!empty($expDocument)) {
                foreach ($expDocument as $value) {
                    $expDocId = $value->getId();
                    $expDocumentArray[$value->getId()] = $value;
                    $expDocTypeId = $value->getField('document_type_id');
                    if (!empty($expDocTypeId)) {
                        $expDocTypeIdArray[$expDocTypeId] = $expDocTypeId;
                    }
                    $expDocIdArray[$expDocId] = $expDocId;
                }
            }

            //не выводим не нужные документы
            //$docTypeNotSearch = array(62);
            if (!$isProgram) {
                $docTypeNotSearch[] = 64;
            }
            if (!$isChair) {
                $docTypeNotSearch[] = 63;
            }
            //заклин, чтоб не искала расп документ в общих
            if (!empty($expDocTypeIdArray[8])) {
                $docTypeNotSearch[] = 8;
            }

            $expertiseWhere = array(
                new \Zend\Db\Sql\Predicate\IsNull('expert_id'),
                //new \Zend\Db\Sql\Predicate\NotIn('document_type_id',array(8)),
                new \Zend\Db\Sql\Predicate\IsNotNull('open_date'),
            );
            if (!empty($docTypeNotSearch)) {
                $expertiseWhere[] = new \Zend\Db\Sql\Predicate\NotIn('document_type_id', $docTypeNotSearch);
            }

            //сообираем общие документы, если есть документы на эксперта
            $expDocument = array();
            if (!empty($expDocumentArray)) {
                $expDocument = $expertise->getExpertiseDocument($expertiseWhere);
            }
            if (!empty($expDocument)) {
                foreach ($expDocument as $value) {
                    $expDocId = $value->getId();
                    $value->setField('isForAll', '1');
                    $expDocumentArray[$value->getId()] = $value;
                    $expDocTypeId = $value->getField('document_type_id');
                    if (!empty($expDocTypeId)) {
                        $expDocTypeIdArray[$expDocTypeId] = $expDocTypeId;
                    }
                    if ($expDocTypeId == 62) {
                        $expDocIdArray62[$expDocId] = $expDocId;
                    } else {
                        $expDocIdArray[$expDocId] = $expDocId;
                    }
                }
            }


            //проверим эксперт конторский, если да, то посмотрим документы конторы
            $expert = new \Model\ExpertEntities\expert($expertId);
            $parentId = $expert->getField('parent_id');
            if (!empty($parentId)) {
                $expDocument = $expertise->getExpertiseDocument(array(
                    'expert_id' => $parentId,
                    new \Zend\Db\Sql\Predicate\IsNotNull('open_date'),
                    new \Zend\Db\Sql\Predicate\NotIn('document_type_id', array(9, 10)),
                ));
                if (!empty($expDocument)) {
                    foreach ($expDocument as $value) {
                        $expDocId = $value->getId();
                        $expDocumentArray[$value->getId()] = $value;
                        $expDocTypeId = $value->getField('document_type_id');
                        if (!empty($expDocTypeId)) {
                            //$expDocumentArray[$expDocTypeId] = $value;
                            $expDocTypeIdArray[$expDocTypeId] = $expDocTypeId;
                        }
                        $expDocIdArray[$expDocId] = $expDocId;
                    }
                }
            }


            //какие типы докумнты применяются
            $expDocTypeArray = array();
            if (!empty($expDocTypeIdArray)) {
                $expDocType = new \Model\ExpertEntities\documenttype();
                $expDocType = $expDocType->getByIds($expDocTypeIdArray);
                if (!empty($expDocType)) {
                    foreach ($expDocType as $value) {
                        $id = $value->getId();
                        $expDocTypeArray[$id] = $value;
                    }
                }
            }

            //собираем файлы документов
            $expDocFileArray = array();
            if (!empty($expDocIdArray)) {
                $expDocIdArray = explode(',', implode(',', $expDocIdArray));
                $expDocFile = new \Model\ExpertEntities\expertisedocumentfile();
                $expDocFile = $expDocFile->getByExpertiseDocumentIds($expDocIdArray);
                if (!empty($expDocFile)) {
                    foreach ($expDocFile as $value) {
                        //$id = $value->getId();
                        $expId = $value->getFieldOrSpace('expertise_document_id');
                        $expDocFileArray[$expId] = $value;
                    }
                }
            }
            if (!empty($expDocIdArray62)) {
                $expDocIdArray62 = '"' . implode('","', $expDocIdArray62) . '"';
                $expDocFile = new \Model\ExpertEntities\expertisedocumentfile();
                $expDocFile = $expDocFile->getByWhere('expertise_document_id IN (' . $expDocIdArray62 . ') ORDER BY version DESC');
                if (!empty($expDocFile)) {
                    $expId = $expDocFile->getFieldOrSpace('expertise_document_id');
                    $expDocFileArray[$expId] = $expDocFile;
                }
            }

            $view = new ViewModel();
            $view->setVariables(array(
                'expertiseDocument' => $expDocumentArray,
                'expertiseDocumentType' => $expDocTypeArray,
                'expertiseDocumentTypeId' => $expDocTypeIdArray,
                'expertiseDocumentFile' => $expDocFileArray,
                'expertId' => $expertId,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    /**
     * Карточка экспертизы
     * Документы От экспертов
     *
     * @return ViewModel
     */
    public function getExpertiseDocumentForExpertAnswerAction()
    {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            //$data = $this->params()->fromPost();
            $post = $this->params()->fromPost();
            if (empty($post['expertise'])) {
                die;
            }
            $expertiseId = $post['expertise'];
            $user = $this->getAuthService()->getIdentity();
            $expertId = $user->getField('fk_eiisEducationalOrganization');


            //список експертов в группе
            $chairmanModel = new \Model\Gateway\ExpertsEntitiesTable('chairman');
            $chairmans = $chairmanModel->getEntityByWhere('expert_id="' . $expertId . '" AND expertise_id="' . $expertiseId . '"');
            $isChairman = false;
            if (!empty($chairmans)) {
                $isChairman = true;
            }
            $contactingModel = new \Model\ExpertEntities\contacting();
            $experts = $contactingModel->getConfirmExperts($expertiseId);
            $confExperts = explode(',', implode(',', $experts));


            //$expertise = new \Model\ExpertEntities\expertise($expertiseId);
            $expertiseDocumentAll = new \Model\ExpertEntities\expertisedocument();
            $expertiseDocumentAll = $expertiseDocumentAll->getAllByWhere(array(
                //'document_type_id'=>array(29,40),
                'expertise_id' => $expertiseId,
                new \Zend\Db\Sql\Predicate\IsNull('open_date'),
                new \Zend\Db\Sql\Predicate\IsNotNull('notification_date'),
                'expert_id' => $isChairman ? $confExperts : $expertId
            ));


            $view = new ViewModel();
            $view->setVariables(array(
                'expertiseDocumentAll' => $expertiseDocumentAll,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    /**
     * Загрузижаем диалог загрузки файла
     *
     * @return ViewModel
     */
    public function getExpertiseUploadDocumentIncludeDialogAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $expertiseId = $this->params('id');
            $docTypeId = $this->params('id2');
            $flagDialogWind = $this->params('id3');
            $programId = $this->params('id4');
            $organization_id = $this->params()->fromPost('organization_id');
            $user = $this->getAuthService()->getIdentity();
            $expertId = $user->getField('fk_eiisEducationalOrganization');

            if (empty($expertiseId) || empty($docTypeId)) {
                echo "не приехала экспертиза или програма или докуменлдт тип";
                die;
            }
            if (empty($docTypeId) || !in_array($docTypeId, array(29, 40, 71))) {
                echo "не приехал или не определен тип документа";
                die;
            }
            $where = array(
                'document_type_id' => $docTypeId,
                'expertise_id' => $expertiseId,
                'expert_id' => $expertId,
            );
            if ($docTypeId == 29) {
                $where['expertise_program_id'] = $programId;
            }

            $expDocumentClass = new \Model\ExpertEntities\expertisedocument();
            $expDocument = $expDocumentClass->getByWhere($where);
            //если записи нет, то пустая
            if (empty($expDocument)) {
                $expDocument = $expDocumentClass;
                $expDocument->setField('document_type_id', $docTypeId);
                $expDocument->setField('expertise_id', $expertiseId);
                //$expDocument->setField('expertise_program_id',$programId);
                $expDocument->setField('expert_id', $expertId);
            }

            $view = new ViewModel();
            $view->setVariables(array(
                'expertiseProgramId' => $programId,
                'expertiseDocument' => $expDocument,
                'expertiseId' => $expertiseId,
                'expertId' => $expertId,
                'documentTypeId' => $docTypeId,
                'organization_id' => $organization_id,
                'flag' => $flagDialogWind,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    public function saveTempReportAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $temp = $_FILES['file']['tmp_name'];
            $path = $_SERVER['DOCUMENT_ROOT'] . "/temp/" . time();
            $isDir = mkdir($path, 0777, true);
            $sign_class = new \Eo\Service\Sign();


            $name_info_file_hash = $sign_class->get_hash_name($_FILES['file']['name']);
            $fullPath = $path . "/" . $name_info_file_hash;

            move_uploaded_file($temp, $fullPath);
            $save_data['HashName'] = $fullPath;// $path.str_replace($_FILES['file']['name'], $name_info_file_hash);

            $sign_data = $sign_class->validSignature($fullPath, false, true);
            if ('ok' != $sign_data['status']) {

                return new JsonModel([
                    'answer' => "error",
                    'status' => $sign_data['result'],
                    'name' => $_FILES['file']['name'],
                ]);
            }

            return new JsonModel([
                'answer' => "ok",
                'tmp' => $fullPath,
                'name' => $_FILES['file']['name'],
                'status' => $sign_data['status'],
                'certificate_info' => $sign_data['certificate_info']

            ]);
        }
        return new JsonModel([
            'answer' => "error"
        ]);
    }

    public function saveExpertiseDocumentFileCertificatesAction()
    {

        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost()->toArray();
            $expertiseDocumentFileCertificates = new EloquentModel\ExpertiseDocumentFileCertificates();

            $expertiseDocumentFileCertificates->expertise_document_file_id = $post['fileId'];
            $expertiseDocumentFileCertificates->local_signing_time = isset($post['LocalSigningTime'])?$post['LocalSigningTime']:null;
            $expertiseDocumentFileCertificates->NotAfter = isset($post['NotAfter'])?$post['NotAfter']:null;
            $expertiseDocumentFileCertificates->NotBefore = isset($post['NotBefore'])?$post['NotBefore']:null;
            $expertiseDocumentFileCertificates->serial_number = isset($post['SerialNumber'])?$post['SerialNumber']:null;
            $expertiseDocumentFileCertificates->cn_work = isset($post['CN'])?$post['CN']:null;
            $expertiseDocumentFileCertificates->email = isset($post['E'])?$post['E']:null;
            $expertiseDocumentFileCertificates->g_name = isset($post['G'])?$post['G']:null;
            $expertiseDocumentFileCertificates->inn = isset($post['INN'])?$post['INN']:null;
            $expertiseDocumentFileCertificates->ogrn = isset($post['OGRN'])?$post['OGRN']:null;
            $expertiseDocumentFileCertificates->sn_name = isset($post['SN'])?$post['SN']:null;
            $expertiseDocumentFileCertificates->street = isset($post['STREET'])?$post['STREET']:null;
            $expertiseDocumentFileCertificates->snils = isset($post['SNILS'])?$post['SNILS']:null;
            $expertiseDocumentFileCertificates->t_specialist = isset($post['T'])?$post['T']:null;
            $expertiseDocumentFileCertificates->verification_date = (new \DateTime())->format("Y-m-d H:i:s");

            $expertiseDocumentFileCertificates->save();
            return new JsonModel([
                'answer' => "ok",
                'result' => $expertiseDocumentFileCertificates
            ]);

        }
        return new JsonModel([
            'answer' => "error"
        ]);


    }

    public function getExpertiseDocumentFileCertificatesAction()
    {

        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost()->toArray();
            if (isset($post['fileId'])) {
                $expertiseDocumentFileCertificates = EloquentModel\ExpertiseDocumentFileCertificates::where('expertise_document_file_id', '=', $post['fileId'])->get();

            }

        }

        if ($expertiseDocumentFileCertificates->isNotEmpty()) {

            $NotAfter = $expertiseDocumentFileCertificates[0]->NotAfter;
            $NotBefore = $expertiseDocumentFileCertificates[0]->NotBefore;
            $verification_date = $expertiseDocumentFileCertificates[0]->verification_date;
            $expertiseDocumentFileCertificates[0]->NotAfter = (new \DateTime($NotAfter))->format('d.m.Y');
            $expertiseDocumentFileCertificates[0]->NotBefore = (new \DateTime($NotBefore))->format('d.m.Y');
            $expertiseDocumentFileCertificates[0]->verification_date = (new \DateTime($verification_date))->format('d.m.Y H:i:s');

            return new JsonModel([
                'answer' => "ok",
                'result' => $expertiseDocumentFileCertificates
            ]);
        } else {
            return new JsonModel([
                'answer' => "error"
            ]);
        }
    }

    /**
     * сохраняем загруженые файлы
     */
    public function saveExpertiseUploadDocumentAction()
    {
        $request = $this->getRequest();
        //if ($request->isXmlHttpRequest()){
        //$expertiseId = $this->params('id');
        try {
            //die;
            //хочу через транзакцию
            $dbAdapter = \Model\Gateway\ExpertsDbAdapter::getInstance();
            $driver = $dbAdapter->getDriver();
            $connection = $driver->getConnection();
            $connection->beginTransaction();

            $expDocument = new \Model\ExpertEntities\expertisedocument();
            $post = $this->params()->fromPost();

            $user = $this->getAuthService()->getIdentity();
            $expertId = $user->getField('fk_eiisEducationalOrganization');

            //переконвертируем даты
            $isNew = true;
            //if(!empty($post['open_date'])){
            //    $post['open_date'] = date('Y-m-d',  strtotime($post['open_date']));
            //}
            //if(!empty($post['file_date'])){
            //    $post['file_date'] = date('Y-m-d',  strtotime($post['file_date']));
            //    $fileDate = $post['file_date'];
            //    unset ($post['file_date']);
            //}

            //проверяем или создаем
            $expDocumentOld = null;
            if (!empty($post['id'])) {
                $expDocumentOld = $expDocument->getById($post['id']);
            }
            /*
            if(empty($expDocumentOld)
                    && !empty($post['expertise_id'])
                    && !empty($post['document_type_id'])
                    && ((!empty($post['expertise_program_id'])
                            && $post['document_type_id'] == 29)
                        ||$post['document_type_id']==40)
                ){
                $where = array(
                    'document_type_id' =>$post['document_type_id'],
                    'expertise_id'=>$post['expertise_id'],
                    'expert_id' => $expertId,
                );
                if($post['document_type_id']==29){
                    $where ['expertise_program_id']= $post['expertise_program_id'];
                }
                $expDocumentOld = $expDocument->getByWhere($where);
            }
            */
            if (!empty($expDocumentOld)) {
                $expDocument = $expDocumentOld;
                $isNew = false;
            }
            unset($expDocumentOld);
            //костыль когда создается пустое там ид ''
            if (empty($post['id'])) {
                unset($post['id']);
            }
            //Дата заполнения экспертом
            if (empty($post['open_date'])) {
                //$post['open_date'] = date('Y-m-d');
            } else {
                $post['notification_date'] = $post['open_date'];
                unset($post['open_date']);
            }
            //дата ответа эксперта
            if (empty($post['notification_date'])) {
                $post['notification_date'] = date('Y-m-d');
            }
            $post['expert_id'] = $expertId;
            //сохраниние
            if ($post['document_type_id'] != 29) {
                unset($post['expertise_program_id']);
            }
            $date_create = date('Y-m-d');
            if (isset($post['file_date'])) {
                $date_create = date('Y-m-d', strtotime($post['file_date']));
                unset($post['file_date']);
            }
            $organization_id = '';
            if (isset($post['organization_id'])) {
                $organization_id = $post['organization_id'];
                unset($post['organization_id']);
            }

            $expDocument->exchangeArray($post);
            $expDocument->save($isNew);

            $expDocumentIsNew = $isNew;
            //$SendFileClass          = new SendFile();
            $allowed = array("gif", "jpg", "png", "jpeg", "rtf", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx", "sig");

            if (!empty($_FILES)) {
                foreach ($_FILES as $key => $one_file) {
                    if (empty($one_file['size'])) {
                        continue;
                    }
                    // $applicationFiles_class                = new \Model\Entities\isgaApplicationFiles();
                    $save_data = array();
                    $ext = substr(strrchr($one_file['name'], '.'), 1);
                    $name = explode('.', $one_file['name']);
                    $ext = end($name);
                    $ext = strtolower($ext);
                    if (!in_array($ext, $allowed)) {
                        /*throw new \Exception('неразрешенное расширенее '.$ext);*/
                        echo '<script>parent.useTopHoverTitleError("Неразрешенное расширение ' . $ext . '",2000);</script>';
                        die;
                    }
                    $name_info_file = $one_file['name'];
                    $name_info_file_hash = md5(\Ron\Controller\IndexController::translit('', $name[0]) . '_' . md5(microtime(true) . rand(0, 1000) . rand(0, 1000))) . '.' . $ext;
                    $expertise = $expDocument->getExpertise();
                    $id_appl = $expertise->getField('applications_id');
                    $expDocType = $expDocument->getDocumentType();

                    $short_path_dir = 'synch/' . date('Y') . '/' . $id_appl . '/' . $expDocType->getField('code') . '/';
                    $full_path_dir = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' . $short_path_dir;
                    if (!is_dir($full_path_dir)) {
                        if (!mkdir($full_path_dir, 0777, true)) {
                            throw new \Exception('нет директории ' . $short_path_dir . ',  проверить права доступа');
                        }
                    }
                    $path = $full_path_dir . $name_info_file_hash;
                    $short_path = $short_path_dir . $name_info_file_hash;
                    if (!move_uploaded_file($one_file['tmp_name'], $path)) {
                        throw new \Exception('не смогло сохранить ' . $short_path . ',  проверить права доступа');
                    };
                    $name_info_file_hash = $short_path;
                    //$result              = $SendFileClass->SendFileToOpenServer($one_file, array("path" => '/public/uploads/'.$short_path_dir, "name" => $name_info_file_hash),10485760);
                    //if ($result == "1") {
                    //    $name_info_file_hash = $short_path_dir . $name_info_file_hash;
                    //} else {
                    //    $error = $result ;
                    //}
                }
            }


            //Вписываем файл
            if (!empty($name_info_file) && !empty($name_info_file_hash)) {
                $isNew = true;
                $expDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                $expDocumentFileOld = $expDocumentFile->getByWhere(array('expertise_document_id' => $expDocument->getId()));
                if (!empty($expDocumentFileOld)) {
                    $expDocumentFile = $expDocumentFileOld;
                    $isNew = false;
                }
                //типа Удаляем старый файл
                if (!$expDocumentFile->isEmptyField('HashName')) {
                    $short_path = $expDocumentFile->getField('HashName');
                    $pathToDelFile = $_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' . $short_path;
                    //пока не надо удалять,позже
                    //unlink($path);
                }
                $expDocumentFile->setField('institution_id', $organization_id);
                $expDocumentFile->setField('RealName', $name_info_file);
                $expDocumentFile->setField('HashName', $name_info_file_hash);
                //$date_create = $expDocumentFile->getDate('date_create');
                /*if(empty($date_create)){*/
                $expDocumentFile->setField('date_create', $date_create);
                /*}*/
                $expDocumentFile->setField('expertise_document_id', $expDocument->getId());
                $expDocumentFile->save($isNew);
            }
            //если все путем, то меняем статус экспертизе
            /*if(empty($expertise)){
                $expertise = $expDocument->getExpertise();
            }
            $expertiseStstusId = $expertise->getFieldOrSpace('external_status_type_id');
            if($expertiseStstusId != 16){
               $expertise->setNewStatus(16);
            }*/
            $connection->commit();
            //Если все хорошо, то удалим старый файл
            if (!empty($pathToDelFile)) {
                @unlink($pathToDelFile);
            }
        } catch (\Exception $e) {
            $connection->rollback();
            echo '<script>parent.useTopHoverTitleError("Ошибка при сохранении",2000);</script>';
            $s = $e->getLine() . ' - ' . $e->getMessage() . '  \n' . $e->getTraceAsString();
            $s = str_replace("\n", '\n', $s);
            echo '<script>console.log("' . $s . '");</script>';
            die;
        }
        echo "<script>parent.expertiseUploadDocumentSaveOk('" . $expDocumentFile->getId() . "');</script>";
        die;
    }


    /**
     * Удаление документа пользователя
     *
     * @return ViewModel
     */
    public function deleteExpertDocumentAction()
    {
        $docId = $this->params('id');
        $expDocument = new \Model\ExpertEntities\expertisedocument();
        if (!empty($docId)) {
            $expertiseDocumentOne = $expDocument->getById($docId);
        } else {
            echo 'error';
            die;
        }
        $expertiseDocumentFile = $expertiseDocumentOne->getDocumentFile();
        if (!empty($expertiseDocumentFile)) {
            if (!$expertiseDocumentFile->isEmptyField('HashName')) {
                $fileHash = $expertiseDocumentFile->getFieldOrSpace('HashName');
                if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' . $fileHash)) {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/public/uploads/' . $fileHash);
                }
            }
            $expertiseDocumentFileId = $expertiseDocumentFile->getId();
            $EDFC = EloquentModel\ExpertiseDocumentFileCertificates::where('expertise_document_file_id', $expertiseDocumentFileId)->get()->first();
            if (!empty($EDFC)) {
                $EDFC->delete();
            }


            $expertiseDocumentFile->delete();
        }
        $expertiseDocumentOne->delete();
        echo "ok";
        die;
    }

    /**
     * Диалог прикрепления  файла к документа
     *
     * @return ViewModel
     */
    public function getExpertiseDocumentUploadDialogAction()
    {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $docId = $this->params('id');
            $expertiseId = $this->params('id2');
            //$expertId = $this->params('id3');
            if (empty($docId) && empty($expertiseId)) {
                echo('Не указан номер документа');
                die;
            } elseif (empty($docId) && !empty($expertiseId)) {
                $docId = 0;
            } else {
                echo('Не номера экспертизы и эксперта');
                die;
            }

            //нужен эксперт

            $expDocument = new \Model\ExpertEntities\expertisedocument();
            if (!empty($docId)) {
                $expDocument = $expDocument->getById($docId);
            } else {
                //$expDocument->setField('document_type_id',);
                $expDocument->setField('expertise_id', $expertiseId);
                //$expDocument->setField('expert_id',$expertId);
            }

            if (empty($expDocument)) {
                echo('Нет такого документа');
                die;
            }


            //$file = $expertiseDocument->getDocumentFile();

            $view = new ViewModel();
            $view->setVariables(array(
                'expertiseDocument' => $expDocument,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }


    public function applicationFilesAction()
    {
        $id_appl = $this->params('id');
        $expertise_id = $this->params('id2');

        $complicatedSelect = new Model\Entities\isgamtmDocumentReason();

        $application_class = new Model\Gateway\EntitiesTable('isga_Applications');
        $currAppl = $application_class->getEntityById($id_appl);
        $educationOrganization_class = new Model\Gateway\EntitiesTable('eiis_EducationalOrganizations');
        $educationOrganization = $educationOrganization_class->getEntityById($currAppl->getField('fk_eiisEducationalOrganization'));
        $orgData = $educationOrganization->getEducationalOrganizationData()->toArray();
        $idsReasons = $currAppl->getIdsReasons();
        $Documents = array();
        if (!empty($idsReasons))
            $Documents = $complicatedSelect->selectComplicated(
                'isga_mtm_Document_Reason',
                array('fk_isgaDocumentTypes', 'Sorting', 'Obligation'),
                array(
                    'isga_mtm_Document_Reason.fk_isgaEducationalOrganizationSpecifics' => $orgData['fk_isgaEducationalOrganizationSpecific'],
                    'isga_mtm_Document_Reason.fk_isgaApplicationReasons' => $idsReasons[0]
                ),
                NULL,
                'isga_mtm_Document_Reason.Sorting ASC',
                NULL,
                NULL,
                'isga_DocumentTypes',
                'isga_mtm_Document_Reason.fk_isgaDocumentTypes = isga_DocumentTypes.Id',
                array('Id', 'Name')
            )->toArray();
        $currFile = $complicatedSelect->selectComplicated(
            "isga_ApplicationFiles",
            array('Id', 'RealName', 'HashName', 'fk_isgaDocumentType', 'isArhive', 'ApprCurator', 'ApprManager'),
            'fk_isgaApplication = "' . $id_appl . '" AND isArhive = "0"'
        );

        /*получим дополнительные документы из таблицы isga_ApplicationDocumentGroups*/
        $application_document_groups_class = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentGroups');
        $application_document_obj = $application_document_groups_class->getEntitiesByWhere("fk_isgaApplication = '" . $id_appl . "' AND isArhive='0'");
        $app_doc_docs = array();
        $isgaApplicationDocumentType = array();
        $application_document_types = array();
        foreach ($application_document_obj as $ado) {
            $isgaApplicationDocumentType[] = $ado->getField('fk_isgaApplicationDocumentType');
            $ftp_directory = $ado->getField('FtpDirectoryUrl');
            if (!empty($ftp_directory)) {
                $app_doc_docs[mb_strtolower($ado->getField('fk_isgaApplicationDocumentType'), 'UTF-8')][] = array(
                    "FtpDirectoryUrl" => $ftp_directory,
                    "RealFileName" => $ado->getField('RealFileName'),
                    "Id" => $ado->getField('Id'),
                    'isArhive' => $ado->getFieldOrSpace('isArhive'),
                );
            }

        }
        if (count($app_doc_docs) > 0) {
            $application_document_types_class = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentTypes');
            $application_document_types_obj = $application_document_types_class->getEntitiesByWhere(array("id" => $isgaApplicationDocumentType));
            foreach ($application_document_types_obj as $adt) {
                $application_document_types[mb_strtolower($adt->getField('Id'), 'UTF-8')] = $adt->getField('Name');
            }
        }
        //получим массив типов документов которые не входят в диапазон причинных связей
        $document_types = array();
        if (!empty($Documents)) {
            foreach ($Documents as $doc_reason) {
                $document_types[] = $doc_reason['fk_isgaDocumentTypes'];
            }
        }
        $application_types = array();
        $oldDoc = $currFile->toArray();
        foreach ($oldDoc as $docs) {
            if (!in_array($docs['fk_isgaDocumentType'], $document_types)) {
                $application_types[] = $docs['fk_isgaDocumentType'];
            }
        }
        $application_types_without_reason = array();
        if (count($application_types) > 0) {
            $document_types_class = new Model\Gateway\EntitiesTable('isga_DocumentTypes');
            $document_types_obj = $document_types_class->getEntitiesByWhere(array("Id" => $application_types));
            foreach ($document_types_obj as $dt) {
                $application_types_without_reason[mb_strtolower($dt->getField('Id'), 'UTF-8')] = $dt->getField('Name');
            }
        }
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $user_id = $user->getField('fk_eiisEducationalOrganization');

        $chairmanModel = new \Model\Gateway\ExpertsEntitiesTable('chairman');
        //$expertise_id = 0;
        $members = new \Model\ExpertEntities\commissionmember();
        $member = $members->getEntityByWhere('expert_id="' . $user_id . '" AND expertise_id="' . $expertise_id . '"');

        $org_id = $member['institution_id'];

        $chairmans = $chairmanModel->getEntityByWhere('expert_id="' . $user_id . '" AND expertise_id="'
            . $expertise_id . '" AND institution_id="' . $org_id . '"');
        if($chairmans){
            $chairman = $chairmans->getExpert();
        }


        $user = $this->getAuthService()->getIdentity();
        $this->layout()->identityUser = $user;
        $id = $user->getField('fk_eiisEducationalOrganization');

        $model_org = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");

        $contactingClass = new Model\ExpertEntities\contacting();
        $contacting = $contactingClass->selectComplicated(
            "contacting",
            array('expertise_id'),
            array('expert_id = "' . $id . '"', 'contacting_result NOT IN ("СonsentDenided","Отказ")'),
            'expertise_id'
        )->toArray();
        $expertises = false;
        $chairmansByExpertise = false;

        if ($contacting) {
            $expertises_id = array_column($contacting, 'expertise_id');
            $expertisesIdArray = $expertises_id;
            //$expertises_id = '"'.implode('","',$expertises_id).'"';
            $expertiseClass = new Model\ExpertEntities\expertise();
            $expertises = $expertiseClass->selectComplicated(
                "expertise",
                array('organization_id', 'id', 'accreditation_number', 'accreditation_procedure_type', 'create_timestamp', 'end_timestamp'),
                array(
                    'expertise.id' => $expertises_id,
                    'expertise_document.document_type_id' => 37,
                    'expertise_document.expert_id' => $id,
                    new \Zend\Db\Sql\Predicate\IsNotNull('expertise_document.traffic_date'),
                ),
                /*
                'expertise.id IN ('.$expertises_id.') AND
                    expertise_document.document_type_id="9" AND
                    expertise_document.fk_status != "613183d2-33b1-4f93-89d4-8ba9f158c467" AND
                    expertise_document.expert_id = "'.$id.'"',
                */
                /*array('id' => $expertises_id,'create_timestamp <= "'.date('Y-m-d').' 00:00:00"','end_timestamp >= "'.date('Y-m-d').' 00:00:00"'),*/
                'expertise.id',
                'create_timestamp DESC',
                null,
                null,
                'expertise_document',
                'expertise_document.expertise_id = expertise.id',
                'fk_status',
                'left'
            )->toArray();
        }

$w = new Model\ExpertEntities\commissionmember();
        $res = $w->getAllByWhere(['expertise_id' => $expertise_id, 'expert_id' => $id]);
        $applc = $res[0]->getApplicationProgram();
        $application_file_corrupt = EloquentModel\ApplicationFileCorrupt::where('fk_isgaApplication', $id_appl)->get();
        $applicationExtraDocument = ApplicationExtraDocument::where('fk_isgaApplication',$id_appl)->get()->toArray();
        $view = new ViewModel();
        $view->setVariables(
            array(
                'application_file_corrupt' => $application_file_corrupt,
                'oop' => $applc,
                'expertise_id' => $expertise_id,
                "userId" => $user_id,
                "id_appl" => $id_appl,
                "cur_appl" => $currAppl,
                'arrayDocs' => $Documents,
                'oldDoc' => $oldDoc,
                "dop_docs" => $app_doc_docs,
                'application_document_types' => $application_document_types,
                'application_types_without_reason' => $application_types_without_reason,
                'application_file_corrupt' => $application_file_corrupt,
                'RecallScan' => array("path" => $currAppl->getField('RecallApplicationScan'),
                "name" => $currAppl->getField('RecallApplicationScanFileName')),
                'educationOrganization' => $educationOrganization,
                'chairman' => $chairman,
                'applicationExtraDocument' => $applicationExtraDocument
            )
        );
        $view->setTerminal(true);
        return $view;
    }


    /**
     * Генирация файла заключения
     */
    public function generationWordTempConclusionAction()
    {
        $expertiseId = $this->params('id');
        if (empty($expertiseId)) {
            die;
            return false;
        }
        $generationWordConclusion = new Expertise\CWordDocumentGeneration();

        //Получаем кол-во программ
        $countPrograms = $generationWordConclusion->getCountProgram($expertiseId);

        //Получаем данные для заполнения шаблона
        $arrayData = $generationWordConclusion->generateDataForTemplateConclusion($expertiseId, $countPrograms);

        $ourDocType = 'docx';
        $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
        $templateName = $templatePath . $generationWordConclusion->getTemplateName(); //шаблон документа
        $outDocName = $generationWordConclusion->getDocumentName();
        $templater = new Templater($templateName, array('PROGRAMS' => $arrayData), $ourDocType, $outDocName);
        $templater->makeDocumentOut();
        die;
    }


    /**
     * печатаем отчеты для эксперта
     * @return boolean
     */
    public function expertReportAction()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        $ourDocType = 'docx';
        //пользователь прописывается в документе.
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $expertiseId = $this->params('id');
        $expertiseProgramId = $this->params('id2');
        if (empty($expertiseId) || empty($expertiseProgramId) || empty($user)) {
            die;
            return false;
        }
        $expertiseClass = new \Model\ExpertEntities\expertise($expertiseId);
        $programmClass = new \Model\Entities\isgaApplicationPrograms($expertiseProgramId);

        $expertise = new \Ron\Model\Expertise\Report\Expert($expertiseClass, $user, $programmClass);
        $arrayData = $expertise->getData();
        $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
        $templateName = $templatePath . $expertise->getTemplateName(); //шаблон документа
        $ourDocName = $expertise->getDocumentName();
        $templater = new \Ron\Model\Templater($templateName, $arrayData, $ourDocType, $ourDocName);
        $templater->makeDocumentOut();
        die;
    }

    public function sendMail($document, $address, $name = 'ЗАПРОС.docx')
    {

        $attachment = new \Swift_Attachment(file_get_contents($document), $name, 'application/text');
        $message = (new \Swift_Message())
            ->setSubject('ЗАПРОС ДОПОЛНИТЕЛЬНЫХ МАТЕРИАЛОВ ')
            ->setTo($address)
            ->setFrom('test@artel7.com')
            ->attach($attachment)
            ->setBody("ЗАПРОС ДОПОЛНИТЕЛЬНЫХ МАТЕРИАЛОВ", 'text/html');

        $transport = (new \Swift_SmtpTransport('govmail.wishhost.net', 587))
            ->setUsername('test@artel7.com')
            ->setPassword('5X5b2B9l');
//        $transport = (new \Swift_SmtpTransport('smtp.yandex.ru', 25))
//            ->setUsername('ron.isga.test@ya.ru')
//            ->setPassword('qaz123wsx');
//
        $mailer = new \Swift_Mailer($transport);
        $result = $mailer->send($message);
        return $result;
    }

    public function phpOfficeAction()
    {

        $user = $this->getAuthService()->getIdentity();

        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        if ($_POST['document']) {
//            $tmp_patch = realpath($_SERVER['DOCUMENT_ROOT'] . '/../module/Templates/template1.docx');
            $tmp_patch =realpath($_SERVER['DOCUMENT_ROOT'].'/../module/Templates/'.$_POST['document']);
        }
//        print_r($_POST);
//        die;

//        var_dump('qqq');
//        die;
        $doc = $phpWord->loadTemplate($tmp_patch);

        $properties = $phpWord->getDocInfo();
        foreach ($_POST as $field => $value) {
            $doc->setValue($field, $value);
        }


        $hash = md5($user->getId() . date("Y-m-d H:i:s:ms"));

        $r = $doc->save();
        $file = $r;
        $path = 'module/GetDocumets/Files/';
        $newfile = $path . $user->getId() . '/' . $hash . '.docx';

        if (!file_exists($path . $user->getId())) {
            mkdir($path . $user->getId() . '/', 0777, true);

        }

        if (!copy($file, $newfile)) {
            return false;
        }

//        $_POST['email-to'] = 'mkuznetsov@ppr.ru';
//            if($_POST['email-to']){
//                $address = $_POST['email-to'];
//                $this->sendMail($newfile, $address);
//            }

        return new JsonModel(['hashName' => $hash]);

    }

    public function decodeFileAction()
    {
//        $explode = explode('.' ,$_FILES['file']['name']);
        $user = $this->getAuthService()->getIdentity();
        $file = $this->params('id');
        $path = 'module/GetDocumets/Files/';
        $newfile = $path . $user->getId() . '/' . $file . '.docx';
        $response = new \Zend\Http\Response\Stream();

        $response->setStream(fopen($newfile, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($file));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($newfile) . '"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($newfile),
            'Expires' => '@0', // @0, because zf2 parses date as string to \DateTime() object
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);

        return $response;

    }

    /**
     * documentId/documentType = 100 - hardCode here
     */

    public function setApplicationExtraDocumentAction()
    {
        $user = $this->getAuthService()->getIdentity();
        $request = $this->getRequest();
        if ($request->isPost()) {

            if (isset($_FILES['file'])) {

                $explode = explode('.' ,$_FILES['file']['name']);
                $exp ="";
                for($i=1; $i< count($explode); $i++){
                    $exp = $exp.".".$explode[$i];
                }

                $hash = md5($_FILES['file']['name'] . date("Y-m-d H:i:s:ms"));
//                var_dump('qwe');
//                die;
                $post = $request->getPost()->toArray();
                parse_str($post['form'], $resultPost);
                $post = $resultPost;

                $temp = $_FILES['file']['tmp_name'];
                $path = $_SERVER['DOCUMENT_ROOT'] . "/temp/" . time();
                $isDir = mkdir($path, 0777, true);
                $sign_class = new \Eo\Service\Sign();
                $name_info_file_hash = $sign_class->get_hash_name($_FILES['file']['name']);
                $fullPath = $path . "/" . $name_info_file_hash;

//                $_POST['email-to'] = 'mkuznetsov@ppr.ru';
                $_POST['email-to'] = 'maxim.i.kuznetsov@yandex.ru';

                move_uploaded_file($temp, $fullPath);
                if($_POST['email-to']){
                    $address = $_POST['email-to'];
                    $this->sendMail($fullPath, $address, $_FILES['file']['name']);
                    $w =2;
                }
                $q =1;
                //$save_data['HashName'] = $fullPath;
                $sign_data = $sign_class->validSignature($fullPath, false, true);
                if ('ok' != $sign_data['status']) {

                    return new JsonModel([
                        'answer' => "error",
                        'status' => $sign_data['result'],
                        'name' => $_FILES['file']['name'],
                    ]);
                }

//                return new JsonModel([
//                    'answer' => "ok",
//                    'tmp' => $fullPath,
//                    'name' => $_FILES['file']['name'],
//                    'status' => $sign_data['status'],
//                    'certificate_info' => $sign_data['certificate_info']
//
//                ]);


                $path = 'module/GetDocumets/extraDocument/';
                $newfile = $path . $user->getId() . '/' . $hash . $exp;

                if (!file_exists('module/GetDocumets')) {
                    mkdir('module/GetDocumets/', 0777, true);
                }
                if (!file_exists($path)) {
                    mkdir('module/GetDocumets/extraDocument/', 0777, true);
                }
                if (!file_exists($path . $user->getId())) {
                    mkdir($path .  $user->getId()  . '/', 0777, true);
                }

            if (!copy($fullPath, $newfile)) {

                return false;
            }


            $newExtra = new EloquentModel\ApplicationFileCorrupt();
            $newExtra->fk_isgaApplication = $post['AppId'];
            $newExtra->RealName = $_FILES['file']['name'];
            $newExtra->HashName = $newfile;
            $newExtra->dateCreate = date('now');
            $newExtra->isArhive = 0;
            $newExtra->corruptText =  $post['corruptText'] ;

            $newExtra->save();


            return new JsonModel([
                'answer' => "ok",
                'status' => $sign_data['status'],
                'certificate_info' => $sign_data['certificate_info']
            ]);
        }
    }
    return new JsonModel(['answer' => "error"]);
    }
}


