<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Ron\Controller;

use Zend\Db\ResultSet\ResultSet;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;
use Zend\Authentication\AuthenticationService;

class IndexController extends AbstractActionController
{
    protected $authservice;

    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                ->get('AuthService');
        }

        return $this->authservice;
    }
   /*
     * Главный Action выводит левое меню футер шапку и шаблон
     */
    public function indexAction()
    {
        //Подключаем js файл
        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $headStuff->appendFile('/js/ron/main.js');
        $headStuff->appendFile('/js/ron/expertise.page.js');
        $headStuff->appendFile('/js/moment.js');
        $headStuff->appendFile('/js/tipsy/jquery.tipsy.js');

        $headStuff = $this->getServiceLocator()->get("viewhelpermanager")->get('headLink');
        $headStuff->appendStylesheet('/js/tipsy/tipsy.css');

        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();

        $id      = $user->getField('fk_eiisEducationalOrganization');

        $model_expert  = new Model\Gateway\ExpertsEntitiesTable('expert');
        $expert = $model_expert->getEntityById($id);
        $org = false;
        if($expert){
            $org = $expert->getField('parent_id');
        }

        $this->layout()->identityUser = $user;

        $view         = new ViewModel();
        $view->setVariables(
            array(
                'org' => $org,
            )
        );
        return $view;
    }


    public function expertiseAction(){
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        $authService = $this->getAuthService();
        $user = $authService->getIdentity();

        $id      = $user->getField('fk_eiisEducationalOrganization');
        $org = false;

        $expertClass  = new \Model\Gateway\ExpertsEntitiesTable("expert");
        $expert = $expertClass->getEntityById($id);
        $comissionMemClass = new \Model\Gateway\ExpertsEntitiesTable("commission_member");
        $expertise = new \Model\ExpertEntities\expertise();
        if($expert->getField('is_organization')!=1){
            $ids = '"'.$id.'"';
        }else{
            $org = true;
            $expertsClass = new \Model\ExpertEntities\expert();
            $experts      = $expertsClass->selectComplicated(
                "expert",
                array('id','last_name','middle_name','name'),
                array('parent_id = "'.$id.'"')
            )->toArray();

            if($experts){
                $ids = array_column($experts, 'id');
                $ids = '"'.implode('","',$ids).'"';
                $exps = array();
                foreach($experts as $exp){
                    $exps[$exp['id']] = $exp['last_name'].' '.$exp['name'].' '.$exp['middle_name'];
                }
            }
        }
        $results_status_model = new \Model\Entities\isgaCollegeResults();
        $results_status = $results_status_model->getAllByWhereParseId(1);
        $expertises = $expertise->getAllByWhereParseId('external_status_type_id IN (15, 16)');
        $items = $comissionMemClass->getAllEntities(null,'expertise_id ASC','expert_id IN ('.$ids.')');
        $arr_items = array();
        foreach($items as $item){
            //узнаем результать согласия
            //отказники проходят мимо
            $contacting = new \Model\ExpertEntities\contacting();
            $contacting = $contacting->getByWhere(array(
                    'expertise_id' => $item->getField('expertise_id'),
                    'expert_id' => $item->getField('expert_id'),
                ));
            if(!empty($contacting)){
                if('СonsentRefused' == $contacting->getField('contacting_result')){
                    continue;
                }
            }
            $res = '';
            $temp = array();
            $programm = $item->getApplicationProgram();
            if(!$programm){
                continue;
            }
            $level = $programm->getEduProgramType();
            $ugs = $programm->getEGS();
            $temp['id'] = $programm->getField('Id');
            $temp['level'] = $level?$level->getField('Name'):'';
            $temp['ugs'] = $ugs?($ugs->getField('Code').' - '.$ugs->getField('Name')):'';
            $temp['programm'] = $programm->getCode().' - '.$programm->getName();
            if(isset($expertises[$item->getField('expertise_id')])){
                $res = $results_status[$item->getField('is_passed')]->getField('Name');
            }
            $temp['res'] = $res;
            if(!isset($arr_items[$item->getField('expert_id')][$item->getField('expertise_id')][$item->getField('institution_id')])){
                $inst = $item->getEducationalOrganization();
                $arr_items[$item->getField('expert_id')][$item->getField('expertise_id')][$item->getField('institution_id')]['date'] = $item->getExpertiseDate();
                $arr_items[$item->getField('expert_id')][$item->getField('expertise_id')][$item->getField('institution_id')]['number'] = $item->getExpertiseNumber();
                $arr_items[$item->getField('expert_id')][$item->getField('expertise_id')][$item->getField('institution_id')]['name'] = $inst->getField('FullName');
                if($org){
                    $arr_items[$item->getField('expert_id')][$item->getField('expertise_id')][$item->getField('institution_id')]['fio'] = $exps[$item->getField('expert_id')];
                }
            }
            $arr_items[$item->getField('expert_id')][$item->getField('expertise_id')][$item->getField('institution_id')]['arr'][] = $temp;
        }

        //Поскольку не можем сделать сртировку через базу придеться прибегнуть к такому методу

        foreach($arr_items as $expert_id=>$item){
            uasort($arr_items[$expert_id],function($item_1, $item_2) {
                $itemDat_1 = null;
                $itemDat_2 = null;
                $y1 = 0;
                $m1 = 0;
                $d1 = 0;
                $y2 = 0;
                $m2 = 0;
                $d2 = 0;

                foreach($item_1 as $val){
                    if( !empty($val["date"])){
                        $itemDat_1 = new \DateTime( $val["date"]);
                        $y1 = (int)$itemDat_1->format('Y');
                        $m1 = (int)$itemDat_1->format('m');
                        $d1 = (int)$itemDat_1->format('d');
                    }
                }
                foreach($item_2 as $val){
                    if( !empty($val["date"])){
                        $itemDat_2 = new \DateTime( $val["date"]);
                        $y2 = (int)$itemDat_2->format('Y');
                        $m2 = (int)$itemDat_2->format('m');
                        $d2 = (int)$itemDat_2->format('d');
                    }
                }

                

                switch(true){
                    case $y1>$y2;
                        return -1;
                    break;
                    case $y1<$y2;
                        return 1;
                    break;
                    case $m1>$m2;
                        return -1;
                    break;
                    case $m1<$m2;
                        return 1;
                    break;
                    case $d1>$d2;
                        return -1;
                    break;
                    case $d1<$d2;
                        return 1;
                    break;
                    default:
                        return 0;
                    break;
                }
                
            });
        }

        $view         = new ViewModel();
        $view->setTerminal(true);
        $view->setVariables(
            array(
                'id' => $id,
                'org' => $org,
                'expert' => $expert,
                'items' => $arr_items
            )
        );
        return $view;
    }

    public function viewExpertiseAction()
    {
        $id                                    = $this->params('id');
        $headStuff                             = $this->getServiceLocator()->get("viewhelpermanager")->get('headScript');
        $user                                  = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser          = $user;
        $view                                  = new ViewModel();
        if (!empty($id)) {
            $model_expertise = new \Model\Gateway\ExpertsEntitiesTable("expertise");
            $applicationsClass  = new Model\Gateway\EntitiesTable('isga_Applications');
            $model_org = new \Model\Gateway\EntitiesTable("eiis_EducationalOrganizations");

            $currentExpertise = $model_expertise->getEntityById($id);
            $org         = $model_org->getEntityById($currentExpertise->getField('organization_id'));
            $currentApplication = $applicationsClass->getEntityById($currentExpertise->getField('applications_id'));
            $currentType = array();
            $currentReason = array();
            $currentLicense = array();
            $currentCert = array();
            if($currentApplication){
                $currentType        = $currentApplication->getType();
                $currentReason      = $currentApplication->getNamesReasons();
                $currentLicense     = $currentApplication->getLinkedItem('eiis_Licenses', array('Id' => $currentApplication->getField("fk_eiisLicense")));
                $currentCert        = $currentApplication->getLinkedItem('isga_Certificates', array('Id' => $currentApplication->getField("fk_isgaPreviousCertificate")));
            }
            $ApplicationDocumentGroups = new Model\Gateway\EntitiesTable('isga_ApplicationDocumentGroups');
            $copyApplicationDoc        = $ApplicationDocumentGroups->getEntitiesByWhere('`fk_isgaApplication` = "' . $currentExpertise->getField('applications_id') . '" AND `fk_isgaApplicationDocumentType` = "0B4B6324-5DAE-4AB8-B973-86C4A39409E1"')->current();
            if($copyApplicationDoc){
                $copyApplicationDoc=$copyApplicationDoc->toArray();
            }
            $expDocument = new \Model\ExpertEntities\expertisedocument();
            $expDocument = $expDocument->getByWhere(array(
                'expertise_id'=>$currentExpertise->getId(),
                'document_type_id' =>8,
            ));

            $view->setVariables(array(
                    "expertise"    => $currentExpertise,
                    "org"    => $org,
                    /*"institution"    => $institution,
                    'accr_numb' => $accr_numb,
                    'idorg' => $id_org,
                    'nameOrg' => $name_org,*/
                    "application"        => $currentApplication,
                    "currentCert"        => $currentCert,
                    "currentLicense"     => $currentLicense,
                    "currentType"        => $currentType,
                    "currentReason"      => $currentReason,
                    "copyApplicationDoc" => $copyApplicationDoc,
                    'expertiseDocumentOrder'=>$expDocument,
                ));
        }

        $view->setTerminal(true);
        return $view;
    }

    public function getCommentFormAction(){
        $id      = $this->params('id');
        $view    = new ViewModel();
        $view->setTerminal(true);
        $view->setVariables(
                array(
                    'id' => $id
                )
        );
        return $view;
    }

    public function setCommentFormAction(){
       $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $data  = $_POST;

            $model = new Model\ExpertEntities\expertcomments();
        $version_data = $model->getExpertCommentVersion($data['expert_id']);
        $version = 1;
        if($version_data->count() > 0){
            $version_data = $version_data->toArray();
            $version = (int)$version_data[0]['version'];
            $version++;
        }
        $model->exchangeArray(array('comment' => trim($data['comment']),"expert_id" => $data['expert_id'], "date" => date("Y-m-d"), "version" => $version, "user_id" => $user->getField('Id')));
        $model->save(1);
        die;
    }

    /**
     * Метод выводит таблицу с сертификатами
     * @return ViewModel
     */
    public function expertsAction(){
        error_reporting(E_ALL);
        ini_set("display_errors", 1);

        $authService = $this->getAuthService();
        $user = $authService->getIdentity();

        $id      = $user->getField('fk_eiisEducationalOrganization');

        $model_expert  = new Model\Gateway\ExpertsEntitiesTable('expert');
        $user = $model_expert->getEntityById($id);

        $parent_id = $user->getField('parent_id');
        $programs    = $user->getExpertProgram($id);
        $eduLevel    = new \Model\Gateway\EntitiesTable("eiis_EduLevels");
        $groupSpec   = new \Model\Gateway\EntitiesTable('isga_EnlargedGroupSpecialities');
        //$personEduc  = new \Model\Gateway\ExpertsEntitiesTable('person_education');
        //$personWork  = new \Model\Gateway\ExpertsEntitiesTable('person_work');
        $personEduc  = $user->getEducationalOrganizationEducation();
        $personWork  = $user->getEducationalOrganizationWork();
        $personWorkNow  = $user->getEducationalOrganizationWorkNow();
        $oldWork  = $user->getEducationalOrganizationWorkOld();
        $academRank  = new \Model\ExpertEntities\academicrank();
        $academDegree= new \Model\ExpertEntities\academicdegree();
        /*$region      = new \Model\Entities\eiisRegions();*/
        $regions      = new \Model\Gateway\EntitiesTable('eiis_Regions');
        $expertCert  = new \Model\Gateway\ExpertsEntitiesTable('expert_certificates');
        $isgaUsers  = new \Model\Gateway\EntitiesTable('isga_Users');
        $SpecDict    = new \Model\Entities\eiisEduPrograms();
        //$orgs  = new \Model\Gateway\EntitiesTable('eiis_EducationalOrganizations');
        //$orgs = $orgs->getAllEntities(null,'RegularName ASC');
        //$orgs_arr = array();
        //foreach($orgs as $org){
        //    $orgs_arr[$org->getField('Id')] = $org->getField('RegularName');
        //}
        $region = $regions->getEntityById($user->getField('region_id'));

        $factAddress = $user->getAddressFact();
        $view        = new ViewModel();
        $view->setTerminal(true);
        $view->setVariables(
            array(
                'parent_id'        => $parent_id,
                'regions'          => $region,
                'ranks'            => $academRank->getArr(),
                'degrees'          => $academDegree->getArr(),
                //'orgs'             => $orgs_arr,
                'factAddress'      => $factAddress,
                'expert'           => $user,
                'user'             => $isgaUsers->getEntityById($id,'fk_eiisEducationalOrganization'),
                'expertCert'       => $expertCert,
                'programs'         => $programs,
                'EduLevelClass'    => $eduLevel,
                'GroupSpec'        => $groupSpec,
                //'personEduc'       => $personEduc->getEntitiesByWhere('person_id = '.$id),
                //'personWork'       => $personWork->getEntitiesByWhere('person_id = '.$id),
                'Contact'       => $user->getContact(),
                'personEduc'       => $personEduc,
                'personWork'       => $personWork,
                'personWorkNow'    => $personWorkNow,
                'oldWork'          => $oldWork,
                'SpecDict'         => $SpecDict,
                'id'               =>$id
            )
        );
        return $view;
    }

    public function getSpecialtyDictionaryAction() {
        $level_id = $_POST['level_id'];
        $group_id = $_POST['group_id'];

        $where = array();
        if(!empty($level_id) && $level_id != '0'){
            $where[] = 'fk_eiisEduProgramType = "'.$level_id.'"';
        }
        if(!empty($group_id) && $group_id != '0'){
            $where[] = 'fk_eiisUgs = "'.$group_id.'"';
        }
        if(count($where)<2){
            $items = array();
        }else{
            $where = implode(' AND ',$where);
            $spec_model = new \Model\Gateway\EntitiesTable("eiis_EduPrograms");
            $items = $spec_model->getAllEntities(null,null,$where);
        }

        $view         = new ViewModel();
        $view->setTerminal(true);
        $view->setVariables(
                array(
                    'items' => $items
                )
        );
        return $view;
    }

    public function getUgsAction() {
        $level_id = $_POST['level_id'];

        $eduProgramsClass = new Model\Entities\eiisEduPrograms();
        $eduPrograms      = $eduProgramsClass->selectComplicated(
            "eiis_EduPrograms",
            array('Id', 'fk_eiisUgs'),
            array('fk_eiisEduLevels = "'.$level_id.'"', 'fk_eiisUgs IS NOT NULL'),
            'fk_eiisUgs'
        )->toArray();
        $enlargedGroupSpecialities = false;

        if($eduPrograms){
            $fk_isgaGSIds                   = array_column($eduPrograms, 'fk_eiisUgs');

            $enlargedGroupSpecialitiesClass = new Model\Entities\isgaEnlargedGroupSpecialities();
            $enlargedGroupSpecialities      = $enlargedGroupSpecialitiesClass->selectComplicated(
                "isga_EnlargedGroupSpecialities",
                array('Id', 'Code', 'Name'),
                array('Id' => $fk_isgaGSIds,'Name IS NOT NULL'),
                null,
                'code ASC,name ASC'
            )->toArray();
        }

        $view         = new ViewModel();
        $view->setTerminal(true);
        $view->setVariables(
                array(
                    'items' => $enlargedGroupSpecialities
                )
        );
        return $view;
    }

    public function addCertificateExpertAction() {
        error_reporting(E_ALL);
        ini_set("display_errors", 1);
        $id      = $this->params('id');
        //$educLevelClass    = new \Model\Gateway\EntitiesTable("eiis_EduLevels");
        $eduProgramsClass = new Model\Entities\eiisEduProgramTypes();
        $educLevels      = $eduProgramsClass->selectComplicated(
            "eiis_EduProgramTypes", array('Id', 'fk_eiisEduLevelsExpert'),
            array('fk_eiisEduLevelsExpert != ""'),
            "fk_eiisEduLevelsExpert",
            "Name ASC"
        )->toArray();
        $enlargedGroupSpecialities = false;

        if($educLevels){
            $fk_isgaGSIds = array_column($educLevels, 'fk_eiisEduLevelsExpert');

            $enlargedGroupSpecialitiesClass = new Model\Entities\eiisEduLevels();
            $enlargedGroupSpecialities      = $enlargedGroupSpecialitiesClass->selectComplicated(
                "eiis_EduLevels",
                array('Id', 'Name'),
                array('Id' => $fk_isgaGSIds,'Name IS NOT NULL'),
                null,
                'name ASC'
            )->toArray();
        }
        //$specGroupClass   = new \Model\Gateway\EntitiesTable('isga_EnlargedGroupSpecialities');
        $typesClass  = new \Model\Gateway\ExpertsEntitiesTable("order_types");
        $view         = new ViewModel();
        $view->setTerminal(true);
        $view->setVariables(
            array(
                'id' => $id,
                'educLevels' => $enlargedGroupSpecialities,
                'types' => $typesClass->getAllEntities(null,'name ASC'),
                //'specGroups' => $specGroupClass->getAllEntities(null,'code ASC,name ASC')
            )
        );
        return $view;
    }

    public function saveCertificateExpertAction() {
        $data=$this->params()->fromPost();
        $expertClass  = new \Model\ExpertEntities\expertcertificates();
        $save['expert_id'] = $data['expert_id'];
        $save['ordertype_id'] = $data['ordertype_id'];
        if($data['ordertype_id'] == '1'){
            $save['doc_number'] = $data['doc_number'];
            $save['date_begin'] = date("Y-m-d", strtotime($data['date_begin']));
            $save['date_end'] = date("Y-m-d", strtotime($data['date_end']));
            $save['doc_date'] = date("Y-m-d");
        }
        $expertClass->exchangeArray($save);
        $expertClass->save(1);
        $id_cert = $expertClass->getLastInsertValue();
        $save_arr = array();
        $save_arr['expert_id'] = $data['expert_id'];
        $save_arr['certificate_id'] = $id_cert;
        $expertEduProgClass  = new \Model\ExpertEntities\experteduprogramms();
       /* foreach($data['Program'] as $k => $v){
            $save_arr['speciality_group_id'] = $v['speciality_group_id'];
            $save_arr['education_level_id'] = $v['education_level_id'];
            //$save_arr['speciality_id'] = $v['speciality_id'];
            $expertEduProgClass->exchangeArray($save_arr);
            $expertEduProgClass->save(1);
        }*/

        foreach($data['Program'] as $k => $v){
            $expertEduProgClass  = new \Model\ExpertEntities\experteduprogramms();
            $save_arr = array();
            $save_arr['expert_id'] = $data['expert_id'];
            $save_arr['certificate_id'] = $id_cert;
            $save_arr['speciality_group_id'] = $v['speciality_group_id'];
            $save_arr['education_level_id'] = $v['education_level_id'];
            $expertEduProgClass->exchangeArray($save_arr);
            $expertEduProgClass->save(1);
        }
        echo 'Ok'; die;
    }

   public function saveExpertiseAction()
   {
       $data = $this->params()->fromPost();
       $expert = $data['Expert'];
       /*$person = $data['Person'];
       $expertDict = $data['Dict'];
       $personEduc = $data['PersonEduc'];*/
       $contact = $data['Contact'];
       $status = 'ok';
       $expertClass  = new \Model\ExpertEntities\expert();
      /* $personClass  = new \Model\ExpertEntities\person();
       $expertDictClass  = new \Model\ExpertEntities\expertdictionary();
       $personEducClass  = new \Model\ExpertEntities\personeducation();*/
       $contactClass  = new \Model\ExpertEntities\contact();
       $usersClass  = new \Model\Entities\isgaUsers();

       $contactClass->exchangeArray($contact);
       if(isset($contact['id'])){
           $contactClass->save();
           $contact_id = $contact['id'];
       }else
           $contact_id = $contactClass->save(1);


       if(isset($expert['is_organization'])&&$expert['is_organization']!=null){
           $expert = $data['Org'];
           $expert['is_organization'] = 1;
       }
       if(isset($expert['birth_date'])){
           $expert['birth_date'] = date('Y-m-d',strtotime($expert['birth_date']));
       }

       $expert['contact'] = $contact_id;
       if(isset($data['academic_degree_id'])){
            $expert['academic_degree_id'] = implode(',',$data['academic_degree_id']);
       }
       if(isset($data['academic_rank_id'])){
            $expert['academic_rank_id'] = implode(',',$data['academic_rank_id']);
       }

       if(!isset($expert['institution_type_id'])) $expert['institution_type_id'] = 3;
       if(!isset($expert['is_gos_secret'])) $expert['is_gos_secret'] = 0;

       if(!isset($expert['is_organization'])||empty($expert['is_organization'])){
           if(!isset($expert['id'])){
               //генерируем пароль
               $chars="qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
               $max=6;
               $size=StrLen($chars)-1;
               $password=null;
               while($max--)
                   $password.=$chars[rand(0,$size)];

               $expert['password'] = $password;
           }
       }

       $expertClass->exchangeArray($expert);
       if(isset($expert['id'])){
            $expertClass->save();
            $expertId = $expertClass->getId();
       } else
            $expertId = $expertClass->save(1);

       if(!isset($expert['is_organization'])||empty($expert['is_organization'])){
           if(!isset($expert['id'])){
               /* Создаем юзера в открытой части */

               $user['Name'] = $expert['last_name'].
                    (!empty($expert['name'])?' '.$expert['name']:'').
                    (!empty($expert['middle_name'])?' '.$expert['middle_name']:'');
               $user['Login'] = $contact['email'];
               $user['Password'] = md5($password);
               $user['Email'] = $contact['email'];
               $user['Role'] = 'expert';
               $user['fk_eiisEducationalOrganization'] = $expertId;

               $usersClass->exchangeArray($user);
               $userId = $usersClass->save(1);

               /***************************************/
           }
       }

       if(isset($data['person_work'])){
            $personWork = $data['person_work'];
            $personWorkText = '';
            if (isset($data['person_work']['text'])) {
                $personWorkText = $data['person_work']['text'];
                unset($data['person_work']['text']);
            }
            $personWorkClass = new \Model\ExpertEntities\personwork();
            $personWorkClass->deleteWhere(array('person_id'=>$expertId));
            if (!empty($personWork) && is_array($personWork)) {
                foreach ($personWork as $value) {
                    if(is_string($value)){
                        $personWorkClass = new \Model\ExpertEntities\personwork();
                        $dataToBase = array();
                        $dataToBase['person_id']=$expertId;
                        $dataToBase['organization_id']=$value;
                        if($data['mywork'] == $value){
                            $dataToBase['now']='1';
                        }
                        $personWorkClass->exchangeArray($dataToBase);
                        $personWorkClass->save(true);
                    }
                }
            }
            if (!empty($personWorkText) && is_array($personWorkText)) {
                foreach ($personWorkText as $value) {
                    $personWorkClass = new \Model\ExpertEntities\personwork();
                    $dataToBase = array();
                    $dataToBase['person_id']=$expertId;
                    $dataToBase['text']=$value;
                    if($data['mywork'] == $value){
                        $dataToBase['now']='1';
                    }
                    $personWorkClass->exchangeArray($dataToBase);
                    $personWorkClass->save(true);
                }
            }
        }

       if(isset($data['person_education'])){
            $personEducation = $data['person_education'];
           $personEducationText = '';
           if (isset($data['person_education']['text'])) {
               $personEducationText = $data['person_education']['text'];
               unset($data['person_education']['text']);
           }
            $personEducationClass = new \Model\ExpertEntities\personeducation();
            $personEducationClass->deleteWhere(array('person_id' => $expertId));
            if (!empty($personEducation) && is_array($personEducation)) {
                foreach ($personEducation as $value) {
                    if(is_string($value)){
                        $personEducationClass = new \Model\ExpertEntities\personeducation();
                        $dataToBase = array();
                        $dataToBase['person_id'] = $expertId;
                        $dataToBase['organization_id'] = $value;
                        $personEducationClass->exchangeArray($dataToBase);
                        $personEducationClass->save(true);
                    }
                }
            }
            if (!empty($personEducationText) && is_array($personEducationText)) {
                foreach ($personEducationText as $value) {
                    $personEducationClass = new \Model\ExpertEntities\personeducation();
                    $dataToBase = array();
                    $dataToBase['person_id']=$expertId;
                    $dataToBase['text']=$value;
                    $personEducationClass->exchangeArray($dataToBase);
                    $personEducationClass->save(true);
                }
            }
        }
        if(isset($expert['parent_id'])&&$expert['parent_id']!=null){
            echo '<script>parent.$(".buttonParentClass  .show_tab.active").click();parent.$(".dialog, .new-dialog-view-card").remove();parent.$(".dialog, .dialog-shot-view-card").remove();</script>';
        }else{
            echo '<script>parent.handleResponse("' . $status . '",' . $expertId . ')</script>';
        }
        die;
   }

    public function agreementsAction(){
        $authService = $this->getAuthService();
        $user = $authService->getIdentity();

        $id      = $user->getField('fk_eiisEducationalOrganization');

        $expertClass  = new \Model\Gateway\ExpertsEntitiesTable("expert");
        $typesClass  = new \Model\Gateway\ExpertsEntitiesTable("order_types");
        $expert = $expertClass->getEntityById($id);
        $certificates = $expert->getExpertCertificates();
        $types = $typesClass->getAllEntities(null,'name ASC');
        $type_arr = array();
        foreach($types as $type){
            $type_arr[$type->getField('id')] = $type->getField('name');
        }
        $view         = new ViewModel();
        $view->setTerminal(true);
        $view->setVariables(
            array(
                'types' => $type_arr,
                'certificates' => $certificates
            )
        );
        return $view;
    }

    public function getEduOrgAction()
    {
        $val = $this->params('id2');
        $now = $this->params('id3');

        $page = $this->params('id');
        if (!empty($val)) {
            $search = $val;
        } else {
            $search = ' ';
        }
        $user                                  = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $educationOrganization                 = $user->getEducationOrganization();
        $this->layout()->educationOrganization = $educationOrganization;
        $this->layout()->identityUser          = $user;
        $organizationClass                     = new Model\Entities\eiisEducationalOrganizations();
        $sql                                   = $organizationClass->confertSearchToSql($search);
        if (empty($sql)){
            $sql='1';
        }
        $countAll                              = $organizationClass->selectComplicated("eiis_EducationalOrganizations",array('Id'), $sql);
        $orgArray                              = $organizationClass->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName'), $sql, NULL, NULL, 20, $page);
        //$countAll                              = $organizationClass->selectComplicated("eiis_EducationalOrganizations",array('Id'), '`FullName` LIKE "%' . $search . '%" OR `ShortName` LIKE "%' . $search . '%" OR `RegularName` LIKE "%' . $search . '%" OR `Inn` ="' . $search . '"');
        //$orgArray                              = $organizationClass->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName'), '`FullName` LIKE "%' . $search . '%" OR `ShortName` LIKE "%' . $search . '%" OR `RegularName` LIKE "%' . $search . '%" OR `Inn` ="' . $search . '"', NULL, NULL, 20, $page);
        $view                                  = new ViewModel();
        $maxPage=ceil(count($countAll)/20);
        $view->setVariables(array(
                "orgs" => $orgArray->toArray(),
                'search' => $search,
                'nextPage'=>$page+1,
                'maxPage'=>$maxPage,
                'now'=>$now
        ));
        $view->setTerminal(true);
        return $view;
    }


    /**
     * печатаем распоряжение на проведение экспертизы
     * @return boolean
     */
    public function expertiseDocumentAction()
    {
        //ini_set('error_reporting', E_ALL);
        //ini_set('display_errors', 1);
        //ini_set('display_startup_errors',1);
        $ourDocType='docx';
        //пользователь прописывается в документе.
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        //$certificateWhat = $this->params('id');
        //$certificateId = $this->params('id2');
        $applicationId = $this->params('id');
        if(empty($applicationId)||empty($user)/*||empty($certificateWhat)*/){
            die;
            return false;
        }
            //$certificate=\Ron\Model\ExportCertificate::certificateFabrica($certificateId,$certificateWhat,$applicationId);
            //if(!$certificate){
            //    die;
            //    return false;
            //}

            $expertise = new \Ron\Model\Expertise\ExpertiseIssueOrder($applicationId);
            $arrayData = $expertise->getData();
            $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
            $templateName = $templatePath.$expertise->getTemplateName(); //шаблон документа
            $ourDocName=$expertise->getDocumentName();
            $templater= new \Ron\Model\Templater($templateName,$arrayData,$ourDocType,$ourDocName );
            $templater->makeDocumentOut();
            die;
    }

    /**печать договора с экспертом
     *
     * @return boolean
     */
    public function expertiseDocumentContractAction()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors',1);
        $ourDocType='docx';
        $contractId = $this->params('id');
        if(empty($contractId)){
            die;
        }
            //$expertise = new \Ron\Model\Expertise\Contract\Person($contractId);
            $expertise = \Ron\Model\Expertise\ExpertiseDocument::fabric($contractId);
            if(empty($expertise)){
                echo  'не найден документ';
            }
            //$expertise = new \Ron\Model\Expertise\ExpertiseDocument
            $arrayData = $expertise->getData();
            $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
            $templateName = $templatePath.$expertise->getTemplateName(); //шаблон документа
            $ourDocName=$expertise->getDocumentName();
            $templater= new \Ron\Model\Templater($templateName,$arrayData,$ourDocType,$ourDocName );
            $templater->makeDocumentOut();
            die;
    }

    /**
     * Сохранение результатов контактов с экспертом
     */
    public function saveContactingAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $allowed = array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx","sig");
            $post = $this->params()->fromPost();

            foreach ($_FILES as $key => $one_file) {
                $applicationFiles_class                = new \Model\Entities\isgaApplicationFiles();
                $save_data = array();
                $ext  = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
                $name = explode('.', $one_file['name']);
                if ($one_file['size'] != 0) {
                    if (isset($one_file) && $one_file['error'] == 0) {
                        $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
                        $name = explode('.', $one_file['name']);
                        if ($one_file['size'] < 10 * 1024 * 1024) {
                            if (in_array(mb_strtolower($ext), $allowed)) {
                                $ext = strrchr($one_file['name'], '.');
                                $name_info_file = $one_file['name'];
                                $name_info_file_hash = md5($this->translit('', $name[0]) . '_' . md5(time())) . $ext;

                                //определяем экспертизу у тип документа
                                $expertise = new \Model\ExpertEntities\expertise();
                                $expertise = $expertise->getById($post['expertise_id']);
                                if(empty($expertise)){
                                    throw new Exception('Не найдено заявление');
                                }
                                $appId = $expertise->getField('applications_id');
                                $expDocType = new \Model\ExpertEntities\documenttype(35);

                                $short_path_dir = 'synch/'.date('Y').'/'.$appId.'/'.$expDocType->getField('code').'/';
                                $full_path_dir =  $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$short_path_dir;
                                if(!is_dir($full_path_dir)){
                                    if(!mkdir($full_path_dir,0777,true )){
                                        throw new \Exception('нет директории '.$short_path_dir.',  проверить права доступа');
                                    }
                                }
                                $path = $full_path_dir.$name_info_file_hash;
                                $short_path = $short_path_dir.$name_info_file_hash;
                                if(!move_uploaded_file($one_file['tmp_name'], $path )){
                                    throw new \Exception('не смогло сохранить '.$short_path.',  проверить права доступа');
                                };
                                //$name_info = $name_info_file . "::" . $name_info_file_hash;
                                $name_info_file_hash=$short_path;

                                $name_info = $name_info_file . "::" . $name_info_file_hash;
                                $post['HashName'] = $name_info_file_hash;
                                $post['RealName'] = $name_info_file;
                                //$save_data['fk_isgaDocumentType'] = substr($key, 0, -2);
                                //$save_data['fk_isgaApplication'] = $id_appl;
                                //$applicationFiles_class->exchangeArray($save_data);
                                //$applicationFiles_class->save(1);
                            } else {
                                $error = "Недопустимый формат файла.";
                            }
                        } else {
                            $error = "Размер файла превышает 10 Мб";
                        }
                    } else {
                        $error = "Ошибка загрузки файла";
                    }
                }
            }
            $result = 'ExpertDefined';
            if( !empty($post['date_send']) ){
               $post['date_send'] = date('Y-m-d',strtotime($post['date_send']));
               $result = 'AgreementSend';
            }
            if( !empty($post['date_answer']) ){
               $post['date_answer'] = date('Y-m-d',strtotime($post['date_answer']));
               if(empty($post['contacting_result'])){
                   $result = 'СonsentConfirm';
               } else {
                   $result = $post['contacting_result'] ;
               }
            }
            $post['contacting_result'] = $result;
            if(empty($post['expert_id'])){
                echo 'не указан эксперт';
            }
            if(empty($post['expertise_id'])){
                echo 'не указана экспертиза';
            }
            //если отказано, то то уберем файла скана
            if($post['contacting_result']=='СonsentRefused'){
                $post['HashName'] = '';
                $post['RealName'] = '';
            }
            $post['HashName'] = empty($post['HashName'])?'':$post['HashName'];
            $post['RealName'] = empty($post['RealName'])?'':$post['RealName'];
            $contacting = new \Model\ExpertEntities\contacting();
            $contacting->createOrUpdate($post);                         
            echo 'Ok';
        }
        die;
    }

    public function expertiseExpertConsentDocumentAction()
    {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors',1);
        $ourDocType='docx';
        //пользователь прописывается в документе.
        //$user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $expertiseId = $this->params('id');
        $expertId = $this->params('id2');
        $orgId = $this->params('id3');
        if(empty($expertId)||empty($expertId)||empty($orgId)/*||empty($certificateWhat)*/){
            die;
            return false;
        }
            //$certificate=\Ron\Model\ExportCertificate::certificateFabrica($certificateId,$certificateWhat,$applicationId);
            //if(!$certificate){
            //    die;
            //    return false;
            //}

            $expertise = new \Ron\Model\Expertise\expertiseExpertConsent($expertiseId,$expertId,$orgId);
            $arrayData = $expertise->getData();
            $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
            $templateName = $templatePath.$expertise->getTemplateName(); //шаблон документа
            $ourDocName=$expertise->getDocumentName();
            $templater= new \Ron\Model\Templater($templateName,$arrayData,$ourDocType,$ourDocName );
            $templater->makeDocumentOut();
            die;
    }

    /**
     * диалоговое окно согласие с экспертом
     * @return boolean|ViewModel
     */
    public function getExpertiseConsentContactingDialogAction()
    {
        $expertiseId = $this->params('id');
        $expertId = $this->params('id2');
        $orgId = $this->params('id3');
        if(empty($expertId)||empty($expertId)||empty($orgId)){
            die;
            return false;
        }
            //$certificate=\Ron\Model\ExportCertificate::certificateFabrica($certificateId,$certificateWhat,$applicationId);
            //if(!$certificate){
            //    die;
            //    return false;
            //}

        $contacting = new \Model\ExpertEntities\contacting();
        $contacting = $contacting->getByWhere(array(
                'expertise_id' => $expertiseId,
                'expert_id' => $expertId,
                //'' => $orgId
                ));

        if (empty($contacting)){
            $contacting = new \Model\ExpertEntities\contacting();
            $contacting->exchangeArray(array(
                'expertise_id' => $expertiseId,
                'expert_id' => $expertId,
                //'' => $orgId
                ));
        }
        $contacting->setField('institution_id', $orgId);
            $view = new ViewModel();
            $view->setVariables(array(
                'contacting' => $contacting,
            ));
            $view->setTerminal(true);
            return $view;
    }

    /**карточка экспертизы
     * вкладка Документы для эксперта
     *
     * @return ViewModel
     */
    public function getExpertiseDocumentForExpertAction() {
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            //$data = $this->params()->fromPost();
            $post = $this->params()->fromPost();
            if (empty($post['expertise'])){
                die ;
            }
            $expertiseId = $post['expertise'];

            $expertise = new \Model\ExpertEntities\expertise($expertiseId);

            $commission = $expertise->getCommissionMemberWithoutRefused();
            $expertsById = array();
            $filialById =array();
            $commissionByExpert = array();
            $curratorById = array();

            if(!empty($commission)){
                foreach ( $commission as $value ) {
                    $expert = $value->getExpert();
                    if(!empty($expert)){
                        $institutionId = $value->getField('institution_id');
                        if( empty($filialById[$institutionId])){
                            $filal = $value->getEducationalOrganization();
                            if(!empty($filal)){
                               $filialById[$institutionId] = $filal;

                            }
                            unset($filal);
                        }
                        //$expert->setField('institution_id',$institutionId);
                        $expertId = $expert->getId();
                        $expertsById[$expertId] = $expert;
                        $commissionByExpert[$expertId] = $value;

                        $curatorId = $value->getField('user_id');
                        if (!empty($curatorId) && empty($curratorById[$curatorId])){
                            $curratorById[$curatorId] = $value->getUser();
                        }

                        unset($institution);
                        unset($expertId);
                    }
                    unset($expert);
                }
            }

            $chairmans = $expertise->getChairman();
            $chairmainByExpert = array();
            foreach($chairmans as $chairman){
                $chairmainByExpert[$chairman->getField("expert_id")]=$chairman;
                // в перечень экспертов добавим еще и руководителя, если его не было еще
                $expertId = $chairman->getField('expert_id');
                if(empty($expertsById[$expertId]) ){
                    $expert = $chairman->getExpert();
                    if(!empty($expert)){
                        $expertsById[$expertId] = $expert;
                    }
                    $commissionByExpert[$expertId] = $chairman;
                }
            }

            $expDocument = $expertise->getExpertiseDocument(array('document_type_id' =>9));
            $expertiseDocumentByExpert = array();
            foreach ( $expDocument  as $value ) {
                $expertId = $value->getField('expert_id');
                if(!empty($expertId)){
                    $expertiseDocumentByExpert[$expertId] = $value;
                }
                unset($expertId);
            }

            $view = new ViewModel();
            $view->setVariables(array(
                'experts' => $expertsById,
                'chairmains' => $chairmainByExpert,
                'expertiseDocuments' => $expertiseDocumentByExpert,
                'filials' => $filialById,
                'commissions' => $commissionByExpert,
                'curratorById' => $curratorById,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }


    public function getExpertiseExpertDocumentedDialogAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $expertiseId = $this->params('id');
            $expertId = $this->params('id2');
            if (empty($expertiseId) || empty($expertId)){
                echo 'не пришли экспертиза';
                die;
            }
            //$post = $this->params()->fromPost();
            $expertise = new \Model\ExpertEntities\expertise($expertiseId);

            //$expDocTypeIdArray = array(2,9,8,10,37,57,11,13);
            $expDocTypePersonalIdArray = array();
            $expDocTypeAllIdArray = array();
            $expDocTypeIdArray = array();
            $expDocIdArray = array();

            //сообираем общие документы
            $expDocument = $expertise->getExpertiseDocument(array(
                new \Zend\Db\Sql\Predicate\IsNull('expert_id'),
                ));
            $expDocumentArray=array();
            if(!empty($expDocument)){
                foreach ( $expDocument as $value ) {
                    $expDocId = $value->getId();
                    $value->setField('isForAll','1');
                    $expDocumentArray[$value->getId()]=$value;
                    $expDocTypeId = $value->getField('document_type_id');
                    if(!empty($expDocTypeId)){
                        $expDocTypeIdArray[$expDocTypeId] = $expDocTypeId;
                    }
                    $expDocIdArray[$expDocId] = $expDocId;
                }
            }

            //сообираем документы конкретного эксперта
            $expDocument = $expertise->getExpertiseDocument(array(
                'expert_id'=> $expertId,
                ));

            if(!empty($expDocument)){
                foreach ( $expDocument as $value ) {
                    $expDocId = $value->getId();
                    $expDocumentArray[$value->getId()] = $value;
                    $expDocTypeId = $value->getField('document_type_id');
                    if(!empty($expDocTypeId)){
                        //$expDocumentArray[$expDocTypeId] = $value;
                        $expDocTypeIdArray[$expDocTypeId] = $expDocTypeId;
                    }
                    $expDocIdArray[$expDocId] = $expDocId;
                }
            }

            //проверим эксперт конторский, если да, то посмотрим документы конторы
            $expert = new \Model\ExpertEntities\expert($expertId);
            $parentId = $expert->getField('parent_id');
            if(!empty($parentId)){
                $expDocument = $expertise->getExpertiseDocument(array(
                    'expert_id'=> $parentId,
                    ));
                if(!empty($expDocument)){
                    foreach ( $expDocument as $value ) {
                        $expDocId = $value->getId();
                        $expDocumentArray[$value->getId()] = $value;
                        $expDocTypeId = $value->getField('document_type_id');
                        if(!empty($expDocTypeId)){
                            //$expDocumentArray[$expDocTypeId] = $value;
                            $expDocTypeIdArray[$expDocTypeId] = $expDocTypeId;
                        }
                        $expDocIdArray[$expDocId] = $expDocId;
                    }
                }

            }


            //какие типы докумнты применяются
            $expDocTypeArray = array();
            if(!empty($expDocTypeIdArray)){
                $expDocType = new \Model\ExpertEntities\documenttype();
                $expDocType = $expDocType->getByIds($expDocTypeIdArray);
                if(!empty($expDocType)){
                    foreach ( $expDocType as $value ) {
                        $id = $value->getId();
                        $expDocTypeArray[$id] = $value;
                    }
                }
            }

            //собираем файлы документов
            $expDocFileArray = array();
            if(!empty($expDocIdArray)){
                $expDocFile = new \Model\ExpertEntities\expertisedocumentfile();
                $expDocFile = $expDocFile->getByExpertiseDocumentIds($expDocIdArray);
                if(!empty($expDocFile)){
                    foreach ( $expDocFile as $value ) {
                        //$id = $value->getId();
                        $expId = $value->getFieldOrSpace('expertise_document_id');
                        $expDocFileArray[$expId] = $value;
                    }
                }
            }

            $view = new ViewModel();
            $view->setVariables(array(
               'expertiseDocument'      => $expDocumentArray,
               'expertiseDocumentType'  => $expDocTypeArray,
               'expertiseDocumentTypeId'=> $expDocTypeIdArray,
               'expertiseDocumentFile'  => $expDocFileArray,
               'expertId'               => $expertId,
            ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }


    static public function translit($url, $title)
    {
        if ($url == '') {
            $url = $title;
        }
        $space  = '-';
        $transl = array(
            'а'  => 'a', 'б'  => 'b', 'в'  => 'v', 'г'  => 'g', 'д'  => 'd', 'е'  => 'e', 'ё'  => 'e', 'ж'  => 'zh',
            'з'  => 'z', 'и'  => 'i', 'й'  => 'j', 'к'  => 'k', 'л'  => 'l', 'м'  => 'm', 'н'  => 'n',
            'о'  => 'o', 'п'  => 'p', 'р'  => 'r', 'с'  => 's', 'т'  => 't', 'у'  => 'u', 'ф'  => 'f', 'х'  => 'h',
            'ц'  => 'c', 'ч'  => 'ch', 'ш'  => 'sh', 'щ'  => 'sh', 'ъ'  => $space, 'ы'  => 'y', 'ь'  => $space, 'э'  => 'e', 'ю'  => 'yu', 'я'  => 'ya',
            'А'  => 'a', 'Б'  => 'b', 'В'  => 'v', 'Г'  => 'g', 'Д'  => 'd', 'Е'  => 'e', 'Ё'  => 'e', 'Ж'  => 'zh',
            'З'  => 'z', 'И'  => 'i', 'Й'  => 'j', 'К'  => 'k', 'Л'  => 'l', 'М'  => 'm', 'Н'  => 'n',
            'О'  => 'o', 'П'  => 'p', 'Р'  => 'r', 'С'  => 's', 'Т'  => 't', 'У'  => 'u', 'Ф'  => 'f', 'Х'  => 'h',
            'Ц'  => 'c', 'Ч'  => 'ch', 'Ш'  => 'sh', 'Щ'  => 'sh', 'Ъ'  => $space, 'Ы'  => 'y', 'Ь'  => $space, 'Э'  => 'e', 'Ю'  => 'yu', 'Я'  => 'ya',
            ' '  => $space, '_'  => $space, '`'  => $space, '~'  => $space, '!'  => $space, '@'  => $space,
            '#'  => $space, '$'  => $space, '%'  => $space, '^'  => $space, '&'  => $space, '*'  => $space,
            '('  => $space, ')'  => $space, '-'  => $space, '\=' => $space, '+'  => $space, '['  => $space,
            ']'  => $space, '\\' => $space, '|'  => $space, '/'  => $space, '.'  => $space, ','  => $space,
            '{'  => $space, '}'  => $space, '\'' => $space, '"'  => $space, ';'  => $space, ':'  => $space,
            '?'  => $space, '<'  => $space, '>'  => $space, '№'  => $space);

        return strtr($url, $transl);
    }
    /**
     * Выборка экспертов которые принадлежат экспертной организации
     */
    public function getExpertFromOrganizationAction(){
        $organization_Id = $this->params('id');
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser = $user;

        $page = 1;
        $filter = array();
        //Забираем переданные постом данные
        if($this->params()->fromPost("filter") != null)
            $filter =  $this->params()->fromPost("filter");
        if($this->params()->fromPost("page") != null)
            $page = $this->params()->fromPost("page");


        //Создаем объект для работы с таблицей
        $expert_model = new \Model\Gateway\ExpertsEntitiesTable("expert");

        //сотворяем запрос
        $expert = new \Model\ExpertEntities\expert();
        $where = array("parent_id"=>$organization_Id);
        $items_count = $expert_model->getCount($where);
        $pages = intval($items_count / 20);


        $items = $expert_model->getAllEntities(array(
                "page" => $page,
                "onPage" => "20"
            ), "id desc",$where);


        $regionsTable = new \Model\Entities\eiisRegions();
        $regions = $regionsTable->getArrRegion();

        $view = new ViewModel();
        $view->setVariables(array(
                "experts" =>$items->getObjectsArray(),
                "regions" =>$regions,
                "pages" => $pages,
                "currentPage" => $page
            ));
        $view->setTerminal(true);
        return $view;
    }
    public function deleteAllExpertInfoAction(){
        $expertId = $this->params('id');

        $expertInfo = new \Model\ExpertEntities\expert($expertId);

        $expertCertificatesClass = new \Model\ExpertEntities\expertcertificates();
        $expertCertificatesClass->deleteWhere(array('expert_id'=>$expertId));

        $expertCommentsClass = new \Model\ExpertEntities\expertcomments();
        $expertCommentsClass->deleteWhere(array('expert_id'=>$expertId));

        $expertEduProgrammsClass = new \Model\ExpertEntities\experteduprogramms();
        $expertEduProgrammsClass->deleteWhere(array('expert_id'=>$expertId));

        $contactClass = new \Model\ExpertEntities\contact();
        $contactClass->deleteWhere(array('id'=>$expertInfo->getField('contact')));

        $personEducationClass = new \Model\ExpertEntities\personeducation();
        $personEducationClass->deleteWhere(array('person_id'=>$expertId));

        $personWorkClass = new \Model\ExpertEntities\personwork();
        $personWorkClass->deleteWhere(array('person_id'=>$expertId));

        $expertClass = new \Model\ExpertEntities\expert();
        $expertClass->deleteWhere(array('id'=>$expertId));

        echo 'ok';die;
    }


    /**
     * Диалог внесения распорядительного документа
     *
     * @return ViewModel
     */
    public function getExpertiseOrderDocumentIncludeDialogAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()){
            $expertiseId = $this->params('id');

            $expDocument = new \Model\ExpertEntities\expertisedocument();
            $expDocument = $expDocument->getByWhere(array('document_type_id' =>8,'expertise_id'=>$expertiseId));

            $view = new ViewModel();
            $view->setVariables(array(
                    'expertiseDocument'=>$expDocument,
                    'expertiseId'=>$expertiseId,
                    'documentTypeId' => 8,
                ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    public function saveExpertiseDeclarationAction()
    {//isga _applications id сохранить
        $user                                  = $this->getServiceLocator()->get('AuthService')->getIdentity();
        $this->layout()->identityUser          = $user;
        $data              = $_POST;
        unset($data['save_type']);
        $application_class = new Model\ExpertEntities\expertise();
        $save = array();

        $save['create_timestamp'] = date('Y-m-d H:i:s',strtotime($data['create_timestamp']));
        $save['end_timestamp'] = date('Y-m-d H:i:s',strtotime($data['end_timestamp']));

        $save['id'] = $data['id'];
        $save['accreditation_number'] = $data['accreditation_number'];
        if(isset($data['no_leav'])){
            $save['no_leav'] = '1';
        }else{
            $save['no_leav'] = '0';
        }


        $application_class->exchangeArray($save);
        if(!empty($save['id'])){
            $res = $application_class->save();
        }

        echo '<script>parent.handleResponse("' . ($res?'ok':'error') . '",' . $save['id'] . ')</script>';
        die;
    }

    /**
     * сохранение распорядительного документа
     *
     * @return ViewModel
     */
    public function saveExpertiseOrderDocumentAction() {
        $request = $this->getRequest();
        //if ($request->isXmlHttpRequest()){
            //$expertiseId = $this->params('id');
        try{
            //хочу через транзакцию
            $dbAdapter= \Model\Gateway\ExpertsDbAdapter::getInstance();
            $driver=$dbAdapter->getDriver();
            $connection=$driver->getConnection();
            $connection->beginTransaction();

            $expDocument = new \Model\ExpertEntities\expertisedocument();
            $post = $this->params()->fromPost();

            //переконвертируем даты
            $isNew = true;
            if(!empty($post['open_date'])){
                $post['open_date'] = date('Y-m-d',  strtotime($post['open_date']));
            }
            if(!empty($post['file_date'])){
                $post['file_date'] = date('Y-m-d',  strtotime($post['file_date']));
                $fileDate = $post['file_date'];
                unset ($post['file_date']);
            }

            //проверяем или создаем
            $expDocumentOld = null;
            if(!empty($post['id']) && is_numeric($post['id'])){
                $expDocumentOld = $expDocument->getById($post['id']);
            }
            if(empty($expDocumentOld) && !empty($post['expertise_id']) && !empty($post['document_type_id']) && $post['document_type_id']!=13){
                $expDocumentOld = $expDocument->getByWhere(array(
                    'expertise_id'=>$post['expertise_id'],
                    'document_type_id' =>$post['document_type_id'],
                        ));
            }
            if(!empty($expDocumentOld)){
                $expDocument = $expDocumentOld;
                $isNew = false;
            }
            unset($expDocumentOld);
            //костыль когда создается пустое там ид ''
            if(empty($post['id'])){
                unset($post['id']);
            }
            //сохраниние
            $expDocument->exchangeArray($post);
            $expDocument->save($isNew);
            $expDocumentIsNew = $isNew;

            $allowed = array("gif", "jpg", "png", "jpeg", "pdf", "doc", "xls", "xlsx", "doc", "docx", "rar", "zip", "ppt", "pptx","sig");
            foreach ($_FILES as $key => $one_file) {
                $applicationFiles_class                = new \Model\Entities\isgaApplicationFiles();
                $save_data = array();
                $ext  = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
                $name = explode('.', $one_file['name']);
                if ($one_file['size'] != 0) {
                    if (isset($one_file) && $one_file['error'] == 0) {
                        $ext = substr($one_file['name'], 1 + strrpos($one_file['name'], "."));
                        $name = explode('.', $one_file['name']);
                        if ($one_file['size'] < 10 * 1024 * 1024) {
                            if (in_array(mb_strtolower($ext), $allowed)) {
                                $ext = strrchr($one_file['name'], '.');
                                $name_info_file = $one_file['name'];
                                $name_info_file_hash = md5($this->translit('', $name[0]) . '_' . md5(time())) . $ext;
                                $expertise = $expDocument->getExpertise();
                                $id_appl = $expertise->getField('applications_id');
                                $expDocType = $expDocument->getDocumentType();

                                $short_path_dir = 'synch/'.date('Y').'/'.$id_appl.'/'.$expDocType->getField('code').'/';
                                $full_path_dir =  $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$short_path_dir;
                                if(!is_dir($full_path_dir)){
                                    if(!mkdir($full_path_dir,0777,true )){
                                        throw new \Exception('нет директории '.$short_path_dir.',  проверить права доступа');
                                    }
                                }
                                $path = $full_path_dir.$name_info_file_hash;
                                $short_path = $short_path_dir.$name_info_file_hash;
                                if(!move_uploaded_file($one_file['tmp_name'], $path )){
                                    throw new \Exception('не смогло сохранить '.$short_path.',  проверить права доступа');
                                };
                                //$name_info = $name_info_file . "::" . $name_info_file_hash;
                                $name_info_file_hash=$short_path;
                                //$post['HashName'] = $name_info_file_hash;
                                //$post['RealName'] = $name_info_file;
                                //$save_data['fk_isgaDocumentType'] = substr($key, 0, -2);
                                //$save_data['fk_isgaApplication'] = $id_appl;
                                //$applicationFiles_class->exchangeArray($save_data);
                                //$applicationFiles_class->save(1);

                            } else {
                                $error = "Недопустимый формат файла.";
                            }
                        } else {
                            $error = "Размер файла превышает 10 Мб";
                        }
                    } else {
                        $error = "Ошибка загрузки файла";
                    }
                }
            }
            if(!empty($error)){
                $connection->rollback();
                echo '<script>alert("'.$error.'");</script>';
                die;
            }
            //Вписываем файл
            if(!empty($name_info_file)&& !empty($name_info_file_hash)){
                $isNew = true;
                $expDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                $expDocumentFileOld = $expDocumentFile->getByWhere(array('expertise_document_id'=>$expDocument->getId()));
                if(!empty($expDocumentFileOld)){
                    $expDocumentFile = $expDocumentFileOld;
                    $isNew = false;
                }
                //Удаляем старый файл
                if(!$expDocumentFile->isEmptyField('HashName')){
                    $short_path = $expDocumentFile->getField('HashName');
                    $path = $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$short_path;
                    //пока не надо удалять
                    //unlink($path);
                }
                $expDocumentFile->setField('RealName', $name_info_file);
                $expDocumentFile->setField('HashName', $name_info_file_hash);
                $date_create = $expDocumentFile->getDate('date_create');
                if(empty($date_create)){
                    $expDocumentFile->setField('date_create', date('Y-m-d'));
                }
                $expDocumentFile->setField('expertise_document_id', $expDocument->getId());
                $expDocumentFile->save($isNew);
            }
            $connection->commit();
            //если  Распоряжение о создании аккред.комиссии
            //то меняем статус
            $expDocumentTyppeId = $expDocument->getFieldOrSpace('document_type_id');
            if($expDocumentIsNew && $expDocumentTyppeId == 8 ){
                $expertise = $expDocument->getExpertise();
                $application = $expertise->getApplication();
                $application->setNewStatus('c9c7687f-5dee-4719-9d2f-09256c4d5ca7');
            }
        }  catch (\Exception $e){
            $connection->rollback();
            echo '<script>alert("Ошибка при сохранении");</script>';
            echo '<script>console.log("'.$e->getMessage().'");</script>';
            die;
        }
        echo '<script>parent.expertiseOrderDocumentSaveOk();</script>';
        die;
    }

    public function testAction() {
        phpinfo();
    }

    /**
     * Диалог редактирования договора с экспертом
     * генерируем запись договора с эксперто и акта выполненых работ
     *
     * @return ViewModel
     */
    public function getExpertContractEditDialogAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $expertiseId = $this->params('id');
            $expertId = $this->params('id2');
            if(empty($expertId) || empty($expertiseId)){
                echo 'Не получено номер экспертизы или эксперта';
                die;
            }

            $expertise = new \Model\ExpertEntities\expertise();
            $expertise = $expertise->getById($expertiseId);

            $expert = new \Model\ExpertEntities\expert();
            $expert = $expert->getById($expertId);

            $application = $expertise->getApplication();
            $eduOrg = $expertise->getEducationalOrganization();
            $appType = $application->getType();

            if(empty($expert) || empty($expertise)|| empty($application)){
                echo 'Отсутствует экспертиза или эксперт или заявления';
                die;
            }

            $parentId = $expert ->getFieldOrSpace('parent_id');
            //если эксперт в конторе, то на контору
            $expertIdWith = empty($parentId)?$expertId:$parentId;

            //спрашиваем Договор, если есть
            $expertiseDocument = new \Model\ExpertEntities\expertisedocument();
            $expertiseDocument = $expertiseDocument->getByWhere(array(
                'expertise_id'=>$expertiseId,
                'expert_id'=>$expertIdWith,
                'document_type_id'=>9,
                ));

            //спрашиваем Акт, если есть
            $expertiseDocumentAct = new \Model\ExpertEntities\expertisedocument();
            $expertiseDocumentAct = $expertiseDocumentAct->getByWhere(array(
                'expertise_id'=>$expertiseId,
                'expert_id'=>$expertIdWith,
                'document_type_id'=>10,
                ));

            $expertiseDocumenOrder = new \Model\ExpertEntities\expertisedocument();
            $expertiseDocumenOrder = $expertiseDocumenOrder->getByWhere(array(
                'expertise_id'=>$expertiseId,
                'document_type_id'=>8,
                ));

            //скан распоряжения на  распоряжения о создание акред комисии
            $expertiseDocumenOrderFile = new \Model\ExpertEntities\expertisedocumentfile();
            $expertiseDocumenOrderFile = $expertiseDocumenOrderFile->getBywhere(array(
                'expertise_document_id'=>$expertiseDocumenOrder->getId(),
                    ));

            //дергним есть ли акт у эксерта

            $commissionMember = $expertise->getCommissionMember(array('expert_id'=>$expertId));
            $commissionMember = $commissionMember->getObjectsArray();
            $commissionMember = empty($commissionMember[0])?null:$commissionMember[0];

            //$chairman = $expertise->getChairman();
            if(!empty($commissionMember)){
                $currator = $commissionMember->getUser();
            } else {
                $currator = false;
            }

            $chairman = $expertise->getChairman(array('expert_id'=>$expertId));

            //проверяем договор, если нет, то создаем
            $isNew = false;
            if(empty($expertiseDocument)){
                $expertiseDocument = new \Model\ExpertEntities\expertisedocument();
                $isNew = true;
                $expertiseDocument->setField('expert_id',  $expertIdWith);
                $expertiseDocument->setField('expertise_id', $expertiseId);
                $expertiseDocument->setField('document_type_id', 9);
                $id=$expertiseDocument->save($isNew);
                //if(  is_numeric($id)){
                //    $expertiseDocument->setField('id', $id);
                //}
                unset($id);
            }
            //проверяем акт, если нет, то создаем
            $isNewAct = false;
            if(empty($expertiseDocumentAct)){
                $expertiseDocumentAct = new \Model\ExpertEntities\expertisedocument();
                $isNewact = true;
                $expertiseDocumentAct->setField('expert_id',  $expertIdWith);
                $expertiseDocumentAct->setField('expertise_id', $expertiseId);
                $expertiseDocumentAct->setField('document_type_id', 10);
                $id=$expertiseDocumentAct->save($isNewAct);
                //if(  is_numeric($id)){
                //    $expertiseDocumentAct->setField('id', $id);
                //}
                unset($id);
            }
            //проверяем номера документов
            $documentNumber = '';
            if($expertiseDocument->isEmptyField('document_number')){
                $documentNumber = $expertiseDocument->generateNumber(
                                $application,
                                $expertise,
                                $expert,
                                $commissionMember,
                                $chairman,
                                $eduOrg
                                );
                $expertiseDocument->setField('document_number',$documentNumber);
            }
            if($expertiseDocumentAct->isEmptyField('document_number')){
                if(empty($documentNumber)){
                    $documentNumber = $expertiseDocument->getFieldOrSpace('document_number');
                }
                if(empty($documentNumber)){
                    $documentNumber = $expertiseDocumentAct->generateNumber(
                                    $application,
                                    $expertise,
                                    $expert,
                                    $commissionMember,
                                    $chairman,
                                    $eduOrg
                                    );
                }
                $expertiseDocumentAct->setField('document_number',$documentNumber);
            }

            //Договор преоверка результат согласия с экспертом, если не конторский
            if(empty($parentId) && $expertiseDocument->isEmptyField('contacting_id')){
                $contacting = new \Model\ExpertEntities\contacting();
                $contacting = $contacting->getByWhere(array(
                    'expert_id'=>$expertId,
                    'expertise_id'=>$expertiseId,
                ));
                if(!empty($contacting)){
                    $expertiseDocument->setField('contacting_id', $contacting->getId());
                }
            }
            //акт преоверка результат согласия с экспертом, если не конторский
            if(empty($parentId) && $expertiseDocumentAct->isEmptyField('contacting_id')){
                if(empty($contacting)){
                    $contacting = new \Model\ExpertEntities\contacting();
                    $contacting = $contacting->getByWhere(array(
                        'expert_id'=>$expertId,
                        'expertise_id'=>$expertiseId,
                    ));
                }
                if(!empty($contacting)){
                    $expertiseDocumentAct->setField('contacting_id', $contacting->getId());
                }
            }

            //Договор метим как член комисии, если не конторский
            if(empty($parentId) && $expertiseDocument->isEmptyField('commission_member_id')){
                if(!empty($commissionMember)){
                    $expertiseDocument->setField('commission_member_id',$commissionMember->getId());
                }
            }
            //Акт метим как член комисии, если не конторский
            if(empty($parentId) && $expertiseDocumentAct->isEmptyField('commission_member_id')){
                if(!empty($commissionMember)){
                    $expertiseDocumentAct->setField('commission_member_id',$commissionMember->getId());
                }
            }

            //прописываем дату создания документа
            $open_date = $expertiseDocument->getDate('open_date');
            if(empty($open_date)){
                $expertiseDocument->setField('open_date',date('Y-m-d'));
            }
            $open_date = $expertiseDocumentAct->getDate('open_date');
            if(empty($open_date)){
                $expertiseDocumentAct->setField('open_date',date('Y-m-d'));
            }

            //if($expertiseDocument->isEmptyField('notification_date')){
            //    if(!empty($commissionMember)){
            //        $expertiseDocument->setField('notification_date',$commissionMember->getId());
            //    }
            //}
            if($expertiseDocument->isEmptyField('year')){
                $date = $expertise->getFieldOrSpace('create_timestamp');
                $date = substr($date, 0,4);
                $expertiseDocument->setField('year',(int)$date);
            }
            if($expertiseDocumentAct->isEmptyField('year')){
                $date = $expertise->getFieldOrSpace('create_timestamp');
                $date = substr($date, 0,4);
                $expertiseDocumentAct->setField('year',(int)$date);
            }
            $expertiseDocument->save(false);
            $expertiseDocumentAct->save(false);

            $commissionMember = empty($commissionMember)?false:$commissionMember;
            $chairman = empty($chairman)?false:$chairman;

            //дергаем уведомление о принятии заявления в производство
            $notify = new \Model\Entities\isgaNotification();
            $notify = $notify->getByApplicationTemplate($application->getId(), '1B969B9C-7ABE-4416-A988-C643D621DDAB');
            $notifyFile = new \Model\Entities\isgaApplicationFiles();
            $notifyFile = $notifyFile->getByApplicationAndDocumentType($application->getId(),'4DC63BC53-52DC-A436-7ACD-5435ACDE3452CC864');

            //данные об оплате
            $expDocPaymentClass = new \Model\ExpertEntities\expertisedocumentpayment();
            $expDocPayment = $expDocPaymentClass->getById($expertiseDocument->getId(),'expertise_document_id');
            if(empty($expDocPayment)){
                $expDocPayment=$expDocPaymentClass;
                $expDocPayment->setField('expertise_document_id',$expertiseDocumentAct->getId());
            }

            //$post = $this->params()->fromPost();
            $view = new ViewModel();
            $view->setVariables(array(
                    'expertise' => $expertise,
                    'expert' => $expert,
                    'application' => $application,
                    'eductionOrganization' => $eduOrg,
                    'chairman' =>$chairman,
                    'commissionMember' => $commissionMember,
                    'currator' => $currator,
                    'applicationType' => $appType,
                    'expertiseDocument' =>$expertiseDocument,
                    'expertiseDocumentAct' =>$expertiseDocumentAct,
                    'expertiseDocumenOrder' => $expertiseDocumenOrder,
                    'expertiseDocumenOrderFile' => $expertiseDocumenOrderFile,
                    'notify' => $notify,
                    'notifyFile' =>$notifyFile,
                    'expertiseDocumentPayment' => $expDocPayment,
                ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    /**
     * проверяем на наличеие документов для экспертов
     * и создаем их
     */
    public function expertiseDocumentCheckOnCreateAction() {
        $expertiseId = $this->params('id');
            if(empty($expertiseId)){
                echo 'Не получено номер экспертизы ';
                die;
            }

            $expertise = new \Model\ExpertEntities\expertise();
            $expertise = $expertise->getById($expertiseId);
            if(empty($expertise)){
                echo 'Не найдено экспертиза ';
                die;
            }

            $applicationId = $expertise->getField('applications_id');
            $options = new \Model\ExpertEntities\options();

            $expertiseDocument = new \Model\ExpertEntities\expertisedocument();
            //проверяем уведомление о принятии
            //не надо
            /*
            $expertiseDocumentNotify = $expertiseDocument->getByWhere(array(
                'expertise_id' => $expertiseId,
                'document_type_id' => 2,
            ));
            if(empty($expertiseDocumentNotify)){
                //дергаем само уведомление
                $notify = new \Model\Entities\isgaNotification();
                $notify = $notify->getByWhere(array(
                    'fk_isgaApplication' => $applicationId,
                    'fk_isgaNotificationTemplate' => '1B969B9C-7ABE-4416-A988-C643D621DDAB',
                ));
                $notifyNumber = '';
                if(!empty($notify)){
                    $notifyNumber = $notify->getFieldOrSpace('Number');
                }
                $expertiseDocumentNotify = new \Model\ExpertEntities\expertisedocument();
                $expertiseDocumentNotify->setField('document_number',$notifyNumber);
                $expertiseDocumentNotify->setField('document_type_id',2);
                $expertiseDocumentNotify->setField('expertise_id',$expertiseId);
                $expertiseDocumentNotify->setField('open_date',date('Y-m-d'));
                $expertiseDocumentNotify->setField('year',date('Y'));
                $idNewExpDocNotify = $expertiseDocumentNotify->save(true);
                unset($notify);
                unset($notifyNumber);
                //дергаем скан уведомления
                $applicationFileNotify = new \Model\Entities\isgaApplicationFiles();
                $applicationFileNotify = $applicationFileNotify->getByWhere(array(
                    'fk_isgaApplication' => $applicationId,
                    'fk_isgaDocumentType' => '4DC63BC53-52DC-A436-7ACD-5435ACDE3452CC864',
                ));
                if(!empty($applicationFileNotify)){
                    $realName = $applicationFileNotify->getField('RealName');
                    $hashName = $applicationFileNotify->getField('HashName');
                    $expertiseDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                    $expertiseDocumentFile->setField('expertise_document_id', $idNewExpDocNotify);
                    $expertiseDocumentFile->setField('RealName', $realName);
                    $expertiseDocumentFile->setField('HashName', $hashName);
                    $expertiseDocumentFile->setField('date_create', date('Y-m-d'));
                    $expertiseDocumentFile->save(true);
                    unset($realName);
                    unset($hashName);
                }
                unset($applicationFileNotify);
                unset($idNewExpDocNotify);
            }
            unset($expertiseDocumentNotify);
            */

            //Методика проведения экспертизы
            $expertiseDocumentMethodic = $expertiseDocument->getByWhere(array(
                'expertise_id' => $expertiseId,
                'document_type_id' => 57,
            ));
            if(empty($expertiseDocumentMethodic)){
                $docPath = $options->getOption('where_docs_path');
                $methodicName = $options->getOption('file_expertise_methodic');
                $methodicHash = $options->getOption('file_expertise_methodic_hash');

                $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                $expertiseDocumentMethodic->setField('document_number','');
                $expertiseDocumentMethodic->setField('document_type_id',57);
                $expertiseDocumentMethodic->setField('expertise_id',$expertiseId);
                $expertiseDocumentMethodic->setField('open_date',date('Y-m-d'));
                $expertiseDocumentMethodic->setField('year',date('Y'));
                $tupit = true;
                $newId = $expertiseDocumentMethodic->save($tupit);
                    $hashName = $methodicHash;
                    $realName = $methodicName.'.'.substr(strrchr($hashName, '.'), 1);
                    if(!empty($hashName) && !empty($realName)){
                    $expertiseDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                    $expertiseDocumentFile->setField('expertise_document_id', $newId);
                    $expertiseDocumentFile->setField('RealName', $realName);
                    $expertiseDocumentFile->setField('HashName', $hashName);
                    $expertiseDocumentFile->setField('date_create', date('Y-m-d'));
                    $expertiseDocumentFile->save(true);
                }
                    unset($realName);
                    unset($hashName);
            }
            unset($expertiseDocumentMethodic);

            //Информационное письмо эксперту аккредитационной комиссии
            $expertiseDocumentMethodic = $expertiseDocument->getByWhere(array(
                'expertise_id' => $expertiseId,
                'document_type_id' => 11,
            ));
            if(empty($expertiseDocumentMethodic)){
                $options = new \Model\ExpertEntities\options();
                $docPath = $options->getOption('where_docs_path');
                $methodicName = $options->getOption('file_expertise_information_leter');
                $methodicHash = $options->getOption('file_expertise_information_leter_hash');

                $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                $expertiseDocumentMethodic->setField('document_number','');
                $expertiseDocumentMethodic->setField('document_type_id',11);
                $expertiseDocumentMethodic->setField('expertise_id',$expertiseId);
                $expertiseDocumentMethodic->setField('open_date',date('Y-m-d'));
                $expertiseDocumentMethodic->setField('year',date('Y'));
                $newId = $expertiseDocumentMethodic->save(true);
                    $hashName = $methodicHash;
                    $realName = $methodicName.'.'.substr(strrchr($hashName, '.'), 1);
                if(!empty($hashName) && !empty($realName)){
                    $expertiseDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                    $expertiseDocumentFile->setField('expertise_document_id', $newId);
                    $expertiseDocumentFile->setField('RealName', $realName);
                    $expertiseDocumentFile->setField('HashName', $hashName);
                    $expertiseDocumentFile->setField('date_create', date('Y-m-d'));
                    $expertiseDocumentFile->save(true);
                }
                    unset($realName);
                    unset($hashName);
            }
            unset($expertiseDocumentMethodic);

            //Опись документов, представленных для получения свидетельства государственной аккредитации
            $expertiseDocumentMethodic = $expertiseDocument->getByWhere(array(
                'expertise_id' => $expertiseId,
                'document_type_id' => 27,
            ));
            if(empty($expertiseDocumentMethodic)){
                $options = new \Model\ExpertEntities\options();
                //$docPath = $options->getOption('where_docs_path');
                $methodicName = $options->getOption('file_expertise_inventory_list');
                $methodicHash = $options->getOption('file_expertise_inventory_list_hash');

                $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                $expertiseDocumentMethodic->setField('document_number','');
                $expertiseDocumentMethodic->setField('document_type_id',27);
                $expertiseDocumentMethodic->setField('expertise_id',$expertiseId);
                $expertiseDocumentMethodic->setField('open_date',date('Y-m-d'));
                $expertiseDocumentMethodic->setField('year',date('Y'));
                $newId = $expertiseDocumentMethodic->save(true);
                    $hashName = $methodicHash;
                    $realName = $methodicName.'.'.substr(strrchr($hashName, '.'), 1);
                if(!empty($hashName) && !empty($realName)){
                    $expertiseDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                    $expertiseDocumentFile->setField('expertise_document_id', $newId);
                    $expertiseDocumentFile->setField('RealName', $realName);
                    $expertiseDocumentFile->setField('HashName', $hashName);
                    $expertiseDocumentFile->setField('date_create', date('Y-m-d'));
                    $expertiseDocumentFile->save(true);
                }
                    unset($realName);
                    unset($hashName);
            }
            unset($expertiseDocumentMethodic);


            //Инструкция по Личному Кабинету
            $expertiseDocumentMethodic = $expertiseDocument->getByWhere(array(
                'expertise_id' => $expertiseId,
                'document_type_id' => 61,
            ));
            if(empty($expertiseDocumentMethodic)){
                $options = new \Model\ExpertEntities\options();
                //$docPath = $options->getOption('where_docs_path');
                $methodicName = $options->getOption('file_instruction_of_personal_cabinet');
                $methodicHash = $options->getOption('file_instruction_of_personal_cabinet_hash');

                $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                $expertiseDocumentMethodic->setField('document_number','');
                $expertiseDocumentMethodic->setField('document_type_id',61);
                $expertiseDocumentMethodic->setField('expertise_id',$expertiseId);
                $expertiseDocumentMethodic->setField('open_date',date('Y-m-d'));
                $expertiseDocumentMethodic->setField('year',date('Y'));
                $newId = $expertiseDocumentMethodic->save(true);
                    $hashName = $methodicHash;
                    $realName = $methodicName.'.'.substr(strrchr($hashName, '.'), 1);
                if(!empty($hashName) && !empty($realName)){
                    $expertiseDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                    $expertiseDocumentFile->setField('expertise_document_id', $newId);
                    $expertiseDocumentFile->setField('RealName', $realName);
                    $expertiseDocumentFile->setField('HashName', $hashName);
                    $expertiseDocumentFile->setField('date_create', date('Y-m-d'));
                    $expertiseDocumentFile->save(true);
                }
                    unset($realName);
                    unset($hashName);
            }
            unset($expertiseDocumentMethodic);

            //Задание эксперта
            //это клин для временного вывода
            $expertiseDocumentMethodic = $expertiseDocument->getByWhere(array(
                'expertise_id' => $expertiseId,
                'document_type_id' => 37,
            ));
            if(empty($expertiseDocumentMethodic)){
                $options = new \Model\ExpertEntities\options();
                //
                //$methodicName = $options->getOption('file_instruction_of_personal_cabinet');
                //$methodicHash = $options->getOption('file_instruction_of_personal_cabinet_hash');
                $methodicHash = '';
                $methodicName = '';

                $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                $expertiseDocumentMethodic->setField('document_number','');
                $expertiseDocumentMethodic->setField('document_type_id',37);
                $expertiseDocumentMethodic->setField('expertise_id',$expertiseId);
                $expertiseDocumentMethodic->setField('open_date',date('Y-m-d'));
                $expertiseDocumentMethodic->setField('year',date('Y'));
                $newId = $expertiseDocumentMethodic->save(true);
                    $hashName = $methodicHash;
                    $realName = $methodicName.'.'.substr(strrchr($hashName, '.'), 1);
                if(!empty($hashName) && !empty($realName)){
                    $expertiseDocumentFile = new \Model\ExpertEntities\expertisedocumentfile();
                    $expertiseDocumentFile->setField('expertise_document_id', $newId);
                    $expertiseDocumentFile->setField('RealName', $realName);
                    $expertiseDocumentFile->setField('HashName', $hashName);
                    $expertiseDocumentFile->setField('date_create', date('Y-m-d'));
                    $expertiseDocumentFile->save(true);
                }
                    unset($realName);
                    unset($hashName);
            }
            unset($expertiseDocumentMethodic);

            //собираем Ид экспертов
            //$axpertAllId = $expertise->getAllExpertId();
            $expertArrayId = array();
            $application = $expertise->getApplication();
            $eduOrg = $application->getEducationalOrganization();

            $chair = $expertise->getChairman();
            $chairArray = array();
            if(!empty($chair)){
                foreach ( $chair as $value ) {
                    $chairArray[$value->getField('expert_id')] = $value;
                    $expertArrayId[$value->getField('expert_id')] = $value->getField('expert_id');
                }
            }
            $member = $expertise->getCommissionMember();
            $memberArray = array();
            if(!empty($member)){
                foreach ( $member as $value ) {
                    $memberArray[$value->getField('expert_id')] = $value;
                    $expertArrayId[$value->getField('expert_id')] = $value->getField('expert_id');
                }
            }
            if(!empty($expertArrayId)){
                $expertAll = new \Model\ExpertEntities\expert();
                $expertAll = $expertAll->getAllById($expertArrayId);
                //$axpertAll = $expertise->getAllExpert();
            }

            if(!empty($expertAll)){
                foreach ( $expertAll as $expertOne ) {
                    $parentId = $expertOne->getField('parent_id');
                    $expertIt = $expertOne;
                    $expertIdIt = $expertIt->getId();
                    if (!empty($parentId)){
                        if(!empty($parentAlredy[$parentId])){
                            continue;
                        }
                        $expertIdIt = $parentId;
                        $expertIt = new \Model\ExpertEntities\expert($expertIdIt);
                        $parentAlredy[$parentId] = $expertIdIt ;
                    }
                    $expertOneId = $expertOne->getId();
                    $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                    $expertiseDocumentMethodicIt = $expertiseDocumentMethodic->getByWhere(array(
                        'document_type_id'=> 9,
                        'expertise_id' => $expertiseId,
                        'expert_id' => $expertIdIt,
                    ));
                    if(empty($expertiseDocumentMethodicIt)){
                        $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                        $expertiseDocumentMethodic->setField('document_number','');
                        $expertiseDocumentMethodic->setField('document_type_id',9);
                        $expertiseDocumentMethodic->setField('expertise_id',$expertiseId);
                        $expertiseDocumentMethodic->setField('expert_id',$expertIdIt);
                        $expertiseDocumentMethodic->setField('open_date',date('Y-m-d'));
                        $expertiseDocumentMethodic->setField('year',date('Y'));
                        $expertiseDocumentMethodic->save(true);
                        $docNum = $expertiseDocumentMethodic->generateNumber(
                                $application,
                                $expertise,
                                $expertIt,
                                empty($memberArray[$expertOneId])?false:$memberArray[$expertOneId],
                                empty($chairArray[$expertOneId]) ?false:$chairArray[$expertOneId],
                                $eduOrg
                        );
                        $expertiseDocumentMethodic->setField('document_number',$docNum );
                        $expertiseDocumentMethodic->save(false);
                    }

                    $expertOneId = $expertOne->getId();
                    $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                    $expertiseDocumentMethodicIt = $expertiseDocumentMethodic->getByWhere(array(
                        'document_type_id'=> 10,
                        'expertise_id' => $expertiseId,
                        'expert_id' => $expertIdIt,
                    ));
                    if(empty($expertiseDocumentMethodicIt)){
                        $expertiseDocumentMethodic = new \Model\ExpertEntities\expertisedocument();
                        $expertiseDocumentMethodic->setField('document_number','');
                        $expertiseDocumentMethodic->setField('document_type_id',10);
                        $expertiseDocumentMethodic->setField('expertise_id',$expertiseId);
                        $expertiseDocumentMethodic->setField('expert_id',$expertIdIt);
                        $expertiseDocumentMethodic->setField('open_date',date('Y-m-d'));
                        $expertiseDocumentMethodic->setField('year',date('Y'));
                        $expertiseDocumentMethodic->save(true);
                        if(empty($docNum)){
                            $docNum = $expertiseDocumentMethodic->generateNumber(
                                    $application,
                                    $expertise,
                                    $expertIt,
                                    empty($memberArray[$expertOneId])?false:$memberArray[$expertOneId],
                                    empty($chairArray[$expertOneId]) ?false:$chairArray[$expertOneId],
                                    $eduOrg
                            );
                        }
                        $expertiseDocumentMethodic->setField('document_number',$docNum );
                        $expertiseDocumentMethodic->save(false);
                    }
                }
            }
            echo 'Ok';
            die;
    }


    /**
     * сохранение cтоимостей диалога контракта с экспертом
     */
    public function getExpertContractSavePaymentAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $post = $this->params()->fromPost();
            $expDocPaymentClass = new \Model\ExpertEntities\expertisedocumentpayment();
            $expDocPayment = false;
            $isNew = false;
            if(!empty($post['expertise_document_id'])){
               $expDocPayment = $expDocPaymentClass->getById($post['expertise_document_id'],'expertise_document_id');
            }
           if(empty($expDocPayment)){
                $expDocPayment = $expDocPaymentClass;
                $isNew = true;
            }
            $expDocPayment->exchangeArray($post);

                $expDocPayment->save($isNew);

            echo 'Ok';
            die;
        }
        echo('только аякс запрос');
        die;
    }


    /**
     * Диалог прикрепления  файла к документа
     *
     * @return ViewModel
     */
    public function getExpertiseDocumentUploadDialogAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()){
            $docId  = $this->params('id');
            $expertiseId = $this->params('id2');
            $expertId = $this->params('id3');
            if(empty($docId) && empty($expertiseId) && empty($expertId)){
                echo ('Не указан номер документа');
                die;
            }elseif(empty($docId) && !empty($expertiseId) && !empty($expertId)){
                $docId = 0;
            }elseif(!empty($docId) && empty($expertiseId) && empty($expertId)){

            }else{
                echo ('Не номера экспертизы и эксперта');
                die;
            }

            //нужен эксперт

            $expDocument = new \Model\ExpertEntities\expertisedocument();
            if(!empty($docId)){
                $expDocument = $expDocument->getById($docId);
            }else{
                //$expDocument->setField('document_type_id',);
                $expDocument->setField('expertise_id',$expertiseId);
                $expDocument->setField('expert_id',$expertId);
            }

            if(empty($expDocument)){
                echo ('Нет такого документа');
                die;
            }


            //$file = $expertiseDocument->getDocumentFile();

            $view = new ViewModel();
            $view->setVariables(array(
                    'expertiseDocument'=>$expDocument,
                ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    /**
     * вкладка Состав группы
     * формируем диалог выбора экксперта из экспертной организации
     *
     * @return ViewModel
     */
    public function getCommissionFindExpertDialogAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()){
            $post = $this->params()->fromPost();
            $expertiseId = $post['expertiseId'];
            $expertId = $post['expertId'];
            $branchId = $post['branchId'];
            $programId = $post['programId'];
            $parentId = $post['parentId'];
            if(empty($expertiseId)){
                echo ('Не указан номер документа');
                die;
            }
            $expert = new \Model\ExpertEntities\expert();
            $experts = $expert->getByParent($parentId);
            $experts = $experts->getObjectsArray();
            //костыль для если редактить
            //$parentId = ($post['parentId']==$post['expertId'])?$post['parentId']:$post['expertId'];
            $view = new ViewModel();
            $view->setVariables(array(
                    'expertiseId' => $expertiseId,
                    'expertId' => $expertId,
                    'programId' => $programId,
                    'experts' => $experts,
                    'branchId' => $branchId,
                    'parentId' => $parentId,
                ));
            $view->setTerminal(true);
            return $view;
        }
        die;

    }

    /**
     * заменяем эксперат как организации на конкретного экспкерта
     */
    public function saveExpertiseConvertOrgExpertAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()){
            $post = $this->params()->fromPost();
            $isChairMan = false;
            if(!empty($post['isChairman'])){
                $isChairMan = true;
                unset($post['isChairman']);
            }
            if(!empty($post['expertise_program_id'])){
                $commision = new \Model\ExpertEntities\commissionmember();
                $commision = $commision->getByWhere(array(
                    'expertise_id' => $post['expertise_id'],
                    'expert_id' => $post['expert_old_id'],
                    'institution_id' => $post['institution_id'],
                    'expertise_program_id' => $post['expertise_program_id'],
                ));
                if(empty($commision)){
                    echo 'Не найден член соммисии';
                    die;
                }
                unset($post['expert_old_id']);
                $commision->exchangeArray($post);
                $commision->save(false);
            }
            if($isChairMan){
                $chairman = new \Model\ExpertEntities\chairman();
                $chairmanOld =    $chairman->getByWhere(array(
                    'expertise_id'=>$post['expertise_id'],
                    //'expert_id' => $post['parent_id'],
                    'institution_id' => $post['institution_id'],
                ));
                $isNew = true;
                if(!empty($chairmanOld)){
                    $isNew = false;
                    $chairman =  $chairmanOld;
                }
                unset($chairmanOld);
                $userId = $user = $this->getServiceLocator()->get('AuthService')->getIdentity()->getId();
                $chairman->exchangeArray(array(
                    'expertise_id'=>$post['expertise_id'],
                    'expert_id' => $post['expert_id'],
                    'institution_id' => $post['institution_id'],
                    'user_id'=> $userId,
                ));
                $chairman->save($isNew);
            }
            echo 'Ok';
            die;
        }
        die;

    }

    public function getExpertiseDocumentChangeStaticDialogAction() {
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()){
            $expertiseId = $this->params('id2');
            $docTypeId = $this->params('id');

            $docType = new \Model\ExpertEntities\documenttype();
            $docType = $docType->getById($docTypeId);
            if(empty($docType)){
                echo 'Нет такого документа';
            }


            $view = new ViewModel();
            $view->setVariables(array(
                    'expertiseId' => $expertiseId,
                    'docType' =>$docType,
                ));
            $view->setTerminal(true);
            return $view;
        }
        die;
    }

    public function saveDocumentChangeStaticAction() {
        $user = $this->getServiceLocator()->get('AuthService')->getIdentity();
        if(empty($user)){
            echo 'нет пользователя' ;
            die;
        }

        $post = $this->params()->fromPost();
        if(empty($post['action'])){
            echo 'нет действия' ;
            die;
        }
        $type = $post['action'];
        if(!in_array($type, array('change','new'))){
            echo 'не определено действие' ;
            die;
        }

        $docTypeId = $post['docTypeId'];
        if(empty($docTypeId)||!is_numeric($docTypeId)
                || !in_array($docTypeId, array( 57,11,27,61,37))){
            echo 'кривой тип документа' ;
            die;
        }

        if(empty($_FILES)){
            echo 'нет файла' ;
            die;
        }
        $file = $_FILES;
        if($file['error'] != 0){
            echo 'Ошибка файла' ;
            die;
        }
        $ext = end(explode('.', $file['name'])) ;
        $name = $file['name'];
        if ($one_file['size'] > 10 * 1024 * 1024) {
            echo 'Длинный файл' ;
            die;
        }
        $name_info_file = $one_file['name'];

        $short_path = '../doc/'.$name;
        $path = $_SERVER['DOCUMENT_ROOT'].'/public/uploads/'.$short_path;
        //$pathArray = explode('/',$path);
        //$pathDirArray = array_pop($pathArray);
        //$pathDir = implode('/',$pathArray);
        //mkdir($pathDir,0777,true );
        move_uploaded_file($one_file['tmp_name'], $path );



    }
    
    
}

