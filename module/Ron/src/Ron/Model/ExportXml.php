<?php

namespace Ron\Model;

class ExportXml
{

    protected $xmlContent = '';
    protected $applicationsClass;
    protected $currentApplication;
    protected $monthArray;
    protected $resultArray = array();
    protected $arrayOrg;
    protected $MainOrg;

    function __construct($id)
    {
        $this->applicationsClass = new \Model\Gateway\EntitiesTable('isga_Applications');
        $this->currentApplication = $this->applicationsClass->getEntityById($id);
        $this->monthArray = array("января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
        /*
        $this->resultArray['templType'] = $this
          ->currentApplication
          ->getLinkedItem('isga_ApplicationTypes',
                          array(
              'Id' => $this
              ->currentApplication
              ->getField("fk_isgaApplicationType")
            )
          )
          ->getField('Code'); //тип шаблона
        */
        $this->MainOrg= $this->currentApplication->getLinkedItem('eiis_EducationalOrganizations',
            array('Id' => $this->currentApplication->getField("fk_eiisEducationalOrganization")));
        $this->arrayOrg=$this->MainOrg->toArray();

    }

    //фабрика
    public static function getXml($id)
    {
        $applicationsClass = new \Model\Gateway\EntitiesTable('isga_Applications');
        $currentApplication = $applicationsClass->getEntityById($id);

        $resultArray['templType'] = $currentApplication->getLinkedItem(
            'isga_ApplicationTypes', array('Id' => $currentApplication->getField("fk_isgaApplicationType"))
          )
          ->getField('Code'); //тип шаблона

        switch ($resultArray['templType']) {
            case 'DublicateCertificate':
                $array = new ExportXmlDublicateCertificate($id);
                return $array;
                break;
            case 'Certificate':
                $array = new ExportXmlCertificate($id);
                return $array;
                break;
            case 'Accreditation':
                $array = new ExportXmlAccreditation($id);
                return $array;
                break;
            case 'TempCertificate':
                $array = new ExportXmlTempCertificate($id);
                return $array;
                break;
            default :
                return false;
                break;
        }
    }

    /*
     *  Формирователь XML доккуменат
     */
    public function getXmlContent()
    {
        if (empty($this->xmlContent)) {
            $memory = xmlwriter_open_memory();
            xmlwriter_start_document($memory, '1.0', 'UTF-8');
                xmlwriter_start_element($memory, 'smev:Data');
                    // xmlwriter_start_attribute($memory);
                    xmlwriter_write_attribute($memory, 'xmlns:smev', 'http://smev.gosuslugi.ru/rev111111');
                    // xmlwriter_end_attribute($memory);
                    xmlwriter_start_element($memory, 'smev:FormData');
                        //xmlwriter_write_element($memory, 'application_type', $this->getCodeUslugi());
                        $this->getSubElement($memory, $this->getFormData());
                    xmlwriter_end_element($memory);
                    $this->getSubElement($memory, $this->addAttachment());
                xmlwriter_end_element($memory);
            xmlwriter_end_element($memory);
            $this->xmlContent = xmlwriter_output_memory($memory, true);
        }
        return $this->xmlContent;
    }

    public function output($file_name = 'xml.xml')
    {
        if ($this->xmlContent != '') {
            header('Content-Type:  text/xml');
            header('Content-Disposition: attachment; filename="' . $file_name . '"');
            //header('Content-Transfer-Encoding: binary');
            $resultLen = strlen($this->xmlContent);
            header('Content-Length: ' . $resultLen);
            header('Cache-Control: no-store, no-cache, must-revalidate');
            echo $this->getXmlContent();
        }
    }

    public function getInstitution($resorse, $data)
    {
        $resorse = $this->getSubElement($resorse, $data);
        return $resorse;
    }

    function arrayDecore()
    {
        $array = array();
        return $array;
    }

    /*Построение дерева xml документа из массива
     * @resors  рессурс на xml документ
     * @data    массив с данными
     * @nameParent  имя родительскоро элемента (внутренее построение)
     */
    public function getSubElement($resors, $data, $nameParent = '')
    {
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $key = $nameParent;
            }
            if (is_array($value)) {
                if (!isset($value[0])) {
                    xmlwriter_start_element($resors, $key);
                    if (isset($value['_atribute'])) {
                        $resors = $this->getSubAtribute($resors, $value['_atribute']);
                        unset($value['_atribute']);
                    }
                }
                $this->getSubElement($resors, $value, $key);
                if (!isset($value[0])) {
                    xmlwriter_end_element($resors);
                }
            } else {
                xmlwriter_write_element($resors, $key, $value);
            }
        }
        return $resors;
    }

    /*Дабовление атрибутов к XML узлу
     *
     * @resors  рессурс на xml документ
     * @data    массив с данными
     */
    public function getSubAtribute($resors, $data)
    {
        if (count($data) > 0) {
            //xmlwriter_start_attribute($resors);
            foreach ($data as $key => $value) {
                xmlwriter_write_attribute($resors, $key, $value);
            }
            //xmlwriter_end_attribute($resors);
        }
        return $resors;
    }

    /*Возращает название страны по ид
     */
    public function getCountry($id)
    {
        if (isset($id) && isset($id['fk_eiisCountry'])) {
            $country = $this->currentApplication->getLinkedItem(
                'eiis_Countries', array('Id' => $id['fk_eiisCountry'])
              )->toArray();
            $country = $country['Name'];
        } else {
            $country = '';
        }
        return $country;
    }

    /*    Получение региона учреждения
     *
     */
    public function getRegion($id)
    {
        if (isset($id['fk_eiisRegion'])) {
            $regoion = $this->currentApplication->getLinkedItem(
                'eiis_Regions', array('Id' => $id['fk_eiisRegion'])
              )->toArray();
            if (count($regoion) > 0) {
                $regoion = $regoion['Name'];
            }
        } else {
            $regoion = '';
        }
        return $regoion;
    }

    /*
     *  Разбор проциля образовательного учреждения
     */
    public function getEducationalOrganuzationProfiles($array)
    {
        if (isset($array['fk_eiisEducationalOrganizationProperties'])) {
            $data = $this->currentApplication->getLinkedItem(
                'eiis_EducationalOrganizationProperties',
                array('Id' => $array['fk_eiisEducationalOrganizationProperties'])
              )->getField('Name');
        } else {
            $data = '';
        }
        return $data;
    }

    /*
     *  Разбор сведения о типе образовательного учреждения
     */
    public function getEducationalOrganuzationType($array)
    {
        if (isset($array['fk_eiisEducationalOrganizationType'])) {
            $data = $this->currentApplication->getLinkedItem(
                'eiis_EducationalOrganizationTypes', array('Id' => $array['fk_eiisEducationalOrganizationType'])
              )->getField('Name');
        } else {
            $data = '';
        }
        return $data;
    }

    public function getEducationalOrganuzationKind($array)
    {
        if (isset($array['fk_eiisEducationalOrganizationKind'])) {

            $data = $this->currentApplication->getLinkedItem(
                'eiis_EducationalOrganizationKinds', array('Id' => $array['fk_eiisEducationalOrganizationKind'])
              )->getField('Name');
        } else {
            $data = '';
        }
        return $data;
    }

    protected function getFIOContactManager($arrayOrg)
    {
        return $arrayOrg['ContactSecondName'] . ' ' . $arrayOrg['ContactFirstName'] . ' ' . $arrayOrg['ContactLastName'];
    }

    /* Получение сведений об учредителях в выходной массив
     *
     */
    public function getUchrArray($arrayOrg,$UchrArray = array())
    {
        $foundersArray = $this->currentApplication->selectComplicated(
            'isga_mtm_Founders_EducationalOrganizations', array('fk_isgaFounder'),
            array('fk_eiisEducationalOrganization' => $arrayOrg['Id'])
          )->toArray();
        if (count($foundersArray) > 0) {
            $foundersArray = array_column($foundersArray, 'fk_isgaFounder');
            $foundersArray = $this->currentApplication->selectComplicated(
                'isga_Founders', array('Id', 'LastName', 'FirstName', 'Patronymic', 'PAddress'),
                array('Id' => $foundersArray)
              )->toArray();
        }
        if (count($foundersArray) > 0) {
            $UchrArray['Founders']['Founder'] = array();
            foreach ($foundersArray as $key => $value) {
                $typeUchr = 1;
                if (empty($value['OrganizationFullName'])) {
                    $value['OrganizationFullName'] = ''
                      . $value['LastName'] . ' '
                      . $value['FirstName'] . ' '
                      . $value['Patronymic'];
                    $typeUchr = 2;
                }
                $UchrArray['Founders']['Founder'][] = array(
                    'UCHR_TYPE'  => $typeUchr,
                    'UCHR_NAME'  => $value['OrganizationFullName'],
                    'UCHR_ADRES' => $value['PAddress'],
                );
                //unset ($typeUchr);
            }
        }
        return $UchrArray;
    }

    /* Добавление сведени об уровныях оброзовательных программ
     *
     */
    public function addEduLevel($programs, $programsTypes)
    {
        $eduLevel = array();
        $arrayLevel = array_column($programsTypes, 'fk_eiisEduLevels');
        if ($arrayLevel) {
            $arrayLevel = array_unique(array_filter($arrayLevel));
            if ($arrayLevel) {
                $eduLevel = $this->currentApplication->selectComplicated("eiis_EduLevels",
                                                                         array('Id', /* 'Name', 'ShortName', */ 'EduProgramKind'),
                                                                         array('Id' => $arrayLevel)
                  )->toArray();
                $eduLevel = array_column($eduLevel, 'EduProgramKind', 'Id');
                $arrayLevel = array_column($programsTypes, 'fk_eiisEduLevels', 'Id');
            }
        }

        foreach ($programs as $key => $value) {
            if (isset($value['fk_eiisEduProgramType']) && isset($arrayLevel[$value['fk_eiisEduProgramType']])
                && isset($eduLevel[$arrayLevel[$value['fk_eiisEduProgramType']]])
            ) {
                $oneEduLevel = $eduLevel[$arrayLevel[$value['fk_eiisEduProgramType']]];
            } else {
                $oneEduLevel = 'default';
            }
            $programs[$key]['EduLevel'] = $oneEduLevel;
        }
        return $programs;
    }

    /*      Добавление сведений о квалификации образовательныхд программ
     *
     */
    public function addQualification($programs)
    {
        $programs=  array_filter($programs);
        $arrayEduKvalif = array_column($programs, 'fk_isgaQualification');
        $arrayEduKvalifRez = $this->currentApplication->selectComplicated("isga_Qualifications",
                                                                          array('Id', 'Name', 'Code'),
                                                                          array('Id' => $arrayEduKvalif))->toArray();
        $arrayEduKvalifName = array_column($arrayEduKvalifRez, 'Name', 'Id');
        $arrayEduKvalifCode = array_column($arrayEduKvalifRez, 'Code', 'Id');

        foreach ($programs as $key => $value) {
            $programs [$key]['QualificationName'] = $arrayEduKvalifName[$value['fk_isgaQualification']];
            $programs [$key]['QualificationCode'] = $arrayEduKvalifCode[$value['fk_isgaQualification']];
        }
        return $programs;
    }

    /*  Дабавеление сведений о коде и назании основной образовательной программы программ.
     *
     */
    public function addGs($programs, $eduPrograms)
    {
        $arrayEduPrograms = array_column($eduPrograms, 'fk_isgaGS', 'Id');
        $isgaGSIds = array_column($eduPrograms, 'fk_isgaGS');
        $isgaGSArray = $this->currentApplication
            ->selectComplicated("isga_GroupSpecialities", array('Id', 'Name', 'Code', 'fk_isgaEGS'),
                                array('Id' => $isgaGSIds))->toArray();
        $isgaGSIdEGS = array_column($isgaGSArray, 'fk_isgaEGS', 'Id');
        $isgaGSArrayName = array_column($isgaGSArray, 'Name', 'Id');
        $isgaGSArrayCode = array_column($isgaGSArray, 'Code', 'Id');
        $isgaEGSIds = array_column($isgaGSArray, 'fk_isgaEGS');
        $isgaEGSArray = $this->currentApplication
            ->selectComplicated("isga_EnlargedGroupSpecialities", array('Id', 'Name', 'Code'),
                                array('Id' => $isgaEGSIds))->toArray();
        $isgaEGSArrayName = array_column($isgaEGSArray, 'Name', 'Id');
        $isgaEGSArrayCode = array_column($isgaEGSArray, 'Code', 'Id');
        foreach ($programs as $key => $value) {
            $programs[$key]['GsCode'] = $isgaEGSArrayCode[$isgaGSIdEGS[$arrayEduPrograms[$value['fk_eiisEduProgram']]]];
            $programs[$key]['GsName'] = $isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$value['fk_eiisEduProgram']]]];
        }
        return $programs;
    }

    /*
     *  Добваление сведений о прикрепленных файлах
     */
    public function addAttachment($resultArray = array())
    {
        $atachmentFiles = $this->currentApplication
          ->selectComplicated('isga_ApplicationFiles',
                              array('Id', 'fk_isgaApplication', 'fk_isgaDocumentType', 'RealName', 'HashName'),
                              array('fk_isgaApplication' => $this->currentApplication->getField("Id")));
        if ($atachmentFiles) {
            $atachmentFiles = $atachmentFiles->toArray();
            // $atachmentFiles;
            if (count($atachmentFiles) > 0) {
                $docType = array_column($atachmentFiles, 'fk_isgaDocumentType');
                $docType = $this->currentApplication
                  ->selectComplicated('isga_DocumentTypes', array('Id', 'Name'), array('Id' => $docType))
                  ->toArray();
                $docType = array_column($docType, 'Name', 'Id');
                $resultArray['smev:AppliedDocuments']['smev:AppliedDocument'] = array();
                foreach ($atachmentFiles as $file) {
                    $resultArray['smev:AppliedDocuments']['smev:AppliedDocument'][] = array(
                        'smev:Name'         => $file['RealName'],
                        'smev:CodeDocument' => $file['HashName'],
                        'smev:Type'         => $docType[$file['fk_isgaDocumentType']],
                        'smev:URL'          => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/'.$file['HashName'],
                    );
                }
            }
        }
        return $resultArray;
    }

    /*
     *  Добавление сведений о сертификатах образовательных программ
     */
    public function addCertificate($resultArray = array(), $id = null)
    {
        if ($id === null) {
            $id = $this->currentApplication->getField("fk_isgaPreviousCertificate");
        }
        $certificate = $this->currentApplication
          ->getLinkedItem(
            'isga_Certificates', array('Id' => $id)
          )
          ->toArray();
        $controlOrgan = $this->currentApplication
          ->getLinkedItem(
            'eiis_ControlOrgans', array('Id' => $certificate['fk_eiisControlOrgan'])
          )
          ->toArray();
        $dateIssue = strtotime($certificate['DateIssue']);
        $dateIssue = date('d.m.Y', $dateIssue);
        $dateEnd = strtotime($certificate['DateEnd']);
        $dateEnd = date('d.m.Y', $dateEnd);
        $resultArray['CertificateData'] = array(
            'RegNumber'    => $certificate['RegNumber'],
            'DateIssue'    => $dateIssue,
            'DateEnd'      => $dateEnd,
            'SerialNumber' => $certificate['SerialNumber'],
            'FormNumber'   => $certificate['FormNumber'],
            'Name'         => $controlOrgan['Name'],
        );
        return $resultArray;
    }

    /*
     * Формирование массива данных данных филиалов
     */
    public function addFilialeData($arrayOrg, $resultArray = array())
    {
        $resultArray['FullName'] = $arrayOrg['FullName'];
        $resultArray['ShortName'] = $arrayOrg['ShortName'];
        $resultArray['OrgForm'] = $this->getEducationalOrganuzationProfiles($arrayOrg);
        $resultArray['ScholType'] = $this->getEducationalOrganuzationType($arrayOrg);
        $resultArray['ScholKind'] = $this->getEducationalOrganuzationKind($arrayOrg);
        $resultArray['Requisites'] = array(
            'Kpp' => $arrayOrg['Kpp'],
        );
        $resultArray['UrAddress'] = array(
            'Country'   => $this->getCountry($arrayOrg),
            'Region'    => $this->getRegion($arrayOrg),
            'PostIndex' => $arrayOrg['PostIndex'],
            'District'  => '',
            'TownName'  => $arrayOrg['TownName'],
            'Address'   => $arrayOrg['Address'],
            'Phones'    => $arrayOrg['Phones'],
            'Faxes'     => $arrayOrg['Faxes'],
            'Mails'     => $arrayOrg['Mails'],
        );
        return $resultArray;
    }

    /*
     *  Формирование массива данных главного вуза
     */
    public function addFilialeMainData($arrayOrg, $resultArray = array())
    {
        $resultArray = $this->addFilialeData($arrayOrg, $resultArray);
        $resultArray['ApplicantPerson'] = array(
            'Fio'            => $this->getFIOContactManager($arrayOrg),
            'ChargePosition' => $arrayOrg['ChargePosition'],
        );
        $resultArray['Requisites'] = array(
            'GosRegNum' => $arrayOrg['GosRegNum'],
            'Inn'       => $arrayOrg['Inn'],
        );
        $resultArray['InstitutionDir'] = array(
            'Fio' => $this->getFIOContactManager($arrayOrg),
            'ChargePosition' => $arrayOrg['ChargePosition'],
        );
        return $resultArray;
    }

    /*
     *  Добавление данных об общественных аккредитациях
     */
    public function addAccreditationOf($arrayOrg, $resultArray = array())
    {
        $organizationData = $this->currentApplication
          ->getLinkedItem('isga_EducationalOrganizationData', array('fk_eiisEducationalOrganization' => $arrayOrg['Id'])          )
          ->toArray();
        if ($organizationData['PubAccr']) {
            $pubAccr = $organizationData['PubAccr'];
            $pubAccr = json_decode($pubAccr, true);
            $pubAccr = json_decode($pubAccr, true);
            $err = json_last_error();
            if (!$err && count($pubAccr) > 0) {
                $resultArray['SocAkkrs']['SocAkkrData'] = array();
                foreach ($pubAccr as $one) {
                    $two = $one;
                    $resultArray['SocAkkrs']['SocAkkrData'][] = array(
                        'NameSocAkkr' => $one['organization'],
                        'DateSocAkkr' => $one['date'],
                    );
                }
            }
        }
        return $resultArray;
    }

    /*
     *  Разбор сведений образавательных программ по именам полей
     */
    public function addProgramFieldOfName($applicationPrograms, $name, $programs = array(), $alter_key = null)
    {
        $startYear = array_column($applicationPrograms, $name, 'fk_eiisLicensedPrograms');
         if (!empty($alter_key)) {
            $name = $alter_key;
        }
        foreach ($programs as $key => $value) {
            $programs[$key][$name] = $startYear[$value['Id']];
        }
        return $programs;
    }

    /*
     *  Разбор и кодировка информации образовательной программы
     */
    public function writeOneOP($oneProgram)
    {
        return array(
            'Id'                  => $oneProgram['Id'],
            'SystemId'            => $oneProgram['SystemId'],
            'GsCode'              => $oneProgram['GsCode'],
            'GsName'              => $oneProgram['GsName'],
            'EduLevel'            => $oneProgram['EduLevel'],
            'Code'                => $oneProgram['Code'],
            'Name'                => $oneProgram['Name'],
            'QualificationCode'   => $oneProgram['QualificationCode'],
            'QualificationName'   => $oneProgram['QualificationName'],
            'yearStartEdu'        => $oneProgram['yearStartEdu'],
            'termEdu'             => $oneProgram['termEdu'],
            'fullTimeCount'       => $oneProgram['fullTimeCount'],
            'correspondenceCount' => $oneProgram['correspondenceCount'],
            'partTimeCount'       => $oneProgram['partTimeCount'],
            'familyCount'         => $oneProgram['familyCount'],
            'networkForm'         => $this->captionYesNo($oneProgram['networkForm']),
            'remoteTechnology'    => $this->captionYesNo($oneProgram['remoteTechnology']),
            'publicAccreditation' => $this->captionYesNo($oneProgram['publicAccreditation']),
        );
    }

    /*
     * Добавление сведений о гос тайне и Информировать ли о ходе регистрации
     */
    public function addMoreInfo($resultArray=array())
    {
        $moreInfo = $this->currentApplication->getLinkedItem(
            'isga_ApplicationMoreInfo', array('fk_isgaApplication' => $this->currentApplication->getField("Id"))
          )->toArray();
        if (count($moreInfo) > 0) {
            $resultArray['GosTayna']['RegNumber'] = $moreInfo['RegNumber'];
            $resultArray['GosTayna']['SiriesNumber'] = $moreInfo['SiriesNumber'];
            $resultArray['GosTayna']['DateReg'] = $moreInfo['DateReg'];
            $resultArray['GosTayna']['DateEnd'] = $moreInfo['DateEnd'];
            $information = $moreInfo['Information'];
            if ($information) {
                $resultArray['InformationMe'] = array(
                    'InformationMe'  => 'Да',
                    'Persona'        => $moreInfo['Persona'],
                    'Number'         => $moreInfo['Number'],
                    'Email'          => $moreInfo['Email'],
                );
            } else {
                $resultArray['informationMe']['InformationMe'] = 'Нет';
            }
        }
        return $resultArray;
    }

    /*
     * Формирование данных о филиалах
     *
     *  @arrayRef - главный вуз
     */
    public function addBranchData($arrayRef, $resultArrayOut = array(),$getProgram=true,$allInCertificate=false)
    {
        $currentApplication = $this->currentApplication;
        if($allInCertificate){
            $branchesApplication        = $currentApplication
              ->selectComplicated("isga_CertificateSupplements",
                array('Id', 'fk_eiisEducationalOrganization'),
                array('fk_isgaCertificate' => $currentApplication->getField("fk_isgaPreviousCertificate")))
              ->toArray();
            foreach ($branchesApplication as $key => $oneBranch) {
                //Странно но 'fk_eiisEducationalOrganization' бывают null
                if (!empty($branchesApplication[$key]['fk_eiisEducationalOrganization'])) {
                    $branchesApplication[$key]['fk_eiisEOBranch'] = $oneBranch['fk_eiisEducationalOrganization'];
                } else {
                    unset($branchesApplication[$key]);
                }
            }
        }
        else{
        $branchesApplication = $currentApplication->selectComplicated("isga_mtm_Application_Branches",
            array('fk_eiisEOBranch'),
            array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        }
        $educationOrganization = new \Model\Entities\eiisEducationalOrganizations();
        foreach ($branchesApplication as $oneBranch) {
            $resultArray = array();
            // пропускаем главный вуз, чтобы
            if ($oneBranch['fk_eiisEOBranch'] === $arrayRef['Id']) {
                continue;
            }
            $MainOrg = $currentApplication->getLinkedItem(
              'eiis_EducationalOrganizations', array('Id' => $oneBranch['fk_eiisEOBranch'])
            );
            $arrayOrg = $MainOrg->toArray();
            $educationOrganization->setField('Id', $arrayOrg['Id']);
            $educationalOrganizationDataRes = $educationOrganization->getEducationalOrganizationData();
            $educationalOrganizationData = ($educationalOrganizationDataRes !== false) ? $educationalOrganizationDataRes->toArray() : array();
            $resultArray['code'] = $arrayOrg['Id'];
            $resultArray['code_ref']  = $arrayRef['Id'];
            $resultArray['applicant'] = 'false';
            //добавляем сведения о филиале
            $resultArray = $this->addFilialeData($arrayOrg, $resultArray);
            if ($getProgram){
            $applicationPrograms = $currentApplication->selectComplicated("isga_ApplicationPrograms",
                array('Id', 'fk_eiisLicensedPrograms', 'yearStartEdu', 'termEdu', 'fullTimeCount', 'partTimeCount', 'correspondenceCount', 'familyCount', 'networkForm', 'remoteTechnology', 'publicAccreditation'),
                array('fk_isgaApplication' => $currentApplication->getField('Id'), 'fk_isgaEOBranches' => $oneBranch['fk_eiisEOBranch']));
            $applicationPrograms = $applicationPrograms->toArray();
            }else {
                $applicationPrograms=array();
            }
            if (count($applicationPrograms) > 0) {
                $newArrApplicationPrograms = array();
                foreach ($applicationPrograms as $oneAplicationProgram) {
                    $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
                }
                $programsIds = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
                $programs = $currentApplication->selectComplicated("eiis_LicensedPrograms",
                    array('Id', 'Code', 'Name', 'fk_eiisEduProgram', 'fk_eiisEduProgramType', 'fk_isgaQualification'),
                    array('Id' => $programsIds))->toArray();
                $resultArray=$this->addEductionPrograms($programs,$applicationPrograms, $resultArray);
            }
            $resultArrayOut['institutions']['institution'][] = $resultArray;
        }
        return $resultArrayOut;
    }

    public function addEductionPrograms($programs,$applicationPrograms, $resultArray=array())
    {
        $programs=array_filter($programs);
        $applicationPrograms=array_filter($applicationPrograms);
        $currentApplication = $this->currentApplication;
        $programTypeIds = array_column($programs, 'fk_eiisEduProgramType');
        $eduProgramIds = array_column($programs, 'fk_eiisEduProgram');
        $programsTypes = $currentApplication->selectComplicated("eiis_EduProgramTypes",
                                                                array('Id', 'Name', 'fk_eiisEduLevels'),
                                                                array('Id' => $programTypeIds))->toArray();
        $arrayTypes = array_column($programsTypes, 'Name', 'Id');
        $eduPrograms = $currentApplication->selectComplicated("eiis_EduPrograms", array('Id', 'fk_isgaGS'),
                                                              array('Id' => $eduProgramIds))->toArray();
        //$arrayEduPrograms = array_column($applicationPrograms, 'fk_eiisLicensedPrograms', 'Id');
        //данные об образовательной программе из формы
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'Id', $programs, 'SystemId');
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'yearStartEdu', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'fullTimeCount', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'partTimeCount', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'correspondenceCount', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'familyCount', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'termEdu', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'networkForm', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'remoteTechnology', $programs);
        $programs = $this->addProgramFieldOfName($applicationPrograms, 'publicAccreditation', $programs);
        //добавляем коды Gs
        $programs = $this->addGs($programs, $eduPrograms);
        //добавляем уровни образования
        $programs = $this->addEduLevel($programs, $programsTypes);
        //уровни квалиффикации
        $programs = $this->addQualification($programs);
        //учебные программы
        foreach ($programs as $oneProgram) {
            $resultArray['OPs']['OP'][] = $this->writeOneOP($oneProgram);
        }
        return $resultArray;
    }

    public function  addMainData($param)
    {
    }

    /*
     * Образовательные программы главного вуза
     */
    public function addMainEductionsProgram($resultArray=array())
    {
        $applicationsClass = $this->applicationsClass;
        $currentApplication = $this->currentApplication;
        $monthArray = $this->monthArray;
        $MainOrg = $this->MainOrg;
        $arrayOrg = $this->arrayOrg;
        $applicationPrograms = $currentApplication
            ->selectComplicated(
              "isga_ApplicationPrograms",
              array(
                'Id',
                'fk_eiisLicensedPrograms',
                'yearStartEdu',
                'termEdu',
                'fullTimeCount',
                'partTimeCount',
                'correspondenceCount',
                'familyCount',
                'networkForm',
                'remoteTechnology',
                'publicAccreditation',
                'yearStartEdu',
              ),
              array(
                'fk_isgaApplication' => $currentApplication
                ->getField('Id'),
                'fk_isgaEOBranches'  => $arrayOrg['Id'])
            )->toArray();
        $newArrApplicationPrograms = array();
        foreach ($applicationPrograms as $oneAplicationProgram) {
            $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
        }
        $programsIds = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
        $programsIds = array_filter($programsIds);
        if (count($programsIds) > 0) {
            $programs = $currentApplication->selectComplicated(
                "eiis_LicensedPrograms",
                array(
                  'Id',
                  'Code',
                  'Name',
                  'fk_eiisEduProgram',
                  'fk_eiisEduProgramType',
                  'Period',
                  'fk_isgaQualification',
                ), array('Id' => $programsIds)
              )->toArray();
            //Добавляем образовательные программы
            $resultArray=$this->addEductionPrograms($programs,$applicationPrograms,$resultArray);
        }
        return $resultArray;
    }

    public function captionYesNo($data)
    {
        return ($data == '1' || $data == 1) ? 'Да' : 'Нет';
    }

    public function getFormData($param=null)
    {
        if (empty($this->resultArray)) {            
            $this->resultArray =  array_merge($this->getMainData(), $this->getData());
        }
        if ($param!==null){
            return isset($this->resultArray[$param])?$this->resultArray[$param]:false;
        }
        return  $this->resultArray;
    }

    public function getData()
    {
        return array();
    }

    public function getMainData()
    {
       $resultArray = array();
       //Системный код
       $resultArray['SystemId'] = $this->currentApplication->getField('Id');
       //тип заявления
        $resultArray['application_type'] = $this->getCodeUslugi();
       //Код типа причины заявления
       $reason = $this->currentApplication->getLinkedItem(
           'isga_ApplicationReasons', array('Id' => $this->currentApplication->getField("fk_isgaApplicationReason"))
         )->toArray();
       if (isset($reason['Code'])) {
           $resultArray ['application_reason'] = $reason['Code'];
       }
       //Код штрих кода
       $resultArray['Barcode'] = $this->currentApplication->getField('Barcode');
       //Дата создания заявления
       $resultArray['DateCreate'] = date('d.m.Y', strtotime($this->currentApplication->getField('DateCreate')));
       //Добавляем сведения о сертиффикатах
       $resultArray = $this->addCertificate($resultArray);
       //добавляем сведения о гос тайне и сообщать о ходе регистрации
       $resultArray = $this->addMoreInfo($resultArray);
       return $resultArray;
    }

}