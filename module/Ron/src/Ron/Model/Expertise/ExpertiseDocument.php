<?php

namespace Ron\Model\Expertise;

/**
 * готовим данные для печати документы экспертизы
 */
class ExpertiseDocument
{
  public static function fabric($expertiseDocumentId) {
      $expertiseDocument = new \Model\ExpertEntities\expertisedocument();
      $expertiseDocument = $expertiseDocument->getById($expertiseDocumentId);
      if (empty($expertiseDocument)){
          return null;
      }
      $documentTypeId = $expertiseDocument->getFieldOrSpace('document_type_id');
      $expert = $expertiseDocument->getExpert();
      $parentId = $expert->getFieldOrSpace('parent_id');     
      $isOrg = $expert->getFieldOrSpace('is_organization');   
      //$documentCode = $expertiseDocumen->getFieldOrSpace('code');
      //Договор
      if ($documentTypeId == 9){
          // Экспeрт в Конторе?
          if (empty($parentId)&&empty($isOrg)){
              //нет
              $result = new \Ron\Model\Expertise\Contract\Person($expertiseDocument);
              return $result;
          }else{
              //Да
              $result = new \Ron\Model\Expertise\Contract\Organization($expertiseDocument);
              return $result;
          }
      }
      //Акт
      if ($documentTypeId == 10){
          // Экспeрт в Конторе?
          if (empty($parentId)&&empty($isOrg)){
              //нет
              $result = new \Ron\Model\Expertise\Act\Person($expertiseDocument);
              return $result;
          }else{
              //Да
              $result = new \Ron\Model\Expertise\Act\Organization($expertiseDocument);
              return $result;
          }
      }      
  } 
}