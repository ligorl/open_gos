<?php

namespace Ron\Model\Expertise\Act;

/**
 * готовим данные для печати акта работы эксперта
 */
class Person
{
    public $expertiseDocument;
    public function __construct($expertiseDocument) {
        //$expertiseDocument = new \Model\ExpertEntities\expertisedocument();
        //$expertiseDocument = $expertiseDocument->getById($expertiseDocumentId);

        if (empty($expertiseDocument)){
            throw new Exception('Не найдено заявление');
        }
        $this->expertiseDocument=$expertiseDocument;
    }
    /*
${fieldContractNumber} номер договора
${fieldContractDate} - Дата договора
${fieldExpertFio}  ФИО эксперта полностью
${fieldEduOrgName}  полное наименование образовательной организации, подававшей заявление
${fieldContractNumberDupl} номер договора
${fieldContractDateDupl} - Дата договора
${fieldContractSumm}  итоговая сумма по договору с экспертом
${fieldExpertFioDupl}  ФИО эксперта полностью
${fieldExpertFioShort}  ФИО эксперта (имя, отчество инициалами)
 */
    public function getData(){
        $result = array();
        $expertiseDocument = $this->expertiseDocument;
        $expert = $expertiseDocument->getExpertIt();
        $expertise = $expertiseDocument->getExpertise();
        $eduOrg = $expertise->getEducationalOrganization();
        $payment = $expertiseDocument->getDocumentPayment();
        $result['fieldContractNumber'] = $expertiseDocument->getField('document_number');
        $result['fieldContractDate'] = $expertiseDocument->getDate('open_date');
        $result['fieldExpertFio'] = $expert->getFio();
        $result['fieldEduOrgName'] = $eduOrg->getField('FullName');
        $result['fieldContractNumberDupl'] = $result['fieldContractNumber'];
        $result['fieldContractDateDupl'] = $result['fieldContractDate'];
        $result['fieldContractSumm'] = empty($payment) ? 0 : $payment->getFieldOrSpace('amount_contract_result');
        $result['fieldExpertFioDupl'] = $result['fieldExpertFio'];
        $result['fieldExpertFioShort'] = $expert->getFioShort();

        return $result;
    }

    public function getTemplateName(){
        return 'expertise/act/person.docx';
    }

    public function getDocumentName() {
        return 'Акт эксперта';
    }
}

