<?php

namespace Ron\Model\Expertise\Act;

/**
 * готовим данные для печати акта экспертной организации
 */
class Organization
{
    public $expertiseDocument;
    public function __construct($expertiseDocument) {
        //$expertiseDocument = new \Model\ExpertEntities\expertisedocument();
        //$expertiseDocument = $expertiseDocument->getById($expertiseDocumentId);

        if (empty($expertiseDocument)){
            throw new Exception('Не найдено заявление');
        }
        $this->expertiseDocument=$expertiseDocument;
    }
    /**
${fieldContractDate}   дата договора в формате «дд» месяц гггг
${fieldContractNumber}     номер договора
${fieldContractDateDupl}   дата договора в формате «дд» месяц гггг
${fieldExpertName}    Наименование организации
${fieldExpertDirectorName}   ФИО директора
${fieldEduOrgName}
${fieldEduOrgAdress}
${fieldContractDateDupl2}
${fieldContractNumberDupl2}
${_tableExpertFio}
    ${tableExpertFio} ФИО эксперта (полностью) из экспертной организации
${fieldContractAwardSumm}  итоговая сумма вознаграждения по договору
${fieldContractAwardSummWord}  итоговая сумма вознаграждения словами (как в платежке в калькуляторе госпошлины)
${fieldContractCompensationSumm}  итоговая сумма возмещения по договору
${fieldContractCompensationSummWord}  итоговая сумма возмещения по договору словами
${fieldContractSumSumm}  итоговая сумма по договору
${fieldContractSumSummWord}  итоговая сумма по договору словами
${fieldContractSumNds}  сумма ндс = ${fieldContractSumSumm}*0,18

 */
    public function getData(){
        $result = array();
        $expertiseDocument = $this->expertiseDocument;
        $expert = $expertiseDocument->getExpertIt();
        $expertIsOrg = $expert->getField('is_organization');

        if(empty($expertIsOrg)){
            $parentId = $expert->getFieldOrSpace('parent_id');
            if (empty($parentId)){
                new \Exception('Не найдена контора');
            }
            $expert = $expert->getById($parentId);
        }

        $expertise = $expertiseDocument->getExpertise();
        $eduOrg = $expertise->getEducationalOrganization();
        $expertiseId = $expertiseDocument-> getField('expertise_id');
        $contoraId = $expert->getId();

        $expertArray= array();
        $expertNameArray = array();
        //проверяем начальников, если он от конторы то добавим
        $chairman = new \Model\ExpertEntities\chairman();
        $chairmanAll = $chairman->getAllByWhere(array(
            'expertise_id' => $expertiseId,
        ));
        //дергаем руководителей
        if(!empty($chairmanAll)){
            foreach ( $chairmanAll as $chairmanOne ) {
                $expertOne = $chairmanOne->getExpert();
                if(empty($expertOne )){
                    continue;
                }
                $expertId = $chairmanOne->getField('expert_id');
                $parentId = $expertOne->getField('parent_id');
                if(!empty($parentId) && $parentId==$contoraId){
                    $expertNameArray[$expertOne->getId()] = array('tableExpertFio'=>$expertOne->getFio());
                }
            }
        }
        //дергаем кто с программой
        $commissioMember = new \Model\ExpertEntities\commissionmember();
        $commissioMemberAll = $commissioMember->getAllByWhere(array(
            'expertise_id' => $expertiseId,
            'parent_id' => $contoraId,
        ));
        if(!empty($commissioMemberAll)){
            foreach ( $commissioMemberAll as $commissioMemberOne ) {
                $expertId = $commissioMemberOne->getField('expert_id');
                if(empty( $expertNameArray[$expertId])){
                    $expertOne = $commissioMemberOne->getExpert();
                    $expertNameArray[$expertOne->getId()] = array('tableExpertFio'=>$expertOne->getFio());
                }
            }
        }
        usort($expertNameArray, function($a, $b){
            $a = $a['tableExpertFio'];
            $b = $b['tableExpertFio'];
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });
        
        //оплаты
        $payment = $expertiseDocument->getDocumentPayment();

        $result['fieldContractNumber'] = $expertiseDocument->getFieldOrSpace('document_number');
        $result['fieldContractDate'] = $expertiseDocument->getDate('open_date');
        $result['fieldContractDateDupl'] = $result['fieldContractDate'];
        $result['fieldExpertName'] =    $expert->getFio();
        $result['fieldExpertDirectorName'] =    $expert->getFieldOrSpace('director');
        $result['fieldEduOrgName'] = $eduOrg->getFieldOrSpace('FullName');
        $result['fieldEduOrgAdress'] = $eduOrg->getFieldOrSpace('LawAddress');
        $result['fieldContractDateDupl2'] = $result['fieldContractDate'];
        $result['fieldContractNumberDupl2'] = $result['fieldContractNumber'];       
        $result['_tableExpertFio'] = $expertNameArray;
        $awardSumm = empty($payment)?0:(int)$payment->getFieldOrSpace('amount_award');
        $result['fieldContractAwardSumm'] = number_format($awardSumm, 2, ',', ' ');
        $result['fieldContractAwardSummWord'] = $this->num2str($awardSumm);
        $compensationSumm = empty($payment)?0:(int)$payment->getFieldOrSpace('amount_compensation_payment');
        $result['fieldContractCompensationSumm'] = number_format($compensationSumm, 2, ',', ' ');
        $result['fieldContractCompensationSummWord'] = $this->num2str($compensationSumm);
        $sumSumm = empty($payment)?0:(int)$payment->getFieldOrSpace('amount_contract_result');
        $result['fieldContractSumSumm'] = number_format($sumSumm, 2, ',', ' ');
        $result['fieldContractSumSummWord'] = $this->num2str($sumSumm);
        $result['fieldContractSumNds'] = number_format($sumSumm*0.18, 2, ',', ' ');

        return $result;
    }

    public function getTemplateName(){
        return 'expertise/act/organisation.docx';
    }

    public function getDocumentName() {
        return 'Акт экспертной организации';
    }

    /**
     * Возвращает сумму прописью
     * @author runcore
     * @uses morph(...)
     */
    function num2str($num)
    {
        $nul = 'ноль';
        $ten = array(
            array('', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            array('', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
        );
        $a20 = array('десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
        $tens = array(2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто');
        $hundred = array('', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот');
        $unit = array(// Units
            array('копейка', 'копейки', 'копеек', 1),
            array('рубль', 'рубля', 'рублей', 0),
            array('тысяча', 'тысячи', 'тысяч', 1),
            array('миллион', 'миллиона', 'миллионов', 0),
            array('миллиард', 'милиарда', 'миллиардов', 0),
        );
        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                } else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                }
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = $this->morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            } //foreach
        } else {
            $out[] = $nul;
        }
        $out[] = $this->morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . $this->morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }

    /**
     * Склоняем словоформу
     * @ author runcore
     */
    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        }
        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }
        if ($n == 1) {
            return $f1;
        }
        return $f5;
    }


}

