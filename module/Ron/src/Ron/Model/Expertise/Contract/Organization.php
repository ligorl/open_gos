<?php

namespace Ron\Model\Expertise\Contract;

/**
 * готовим данные для печати договора с экспертной организацией
 */
class Organization
{
    public $expertiseDocument;
    public function __construct($expertiseDocument) {
        //$expertiseDocument = new \Model\ExpertEntities\expertisedocument();
        //$expertiseDocument = $expertiseDocument->getById($expertiseDocumentId);

        if (empty($expertiseDocument)){
            throw new Exception('Не найдено заявление');
        }
        $this->expertiseDocument=$expertiseDocument;
    }
    /*
${fieldContractNumber}
${fieldContractDate}
${fieldExpertFio}
${fieldExpertDirect}
${fieldEduOrgName}
${fieldEduOrgAdress}
${fieldExpertiseDateBegin}
${fieldExpertiseDateEnd}
${fieldExpertOrganisationCount}
${fieldExpertAddress}
${fieldExpertAddressFact}
${fieldExpertTelefon}
${fieldExpertInn}
${fieldExpertKpp}
${fieldExpertBank}
${fieldExpertBik}
${fieldExpertOkpo}

 */
    public function getData(){
        $result = array();
        $expertiseDocument = $this->expertiseDocument;
        $expert = $expertiseDocument->getExpertIt();
        
        $expertise = $expertiseDocument->getExpertise();
        $eduOrg = $expertise->getEducationalOrganization();
        
        $contoraId = $expert-> getField('parent_id');
        $expertiseId = $expertiseDocument-> getField('expertise_id'); 
        $chairmanExpertCount = 0;
        //$expertParent = new \Model\ExpertEntities\expert($contoraId);
        $expertParent = $expert;
        
        $contact = new \Model\ExpertEntities\contact($expert->getField('contact'));
        //проверяем начальников, если он от конторы то добавим
        $chairman = new \Model\ExpertEntities\chairman();
        $chairmanAll = $chairman->getAllByWhere(array(
            'expertise_id' => $expertiseId,
        )); 
        $chairmanParentId =0;
        if(!empty($chairmanAll)){
            foreach ( $chairmanAll as $chairmanOne ) {
                $expertOne = $chairmanOne->getExpert();
                $parentId = $expertOne->getField('parent_id');
                if(!empty($parentId) && $parentId==$contoraId){
                    $chairmanParentId = $chairmanOne->getField('expert_id');
                    $chairmanExpertCount++;
                    break;
                }
            }
        }
        $commissioMember = new \Model\ExpertEntities\commissionmember();
        $commissioMemberAll = $commissioMember->getAllByWhere(array(
            'expertise_id' => $expertiseId,
            'parent_id' => $contoraId,
        ));
        if(!empty($commissioMemberAll)){
            foreach ( $commissioMemberAll as $commissioMemberOne ) {
                $parentId = $commissioMemberOne->getField('parent_id');
                $expertId = $commissioMemberOne->getField('expert_id');
                if ($parentId == $contoraId && $expertId != $chairmanParentId){
                    $chairmanExpertCount++;
                }                
            }
        }       
        $result['fieldContractNumber'] = $expertiseDocument->getFieldOrSpace('document_number');
        $result['fieldContractDate'] = $expertiseDocument->getDate('open_date');
        $result['fieldExpertFio'] =    $expert->getFio();
        $result['fieldExpertDirect'] =  $expert->getFieldOrSpace('director');
        $result['fieldEduOrgName'] = $eduOrg->getFieldOrSpace('FullName');
        $result['fieldEduOrgAdress'] = $eduOrg->getFieldOrSpace('LawAddress');
        $result['fieldExpertiseDateBegin'] = $expertise->getDate('create_timestamp');
        $result['fieldExpertiseDateEnd'] = $expertise->getDate('end_timestamp');
        $result['fieldExpertOrganisationCount'] = $chairmanExpertCount;
        $result['fieldExpertAddress'] =  $expertParent->getFieldOrSpace('address');
        $result['fieldExpertAddressFact'] =  $expertParent->getFieldOrSpace('fact_address');
        $result['fieldExpertTelefon'] =  $contact->getFieldOrSpace('phone');
        $result['fieldExpertInn'] =  $expertParent->getFieldOrSpace('inn');
        $result['fieldExpertKpp'] =  $expertParent->getFieldOrSpace('kpp');
        $result['fieldExpertBank'] =  $expertParent->getFieldOrSpace('invoice');
        $result['fieldExpertBik'] =  $expertParent->getFieldOrSpace('bik');
        $result['fieldExpertOkpo'] =  $expertParent->getFieldOrSpace('okpo');
        return $result;
    }

    public function getTemplateName(){
        return 'expertise/contract/orgznisation.docx';
    }

    public function getDocumentName() {
        return 'Договор с экспертной организацией';
    }
}

