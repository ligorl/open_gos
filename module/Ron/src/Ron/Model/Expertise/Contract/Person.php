<?php

namespace Ron\Model\Expertise\Contract;

/**
 * готовим данные для печати договора с экспертом
 */
class Person
{
    public $expertiseDocument;
    public function __construct($expertiseDocument) {
        //$expertiseDocument = new \Model\ExpertEntities\expertisedocument();
        //$expertiseDocument = $expertiseDocument->getById($expertiseDocumentId);

        if (empty($expertiseDocument)){
            throw new Exception('Не найдено заявление');
        }
        $this->expertiseDocument=$expertiseDocument;
    }
    /*
${fieldContractNumber}
${fieldContractDate}
${fieldExpertFio}
${fieldExpertRole}
${fieldEduOrgName}
${fieldEduOrgAdress}
${fieldExpertiseDateBegin}
${fieldExpertiseDateEnd}
     * ${blockChair}   ${BLOCKCHAIR}
     * ${/blockChair} ${/BLOCKCHAIR}
     * ${blockExpert}  ${BLOCKEXPERT}
     * ${/blockExpert}${/BLOCKEXPERT}
${fieldExpertFioFull}
${fieldExpertWork}
${fieldExpertWorkPost}
${fieldExpertInn}
${fieldExpertBirth}
${fieldExpertTelefon}
${fieldExpertEmail}
 */
    public function getData(){
        $result = array();
        $expertiseDocument = $this->expertiseDocument;
        $expert = $expertiseDocument->getExpertIt();
        $expertise = $expertiseDocument->getExpertise();
        $chairman = $expertiseDocument->getChairman();
        $commissionMember = $expertiseDocument->getCommissionMember();
        $eduOrg = $expertise->getEducationalOrganization();                
        $personWorkEduOrg = $expert->getPersonWorkOrganisationNow();        
        $expertContact = $expert->getContact();
        if(empty($expertContact)){
            $expertContact = new \Model\ExpertEntities\contact();
        }
        if($chairman){
            $chair = 'руководителя';
        }else{
            $chair = '';
        }        
        if($commissionMember){
            $member = 'эксперта';
        }else{
            $member  = '';
        }  
        if(empty($chair)||empty($member)){
            $role = $member . $chair ;
        }else{
            $role = $member .' и '.$chair ;
        }        
        $result['fieldContractNumber'] = $expertiseDocument->getFieldOrSpace('document_number');
        $result['fieldContractDate'] = $expertiseDocument->getDate('open_date');
        $result['fieldExpertFio'] =    $expert->getFio();
        $result['fieldExpertRole'] = $role;        
        $result['fieldEduOrgName'] = $eduOrg->getFieldOrSpace('FullName');
        $result['fieldEduOrgAdress'] = $eduOrg->getFieldOrSpace('LawAddress');
        $result['fieldExpertiseDateBegin'] = $expertiseDocument->getDate('create_timestamp');
        $result['fieldExpertiseDateEnd'] = $expertiseDocument->getDate('end_timestamp');
        if(empty($chair)){
            $result['blockChair'] = array();
        }else{
            $result['blockChair'] = array(1);
        }
        if(empty($member)){
            $result['blockExpert'] = array();
        }else{
            $result['blockExpert'] = array(1);
        }        
        $result['fieldExpertFioFull'] =  $result['fieldExpertFio'];
        $personWorkEduOrgName = '';
        if(!empty($personWorkEduOrg)){
            $personWorkEduOrgName=$personWorkEduOrg->getFieldOrSpace('FullName');
        }
        if(empty($personWorkEduOrgName)){
            $eduOrgName = $expert->getPersonWorkOrganisationNowName();
            if(!empty($eduOrgName)){
                $personWorkEduOrgName = $eduOrgName;
            }
        }
        $result['fieldExpertWork'] = $personWorkEduOrgName;
        $result['fieldExpertWorkPost'] =  $expert->getFieldOrSpace('post');
        $result['fieldExpertInn'] =  $expert->getFieldOrSpace('inn');
        $result['fieldExpertBirth'] = $expert->getDate('birth_date');
        $result['fieldExpertTelefon'] =  $expertContact->getFieldOrSpace('phone');
        $result['fieldExpertEmail'] =  $expertContact->getFieldOrSpace('email');     

        return $result;
    }

    public function getTemplateName(){
        return 'expertise/contract/person.docx';
    }
    
    public function getDocumentName() {
        return 'Договор с экспертом';
    }
}

