<?php

namespace Ron\Model\Expertise;

class ExpertiseIssueOrder
{
    public $application;
    public $expertise;
    //public $commissionMember;

    public function __construct($applicationId) {
        $expertise = new \Model\ExpertEntities\expertise($applicationId);
        $expertiseId = $expertise->getId();
        if (empty($expertiseId)){
            throw new \Exception('Не найдена экспертиза');
        }
        $this->expertise = $expertise;
        $application = $expertise->getApplication();
        //$application = new \Model\Entities\isgaApplications($applicationId);
        //$applicationId2 = $application->getId();
        if (empty($application)){
            throw new Exception('Не найдено заявление');
        }
        $this->application=$application;

    }
/**
 * - ${tableProgramPeriod}
 * @return type
 */
    public function getData(){
        $result = array();
        $application = $this->application;
        $expertise = $this->expertise;
        $eduOrg = $expertise->getEducationalOrganization();
        $eduData = $eduOrg->getEducationalOrganizationData();
        $appOrgId = $application->getFieldOrSpace('fk_eiisEducationalOrganization');
        $this->appOrgId = $appOrgId;

        $nameInstrCase=$eduData->getField('NameInstrCase');
        $nameInstrCase = empty($nameInstrCase)?$eduOrg->getField('FullName'):$nameInstrCase;

        $result['FieldEduAblativeCase'] = $nameInstrCase;
        $result['FieldEduAblativeCaseDub'] = $nameInstrCase;
        $result['fieldAppDateReg'] =    $application->getDate('DateReg');
        $result['fieldExpertiseDateBegin'] = $expertise->getDate('create_timestamp');
        $result['fieldExpertiseDateEnd'] =   $expertise->getDate('end_timestamp');

        $expertiseProgram = $expertise->getExpertisePrograms();
        //$result['_tableExpertFio']=array();
        $result['blockFilial']=array();

        $commissionMemberAll = $expertise->getCommissionMember();
        $commissionMemberAll = $commissionMemberAll->getObjectsArray();

        $orgIdArray = array();
        $blockOrg = array();
        $orgIdByExpertId = array();
        $blockOrgData = array();
        if ($commissionMemberAll){
                usort($commissionMemberAll, function($a_obj, $b_obj){
                    $a = $a_obj->getField('institution_id');
                    $b = $b_obj->getField('institution_id');
                    if ($a == $b) {
                        return 0;
                    }
                    //мутка, чтоб главный выплыл в начало
                    if(!empty($this->appOrgId)){
                        if($a == $this->appOrgId){
                            return -1;
                        }
                        if($b == $this->appOrgId){
                            return 1;
                        }
                    }
                    return ($a < $b) ? -1 : 1;
                });
        }

        if ($commissionMemberAll){
            $expertArray = array();
            $eduLevelArray = array();
            $ugsArray = array();
            $eduProgramArray = array();
            $tableExp = array();
            $chairmainArray = array();
            $expertByProgramId = array();
            $chairmainProgram = array();

            //сбиваем руководителей
            $chairmain = $expertise->getChairman();
            if(!empty($chairmain)){
                foreach ( $chairmain as $chairmainOne ) {
                    $expertId = $chairmainOne->getField('expert_id');
                    if(empty($tableExp[$expertId])){
                        //$tableExp[$expertId] = array();
                        if(empty($expertArray[$expertId])){
                            $expert = new \Model\ExpertEntities\expert();
                            $expert = $expert->getById($expertId);
                            if(!empty($expert)){
                                $expertArray[$expertId] = $expert;
                            }
                        }
                    }
                    $chairmainArray[$expertId] = 1;
                    $chairmainExpertIdOfOrg[$chairmainOne->getField('institution_id')] = $expertId;
                    //помечаем в каком филиале сидит эксперт
                    //тут фишка, эксперт только в одном вузе может быть
                    //if(empty($orgIdByExpertId[$expertId])){
                        $orgIdByExpertId[$expertId] = $chairmainOne->getField('institution_id');
                    //}
                }
            }

            //сбиваем программы
            foreach ($commissionMemberAll as  $oneMember){
                if($oneMember->isRefused()){
                    continue;
                }                
                $appProgram = $oneMember->getApplicationProgram();
                if(empty($appProgram)){
                    continue;
                }
                $licProgram = $appProgram->getLicensedProgram();
                $baseProgram = $licProgram;                
                
                $commissionMemberAllTrace[$oneMember->getField('expert_id')][] = $oneMember;

                $expertId = null;
                $expertId = $oneMember->getField('expert_id');
                $institutionId = $oneMember->getField('institution_id');

                if(!empty($expertId) && empty($expertArray[$expertId])){
                    $expertArray[$expertId] = $expert = $oneMember->getExpert();
                }
                //определяем какой эксперт в каком вузе
                if(empty($orgIdByExpertId[$expertId])){
                    $orgIdByExpertId[$expertId] = $institutionId;
                }

                //проверяем к какому филиалу относится эксперт
                //если нет пустого то создадим
                //он будет начальником
                //это чтоб начальник в филиале первым был
                if( !empty($expertArray[$expertId])){
                    if(!empty($chairmainExpertIdOfOrg[$institutionId])){
                        if(empty($tableExp[$chairmainExpertIdOfOrg[$institutionId]])){
                            $tableExp[$chairmainExpertIdOfOrg[$institutionId]]=array();
                        }
                    }
                }


                $eduLevelId = null;
                $eduLevelId = $licProgram->getField("fk_eiisEduLevel");
                if(!empty($eduLevelId) && empty($eduLevelArray[$eduLevelId])){
                    $eduLevelArray[$eduLevelId] = $licProgram->getEduProgramLevel();
                }

                $ugs = null;
                $ugsId = null;
                $ugs = $baseProgram->getUgs();
                if (!empty($ugs)){
                    $ugsId = $ugs->getId();
                    $ugsArray[$ugsId] = $ugs;
                }
                $progdamData = array();
                $progdamData['Code'] = $licProgram->getCode();
                $progdamData['Name'] = $licProgram->getName();
                $progdamData['Period'] = $appProgram->getPeriod();

                //делаем табличку программ
                $tableExp[$expertId][$eduLevelId][$ugsId][$appProgram->getId()] = $progdamData;

                //если  эксперт не руководитель с программой запомним это
                //выше зачем
                if(empty($chairmainArray[$expertId])){
                    $expertByProgramId[$appProgram->getId()]=array(
                        'expertId' => $expertId,
                        'eduLevelId' => $eduLevelId,
                        'ugsId' => $ugsId,
                    );
                    //$chairmainProgram[$institutionId][$eduLevelId][$ugsId][$appProgram->getId()]=1;
                } else {
                    $chairmainProgram [$expertId] = $appProgram->getId();
                }
            }


            //метим у начальника, что такаяже программаесть у эксперта
            //удаляем эту программу у эскперта
            $expertDuplicatArray = array();
            if(!empty($chairmainProgram)){
                foreach ( $chairmainProgram as $expertId => $programId ) {
                    if(!empty($expertByProgramId[$programId])){
                        $expertIdIt = $expertByProgramId[$programId]['expertId'];
                        $eduLevelId = $expertByProgramId[$programId]['eduLevelId'];
                        $ugsId = $expertByProgramId[$programId]['ugsId'];
                        //$ugsId = $expertByProgramId[$programId]['ugsId'];
                        //$programId = $expertByProgramId[$programId]['programId'];

                        $expertDuplicatArray[$expertId] = $expertIdIt;
                        unset($tableExp[$expertIdIt][$eduLevelId][$ugsId][$programId]);
                        //это костыли чтоб пустых небыло
                        if(empty($tableExp[$expertIdIt][$eduLevelId][$ugsId])){
                            unset($tableExp[$expertIdIt][$eduLevelId][$ugsId]);
                        }
                        if(empty($tableExp[$expertIdIt][$eduLevelId])){
                            unset($tableExp[$expertIdIt][$eduLevelId]);
                        }
                        if(empty($tableExp[$expertIdIt])){
                            unset($tableExp[$expertIdIt]);
                        }
                    }
                }
            }

            //находим все вузы
            $orgArray = array();
            if(!empty($orgIdByExpertId)){
                $orgIdArray = array_unique($orgIdByExpertId);
                $orgArrayAll = new \Model\Entities\eiisEducationalOrganizations();
                $orgArrayAll =  $orgArrayAll->getAllById($orgIdArray);
                if(!empty($orgArrayAll)){
                    foreach ( $orgArrayAll as $value ) {
                        $orgNameArray[$value->getId()] = $value->getFieldOrSpace('FullName');
                    }
                }
                unset($orgArrayAll );
                unset($orgIdArray );
                unset($orgArrayClass);
            }

            //сбиваем выходной таблиц
            //$result['_tableExpertFio']=array();

            if(!empty($tableExp)){
                $expertRow = array();
                foreach ( $tableExp as $expertId => $eduLevel ) {
                    $expert = null;
                    if(!empty($expertArray[$expertId])){
                        $expert = $expertArray[$expertId];
                    }
                    $fio = '';
                    if(!empty($expert)){
                        $fio = $expert->getFieldOrSpace('last_name')
                                .' '.$expert->getFieldOrSpace('name')
                                .' '.$expert->getFieldOrSpace('middle_name');
                    }
                    $oneRow['tableExpertFio'] = $fio;
                    //${tableExpertChainMain}
                    $oneRow['tableExpertChainMain'] = empty($chairmainArray[$expertId])?' ':'(руководитель группы)';
                    //${tableExpertDuplicat}
                    $oneRow['tableExpertDuplicat'] = '';
                    if(!empty($expertDuplicatArray[$expertId])){
                        $expertDuplicat = $expertArray[$expertDuplicatArray[$expertId]];
                        $fio = $expertDuplicat->getFieldOrSpace('last_name')
                                .' '.$expertDuplicat->getFieldOrSpace('name')
                                .' '.$expertDuplicat->getFieldOrSpace('middle_name');
                        $oneRow['tableExpertDuplicat'] = $fio;
                    }
                    //${tableBlokEduLevel}
                    //${/tableBlokEduLevel}
                    //${TABLEBLOKEDULEVEL}
                    //${/TABLEBLOKEDULEVEL}
                    $moreLevel = array();
                    if (!empty($eduLevel)){
                        foreach ( $eduLevel as $eduLevelId => $ugs ) {
                            $oneLevel = array();
                            //${tableLeveEduCode}
                            //${tableLeveEduName}
                            //$oneLevel['tableLeveEduCode'] = $eduLevelArray[$eduLevelId]->getCode();
                            $oneLevel['tableLeveEduName'] = $eduLevelArray[$eduLevelId]->getPrintName();
                            $moreUgs = array();
                            if(!empty($ugs)){
                                if(!empty($ugs)){
                                    foreach ( $ugs as $ugsId => $programDataAll ) {
                                        $ugsItem = $ugsArray[$ugsId];
                                        $oneUgs = array();
                                        $oneUgs['tableUgsCode'] = $ugsItem ->getFieldOrSpace('Code');
                                        $oneUgs['tableUgsName'] = $ugsItem ->getFieldOrSpace('Name');
                                        $programMore = array();
                                        if(!empty($programDataAll)){
                                            foreach ( $programDataAll as $progdamData ) {
                                                $programOne = array();
                                                $programOne['tableProgramCode'] = $progdamData['Code'];
                                                $programOne['tableProgramName'] = $progdamData['Name'];
                                                $programOne['tableProgramPeriod'] = empty($progdamData['Period'])?'':' ('.$progdamData['Period'].')';
                                                if(!empty($programOne['tableProgramPeriod'])){
                                                    $programOne['tableProgramName'] = $programOne['tableProgramName'].$programOne['tableProgramPeriod'] ;
                                                }
                                                $programOne['tableProgramPeriod'] = '';
                                                $programMore[] = $programOne;
                                            }
                                        }
                                        //${tableBlokProgram}
                                        //${/tableBlokProgram}
                                        // ${TABLEBLOKPROGRAM}
                                        //${/TABLEBLOKPROGRAM}
                                        $oneUgs['tableBlokProgram'] = $programMore;
                                        $moreUgs[]=$oneUgs;
                                    }
                                }
                            }
                            // ${TABLEBLOKUGS}
                            //${/TABLEBLOKUGS}
                            $oneLevel['tableBlokUgs'] = $moreUgs;
                            $moreLevel[] = $oneLevel;
                        }
                    }   else{
                        $moreLevel =array(array('tableBlokUgs'=>array(),'tableLeveEduName'=>'-'));
                    }
                    $oneRow['tableBlokEduLevel'] = $moreLevel;
                    //$expertRow[] = $oneRow;

                    $orgId = $orgIdByExpertId[$expertId];
                    $orgName = $orgNameArray[$orgId];
                    //${fieldFilalName}
                    $blockOrgData[$orgId]['fieldFilalName'] = $orgName;
                        $blockOrgData[$orgId]['_tableExpertFio'][] = $oneRow;
                }
                //$result['_tableExpertFio'] = $expertRow;
                //${blockFilial}  ${BLOCKFILIAL}
                //${/blockFilial}${/BLOCKFILIAL}
                $result['blockFilial'] = $blockOrgData;
                //throw new \Exception("stop");
            }

            $isOnlyOneProgram = false;
            if(!empty($result['blockFilial'])&& count ($result['blockFilial'])==1){
                $firstFilial = reset ($result['blockFilial']);            
                if(!empty($firstFilial['_tableExpertFio'])&& count ($firstFilial['_tableExpertFio'])==1){
                    $firstProgram = reset ($firstFilial['_tableExpertFio']);
                    if(!empty($firstProgram['tableBlokEduLevel'])&& count ($firstProgram['tableBlokEduLevel'])==1){
                        $firstLevel = reset($firstProgram['tableBlokEduLevel']);
                        if(!empty($firstLevel['tableBlokUgs'])&& count ($firstLevel['tableBlokUgs'])==1){
                            $firstUgs = reset($firstLevel['tableBlokUgs']);
                            if(!empty($firstUgs['tableBlokProgram'])&& count ($firstUgs['tableBlokProgram'])==1){
                                $isOnlyOneProgram = true;
                            }
                        }
                    }
                }
            }
            //${fieldlabelOfCountProgram}
            if($isOnlyOneProgram){
                $result['fieldlabelOfCountProgram'] = 'образовательной программы, реализуемой';
            }else{
                $result['fieldlabelOfCountProgram'] = 'образовательных программ, реализуемых';
            }



        }
        //print_r($result);
        //die;
        return $result;
    }

    public function getTemplateName(){
        return 'expertise/ExpertiseIssueOrder.docx';
    }
    public function getDocumentName() {
        return 'распоряжение об акредитации';
    }
}