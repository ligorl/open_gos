<?php

namespace Ron\Model\Expertise;

/**
 * готовим данные для печати договора с экспертом
 */
class expertiseExpertConsent
{
    public $expertise;
    public $commissionMember;

    public function __construct($expertiseId,$expertId,$organizationId) {
        $expertise = new \Model\ExpertEntities\expertise();
        $expertise = $expertise->getById($expertiseId);
        if (empty($expertise)){
            throw new Exception('Не найдено экспертизы');
        }
        $this->expertise = $expertise;

        $commissionMember = $expertise->getCommissionMember(array(
            'expert_id'      => $expertId,
            'institution_id' => $organizationId,            
        ));
        if (empty($expertise)){
            throw new Exception('Не члелн эксперной комисии');
        }
        $this->commissionMember = $commissionMember;                       
    }
    
    /*
${fieldEduOrgName}
${fieldExpertiseDateBegin}
${fieldExpertiseDateEnd}
    ${tableProgramCode}
    ${tableProgramName}
    ${tableLeveEdu}
${fieldCuratorFio}
${fieldCuratorEmail}
${fieldCuratorTelefon}
 */
    public function getData(){
        $result = array();        
        $expertise = $this->expertise;
        
        $commissionMember = $this->commissionMember;    
        
        $result['fieldExpertiseDateBegin'] = $expertise->getDate('create_timestamp');
        $result['fieldExpertiseDateEnd'] = $expertise->getDate('end_timestamp');
        
        $result['_tableProgramCode']=array();
        $commissionMember=$commissionMember->getObjectsArray();
        if (!empty($commissionMember)){                        
            foreach ($commissionMember as  $oneMember) {
                $onerogramTable = array();
                $appProgram = $oneMember->getApplicationProgram();
                $licProgram = $appProgram->getLicensedProgram();
                $baseProgram = $licProgram;
                
                $programLevel = $baseProgram->getEduProgramLevel();
                $onerogramTable['tableLeveEdu'] = $programLevel->getField('Name');
                $onerogramTable['tableProgramCode'] = $baseProgram->getCode();
                $onerogramTable['tableProgramName'] = $baseProgram->getName();
                $result['_tableProgramCode'][]=$onerogramTable;
            }
            $user   = $commissionMember[0]->getUser();
            $eduOrg = $commissionMember[0]->getEducationalOrganization();            
        }
        
        if(empty($eduOrg)){
            $eduOrg = new \Model\Entities\eiisEducationalOrganizations();
        }
        $result['fieldEduOrgName'] = $eduOrg->getFieldOrSpace('FullName');
                
        if(empty($user)){
            $user = new \Model\Entities\ronUsers();
        }          
        
        $result['fieldCuratorFio'] = $user->getFieldOrSpace('Name');
        $result['fieldCuratorEmail'] = $user->getFieldOrSpace('Email');
        $result['fieldCuratorTelefon'] = $user->getFieldOrSpace('Telefon');                
        
        return $result;
    }

    public function getTemplateName(){
        return 'expertise/ExpertConsent.docx';
    }

    public function getDocumentName() {
        return 'Согласие эксперта';
    }
}


