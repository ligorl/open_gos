<?php

namespace Ron\Model\Expertise\Report;

/**
 * готовим данные для шаблона отчета для эксперта
 */
class Expert
{
    public $expertise;
    public $user;
    public $program;
    public function __construct($expertise, $user, $program) {
        if (empty($expertise)){
            throw new Exception('Не найдена экспертиза');
        }
        if (empty($user)){
            throw new Exception('Не найден пользователь');
        }
        $this->user=$user;
        $this->expertise=$expertise;
        $this->program=$program;
    }
    
    public function getData(){
        $Month_r = array(
            "01" => "января",
            "02" => "февраля",
            "03" => "марта",
            "04" => "апреля",
            "05" => "мая",
            "06" => "июня",
            "07" => "июля",
            "08" => "августа",
            "09" => "сентября",
            "10" => "октября",
            "11" => "ноября",
            "12" => "декабря"
        );
        $result = array();
        $expertise = $this->expertise;
        $program = $this->program;
        $user = $this->user;
        $eduOrg = $expertise->getEducationalOrganization();
        $expertiseDoc = $expertise->getExpertiseDocument(array('document_type_id' => '8'));
        $locationTable = new \Model\LodEntities\OrganizationLocation();
        $location = $locationTable->getOrganizationLocationFromOrg($eduOrg->getField('Id'));
        $egs = $program->getEGS();
        $licProgramm = $program->getLicensedProgram();
        $levelProgramm = $program->getEduProgramType();
        $address = '';
        $level = '';
        $codeNameUgs = '';
        $codeNameProgramm = '';
        $document = '';
        if(!empty($location)){
            $address = $location[0]['address'];
        }
        if($egs){
            $codeNameUgs = $egs->getField('Code').' '.$egs->getField('Name');
        }
        $level1 = '';
        $level2 = '';
        $level12 = '';
        $level22 = '';
        if($levelProgramm){
            $level = $levelProgramm->isEmptyField('LodTitle')?'':$levelProgramm->getField('LodTitle');
            $level = str_replace('высшее образование -','',$level);
            if($level != ''){
                $levelArr = explode(' ',$level);
                $flag = false;
                $charset = mb_detect_encoding($level);
                foreach($levelArr as $name){
                    if(mb_strlen($level1.' '.$name,$charset) <= 15){
                        $level1 .= ' '.$name;
                    }else{
                        $flag = true;
                        break;
                    }
                }
                if($flag){
                    $level2 = substr($level,strlen($level1)-1);
                }
                $flag = false;
                foreach($levelArr as $name){
                    if(mb_strlen($level12.' '.$name,$charset) <= 30){
                        $level12 .= ' '.$name;
                    }else{
                        $flag = true;
                        break;
                    }
                }
                if($flag){
                    $level22 = substr($level2,strlen($level12)-1);
                }
            }
        }
        $codeNameProgramm2 = '';
        $codeNameProgramm22 = '';
        if($licProgramm){
            $codeNameProgramm = $licProgramm->getField('Code').' '.$licProgramm->getField('Name');
            $codeNameProgrammStr = $codeNameProgramm . ( !empty($level)?'('.$level.')':'') . ',';
            $charset = mb_detect_encoding($codeNameProgrammStr);
            $codeNameProgrammArr = explode(' ',$codeNameProgrammStr);
            $flag = false;
            foreach($codeNameProgrammArr as $value){
                if(mb_strlen($codeNameProgramm2.' '.$value,$charset) <= 65){
                    $codeNameProgramm2 .= ' '.$value;
                }else{
                    $flag = true;
                    break;
                }
            }
            if($flag){
                $codeNameProgramm22 = substr($codeNameProgrammStr,strlen($codeNameProgramm2)-1);
            }
        }
        if($expertiseDoc){
            $expertiseDoc = $expertiseDoc->toArray();
            $time = strtotime($expertiseDoc[0]['open_date']);
            $document = date('d',$time).' '.$Month_r[date('m',$time)].' '.date('Y',$time).'г. № '.$expertiseDoc[0]['document_number'];
        }
        $fullName = explode(' ',$eduOrg->getFieldOrSpace('FullName'));
        $fullNameOrg = array();
        $fullNameOrg2 = array();
        $fullNameOrg3 = array();
        $fullNameOrg4 = array();
        $fullNameOrg5 = array();
        $count = 0;
        foreach ($fullName as $key => $val){
            $len = strlen($val)/2 +1;
            $count += $len;

            if($count < 65){
                $fullNameOrg[] = $val;
            }elseif($count > 65 && $count < 120){
                $fullNameOrg2[] = $val;
            }elseif($count > 120 && $count < 175){
                $fullNameOrg3[] = $val;
            }elseif($count > 175 && $count < 230){
                $fullNameOrg4[] = $val;
            }elseif($count > 230){
                $fullNameOrg5[] = $val;
            }
        }
        $fio = '';
        $fio2 = '';
        $fioArr = explode(' ',$user->getField('Name'));
        foreach($fioArr as $val){
            $fio .= ucfirst($val).' ';
        }

        $morphy = new \Eo\Model\Licensing\Tools\Morphy();
        $morphyName = $morphy-> getForms( $user->getField('Name') );
        $morphyOrg1 = $morphy-> getForms( implode(' ', $fullNameOrg) );
        $morphyOrg2 = $morphy-> getForms( implode(' ', $fullNameOrg2) );
        $morphyOrg3 = $morphy-> getForms( implode(' ', $fullNameOrg3) );
        $morphyOrg4 = $morphy-> getForms( implode(' ', $fullNameOrg4) );
        $morphyOrg5 = $morphy-> getForms( implode(' ', $fullNameOrg5) );
        $charset = mb_detect_encoding($morphyName['ablative']);
        $fio2Arr = explode(' ',$morphyName['ablative']);
        foreach($fio2Arr as $val){
            $fio2 .= mb_strtoupper(mb_substr($val,0,1,$charset),$charset).mb_substr($val,1,mb_strlen($val,$charset),$charset).' ';
        }

        $result['eduOrgFirst1'] = implode(' ', $fullNameOrg);
        $result['eduOrgFirst2'] = implode(' ', $fullNameOrg2);
        $result['eduOrgFirst3'] = implode(' ', $fullNameOrg3);
        $result['eduOrgFirst4'] = implode(' ', $fullNameOrg4);
        $result['eduOrgFirst5'] = implode(' ', $fullNameOrg5);
        $result['eduOrgSec1'] = $morphyOrg1['prepositive'];//  про склонять
        $result['eduOrgSec2'] = $morphyOrg2['prepositive'];//  про склонять
        $result['eduOrgSec3'] = $morphyOrg3['prepositive'];//  про склонять
        $result['eduOrgSec4'] = $morphyOrg4['prepositive'];//  про склонять
        $result['eduOrgSec5'] = $morphyOrg5['prepositive'];//  про склонять
        $result['addressOrg'] = $eduOrg->getFieldOrSpace('LawAddress');
        $result['level1'] = $level1;
        $result['level12'] = $level2;
        $result['level3'] = $level12;
        $result['level32'] = $level22;
        $result['programm1'] = $program->isEmptyField('Name')?'':$program->getField('Name');
        $result['programm2'] = $program->isEmptyField('Name')?'':$program->getField('Name');
        $result['codeNameUgs1'] = $codeNameUgs;
        $result['codeNameUgs2'] = $codeNameUgs;
        $result['codeNameProgramm1'] = $codeNameProgramm;
        $result['codeNameProgramm2'] = $codeNameProgramm2;
        $result['codeNameProgramm22'] = $codeNameProgramm22;
        $result['codeNameProgramm3'] = $codeNameProgramm;
        $result['addressLicense'] = $address;
        $result['expertFIO'] = $fio2;// про склонять
        $result['document'] =  $document;
        $result['fio'] =  $fio;
        $result['date'] =  date('d.m.Y');

        return $result;
    }

    public function getTemplateName(){
        return 'expertReportTemplate.docx';
    }
    
    public function getDocumentName() {
        return 'Отчет для эксперта';
    }
}

