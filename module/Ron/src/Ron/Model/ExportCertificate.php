<?php

namespace Ron\Model;

class ExportCertificate extends Export
{

    private $certificateId;
    private $certificate;

    public static function certificateFabrica($certificateId, $what,$applicationId)
    {
        //цепануть сертфикат
        if ($what == 'certificate') {
            $certificate = new \Model\Entities\isgaCertificates($certificateId);
            if (empty($certificate)) {
                return null;
            }
            //получить левцю еду
            $eduLevel = $certificate->getEduLevel();
            if($eduLevel===null){
                throw new \Exception('Не опредлен уровень образовательной программы');
            }
            //раскидать по уровню
            $eduLevelIsProgram = $eduLevel->getField('IsProgram');
            if (!empty($eduLevelIsProgram)) {
                return new \Ron\Model\ExportCertificateVuz($certificate);
            } else {
                return new \Ron\Model\ExportCertificateSchool($certificate);
            }
        }
        if ($what == 'supplement') {
            $supplements = new \Model\Entities\isgaCertificateSupplements($certificateId);
            if (empty($supplements)) {
                return null;
            }
            //получить левцю еду
            $eduLevel=$supplements->getCertificate()->getEduLevel();
            if($eduLevel===null){
                throw new \Exception('Не опредлен уровень образовательной программы');
            }
            //раскидать по уровню
            $eduLevelIsProgram = $eduLevel->getField('IsProgram');
            if (!empty($eduLevelIsProgram)) {
                return new \Ron\Model\ExportCertificateSupplementVuz($supplements,$applicationId);
            } else {
                return new \Ron\Model\ExportCertificateSupplementSchool($supplements,$applicationId);
            }
        }
    }

    public function __construct($certificate)
    {
        if (empty($certificate)) {
            return null;
        }
        $this->certificateId = $certificate->getField('Id');
        $this->certificate = $certificate;
    }

    public function getCertificateId()
    {
        return $this->certificateId;
    }

    public function getCertificate()
    {
        return $this->certificate;
    }

    //подготовить выборку для печати сертиффиката
    public function getData()
    {
        $certificate = $this->getCertificate();
        $result = array();
        $dateIssue = $certificate->getField('DateIssue');
        $dateEnd = $certificate->getField('DateEnd');
        $eduOrg = $certificate->getEducationalOrganization();
        $eduOrgdata=$eduOrg->getEducationalOrganizationData();
        $eduInDatus=$eduOrgdata->getField('NameDatCase');
        $result['fieldDay'] = $this->getDayOfDate($dateIssue);
        $result['fieldMount'] = $this->getMountOfDate($dateIssue);
        $result['fieldYear'] = $this->getYearOfDate($dateIssue);
        if(empty($eduInDatus)){
            $result['fieldNameDatus'] = $eduOrg->getField('FullName');
        }else{
           $result['fieldNameDatus'] =$eduInDatus;
        }
        $result['fieldAdres'] = $eduOrg->getField('LawAddress');
        $result['fieldOgrn'] = $eduOrg->getField('GosRegNum');
        $result['fieldInn'] = $eduOrg->getField('Inn');
        $result['fieldDayEnd'] = $this->getDayOfDate($dateEnd);
        $result['fieldMountEnd'] = $this->getMountOfDate($dateEnd);
        $result['fieldYearEnd'] = $this->getYearOfDate($dateEnd);
        $headName = $certificate->getField('ControlOrganHeadFullName');
        $headName = empty($headName) ? '' : $headName;
        $result['fieldHeadName'] = $headName;
        $headPost = $certificate->getField('ControlOrganHeadPost');
        $headPost = empty($headPost) ? 'Руководитель' : $headPost;
        $result['fieldHeadPost'] = $headPost;                
        return $result;
    //${fieldHeadPost}
       // ${fieldHeadName}
        
    }

    /**
     *  возращает имя выходного файла.
     *
     * @return string
     */
    public function getDocumentName()
    {
        $encod=mb_internal_encoding();        
        $certificate = $this->getCertificate();
        $certNum = $certificate->getField('RegNumber');
        $certNum = empty($certNum) ? '0' : $certNum;
        //$eduOrg = $certificate->getEducationalOrganization();
        $applEduOrgName = $certificate->getField('EOFullName');
        //берем то, что в кавычках
        $index1 = mb_strpos($applEduOrgName, '«', 0, 'utf-8');
        $index2 = false;
        if ($index1 !== false) {
            $index2 = mb_strpos($applEduOrgName, '»', $index1, 'utf-8');
        }
        $outOrgName = '';
        if ($index2 === false) {
            $eduOrg = $certificate->getEducationalOrganization();
            $suppEduOrgNameString=$eduOrg->getField('RegularName');
        }
            //если кошерно, то обрезание
            $suppEduOrgNameString = mb_substr($applEduOrgName, $index1 + 1, $index2 - $index1 - 1, 'utf-8');
            $suppEduOrgNameArray = mb_split(' ', $suppEduOrgNameString);
            foreach ($suppEduOrgNameArray as $oneString) {
                $oneStringShort = mb_substr($oneString, 0, 4, 'utf-8');
                if (empty($outOrgName)) {
                    $outOrgName = $oneStringShort;
                } else {
                    $outOrgName .= '_' . $oneStringShort;
                }
            }
        return 'Свидетельство_'.$certNum.'_'.$outOrgName;
    }

    public function createDatePrint($param)
    {
        $certificate=$this->getCertificate();
        $datePrint=$certificate->getDate('DatePrint');
        if(empty($datePrint)){
             $certificate->exchangeArray(array('DatePrint'=>date('Y-m-d')));
             $certificate->save(0);
        }

    }
}