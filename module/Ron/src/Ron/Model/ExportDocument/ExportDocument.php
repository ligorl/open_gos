<?php

namespace Ron\Model\ExportDocument;

class ExportDocument
{

    static function getDocumentByIdApplication($id)
    {
        //$applicationsClass = new \Model\Gateway\EntitiesTable('isga_Applications');
        //$currentApplication = $applicationsClass->getEntityById($id);
        $currentApplication = new \Model\Entities\isgaApplications($id);
        //$applicationType = $currentApplication->getTypeCode();
        $applicationType = $currentApplication->getType()->toArray();
        //$type =  $currentApplication->getTypeCode();
        $type = $applicationType ['Code'];
        $name = $applicationType ['NameAccusativeCase'];
        switch ($type) {
            case 'DublicateCertificate':
                return new ExportDublicateCertificate($currentApplication, $name);
                break;
            case 'ReAccreditation':
            case 'Certificate':
                return new ExportCertificate($currentApplication, $name);
                break;
            case 'Accreditation':
                return new ExportAccreditation($currentApplication, $name);
                break;
            case 'TempCertificate':
                return new ExportTempCertificate($currentApplication, $name);
                break;
        }
        return false;
    }

    function getData()
    {

    }

    function getType()
    {

    }

    protected $applicationsClass;
    protected $currentApplication;
    protected $monthArray = array('год', "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
    protected $resultArray = array();
    protected $mainOrg;
    protected $moreInfo;

    function __construct($currentApplication, $name)
    {
        $this->currentApplication = $currentApplication;
        $this->resultArray['templName'] = $name;
        $this->resultArray['templType'] = $this->getType();
        $this->resultArray['dateCreate'] = $currentApplication->getField('DateCreate');
        $this->mainOrg = $this->currentApplication->getEducationalOrganization();
               // $this->currentApplication->getLinkedItem('eiis_EducationalOrganizations',
               //                                                   array('Id' => $this->currentApplication->getField("fk_eiisEducationalOrganization")));
        /*$this->moreInfo = $this->currentApplication->getLinkedItem(
            'isga_ApplicationMoreInfo', array('fk_isgaApplication' => $this->currentApplication->getField("Id"))
          )->toArray();*/
        $moreInfoData = $this->currentApplication->getLinkedItems(
            'isga_ApplicationMoreInfo', array('fk_isgaApplication' => $this->currentApplication->getField("Id"))
        );
        $moreInfoArr = array();
        if(!empty($moreInfoData)){
            foreach($moreInfoData as $moreInfoItem){
                $idOrg = 0;
                if(!$moreInfoItem->isEmptyField('fk_eiisEducationalOrganization')){
                    $idOrg = $moreInfoItem->getField('fk_eiisEducationalOrganization');
                }
                $moreInfoArr[$idOrg] = $moreInfoItem;
            }
        }
        $this->moreInfo = $moreInfoArr;
    }

    /*
     * возращает перечень таблиц в шаблоне
     * имя таблицы=>имя первого поля
     */
    public function getTablesOut()
    {
        return array('table1' => 'tabfield1');
    }

    public function ifNotEmpty($s)
    {
        $result=((empty($s)|| $s==='NULL')? '' : $s);
        return $result;
    }

    public function ifNotDefis($s)
    {
        return (empty($s)) ? ' - ' : $s;
    }

    public function ifActive($value)
    {
        return $this->writeSymbolActive($this->conditionSymbolActive($value));
    }

    public function ifNotActive($value)
    {
        return $this->writeSymbolActive(!$this->conditionSymbolActive($value));
    }

    protected function conditionSymbolActive($value)
    {
        return $value == 1;
    }

    private function writeSymbolActive($condition)
    {
        return ($condition) ? 'X' : ' ';
    }

    protected function getCodeProgram($program)
    {
        return (array_key_exists('Code', $program)) ? $program['Code'] : '';
    }

    protected function getDetailsLicenseWorkUsingInformationSecret($MoreInfoArr,$idOrg = 0)
    {
        if(isset($MoreInfoArr[$idOrg])){
            $MoreInfo = $MoreInfoArr[$idOrg]->toArray();
        }elseif(isset($MoreInfoArr[0])){
            $MoreInfo = $MoreInfoArr[0]->toArray();
        }else{
            return '';
        }
        if (empty($MoreInfo['RegNumber'])) {
            return '';
        }
        $data_reg = '';
        if (!empty($MoreInfo['DateReg'])) {
            $data_reg = " от " . date('d.m.Y', strtotime($MoreInfo['DateReg']));
        }
        $data_end = '';
        if (!empty($MoreInfo['DateReg'])) {
            $data_end = ", действует до " . date('d.m.Y', strtotime($MoreInfo['DateEnd']));
        }
        return "№ {$MoreInfo['RegNumber']}, {$MoreInfo['SiriesNumber']}" . $data_reg . $data_end . ".";
    }

    protected function getRequisitesCertificate($certificate)
    {
        if (empty($certificate)) {
            return '';
        }
        if (!empty($certificate['DateIssue'])) {
            $data_reg = " от " . date('d.m.Y', strtotime($certificate['DateIssue']));
        }
        return "№ {$certificate['RegNumber']}, {$certificate['SerialNumber']}" . $data_reg;
    }

    protected function getPrhoneFaxeseducationalOrganizationData($data)
    {
        $result = array();
        $mas = array('Phones', 'Faxes');
        foreach ($mas as $key) {
            if (array_key_exists($key, $data) && !empty($data[$key])) {
                $result[] = $data[$key];
            }
        }
        return implode(', ', $result);
    }

    protected function getFIOContactManager($arrayOrg)
    {
        //$resuls=$this->ifNotEmpty($arrayOrg['ContactSecondName'])
        //  . ' ' .
        //  $this->ifNotEmpty($arrayOrg['ContactFirstName'])
        //  . ' ' .
        //  $this->ifNotEmpty($arrayOrg['ContactLastName']);
        //if($resuls==='     '){
            $resuls=  $this->ifNotEmpty($arrayOrg['ChargeFio']);
        //}
        return $resuls;
    }

    /*
     * Формирует картинку штрих кода в формате GIF
     */
    public function getShtrihKod($code = null, $fk_isgaApplicationStatus = true)
    {
        if (empty($fk_isgaApplicationStatus) || empty($code)) {
            return '';
        }
        $barcodeOptions = array('text' => abs($code));
        $barcode = new \Zend\Barcode\Object\Ean13($barcodeOptions);
        $rendererOptions = array();
        $renderer = new \Zend\Barcode\Renderer\Image($rendererOptions);
        $image = $renderer->setBarcode($barcode)->draw();

        ob_start();
        imagegif($image);
        $contents = ob_get_contents();
        ob_end_clean();
        return $contents;
    }

    public function dateParse($resultArray = array())
    {
        $dateCreate = $this->currentApplication->getField('DateCreate');
        if (($dateCreate = strtotime($dateCreate)) === false) {
            $dateCreate = time();
        }
        $resultArray['fieldDateDay'] = date('d', $dateCreate);
        $resultArray['fieldDateYear'] = date('Y', $dateCreate);
        $resultArray['fieldDateMounth'] = $this->monthArray[(int) date('m', $dateCreate)];
        return $resultArray;
    }

    /*
     * заполнение таблиц в документе
     */
    public function templateMakeTable($tableArray, $table, $templateProcessor)
    {
        foreach ($table as $tableName => $firstField) {
            if (!empty($tableArray[$tableName]) && is_array($tableArray[$tableName])) {
                $templateProcessor->cloneRow($firstField, count($tableArray[$tableName]));
                foreach ($tableArray[$tableName] as $keyRow => $valueRow) {
                    //$valueRow['countcol'] = $keyRow + 1;
                    foreach ($valueRow as $keyFiled => $valueField) {
                        $macro = $keyFiled . '#' . (string) ($keyRow + 1);
                        $templateProcessor->setValue($macro, htmlspecialchars($valueField, ENT_COMPAT, 'UTF-8'), 1);
                    }
                };
            } else {
                //Стираем строки, если для них нет значений.
                $templateProcessor->cloneRow($firstField, 0);
            }
        }
        return $templateProcessor;
    }

    /*
     * Заполнение полей в документе
     */
    public function templateMakeValue($valueArray, $templateProcessor)
    {
        foreach ($valueArray as $key => $val) {
            if (is_string($val) || is_numeric($val)) {
                $templateProcessor->setValue($key, htmlspecialchars($val, ENT_COMPAT, 'UTF-8'), 1);
            }
        }
        return $templateProcessor;
    }

    /*
     * Форирование документа из шаблона
     */
    public function templateMakeDoccument($arrayData, $templateDocPath, $outType = 'docx')
    {
        //создаем временное хранилище
        $tempDir = sys_get_temp_dir() . '/' . uniqid() . '/';
        if (!mkdir($tempDir, 0777, true)) {
            return false;
        }
        ///Берем класс шаблонизатора
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateDocPath);
        //Изменяем штрих код
        if ($outType != 'pdf' || empty($arrayData['shtrihKod'])) {
            $templateProcessor->deleteImage();
        } else
        if (!empty($arrayData['shtrihKod'])) {
            $strihFile = 'image1.gif';
            $strihFilePath = $tempDir . $strihFile;
            file_put_contents($strihFilePath, $arrayData['shtrihKod']);
            $templateProcessor->setImageValue($strihFile, $strihFilePath);
            unset($strihFile);
        }
        //вставляем значения полей
        $templateProcessor = $this->templateMakeValue($arrayData, $templateProcessor);
        //Заполняем таблички.
        $templateProcessor = $this->templateMakeTable($arrayData, $this->getTablesOut(), $templateProcessor);
        //Заполняем филиалы
        if (!empty($arrayData['branches']) && is_array($arrayData['branches'])) {
            $templateProcessor->cloneBlock('BRANCH', count($arrayData['branches']));
            foreach ($arrayData['branches']as $branchKey => $branchValue) {
                //вставляем значения полей
                $templateProcessor = $this->templateMakeValue($branchValue, $templateProcessor);
                //Заполняем табличку филиалов.
                $templateProcessor = $this->templateMakeTable($branchValue, $this->getTablesOut(), $templateProcessor);
            }
        } else {
            $templateProcessor->cloneBlock('BRANCH', 0);
        }
        //Сохраняем полученый документ и готовим к отправке
        $fileTempPath = $tempDir . 'out.docx';
        $templateProcessor->saveAs($fileTempPath);
        $result = '';
        if (file_exists($fileTempPath)) {
            $outTypes = array('pdf', 'doc', 'odt');
            if (in_array($outType, $outTypes)) {
                $s = '/usr/bin/libreoffice5.0 '
                  . ' --headless --convert-to ' . $outType . ' --nofirststartwizard '
                  . ' --nologo --norestore --invisible '
                  . ' --outdir ' . $tempDir
                  . ' ' . $fileTempPath;
                \Eo\Model\Templater::exec( $s );
                $fileTempConvert = $tempDir . 'out.' . $outType;
                if (file_exists($fileTempConvert)) {
                    $result = file_get_contents($fileTempConvert);
                    @unlink($fileTempConvert);
                }
            } else {
                $result = file_get_contents($fileTempPath);
            }
            unset($outTypes);
            @unlink($fileTempPath);
        }
        unset($fileTempPath);
        //Удаляем картинку штрихкода
        if (isset($strihFilePath)) {
            if (file_exists($strihFilePath)) {
                @unlink($strihFilePath);
            }
            unset($strihFilePath);
        }
        if (is_dir($tempDir)) {
            @rmdir($tempDir);
        }
        unset($tempDir);
        return $result;
    }

    protected $certificate = null;

    public function getCertificate()
    {
        if ($this->certificate === null) {
            $this->certificate = $this->currentApplication
              ->getLinkedItem(
                'isga_Certificates', array('Id' => $this->currentApplication->getField("fk_isgaPreviousCertificate"))
              )
              ->toArray();
        }
        return $this->certificate;
    }

    protected $applicationReason;

    public function getApplicationReason($param = null)
    {
        if (!isset($this->applicationReason)) {
            $this->applicationReason = $this->currentApplication->getLinkedItem(
              'isga_ApplicationReasons',
              array(
                'Id' => $this->currentApplication
                  ->getField("fk_isgaApplicationReason")
              )
            );
        }
        if ($param == null) {
            return $this->applicationReason->toArray();
        } else {
            return $this->applicationReason->getField($param);
        }
    }

    public function getApplicationReasonNames()
    {
        $application = $this->currentApplication;
        $reasonsArray = $application->getReasonArray();
        $reasonsAblativeNameArray = array_column($reasonsArray, 'AblativeName');
        $result = '';
        $i=1;
        foreach ($reasonsAblativeNameArray as $oneItem){
            if($i!=1){
              $result .= '; ';
            }
            $result .= $oneItem;
            $i++;
        }
        return $result;
    }

    public function getApplicationPrograms($param)
    {
        $applicationPrograms = $this->currentApplication
            ->selectComplicated(
              "isga_ApplicationPrograms",
              array(
                //'fk_eiisLicensedPrograms',
                //'yearStartEdu',
                //'termEdu',
                //'fullTimeCount',
                //'partTimeCount',
                //'correspondenceCount',
                //'familyCount',
                //'networkForm',
                //'remoteTechnology',
                //'publicAccreditation'
              ),
              array(
                'fk_isgaApplication' => $this->currentApplication
                ->getField('Id'),
                'fk_isgaEOBranches'  => $param)
            )->toArray();
        return $applicationPrograms;
    }

    public function getWwwOrganizationData($organizationData){
        if(!empty($organizationData['Www']) && $rgData['Www']!=='NULL'){
            return $organizationData['Www'];
        }
        return '';
    }

    public function getProgramToTableArray($programsIds,$newArrApplicationPrograms) {

        $currentApplication = $this->currentApplication;
            $programItems = $currentApplication->getLinkedItems(
                    "eiis_LicensedPrograms",
                    array('Id' => $programsIds)
                )->getObjectsArray();

            //учебные программы
            $i = 0;
            foreach ($programItems as $oneProgram) {
                $oneProgramtId=$oneProgram->getId();
                $appProgram=$newArrApplicationPrograms[$oneProgramtId];
                $oneProgramName = empty($appProgram['Name'])?$oneProgram->getField('Name'):$appProgram['Name'];

                if(empty($appProgram['fk_eiisEduProgramTypes'])){
                    $programType=$oneProgram->getEduProgramType();
                }else{
                    $programType=$oneProgram->getEduProgramType($appProgram['fk_eiisEduProgramTypes']);
                }

                if(empty($appProgram['fk_isgaEnlargedGroupSpecialities'])){
                    $ugs=$oneProgram->getUgs();
                }else{
                    $ugs=$oneProgram->getUgs($appProgram['fk_isgaEnlargedGroupSpecialities']);
                }

                $oneProgramCode=$oneProgram->getField('Code');
                $oneProgramCode=empty($oneProgramCode)?"":$oneProgramCode;

                $resultArray[$i]['countcol']=$i+1;
                $resultArray[$i]['tabfield1'] = $oneProgramName;
                $resultArray[$i]['tabfield2'] = $programType->getField('Name');
                $resultArray[$i]['tabfield3'] = $ugs->getField('Code');
                $resultArray[$i]['tabfield4'] = $ugs->getField('Name');
                $resultArray[$i]['tabfield5'] = $oneProgramCode;
                $resultArray[$i]['tabfield6'] = $oneProgram->getField('Name');
                $resultArray[$i]['tabfield7'] = $newArrApplicationPrograms[$oneProgramtId]['yearStartEdu'];
                $resultArray[$i]['tabfield8'] = $newArrApplicationPrograms[$oneProgramtId]['termEdu'];
                $resultArray[$i]['tabfield9']  = $this->ifNotDefis($newArrApplicationPrograms[$oneProgramtId]['fullTimeCount']);
                $resultArray[$i]['tabfield10'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgramtId]['partTimeCount']);
                $resultArray[$i]['tabfield11'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgramtId]['correspondenceCount']);
                $resultArray[$i]['tabfield12'] = $this->ifNotDefis($newArrApplicationPrograms[$oneProgramtId]['familyCount']);
                $resultArray[$i]['tabfield13'] = ($newArrApplicationPrograms[$oneProgramtId]['networkForm'] == 1 ? 'Да' : "Нет");
                $resultArray[$i]['tabfield14'] = ($newArrApplicationPrograms[$oneProgramtId]['remoteTechnology'] == 1 ? 'Да' : "Нет");
                $resultArray[$i]['tabfield15'] = ($newArrApplicationPrograms[$oneProgramtId]['publicAccreditation'] == 1 ? 'Да' : "Нет");
                ++$i;
            }
          return   $resultArray;
    }
}