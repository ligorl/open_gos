<?php

namespace Ron\Model\ExportDocument;

use Model\Gateway\EntitiesTable;
use Model\Entities\eiisEducationalOrganizations;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ExportDublicateCertificate
 *
 * @author sany
 */
class ExportTempCertificate extends ExportDocument {

    public  function getTablesOut (){
      return array('table1'=>'tabfield1','table2'=>'tabfield2');
    }

    function getType()
    {
        return  'TempCertificate';
    }

    public function getData()
    {
        $currentApplication = $this->currentApplication;
        $monthArray         = $this->monthArray;
        $resultArray        = $this->resultArray;

        $certificate = $this->getCertificate();
        $MainOrg     = $this->mainOrg;
        $arrayOrg    = $MainOrg->toArray();
        $moreInfo = $this->moreInfo;

        $reorgOrganNames = array();
        $isga_mtm_Application_Organizations = $currentApplication->selectComplicated("isga_mtm_Application_Organizations", array('fk_eiisEducationalOrganization'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        if ($isga_mtm_Application_Organizations) {
            $reorgOrganIDs = array_column($isga_mtm_Application_Organizations, 'fk_eiisEducationalOrganization');
            $reorgOrgans =  $isgaEGSArray = $currentApplication->selectComplicated("eiis_EducationalOrganizations", array('Id', 'FullName'), array('Id' => $reorgOrganIDs))->toArray();
            $reorgOrganNames = array_column($reorgOrgans, 'FullName');
        }

        $resultArray['fieldFullName']  = $arrayOrg['FullName'] . ( empty($arrayOrg['ShortName'])|| $arrayOrg['ShortName']='NULL'?'':', ' . $arrayOrg['ShortName']);
        $resultArray['fieldLawAddress']  = $arrayOrg['LawAddress'];
        $resultArray['fieldGosRegNum']  = $arrayOrg['GosRegNum'];
        $resultArray['fieldInn']  =  $this->ifNotEmpty($arrayOrg['Inn']);
        $resultArray['fieldKpp']  =  $this->ifNotEmpty($arrayOrg['Kpp']);
        $resultArray['fieldFullNameOnly']  = $arrayOrg['FullName'];
        $resultArray['fieldReason']  = $this->getApplicationReasonNames();
        $resultArray['fieldReorgOrganNames'] = implode(', ', $reorgOrganNames);
        $resultArray['field10'] = $this->getRequisitesCertificate($certificate);
        $resultArray['field11'] = 'Федеральная служба по надзору в сфере образования и науки ';
        $resultArray['fieldPhones']  =  $this->ifNotEmpty($arrayOrg['Phones']);
        $resultArray['fieldMails']  =  $this->ifNotEmpty($arrayOrg['Mails']);
        $resultArray['fieldWww']  =  $this->ifNotEmpty( $arrayOrg['Www']);
        $resultArray['fieldI'] = $this->ifActive($moreInfo['Information']);
        $resultArray['fieldInformationEmail'] = $this->conditionSymbolActive($moreInfo['Information']) ? $moreInfo['Email'] : ' ';
        $resultArray['fieldINo'] = $this->ifNotActive($moreInfo['Information']);
        $resultArray['fieldChargePosition'] =  $this->ifNotEmpty($arrayOrg['ChargePosition']);
        $resultArray['fieldFIOContactManager'] =  $this->ifNotEmpty($this->getFIOContactManager($arrayOrg));
        $resultArray=$this->dateParse($resultArray);
        $resultArray['shtrihKod'] = $this->getShtrihKod($currentApplication->getField('Barcode'), $currentApplication->getField('fk_isgaApplicationStatus'));

        $applicationPrograms       = $currentApplication
            ->selectComplicated(
                "isga_ApplicationPrograms",
                array(
                    'fk_eiisLicensedPrograms',
                    'yearStartEdu',
                    'termEdu',
                    'fullTimeCount',
                    'partTimeCount',
                    'correspondenceCount',
                    'familyCount',
                    'networkForm',
                    'remoteTechnology',
                    'publicAccreditation'
                ),
                array(
                    'fk_isgaApplication' => $currentApplication
                    ->getField('Id'),
                    'fk_isgaEOBranches'  => $arrayOrg['Id'])
                )->toArray();
        $newArrApplicationPrograms = array();
        foreach ($applicationPrograms as $oneAplicationProgram) {
            $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
        }
        $programsIds = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
        $programsIds = array_filter($programsIds);
        if (sizeof($programsIds) != 0) {
            $programItems = $currentApplication->getLinkedItems(
                    "eiis_LicensedPrograms",
                    array('Id' => $programsIds)
                )->getObjectsArray();
            $resultArray = array_merge($resultArray, $this->getTablesByProgram($programItems, $newArrApplicationPrograms));
        }

        $branchesApplication = $currentApplication->selectComplicated("isga_mtm_Application_Branches", array('fk_eiisEOBranch'), array('fk_isgaApplication' => $currentApplication->getField('Id')))->toArray();
        $j                   = 0;
        $educationOrganization = new eiisEducationalOrganizations();
        if (isset($branchesApplication)) {
            foreach ($branchesApplication as $oneBranch) {
                // пропускаем главный вуз, чтобы
                if ($oneBranch['fk_eiisEOBranch'] === $arrayOrg['Id']) {
                    continue;
                }
                $MainOrg                               = $currentApplication->getLinkedItem('eiis_EducationalOrganizations', array('Id' => $oneBranch['fk_eiisEOBranch']));
                $arrayOrg                              = $MainOrg->toArray();
                $educationOrganization->setField('Id', $arrayOrg['Id']);
                $educationalOrganizationDataRes = $educationOrganization->getEducationalOrganizationData();
                $educationalOrganizationData = ($educationalOrganizationDataRes !== false) ? $educationalOrganizationDataRes->toArray() : array();
                $resultArray['branches'][$j]['field1'] = $arrayOrg['FullName'] . (empty($arrayOrg['ShortName'])?'':', ' . $arrayOrg['ShortName']);
                $resultArray['branches'][$j]['field2'] = $arrayOrg['LawAddress'];
                $resultArray['branches'][$j]['field3'] =  $this->ifNotEmpty($arrayOrg['Kpp']);
                $resultArray['branches'][$j]['field4'] = $this->getPrhoneFaxeseducationalOrganizationData($educationalOrganizationData);
                $resultArray['branches'][$j]['field5'] =  $this->ifNotEmpty($arrayOrg['Mails']);
                $resultArray['branches'][$j]['field6'] = ($educationalOrganizationData) ? $educationalOrganizationData['Site'] : '';
                $applicationPrograms                   = $currentApplication->selectComplicated("isga_ApplicationPrograms", array('fk_eiisLicensedPrograms', 'yearStartEdu', 'termEdu', 'fullTimeCount', 'partTimeCount', 'correspondenceCount', 'familyCount', 'networkForm', 'remoteTechnology', 'publicAccreditation'), array('fk_isgaApplication' => $currentApplication->getField('Id'), 'fk_isgaEOBranches' => $oneBranch['fk_eiisEOBranch']));
                $applicationPrograms                   = $applicationPrograms->toArray();
                $applicationPrograms=array_filter($applicationPrograms);
                if (count($applicationPrograms) > 0) {
                    $newArrApplicationPrograms = array();
                    foreach ($applicationPrograms as $oneAplicationProgram) {
                        $newArrApplicationPrograms[$oneAplicationProgram['fk_eiisLicensedPrograms']] = $oneAplicationProgram;
                    }
                    $programsIds      = array_column($applicationPrograms, 'fk_eiisLicensedPrograms');
                    $programs         = $currentApplication->selectComplicated("eiis_LicensedPrograms", array('Id', 'Code', 'Name', 'fk_eiisEduProgram', 'fk_eiisEduProgramType'), array('Id' => $programsIds))->toArray();
                    $programItems = $currentApplication->getLinkedItems(
                                    "eiis_LicensedPrograms", array('Id' => $programsIds)
                            )->getObjectsArray();
                    if (count($programItems) > 0) {
                        $resultArray['branches'][$j]=array_merge($resultArray['branches'][$j], $this->getTablesByProgram($programItems, $newArrApplicationPrograms));
                    }
                }
                ++$j;
            }
        }
        return $resultArray;
    }

    /**
     * Сбиваем табличку/и из массива лицензионных программ
     * @param type $programItems - массис лицензионнх кпрограмм - классов
     * @return type - массив таблички
     */
    public function getTablesByProgram($programItems,$newArrApplicationPrograms) {
            $i1 = $i2 = 0;
            $array_unique1 = $array_unique2 = array();
            foreach ($programItems as $oneProgram) {
                $oneProgramId = $oneProgram->getId();
                $appProgram = $newArrApplicationPrograms[$oneProgramId];
                
                if(empty($appProgram['fk_eiisEduProgramTypes'])){
                    $programType=$oneProgram->getEduProgramType();
                }else{
                    $programType=$oneProgram->getEduProgramType($appProgram['fk_eiisEduProgramTypes']);
                }
                
                $oneProgramLevel = $programType->getEduLevel();
                if(empty($oneProgramLevel)){
                    $oneProgramLevel = $oneProgram->getEduProgramLevel();
                }
                $oneProgramLevelName = $oneProgramLevel->getField('Name');
                
                if (empty($appProgram['fk_isgaEnlargedGroupSpecialities'])) {
                    $oneProgramUgs = $oneProgram->getUgs();
                } else {
                    $oneProgramUgs = $oneProgram->getUgs($appProgram['fk_isgaEnlargedGroupSpecialities']);
                }

                $oneProgramUgsCode = $oneProgramUgs->getField('Code');
                $oneProgramUgsName = $oneProgramUgs->getField('Name');
                $oneProgramIsProgram = $oneProgramLevel->getField('IsProgram');
                if (empty($oneProgramIsProgram)) {
                   $name = $oneProgramLevelName;
                    if (!in_array($name, $array_unique1)) {
                        $resultArray['table1'][$i1]['countcol']=$i1+1;
                        $resultArray['table1'][$i1]['tabfield1'] = $name;
                        $array_unique1[] = $name;
                        ++$i1;
                    }
                } else {
                    $code = $oneProgramUgsCode;
                    $code = $code=='Void'?' ':$code;
                    if (!in_array($code, $array_unique2)) {
                        $resultArray['table2'][$i2]['countcol']=$i2+1;
                        $resultArray['table2'][$i2]['tabfield2'] = $code;
                        $resultArray['table2'][$i2]['tabfield3'] = $oneProgramUgsName;//$isgaEGSArrayName[$isgaGSIdEGS[$arrayEduPrograms[$oneProgram['fk_eiisEduProgram']]]];
                        $resultArray['table2'][$i2]['tabfield4'] = $oneProgramLevelName;//$arrayTypes[$oneProgram['fk_eiisEduProgramType']];
                        $array_unique2[] = $code;
                        ++$i2;
                    }
                    unset($code);
                }
            }
            return $resultArray;
    }

}


