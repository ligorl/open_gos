<?php

namespace Ron\Model;

class ExportXmlDublicateCertificate extends ExportXml
{

    public function getCodeUslugi()
    {
        return '4';
    }

    public function getData()
    {
        $currentApplication = $this->currentApplication;
        $arrayOrg = $this->arrayOrg;
        $resultArray = array();
        $resultArray['code'] = $arrayOrg['Id'];
        $resultArray['code_ref']  = '';
        $resultArray['applicant'] = 'true';
        //добавляем общие данные главного филиала
        $resultArray = $this->addFilialeMainData($arrayOrg, $resultArray);
        //Добавляем сведения об учредителях
        //$resultArray = $this->getUchrArray($arrayOrg,$resultArray);
        //перекидываем массив
        $resultArrayOut['institutions']['institution'] = array();
        $resultArrayOut['institutions']['institution'][] = $resultArray;
        $resultArray = array();
        //филиалы
        $resultArrayOut = $this->addBranchData($this->arrayOrg, $resultArrayOut,false, true);
        return $resultArrayOut;
    }

}