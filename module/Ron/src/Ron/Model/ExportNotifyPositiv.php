<?php

namespace Ron\Model;

class ExportNotifyPositiv extends ExportNotify
{

    /**
     * ставим метку о соответствии
     */
    public function chekkedIt()
    {
        $this->getApplicationDocumentGroup()->chekedPositiv();
    }

    /**
     *
     * @return string - новый статус документа
     */
    public function getStatusNew()
    {
        return '192660B9-977F-4CBD-9E5A-279097849359';
    }

    /**
     *
     * @return type - сформированный массив уведомления
     */
    public function getData()
    {
        $result = parent::getData();
        return $result;
    }

    /**
     *
     * @return string - имя уведомления
     */
    public function getNotifyName()
    {
        return 'Уведомление о соответствии';
    }

    /**
     * возвращает ид шаблона
     * (вместо вызова isga_NotificationTemplates)
     *
     * @return string
     */
    public function getNotifyTemplateId(){
        return '1B969B9C-7ABE-4416-A988-C643D621DDAB';
    }

}