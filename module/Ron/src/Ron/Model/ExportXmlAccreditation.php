<?php

namespace Ron\Model;

class ExportXmlAccreditation extends ExportXml
{

    public function getCodeUslugi()
    {
        return '1';
    }

    public function getData()
    {
        $applicationsClass = $this->applicationsClass;
        $currentApplication = $this->currentApplication;
        $monthArray = $this->monthArray;
        $MainOrg=$this->MainOrg;
        $arrayOrg = $this->arrayOrg;
        $resultArray = array();
        $resultArray['code'] = $arrayOrg['Id'];
        $resultArray['code_ref']  = '';
        $resultArray['applicant'] = 'true';
        $resultArrayOut = array();
        //добавляем общие данные главного филиала
        $resultArray = $this->addFilialeMainData($arrayOrg, $resultArray);
        //Добавление данных о общественных лицензиях
        $resultArray = $this->addAccreditationOf($arrayOrg, $resultArray);
        //Добавляем сведения об учредителях
        //$resultArray = $this->getUchrArray($arrayOrg,$resultArray);
        //Добавляем образовательные програмы главного вуза
        $resultArray=$this->addMainEductionsProgram($resultArray);
        //перекидываем массив
        $resultArrayOut['institutions']['institution'] = array();
        $resultArrayOut['institutions']['institution'][] = $resultArray;
        //филиалы
        $resultArrayOut = $this->addBranchData($this->arrayOrg, $resultArrayOut);
        return $resultArrayOut;
    }

}