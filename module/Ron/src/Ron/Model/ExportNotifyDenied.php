<?php

namespace Ron\Model;

class ExportNotifyDenied extends ExportNotify
{

    /**
     * ставим метку о соответствии
     */
    public function chekkedIt()
    {
        $this->getApplicationDocumentGroup()->chekedDenied();
    }

    /**
     *возвращает новый статус после создания уведомления
     *
     * @return string - новый статус документа
     */
    public function getStatusNew()
    {
        return '040677F0-45F2-4997-9E33-860B475C8779';
    }

    /**
     *
     * @return type - сформированный массив уведомления
     * 
     *  ${BLOKREASONSPAGECOUNTNOT} 
     *  ${/BLOKREASONSPAGECOUNTNOT}
     *  ${BLOKREASONSPAGECOUNTYES} 
     *  ${/BLOKREASONSPAGECOUNTYES}
     *  ${BLOKREASONSPAGE} 
     *      ${REASONSBLOCK}
     *      ${/REASONSBLOCK}
     *  ${/BLOKREASONSPAGE}     
     */
    public function getData()
    {
        $result = parent::getData();        
        //$comment = $this->notifyCurrent->getField('Comments');
        //$commentData = json_decode($comment);
        $docGroupGroup= new \Model\Entities\isgaApplicationDocumentGroupGroupStatusReason();
        $reasons = $docGroupGroup
          ->getReasons($this->getApplicationCurrent()->getField('Id'));
        $reasonsArray= $reasons ->toArray();
        $commentData=array();
        if (!empty ($reasonsArray)) {
            $reasonsOut = array();
            foreach ( $reasonsArray as $value) {
                if (isset($value['Comment'])) {
                    $commentData[]['fieldReason']= $value['Comment'];
                } elseif (isset($value ['fk_isgaApplicationDocumentGroupStatusReasons'])) {
                   $commentData[]['fieldReason'] =$docGroupGroup->getStatusReasonValue( $value ['fk_isgaApplicationDocumentGroupStatusReasons']);
                } else
                    continue;
            }
        }
        $reasonPage = array();
        if(!empty($commentData)){     
            $reasonPage[0]['fieldNameForSupplement']=$result['fieldName'];
            //$reasonPage[0]['fieldDateCreateSupplement']=$result['fieldDateCreate'];
            //$reasonPage[0]['fieldNumberSupplement']=$result['fieldNumber'];
            $reasonPage[0]['REASONSBLOCK'] = $commentData; 
        }
        $result['BLOKREASONSPAGE'] = $reasonPage;
        if(empty($commentData)){
            //$result['BLOKREASONSPAGECOUNTNOT'] = array(array());
            $result['BLOKREASONSPAGECOUNTYES'] = array();
        }else{
            //$result['BLOKREASONSPAGECOUNTNOT'] = array();
            $result['BLOKREASONSPAGECOUNTYES'] = array(array());
        }        
        return $result;
    }

    /**
     *
     * @return string - имя уведомления
     */
    public function getNotifyName()
    {
        return 'Уведомление об отказе';
    }

    /**
     *
     * @return type - регистрационный номер уведомления
     */
    public function getRegNumber()
    {
        $result = parent::getRegNumber();
        return $result . '/о';
    }

    /**
     * возвращает ид шаблона
     * (вместо вызова isga_NotificationTemplates)
     *
     * @return string
     */
    public function getNotifyTemplateId(){
        return '5119B0BF-9DDF-4A6C-B9FE-B622001A2443';
    }
}