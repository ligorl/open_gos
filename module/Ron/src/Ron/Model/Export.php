<?php



namespace Ron\Model;


class Export
{

    public function getDayOfDate($dateStrInEn)
    {
        return date('d', strtotime($dateStrInEn));
    }

    public function getYearOfDate($dateStrInEn)
    {
        return date('Y', strtotime($dateStrInEn));
    }

    public function getMountOfDate($dateStrInEn)
    {
        $months = array(
            '01' => 'января',
            '02' => 'февраля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря'
        );
        return $months[date('m', strtotime($dateStrInEn))];
    }

    /**
     * заготовка , возращает имя шаблока.
     *
     * @return string
     */
    public function getTemplateName()
    {
        return '';
    }

    /**
     * заготовка , возращает имя выходного файла.
     *
     * @return string
     */
    public function getDocumentName()
    {
        return '';
    }
}