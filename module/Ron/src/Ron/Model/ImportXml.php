<?php

namespace Ron\Model;

class ImportXml
{

    const DELETE_FILE_EXCEPTION_CODE = 2;

    /**
     * имя файла на диске
     * @var string
     */
    private $file_name;
    /**
     * содержимое файла
     * @var string
     */
    private $file_data;
    /**
     * массив с различиями
     * @var array
     */
    private $data_diff = array();
    /**
     * путь к папке для загрузки xml-документов
     * @var string
     */
    private static $upload_path = '/public/uploads/xml/';
    /**
     *
     * @var array
     */
    private static $spec_keys = array('institution', 'OP', 'SocAkkrData');
    /**
     * объект Заявки
     * @var \Model\Entities\isgaApplications
     */
    private $currentApplication;
    /**
     * Массиивы связок
     * @var array
     */
    public static $XML_DATA = array(
        'application_type'   => 'Тип',
        'application_reason' => 'Код',
        'Barcode'            => 'Штрих-код',
        'DateCreate'         => 'Дата создания',
        'CertificateData'    => 'Реквизиты лицензии',
        'GosTayna'           => 'Реквизиты лицензии на проведение работ с использованием сведений, составляющих государственную тайну',
        'informationMe'      => 'Информация',
        'Insts_reorg'        => 'Наименование реорганизованной организации',
        'institutions'       => 'Филиалы',
    );
    public static $CertificateData = array(
        'RegNumber'    => 'Регистрационный номер',
        'DateIssue'    => 'Дата выдачи',
        'DateEnd'      => 'Дата окончания',
        'SerialNumber' => 'Серия бланка',
        'FormNumber'   => 'Номер бланка',
    );
    public static $GosTayna = array(
        'RegNumber'    => 'Регистрационный номер',
        'SiriesNumber' => 'Серийный номер',
        'DateReg'      => 'Дата регистрации',
        'DateEnd'      => 'Дата окончания действия',
    );
    public static $informationMe = array(
        'Persona' => 'Имя',
        'Number'  => 'Номер',
        'Email'   => 'Email',
    );
    public static $Insts_reorg = array(
        'xxx' => 'xxx',
    );
    public static $institutions = array(
        'institution' => array(
            'FullName'        => 'Полное наименование организации',
            'ShortName'       => 'Сокращенное наименование',
            'OrgForm'         => 'Организационно-правовая форма',
            'ScholType'       => 'Тип организации',
            'ScholKind'       => 'Вид организации',
            'UrAddress'  => array(
                'Country'   => 'Страна',
                'Region'    => 'Регион',
                'PostIndex' => 'Индекс',
                'District'  => 'Область',
                'TownName'  => 'Город',
                'Address'   => 'Адрес',
                'Phones'    => 'Телефон',
                'Faxes'     => 'Факс',
                'Mails'     => 'Email',
            ),
            'Requisites'      => array(
                'Kpp'       => 'КПП',
                'GosRegNum' => 'ОГРН',
                'Inn'       => 'ИНН',
            ),
            'ApplicantPerson' => array(
                'Fio'            => 'ФИО',
                'ChargePosition' => 'Должность',
            ),
            'InstitutionDir'  => array(
                'Fio'            => 'ФИО',
                'ChargePosition' => 'Должность',
            ),
            'SocAkkrs'        => array()
        )
    );
    public static $OPs = array(
        'OP' => array(
            'yearStartEdu'        => 'Год начала реализации ОП',
            'termEdu'             => 'Срок получения образования',
            'fullTimeCount'       => 'Очная форма обучения',
            'correspondenceCount' => 'Очно-заочная форма',
            'partTimeCount'       => 'Заочная форма',
            'familyCount'         => 'Семейное образование',
            'networkForm'         => 'Использование сетевой формы реализации ОП',
            'remoteTechnology'    => 'Применение дистанционных технологий обучения',
            'publicAccreditation' => 'Наличие общественной аккредитации',
        )
    );

    public function __construct()
    {
        self::$upload_path = $_SERVER['DOCUMENT_ROOT'] . self::$upload_path;
    }

    public function setFileName($file_name, $translit = false)
    {
        $name = substr($file_name, 0, strripos($file_name, '.'));
        if ($translit) {
            $name = md5($this->translit('', $name) . '_' . md5(time()));
        }
        $this->file_name = $name . '.xml';
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function getFulFilelName()
    {
        return self::$upload_path . $this->getFileName();
    }

    public function getFileData($replace = false)
    {
        if (!$this->isSetFileData()) {
            $this->setFileData();
        }
        return ($replace) ? $this->getReplaceFileData() : $this->file_data;
    }

    public function load()
    {
        if (empty($_FILES) || !array_key_exists('doc_xml', $_FILES)) {
            throw new \Exception('Файл отсутствует');
        }
        $one_file = $_FILES['doc_xml'];
        error_log(print_r($one_file, true));
        if ($one_file['error'] != 0) {
            throw new \Exception('Ошибка загрузки файла');
        }
        if ($one_file['size'] <= 0) {
            throw new \Exception('Не верный размер файла');
        }
        if ($one_file['size'] > 10 * 1024 * 1024) {
            throw new \Exception('Размер файла превышает 10 Мб');
        }
        $allowed = array("xml");
        $mas = explode('.', $one_file['name']);
        $ext = end($mas);
        if (!in_array(mb_strtolower($ext), $allowed)) {
            throw new \Exception('Недопустимый формат файла');
        }
        $this->setFileName($one_file['name'], true);
        if (!move_uploaded_file($one_file['tmp_name'], $this->getFulFilelName())) {
            throw new \Exception('Ошибка сохранения файла');
        }
        // TODO: проверка подписан ли докумен. А также принадлежит ли он isga
        $file_data = $this->getFileData();
        if (false) {
            throw new \Exception('Файл не подписан', self::DELETE_FILE_EXCEPTION_CODE);
        }
        if (false) {
            throw new \Exception('Файл подписан не верно, так как не пренадлежит организации', self::DELETE_FILE_EXCEPTION_CODE);
        }
        return $this->getFileName();
    }

    public function deleteFile()
    {
        unlink($this->getFulFilelName());
    }

    public function parse()
    {
        $xml = \simplexml_load_string($this->getFileData(true));
        // основные данные
        $form_data = $this->parseNode($xml->xpath('//FormData')[0]);
        // документы
        $applied_documents = array();
        $applied_documents['AppliedDocuments']['smev:AppliedDocuments'] =
            $this->parseNode($xml->xpath('//AppliedDocuments')[0], 'smev:');
        $res = array_merge($form_data, $applied_documents);
        //file_put_contents('D:\1.txt', var_export($res, true));
        return $res;
    }

    public function isValidStructure($data_diff)
    {
        $mas = array('SystemId', 'Id', 'code');
        $iter = new \RecursiveArrayIterator($data_diff);
        foreach (new \RecursiveIteratorIterator($iter) as $key => $value) {
            if ($key && in_array($key, $mas)) {
                return false;
            }
        }
        return true;
    }

    public function getApplicationID()
    {
        $mas = array();
        preg_match('/<SystemId>([^<> ]+)<\/SystemId>/s', $this->getFileData(), $mas);
        return array_key_exists(1, $mas) ? $mas[1] : '';
    }

    public function getApplicationFullInfo()
    {
        $applicationsClass = new \Model\Gateway\EntitiesTable('isga_Applications');
        $application = $applicationsClass->getEntityById($this->getApplicationID());
        if (!$application) {
            $this->deleteFile();
            return false;
        }
        return $application;
    }

    public function getOldData($data)
    {
        $_SERVER['HTTP_HOST'] = \mb_ereg_replace ('f11ron', 'f11', $_SERVER['HTTP_HOST']);
        $xml = ExportXml::getXml($data['SystemId']);
        $form_data = $xml->getFormData();
        $applied_documents['AppliedDocuments'] = $xml->addAttachment();
        $res = array_merge($form_data, $applied_documents);
        //file_put_contents('D:\2.txt', var_export($res, true));
        return $res;
    }

    public function diff($data_old, $data_new)
    {
        foreach ($data_old as $key => $value) {
            if (is_array($value)) {
                if (array_key_exists($key, $data_new)) {
                    $data = $this->diff($value, $data_new[$key]);
                    if (empty($data)) {
                        unset($data_old[$key]);
                    } else {
                        $data_old[$key] = $data;
                    }
                }
            } else {
                if (@$data_old[$key] == @$data_new[$key]) {
                    unset($data_old[$key]);
                }
            }
        }
        return $data_old;
    }

    public function setDataDiff($data)
    {
        $this->data_diff = $data;
    }
    
    public function getDataDiff()
    {
        return $this->data_diff;
    }

    public function initCurrentApplication($data_new)
    {
        $applicationsClass  = new \Model\Gateway\EntitiesTable('isga_Applications');
        return $this->currentApplication = $applicationsClass->getEntityById($data_new['SystemId']);
    }

    public function getCurrentApplication()
    {
        return $this->currentApplication;
    }

    public function editApplicationData()
    {
        $mas = array('application_type', 'application_reason', 'Barcode', 'DateCreate');
        $mas = array_flip($mas);
        $edit_data = array_intersect_key($this->getDataDiff(), $mas);
        if ($edit_data) {
            $this->eidtObject($this->currentApplication, $edit_data);
        }
    }

    public function editCertificateData()
    {
        $data_diff = $this->getDataDiff();
        if (!array_key_exists('CertificateData', $data_diff)) {
            return false;
        }
        $edit_data = array_intersect_key($data_diff['CertificateData'], self::$CertificateData);
        if ($edit_data) {
            $certificate = $this->currentApplication->getLinkedItem('isga_Certificates', 
                array('Id' => $this->currentApplication->getField("fk_isgaPreviousCertificate")));
            $this->eidtObject($certificate, $edit_data);
        }
    }

    public function editMoreInfoData()
    {
        $data_diff = $this->getDataDiff();
        $GosTayna = $informationMe = array();
        if (array_key_exists('GosTayna', $data_diff)) {
            $GosTayna = array_intersect_key($data_diff['GosTayna'], self::$GosTayna);
        }
        if (array_key_exists('informationMe', $data_diff)) {
            $informationMe = array_intersect_key($data_diff['informationMe'], self::$informationMe);
        }
        $edit_data = array_merge($GosTayna, $informationMe);
        if ($edit_data) {
            $moreInfo = $this->currentApplication->getLinkedItem('isga_ApplicationMoreInfo',
                array('fk_isgaApplication' => $this->currentApplication->getField("Id")));
            $this->eidtObject($moreInfo, $edit_data);
        }
    }

    public function editInstsReorgData()
    {
    }

    public function editBranchData($data_new)
    {
        $data_diff = $this->getDataDiff();
        if (!array_key_exists('institutions', $data_diff)) {
            return false;
        }
        $skip = array('OrgForm' => 1, 'ScholType' => 1, 'ScholKind' => 1, 'Country' => 1, 'Region' => 1);
        foreach ($data_diff['institutions']['institution'] as $ins_key => $institution) {
            $cdata_new = ((array_key_exists($ins_key, $data_new['institutions']['institution'])) ?
                $data_new['institutions']['institution'][$ins_key] : array());
            $edit_data = array_intersect_key($institution, self::$institutions['institution']);
            foreach ($institution as $key => $value) {
                if (is_array($value)) {
                    unset($edit_data[$key]);
                    if ($key == 'OPs') {
                        continue;
                    }
                    $edit_data = array_merge(
                        $edit_data,
                        array_intersect_key($value, self::$institutions['institution'][$key])
                    );
                }
            }
            $edit_data = array_diff_key($edit_data, $skip);
            if ($edit_data) {
                $organ = $this->currentApplication->getLinkedItem('eiis_EducationalOrganizations',
                    array('Id' => $cdata_new['code']));
                $this->eidtObject($organ, $edit_data);
            }

            // редактирование образовательных программ
            $this->editEductionProgramData($institution, $cdata_new);
        }
    }

    public function editEductionProgramData($institution, $data_new)
    {
        if (!array_key_exists('OPs', $institution)) {
            return false;
        }

        $mas = array('networkForm', 'remoteTechnology', 'publicAccreditation');
        foreach ($institution['OPs']['OP'] as $op_key => $op) {
            $edit_data = array_intersect_key($op, self::$OPs['OP']);
            foreach ($mas as $key) {
                if (array_key_exists($key, $edit_data)) {
                    $edit_data[$key] = ($edit_data[$key] === 'Да') ? 1 : 0;
                }
            }
            if ($edit_data) {
                $cdata_new = ((array_key_exists($op_key, $data_new['OPs']['OP'])) ?
                      $data_new['OPs']['OP'][$op_key] : array());
                $program = $this->currentApplication->getLinkedItem('isga_ApplicationPrograms',
                   array('Id' => $cdata_new['SystemId']));
                $this->eidtObject($program, $edit_data);
            }
        }
    }

    private function isSetFileData()
    {
        return !empty($this->file_data);
    }

    private function setFileData()
    {
        $this->file_data = \file_get_contents($this->getFulFilelName());
    }

    private function getReplaceFileData()
    {
        return \mb_ereg_replace('smev:', '', $this->file_data);
    }

    private function parseNode($data, $prefix_key = null)
    {
        if ($data instanceof \SimpleXMLElement) {
            $vars = get_object_vars($data);
            if (!empty($vars)) {
                $data = $vars;
            }
        }
        if (!empty($data) && is_array($data)) {
            $vars = array();
            foreach ($data as $key => $value) {
                if ($key === '@attributes') {
                    $key = '_atribute';
                }
                if ($prefix_key && !is_int($key)) {
                    $key = $prefix_key . $key;
                }
                $vars[$key] = $this->parseNode($value, $prefix_key);
            }
            if (in_array($key, self::$spec_keys) && !array_key_exists(0, $vars[$key])) {
                $vars[$key] = array($vars[$key]);
            }
            return $vars;
        }
        return (string) $data;
    }

    private function eidtObject($object, $edit_data)
    {
        if (!empty($object)) {
            $object->exchangeArray($edit_data);
            $res = $object->save();
        }
    }

    private function translit($url, $title)
    {
        if ($url == '') {
            $url = $title;
        }
        $space  = '-';
        $transl = array(
            'а'  => 'a', 'б'  => 'b', 'в'  => 'v', 'г'  => 'g', 'д'  => 'd', 'е'  => 'e', 'ё'  => 'e', 'ж'  => 'zh',
            'з'  => 'z', 'и'  => 'i', 'й'  => 'j', 'к'  => 'k', 'л'  => 'l', 'м'  => 'm', 'н'  => 'n',
            'о'  => 'o', 'п'  => 'p', 'р'  => 'r', 'с'  => 's', 'т'  => 't', 'у'  => 'u', 'ф'  => 'f', 'х'  => 'h',
            'ц'  => 'c', 'ч'  => 'ch', 'ш'  => 'sh', 'щ'  => 'sh', 'ъ'  => $space, 'ы'  => 'y', 'ь'  => $space, 'э'  => 'e', 'ю'  => 'yu', 'я'  => 'ya',
            'А'  => 'a', 'Б'  => 'b', 'В'  => 'v', 'Г'  => 'g', 'Д'  => 'd', 'Е'  => 'e', 'Ё'  => 'e', 'Ж'  => 'zh',
            'З'  => 'z', 'И'  => 'i', 'Й'  => 'j', 'К'  => 'k', 'Л'  => 'l', 'М'  => 'm', 'Н'  => 'n',
            'О'  => 'o', 'П'  => 'p', 'Р'  => 'r', 'С'  => 's', 'Т'  => 't', 'У'  => 'u', 'Ф'  => 'f', 'Х'  => 'h',
            'Ц'  => 'c', 'Ч'  => 'ch', 'Ш'  => 'sh', 'Щ'  => 'sh', 'Ъ'  => $space, 'Ы'  => 'y', 'Ь'  => $space, 'Э'  => 'e', 'Ю'  => 'yu', 'Я'  => 'ya',
            ' '  => $space, '_'  => $space, '`'  => $space, '~'  => $space, '!'  => $space, '@'  => $space,
            '#'  => $space, '$'  => $space, '%'  => $space, '^'  => $space, '&'  => $space, '*'  => $space,
            '('  => $space, ')'  => $space, '-'  => $space, '\=' => $space, '+'  => $space, '['  => $space,
            ']'  => $space, '\\' => $space, '|'  => $space, '/'  => $space, '.'  => $space, ','  => $space,
            '{'  => $space, '}'  => $space, '\'' => $space, '"'  => $space, ';'  => $space, ':'  => $space,
            '?'  => $space, '<'  => $space, '>'  => $space, '№'  => $space);

        return strtr($url, $transl);
    }

}