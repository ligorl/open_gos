<?php

namespace Ron\Model;

class ExportNotify extends Export{
    //private $applicationClass=null;
    private $applicationId;
    private $applicationCurrent=null;
    protected $monthArray = array('год', "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря");
    protected $resultArray = array();
    private $mainOrg=null;
    //protected $moreInfo;
    protected $user;
    private $notifyClass=null;
    private $notifyCurrent=null;
    private $applicationDocumentGroup=null;
    public static $notifyTypes=array('positiv', 'negativ','denied','return');
    public static  $typeToDocumentGroupStatuses=array(
        'positiv'=>'1d8d2fd4-e20c-42ba-a24a-9fe0268d5f89',
        'negativ'=>'43cfbc22-4f50-4017-8d01-64be02c05069',
        'denied'=>'165e0873-02dc-4aaf-a9d0-2cd7713b67a2',
        'return'=>'1f98ed4f-60e7-46c6-81be-7e10daffa4a0',
    );

    /**
     * фабрика, определяющая, какой виж документа надо создать
     *
     * @param  $appId - Ид заявления
     * @param  $type - тип заявления - posiyiv/negativ/denied
     * @param  $user - пользователь
     */
    public static function fabricNotify($appId, $type=null, $user)
    {
        if($type===null){
            $appClass=new \Model\Entities\isgaApplications($appId);
            if(!$appClass){
                return false;
            }
            $appStatus=$appClass->getField('fk_isgaApplicationStatus');
            $statusTopType = array(
                //'0FE99A13-98BB-4235-97B2-B65819AB34AF', //Регистрация и приём заявления
                //'E4435D27-3A66-49B6-B1A6-8804AB318C01', //Заявление и документы приняты к рассмотрению по существу
                '192660B9-977F-4CBD-9E5A-279097849359'=>'positiv', //Подготовка уведомления о приеме
                '342FA3BA-27A5-421C-9FAA-3784E3E7F7A9'=>'positiv', //Уведомление о приеме подписано
                '749CFF20-1A7A-4653-8D2A-647FEC5DA250'=>'positiv', //Уведомление о приеме отправлено
                //'E6C897C1-271F-4889-8E80-126CEA16C461'=>'positiv',//Уведомление о приеме возвращено
                //'E97731DA-B144-498B-9523-6CD0A02FD990'=>'positiv',//Уведомление о приеме вручено
                '040677F0-45F2-4997-9E33-860B475C8779'=>'denied', //Подготовка уведомления об отказе
                '73647D3E-744D-40F4-8995-C92B17126269'=>'denied', //Уведомление об отказе подписано
                'FFCFD0FF-9CE4-4683-870B-3D87B420C1D3'=>'denied', //Уведомление об отказе отправлено
                '2F7A2025-FDBF-448A-8635-2AEE51227151'=>'negativ', //Подготовка уведомления о несоответствии
                'D870787A-609D-4090-91A7-1D553D1F3175'=>'negativ', //Уведомление о несоответствии подписано
                '095F14E7-CF93-4EEA-BDD3-D3EBBCC8FCC0'=>'negativ', //Уведомление о несоответствии отправлено
                '2D675622-1E14-4063-AD11-B647B836746D'=>'return', //Ожидание ответа от заявителя
                '4F4D8935-0175-4343-BC7B-154AB6E65E07'=>'return', //Подготовка уведомления о возврате
                '5A0ABA01-B219-4AE1-BD84-B1CB3281B468'=>'return', //Уведомление о возврате отправлено
                'EE0F5BD5-611F-43B1-9DDE-72187CB1167F'=>'return', //Уведомление о возврате подписано
            );
            if(isset($statusTopType[$appStatus])){
                $type=$statusTopType[$appStatus];
            }else{
                return false;
            }
        }
        $notifyType = strtolower($type);
        $notifyTypes = ExportNotify::$notifyTypes;                
        if (in_array($notifyType, $notifyTypes))     {
            $notifyType=ucfirst($notifyType);
            $className = '\Ron\Model\ExportNotify' . $notifyType;
            $notify = new $className($appId, $user);
            if (!$notify) {
                return false;
            }
            return $notify;
        }
        return false;
    }

    /**
     * Создаем заявление
     * и закидываем нужные данные для выборок
     *
     * @param type $appId
     * @param type $user
     */
    function __construct($appId, $user)
    {
        $this->applicationId=$appId;

        //$applicationType = $currentApplication->getLinkedItem(
        //    'isga_ApplicationTypes', array('Id' => $currentApplication->getField("fk_isgaApplicationType"))
        //  )->toArray();

        //$this->resultArray['templName'] = $this->getNotifyName();
        //$this->resultArray['dateCreate'] = $this->getApplicationCurrent()->getField('DateCreate');

        $this->user = $user;
        //$this->notifyClass = new \Model\Gateway\EntitiesTable('isga_ApplicationNotify');
        //$this->notifyClass = new \Model\Entities\isgaApplicationNotify();

    }


    /**
     * Возвращает Id заявления текущего
     *
     * @return type
     */
    public function getApplicationId()
    {
        return $this->applicationId;
    }

    public function getApplicationCurrent()
    {
        if ($this->applicationCurrent === null) {
            //$applicationsClass = new \Model\Gateway\EntitiesTable('isga_Applications');
            //$currentApplication = $applicationsClass->getEntityById($this->getApplicationId());
            $currentApplication = new \Model\Entities\isgaApplications($this->getApplicationId());
            $this->applicationCurrent = $currentApplication;
        }
        return $this->applicationCurrent;
    }

    public function getMainOrg()
    {
        if ($this->mainOrg === null) {
            $this->mainOrg = $this
              ->getApplicationCurrent()
              ->getEducationalOrganization();
        }
        return $this->mainOrg;
    }

    public function getNotifyCurrent()
    {
        if ($this->notifyCurrent === null) {
            $notifyClass = new \Model\Entities\isgaNotification();
            $this->notifyCurrent = $notifyClass
              ->getByAppId($this->getApplicationId(),
                $this->getNotifyTemplateId());
        }
        return $this->notifyCurrent;
    }

    public function getApplicationDocumentGroup()
    {

        if ($this->applicationDocumentGroup === null) {
            //берем табличку c с данными осформированых док-тах
            $this->applicationDocumentGroup = new \Model\Entities\isgaApplicationDocumentGroups();
            if (!$this->applicationDocumentGroup->getByApplicationId($this->getApplicationId())) {
                //нет так создадим свою
                $this->applicationDocumentGroup->createNew($this->getApplicationId());
            }
        }
        return $this->applicationDocumentGroup;
    }

    /**
     * Возращает имя заявлниея (для подписи в имени файла
     *
     * @return string
     */
    public function getNotifyName()
    {
        return '';
    }

    /**
     * формируем табличку
     *
     * @param type $tableArray - массив входных данных
     * @param type $table -
     * @param type $templateProcessor
     * @return type

    public function templateMakeTable($tableArray, $table, $templateProcessor)
    {
        if (!empty($valueArray)) {
            foreach ($table as $tableName => $firstField) {
                if (!empty($tableArray[$tableName]) && is_array($tableArray[$tableName])) {
                    $templateProcessor->cloneRow($firstField, count($tableArray[$tableName]));
                    foreach ($tableArray[$tableName] as $keyRow => $valueRow) {
                        //$valueRow['countcol'] = $keyRow + 1;
                        foreach ($valueRow as $keyFiled => $valueField) {
                            $macro = $keyFiled . '#' . (string) ($keyRow + 1);
                            $templateProcessor->setValue($macro, htmlspecialchars($valueField, ENT_COMPAT, 'UTF-8'), 1);
                        }
                    };
                } else {
                    //Стираем строки, если для них нет значений.
                    $templateProcessor->cloneRow($firstField, 0);
                }
            }
        }
        return $templateProcessor;
    }
*/

    /**
     * Заполнение полей в документе
     *
     * @param type $valueArray - массив имя поля=>значение
     * @param type $templateProcessor - класс шаблонизатора
     * @param type $count - количество производимых замен
     *
     * @return type - класс шаблонизатора
     */
    public function templateMakeValue($valueArray, $templateProcessor, $count = 1)
    {
        if (!empty($valueArray)) {
            foreach ($valueArray as $key => $val) {
                if (is_string($val) || is_numeric($val)) {
                    $templateProcessor->setValue($key, htmlspecialchars($val, ENT_COMPAT, 'UTF-8'), $count);
                }
            }
        }
        return $templateProcessor;
    }


    /**
     * Форирование документа из шаблона
     *
     * @param type $arrayData - массив данных
     * @param type $templateDocPath - полный путь к шаблону
     * @param type $outType - тип выходного документа
     *
     * @return - сформированный документ
     */
    public function templateMakeDoccument($arrayData, $templateDocPath, $outType = 'docx')
    {
        //создаем временное хранилище
        $tempDir = sys_get_temp_dir() . '/' . uniqid() . '/';
        if (!mkdir($tempDir, 0777, true)) {
            return false;
        }
        ///Берем класс шаблонизатора
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateDocPath, 2);
        //вставляем значения полей
        $templateProcessor = $this->templateMakeValue($arrayData, $templateProcessor, -1);
        //ели есть, заполняем несоответствия
        if (!empty($arrayData['REASONSBLOCK']) &&
          is_array($arrayData['REASONSBLOCK'])
        ) {
            $templateProcessor->cloneBlock('REASONSBLOCK', count($arrayData['REASONSBLOCK']));
            foreach ($arrayData['REASONSBLOCK']as $branchKey => $branchValue) {
                //вставляем значения полей
                $templateProcessor = $this->templateMakeValue(array('fieldReason' => $branchValue), $templateProcessor);
            }
        }
        //Сохраняем полученый документ и готовим к отправке
        $fileTempPath = $tempDir . 'out.docx';
        $templateProcessor->saveAs($fileTempPath);
        $result = '';
        if (file_exists($fileTempPath)) {
            $outTypes = array('pdf', 'doc', 'odt');
            if (in_array($outType, $outTypes)) {
                $s = '/usr/bin/libreoffice5.0 '
                  . ' --headless --convert-to ' . $outType . ' --nofirststartwizard '
                  . ' --nologo --norestore --invisible '
                  . ' --outdir ' . $tempDir
                  . ' ' . $fileTempPath;
                \Eo\Model\Templater::exec( $s );
                $fileTempConvert = $tempDir . 'out.' . $outType;
                if (file_exists($fileTempConvert)) {
                    $result = file_get_contents($fileTempConvert);
                    @unlink($fileTempConvert);
                }
            } else {
                $result = file_get_contents($fileTempPath);
            }
            unset($outTypes);
            @unlink($fileTempPath);
        }
        unset($fileTempPath);

        if (is_dir($tempDir)) {
            @rmdir($tempDir);
        }
        unset($tempDir);
        return $result;
    }

    /**
     * формируем массив с данными, для печати
     *
     * @return  сформированный массив
     */
    public function getData()
    {
        $result = array();
        $mainOrg=$this->getMainOrg();
        $notifyCurrent=  $this->getNotifyCurrent();
        $applicationCurrent=$this->getApplicationCurrent();
        $dataOrg = $mainOrg->getEducationalOrganizationData();

        $result['fieldName'] = $mainOrg->getField('FullName');
        $address = $mainOrg->getField('Address2');                
        if(empty($address)){
            $addressArray[] = $mainOrg->getField('LawStreet');
            $addressArray[] = $mainOrg->getField('LawHouse');
            $addressArray[] = $mainOrg->getField('LawOffice');
            $addressArray[] = $city =$mainOrg->getField('LawCity');
            $region = new \Model\Entities\eiisRegions($mainOrg->getField('fk_eiisLawRegion'));
            $region = $region->getField('Name');
            if($city!=$region){
                $addressArray[] = $region;
            }
            $addressArray[] = $mainOrg->getField('PostIndex');
            $addressArray = array_filter($addressArray);
            $address = implode(', ', $addressArray);
        }    
        if(empty($address)){
            $address = '';
        }else{
            $address .= ', Российская Федерация' ;
        }
        $result['fieldAdres'] = $address;
        $nameOrg = $dataOrg->getField('NameAccCase');
        $nameOrg = empty($nameOrg)?$mainOrg->getField('FullName'):$nameOrg ;
        $result['fieldNameOrg'] = $nameOrg;
        unset ($nameOrg);
        $nameOrgDatus = $dataOrg->getField('NameDatCase');
        $nameOrgDatus = empty($nameOrgDatus)?$mainOrg->getField('FullName'):$nameOrgDatus;
        $result['fieldNameDatus'] = $nameOrgDatus;
        unset ($nameOrgDatus);  
        
        $contactPersonArray=explode(' ',$mainOrg->getField('ChargeFio'));
        if(count($contactPersonArray)==3){
            $contactPerson=mb_substr($contactPersonArray[1],0,1,'utf-8').'. '. mb_substr(  $contactPersonArray[2],0,1,'utf-8').'. '.  $contactPersonArray[0];
        }else{
            $contactPerson=$mainOrg->getField('ChargeFio');
        }
        $result['fieldContactPerson'] =$contactPerson;
        $result['fieldDateCreate'] = date('d.m.Y', strtotime($notifyCurrent->getField('DateCreation')));
        $result['fieldNumber'] = $notifyCurrent->getField("Number");
        $appNum = $applicationCurrent->getField('RegNumber');
        $result['fieldAppNum'] = empty($appNum) ? ' ' : $appNum;
        $result['fieldAppDate'] = date('d.m.Y', strtotime($applicationCurrent->getField('DateCreate')));
        $result['fieldFIOUser'] = $this->user->getField('Name');
        $telefon = $this->user->getField('Telefon');
        $result['fieldTelUser'] = empty($telefon)?'':$telefon;
        unset($telefon);
        //$result['REASONSBLOCK'] = '';
        $reasons = $applicationCurrent->getReasons();
        $reasonsFirst = reset($reasons);
        $reasonType = $reasonsFirst->getType();
        $typeNameGenitiveCase = $reasonType->getNameGenitiveCase();
        $typeName = $typeNameGenitiveCase ;
        $typeCode =  $reasonType->getCode();
        if('Accreditation'!=$typeCode)  {       
            $typeName .= ' в связи с ';
            $i=1;
            $count= count($reasons);
            foreach ($reasons as $oneReason) {
                    if($i!=1){
                         $typeName .= ', ';
                    }                    
                    if ($i==$count && $i!=1){
                         $typeName .= 'а также ';
                    }
                    $typeName .= $oneReason->getAblativeName();
                    $i++;
                }
        }        
        //$typeName = $applicationCurrent->getTypeNameGenitiveCase();
        if (empty($typeName)) {
            $typeName = ' ';
        }
        $result['fieldTypeName'] = $typeName;

        return $result;
    }

    /**
     * Возращает новый статус документа
     *
     * @return string новый статус
     */
    public function getStatusNew()
    {
        return null;
    }

    /**
     * устанавливаем статус документу
     *
     * @return
     */
    public function setStatus()
    {
        $status = $this->getStatusNew();
        if (empty($status)) {
            return null;
        }
        return $this->applicationCurrent->setNewStatus($status);
    }

    /**
     * Создаем регистрационный номер документу
     *
     * @return string - сформированный номер документа
     */
    public function getRegNumber()
    {
        $applicationCurrent=$this->getApplicationCurrent();
        $mainOrg=$this->getMainOrg();

        $appRegNumber = $applicationCurrent->getField('RegNumber');
        $appRegNumber = empty($appRegNumber) ? (string) '' : $appRegNumber;
        //$appType = $applicationCurrent->getField('RegNumber');
        //$appType = empty($appType) ? (string) '' : $appType;
        $notitionAppType = '';
        $appType = strtolower($applicationCurrent->getTypeId());
        $appOrgType = $mainOrg->getField('fk_eiisEducationalOrganizationType');
        $appReason = $applicationCurrent->getIdsReasons();
        if ($appType == 'b30d724a-17d1-4792-8c75-8f9db2a6ce57'
          || $appType == 'eb7380cd-4b59-4f3f-bd88-34728fd58de1') {
            //заявления о полной аккредитации
            if ($appOrgType == '001587DD0F0449F8DF5C66C6A715E308') {
                $notitionAppType = 'д';
            } else {
                $notitionAppType = 'в';
            }
        };        
        if ($appType == '6c1a4a80-e1f7-4f60-940a-c19afa1951ef'
          || $appType == '711776cf-7871-4c90-9ed7-9669c0e0356f') {
            if (in_array('67f97827-57f9-4a0c-b2c7-d9806969bef6',$appReason)) {
                //заявления о переоформлении свидетельства, но на основании аккредитации ранее неакредитованных образовательных программ
                if ($appOrgType == '001587DD0F0449F8DF5C66C6A715E308') {
                    $notitionAppType = 'до';
                } else {
                    $notitionAppType = 'во';
                }
            }else{
                $notitionAppType = 'п';
            }
        }  
        $result = '06-АК-' . $appRegNumber . ' '.$notitionAppType;
        return $result;
    }

    /**
     * Вносим в базу метки о создании уведомления
     *
     * @return boolean
     */
    public function create()
    {
        //мможно, если эти статусы
        $statuses = array( '0FE99A13-98BB-4235-97B2-B65819AB34AF',
            '192660B9-977F-4CBD-9E5A-279097849359',
            '2F7A2025-FDBF-448A-8635-2AEE51227151',
            '040677F0-45F2-4997-9E33-860B475C8779',
            '2D675622-1E14-4063-AD11-B647B836746D',
        );
        //созд в базе уведомление, изменить статус заяву
        $appStatus = strtoupper($this->getApplicationCurrent()->getField('fk_isgaApplicationStatus'));
        if (empty($appStatus) && !in_array($appStatus, $statuses)) {
            return false;
        }
        $regNumer = $this->getRegNumber();
        $notifyClass=new \Model\Entities\isgaNotification();
        $res = $notifyClass->registreNotify(
          $this->getApplicationId(),
          $regNumer,
          $this->getNotifyTemplateId(),
          $this->getApplicationCurrent()->getField('RegNumber')
          );
        $this->getApplicationCurrent()->setNewStatus($this->getStatusNew());
        //изменим статус в ДокГроуп
        $this->chekkedIt();
        return $res;
    }

    /**
     * изменяем статус в ДокГроуп
     */
    public function chekkedIt()
    {

    }

    /**
     * возвращает ид шаблона
     * (вместо вызова isga_NotificationTemplates)
     *
     * @return string
     */
    public function getNotifyTemplateId(){
        return '';
    }

    public function notifySign($param)
    {
        $notify = new \Model\Entities\isgaNotification();
        return $notify->signNotify($applicationId,$this->getNotifyTemplateId());
    }

   
}