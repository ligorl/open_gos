<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ron\Model;

class MakeCertificateDuplicate extends \Ron\Model\MakeCertificate
{

    public function make()
    {
        $application = $this->getApplication();       
        $certificateOld=  $this->getCertificateCurrent();
        $orderDocument=  $this->getOrderDocument();
        $applicationId = $this->getApplication()->getId();
        $certificateNew = new \Model\Entities\isgaCertificates();
        $certificateStatus = $certificateOld->getStatusIdByCode('Draft');
        $data = $certificateOld->toArray();
        unset($data['Id']);
        unset($data['FormNumber']);
        unset($data['SerialNumber']);
        unset($data['RegNumber']);
        unset($data['DatePrint']);
        unset($data['Note']);
        unset($data['DateCheck']);
        unset($data['fk_isgaCollege']);
        unset($data['fk_isgaCheckStatus']);
        //unset($data['RaaId']);
        unset($data['SpoiledFormsCount']);
        unset($data['SpoiledFormsData']);
        unset($data['DateReadyToIssue']);
        //unset($data['DateIssue']);
        //$data['DateIssue']=date('Y-m-d');
        unset($data['DateIssueToApplicant']);
        $data['fk_oisgaApplication'] = $applicationId;
        $data['fk_isgaCertificateStatus'] = $certificateStatus;
        $data['fk_isgaOrderDocument']=$orderDocument->getId();
        $data['OrderDocumentNumber']=$orderDocument->getField('Number');
        $data['DateOrderDocumentSign']=$orderDocument->getField('DateSign');
        $data['fk_OrderDocumentKind']=$orderDocument->getField('fk_isgaOrderDocumentKind');
        //$data['DateIssue'] =  $orderDocument->getField('DateSign');
        $data['DateCreate']=date('Y-m-d');//поинтерисоватся
        //$data['DateIssue']=date('Y-m-d');
        unset($data['CertificateFile']);
        unset($data['CertificateFileName']);

        $eduOrg=$application->getEducationalOrganization();
        $data['fk_eiisEducationalOrganization'] = $eduOrg->getId();//$applicationOrganization;
        $data['fk_Region_EOAddressL'] = $eduOrg->getField('fk_eiisLawRegion');
        $data['EOFullName'] = $eduOrg->getField('FullName');
        $data['EOShortName'] = $eduOrg->getField('ShortName');
        $data['fk_EOType'] = $eduOrg->getField('fk_eiisEducationalOrganizationType');
        $data['fk_EOKind'] = $eduOrg->getField('fk_eiisEducationalOrganizationKind');
        $data['fk_EOProperty'] = $eduOrg->getField('fk_eiisEducationalOrganizationProperties');
        $data['Inn'] = $eduOrg->getField('Inn');

        $data['EOFullName'] = $eduOrg->getField('FullName');
        $data['EOShortName'] = $eduOrg->getField('ShortName');
        $data['EOAddressL'] = $eduOrg->getField('LawAddress');
        


        $certificateNew = new \Model\Entities\isgaCertificates();
        $certificateNew->exchangeArray($data);
        $certificateNew->save(true);

        //Прикрепляем распорядительный документ
        $this->bindOrderDocument($certificateNew, $orderDocument);

        $supplementNew = new \Model\Entities\isgaCertificateSupplements();
        $supplementStatusId = $supplementNew->getStatusIdByCode('Draft');
        $suplementsOld = $certificateOld->getSupplements();
        foreach ($suplementsOld as $suplementOld) {
            $data = $suplementOld->toArray();
            unset($data['Id']);
            unset($data['FormNumber']);
            unset($data['SerialNumber']);
            unset($data['DatePrint']);
            unset($data['Note']);
            unset($data['SupplementFile']);
            unset($data['SupplementFileName']);
            //$data['DateIssue']=date('Y-m-d');
            unset($data['DateIssueToApplicant']);
            unset($data['DateReadyToIssue']);

            $data['fk_isgaCertificate'] = $certificateNew->getId();
            $data['fk_isgaCertificateSupplementStatus'] = $supplementStatusId;            
            $data['fk_isgaApplication'] = $applicationId;
            //$data['fk_isgaOrderDocument'] = $applicationOrder;
            $data['DateCreate']=date('Y-m-d');

            $eduOrg=$suplementOld->getEducationalOrganization();
            if (!count($eduOrg->toArray())) {
               throw new \Exception('Не найден филиал '.$eduOrgId);
            }
            $data['fk_eiisEducationalOrganization'] = $eduOrg->getId();
            $data['EOFullName'] = $eduOrg->getField('FullName');
            $data['EOShortName'] = $eduOrg->getField('ShortName');
            $data['fk_EOType'] = $eduOrg->getField('fk_eiisEducationalOrganizationType');
            $data['fk_EOKind'] = $eduOrg->getField('fk_eiisEducationalOrganizationKind');
            $data['fk_EOProperty'] = $eduOrg->getField('fk_eiisEducationalOrganizationProperties');
            $data['EOInn'] = $eduOrg->getField('Inn');
            $data['fk_Region_L'] = $eduOrg->getField('fk_eiisLawRegion');
            $data['EOAddressL'] = $eduOrg->getField('LawAddress');
            //$dataSupplement['fk_Region_EOAddressL'] = $eduOrg->getField('Inn');

            $data['fk_isgaOrderDocument'] = $orderDocument->getId();
            $data['OrderDocumentNumber'] = $orderDocument->getField('Number');
            $data['DateOrderDocumentSign'] = $orderDocument->getField('DateSign');
            $data['fk_isgaOrderDocumentKind'] = $orderDocument->getField('fk_isgaOrderDocumentKind');
            //$data['DateIssue'] =  $orderDocument->getField('DateSign');
            
            $supplementNew = new \Model\Entities\isgaCertificateSupplements();
            $supplementNew->exchangeArray($data);
            $supplementNew->save(true);

            $programs = $suplementOld->getAccreditedPrograms();
            foreach ($programs as $programOld) {
                $data = $programOld->toArray();
                unset($data['Id']);
                $data['fk_oisgaApplication'] = $applicationId;
                $data['fk_isgaCertificateSupplement'] = $supplementNew->getId();
                $accrepetProgram = new \Model\Entities\isgaAccreditedPrograms();
                $accrepetProgram->exchangeArray($data);
                $accrepetProgram->save(true);
            }
        }
    }

}