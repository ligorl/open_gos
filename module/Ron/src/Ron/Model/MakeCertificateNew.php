<?php

namespace Ron\Model;

class MakeCertificateNew extends \Ron\Model\MakeCertificate
{

    public function make()
    {
        $applicationClass = $this->getApplication();
        //дергаем программы заявлений
        $programs = $applicationClass->getLinkedProgramsChecked();
        //пройтись по образовательным программам и раскидать по уровням образования.
        $programsBase = array(); //базовое
        $programsProfy = array(); //профессиональное
        $applicationDateCreate = $this->getApplication()->getField('DateCreate');
        $applicationOrganization = strtoupper($this->getApplication()->getField('fk_eiisEducationalOrganization'));
        $applicationId = $this->getApplication()->getId();
        $applicationOrder = $this->getApplication()->getField('fk_isgaOrderDocument');

        //$orderDocumentId=$applicationClass->getField("fk_isgaOrderDocument");
        //if (empty($orderDocumentId)) {
        //   throw new \Exception('Не указан распорядительный документ');
        //}
        //$orderDocument=new \Model\Entities\isgaOrderDocuments( $orderDocumentId);
        //if (empty($orderDocument)) {
        //   throw new \Exception('Не найден распорядительный документ');
        //}
        $orderDocument =  $this->getOrderDocument();

        $eduOrg=$applicationClass->getEducationalOrganization();
        if (empty($eduOrg)) {
           throw new \Exception('Не найдена образовательная организация');
        }
        $programsObjects = $programs->getObjectsArray();
        foreach ($programsObjects as $keyProgram => $oneProgram) {
            $isProfy = $oneProgram->isProfy();
            if ($isProfy === false) {
                $programsBase[$oneProgram->getField('fk_isgaEOBranches')][] = $oneProgram->toArray();
                //$programsArray[$keyProgram]['eduLevel'] = $oneProgram->getEduProgramLevel();
            } else if ($isProfy === true) {
                $programsProfy[$oneProgram->getField('fk_isgaEOBranches')][] = $oneProgram->toArray();
                //$programsArray[$keyProgram]['eduLevel'] = $oneProgram->getEduProgramLevel();
            }else{
                throw new \Exception('Не удалось задать уровень образования');
            }
        }
        $certificateInfo = array();
        if (count($programsBase) > 0) {
            $certificateInfo['EduPrograms'] = array(
                //'CertificateInfo'=>$this->getCertificateInfo(),
                'SupplementsInfo' => $programsBase,
            );
        }
        if (count($programsProfy) > 0) {
            $certificateInfo['EduProgramGroups'] = array(
                // 'CertificateInfo'=>$this->getCertificateInfo(),
                'SupplementsInfo' => $programsProfy,
            );
        }
        //вносим в базу запись о сертиффикатах
        foreach ($certificateInfo as $certificateKey => $certificateOne) {
            $certificate = new \Model\Entities\isgaCertificates();
            $certificateType = $certificate->getTypeIdByCode('Permanent');
            $certificateStatus = $certificate->getStatusIdByCode('Draft');
            $data = array();
            //$data['RegNumber']
            $data['DateIssue'] =  $orderDocument->getField('DateSign');
            $data['DateCreate'] = date('Y-m-d');
            $data['DateEnd'] = $this->getDateEnd($certificateKey,$data['DateIssue']);
            $data['fk_oisgaApplication'] = $applicationId;

            $data['fk_eiisEducationalOrganization'] = $eduOrg->getId();//$applicationOrganization;
            $data['fk_Region_EOAddressL'] = $eduOrg->getField('fk_eiisLawRegion');
            $data['EOFullName'] = $eduOrg->getField('FullName');
            $data['EOShortName'] = $eduOrg->getField('ShortName');
            $data['EOAddressL'] = $eduOrg->getLAddress();           
            $data['fk_EOType'] = $eduOrg->getField('fk_eiisEducationalOrganizationType');
            $data['fk_EOKind'] = $eduOrg->getField('fk_eiisEducationalOrganizationKind');
            $data['fk_EOProperty'] = $eduOrg->getField('fk_eiisEducationalOrganizationProperties');
            $data['Inn'] = $eduOrg->getField('Inn');

            $data['ControlOrganHeadFullName'] = $this->getControlOrganHeadFullName();
            $data['ControlOrganHeadPost'] = $this->getControlOrganHeadPost();
            $data['fk_eiisControlOrgan'] = 'BFB891F1F234F61C11835CDEAF8E9CA0';            

            $data['fk_isgaOrderDocument'] = $orderDocument->getId();//$applicationOrder;
            $data['OrderDocumentNumber'] = $orderDocument->getField('Number');
            $data['DateOrderDocumentSign'] = $orderDocument->getField('DateSign');
            $data['fk_OrderDocumentKind'] = $orderDocument->getField('fk_isgaOrderDocumentKind');

            $data['fk_isgaCertificateStatus'] = $certificateStatus;
            $data['fk_isgaCertificateType'] = $certificateType;            
            
            //$data['EOFullName'] = $eduOrg->getField('FullName');
            //$data['EOShortName'] = $eduOrg->getField('ShortName');
            
            
            //Добавляем
            $certificate->exchangeArray($data);
            $certificate->save(true);

            //Прикрепляем распорядительный документ
            $this->bindOrderDocument($certificate, $orderDocument);

            //Формируем приложения
            $certificateSupplementsArray = array();
            $supplementCountert = 1;
            //Сначало главный филиал
            if (isset($certificateOne['SupplementsInfo'][$applicationOrganization])&&count($certificateOne['SupplementsInfo'][$applicationOrganization]) > 0) {
                $certificateSupplementsArray[$supplementCountert] =
                  $this->makeSupplements($certificateOne['SupplementsInfo'][$applicationOrganization],
                       $applicationOrganization,
                       $certificate,
                       $certificateKey,
                       $supplementCountert
                  );
                $supplementCountert = 2;
            }

//Теперь филиалы, если остались
            //if(count($certificateOne['SupplementsInfo'])>=$supplementCountert){
                foreach ($certificateOne['SupplementsInfo'] as $supplementKey => $supplementOne) {
                    if ($supplementKey == $applicationOrganization) {
                        continue;
                    }
                    $certificateSupplementsArray[$supplementCountert] =
                        $this->makeSupplements($supplementOne,
                           $supplementKey,
                          $certificate,
                           $certificateKey,
                           $supplementCountert
                          );
                    ++$supplementCountert;
                }
            //}
        }
    }
    
/*
    public function makeSupplements($supplementsInfo, $eduOrgId, $certificate, $accreditationKindCode, $number = 0)
    {
        $dataSupplement = array();
        //$applicationDateCreate = $this->getApplication()->getField('DateCreate');
        $applicationId = $this->getApplication()->getId();
        //$applicationOrder = $this->getApplication()->getField('fk_isgaOrderDocument');

        //$orderDocumentId=  $this->getApplication()->getField("fk_isgaOrderDocument");
        //$orderDocument=new \Model\Entities\isgaOrderDocuments( $orderDocumentId);
        $orderDocument=  $this->getOrderDocument();

        $applicationLicenseId = $this->getApplication()->getField('fk_eiisLicense');
        $license = new \Model\Entities\eiisLicenses($applicationLicenseId);
        $licenseDateEnd = $license->getField('DateEnd');
        $crtificateDateEnd = $certificate->getField('DateEnd');

        $eduOrg= new \Model\Entities\eiisEducationalOrganizations($eduOrgId);
        if (!count($eduOrg->toArray())) {
           throw new \Exception('Не найден филиал '.$eduOrgId);
        }

        $certificateSupplements = new \Model\Entities\isgaCertificateSupplements();
        $certificateSupplementsStatusId = $certificateSupplements->getStatusIdByCode('Draft');
        $certificateSupplementsKindId = $certificateSupplements->getKindIdByCode($accreditationKindCode);

        //$dataSupplement['fk_oisgaApplication']=$applicationId;
        $dataSupplement = array();
        $dataSupplement['fk_isgaCertificate'] = $certificate->getId();
        $dataSupplement['fk_isgaCertificateSupplementStatus'] = $certificateSupplementsStatusId;
        $dataSupplement['DateIssue'] = $orderDocument->getDate('DateSign');;
        $dataSupplement['DateCreate'] = date('Y-m-d');
        $dataSupplement['fk_isgaApplication'] = $applicationId;
        $dataSupplement['fk_isgaAccreditationKind'] = $certificateSupplementsKindId;
        //$dataSupplement['DateReadyToIssue'] = $applicationDateCreate;

        $dataSupplement['fk_isgaOrderDocument'] = $orderDocument->getId();
        $dataSupplement['OrderDocumentNumber'] = $orderDocument->getField('Number');
        $dataSupplement['DateOrderDocumentSign'] = $orderDocument->getField('DateSign');;
        $dataSupplement['fk_isgaOrderDocumentKind'] = $orderDocument->getField('fk_isgaOrderDocumentKind');;

        $dataSupplement['fk_eiisEducationalOrganization'] = $eduOrg->getId();
        $dataSupplement['EOFullName'] = $eduOrg->getField('FullName');
        $dataSupplement['EOShortName'] = $eduOrg->getField('ShortName');
        $dataSupplement['fk_EOType'] = $eduOrg->getField('fk_eiisEducationalOrganizationType');
        $dataSupplement['fk_EOKind'] = $eduOrg->getField('fk_eiisEducationalOrganizationKind');
        $dataSupplement['fk_EOProperty'] = $eduOrg->getField('fk_eiisEducationalOrganizationProperties');
        $dataSupplement['EOInn'] = $eduOrg->getField('Inn');
        $dataSupplement['fk_Region_L'] = $eduOrg->getField('fk_eiisLawRegion');
        $dataSupplement['EOAddressL'] = $eduOrg->getField('LawAddress');
        //$dataSupplement['fk_Region_EOAddressL'] = $eduOrg->getField('Inn');

        $dataSupplement['Number'] = (string) $number; //(($supplementKey==$applicationOrganization)?'1':(string)($i++));

        $certificateSupplements->exchangeArray($dataSupplement);
        $result = $certificateSupplements->save(true);

        foreach ($supplementsInfo as $accreditedKey => $accriditedProgram) {
            $licensedProgram = new \Model\Entities\eiisLicensedPrograms($accriditedProgram['fk_eiisLicensedPrograms']);
            $eduProgram=$licensedProgram->getEduProgram();

            $dataProgram = array();
            $dataProgram['fk_oisgaApplication'] = $applicationId;
            $dataProgram['fk_eiisLicensedProgram'] =  $licensedProgram->getId();
            $dataProgram['Name'] = $licensedProgram->getField('Name');
            $dataProgram['EduNormativePeriod'] = $accriditedProgram['termEdu'];
            $dataProgram['Code'] = $licensedProgram->getField('Code');
            $dataProgram['StartYear'] = $accriditedProgram['yearStartEdu'];
            $dataProgram['FullTimeFormGraduatesNumber'] = $accriditedProgram['fullTimeCount'];
            $dataProgram['PartTimeFormGraduatesNumber'] = $accriditedProgram['partTimeCount'];
            $dataProgram['ExtramuralTimeFormGraduatesNumber'] = $accriditedProgram['correspondenceCount'];
            $dataProgram['fk_isgaCertificateSupplement'] = $certificateSupplements->getId();
            $dataProgram['DateLicenseEnd'] = $licenseDateEnd;

            if(!empty($eduProgram)){
                $dataProgram['fk_isgaGroupSpeciality'] = $eduProgram->getField('fk_eiisGS');
                $dataProgram['QualificationGrade'] = $eduProgram->getField('QualificationGrade');
                $dataProgram['fk_eiisEduProgramType'] = $eduProgram->getField('fk_eiisEduProgramType');
                $dataProgram['fk_isgaEduStandard'] = $eduProgram->getField('fk_isgaEduStandard');
                $dataProgram['fk_eiisEduProgram'] = $eduProgram->getId();
                //$dataProgram['LicenseIsUnlimited'] =
                $dataProgram['fk_isgaQualification'] = $eduProgram->getField('fk_isgaQualification');
                $dataProgram['AdditionalInfo']=$eduProgram->getField('EduNote');
            } else{
                //$dataProgram['fk_isgaGroupSpeciality'] = $eduProgram->getField('fk_eiisGS');
                //$dataProgram['QualificationGrade'] = $eduProgram->getField('QualificationGrade');
                //$dataProgram['fk_eiisEduProgramType'] = $eduProgram->getField('fk_eiisEduProgramType');
                //$dataProgram['fk_isgaEduStandard'] = $eduProgram->getField('fk_isgaEduStandard');
                //$dataProgram['fk_eiisEduProgram'] = $eduProgram->getId();
                //$dataProgram['fk_isgaQualification'] = $eduProgram->getField('fk_isgaQualification');
                //$dataProgram['AdditionalInfo']=$eduProgram->getField('EduNote');
            }


            $dataProgram['DateCertificateEnd'] = $certificate->getField('DateEnd');

            $dataProgram['fk_eiisEducationalOrganization'] = $eduOrg->getId();
            $dataProgram['DateCreate'] = date('Y-m-d');
            //$dataProgram['LicenseIsUnlimited']=$license->getField($fieldName)
            $dataProgram['OksoCode']=  $licensedProgram->getField('OKSO');

            //$dataProgram['DateLicenseEnd'] = $crtificateDateEnd;
            $accrepetProgram = new \Model\Entities\isgaAccreditedPrograms();
            $accrepetProgram->exchangeArray($dataProgram);
            $accrepetProgram->save(true);
        }
        return $dataSupplement;
    }
*/
    public function getDateEnd($param,$date)
    {
        if(empty($date)){
            $date=date('Y-m-d');
        }       
        $dateTime=strtotime($date);
        if ($param == 'EduProgramGroups') {
            return date('Y-m-d', strtotime('+6 year', $dateTime));
        }
        if ($param == 'EduPrograms') {
            return date('Y-m-d', strtotime('+12 year', $dateTime));
        }
    }

    /*
      public function getData(){
      //дергаем программы заявлений
      //$programs= new \Model\Entities\isgaApplicationPrograms();
      $programs = $applicationClass->getLinkedProgramsChecked();
      //$branchas=$applicationClass->getLinkedBranches();
      //пройтись по образовательным программам и раскидать по уровням образования.
      $programsObjects = $programs->getObjectsArray();
      $programsBase=array();//базовое
      $programsProfy=array();//профессиональное
      foreach ($programsObjects as $keyProgram => $oneProgram) {
      if($oneProgram->isProfy()===false){
      $programsBase[]=$oneProgram->toArray();
      //$programsArray[$keyProgram]['eduLevel'] = $oneProgram->getEduProgramLevel();
      }
      if($oneProgram->isProfy()===true){
      $programsProfy[]=$oneProgram->toArray();
      //$programsArray[$keyProgram]['eduLevel'] = $oneProgram->getEduProgramLevel();
      }
      }


      $certificate=array();
      if (count($programsBase[]>0)){
      $certificate['Info']='';
      }
      $certificate=array();
      $certificate[0][];



      }

     */
}