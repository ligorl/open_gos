<?php

namespace Ron\Model;

class MakeCertificate
{

    /**возрщает формирователь заявления
     * для
     *
     * на полную акредитацию
     * на переоформление
     *      доакредитация
     *      переименование
     *          частичное
     *          полное
     *      переадресация
     *          полная
     *          частичная
     *      добавлене программ с новыми кодами
     *
     * временное свидетельство
     * дупликат свидетельства
     *
     *
     * @param type $applicationId
     * @return \Ron\Model\MakeCertificateTemp|\Ron\Model\MakeCertificateReAll|\Ron\Model\MakeCertificateDuplicate|\Ron\Model\MakeCertificateAdd|\Ron\Model\MakeCertificateNew
     * @throws \Exception
     */
    public static function certificateFabric($applicationId)
    {
        $applicationClass = new \Model\Entities\isgaApplications($applicationId);
        if (!$applicationClass) {
            throw new \Exception('Не найдено такое заявление');
        }
        $applicationType = $applicationClass->getTypeCode();
        $applicationReasons=$applicationClass->getReasonCodeArray();
        //$applicationReason = $applicationClass->getReasonCode();
        if ($applicationType == 'Accreditation') {
            return new \Ron\Model\MakeCertificateNew($applicationClass);
        } elseif ($applicationType == 'Certificate' || $applicationType == 'ReAccreditation'){
            if( in_array ('ReAccreditation_AddNotAccrEduPrograms', $applicationReasons)
                ||(in_array ('ReAccreditation_ReCodeNamePrograms', $applicationReasons)
                    && count($applicationReasons)==1
                    )
            ){
                return new \Ron\Model\MakeCertificateAdd($applicationClass);
            }elseif (in_array ('ReAccreditation_ReAddress',$applicationReasons )
                ||in_array ('ReAccreditation_Rename',$applicationReasons )
                ){
                    $isGetMainOrg = self::isGetMainOrg($applicationClass);
                    if($isGetMainOrg){
                        return new \Ron\Model\MakeCertificateNew($applicationClass);
                    }else{
                        return new \Ron\Model\MakeCertificateAdd($applicationClass);
                    }
            }elseif  (in_array ('ReAccreditation_ConnectForm',$applicationReasons )
                ||in_array ('ReAccreditation_FusionForm',$applicationReasons)                
                ){
            return new \Ron\Model\MakeCertificateReorg($applicationClass);
            }
        }elseif ($applicationType == 'TempCertificate'
              || $applicationType ==  'TemporaryAccreditation'
        ) {
            return new \Ron\Model\MakeCertificateTemp($applicationClass);
        } elseif ( $applicationType == 'DublicateCertificate'
                  || $applicationType == 'DuplicateIssue'
            ) {
                return new \Ron\Model\MakeCertificateDuplicate($applicationClass);
        } 
        throw new \Exception('Не найдено такой формирователь тип заявления: '. $applicationType. ' причина/основание:'.implode(', ',$applicationReason));
               
    }

    private $application;

    public function __construct($applicationClass)
    {
        $this->application = $applicationClass;
    }

    public function getApplication(){
        return $this->application;
    }

    public function getCertificateInfo()
    {
        $certificateInfo = array();
        $certificateInfo['ApplicationRegNumber']['Value'] = $applicationClass->getField('RegNumber');
        $certificateInfo['RegNumber']['Value'] = '';
        $certificateInfo['SerialNumber']['Value'] = '';
        $certificateInfo['DateIssue']['Value'] = date('d.m.Y');
        $certificateInfo['DateEnd']['Value'] = '';
        $certificateInfo['fk_isgaCertificateType']['Value'] = '';
        $certificateTepes = new \Model\Entities\isgaCertificateTypes();
        $certificateInfo['fk_isgaCertificateType']['Variable'] = $certificateTepes->getAllArrayNameById();
        $certificateStatus = new \Model\Entities\isgaCertificateStatuses();
        $certificateInfo['fk_isgaCertificateStatus']['Variable'] = $certificateStatuses->getAllArrayNameById();
        $certificateInfo['fk_eiisControlOrgan']['Variable']['BFB891F1-F234-F61C-1183-5CDEAF8E9CA0'] = 'Федеральная служба по надзору в сфере образования и науки';
        $certificateInfo['DatePrint']['Value'] = date('d.m.Y');
    }

    public function checkDatePrint()
    {
        $application=  $this->getApplication();
        $forValid=array(
           'DatePrint'=>'не распечатано',
         );
         $valdate= new \Ron\Model\MakeCertificateValidator($application,$forValid);
         return $valdate->validate();
    }

    public function checkReadyToIssue()
    {
        $application=  $this->getApplication();
        $forValid=array(
           'SerialNumber'=>'не указана серия бланка',
           'FormNumber'=>'не указан  номер бланка',
         );
         $valdate= new \Ron\Model\MakeCertificateValidator($application,$forValid);
         return $valdate->validate();
    }

     public function checkDateIssueToApplicant()
     {
         $application=  $this->getApplication();
         $forValid=array(
           'DateIssueToApplicant'=>'нет даты выдачи',
         );
         $valdate= new \Ron\Model\MakeCertificateValidator($application,$forValid);
         return $valdate->validate();
     }
    /*
    public function getCertificateBaseInfo()
    {

        $certificateInfo = array();
        $certificateInfo['ApplicationRegNumber']['Value'] = $applicationClass->getField('RegNumber');
        $certificateInfo['RegNumber']['Value'] = '';
        $certificateInfo['SerialNumber']['Value'] = '';
        $certificateInfo['DateIssue']['Value'] = date('d.m.Y');
        $certificateInfo['DateEnd']['Value'] = '';
        $certificateInfo['fk_isgaCertificateType']['Value'] = '';
        $certificateTepes = new \Model\Entities\isgaCertificateTypes();
        $certificateInfo['fk_isgaCertificateType']['Variable'] = $certificateTepes->getAllArrayNameById();
        $certificateStatus = new \Model\Entities\isgaCertificateStatuses();
        $certificateInfo['fk_isgaCertificateStatus']['Variable'] = $certificateStatuses->getAllArrayNameById();
        $certificateInfo['fk_eiisControlOrgan']['Variable']['BFB891F1-F234-F61C-1183-5CDEAF8E9CA0'] = 'Федеральная служба по надзору в сфере образования и науки';
        $certificateInfo['DatePrint']['Value'] = date('d.m.Y');
    }
*/

     protected $orderDocumen=null;
     public function getOrderDocument()
     {
         if($this->orderDocumen===null){
            $applicationClass = $this->getApplication();
            $orderDocumentId=$applicationClass->getField("fk_isgaOrderDocument");
            if (empty($orderDocumentId)) {
               throw new \Exception('Не указан распорядительный документ');
            }
            $orderDocument=new \Model\Entities\isgaOrderDocuments( $orderDocumentId);
            if (empty($orderDocument)) {
               throw new \Exception('Не найден распорядительный документ');
            }
            $this->orderDocumen=$orderDocument;
         }
         return $this->orderDocumen;
     }

     protected $certificateCurrent=null;
     public function getCertificateCurrent()
     {
        if($this->certificateCurrent===null){
            $applicationClass = $this->getApplication();
            $certificateOldId = $applicationClass->getField('fk_isgaPreviousCertificate');
            if (empty($certificateOldId)) {
               throw new \Exception('Предыдущий сертификат не указан');
            }

            $certificateOld = new \Model\Entities\isgaCertificates($certificateOldId);
            if (!$certificateOld) {
                throw new \Exception('Предыдущий сертификат отсутствует в базе');
            }
            $this->certificateCurrent=$certificateOld;
        }
        return $this->certificateCurrent;
     }

     public function bindOrderDocument($certificate,$orderDocument=null)
     {
         if(empty($certificate)){
             return null;
         }
        if($orderDocument===null){
             $orderDocument=$this->getOrderDocument();
         }
         if(empty($orderDocument)){
             return null;
         }
            $mtmCertificateOrder = new \Model\Entities\isgamtmCertificatesOrderDocuments();
            $mtmCertificateOrder->setField('fk_isgaCertificate', $certificate->getId());
            $mtmCertificateOrder->setField('fk_isgaOrderDocument',$orderDocument->getId());
            $mtmCertificateOrder->save(true);
            return true;
     }


     public function rebuildStatusDraftToForce() {
            //меняем статус макет на  действующее
            $application = $maker->getApplication();
            $certificates = $application->getCertificates();
            //если нет созданых, то берем предыдущий
            if (!$certificates) {
                $certificates = array($application->getCertificate());
            }
            if (empty($certificates)) {
                $this->reasonsAdd('нет сертификатов');
                return;
            }
            $certificatesFirst = reset($certificates);
            $certificateStatusDraft = $certificatesFirst->getStatusIdByCode('Draft');
            $certificateStatusInForce = $certificatesFirst->getStatusIdByCode('InForce');
            $result = array();
            foreach ($certificates as $certificateOne) {
                $statusId=$certificateOne->getStatusId();
                if($statusId==$certificateStatusDraft){
                    $certificateOne->setStatusId($certificateStatusInForce);
                }
            }
    }

    static public function isGetMainOrg($application){
        if(empty($application)){
            return true;
        }
        //$application = $this->getApplication();
        $filals = $application->getFilialIds();
        If(empty($filals)){
            return true;
        }
        $applicationId = $application->getEducationalOrganization()->getId();
        $result = in_array($applicationId, $filals);
        return $result;
    }

    public function getControlOrganHeadFullName()
    {
        return 'С.С. Кравцов';
    }

    public function getControlOrganHeadPost()
    {
        return 'Руководитель';
    }

    public function makeSupplements($supplementsInfo, $eduOrgId, $certificate, $accreditationKindCode, $number = 0)
    {
        $dataSupplement = array();
        //$applicationDateCreate = $this->getApplication()->getField('DateCreate');
        $applicationId = $this->getApplication()->getId();
        //$applicationOrder = $this->getApplication()->getField('fk_isgaOrderDocument');

        //$orderDocumentId=  $this->getApplication()->getField("fk_isgaOrderDocument");
        //$orderDocument=new \Model\Entities\isgaOrderDocuments( $orderDocumentId);
        $orderDocument=  $this->getOrderDocument();

        $applicationLicenseId = $this->getApplication()->getField('fk_eiisLicense');
        $license = new \Model\Entities\eiisLicenses($applicationLicenseId);
        $licenseDateEnd = $license->getField('DateEnd');
        $crtificateDateEnd = $certificate->getField('DateEnd');

        $eduOrg= new \Model\Entities\eiisEducationalOrganizations($eduOrgId);
        if (!count($eduOrg->toArray())) {
           throw new \Exception('Не найден филиал '.$eduOrgId);
        }

        $certificateSupplements = new \Model\Entities\isgaCertificateSupplements();
        $certificateSupplementsStatusId = $certificateSupplements->getStatusIdByCode('Draft');
        $certificateSupplementsKindId = $certificateSupplements->getKindIdByCode($accreditationKindCode);

        //$dataSupplement['fk_oisgaApplication']=$applicationId;
        $dataSupplement = array();
        $dataSupplement['fk_isgaCertificate'] = $certificate->getId();
        $dataSupplement['fk_isgaCertificateSupplementStatus'] = $certificateSupplementsStatusId;
        $dataSupplement['DateIssue'] = $orderDocument->getField('DateSign');;
        $dataSupplement['DateCreate'] = date('Y-m-d');
        $dataSupplement['fk_isgaApplication'] = $applicationId;
        $dataSupplement['fk_isgaAccreditationKind'] = $certificateSupplementsKindId;
        //$dataSupplement['DateReadyToIssue'] = $applicationDateCreate;

        $dataSupplement['fk_isgaOrderDocument'] = $orderDocument->getId();
        $dataSupplement['OrderDocumentNumber'] = $orderDocument->getField('Number');
        $dataSupplement['DateOrderDocumentSign'] = $orderDocument->getField('DateSign');;
        $dataSupplement['fk_isgaOrderDocumentKind'] = $orderDocument->getField('fk_isgaOrderDocumentKind');;

        $dataSupplement['fk_eiisEducationalOrganization'] = $eduOrg->getId();
        $dataSupplement['EOFullName'] = $eduOrg->getField('FullName');
        $dataSupplement['EOShortName'] = $eduOrg->getField('ShortName');
        $dataSupplement['fk_EOType'] = $eduOrg->getField('fk_eiisEducationalOrganizationType');
        $dataSupplement['fk_EOKind'] = $eduOrg->getField('fk_eiisEducationalOrganizationKind');
        $dataSupplement['fk_EOProperty'] = $eduOrg->getField('fk_eiisEducationalOrganizationProperties');
        $dataSupplement['EOInn'] = $eduOrg->getField('Inn');
        $dataSupplement['fk_Region_L'] = $eduOrg->getField('fk_eiisLawRegion');
        $dataSupplement['EOAddressL'] = $eduOrg->getLAddress();
        //$dataSupplement['fk_Region_EOAddressL'] = $eduOrg->getField('Inn');
        $dataSupplement['ControlOrganHeadFullName'] = $this->getControlOrganHeadFullName();
        $dataSupplement['ControlOrganHeadPost'] = $this->getControlOrganHeadPost();
        $dataSupplement['fk_eiisControlOrgan'] = 'BFB891F1F234F61C11835CDEAF8E9CA0';

        $dataSupplement['Number'] = (string) $number; //(($supplementKey==$applicationOrganization)?'1':(string)($i++));

        $certificateSupplements->exchangeArray($dataSupplement);
        $result = $certificateSupplements->save(true);

        foreach ($supplementsInfo as $accreditedKey => $accriditedProgram) {
            $licensedProgram = new \Model\Entities\eiisLicensedPrograms($accriditedProgram['fk_eiisLicensedPrograms']);
            $eduProgram=$licensedProgram->getEduProgram();
            $eiisEduProgramType='';
            if(empty($eiisEduProgramType)){
                if(!empty($accriditedProgram['fk_eiisEduProgramTypes'])){
                    $eiisEduProgramType = $accriditedProgram['fk_eiisEduProgramTypes'];
                }
                
            }
            if(!empty($licensedProgram) && empty($eiisEduProgramType)){
                $eiisEduProgramType = $licensedProgram->getFieldOrSpace('fk_eiisEduProgramType');
            }
            if(!empty($eduProgram) && empty($eiisEduProgramType)){
                $eiisEduProgramType = $eduProgram->getFieldOrSpace('fk_eiisEduProgramType');
            }
            

            $dataProgram = array();
            $dataProgram['fk_oisgaApplication'] = $applicationId;
            $dataProgram['fk_eiisLicensedProgram'] =  $licensedProgram->getId();
            $dataProgram['Name'] = $licensedProgram->getField('Name');
            $dataProgram['EduNormativePeriod'] = $accriditedProgram['termEdu'];
            $dataProgram['Code'] = $licensedProgram->getField('Code');
            $dataProgram['StartYear'] = $accriditedProgram['yearStartEdu'];
            $dataProgram['FullTimeFormGraduatesNumber'] = $accriditedProgram['fullTimeCount'];
            $dataProgram['PartTimeFormGraduatesNumber'] = $accriditedProgram['partTimeCount'];
            $dataProgram['ExtramuralTimeFormGraduatesNumber'] = $accriditedProgram['correspondenceCount'];
            $dataProgram['fk_isgaCertificateSupplement'] = $certificateSupplements->getId();
            $dataProgram['DateLicenseEnd'] = $licenseDateEnd;

            if(!empty($eduProgram)){
                $dataProgram['fk_isgaGroupSpeciality'] = $eduProgram->getField('fk_eiisGS');
                $dataProgram['QualificationGrade'] = $eduProgram->getField('QualificationGrade');
                //$dataProgram['fk_eiisEduProgramType'] = $eduProgram->getField('fk_eiisEduProgramType');
                $dataProgram['fk_isgaEduStandard'] = $eduProgram->getField('fk_isgaEduStandard');
                $dataProgram['fk_eiisEduProgram'] = $eduProgram->getId();
                //$dataProgram['LicenseIsUnlimited'] =
                $dataProgram['fk_isgaQualification'] = $eduProgram->getField('fk_isgaQualification');
                $dataProgram['AdditionalInfo']=$eduProgram->getField('EduNote');
            } else{
                //$dataProgram['fk_isgaGroupSpeciality'] = $eduProgram->getField('fk_eiisGS');
                //$dataProgram['QualificationGrade'] = $eduProgram->getField('QualificationGrade');
                //if(!$licensedProgram->isEmptyField('fk_eiisEduProgramType')){
                //    $dataProgram['fk_eiisEduProgramType'] = $licensedProgram->getField('fk_eiisEduProgramType');
                //}
                //$dataProgram['fk_isgaEduStandard'] = $eduProgram->getField('fk_isgaEduStandard');
                //$dataProgram['fk_eiisEduProgram'] = $eduProgram->getId();
                //$dataProgram['fk_isgaQualification'] = $eduProgram->getField('fk_isgaQualification');
                //$dataProgram['AdditionalInfo']=$eduProgram->getField('EduNote');
            }
            $dataProgram['fk_eiisEduProgramType'] = $eiisEduProgramType;


            $dataProgram['DateCertificateEnd'] = $certificate->getField('DateEnd');

            $dataProgram['fk_eiisEducationalOrganization'] = $eduOrg->getId();
            $dataProgram['DateCreate'] = date('Y-m-d');
            //$dataProgram['LicenseIsUnlimited']=$license->getField($fieldName)
            $dataProgram['OksoCode']=  $licensedProgram->getField('OKSO');

            //$dataProgram['DateLicenseEnd'] = $crtificateDateEnd;
            $accrepetProgram = new \Model\Entities\isgaAccreditedPrograms();
            $accrepetProgram->exchangeArray($dataProgram);
            $accrepetProgram->save(true);
        }
        return $dataSupplement;
    }
/*
    public function getDateEnd($param)
    {

    }
*/

    public function setDateFieldIfEmpty($certificate,$fieldName) {
        $date=$certificate->getDate($fieldName);
        if(empty($date)){
            $certificate->setField($fieldName,date('Y-m-d'));
            $certificate->save();
        }
    }
    
    public function setDateIfNot($fieldName)
    {
        $application = $this->application;
        $certificates = $application->getCertificates();
        if (!$certificates) {
            $this->reasonsAdd('нет сертификатов');
            return;
        }
        foreach ($certificates as $certificateOne) {            
            $this->setDateFieldIfEmpty($certificateOne ,$fieldName);
            $suplements = $certificateOne->getSupplements();
            foreach ($suplements as $suplementOne) {                
                $this->setDateFieldIfEmpty($suplementOne ,$fieldName);
            }
        }
    }

}
