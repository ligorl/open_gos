<?php

namespace Ron\Model;


class ExportCertificateSupplement extends \Ron\Model\Export
{

    private $supplement;
    private $supplementId;
    private $applicationId;
    private $application=null;

    public function __construct($supplement,$applicationId)
    {
        $this->supplement=$supplement;
        $this->applicationId=$applicationId;
    }

    public function getSupplement()
    {
        return $this->supplement;
    }

    public function getSupplementId()
    {
        return $this->supplement->getField('Id');
    }

    public function getApplication() {
        if($this->application===null){
            $appId=  $this->applicationId;
            $this->application = new \Model\Entities\isgaApplications($appId);
        }
        return $this->application;
        
    }

    public function getData()
    {
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors',1);
        //дергнуть организацию
        $eduOrg=$this->getSupplement()->getEducationalOrganization();
         //дергнуть сам сертификат
        $certificate=$this->getSupplement()->getCertificate();
        $result = array();
        $dateIssue = $certificate->getField('DateIssue');

        $result['fieldDay'] = $this->getDayOfDate($dateIssue);
        $result['fieldMount'] = $this->getMountOfDate($dateIssue);
        $result['fieldYear'] = $this->getYearOfDate($dateIssue);
        $eduData = $eduOrg->getEducationalOrganizationData();
        //if (!empty($eduData)) {
        //    $eduName = $eduData->getField('NameDatCase');
        //}
        if (empty($eduName)) {
            $eduName = $eduOrg->getField('FullName');
        }
        $result['fieldNameDatus'] = $eduName; //Должно быть в дательноам падеже
        unset($eduName);
        $result['fieldAdres'] = $eduOrg->getField('LawAddress');
        unset($dateIssue);
        $supplementNumber=$this->getSupplement()->getField('Number');
        $result['fieldSupplementNum'] = empty( $supplementNumber)?'0':$supplementNumber;
        unset($supplementNumber);
        $certificateNumber=$certificate->getField('RegNumber');
        $result['fieldCertificateNum'] = empty($certificateNumber)?'':$certificateNumber;
        unset($certificateNumber);

        $orderDoc = $this->getSupplement()->getOrderDocument();
        $orderDocFromSup= $this->getSupplement()->getField('fk_isgaOrderDocument');
        if ($orderDoc) {
            $dateSign = $orderDoc->getField('DateSign');
            $result['fieldOrderDocumentType'] = $orderDoc->getDocumentType();
            $result['fieldODNumber'] = $orderDoc->getField('Number');
            $result['fieldODDay'] = $this->getDayOfDate($dateSign);
            $result['fieldODMount'] = $this->getMountOfDate($dateSign);
            $result['fieldODYear'] = $this->getYearOfDate($dateSign);
            unset($dateSign);
        } else if(!empty($orderDocFromSup)){
            $dateSign = $this->getSupplement()->getField('DateOrderDocumentSign');
            $docKindId=$this->getSupplement()->getField('fk_isgaOrderDocumentKind');
            $docNumber=$this->getSupplement()->getField('OrderDocumentNumber');
            $docKind=new \Model\Entities\isgaOrderDocumentKinds($docKindId);
            $docType= $docKind->getType();
            $docTypeName=$docType->getField('Name');
            //fk_isgaOrderDocumentKind
            $result['fieldOrderDocumentType'] = $docTypeName;
            $result['fieldODNumber'] = $docNumber;
            $result['fieldODDay'] = $this->getDayOfDate($dateSign);
            $result['fieldODMount'] = $this->getMountOfDate($dateSign);
            $result['fieldODYear'] = $this->getYearOfDate($dateSign);
            unset($dateSign);
            unset($docKindId);
            unset($docNumbe);
            unset($docKind);
            unset($docType);
            unset($docTypeNam);
        }else{
            $result['fieldOrderDocumentType'] = 'Приказ';
            $result['fieldODNumber'] = '000000';
            $result['fieldODDay'] = '00';
            $result['fieldODMount'] = 'января';
            $result['fieldODYear'] = date('Y');
        }
        $application=$this->getApplication();
        if (!empty($application)) {
            $type = $application->getTypeCode();
            $reason = $application->getReasonCodeArray();
        } else {
            $type = 'Accreditation';
            $reason = array('Accreditation');
        }
        //$result['blockODData']=array();
        $result['BLOCKODDATA'][0]['fieldODNumber']=$result['fieldODNumber'];
        $result['BLOCKODDATA'][0]['fieldODDay']=$result['fieldODDay'];
        $result['BLOCKODDATA'][0]['fieldODMount']=$result['fieldODMount'];
        $result['BLOCKODDATA'][0]['fieldODYear']=$result['fieldODYear'];
        //$orderDocDop=array();
        $result['blockOrderDocumentDop']=array();
        $result['blockOrderDocumentDopEmpty'][0]='';
        if ($type=='Certificate'                
          && !in_array('ReAccreditation_AddNotAccrEduPrograms', $reason)){
            $result['blockOrderDocumentDop'][0]['fieldOrderDocumentDopType']  = $result['fieldOrderDocumentType'] ;
            $result['blockOrderDocumentDop'][0]['fieldODDopNumber']           = $result['fieldODNumber'] ;
            $result['blockOrderDocumentDop'][0]['fieldODDopDay']              = $result['fieldODDay'];
            $result['blockOrderDocumentDop'][0]['fieldODDopMount']            = $result['fieldODMount'] ;
            $result['blockOrderDocumentDop'][0]['fieldODDopYear']             = $result['fieldODYear'] ;
            $result['BLOCKODDATA']=array();
            $result['fieldOrderDocumentType'] = 'Приказ';
            $result['blockOrderDocumentDopEmpty']=array();
        }
        $programs=$this->getSupplement()->getAccreditedProgramsIsAccredited();
        $programsArray=$programs->getObjectsArray();
        $programsPrint[]=array();
        foreach ($programsArray as $value) {
            $programsPrint[] = $value->getProgramData();
        }
        $programsPrint = array_filter($programsPrint);
        usort($programsPrint,
              function($a, $b) {
            $a = $a["Код ОП"];
            $b = $b["Код ОП"];
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });
        
        $blockLevel=$this->paersePrograms($programsPrint);
        
        //Добовляем номера к образовательным программам
        foreach ($blockLevel as $levelNum=>$level) {
            $blockLevel[$levelNum]['fieldLevelNum']=$levelNum+1;
            foreach ($level['_fieldNum'] as $programNum => $program) {
                $blockLevel[$levelNum]['_fieldNum'][$programNum]['fieldNum']=$programNum +1;
            }
        }
        $result['blockPrograms']=$blockLevel;
        return $result;
    }

    /**
     * заготовка , возращает имя шаблока.
     *
     * @return string
     */
    public function getTemplateName()
    {
        return '';
    }

    /**
     *  возращает имя выходного файла.
     *
     * @return string
     */
    public function getDocumentName()
    {
        $encod=mb_internal_encoding();
        $suppl = $this->getSupplement();
        $eduOrg = $suppl->getEducationalOrganization();
        $certificate = $suppl->getCertificate();
        $certNum = $certificate->getField('RegNumber');
        $certNum = empty($certNum) ? '0' : $certNum;
        $supplNum = $suppl->getField('Number');
        $supplNum = empty($supplNum) ? '0' : $supplNum;
        $suppEduOrgName = $suppl->getField('EOFullName');
        //берем то, что в кавычках
        $index1 = mb_strpos($suppEduOrgName, '«', 0, 'utf-8');
        $index2 = false;
        if ($index1 !== false) {
            $index2 = mb_strpos($suppEduOrgName, '»', $index1, 'utf-8');
        }
        $outOrgName = '';
        if ($index2 === false) {
            $eduOrg = $suppl->getEducationalOrganization();
            $suppEduOrgNameString=$eduOrg->getField('RegularName');
        }
            //если кошерно, то обрезание
            $suppEduOrgNameString = mb_substr($suppEduOrgName, $index1 + 1, $index2 - $index1 - 1, 'utf-8');
            $suppEduOrgNameArray = mb_split(' ', $suppEduOrgNameString);
            foreach ($suppEduOrgNameArray as $oneString) {
                $oneStringShort = mb_substr($oneString, 0, 4, 'utf-8');
                if (empty($outOrgName)) {
                    $outOrgName = $oneStringShort;
                } else {
                    $outOrgName .= '_' . $oneStringShort;
                }
            }
        return 'Приложение_'.$certNum.'_'.$supplNum.'_'.$outOrgName;
    }


    public function getLevelName($programData){

            $pdLevel=$programData["Уровень образования"];

            return $pdLevel;
    }

    public function createDatePrint($param)
    {
        $supplement=$this->getSupplement();
        $datePrint=$supplement->getDate('DatePrint');
        if(empty($datePrint)){
             $supplement->exchangeArray(array('DatePrint'=>date('Y-m-d')));
             $supplement->save(0);
        }

    }
    
    
    /**
     * Раскидываем программы по табличкам
     * 
     * @param type $programsPrint - массив программ
     */
     public function paersePrograms($programsPrint){
         
     }
             
}