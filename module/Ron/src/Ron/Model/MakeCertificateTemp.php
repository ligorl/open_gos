<?php

namespace Ron\Model;

class MakeCertificateTemp extends \Ron\Model\MakeCertificateNew
{
/*
    public function make()
    {

        $applicationClass = $this->getApplication();

        $applicationId = $this->getApplication()->getId();
        //$applicationDateCreate = $this->getApplication()->getField('DateCreate');
        $applicationOrder = $this->getApplication()->getField('fk_isgaOrderDocument');

        $applicationOrganization = strtoupper($this->getApplication()->getField('fk_eiisEducationalOrganization'));

        //$orderDocumentId=$applicationClass->getField("fk_isgaOrderDocument");
        //if (empty($orderDocumentId)) {
        //   throw new \Exception('Не указан распорядительный документ');
        //}
        //$orderDocument=new \Model\Entities\isgaOrderDocuments( $orderDocumentId);
        //if (empty($orderDocument)) {
        //   throw new \Exception('Не найден распорядительный документ');
        //}
        $orderDocument=  $this->getOrderDocument();

        $eduOrg=$applicationClass->getEducationalOrganization();
        if (empty($eduOrg)) {
           throw new \Exception('Не найдена образовательная организация');
        }
        $programs = $applicationClass->getLinkedProgramsChecked();
        $programsObjects = $programs->getObjectsArray();
        foreach ($programsObjects as $keyProgram => $oneProgram) {
            $programsProfy[$oneProgram->getField('fk_isgaEOBranches')][] = $oneProgram->toArray();
        }

        $certificateInfo = array();
        if (count($programsProfy) > 0) {
            $certificateInfo['EduProgramGroups'] = array(
                // 'CertificateInfo'=>$this->getCertificateInfo(),
                'SupplementsInfo' => $programsProfy,
            );
        }

        //вносим в базу запись о сертиффикатах
        foreach ($certificateInfo as $certificateKey => $certificateOne) {
            $certificate = new \Model\Entities\isgaCertificates();
            $certificateType = $certificate->getTypeIdByCode('Temporary');
            $certificateStatus = $certificate->getStatusIdByCode('Draft');
            $data = array();
            //$data['RegNumber']
            $data['DateIssue'] = $orderDocument->getField('DateSign');
            $data['DateCreate'] = date('Y-m-d');
            $data['DateEnd'] = $this->getDateEnd($certificateKey,$data['DateIssue']);

            $data['fk_oisgaApplication'] = $applicationId;

            $data['fk_eiisEducationalOrganization'] = $eduOrg->getId();//$applicationOrganization;
            $data['fk_Region_EOAddressL'] = $eduOrg->getField('fk_eiisLawRegion');
            $data['EOFullName'] = $eduOrg->getField('FullName');
            $data['EOShortName'] = $eduOrg->getField('ShortName');
            $data['fk_EOType'] = $eduOrg->getField('fk_eiisEducationalOrganizationType');
            $data['fk_EOKind'] = $eduOrg->getField('fk_eiisEducationalOrganizationKind');
            $data['fk_EOProperty'] = $eduOrg->getField('fk_eiisEducationalOrganizationProperties');
            $data['Inn'] = $eduOrg->getField('Inn');

            $data['ControlOrganHeadFullName'] = $this->getControlOrganHeadFullName();
            $data['ControlOrganHeadPost'] = $this->getControlOrganHeadPost();
            $data['fk_eiisControlOrgan'] = 'BFB891F1F234F61C11835CDEAF8E9CA0';

            $data['fk_isgaOrderDocument'] = $orderDocument->getId();//$applicationOrder;
            $data['OrderDocumentNumber'] = $orderDocument->getField('Number');
            $data['DateOrderDocumentSign'] = $orderDocument->getField('DateSign');
            $data['fk_OrderDocumentKind'] = $orderDocument->getField('fk_isgaOrderDocumentKind');

            $data['fk_isgaCertificateStatus'] = $certificateStatus;
            $data['fk_isgaCertificateType'] = $certificateType;
            //Добавляем
            $certificate->exchangeArray($data);
            $certificate->save(true);

            //Прикрепляем распорядительный документ
            $this->bindOrderDocument($certificate, $orderDocument);

            //Формируем приложения
            $certificateSupplementsArray = array();
            $supplementCountert = 1;
            //Сначало главный филиал
            if (isset($certificateOne['SupplementsInfo'][$applicationOrganization])&&count($certificateOne['SupplementsInfo'][$applicationOrganization]) > 0) {
                $certificateSupplementsArray[$supplementCountert] =
                  $this->makeSupplements($certificateOne['SupplementsInfo'][$applicationOrganization],
                       $applicationOrganization,
                       $certificate,
                       $certificateKey,
                       $supplementCountert
                  );
                $supplementCountert = 2;
            }

//Теперь филиалы, если остались
            //if(count($certificateOne['SupplementsInfo'])>=$supplementCountert){
                foreach ($certificateOne['SupplementsInfo'] as $supplementKey => $supplementOne) {
                    if ($supplementKey == $applicationOrganization) {
                        continue;
                    }
                    $certificateSupplementsArray[$supplementCountert] =
                        $this->makeSupplements($supplementOne,
                           $supplementKey,
                          $certificate,
                           $certificateKey,
                           $supplementCountert
                          );
                    ++$supplementCountert;
                }
            //}
        }
        //throw new \Exception('Недосделано');

    }
*/

    public function getDateEnd($param,$date)
    {
        if(empty($date)){
            $date=date('Y-m-d');
        }       
        $dateTime=strtotime($date);             
        return date('Y-m-d', strtotime('+1 year',$dateTime));
    }

}