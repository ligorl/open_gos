<?php

namespace Ron\Model;

class ExportCertificateSupplementSchool extends ExportCertificateSupplement
{

    public function getTemplateName()
    {
        return 'SertificateSupplementSchool.docx';
    }
    
    public function paersePrograms($programsPrint) {
        $outProgramData = array();
        $i = 0;
        $blockLevel = array();
        foreach ($programsPrint as $program) {
            $pd = $program;
            $pdLevel = $this->getLevelName($pd);
            $pdLevelMinor = $pd["Подуровень образования"];
            $pdLevel = $pd["Уровень образования"];
            if (!isset($outProgramData[$pdLevelMinor])) {
                $outProgramData[$pdLevelMinor]['index'] = $i;
                $blockLevel[$i]['fieldLevelName'] = $pdLevelMinor;
                ++$i;
            }
            $ugsCode = $pd["Код УГС"];
            $programArray = array(
                'fieldEduLevel' => $pd["Уровень образования"],
                'fieldLevelTypeName' => $pd['Наименование ОП'],
            );
            $j = $outProgramData[$pdLevelMinor]['index'];

            $blockLevel[$j]['_fieldNum'][] = $programArray;
        }
        return $blockLevel;
    }

}