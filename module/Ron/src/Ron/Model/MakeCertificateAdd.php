<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//
//Прикреплять новый расп документ !!!!!!!!!!!1
//
namespace Ron\Model;

class MakeCertificateAdd extends \Ron\Model\MakeCertificate
{

    public function make()
    {
        $application = $this->getApplication();
        $certificateOld=  $this->getCertificateCurrent();
        $applicationId = $this->getApplication()->getId();
        $orderDocument=$this->getOrderDocument();

        $this->bindOrderDocument($certificateOld, $orderDocument);

        $supplementNew = new \Model\Entities\isgaCertificateSupplements();
        $supplementStatusId = $supplementNew->getStatusIdByCode('Draft');
        $suplementsOld = $certificateOld->getSupplements();

        $maxNum=0;
        foreach ($suplementsOld as $suplementOld) {
            $data = $suplementOld->toArray();
            $number=(int)$data['Number'];
            $maxNum=$maxNum>$number?$maxNum:$number;
            unset($number);
        }
        //Теперь добавим новые
        $programs = $application->getLinkedProgramsChecked();

        //определяем сертификат по груммам или общего образования
        $programsObjects = $programs->getObjectsArray();
        $programsBase = array(); //базовое
        $programsProfy = array(); //профессиональное
        foreach ($programsObjects as $keyProgram => $oneProgram) {
            $isProfy = $oneProgram->isProfy();
            if ($isProfy === false) {
                $programsBase[$oneProgram->getField('fk_isgaEOBranches')][] = $oneProgram->toArray();
                //$programsArray[$keyProgram]['eduLevel'] = $oneProgram->getEduProgramLevel();
            }
            if ($isProfy === true) {
                $programsProfy[$oneProgram->getField('fk_isgaEOBranches')][] = $oneProgram->toArray();
                //$programsArray[$keyProgram]['eduLevel'] = $oneProgram->getEduProgramLevel();
            }
        }

        $certificateInfo=array();
        if (count($programsBase) > 0) {
            $certificateInfo['EduPrograms'] = array(
                //'CertificateInfo'=>$this->getCertificateInfo(),
                'SupplementsInfo' => $programsBase,
            );
        }

        if (count($programsProfy) > 0) {
            $certificateInfo['EduProgramGroups'] = array(
                // 'CertificateInfo'=>$this->getCertificateInfo(),
                'SupplementsInfo' => $programsProfy,
            );
        }
        $supplementCountert=$maxNum+1;

         foreach ($certificateInfo as $certificateKey => $certificateOne) {
                foreach ($certificateOne['SupplementsInfo'] as $supplementKey => $supplementOne) {
                    //if ($supplementKey == $applicationOrganization) {
                    //    continue;
                    //}
                    $certificateSupplementsArray[$supplementCountert] =
                        $this->makeSupplements($supplementOne,
                           $supplementKey,
                          $certificateOld,
                           $certificateKey,
                           $supplementCountert
                          );
                    ++$supplementCountert;
                }
         }

    }
/**
 *
 * @param type $supplementsInfo
 * @param type $eduOrgId
 * @param type $certificate
 * @param type $accreditationKindCode
 * @param type $number
 * @return type
 */
/*
    public function makeSupplements($supplementsInfo, $eduOrgId, $certificate, $accreditationKindCode, $number = 0)
    {
        $dataSupplement = array();
        //$applicationDateCreate = $this->getApplication()->getField('DateCreate');
        $applicationId = $this->getApplication()->getId();
        //$applicationOrder = $this->getApplication()->getField('fk_isgaOrderDocument');

        //$orderDocumentId=  $this->getApplication()->getField("fk_isgaOrderDocument");
        //$orderDocument=new \Model\Entities\isgaOrderDocuments( $orderDocumentId);
        $orderDocument=  $this->getOrderDocument();

        $applicationLicenseId = $this->getApplication()->getField('fk_eiisLicense');
        $license = new \Model\Entities\eiisLicenses($applicationLicenseId);
        $licenseDateEnd = $license->getField('DateEnd');
        $crtificateDateEnd = $certificate->getField('DateEnd');

        $eduOrg= new \Model\Entities\eiisEducationalOrganizations($eduOrgId);
        if (!count($eduOrg->toArray())) {
           throw new \Exception('Не найден филиал '.$eduOrgId);
        }

        $certificateSupplements = new \Model\Entities\isgaCertificateSupplements();
        $certificateSupplementsStatusId = $certificateSupplements->getStatusIdByCode('Draft');
        $certificateSupplementsKindId = $certificateSupplements->getKindIdByCode($accreditationKindCode);

        //$dataSupplement['fk_oisgaApplication']=$applicationId;
        $dataSupplement = array();
        $dataSupplement['fk_isgaCertificate'] = $certificate->getId();
        $dataSupplement['fk_isgaCertificateSupplementStatus'] = $certificateSupplementsStatusId;
        $dataSupplement['DateIssue'] = date('Y-m-d');
        $dataSupplement['DateCreate'] = date('Y-m-d');
        $dataSupplement['fk_isgaApplication'] = $applicationId;
        $dataSupplement['fk_isgaAccreditationKind'] = $certificateSupplementsKindId;
        //$dataSupplement['DateReadyToIssue'] = $applicationDateCreate;

        $dataSupplement['fk_isgaOrderDocument'] = $orderDocument->getId();
        $dataSupplement['OrderDocumentNumber'] = $orderDocument->getField('Number');
        $dataSupplement['DateOrderDocumentSign'] = $orderDocument->getField('DateSign');;
        $dataSupplement['fk_isgaOrderDocumentKind'] = $orderDocument->getField('fk_isgaOrderDocumentKind');;

        $dataSupplement['fk_eiisEducationalOrganization'] = $eduOrg->getId();
        $dataSupplement['EOFullName'] = $eduOrg->getField('FullName');
        $dataSupplement['EOShortName'] = $eduOrg->getField('ShortName');
        $dataSupplement['fk_EOType'] = $eduOrg->getField('fk_eiisEducationalOrganizationType');
        $dataSupplement['fk_EOKind'] = $eduOrg->getField('fk_eiisEducationalOrganizationKind');
        $dataSupplement['fk_EOProperty'] = $eduOrg->getField('fk_eiisEducationalOrganizationProperties');
        $dataSupplement['EOInn'] = $eduOrg->getField('Inn');
        $dataSupplement['fk_Region_L'] = $eduOrg->getField('fk_eiisLawRegion');
        $dataSupplement['EOAddressL'] = $eduOrg->getField('LawAddress');
        //$dataSupplement['fk_Region_EOAddressL'] = $eduOrg->getField('Inn');

        $dataSupplement['Number'] = (string) $number; //(($supplementKey==$applicationOrganization)?'1':(string)($i++));

        $certificateSupplements->exchangeArray($dataSupplement);
        $result = $certificateSupplements->save(true);

        foreach ($supplementsInfo as $accreditedKey => $accriditedProgram) {
            $licensedProgram = new \Model\Entities\eiisLicensedPrograms($accriditedProgram['fk_eiisLicensedPrograms']);
            $eduProgram=$licensedProgram->getEduProgram();

            $dataProgram = array();
            $dataProgram['fk_oisgaApplication'] = $applicationId;
            $dataProgram['fk_eiisLicensedProgram'] =  $licensedProgram->getId();
            $dataProgram['Name'] = $licensedProgram->getField('Name');
            $dataProgram['EduNormativePeriod'] = $accriditedProgram['termEdu'];
            $dataProgram['Code'] = $licensedProgram->getField('Code');
            $dataProgram['StartYear'] = $accriditedProgram['yearStartEdu'];
            $dataProgram['FullTimeFormGraduatesNumber'] = $accriditedProgram['fullTimeCount'];
            $dataProgram['PartTimeFormGraduatesNumber'] = $accriditedProgram['partTimeCount'];
            $dataProgram['ExtramuralTimeFormGraduatesNumber'] = $accriditedProgram['correspondenceCount'];
            $dataProgram['fk_isgaCertificateSupplement'] = $certificateSupplements->getId();
            $dataProgram['DateLicenseEnd'] = $licenseDateEnd;

            $dataProgram['fk_isgaGroupSpeciality'] = $eduProgram->getField('fk_eiisGS');
            $dataProgram['QualificationGrade'] = $eduProgram->getField('QualificationGrade');
            $dataProgram['fk_eiisEduProgramType'] = $eduProgram->getField('fk_eiisEduProgramType');
            $dataProgram['fk_isgaEduStandard'] = $eduProgram->getField('fk_isgaEduStandard');
            $dataProgram['fk_eiisEduProgram'] = $eduProgram->getId();
            $dataProgram['DateCertificateEnd'] = $certificate->getField('DateEnd');
            //$dataProgram['LicenseIsUnlimited'] =
            $dataProgram['fk_isgaQualification'] = $eduProgram->getField('fk_isgaQualification');
            $dataProgram['AdditionalInfo']=$eduProgram->getField('EduNote');

            $dataProgram['fk_eiisEducationalOrganization'] = $eduOrg->getId();
            $dataProgram['DateCreate'] = date('Y-m-d');
            //$dataProgram['LicenseIsUnlimited']=$license->getField($fieldName)
            $dataProgram['OksoCode']=  $licensedProgram->getField('OKSO');

            //$dataProgram['DateLicenseEnd'] = $crtificateDateEnd;
            $accrepetProgram = new \Model\Entities\isgaAccreditedPrograms();
            $accrepetProgram->exchangeArray($dataProgram);
            $accrepetProgram->save(true);
        }
        return $dataSupplement;
    }
*/
/*
public function getDateEnd($param)
    {


        if ($param == 'EduProgramGroups') {
            return date('Y-m-d', strtotime('+6 year'));
        }
        if ($param == 'EduPrograms') {
            return date('Y-m-d', strtotime('+12 year'));
        }
    }
 * 
 */
}
