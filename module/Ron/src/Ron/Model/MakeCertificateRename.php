<?php

namespace Ron\Model;

class MakeCertificateRename extends \Ron\Model\MakeCertificate
{
    public function make()
    {
        $application = $this->getApplication();
        $certificateOld=  $this->getCertificateCurrent();
        $orderDocument=  $this->getOrderDocument();
        $applicationId = $this->getApplication()->getId();
        $certificateNew = new \Model\Entities\isgaCertificates();
        $certificateStatus = $certificateOld->getStatusIdByCode('Draft');
        $data = $certificateOld->toArray();
        unset($data['Id']);
        unset($data['FormNumber']);
        unset($data['SerialNumber']);
        unset($data['DatePrint']);
        unset($data['Note']);
        unset($data['DateCheck']);
        unset($data['fk_isgaCollege']);
        unset($data['fk_isgaCheckStatus']);
        //unset($data['RaaId']);
        unset($data['SpoiledFormsCount']);
        unset($data['SpoiledFormsData']);
        unset($data['DateReadyToIssue']);
        //unset($data['DateIssue']);

        //$data['DateIssue']=date('Y-m-d');
        unset($data['DateIssueToApplicant']);
        $data['fk_oisgaApplication'] = $applicationId;
        $data['fk_isgaCertificateStatus'] = $certificateStatus;
        $data['fk_isgaOrderDocument']=$application->getField("fk_isgaOrderDocument");
        $data['OrderDocumentNumber']=$orderDocument->getId();
        $data['DateOrderDocumentSign']=$orderDocument->getField('DateSign');
        $data['fk_OrderDocumentKind']=$orderDocument->getField('fk_isgaOrderDocumentKind');
        $data['DateCreate']=date('Y-m-d');//поинтерисоватся
        $data['DateIssue']=date('Y-m-d');
        unset($data['CertificateFile']);
        unset($data['CertificateFileName']);

        $certificateNew = new \Model\Entities\isgaCertificates();
        $certificateNew->exchangeArray($data);
        $certificateNew->save(true);

        //Прикрепляем распорядительный документ
        $this->bindOrderDocument($certificateNew, $orderDocument);

        $supplementNew = new \Model\Entities\isgaCertificateSupplements();
        $supplementStatusId = $supplementNew->getStatusIdByCode('Draft');
        $suplementsOld = $certificateOld->getSupplements();
        foreach ($suplementsOld as $suplementOld) {
            $data = $suplementOld->toArray();
            unset($data['Id']);
            unset($data['FormNumber']);
            unset($data['SerialNumber']);
            unset($data['DatePrint']);
            unset($data['Note']);
            unset($data['SupplementFile']);
            unset($data['SupplementFileName']);
            //$data['DateIssue']=date('Y-m-d');
            unset($data['DateIssueToApplicant']);
            unset($data['DateReadyToIssue']);

            $data['fk_isgaCertificate'] = $certificateNew->getId();
            $data['fk_isgaCertificateSupplementStatus'] = $supplementStatusId;
            $data['fk_isgaApplication'] = $applicationId;
            //$data['fk_isgaOrderDocument'] = $applicationOrder;
            //$data['DateCreate']=date('Y-m-d');

            $supplementNew = new \Model\Entities\isgaCertificateSupplements();
            $supplementNew->exchangeArray($data);
            $supplementNew->save(true);

            $programs = $suplementOld->getAccreditedPrograms();
            foreach ($programs as $programOld) {
                $data = $programOld->toArray();
                unset($data['Id']);
                $data['fk_oisgaApplication'] = $applicationId;
                $data['fk_isgaCertificateSupplement'] = $supplementNew->getId();
                $accrepetProgram = new \Model\Entities\isgaAccreditedPrograms();
                $accrepetProgram->exchangeArray($data);
                $accrepetProgram->save(true);
            }
        }
    }
}