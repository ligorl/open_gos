<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ron\Model;

class ExportCertificateSupplementVuz extends ExportCertificateSupplement
{


    public function getTemplateName()
    {
        return 'SertificateSupplementVuz.docx';
    }

    public function getLevelName($programData){

          $pdLevelMinor=$programData["Подуровень образования"];
            $pdLevel=$programData["Уровень образования"];
            if (!empty($pdLevelMinor)){
                $pdLevel=$pdLevel.' - '.$pdLevelMinor;
            }
            return $pdLevel;
    }


    public function paersePrograms($programsPrint) {
        $outProgramData = array();
        $i = 0;
        $blockLevel = array();
        foreach ($programsPrint as $program) {
            $pd = $program;
            $pdLevel = $this->getLevelName($pd);
            $pdLevelMinor = $pd["Подуровень образования"];
            $pdLevel = $pd["Уровень образования"];
            if (!empty($pdLevelMinor)) {
                $pdLevel = /* $pdLevel.' - '. */$pdLevelMinor;
            }

            if (!isset($outProgramData[$pdLevel])) {
                $outProgramData[$pdLevel]['index'] = $i;
                $blockLevel[$i]['fieldLevelName'] = $pdLevel;
                $blockLevel[$i]['fieldLevelSortOrder'] = $pd['SortOrder'];
                ++$i;
            }
            $ugsCode = $pd["Код УГС"];
            if (isset($outProgramData[$pdLevel][$ugsCode])) {
                continue;
            }
            $outProgramData[$pdLevel][$ugsCode] = $ugsCode;
            $programArray = array(
                'fieldUgsCode' => $pd["Код УГС"],
                'fieldUgsName' => $pd["Наименование УГС"],
                'fieldEduLevel' => $pd["Уровень образования"],
                'fieldLevelTypeName' => $pd['Наименование ОП'],
            );
            $j = $outProgramData[$pdLevel]['index'];

            $blockLevel[$j]['_fieldNum'][] = $programArray;
        }
        
        //Сортировка по типом програм(загловок таблиц)
        usort($blockLevel,
              function($a, $b) {
            $a = $a["fieldLevelSortOrder"];
            $b = $b["fieldLevelSortOrder"];
            if ($a == $b) {
                return 0;
            }
            return ($a < $b) ? -1 : 1;
        });

        return $blockLevel;
    }

}