<?php

namespace Ron\Model;


class ExportCertificateVuz extends ExportCertificate
{


    /**
     *  возращает имя шаблока.
     *
     * @return string
     */
    public function getTemplateName()
    {
        return 'CertificateVuz.docx';
    }

}