<?php


namespace Ron\Model;

class ExportCertificateSchool extends ExportCertificate
{
    /**
     *  возращает имя шаблока.
     *
     * @return string
     */
    public function getTemplateName()
    {
        return 'CertificateSchool.docx';
    }
    
}