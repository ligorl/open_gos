<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ron\Model;

/**
 * Description of ExportNotifyReturn
 *
 * @author sany
 */
class ExportNotifyReturn  extends ExportNotify
{

    /**
     * ставим статус о несоответствии
     */
    public function chekkedIt(){
        $this->getApplicationDocumentGroup()->chekedReturn();
    }

    /**
     * возращаем новый статус заявления
     *
     * @return string
     */
    public function getStatusNew()
    {
        return '4F4D8935-0175-4343-BC7B-154AB6E65E07';
    }

    /**
     * формируем массив выходных данных
     *
     * @return type - массив с выходными данными
     */
    public function getData()
    {
        $result = parent::getData();
        //$comment = $this->notifyCurrent->getField('Comments');
        //$commentData = json_decode($comment);
        $docGroupGroup= new \Model\Entities\isgaApplicationDocumentGroupGroupStatusReason();
        $reasons = $docGroupGroup
          ->getReasons($this->getApplicationCurrent()->getField('Id'));

        //Дергнуть заявление о несоответствии
        $negativClass=new \Ron\Model\ExportNotifyNegativ($this->getApplicationId(),$this->user);
        $negativ=$negativClass->getNotifyCurrent();
        $negativDate=$negativ->getField('DateCreation');
        $result['fieldNegativDay']=  $this->getDayOfDate($negativDate);
        $result['fieldNegativMount']=  $this->getMountOfDate($negativDate);
        $result['fieldNegativYear']=  $this->getYearOfDate($negativDate);
        $result['fieldNegativNumber']=$negativ->getField('Number');
        
        return $result;
    }

    /**
     * @return string - имя уведомления
     */
    public function getNotifyName()
    {
        return 'Уведомление о возрате';
    }

    /**
     *
     * @return type - регистрационный номер уведомления
     */
    public function getRegNumber()
    {
        $result = parent::getRegNumber();
        return $result . '/во';
    }

    /**
     * возвращает ид шаблона
     * (вместо вызова isga_NotificationTemplates)
     *
     * @return string
     */
    public function getNotifyTemplateId(){
        return 'E4054762-8374-4F9C-A697-2C8A2519E68E';
    }
}