<?php

namespace Ron\Model;

class MakeCertificateReorg extends \Ron\Model\MakeCertificateNew
{
    private $dateEnd=null;
    public function getDateEnd($param,$date)
    {
        if ($this->dateEnd === null) {
            $app = $this->getApplication();
            $appId = $app->getId();
            //дергаме перечень вузов, в реорганизации
            $mtmOrg = new \Model\Entities\isgamtmApplicationOrganizations();
            $reorgOrg = $mtmOrg->getOrganizationsByApplicationId($appId);
            $reorgOrgArray = $reorgOrg->getObjectsArray();
            $dateEndMin = '2199-12-31';
            foreach ($reorgOrgArray as $oneOrg) {
                $orgId = $oneOrg->getField('Id');
                //дергаим их действующие сертификаты
                $orgCert = $oneOrg->getCertificates();
                $orgCertArray = $orgCert->getObjectsArray();
                foreach ($orgCertArray as $certOne) {
                    //собираем их даты
                    $dateEndDate = $certOne->getDate('DateEnd');
                    if(empty($dateEndDate) )       {
                        continue;
                    }
                    $dateEnd = $certOne->getField('DateEnd');
                    //выбираем минимальную,
                    if ($dateEnd < $dateEndMin) {
                        $dateEndMin = $dateEnd;
                    }
                }
            }
            if ($dateEndMin=='2199-12-31'){
                throw new \Exception('у сертификатов вузов не определены даты окончания');
            }
             //возращаем минимальную
            $this->dateEnd = $dateEndMin;
        }
        return $this->dateEnd;
    }

}