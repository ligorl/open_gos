<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ron\Model;

/**
 * Description of MakeCertificateValidator
 *
 * @author sany
 */
class MakeCertificateValidator
{

    private $application;
    private $reasons = array();
    private $certificateOne;
    private $suplementOne;
    private $toValidate;

    public function __construct($application, $toValidate = array())
    {
        if (empty($application)) {
            return array('нет такого заявления');
        }
        $this->application = $application;
        $this->toValidate = $toValidate;
        //$this->valid();
        //return $this->reasonsGet();
    }

    public function validate()
    {
        $this->valid();
        return $this->reasonsGet();
    }

    public function reasonsAdd($param)
    {
        $this->reasons[] = $param;
    }

    public function reasonsAddCertificate($reason)
    {
        $this->reasonsAdd('Свидительство №' . $this->certificateOne->getField('RegNumber') . ' - '. $reason );
    }

    public function reasonsAddSupplement($reason)
    {
        $supNum = $this->suplementOne->getField('Number');
        $regnum = $this->certificateOne->getField('RegNumber');
        $s = '';
        $s .='Свидетельство № ';
        $s .= empty($regnum) ? '0' : $regnum;
        $s .= ' приложение №';
        $s .=empty($supNum) ? '0' : $supNum;
        $s .= ' - ' . $reason;
        $this->reasonsAdd($s);
    }

    public function reasonsGet()
    {
        return $this->reasons;
    }

    public function valid()
    {
        $application = $this->application;
        $certificates = $application->getCertificates();
        if (!$certificates) {
            $this->reasonsAdd('нет сертификатов');
            return;
        }
        $result = array();
        foreach ($certificates as $certificateOne) {
            $this->certificateOne = $certificateOne;
            $this->validateCertificate();
            $suplements = $certificateOne->getSupplements();
            foreach ($suplements as $suplementOne) {
                $this->suplementOne = $suplementOne;
                $this->validateSupplement();
            }
        }
    }

    public function validateCertificate()
    {
        foreach ($this->toValidate as $fieldName => $mesage) {
            $this->validateCertificateEmpty($this->certificateOne, $fieldName, $mesage);
        }
        //$this->validateEmpty($this->certificateOne,'SerialNumber','не указан серийный номер бланка');
        //$this->validateEmpty($this->certificateOne,'FormNumber','не указан номер бланка');
    }

    public function validateCertificateEmpty($tablleTo, $fieldName, $mesage)
    {
        $field = $tablleTo->getField($fieldName);
        $fieldDate = $tablleTo->getDate($fieldName);
        if (empty($field)||empty($fieldDate)) {
            $this->reasonsAddCertificate($mesage);
        }
    }

    public function validateSupplement()
    {
        foreach ($this->toValidate as $fieldName => $mesage) {
            $this->validateSupplementEmpty($this->suplementOne, $fieldName, $mesage);
        }
    }

    public function validateSupplementEmpty($tablleTo, $fieldName, $mesage)
    {
        $field = $tablleTo->getField($fieldName);
        $fieldDate = $tablleTo->getField($fieldName);        
        if (empty($field) || empty($fieldDate)) {
            $this->reasonsAddSupplement($mesage);
        }
    }

}