<?php

namespace Ron\Model;

/**
 * формирование и вывод уведомления о не соответствии
 */
class ExportNotifyNegativ extends ExportNotify
{

    /**
     * ставим статус о несоответствии
     */
    public function chekkedIt(){
        $this->getApplicationDocumentGroup()->chekedNegativ();
    }

    /**
     * возращаем новый статус заявления
     *
     * @return string
     */
    public function getStatusNew()
    {
        return '2F7A2025-FDBF-448A-8635-2AEE51227151';
    }

    /**
     * формируем массив выходных данных
     *
     *  ${BLOKREASONSPAGECOUNTNOT} 
     *  ${/BLOKREASONSPAGECOUNTNOT}
     *  ${BLOKREASONSPAGECOUNTYES} 
     *  ${/BLOKREASONSPAGECOUNTYES}
     *  ${BLOKREASONSPAGE} 
     *      ${REASONSBLOCK}
     *      ${/REASONSBLOCK}
     *  ${/BLOKREASONSPAGE}
     * @return type - массив с выходными данными
     */
    public function getData()
    {
        $result = parent::getData();
        //$result['fieldNameForSupplement']=$result['fieldName'];
        //$comment = $this->notifyCurrent->getField('Comments');
        //$commentData = json_decode($comment);
        $docGroupGroup= new \Model\Entities\isgaApplicationDocumentGroupGroupStatusReason();
        $reasons = $docGroupGroup
          ->getReasons($this->getApplicationCurrent()->getField('Id'));
        $reasonsArray= $reasons ->toArray();
        $commentData=array();
        if (!empty ($reasonsArray)) {
            $reasonsOut = array();
            foreach ( $reasonsArray as $value) {
                if (isset($value['Comment'])) {
                    $commentData[]['fieldReason']= $value['Comment'];
                } elseif (isset($value ['fk_isgaApplicationDocumentGroupStatusReasons'])) {
                   $commentData[]['fieldReason'] =$docGroupGroup->getStatusReasonValue( $value ['fk_isgaApplicationDocumentGroupStatusReasons']);
                } else
                    continue;
            }
        }        
        $reasonPage = array();
        if(!empty($commentData)){    
            $reasonPage[0]['fieldNameForSupplement']=$result['fieldName'];
            $reasonPage[0]['fieldDateCreateSupplement']=$result['fieldDateCreate'];
            $reasonPage[0]['fieldNumberSupplement']=$result['fieldNumber'];
            $reasonPage[0]['REASONSBLOCK'] = $commentData; 
        }
        $result['BLOKREASONSPAGE'] = $reasonPage;
        if(empty($commentData)){
            //$result['BLOKREASONSPAGECOUNTNOT'] = array(array());
            $result['BLOKREASONSPAGECOUNTYES'] = array();
        }else{
            //$result['BLOKREASONSPAGECOUNTNOT'] = array();
            $result['BLOKREASONSPAGECOUNTYES'] = array(array());
        }        
        return $result;
    }

    /**
     * @return string - имя уведомления
     */
    public function getNotifyName()
    {
        return 'Уведомление о  не соответствии';
    }

    /**
     * 
     * @return type - регистрационный номер уведомления
     */
    public function getRegNumber()
    {
        $result = parent::getRegNumber();
        return $result . '/н';
    }

    /**
     * возвращает ид шаблона
     * (вместо вызова isga_NotificationTemplates)
     *
     * @return string
     */
    public function getNotifyTemplateId(){
        return '5C66BDA8-BD63-4962-91DB-BDCBA893D53F';
    }

}