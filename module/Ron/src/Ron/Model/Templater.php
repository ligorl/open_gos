<?php

namespace Ron\Model;

class Templater
{

    //protected $templateFile;
    protected $dataArray;
    protected $outputType;
    protected $outputName;
    protected $tempDir;
    protected $templateProcessor;
    protected $fileOutName;
    protected $result='';

    /**
     *
     * @param type $templateFile - полный путь к имени файла шаблона
     * @param type $dataArray - массив с данными
     * @param type $outType - тип выходного документа
     * @throws Exception
     */
    public function __construct($templateFile,$dataArray=array(),$outType = 'docx',$outName)
    {
        if (!isset($templateFile)){
            throw new \Exception('Не указан файл шаблона');
        }
        if (!file_exists($templateFile)){
            throw new \Exception('Файла шаблона не существует: '.$templateFile);
        }
        //создаем временное хранилище
        $this->tempDir = sys_get_temp_dir() . '/' . uniqid() . '/';
        if (!mkdir($this->tempDir, 0777, true)) {
            throw new Exception('Ошибка при создании временного хранилища ');
        }
        $this->templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templateFile);
        $outputType=  strtolower($outType);
        if(!in_array($outputType,array('docx','doc','pdf','odt'))){
            $outputType='docx';
        }
        $this->dataArray=$dataArray;
        $this->outputName=$outName;
        $this->outputType=$outputType;
    }


    function __destruct(){
        if (is_dir($this->tempDir)) {
            $files=scandir($this->tempDir, SCANDIR_SORT_NONE);
            if (is_array($files)) {
                foreach ($files as $filename) {
                    if ($filename!=='.' || $filename!=='..'){
                        if(is_file($filename)){
                            @unlink($filename);
                        }
                    }
                }
            }
            @rmdir($this->tempDir);
        }
    }

    /**
     * сохраняет документ, если надо, то конвертирует
     *
     * @return сформированный доккумент
     */

    protected function doDocment(){
        //Сохраняем полученый документ и готовим к отправке
        $fileTempPath = $this->tempDir . 'out.docx';
        $this->templateProcessor->saveAs($fileTempPath);
        $result = '';
        if (file_exists($fileTempPath)) {
            $outTypes = array('pdf', 'doc', 'odt');
            if (in_array($this->outputType, $outTypes)) {
                $s = '/usr/bin/libreoffice5.0 '
                  . ' --headless --convert-to ' . $this->outputType
                  . ' --nofirststartwizard '
                  . ' --nologo --norestore --invisible '
                  . ' --outdir ' . $this->tempDir
                  . ' ' . $fileTempPath;
                \Eo\Model\Templater::exec( $s );
                $fileTempConvert = $this->tempDir . 'out.' . $this->outputType;
                if (file_exists($fileTempConvert)) {
                    $result = file_get_contents($fileTempConvert);
                    @unlink($fileTempConvert);
                }
            } else {
                $result = file_get_contents($fileTempPath);
            }
            unset($outTypes);
            @unlink($fileTempPath);
        }
        return $result;
    }


    /**
     * Заполнение полей в документе
     *
     * @param type $valueArray - массив имя поля=>значение
     * @param type $templateProcessor - класс шаблонизатора
     * @param type $count - количество производимых замен
     *
     * @return type - класс шаблонизатора
     */
    /*
    public function templateMakeValue($valueArray, $count = 1)
    {
        if (!empty($valueArray)) {
            foreach ($valueArray as $key => $val) {
                if (is_string($val) || is_numeric($val)) {
                    $this->templateProcessor->setValue($key, htmlspecialchars($val, ENT_COMPAT, 'UTF-8'), $count);
                }
            }
        }
    }
*/

    /**
     * возращает массив таблицы шаблона
     * имя_таблицы => имя_ первого_поля
     *
     * @return type
     */
    public function getTemplateTable(){
        return array();
    }

    /**
     * заполнение таблиц в документе
     *
     * @param type $tableArray - массав с данными таблицы в виде под массива
     * @param type $firstField - имя таблицы
     * @return type
     */
    public function templateMakeTable($tableArray,$firstField)
    {
        //foreach ($this->getTemplateTable() as $tableName => $firstField) {
        //reset($tableArray[0]);
        //$firstField = key($tableArray[0]);
        if (count($tableArray) > 0) {
            $this->templateProcessor->cloneRow($firstField, count($tableArray));
            foreach ($tableArray as $keyRow => $valueRow) {
                //$valueRow['countcol'] = $keyRow + 1;
                if( !is_array($valueRow)){
                    $this->templateMakeDocument(array($keyRow =>$valueRow));
                }else {
                    $this->templateMakeDocument($valueRow, '#'.(string) ($keyRow + 1));
                }
                //старый метод, нет вхождений, только строки
                //foreach ($valueRow as $keyFiled => $valueField) {
                //    //$macro = $keyFiled . '#' . (string) ($keyRow + 1);
                //    //$this->templateProcessor->setValue($macro, htmlspecialchars($valueField, ENT_COMPAT, 'UTF-8'), 1);
                //}
            }
        } else {
            //Стираем строки, если для них нет значений.
            $this->templateProcessor->cloneRow($firstField, 0);
        }
        // }
    }

    /**
     *
     * @param type $arrayData
     * @param type $templateDocPath
     * @param type $outType
     * @return type
     */
    public function templateMakeDocument($dataArray,$minor='')
    {

        if(empty($dataArray)){
            return false;
            //$dataArray=$this->dataArray;
        }
        //Изменяем штрих код
        //if ($outType != 'pdf' || empty($arrayData['shtrihKod'])) {
        //    $templateProcessor->deleteImage();
        //} else
        //if (!empty($arrayData['shtrihKod'])) {
        //    $strihFile = 'image1.gif';
        //    $strihFilePath = $tempDir . $strihFile;
        //    file_put_contents($strihFilePath, $arrayData['shtrihKod']);
        //    $templateProcessor->setImageValue($strihFile, $strihFilePath);
        //   unset($strihFile);
        //}
        if(is_array($dataArray) && count($dataArray)){
           foreach ($dataArray as $key => $value) {
               $key = $key.$minor;
               if (is_string($value) || is_numeric($value)){
                   //вставляем значение
                   $this->templateProcessor->setValue($key, htmlspecialchars($value, ENT_COMPAT, 'UTF-8'),1);
               }
               if (is_array($value)) {
                    $firstKey = substr($key, 0, 1);
                    if ($firstKey == '_') {
                        //таблица
                        $this->templateMakeTable($value,  substr($key, 1));
                    } else {
                        //блок
                        if (!empty($value) ) {
                            $blockCount = 0;
                            if(  is_numeric($value)){
                                $blockCount = $value;
                            }
                            if( is_array($value)){
                                $blockCount = count($value);
                            }
                            $res = $this->templateProcessor->cloneBlock($key, $blockCount);
                            foreach ($value as $branchKey => $branchValue) {
                                $this->templateMakeDocument($branchValue,$minor);
                                //вставляем значения полей
                                //$templateProcessor = $this->templateMakeValue($branchValue, $templateProcessor);
                                //Заполняем табличку филиалов.
                                //$templateProcessor = $this->templateMakeTable($branchValue, $this->getTablesOut(),
                                //                                              $templateProcessor);
                            }
                        } else {
                            $this->templateProcessor->cloneBlock($key, 0);
                        }
                        //$this->templateMakeDocument($value);
                    }
                }
            }
        }
        //вставляем значения полей
        //$templateProcessor = $this->templateMakeValue($arrayData, $templateProcessor);
        //Заполняем таблички.
        //$templateProcessor = $this->templateMakeTable($arrayData, $this->getTablesOut(), $templateProcessor);
        //Заполняем филиалы
        //if (!empty($arrayData['branches']) && is_array($arrayData['branches'])) {
        //    $templateProcessor->cloneBlock('BRANCH', count($arrayData['branches']));
        //    foreach ($arrayData['branches']as $branchKey => $branchValue) {
        //        //вставляем значения полей
        //        $templateProcessor = $this->templateMakeValue($branchValue, $templateProcessor);
        //        //Заполняем табличку филиалов.
        //        $templateProcessor = $this->templateMakeTable($branchValue, $this->getTablesOut(), $templateProcessor);
        //    }
       // } else {
        //    $templateProcessor->cloneBlock('BRANCH', 0);
        //}
    }


    function makeDocument()
    {
        $this->templateMakeDocument($this->dataArray);
        return $this->doDocment();
    }

    function makeDocumentOut()
    {
        $this->result=$this->makeDocument();
        $this->sendHeader();
        echo $this->result;
    }


    public function sendHeader()
    {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . '"' . $this->getFileOutName() . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . strlen($this->result));
        //header('Pragma: no-cache');//глюко ошибка
        header('Cache-Control: no-store, no-cache, must-revalidate');
        clearstatcache();
    }

    public function setFileOutName($fileName)
    {
        $this->fileOutName=$fileName.'.'.$this->outputType;
    }

    public function getFileOutName()
    {
        $result= $this->outputName.'.'.$this->outputType;
        return $result;
    }

}