<?php

namespace Ron\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Pages extends AbstractHelper
{
    public function __invoke($c, $o, $r=4)
    {
        if($c==0) $c=1;

        $j=0;$rez="";
        //в 2 нижестоящих строчках я решаю, с какой позиции начать вывод страниц
        if (($o-$r)<1) {$i=1;} else {$i=$o-$r;} if ($i==1) {$i++;}
        if (($c-($r*2+1))<$i) {$i=$c-($r*2+1);} if ($i<2) {$i=2;}

        //первую страницу вывожу всегда
        if ($o!=1) {
            $rez.="<span class='page' page='1'>1</span>";
        } else { $rez.="<span class='current-page'>1</span>"; }

        if ($i>2) {$rez.="<span class='interval'>..</span>";}

        while ( ( ($i<=($o+$r)) or ($j<($r*2+1)) ) and ($i<$c)){
            if ($i!=$o) {$rez.=("<span class='page' page='".$i."'>".$i."</span>");}
            else{ $rez.="<span class='current-page'>".$i."</span>"; }//проверяю текущая ли страница. если да то помечаю ее жирным

            $i++;$j++;
        }

        if ($i<$c) {$rez.="<span class='interval'>..</span>";}

        if ($c>1) {if ($c!=$o) {
            $rez.=("<span class='page' page='".$c."'>".$c."</span>");
        } else{
            $rez.="<span class='current-page'>".$c."</span>";
        }}
        return $rez;
    }
}