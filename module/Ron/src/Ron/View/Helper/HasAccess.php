<?php

namespace Ron\View\Helper;

use Zend\View\Helper\AbstractHelper;

class HasAccess extends AbstractHelper
{
    public function __invoke($accessName)
    {
        $user = $this->getView()->getHelperPluginManager()->getServiceLocator()->get('AuthService')->getIdentity();;
        return $user->hasAccess($accessName);
    }
}