<?php

namespace Ron\EloquentModel;
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 07.12.2017
 * Time: 16:38
 */
use Illuminate\Database\Eloquent\Model as EloquentModel;

class ApplicationFileCorrupt extends EloquentModel
{

    protected $connection = 'f11_mon_open';
    protected $table = 'isga_applicationfilescorrupt';
    public $timestamps = false;
}