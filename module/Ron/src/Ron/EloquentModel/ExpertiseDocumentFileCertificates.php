<?php
/**
 * Created by PhpStorm.
 * User: back-
 * Date: 10.12.2017
 * Time: 15:57
 */

namespace Ron\EloquentModel;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class ExpertiseDocumentFileCertificates extends EloquentModel
{
    protected $connection = 'f11_mon_experts_open';
    protected $table = 'expertise_document_file_certificates';

    public $timestamps = false;

}
