<?php

namespace Ron\EloquentModel;
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 07.12.2017
 * Time: 16:38
 */
use Illuminate\Database\Eloquent\Model as EloquentModel;

class ExpertiseDocumentFile extends EloquentModel
{

    protected $connection = 'f11_mon_experts_open';
    protected $table = 'expertise_document_file';
    public $timestamps = false;
}