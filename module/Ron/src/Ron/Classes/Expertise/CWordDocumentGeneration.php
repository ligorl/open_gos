<?php

namespace Ron\Classes\Expertise;

class CWordDocumentGeneration
{

    /**
     * Генирация массива данных, которые пойдут в шаблон
     *
     * @param $expertiseId
     * @return array
     */
    public function generateDataForTemplateConclusion($expertiseId, $countPrograms)
    {

        //Дата составления заключения
        $expertiseModel = new \Model\ExpertEntities\expertise($expertiseId);
        $dateEndTimeStamp = $expertiseModel->getField('end_timestamp');
        $readyDateEndTimeStamp = $this->generateDate($dateEndTimeStamp);


        //Реквизиты заключения о составе комиссии
        //Дата
        $expertiseDocumentModel = new \Model\ExpertEntities\expertisedocument();
        $expertiseDocumentData = $expertiseDocumentModel->getByWhere(array(
            'document_type_id = 8',
            'expertise_id' => $expertiseId,
        ));

        $expertiseOpenDate = $expertiseDocumentData->getField('open_date');
        $readyOpenDate = $this->generateDate($expertiseOpenDate);

        //номер документа
        $expertiseDcoumentNumber = $expertiseDocumentData->getField('document_number');


        //Полное наименование ОО
        $commissionMemberModel = new \Model\ExpertEntities\commissionmember();
        $members = $commissionMemberModel->getByWhere(array(
            'expertise_id' => $expertiseId
        ));

        $getInstitutionId = $members->getField('institution_id');

        $eduOrgModel = new \Model\Entities\eiisEducationalOrganizations($getInstitutionId);
        $nameOrg = $eduOrgModel->getField('FullName');

        //Полученное название переводим в творительный падеж
        $morphy = new \Eo\Model\Licensing\Tools\Morphy();
        $morphy = $morphy->getForms($nameOrg);

        $getAblativeNameOrg = $morphy['ablative'];

        //Разбиваю название на разные строки. КОСТЫЛЬ
        $arrWords = explode(' ', $getAblativeNameOrg);
        $fullNameOrg = array();
        $fullNameOrg2 = array();
        $fullNameOrg3 = array();
        $fullNameOrg4 = array();
        $fullNameOrg5 = array();
        $count = 0;
        foreach ($arrWords as $key => $val){
            $len = strlen($val)/2 +1;
            $count += $len;

            if($count < 65){
                $fullNameOrg[] = $val;
            }elseif($count > 65 && $count < 130){
                $fullNameOrg2[] = $val;
            }elseif($count > 130 && $count < 190){
                $fullNameOrg3[] = $val;
            }elseif($count > 190 && $count < 250){
                $fullNameOrg4[] = $val;
            }elseif($count > 250){
                $fullNameOrg5[] = $val;
            }
        }


       //Реквизиты ОП
        $getTextTitle = $this->getRequisites($commissionMemberModel, $expertiseId);

        //Название основной образовательной программы
        $nameEduProgram = $this->getNameEduOrg($commissionMemberModel, $expertiseId);

        //Код и наименование группы профессий
        $enlargedNameAndCode = $this->getCodeAndNameProfessions($commissionMemberModel, $expertiseId, 'fk_isgaEnlargedGroupSpecialities');

        //Код и наименование профессии
        $licensedProgramNameAndCode = $this->getCodeAndNameProfessions($commissionMemberModel, $expertiseId, 'fk_eiisLicensedPrograms');

        //ФИО руководителя эксп. группы
        $chairmansModel = new \Model\ExpertEntities\chairman();
        $chairmansId = $chairmansModel->getByWhere(
            array(
                'institution_id' => $getInstitutionId,
                'expertise_id' => $expertiseId,
            ));
        
        $headExpGroupId = $chairmansId->getField('expert_id');

        $expertiseModel = new \Model\ExpertEntities\expert($headExpGroupId);
        $fioHeadExpGroup = $expertiseModel->getField('last_name') . ' ' . $expertiseModel->getField('name') . ' ' . $expertiseModel->getField('middle_name');


        //Формирование массива данных
        $allData = [];

        for($i = 1; $i <= $countPrograms; $i++) {
            $arrayData = array(
                'fieldEndTimeStamp' => $readyDateEndTimeStamp,
                'fieldOpenDate' => $readyOpenDate,
                'fieldDocumentNumber' => $expertiseDcoumentNumber,
                'fieldEductionOrganization' => implode(' ', $fullNameOrg),
                'fieldEductionOrganization2' => implode(' ', $fullNameOrg2),
                'fieldEductionOrganization3' => implode(' ', $fullNameOrg3),
                'fieldEductionOrganization4' => implode(' ', $fullNameOrg4),
                'fieldEductionOrganization5' => implode(' ', $fullNameOrg5),
                'fieldExpertFio' => $fioHeadExpGroup,
                'fieldLodTitle' => $getTextTitle[$i-1],
                'fieldLodTitle1' => $getTextTitle[$i-1],
//                'fieldLodTitle2' => $lodTitle[$i],
                'fieldLodTitle2' => $getTextTitle[$i-1],
                'fieldLodTitle3' => $getTextTitle[$i-1],
                'fieldLodTitle4' => $getTextTitle[$i-1],
                'fieldLodTitle5' => $getTextTitle[$i-1],
                'fieldLodTitle6' => $getTextTitle[$i-1],
                'fieldLodTitle7' => $getTextTitle[$i-1],
                'fieldLodTitle8' => $getTextTitle[$i-1],
                'fieldLodTitle9' => $getTextTitle[$i-1],
                'fieldLodTitle10' => $getTextTitle[$i-1],
                'fieldLodTitle11' => $getTextTitle[$i-1],
                'fieldLodTitle12' => $getTextTitle[$i-1],
                'fieldLodTitle13' => $getTextTitle[$i-1],
                'fieldLodTitle14' => $getTextTitle[$i-1],
                'fieldLodTitle15' => $getTextTitle[$i-1],
                'fieldNameEducationalOrganization' => $nameEduProgram[$i-1],
                'fieldNameEducationalOrganization2' => $nameEduProgram[$i-1],
                'fieldCodeNameGroupSpecialities' => $enlargedNameAndCode[$i-1],
                'fieldCodeNameGroupSpecialities2' => $enlargedNameAndCode[$i-1],
                'fieldCodeNameLicensePrograms' => $licensedProgramNameAndCode[$i-1],
                'fieldCodeNameLicensePrograms2' => $licensedProgramNameAndCode[$i-1],
                'fieldCodeNameLicensePrograms3' => $licensedProgramNameAndCode[$i-1],
            );
            array_push($allData, $arrayData);
        }

        //Замена значений
        foreach($allData as $key => $value){
            foreach ($value as $k => $val) {
                if(is_null($value[$k])) {
                    $value[$k] = '';
                }
            }
            $allData[$key] = $value;;
        }

        return $allData;
    }

    /**
     * Код и наименование профессии
     *
     * @param $commissionMemberModel
     * @param $expertiseId
     * @param $fieldName
     * @return array
     */
    public function getCodeAndNameProfessions($commissionMemberModel, $expertiseId, $fieldName)
    {
        $expertiseProgramsId = $this->getProgramId($commissionMemberModel, $expertiseId);
        $eduProgramGroupSpec = [];
        $codeAndName = [];

        if($fieldName === 'fk_isgaEnlargedGroupSpecialities'){
            foreach ($expertiseProgramsId as $id) {
                $appProgramsModel = new \Model\Entities\isgaApplicationPrograms($id);
                $eduProgramGroupSpec[] = $appProgramsModel->getField($fieldName);
            }

            foreach ($eduProgramGroupSpec as $id){
                $enlargedModel = new \Model\Entities\isgaEnlargedGroupSpecialities($id);
                $code = $enlargedModel->getField('Code');
                $name = $enlargedModel->getField('Name');
                $codeAndName[] = $code . ' ' . $name;
            }

            return $codeAndName;

        }else{
            foreach ($expertiseProgramsId as $id) {
                $appProgramsModel = new \Model\Entities\isgaApplicationPrograms($id);
                $eduProgramGroupSpec[] = $appProgramsModel->getField($fieldName);
            }

            foreach ($eduProgramGroupSpec as $id){
                $enlargedModel = new \Model\Entities\eiisLicensedPrograms($id);
                $code = $enlargedModel->getField('Code');
                $name = $enlargedModel->getField('Name');
                $codeAndName[] = $code . ' ' . $name;
            }

            return $codeAndName;
        }
    }


    /**
     * Получаем реквизиты ОП
     *
     * @param $commissionMemberModel
     * @param $expertiseId
     * @return array
     */
    public function getRequisites($commissionMemberModel, $expertiseId)
    {
        $eduProgramType = [];
        $lodTitle = [];
        $getTextTitle = [];

        $expertiseProgramsId = $this->getProgramId($commissionMemberModel, $expertiseId);

        foreach ($expertiseProgramsId as $id){
            $appProgramsModel = new \Model\Entities\isgaApplicationPrograms($id);
            $eduProgramType[] = $appProgramsModel->getField('fk_eiisEduProgramTypes');
        }


        foreach ($eduProgramType as $type){
            $eduProgramTypesModel = new \Model\Entities\eiisEduProgramTypes($type);
            $lodTitle[] = $eduProgramTypesModel->getField('LodTitle');
        }

        foreach ($lodTitle as $title){
            if(stristr($title, 'программы')){
                $explodeTitle = explode('программы', $title);
                $getTextTitle[] = $explodeTitle[1];
            }else{
                $getTextTitle[] = $title;
            }
        }
        return $getTextTitle;
    }

    /**
     * Получаем имена организаций
     *
     * @param $commissionMemberModel
     * @param $expertiseId
     * @return array
     */
    public function getNameEduOrg($commissionMemberModel, $expertiseId)
    {
        $expertiseProgramsId = $this->getProgramId($commissionMemberModel, $expertiseId);
        $nameEduOrg = [];

        foreach ($expertiseProgramsId as $id){
            $appProgramsModel = new \Model\Entities\isgaApplicationPrograms($id);
            $nameEduOrg[] = $appProgramsModel->getField('Name');
        }
        return $nameEduOrg;
    }


    /**
     * Получаем id программ экспертизы
     *
     * @param $commissionMemberModel
     * @param $expertiseId
     * @return array
     */
    public function getProgramId($commissionMemberModel, $expertiseId)
    {
        $members = $commissionMemberModel->getAllByWhere(array(
            'expertise_id' => $expertiseId
        ));
        $expertiseProgramsId = [];

        foreach ($members as $member) {
            $expertiseProgramsId[] = $member->getField('expertise_program_id');
        }

        return $expertiseProgramsId;
    }


    /**
     * Генирация названия намесяца
     *
     * @param $month
     * @return mixed
     */
    public function generateMonthName($month)
    {
        switch ($month[1]) {
            case 1:
                $monthName = 'января';
                break;
            case 2:
                $monthName = 'февраля';
                break;
            case 3:
                $monthName = 'марта';
                break;
            case 4:
                $monthName = 'апреля';
                break;
            case 5:
                $monthName = 'мая';
                break;
            case 6:
                $monthName = 'июня';
                break;
            case 7:
                $monthName = 'июля';
                break;
            case 8:
                $monthName = 'августа';
                break;
            case 9:
                $monthName = 'сентября';
                break;
            case 10:
                $monthName = 'октября';
                break;
            case 11:
                $monthName = 'ноября';
                break;
            case 12:
                $monthName = 'декабря';
                break;
        }
        $month[1] = $monthName;
        return $month;
    }

    /**
     * Генирация полной даты
     *
     * @param $date
     * @return string
     */
    public function generateDate($date){
        $dateForm = date("d.m.Y", strtotime($date));
        $dateArr = explode(".", $dateForm);
        $fullDate = implode(" ", $this->generateMonthName($dateArr));
        return $fullDate;
    }

    /**
     * Название шаблона на сервер
     *
     * @return string
     */
    public function getTemplateName(){
        return 'Conclusion.docx';
    }

    /**
     * Название документа при скачивании
     *
     * @return string
     */
    public function getDocumentName() {
        return 'Заключение';
    }


    /**
     * Получаем кол-во програм
     *
     * @param $expertiseId
     * @return int
     */
    public function getCountProgram($expertiseId)
    {
//        $countPrograms = [];
//
//        $epxertiseProgram = new \Model\ExpertEntities\expertiseprogram();
//        $getPrograms = $epxertiseProgram->getAllByWhere(array(
//            'expertise_id' => $expertiseId,
//        ));
//        foreach ($getPrograms as $program){
//            $countPrograms[] = $program->getField('id');
//        }
//
//        return count($countPrograms);


        $countPrograms = [];

        $commissionMemberModel = new \Model\ExpertEntities\commissionmember();
        $members = $commissionMemberModel->getAllByWhere(array(
            'expertise_id' => $expertiseId
        ));
        foreach ($members as $program){
            $countPrograms[] = $program->getField('id');
        }

        return count($countPrograms);
    }


}