<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(

            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'ron' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/ron',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Ron\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]][/:id][/:id2][/:id3][/:id4]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'index',
                                'action'        => 'index',
                            ),
                        ),
                    ),
                ),
            ),
        )
    ),
/*
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'updateLicenses' => array(
                    'options' => array(
                        'route'    => 'updateLicenses',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateLicenses'
                        )
                    )
                ),
                'updateSchools' => array(
                    'options' => array(
                        'route'    => 'updateSchools',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateSchools'
                        )
                    )
                ),
                'updateCertificates' => array(
                    'options' => array(
                        'route'    => 'updateCertificates',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateCertificates'
                        )
                    )
                ),
                'updateAccrPrograms' => array(
                    'options' => array(
                        'route'    => 'updateAccrPrograms',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateAccrPrograms'
                        )
                    )
                ),

                'updateLicenseSupplements' => array(
                    'options' => array(
                        'route'    => 'updateLicenseSupplements',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateLicenseSupplements'
                        )
                    )
                ),
                'updateCertificateSupplements' => array(
                    'options' => array(
                        'route'    => 'updateCertificateSupplements',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateCertificateSupplements'
                        )
                    )
                ),
                'updateEduPrograms' => array(
                    'options' => array(
                        'route'    => 'updateEduPrograms',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateEduPrograms'
                        )
                    )
                ),
                'updateUGS' => array(
                    'options' => array(
                        'route'    => 'updateUGS',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateUGS'
                        )
                    )
                ),
                'updateLicPrograms' => array(
                    'options' => array(
                        'route'    => 'updateLicPrograms',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateLicPrograms'
                        )
                    )
                ),
                'updateApplications' => array(
                    'options' => array(
                        'route'    => 'updateApplications',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'updateApplications'
                        )
                    )
                ),
                'exchangeData' => array(
                    'options' => array(
                        'route'    => 'exchangeData',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'exchangeData'
                        )
                    )
                ),
                'serviceCreatePackage' => array(
                    'options' => array(
                        'route'    => 'serviceCreatePackage <packageId>',
                        'defaults' => array(
                            'controller' => 'Ron\Controller\Console',
                            'action' => 'serviceCreatePackage'
                        )
                    )
                )
            )
        )
    ),
*/
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',

        ),

        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Ron\Controller\Index' => 'Ron\Controller\IndexController',
            'Ron\Controller\Expertise' => 'Ron\Controller\ExpertiseController',
            /*'Ron\Controller\Declaration' => 'Ron\Controller\DeclarationController',
            'Ron\Controller\Reports' => 'Ron\Controller\ReportsController',
            'Ron\Controller\Console' => 'Ron\Controller\ConsoleController',*/
        ),
        'factories' => [
            'Ron/Controller/phpOffice'

        ]
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/ron'           => __DIR__ . '/../view/layout/layout.phtml',

            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'view_helpers' => array(
        'invokables'=> array(
            'pages' => 'Ron\View\Helper\Pages',
            'hasAccess' => 'Ron\View\Helper\HasAccess'
        )
    ),
);
