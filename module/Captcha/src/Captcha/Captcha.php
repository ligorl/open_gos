<?php
namespace Captcha;


class Captcha{
    function generateCaptcha() {
        $captcha = new \Zend\Captcha\Image();

        //var_dump(__DIR__);die;

        $captcha->setTimeout('300')
            ->setWordLen('6')
            ->setHeight('60')
            ->setFont(__DIR__.'/../../font/captcha.ttf')
            ->setImgDir(__DIR__.'/../../../../public/captcha_dir');

        $captcha->generate();    //command to generate session + create image

        return $captcha->getId();   //returns the ID given to session &amp; image
    }   //end function generateCaptcha

    //validates captcha response
    function validateCaptcha($id, $value) {
        $captchaId = $id;
        $captchaInput = $value;

        $captchaSession = new \Zend\Session\Container('Zend_Form_Captcha_' . $captchaId);
        $captchaIterator = $captchaSession->getIterator();
        $captchaWord = $captchaIterator['word'];

        if( $captchaWord ) {
            if( $captchaInput != $captchaWord ){
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}