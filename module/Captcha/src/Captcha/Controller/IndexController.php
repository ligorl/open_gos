<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Captcha\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Model;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $captcha = new \Captcha\Captcha();
        $id = $captcha->generateCaptcha();

        echo $id;
        die;
    }

    public function updateAction(){
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()){
            if($this->params("id") != null && strlen($this->params("id")) == 32 ){
                $id = $this->params("id");
                $image = __DIR__.'/../../../../../public/captcha_dir/' . $id.'.png';
                if (file_exists($image) == true) {
                    unlink($image);
                }
            }
        }
        die;
    }
}
