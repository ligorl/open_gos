<?php

namespace LicensesReestr\Service;

use LicensesReestr\Exception\LicensesReestrException;
use Reestr\Entity\AbstractEntity;
use LicensesReestr\Options\ModuleOptions;
use Reestr\Model\SearchEngine;

/**
 * Импорт данных из XML, после импорта вызывать хранимую процедуру setReferences в БД
 *
  BEGIN
  update decision as d inner join license as l on d.license_guid = l.guid
  set d.license_id = l._id where d.license_id is null;
  update license as l inner join eo on l.eo_guid = eo.guid
  set l.eo_id = eo._id where l.eo_id is null;
  update supplement as s inner join eo on s.eo_guid = eo.guid
  set s.eo_id = eo._id where s.eo_id is null;
  update supplement as s inner join license as l on s.license_guid = l.guid
  set s.license_id = l._id where s.license_id is null;
  update program as p inner join supplement as s on p.supplement_guid = s.guid
  set p.supplement_id = s._id where p.supplement_id is null;
  END
 *
 * @author Дмитрий Бубякин
 */
class ImportService
{

    /**
     * 
     * @var \Reestr\Model\SearchEngine
     */
    private $searchEngine;
    /**
     * id текущего пакета
     * @var int
     */
    private $packageId;
    /**
     * Время начала импорта
     * @var type
     */
    private $startTime;
    /**
     * Ошибки возникшие при импорте
     * @var array
     */
    private $errors = [];
    /**
     * Объекты для импорта
     * @var array
     */
    private $toImport = [
        ModuleOptions::DECISION   => [],
        ModuleOptions::EO         => [],
        ModuleOptions::LICENSE    => [],
        ModuleOptions::PROGRAM    => [],
        ModuleOptions::SUPPLEMENT => []
    ];

    /**
     *
     * @param SearchEngine $searchEngine
     */
    public function __construct(SearchEngine $searchEngine)
    {
        $this->searchEngine = $searchEngine;
        $this->startPackage();
    }

    /**
     * Импорт xml-файла в базу
     * @param string $path Путь к XML-файлу
     * @return array ошибки
     */
    public function import($path)
    {
        if (!file_exists($path)) {
            $this->finishPackage('Import file not found in ' . $path);
            throw new LicensesReestrException('Import file not found in ' . $path);
        }
        $this->parse($path);
        $this->apply();
        $this->finishPackage();
        unlink($path);
        return $this->errors;
    }

    /**
     * Получаем название таблицы и поле, которое соответствует XML-полю
     * @param string $name
     * @return string|null
     */
    protected function map($name)
    {
        if (!array_key_exists($name, ModuleOptions::$map)) {
            return null;
        }
        return ModuleOptions::$map[$name];
    }

    /**
     * Парсим XML файл
     * @param string $path
     */
    protected function parse($path)
    {
        $content = file_get_contents($path);
        $licenses = new \SimpleXMLElement($content);
        foreach ($licenses as $license) {
            $this->parseLicense(get_object_vars($license));
        }
    }

    /**
     * Парсим лицензии
     * @param array $properties
     */
    protected function parseLicense(array $properties)
    {
        $allowedDependences = [ModuleOptions::LICENSE_ORGAN, ModuleOptions::LICENSE_STATE];
        $license = new AbstractEntity(ModuleOptions::LICENSE);
        $eoProperties = [];
        foreach ($properties as $key => $property) {
            $newKey = $this->map($key);
            if ($newKey && false === strpos($newKey, '.')) {
                $license->$newKey = $property;
            } else if (strpos($newKey, '.') !== false) {
                $table = substr($newKey, 0, strpos($newKey, '.'));
                if ($table === ModuleOptions::EO) {
                    $name = substr($newKey, strpos($newKey, '.') + 1);
                    $eoProperties[$name] = $property;
                }
                $this->setDependence($license, $table, $property, $allowedDependences);
            }
            if ($newKey === 'eo_guid') {
                $license->eo_guid = $license->guid . '_' . $license->eo_guid;
                $eoProperties['guid'] = $license->eo_guid;
            }
        }
        if (empty($license->eo_guid)) {
            $this->errors[] = sprintf('Empty license.[%s]', print_r($license->getProperties(), true));
            return;
        }
        if (isset($this->toImport[ModuleOptions::LICENSE][$license->guid])) {
            $this->toImport[ModuleOptions::LICENSE][$license->guid]->mergeWith($license);
        } else {
            $this->toImport[ModuleOptions::LICENSE][$license->guid] = $license;
        }
        $this->parseEo($eoProperties);
        foreach ($properties['Supplements'] as $supplement) {
            $this->parseSupplement(get_object_vars($supplement));
        }
        foreach ($properties['Decisions'] as $decision) {
            $this->parseDecision(get_object_vars($decision));
        }
    }

    /**
     * Парсим приложения
     * @param array $properties
     */
    protected function parseSupplement(array $properties)
    {
        $supplement = new AbstractEntity(ModuleOptions::SUPPLEMENT);
        $eoProperties = [];
        foreach ($properties as $key => $property) {
            $newKey = $this->map($key);
            if ($newKey && false === strpos($newKey, '.')) {
                $supplement->$newKey = $property;
            } else if (strpos($newKey, '.') !== false) {
                $table = substr($newKey, 0, strpos($newKey, '.'));
                if ($table === ModuleOptions::EO) {
                    $name = substr($newKey, strpos($newKey, '.') + 1);
                    $eoProperties[$name] = $property;
                    if($name === 'guid'){
                        $supplement->eo_guid = $supplement->guid . '_' . $property;
                        $eoProperties[$name] = $supplement->eo_guid;
                    }
                }
            }
        }
        if (isset($this->toImport[ModuleOptions::SUPPLEMENT][$supplement->guid])) {
            $this->toImport[ModuleOptions::SUPPLEMENT][$supplement->guid]->mergeWith($supplement);
        } else {
            $this->toImport[ModuleOptions::SUPPLEMENT][$supplement->guid] = $supplement;
        }
        $this->parseEo($eoProperties);
        foreach ($properties['Programms'] as $program) {
            $this->parseProgram(get_object_vars($program));
        }
    }

    /**
     * Парсим решения
     * @param array $properties
     */
    protected function parseDecision(array $properties)
    {
        $decision = new AbstractEntity(ModuleOptions::DECISION);
        foreach ($properties as $key => $property) {
            $newKey = $this->map($key);
            if ($newKey && false === strpos($newKey, '.')) {
                $decision->$newKey = $property;
            }
        }
        if (isset($this->toImport[ModuleOptions::DECISION][$decision->guid])) {
            $this->toImport[ModuleOptions::DECISION][$decision->guid]->mergeWith($decision);
        } else {
            $this->toImport[ModuleOptions::DECISION][$decision->guid] = $decision;
        }
    }

    /**
     * Парсим программы
     * @param array $properties
     */
    protected function parseProgram(array $properties)
    {
        $program = new AbstractEntity(ModuleOptions::PROGRAM);
        foreach ($properties as $key => $property) {
            $newKey = $this->map($key);
            if ($newKey && false === strpos($newKey, '.')) {
                $program->$newKey = $property;
            }
        }

        if (isset($this->toImport[ModuleOptions::PROGRAM][$program->guid])) {
            $this->toImport[ModuleOptions::PROGRAM][$program->guid]->mergeWith($program);
        } else {
            $this->toImport[ModuleOptions::PROGRAM][$program->guid] = $program;
        }
    }

    /**
     * Парсим образовательные организации
     * @param array $properties
     */
    protected function parseEo(array $properties)
    {
        $allowedDependences = [ModuleOptions::REGION];
        $eo = new AbstractEntity(ModuleOptions::EO);
        foreach ($properties as $key => $property) {
            if (false === strpos($key, '.')) {
                $eo->$key = $property;
            } else if (strpos($key, '.') !== false) {
                $table = substr($key, 0, strpos($key, '.'));
                $this->setDependence($eo, $table, $property, $allowedDependences);
            }
        }
        if (isset($this->toImport[ModuleOptions::EO][$eo->guid])) {
            $this->toImport[ModuleOptions::EO][$eo->guid]->mergeWith($eo);
        } else {
            $this->toImport[ModuleOptions::EO][$eo->guid] = $eo;
        }
    }

    /**
     * Устанавливаем значение зависимости
     * @param AbstractEntity $entity
     * @param string $table
     * @param string $property
     * @param array $allowedDependences разрешенные таблицы
     */
    protected function setDependence(AbstractEntity $entity, $table, $property, array $allowedDependences)
    {
        if (!in_array($table, $allowedDependences)) {
            return;
        }
        if (!$this->searchEngine->has($table)) {
            throw new LicensesReestrException('Table ' . $table . ' is not exists');
        }
        $field = $table . '_id';
        $entity->$field = $this->getDependenceId($table, ['name' => $property,]);
    }

    private function apply()
    {
        $this->checkUpdates();
        $dataSet = [
            ModuleOptions::PROGRAM    => $this->prepareMultiDataSet(ModuleOptions::PROGRAM),
            ModuleOptions::DECISION   => $this->prepareMultiDataSet(ModuleOptions::DECISION),
            ModuleOptions::EO         => $this->prepareMultiDataSet(ModuleOptions::EO),
            ModuleOptions::SUPPLEMENT => $this->prepareMultiDataSet(ModuleOptions::SUPPLEMENT),
            ModuleOptions::LICENSE    => $this->prepareMultiDataSet(ModuleOptions::LICENSE),
        ];
        foreach ($dataSet as $table => &$set) {
            $this->searchEngine->multiSave($table, $set, '_id', $this->errors);
        }
        $this->searchEngine->executeSql('call setReferences();call setActive()');
    }

    private function checkUpdates()
    {
        $params = [
            'table'   => null,
            'columns' => [
                '_id',
                'guid',
            ],
            'where'   => [
                'in' => [
                    'guid' => [],
                ],
            ],
        ];
        foreach ($this->toImport as $table => &$entities) {
            $guids = $this->extractGuids($entities);
            $params['table'] = $table;
            $params['where']['in']['guid'] = $guids;
            if (empty($guids)) {
                continue;
            }
            $result = $this->searchEngine->select($params);
            $this->updateId($table, $result);
        }
    }

    private function updateId($table, array &$resultSet)
    {
        /* @var $entity AbstractEntity */
        foreach ($resultSet as $entity) {
            if ($entity instanceof AbstractEntity) {
                $this->toImport[$table][$entity->guid]->_id = $entity->_id;
                $this->toImport[$table][$entity->guid]->update_date = $this->getCurrentSqlDate();
            }
        }
    }

    /**
     * Извлекаем guid'ы из объектов
     * @param array $entities
     * @return array
     */
    private function extractGuids(array &$entities)
    {
        $guids = [];
        foreach ($entities as $entity) {
            $guids[] = $entity->guid;
        }
        return $guids;
    }

    private function prepareMultiDataSet($table)
    {
        $dataSet = [];
        foreach ($this->toImport[$table] as $entity) {
            $entity->package_id = $this->packageId;
            if (empty($entity->update_date)) {
                $entity->create_date = $this->getCurrentSqlDate();
            }
            $dataSet[] = $entity->prepareDataSet();
        }
        return $dataSet;
    }

    /**
     * Записываем в базу информацию о импорте
     */
    private function startPackage()
    {
        $this->startTime = microtime(true);
        $package = new AbstractEntity(ModuleOptions::PACKAGE);
        $package->import_date = $this->getCurrentSqlDate();
        $this->packageId = $this->searchEngine->insert($package);
    }

    /**
     * Дописываем в базу информацию о состоянии импорта
     * @param string $error
     */
    private function finishPackage($error = null)
    {
        $package = new AbstractEntity(ModuleOptions::PACKAGE);
        if ($error) {
            $package->error_log = $error;
            $this->searchEngine->update($package, ['_id' => $this->packageId]);
            return;
        }
        $package->state = 'VALID';
        $package->log = vsprintf('Imported[%0.3f sec]: supplement - %d, eo - %d, license - %d, decision - %d, program - %d. [%s]',
                                 [
            microtime(true) - $this->startTime,
            count($this->toImport[ModuleOptions::SUPPLEMENT]),
            count($this->toImport[ModuleOptions::EO]),
            count($this->toImport[ModuleOptions::LICENSE]),
            count($this->toImport[ModuleOptions::DECISION]),
            count($this->toImport[ModuleOptions::PROGRAM]),
            implode(', ',$this->getGuids(ModuleOptions::LICENSE)),
        ]);
        if (!empty($this->errors)) {
            $package->error_log = implode(PHP_EOL, $this->errors);
        }
        $this->searchEngine->update($package, ['_id' => $this->packageId]);
    }

    private function getGuids($table){
        return array_keys($this->toImport[$table]);
    }

    /**
     * Получаем _id для связи
     * @param string $table
     * @param array $where
     * @return int|null
     */
    private function getDependenceId($table, array $where)
    {
        $result = $this->searchEngine->getColumnsByWhere($table, ['_id'], $where);
        if ($result instanceof AbstractEntity) {
            return $result->_id;
        }
        $entity = new AbstractEntity($table);
        foreach ($where as $key => $value) {
            if (empty($value)) {
                return null;
            }
            $entity->$key = $value;
        }
        $this->insertDependence($entity);
        return $this->getDependenceId($table, $where);
    }

    /**
     * Добавляем зависимость в базу(если не существует)
     * @param AbstractEntity $entity
     */
    private function insertDependence(AbstractEntity $entity)
    {
        $entity->create_date = $this->getCurrentSqlDate();
        $entity->package_id = $this->packageId;
        $this->searchEngine->insert($entity);
    }

    /**
     * Текущая дата в MySQL формате
     * @return date
     */
    private function getCurrentSqlDate()
    {
        return date('Y-m-d H:i:s');
    }

}