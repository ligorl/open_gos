<?php

namespace LicensesReestr\Service;

use LicensesReestr\Exception\LicensesReestrException;
use Reestr\Entity\AbstractEntity;
use LicensesReestr\Options\ModuleOptions;
use Reestr\Model\SearchEngine;

/**
 * 
 *
 * @author Дмитрий Бубякин
 */
class HistoryImportService
{

    /**
     * 
     * @var \Reestr\Model\SearchEngine
     */
    private $searchEngine;
    /**
     * Данные из xml
     * @var array|AbstractEntity
     */
    private $dataSet = [];
    /**
    * Пропущенные дела
    * @var array
    */
    private $skipped = [];

    /**
     *
     * @param SearchEngine $searchEngine
     */
    public function __construct(SearchEngine $searchEngine)
    {
        $this->searchEngine = $searchEngine;
    }

    /**
     * Импорт xml-файла в базу
     * @param string $path Путь к XML-файлу
     * @return array ошибки
     */
    public function import($path)
    {
        if (!file_exists($path)) {
            $this->finishPackage('Import file not found in ' . $path);
            throw new LicensesReestrException('Import file not found in ' . $path);
        }
        $this->parse($path);
        $this->checkUpdates();
        $preparedDataSet = $this->prepareDataSet();
        $errors = [];
        $this->finishPackage();
        $this->searchEngine->multiSave(ModuleOptions::DEAL_HISTORY, $preparedDataSet, '_id',$errors);
        unlink($path);
        return $errors;
    }

    protected function prepareDataSet()
    {
        $dataSet = [];
        foreach ($this->dataSet as $entity) {
            $dataSet[] = $entity->prepareDataSet();
        }
        return $dataSet;
    }

    /**
     * Получаем название таблицы и поле, которое соответствует XML-полю
     * @param string $name
     * @return string|null
     */
    protected function map($name)
    {
        if (!array_key_exists($name, ModuleOptions::$dealMap)) {
            return null;
        }
        return ModuleOptions::$dealMap[$name];
    }

    /**
     * Парсим XML файл
     * @param string $path
     */
    protected function parse($path)
    {
        $content = file_get_contents($path);
        $deals = new \SimpleXMLElement($content);
        foreach ($deals as $deal) {
            $this->parseDealHistory(get_object_vars($deal));
        }
    }

    /**
     * Парсим
     * @param array $properties
     */
    protected function parseDealHistory(array $properties)
    {
        $deal = new AbstractEntity(ModuleOptions::DEAL_HISTORY);
        foreach ($properties as $key => $property) {
            $newKey = $this->map($key);
            $deal->$newKey = $property;
        }
        $deal->create_date = $this->getCurrentSqlDate();
        $ignoredStates = [
            'Возврат',
            'Подготовка акта',
            'Печать',
            'Направление уведомления',
            'Проверка',
            'Прием и регистрация'
        ];
        if(!empty($deal->state) && !in_array($deal->state, $ignoredStates)){
            $this->dataSet[$deal->guid] = $deal;
        }else{
            $this->skipped[] = $deal->guid;
        }
    }

    private function checkUpdates()
    {
        $params = [
            'table'   => ModuleOptions::DEAL_HISTORY,
            'columns' => [
                '_id',
                'guid',
            ],
            'where'   => [
                'in' => [
                    'guid' => $this->extractGuids($this->dataSet),
                ],
            ],
        ];
        $result = $this->searchEngine->select($params);
        $this->updateId($result);
    }

    /**
     * Извлекаем guid'ы из объектов
     * @param array $entities
     * @return array
     */
    private function extractGuids(array &$entities)
    {
        $guids = [];
        foreach ($entities as $entity) {
            $guids[] = $entity->guid;
        }
        return $guids;
    }

    private function updateId(array &$resultSet)
    {
        /* @var $entity AbstractEntity */
        foreach ($resultSet as $entity) {
            if ($entity instanceof AbstractEntity) {
                $this->dataSet[$entity->guid]->_id = $entity->_id;
                $this->dataSet[$entity->guid]->create_date = null;
                $this->dataSet[$entity->guid]->update_date = $this->getCurrentSqlDate();
            }
        }
    }

    /**
     * Текущая дата в MySQL формате
     * @return date
     */
    private function getCurrentSqlDate()
    {
        return date('Y-m-d H:i:s');
    }

    private function finishPackage($error = null)
    {
        $package = new AbstractEntity(ModuleOptions::PACKAGE);
        $package->import_date = $this->getCurrentSqlDate();
        $package->error_log = $error;
        $package->state = $error ? 'INVALID' : 'VALID';
        $package->log = sprintf('History [%d]: %s, skipped %s',
            count($this->dataSet), 
            implode(', ', $this->extractGuids($this->dataSet)),
            implode(', ', $this->skipped)
        );
        $this->searchEngine->insert($package);
    }

}