<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace LicensesReestr\Service;

use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Log\Formatter\Simple as SimpleFormatter;
use Zend\Filter\Compress;
use XMLWriter;
use Reestr\Model\SearchEngine;
use LicensesReestr\Options\ModuleOptions;

/**
 * Description of ExportOpenDataService
 *
 * @author dmitry
 */
class ExportOpenDataService
{

    const LOG_FILE = 'exportOpenData.log';
    const XML_FILE_PATH = 'public/opendata-lic/';
    const LOG_FILE_PATH = 'public/log/';

    /**
     *
     * @var SearchEngine
     */
    protected $searchEngine = null;
    /**
     *
     * @var XMLWriter
     */
    protected $writer = null;
    /**
     * Имя xml файла
     * @var string
     */
    protected $filename;
    /**
     * Имя файла архива
     * @var string
     */
    protected $zipFile;
    /**
     * Полный путь к файлу
     * @var string
     */
    protected $fullpath;
    /**
     *
     * @var Logger
     */
    protected $logger = null;
    /**
     * Кол-во лицензий хранимых в памяти
     * @var int
     */
    protected $limit = null;
    /**
     * Обработано лицензий
     * @var int
     */
    protected $offset = null;
    /**
     * Всего лицензий
     * @var int
     */
    protected $total = 0;
    /**
     * Время начала обработки порции данных
     * @var float
     */
    protected $time = null;
    /**
     *  Время начала обработки данных
     * @var type
     */
    protected $totalTime = null;
    /**
     * Затраченная память
     * @var float
     */
    protected $memory = null;
    /**
     * Затраченная память, максимум
     * @var float
     */
    protected $maxMemory = null;
    /**
     * Состояния лицензий
     * @var array|string
     */
    protected $licenses = [];
    protected $supplements = [];
    protected $programs = [];
    protected $eos = [];
    protected $support = [];

    /**
     *
     * @param SearchEngine $searchEngine
     * @param int $limit
     * @param int $offset
     */
    public function __construct(SearchEngine $searchEngine, $limit = 100, $offset = 0 , $total = 0)
    {
        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', '900');
        $this->searchEngine = $searchEngine;
        $this->limit = $limit;
        $this->offset = $offset;
        //$this->filename = sprintf('data-%s-structure-20160713.xml', date('Ymd'));
        $this->filename = sprintf('data-%s-structure-20150421.xml', date('Ymd'));
        $this->fullpath = self::XML_FILE_PATH . $this->filename;
        $this->zipFile = str_replace('xml', 'zip', $this->fullpath);
        $this->total($total );
        $this->initLogger();
    }

    /**
     * Экспорт
     */
    public function export()
    {
        try {
            $this->totalTime = microtime(true);
            $this->startXml();
            $this->bulk();
        } catch (\Exception $e) {
            $this->logger->err($e->getMessage());
        }
    }

    /**
     * Обрабатываем часть данных
     */
    protected function bulk()
    {
        $this->clear();
        $this->time = microtime(true);
        $this->loadLicensesBulk();
        $this->loadSupplementsBulk();
        $this->loadProgramsBulk();
        $this->memory = memory_get_usage() / 1024 / 1024;
        $this->write();
        $this->nextBulk();
    }

    /**
     * Инициализируем вывод логов
     */
    protected function initLogger()
    {
        $this->createDirectory(self::LOG_FILE_PATH);
        $this->logger = new Logger();

        $logWriter = new Stream(self::LOG_FILE_PATH . self::LOG_FILE);
        $phpWriter = new Stream('php://output');

        $formatter = new SimpleFormatter('[%timestamp%] %priorityName%: %message%', 'd-m-Y H:i:s');
        $logWriter->setFormatter($formatter);
        $phpWriter->setFormatter($formatter);

        $this->logger->addWriter($logWriter);
        $this->logger->addWriter($phpWriter);
        $this->logger->info(sprintf('License export started. Total %d. File %s', $this->total, $this->filename));
    }

    /**
     * Создаем xml файл и записываем в него корневой элемент
     * @throws \Exception
     */
    protected function startXml()
    {
        $this->createDirectory(self::XML_FILE_PATH);
        //$this->removeFile($this->zipFile);
        $this->writer = new XMLWriter();
        $this->writer->openMemory();
        $this->writer->startDocument('1.0', 'UTF-8');
        $this->writer->startElement('licenses');
        file_put_contents($this->fullpath, $this->writer->flush(true));
        if (!file_exists($this->fullpath)) {
            throw new \Exception(sprintf('Can\'t create file %s', $this->fullpath));
        }
    }

    /**
     * Удаляем файл
     * @param string $filename
     */
    protected function removeFile($filename)
    {
        if (file_exists($filename)) {
            unlink($filename);
            $this->logger->info(sprintf('Removed %s', $filename));
        }
    }

    /**
     * Создаем папку
     * @param string $dirname
     */
    protected function createDirectory($dirname)
    {
        if (!file_exists($dirname)) {
            mkdir($dirname);
            if ($this->logger) {
                $this->logger->info(sprintf('Directory %s created', $dirname));
            }
        }
    }

    /**
     * Завершаем формирование xml
     */
    protected function finishXml()
    {
        $this->writer->endElement(); //licenses
        file_put_contents($this->fullpath, $this->writer->flush(true), FILE_APPEND);

        $time = microtime(true) - $this->totalTime;
        $this->logger->info(
          sprintf('Xml successfully created. Total time %0.2f s. Max memory %0.2f Mb', $time, $this->maxMemory)
        );
        $this->zip();
    }

    /**
     * Сжимаем файл
     */
    protected function zip()
    {
        $beforeSize = filesize($this->fullpath) / 1024 / 1024;
        $startTime = microtime(true);
        $this->logger->info('File zipping started');

        $zipFileName = str_replace('xml', 'zip', $this->filename);
        $this->removeFile( $this->zipFile );
        $filter = new Compress([
            'adapter' => 'Zip',
            'options' => [
                'archive' => $this->zipFile,
            ],
        ]);
        $compressed = $filter->filter($this->fullpath);
        $files = array_values(array_diff(scandir(self::XML_FILE_PATH), ['.', '..', '.svn', $zipFileName]));
        if ($compressed) {
            foreach ($files as $file) {
                $fileArray = explode('.', $file);
                $fileExt = end($fileArray);
                if( 'zip' == $fileExt ){
                    continue;
                }
                $this->removeFile(self::XML_FILE_PATH . $file);
            }
        } else {
            $this->logger->err('Can\'t compress the file');
        }

        $time = microtime(true) - $startTime;
        $this->logger->info(sprintf('File zipping finished. Time %0.2f s', $time));
        $afterSize = filesize($this->zipFile) / 1024 / 1024;
        $this->logger->info(sprintf('Before %0.2f Mb. After %0.2f Mb', $beforeSize, $afterSize));
        $this->logger->info('Done' . PHP_EOL);
    }

    /**
     * Пишем полученные данные в xml
     */
    protected function write()
    {
        $this->writeLicense();
        file_put_contents($this->fullpath, $this->writer->flush(true), FILE_APPEND);
    }

    /**
     * Пишем лицензии
     */
    protected function writeLicense()
    {
        foreach ($this->licenses as $guid => &$license) {
            $this->writer->startElement('license');
            $this->writeArray($license);
            $this->writer->startElement('supplements');
            $this->writeSupplement($guid);
            $this->writer->endElement(); //supplements
            $this->writer->endElement(); //license
        }
        $this->licenses = [];
    }

    /**
     * Пишем приложения
     * @param string $licenseGuid
     */
    protected function writeSupplement($licenseGuid)
    {
        foreach ($this->supplements[$licenseGuid] as $guid => &$supplement) {
            $this->writer->startElement('supplement');
            $this->writeArray($supplement);
            $this->writer->startElement('licensedPrograms');
            $this->writePrograms($guid);
            $this->writer->endElement(); //licensedPrograms
            $this->writer->endElement(); //supplement
        }
        $this->supplements[$licenseGuid] = [];
    }

    /**
     * Пишем программы
     * @param string $supplementGuid
     */
    protected function writePrograms($supplementGuid)
    {
        foreach ($this->programs[$supplementGuid] as &$program) {
            $this->writer->startElement('licensedProgram');
            $this->writeArray($program);
            $this->writer->endElement(); //licensedProgram
        }
        $this->programs[$supplementGuid] = [];
    }

    /**
     * Пишем массив в XML
     * @param array $array
     */
    protected function writeArray(array &$array)
    {
        foreach ($array as $key => $value) {
            $this->writer->writeElement($key, $value);
        }
    }

    /**
     * Обрабатываем следующую порцию данных
     */
    protected function nextBulk()
    {
        $this->offset += $this->limit;
        $percent = 100.0 / (float) $this->total * $this->offset;
        $memory = $this->memory;
        $this->maxMemory = $memory > $this->maxMemory ? $memory : $this->maxMemory;
        $time = microtime(true) - $this->time;
        $this->logger->info(vsprintf('Processed %0.2f%% (%d). Memory %0.2f Mb. Time %0.2f s',
                                     [
            $percent,
            $this->offset,
            $memory,
            $time,
        ]));


        if ($this->offset < $this->total ) {
            $this->bulk();
        } else {
            $this->finishXml();
        }
    }

    /**
     * Общее число лицензий
     */
    protected function total( $total  = 0)
    {
        $total = (int) $total;
        $result = $this->searchEngine->select([
            'table'   => 'license',
            'columns' => [
                new \Zend\Db\Sql\Expression('count(_id) as total'),
            ],
        ]);        
        $this->total = (int) current($result)->total;
        if( !empty($total) && $total < $this->total){
            $this->total = $total;
        }
        
    }

    /**
     * Загружаем лицензии
     */
    protected function loadLicensesBulk()
    {
        $params = [
            'table'   => 'license',
            'columns' => [
                '_id',
                'guid',
                'eo_guid',
                'number',
                'issue_date',
                'end_date',
            ],
            'join'    => [
                'eo'            => [
                    'alias'   => 'e',
                    'on'      => 'e._id = license.eo_id',
                    'columns' => [
                        'name',
                        'short_name',
                        'type',
                        'address',
                    ],
                ],
                'license_state' => [
                    'alias'   => 'ls',
                    'on'      => 'ls._id = license.license_state_id',
                    'columns' => [
                        'license_state' => 'name',
                    ],
                ],
                'license_organ' => [
                    'alias'   => 'lo',
                    'on'      => 'lo._id = license.license_organ_id',
                    'columns' => [
                        'license_organ' => 'name',
                    ],
                ],
            ],
            'limit'   => $this->limit,
            'offset'  => $this->offset,
        ];

        foreach ($this->searchEngine->select($params) as $item) {
            if (empty($item)) {
                continue;
            }
            $item->noError = true;
            $this->licenses[$item->guid] = [
                'sysGuid'        => $item->guid,
                'schoolGuid'     => $this->trimPart($item->eo_guid),
                'statusName'     => $item->license_state,
                'schoolName'     => $item->name,
                'shortName'      => $item->short_name,
                'schoolTypeName' => $item->type,
                'lawAddress'     => $item->address,
                'orgName'        => $item->license_organ,
                'regNum'         => $item->number,
                'dateLicDoc'     => $item->issue_date,
                'dateEnd'        => $item->end_date,
            ];
            $this->support['ids'][] = $item->_id;
            $this->support['org'][$item->guid] = $item->license_organ;
            $this->supplements[$item->guid] = [];
        }
    }

    /**
     * Загружаем приложения
     */
    protected function loadSupplementsBulk()
    {
        if (empty($this->support['ids'])) {
            return;
        }
        $params = [
            'table'   => 'supplement',
            'columns' => [
                '_id',
                'license_guid',
                'number',
                'state_code',
                'eo_guid',
                'doc_number',
                'guid',
                'issue_date',
                'guid',
            ],
            'join'    => [
                'eo' => [
                    'alias'   => 'e',
                    'on'      => 'e._id = supplement.eo_id',
                    'columns' => [
                        'name',
                        'short_name',
                        'type',
                        'address',
                    ],
                ],
            ],
            'where'   => [
                'in' => [
                    'supplement.license_id' => $this->support['ids'],
                ],
            ],
        ];
        $this->support['ids'] = [];
        foreach ($this->searchEngine->select($params) as $item) {
            if (empty($item)) {
                continue;
            }
            $item->noError = true;
            $this->supplements[$item->license_guid][$item->guid] = [
                'licenseFK'  => $item->license_guid,
                'number'     => $item->number,
                'statusName' => $this->get(ModuleOptions::$supplementStatesMap, $item->state_code),
                'schoolGuid' => $this->trimPart($item->eo_guid),
                'schoolName' => $item->name,
                'shortName'  => $item->short_name,
                'lawAddress' => $item->address,
                'orgName'    => $this->support['org'][$item->license_guid],
                'numLicDoc'  => $item->doc_number,
                'dateLicDoc' => $item->issue_date,
                'sysGuid'    => $item->guid,
            ];
            $this->support['ids'][] = $item->_id;
            $this->programs[$item->guid] = [];
        }
    }

    /**
     * Загружаем программы
     */
    protected function loadProgramsBulk()
    {
        if (empty($this->support['ids'])) {
            return;
        }
        $params = [
            'table'   => 'program',
            'columns' => [
                'supplement_guid',
                'type',
                'code',
                'name',
                'level',
                'qualification',
                'guid',
            ],
            'where'   => [
                'in' => [
                    'supplement_id' => $this->support['ids'],
                ],
            ],
        ];
        foreach ($this->searchEngine->select($params) as $item) {
            if (empty($item)) {
                continue;
            }
            $item->noError = true;
            $this->programs[$item->supplement_guid][] = [
                'supplementFk'      => $item->supplement_guid,
                'eduProgramType'    => $item->type,
                'code'              => $item->code,
                'name'              => $item->name,
                'eduLevelName'      => $item->level,
                'eduProgramKind'    => '',
                'qualificationCode' => '',
                'qualificationName' => $item->qualification,
                'sysGuid'           => $item->guid,
            ];
        }
        $this->support = [];
    }

    /**
     * Очищаем массивы
     */
    protected function clear()
    {
        $this->licenses = [];
        $this->supplements = [];
        $this->programms = [];
        $this->eos = [];
        $this->support = [];
    }

    /**
     * Получаем значение массива или значение по умолчанию, если нет
     * @param array $values
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    protected function get(array $values, $name, $default = null)
    {
        return empty($values[$name]) ? $default : $values[$name];
    }

    /**
     * guid1_guid2 => guid2
     * @param string $str
     * @return string
     */
    protected function trimPart($str)
    {
        $pos = (int) strpos($str, '_') + 1;
        return substr($str, $pos);
    }

    /**
     * Получаем файлы в папке с XML
     * @param int $count
     * @return array
     */
    public static function getLastFiles($count)
    {
        $files = glob(static::XML_FILE_PATH . '*.zip');
        rsort($files);
        array_map(function($item) {
            echo date('H:i:s', filemtime($item)) . " $item\n";
        }, $files);
        return $files;
    }

}