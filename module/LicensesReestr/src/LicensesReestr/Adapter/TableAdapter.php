<?php

namespace LicensesReestr\Adapter;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Reestr\Entity\AbstractEntity;
use LicensesReestr\Exception\LicensesReestrException;
use Reestr\Adapter\TableAdapterInterface;

/**
 * Description of TableAdapter
 *
 * @author Дмитрий Бубякин
 */
class TableAdapter implements TableAdapterInterface {

    const DATABASE_NAME = 'lic_reestr';

    /**
     *
     * @var AdapterInterface 
     */
    protected $dbAdapter;

    /**
     * 
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator) {
        $this->dbAdapter = $serviceLocator->get(self::DATABASE_NAME);
    }

    /**
     * Возвращает шлюз для объекта
     * @param AbstractEntity $prototype
     * @return TableGateway
     * @throws ReestrException
     */
    public function getTableGateway(AbstractEntity $prototype) {
        if (!$prototype->getTable()) {
            throw new LicensesReestrException('Table is not defined');
        }
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype($prototype);
        return new TableGateway($prototype->getTable(), $this->dbAdapter, null, $resultSetPrototype);
    }

}
