<?php

namespace LicensesReestr\Options;

/**
 * Description of ModuleOptions
 *
 * @author dima
 */
class ModuleOptions
{

    //Названия таблиц бд
    const DECISION = 'decision';
    const EO = 'eo';
    const LICENSE = 'license';
    const LICENSE_ORGAN = 'license_organ';
    const LICENSE_STATE = 'license_state';
    const PROGRAM = 'program';
    const REGION = 'region';
    const SUPPLEMENT = 'supplement';
    const PACKAGE = 'package';
    const DEAL_HISTORY = 'deal_history';

    //параметры импорта
    /**
     * Сопоставление полей из XML-файла лицензий и БД
     * Если поле должно быть в конкретно определенной таблице, то пишем имя этой таблицы, затем через точку
     * указываем поле
     * @var array
     */
    public static $map = [
        //xml => db
        //license
        'Id'                              => 'guid',
        'EOId'                            => 'eo_guid',
        'ControlOrganName'                => 'license_organ.name',
        'EOName'                          => 'eo.name',
        'EOShortName'                     => 'eo.short_name',
        'EOLawAddress'                    => 'eo.address',
        'EOGosRegNum'                     => 'eo.ogrn',
        'Kpp'                             => 'eo.kpp',
        'EOInn'                           => 'eo.inn',
        'OrgFormName'                     => 'eo.form',
        'EducationalOrganizationTypeName' => 'eo.type',
        'RegionName'                      => 'eo.region.name',
        'ChargePosition'                  => 'eo.charge_position',
        'ChargeFio'                       => 'eo.charge_name',
        'Phones'                          => 'eo.phone',
        'Faxes'                           => 'eo.fax',
        'Mails'                           => 'eo.email',
        'Www'                             => 'eo.site',
        'SerDoc'                          => 'doc_serie',
        'NumDoc'                          => 'doc_number',
        'DateLicDoc'                      => 'issue_date',
        'LicenseRegNum'                   => 'number',
        'LodStateName'                    => 'license_state.name',
        'DateEnd'                         => 'end_date',
        'Reorganization'                  => 'reorganization',
        'ReorganizationType'              => 'reorganization_type',
        //supplement
        'LicenseId'                       => 'license_guid',
        'BranchId'                        => 'eo.guid',
        'Number'                          => 'number',
        'Addresses'                       => 'addresses',
        'lod_stateCode'                   => 'state_code',
        //programs
        'id'                              => 'guid',
        'LicenseSupplementID'             => 'supplement_guid',
        'ProgrammCode'                    => 'code',
        'ProgrammName'                    => 'name',
        'LevelName'                       => 'level',
        'TypeName'                        => 'type',
        'QualificationName'               => 'qualification',
        //decision
        'id'                              => 'guid',
        'documentId'                      => 'document_guid',
        'licenseId'                       => 'license_guid',
        'licenseAttachId'                 => 'license_attach_guid',
        'registrationDate'                => 'date',
        'documentType'                    => 'type_name',
        'documentTypeId'                  => 'type_guid',
        'registrationNumber'              => 'number',
        //фалы лицении  и приложения
        'fileDocument'                      => 'file_document',
        'fileSign'                          => 'file_sign',
        'fileXml'                           => 'file_xml',
        
    ];
    /**
     * Параметры импорта для истории дел
     * @var array
     */
    public static $dealMap = [
        'id'               => 'guid',
        'organizationId'   => 'eo_guid',
        'eoFullName'       => 'eo_name',
        'inn'              => 'inn',
        'number'           => 'number',
        'registrationDate' => 'registrationDate',
        'reasons'          => 'reasons',
        'state'            => 'state',
    ];
    /**
     * Guid решений о выдаче
     * @var array
     */
    public static $inoutGuids = [
        'e0e8eb9952464ea798f7e41b6a81c93f',
    ];
    /**
     * Guid решений о приостановлении
     * @var array
     */
    public static $suspensionGuids = [
        '613877a0a16b4822af2b3319b7559771',
        'b69be1738ca6495cb76cef90ab3ae102',
    ];
    /**
     * Guid решений о возобновлении
     * @var array
     */
    public static $renewalGuids = [
        '992cdd6800d3486ab919f323d7e312f2',
    ];
    /**
     * Guid решений о прекращении
     * @var array
     */
    public static $terminationGuids = [
        '1dc3e282134a4b7e9578216e6f07f096',
    ];
    /**
     * Guid решений о аннулировании
     * @var array
     */
    public static $revocationGuids = [
        '046fe9790a3840f2bb4c600064ad725d',
    ];
    /**
     * Статусы приложений code => название
     * @var array
     */
    public static $supplementStatesMap = [
        'SUSPENDED' => 'Приостановлено',
        'ANNULLED'  => 'Аннулировано',
        'DRAFT'     => 'Проект',
        'NOT_VALID' => 'Не действует',
        'VALID'     => 'Действует',
    ];

}