<?php

namespace LicensesReestr\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\ServiceManager\ServiceLocatorInterface;
use Reestr\Model\SearchEngine;
use LicensesReestr\Options\ModuleOptions;

//TODO: добавить выборку только актуальных данных (is_actual = 1) во всех методах что формируют запросы

/**
 * Description of BaseController
 *
 * @author Дмитрий Бубякин
 */
class BaseController extends AbstractActionController
{

    /**
     *
     * @var SearchEngine 
     */
    protected $searchEngine = null;
    /**
     *  id & pass для простого
     * @var array 
     */
    protected $prostoyConfig = null;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->searchEngine = $serviceLocator->get('LicensesReestr\SearchEngine');
        $this->prostoyConfig = $serviceLocator->get('config')['lic_reestr_prostoy'];

        $defaultOptions = ['pattern' => '%s', 'properties' => ['name',], 'id' => '_id'];
        $this->searchEngine->addAll([
            ModuleOptions::DECISION,
            ModuleOptions::EO,
            ModuleOptions::LICENSE,
            ModuleOptions::LICENSE_ORGAN => $defaultOptions,
            ModuleOptions::LICENSE_STATE => $defaultOptions,
            ModuleOptions::PROGRAM,
            ModuleOptions::REGION        => $defaultOptions,
            ModuleOptions::SUPPLEMENT,
            ModuleOptions::PACKAGE,
            ModuleOptions::DEAL_HISTORY]);
    }

    /**
     * Устанавливаем щаголовок страницы
     * @param string $title Заголовок страницы
     */
    protected function headTitle($title)
    {
        $this->layout()->setVariable('title', $title);
    }

    /**
     * Отправить сообщение в клиент простого
     * @param array $params
     */
    protected function sendMailProstoy($params = [])
    {
        extract($params);
        if (empty($subject)) {
            $subject = '';
        }
        mb_internal_encoding('WINDOWS-1251');
        file('http://agent.prostoy.ru/addComment.php?tid=' . $id . '&tpass=' . $pass .
          '&tname=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $name)) .
          '&temail=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $email)) .
          '&tsubj=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $subject)) .
          '&tcom=' . urlencode(iconv('UTF-8', 'WINDOWS-1251', $body)));
    }

    /**
     * Собираем sql запрос
     * @param  \Zend\Mvc\Controller\Plugin\Params $params
     * @param array $order ['column','type']
     * @return array
     */
    protected function getSearchParams($params, array $order)
    {

        $decisionWhereIn = sprintf("('%s')", implode("','", ModuleOptions::$inoutGuids));

        $whereMap = [
            'e.region_id'              => 'regionId',
            'license.license_organ_id' => 'licenseOrganId',
            'license.license_state_id' => 'licenseStateId',
            'license.number'           => 'number',
            'e.inn'                    => 'eoInn',
            'e.ogrn'                   => 'eoOgrn',
        ];

        $whereEqual = [
            'license.is_actual' => 1,
        ];
        foreach ($whereMap as $key => $entry) {
            $value = $params->fromPost($entry);
            if (!empty($value)) {
                $whereEqual[$key] = $value;
            }
        }


        $whereLike = [];
        $whereGreaterEqual = [];
        $whereLessEqual = [];
        $eoName = $params->fromPost('eoName');
        $issueFrom = $params->fromPost('issueFrom');
        $issueTo = $params->fromPost('issueTo');

        if (!empty($eoName)) {
            $whereLike['e.name'] = "%{$eoName}%";
        }

        if (!empty($issueFrom)) {
            $whereGreaterEqual['issue_date'] = $issueFrom;
        }

        if (!empty($issueTo)) {
            $whereLessEqual['issue_date'] = $issueTo;            
        }
        
        //влиниваем гуид организации
        $eoGuid = $params->fromPost('eoId');
        if( empty($eoGuid) ){
            $eoGuid = $params->fromPost('eoGUID');
        }
        if( !empty($eoGuid) ){
            $whereLike['eo_guid'] = '%'. $eoGuid . '%';
        }
        

        $selectParams = [
            'table'   => 'license',
            'columns' => [
                'guid'           => 'guid',
                'number'         => 'number',
                'docSerie'       => 'doc_serie',
                'docNumber'      => 'doc_number',
                'endDate'        => 'end_date',
                'issueDate'      => 'issue_date',
                'licenseOrganId' => 'license_organ_id',
                'licenseStateId' => 'license_state_id',
            ],
            'join'    => [
                'eo'            => [
                    'alias'   => 'e',
                    'on'      => 'e._id = license.eo_id',
                    'columns' => [
                        'eoName'   => 'name',
                        'eoInn'    => 'inn',
                        'eoOgrn'   => 'ogrn',
                        'regionId' => 'region_id',
                    ],
                ],
                'license_state' => [
                    'alias'   => 'ls',
                    'on'      => 'ls._id = license.license_state_id',
                    'columns' => [
                        'state' => 'name',
                    ],
                ],
                'license_organ' => [
                    'alias' => 'lo',
                    'on'    => 'lo._id = license.license_organ_id',
                ],
                'decision'      => [
                    'alias'   => 'd',
                    'on'      => new \Zend\Db\Sql\Expression(
                      'd.license_id = license._id and d.type_guid in ' . $decisionWhereIn
                    ),
                    'columns' => [
                        'decDate'   => 'date',
                        'decNumber' => 'number',
                        'decType'   => 'type_name'
                    ],
                    'type'    => 'left',
                ],
            ],
            'where'   => [
                'equal'        => $whereEqual,
                'like'         => $whereLike,
                'greaterEqual' => $whereGreaterEqual,
                'lessEqual'    => $whereLessEqual,                
            ],
            'group'   => [
                'guid',
            ],
            'order'   => [
                sprintf('%s %s', $order[0], $order[1]),
            ],
        ];

        return $selectParams;
    }

    /**
     * Собираем запрос для получения информации о лицензии
     * @param string $guid
     */
    protected function getLicenseInfo($guid)
    {
        $params = [
            'table'   => 'license',
            'columns' => [
                'number'                => 'number',
                'docSerie'              => 'doc_serie',
                'docNumber'             => 'doc_number',
                'endDate'               => 'end_date',
                'reorganization'        => 'reorganization',
                'reorganizationType'    => 'reorganization_type',
                'fileDocument'          =>  'file_document',
                'fileSign'              =>  'file_sign',
                'fileXml'               =>  'file_xml',
            ],
            'join'    => [
                'eo'            => [
                    'alias'   => 'e',
                    'on'      => 'e._id = license.eo_id',
                    'columns' => [
                        'eoName'      => 'name',
                        'eoShortName' => 'short_name',
                        'eoInn'       => 'inn',
                        'eoOgrn'      => 'ogrn',
                        'eoKpp'       => 'kpp',
                        'eoAddress'   => 'address',
                    ],
                ],
                'region'        => [
                    'alias'   => 'r',
                    'on'      => 'r._id = e.region_id',
                    'columns' => [
                        'region' => 'name',
                    ],
                    'type' => 'left',
                ],
                'license_state' => [
                    'alias'   => 'ls',
                    'on'      => 'ls._id = license.license_state_id',
                    'columns' => [
                        'state' => 'name',
                    ],
                ],
                'license_organ' => [
                    'alias'   => 'lo',
                    'on'      => 'lo._id = license.license_organ_id',
                    'columns' => [
                        'organ' => 'name',
                    ],
                ],
            /* 'decision'      => [
              'alias'   => 'd',
              'on'      => new \Zend\Db\Sql\Expression(
              'd.license_id = license._id and d.type_guid in ' . $decisionWhereIn),
              'columns' => [
              'decDate'   => 'date',
              'decNumber' => 'number',
              ],
              'type'    => 'left',
              ], */
            ],
            'where'   => [
                'equal' => [
                    'license.guid' => $guid,
                ]
            ],
        ];
        return $params;
    }

    protected function getLicenseDecisions($guid)
    {
        $params = [
            'table'   => 'decision',
            'columns' => [
                'decDate'   => 'date',
                'decNumber' => 'number',
                'type'      => 'type_name',
                'typeGuid'  => 'type_guid',
            ],
            'where'   => [
                'equal' => [
                    'license_guid' => $guid,
                    'is_actual'    => 1,
                ],
            ]
        ];
        return $params;
    }

    protected function getLicenseSupplements($guid)
    {
        $decisionWhereIn = sprintf("('%s')", implode("','", ModuleOptions::$inoutGuids));
        $params = [
            'table'   => 'supplement',
            'columns' => [
                'guid'      => 'guid',
                'number'    => 'number',
                'stateCode' => 'state_code',
                'stateName' => 'state_name',
            ],
            'join'    => [
                'eo'       => [
                    'alias'   => 'e',
                    'on'      => 'e._id = supplement.eo_id',
                    'columns' => [
                        'eoName' => 'name',
                    ],
                ],
                'decision' => [
                    'alias'   => 'd',
                    'on'      => new \Zend\Db\Sql\Expression(
                      'd.license_attach_guid = supplement.guid and d.type_guid in ' . $decisionWhereIn
                    ),
                    'columns' => [
                        'decDate'   => 'date',
                        'decNumber' => 'number',
                        'type'      => 'type_name',
                    ],
                    'type'    => 'left',
                ],
            ],
            'where'   => [
                'equal' => [
                    'supplement.license_guid' => $guid,
                    'supplement.is_actual'    => 1,
                ],
            ],
            'order'   => [
                'supplement.number asc',
            ],
        ];

        return $params;
    }

    protected function getSupplementInfo($guid)
    {
        return [
            'table'   => 'supplement',
            'columns' => [
                'addresses'             => 'addresses',
                'reorganization'        => 'reorganization',
                'reorganizationType'    => 'reorganization_type',
                'fileDocument'          =>  'file_document',
                'fileSign'              =>  'file_sign',
                'fileXml'               =>  'file_xml',
            ],
            'join'    => [
                'eo' => [
                    'alias'   => 'e',
                    'on'      => 'e._id = supplement.eo_id',
                    'columns' => [
                        'eoName'      => 'name',
                        'eoShortName' => 'short_name',
                        'eoAddress'   => 'address',
                    ],
                ],
            ],
            'where'   => [
                'equal' => [
                    'supplement.guid'      => $guid,
                    'supplement.is_actual' => 1,
                ],
            ],
        ];
    }

    protected function getSupplementPrograms($guid)
    {
        return [
            'table'   => 'program',
            'columns' => [
                'code',
                'name',
                'level',
                'type',
                'qualification',
            ],
            'where'   => [
                'equal' => [
                    'is_actual'       => 1,
                    'supplement_guid' => $guid,
                ],
            ],
            'order'   => [
                'code asc',
            ],
        ];
    }

    protected function getSupplementDecisions($guid)
    {
        return [
            'table'   => 'decision',
            'columns' => [
                'decDate'   => 'date',
                'decNumber' => 'number',
                'type'      => 'type_name',
                'typeGuid'  => 'type_guid',
            ],
            'where'   => [
                'equal' => [
                    'license_attach_guid' => $guid,
                    'is_actual'           => 1,
                ],
            ]
        ];
    }

    protected function prepareDeal(\Reestr\Entity\AbstractEntity $deal)
    {
        return [
            'name'    => $deal->eo_name,
            'number'  => $deal->number,
            'date'    => date('d.m.Y', strtotime($deal->registrationDate)),
            'reasons' => str_replace(';', ';<br>', $deal->reasons),
            'state'   => $deal->state,
        ];
    }

    

}