<?php

namespace LicensesReestr\Controller;

use Zend\View\Model\ViewModel;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;
use Reestr\Form\ReportForm;
use Reestr\Form\ReportFormInputFilter;
use LicensesReestr\Controller\BaseController;
use LicensesReestr\Options\ModuleOptions;
use LicensesReestr\Form\SearchForm;
use LicensesReestr\Exception\LicensesReestrException;
use LicensesReestr\Service\ImportService;
use LicensesReestr\Form\CheckForm;
use LicensesReestr\Service\ExportOpenDataService as OpenDataService;
use Zend\Filter\Decompress;

/**
 * Description of IndexController
 *
 * @author Дмитрий Бубякин
 */
class IndexController extends BaseController
{

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        parent::__construct($serviceLocator);
    }
    
    function array_kshift(&$arr)
    {
        reset($arr);
        $firstKey = key($arr);
        $firstVal = current($arr);
        unset($arr[$firstKey]);
        return [ $firstKey => $firstVal];
    }
    
    /**
     * 
     * @param type $array
     */
    protected function joinSelectArray($arr){
        $ret = [];
        
        while( count($arr) > 0){
            $tempArr = $this->array_kshift($arr);
            $key = key($tempArr);
            $curentName = current($tempArr);
            $onArray = false;
            
             foreach ($arr as $keyD => $valueD) {
                
                if( $curentName == $valueD && $key != $keyD){
                    $joinIds = "$key-$keyD";
                    $ret[$joinIds] = $valueD;
                    $onArray = true;
                    break;
                }
            }
            
            if($onArray){
                unset($arr[$keyD]);
                unset($arr[$key]);
            }else{
                $ret[$key] = $curentName;
                unset($arr[$key]);
            }
        }
        
        return $ret;
    }

    /**
     * реестр - рисует форму поиска лиценций
     *
     * url:  /rlic/
     * view: /module/LicensesReestr/view/licenses-reestr/index/index.phtml
     *
     * в гете можно загнать
     *      eoInn       - поиск по инн
     *      eoGUID      - поиск по ид организации
     *      eoId        -   --//--
     *
     * @return ViewModel
     */
    public function indexAction()
    {
        $regions = $this->searchEngine
          ->getOptions(['table' => ModuleOptions::REGION, 'order' => ['name asc']]);
        $licenseOrgans = $this->searchEngine
          ->getOptions(['table' => ModuleOptions::LICENSE_ORGAN, 'order' => ['name asc']]);
        $licenseStates = $this->searchEngine
          ->getOptions(['table' => ModuleOptions::LICENSE_STATE, 'order' => ['name asc']], true);
        $supplementStates = $this->searchEngine
          ->getOptions(['table' => ModuleOptions::LICENSE_STATE, 'order' => ['name asc']], true, 'code');

        $dataOption = [
            'regionId'       => $regions,
            'licenseOrganId' => $licenseOrgans,
            'licenseStateId' => $licenseStates,
            'supplementStateId' => $this->joinSelectArray($supplementStates),
        ];


        $reportForm = new ReportForm();
        $searchForm = new SearchForm();
        $searchForm->setSelectOptions($dataOption);

        $autoSearch = false;
        $eoInn = $this->getRequest()->getQuery( 'eoInn' , '' );
        if( !empty($eoInn) ){
            $searchForm->setData( array( 'eoInn'=> strip_tags( trim($eoInn) ) ) );
            $autoSearch = true;
        }

        $eoId = $this->getRequest()->getQuery( 'eoId' , '' );
        if( empty( $eoId ) ){
            $eoId = $this->getRequest()->getQuery( 'eoGUID' , '' );
        }
        if( !empty($eoId) ){
            $searchForm->setData( array( 'eoId'=> strip_tags( trim($eoId) ) ) );
            $autoSearch = true;
        }

        return new ViewModel([
            'searchForm' => $searchForm,
            'reportForm' => $reportForm,
            'autoSearch' => $autoSearch,
        ]);
    }

    public function reportAction()
    {
        $response = $this->getResponse();
        $response->setStatusCode(200);
        $request = $this->getRequest();
        $form = new ReportForm();
        $inputFilter = (new ReportFormInputFilter())->getInputFilter();
        $form->setInputFilter($inputFilter);
        //session для капчи
        $captcha = new Container('captcha');
        $post = $request->getPost();
        $ownMessages = [];
        try {
            if (!$post) {
                throw new LicensesReestrException('Form is not submitted');
            }
            $form->setData($post);
            //проверим наличае кода капчи в сессии
            if ($post['captcha'] !== $captcha->value || empty($captcha->value)) {
                $ownMessages['captcha']['isEmpty'] = 'Переместите ползунок';
            }
            if (!$form->isValid() || count($ownMessages)) {
                throw new LicensesReestrException('Invalid form data');
            }

            $args = [
                $post['organization'], empty($post['name']) ? '' : "ФИО: {$post['name']}<br/>",
                $post['email'],
                $post['phone'],
                empty($post['description']) ? '' : "Сообщение:<br/>{$post['description']}"
            ];

            $mailOptions = [
                'id'      => $this->prostoyConfig['id'],
                'pass'    => $this->prostoyConfig['password'],
                'name'    => 'Undefined',
                'email'   => $post['email'],
                'subject' => $post['organization'],
                'body'    => vsprintf('Организация: %s<br/>%sE-mail: %s<br/>Телефон для связи: %s<br/><br/>%s', $args),
            ];
            $this->sendMailProstoy($mailOptions);

            $response->setContent(json_encode([
                'status' => 'OK',
            ]));
        } catch (LicensesReestrException $ex) {
            $response->setContent(json_encode([
                'status'  => 'FAIL',
                'errors'  => array_merge($form->getMessages(), $ownMessages),
                'message' => $ex->getMessage(),
            ]));
        }
        $captcha->getManager()->getStorage()->clear('captcha');
        return $response;
    }

    public function captchaAction()
    {

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $captcha = new Container('captcha');
        try {
            $post = $this->getRequest()->getPost();
            if (!(isset($post['action']) && isset($post['value']))) {
                throw new LicensesReestrException('Value or action is undefined');
            }
            $captcha->value = $post['value'];
            $response->setContent(json_encode([
                'status' => 'OK',
            ]));
        } catch (LicensesReestrException $ex) {
            $response->setContent(json_encode([
                'status'  => 'FAIL',
                'message' => $ex->getMessage(),
            ]));
        }
        return $response;
    }

    function searchAction()
    {
        $itemsPerPage = 10;
        $page = (int) $this->params()->fromQuery('page', 1);
        $orderColumn = $this->params()->fromQuery('order', 'decDate');
        $orderType = $this->params()->fromQuery('direction', 'desc');
        $post = $this->getRequest()->getPost();
        $entity = new \Reestr\Entity\AbstractEntity();
        $entity->exchangeArray($post->toArray());
        $extended = isset($entity->extended) && (!empty($entity->supplementStateId));
        
        if(!empty($entity->supplementStateId)){
            $inArr = explode('-', $entity->supplementStateId);
        }
        

        $params = $this->getSearchParams($this->params(), [$orderColumn, $orderType]);
        
        
        if( isset($entity->extended) && (!empty($entity->supplementStateId))){
            
            $params['join']['supplement'] = [
                   'type' => 'inner',
                   'on' => 'supplement.license_guid = license.guid',
                   'columns' => [
                        'license_guid'
                   ]
           ];
           $params['where']['in'] = ['supplement.state_code' => $inArr,];
        }
        
        $paginator = $this->searchEngine->getSearchPaginator($params);
        
        $paginator->setItemCountPerPage($itemsPerPage);
        $paginator->setCurrentPageNumber($page);
        $model = new ViewModel();
        $model->setTerminal(true);
        $model->setVariables([
            'paginator' => $paginator,
            'count'     => $paginator->getTotalItemCount(),
            'column'    => $orderColumn,
            'type'      => $orderType
        ]);
        return $model;
    }

    function detailsAction()
    {
        $guid = $this->params()->fromRoute('guid');
        if (!$guid) {
            throw new LicensesReestrException('License guid is not defined');
        }

        $license = current($this->searchEngine->select($this->getLicenseInfo($guid)));
        if (!$license) {
            throw new LicensesReestrException('License guid is not defined');
        }
        $license->decisions = $this->searchEngine->select($this->getLicenseDecisions($guid));
        $license->supplements = $this->searchEngine->select($this->getLicenseSupplements($guid));

        $this->headTitle('Просмотр лицензии');

        return new ViewModel([
            'license' => $license,
            'guid' => $guid
        ]);
    }

    function supplementAction()
    {
        $guid = $this->params()->fromRoute('guid');
        if (!$guid) {
            throw new LicensesReestrException('Supplement guid is not defined');
        }

        $supplement = current($this->searchEngine->select($this->getSupplementInfo($guid)));
        $supplement->programs = $this->searchEngine->select($this->getSupplementPrograms($guid));
        $supplement->decisions = $this->searchEngine->select($this->getSupplementDecisions($guid));
        $view = new ViewModel([
            'supplement' => $supplement,
        ]);
        $view->setTerminal(true);
        return $view;
    }

    function mailLog($logMsg, $fileName = 'public/log/fileUploadService.log')
    {
        $mailer = new \Exchange\Controller\IndexController();
        $mailer->mailLog($logMsg, $fileName);
    }

    function importAction()
    {
        $this->mailLog('Импорт лицензий');
        //$this->mailLog(  print_r( $_GET ,true ) );
        //$this->mailLog(  print_r( $_FILES ,true ) );        
        
        $xmlPath = $this->params()->fromRoute('file', '/var/www/html/f11/public/uploads/test.xml');
        $this->mailLog(  'импортировать ' . $xmlPath );       
        
        $importService = new ImportService($this->searchEngine);
        $errors = $importService->import($xmlPath);
        $this->mailLog(json_encode($errors));
        $response = $this->getResponse();
        $response->setStatusCode(200);
        return $response;
    }

    function importHistoryAction()
    {
        $xmlPath = $this->params()->fromRoute('file', '/var/www/html/f11/public/uploads/test.xml');
        $importService = new \LicensesReestr\Service\HistoryImportService($this->searchEngine);
        $importService->import($xmlPath);
        $response = $this->getResponse();
        $response->setStatusCode(200);
        return $response;
    }

    function checkStateAction()
    {
        $form = new CheckForm();
        $this->headTitle('Проверка состояния дела');
        return new ViewModel([
            'form' => $form,
        ]);
    }

    function searchDealAction()
    {
        $result = [
            'state'   => 'ok',
            'found'   => false,
            'message' => ':(',
        ];

        try {

            $year = (int) $this->params()->fromPost('year');
            $inn = $this->params()->fromPost('inn');
            $number = $this->params()->fromPost('number');

            $where = array_filter([
                'inn'    => $inn,
                'number' => $number,
              ], function($value) {
                return !empty($value);
            });

            if (!empty($year)) {
                $where[] = new \Zend\Db\Sql\Predicate\Expression('year(registrationDate) = ?', $year);
            }

            if (empty($where)) {
                throw new LicensesReestrException('Заполните по крайней мере одно поле');
            }

            if (empty($inn) && (empty($year) || empty($number))) {
                throw new LicensesReestrException(
                empty($year) ? 'Поле "Год регистрации" обязательно для заполнения' : 'Поле "Номер дела" обязательно для заполнения'
                );
            } else if (!empty($inn) && (!empty($year) || !empty($number))) {
                throw new LicensesReestrException('Укажите только поле "ИНН" или поля "Номер дела" и "Год регистрации"');
            }

            $deals = $this->searchEngine->select([
                'table' => ModuleOptions::DEAL_HISTORY,
                'where' => [
                    'equal' => $where,
                ],
            ]);

            foreach ($deals as $deal) {
                if ($deal instanceof \Reestr\Entity\AbstractEntity) {
                    $result['found'] = true;
                    $result['deals'][] = $this->prepareDeal($deal);
                }
            }
        } catch (LicensesReestrException $e) {
            $result['message'] = $e->getMessage();
            $result['state'] = 'fail';
        }

        return new \Zend\View\Model\JsonModel($result);
    }

    public function exportOpenDataAction()
    {
        $limit = $this->params()->fromQuery('l', 100);
        $offset = $this->params()->fromQuery('o', 0);
        $total = $this->params()->fromQuery('t', 0);
        $key = $this->params()->fromQuery('key');
        $accessKey = 'asdnlfiuaywev87rvyaiojfjzdnyfs';
        if ($key !== $accessKey) {
            echo 'Invalid key';
            die;
        }
        header('Content-Type: text/plain');
        $service = new OpenDataService($this->searchEngine, $limit, $offset, $total );
        $service->export();
        die;
    }

    public function opendataAction()
    {
        $response = new \Zend\Http\Response\Stream();
        try {
            $path = OpenDataService::XML_FILE_PATH;
            $files = array_values(array_diff(scandir($path), ['.', '..', '.svn']));
            if (empty($files)) {
                throw new LicensesReestrException('Nothing found');
            }
            sort($files);
            $fileName = end( $files );
            $filePath = sprintf('%s%s', $path, $fileName);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $ctype = finfo_file($finfo, $filePath);
            $response->setStream(fopen($filePath, 'r'));
            $response->setStatusCode(200);
            $headers = new \Zend\Http\Headers();
            $headers->addHeaderLine('Content-Type', $ctype)
              ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $fileName . '"')
              ->addHeaderLine('Content-Length', filesize($filePath));
            $response->setHeaders($headers);
            return $response;
        } catch (LicensesReestrException $ex) {
            $tmp = $this->getResponse();
            $tmp->setStatusCode(200);
            $tmp->setContent($ex->getMessage());
            return $tmp;
        }
    }

    public function opendataLinksAction()
    {
        $files = glob('/home/eiis/TRS*.zip');
        print_r($files);
        die;
    }

    public function watchEiisDirAction()
    {
        echo __DIR__."\n";
        $files = glob('public/trs/*');
        print_r($files);
        die;
    }

    public function unzipTrsAction(){
        $targetDir = 'public/trs/';
        $zipFile = $this->params()->fromQuery('file');
        $file = '/home/eiis/' . $zipFile . '.zip';
        if(!file_exists($file)){
            throw new LicensesReestrException(sprintf('File %s does not exists',$file));
        }
        if (!file_exists($targetDir)) {
            mkdir($targetDir);
        }else{
            array_map('unlink',glob($targetDir.'*'));
        }
        $filter = new Decompress([
            'adapter' => 'Zip',
            'options' => [
                'target' => $targetDir . $zipFile,
                'archive' => $file,
            ],
        ]);
        if(!$filter->filter($file)){
            throw new LicensesReestrException(sprintf('Can not decompress file %s',$file));
        }
        $files = glob('public/trs/*');
        print_r($files);
        die;
    }

    public function eoTestAction()
    {
        echo "started\n";
        $logger = new \Model\Logger\Logger();
        $logger->Log("");
        $logger->Log("==============================");
        $logger->Log("Старт скрипта получения Лицензий");
        ini_set("memory_limit", "10000M");
        set_time_limit(0);

        $serviceEntities = new \Model\EiisService\ServiceEntities([
            'wsdl' => 'http://eiis-production.srvdev.ru/integrationservice/baseservice.asmx',
        ]);
        $serviceEntities->getEntitiesFromService("EIIS.F_LICENSES", 260, true);
        die;
    }

    /**
     * возращаем перечень файло в каталоге архивов реестра
     */
    public function perechenArhivReestrAction(){

        $path = OpenDataService::XML_FILE_PATH;
        $path = str_replace( 'public/' , '' , $path );
        $files1 = glob($path.'*.zip' /*, GLOB_ONLYDIR|GLOB_NOSORT*/);
        $files2 = glob('public/'.$path.'*.zip' , GLOB_NOSORT);
        $files = array_merge( $files1 , $files2 );
        $files = array_flip( $files );
        //unset( $files['public/opendata-lic/data-'.date('Ymd').'-structure-20150421.zip'] );
        unset( $files['public/opendata-lic/data-'.date('Ymd').'-structure-20160713.zip'] );
        $files = array_flip( $files );
        if( empty($files) ){
            die( 'нет файлов' );
        }
        rsort( $files );
        echo('<lu><style>a{font-size: 13px;}</style>');
        foreach( $files as $key => $value ){
            $value = '/'.$value;
            $fileName = str_replace('/public/', '' , $value);
            echo('<li style="list-style-type:none;" ><a href="/'.$fileName.'" target="_blank">'.basename($value).'<a></li>');
        }
        echo('</lu>');
        die;
    }

}