<?php

namespace LicensesReestr\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use LicensesReestr\Controller\IndexController;

/**
 * Description of IndexControllerFactory
 *
 * @author Дмитрий Бубякин
 */
class IndexControllerFactory implements FactoryInterface {

    public function __invoke(ServiceLocatorInterface $container) {
        return new IndexController($container->getServiceLocator());
    }

    public function createService(ServiceLocatorInterface $container) {
        return $this($container);
    }

}
