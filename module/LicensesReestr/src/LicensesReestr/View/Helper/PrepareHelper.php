<?php

namespace LicensesReestr\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Reestr\Entity\AbstractEntity;
use LicensesReestr\Options\ModuleOptions;

/**
 * Description of PrepareHelper
 *
 * @author dima
 */
class PrepareHelper extends AbstractHelper
{

    const LICENSE_SEARCH = 'license-search';
    const LICENSE_DETAILS = 'license-details';
    const SUPPLEMENT_LIST = 'supplement-list';
    const SUPPLEMENT_DECISIONS = 'supplement-decisions';

    /**
     * Подготавливаем поля объекта
     * @param AbstractEntity $entity
     * @param string $prepareFor
     */
    public function __invoke(AbstractEntity $entity, $prepareFor)
    {
        if ($prepareFor === self::LICENSE_SEARCH) {
            $this->licenseSearch($entity);
        } else if ($prepareFor === self::LICENSE_DETAILS) {
            $this->licenseDetails($entity);
        } else if ($prepareFor === self::SUPPLEMENT_LIST) {
            $this->supplementList($entity);
        } else if ($prepareFor === self::SUPPLEMENT_DECISIONS) {
            $this->supplementDecisions($entity);
        }
    }

    /**
     * Подготавливаем параметры лицензии для вывода в поиске
     * @param AbstractEntity $license
     */
    private function licenseSearch(AbstractEntity $license)
    {
        $serieAndNumber = sprintf('%s %s', $license->docSerie, $license->docNumber);
        $endDateFormatted = date('d.m.Y', strtotime($license->endDate));
        $endDate = empty($license->endDate) || $license->endDate === '0000-00-00' ? 'Бессрочная' : $endDateFormatted;

        $license->decision = $this->formatDecision($license->decDate, $license->decNumber, $license->decType);
        $license->serieAndNumber = $serieAndNumber;
        $license->endDate = $endDate;
    }

    /**
     * Подготавливаем параметры для вывода на странице детальной информации
     * @param AbstractEntity $license
     */
    private function licenseDetails(AbstractEntity $license)
    {
        $endDateFormatted = date('d.m.Y', strtotime($license->endDate));
        $endDate = empty($license->endDate) || $license->endDate === '0000-00-00' ? 'Бессрочная' : $endDateFormatted;
        $serieAndNumber = sprintf('%s %s', $license->docSerie, $license->docNumber);

        $license->serieAndNumber = $serieAndNumber;
        $license->endDate = $endDate;

        $this->decisions($license);
    }

    /**
     * Добавляем значения решений
     * @param AbstractEntity $entity
     */
    private function decisions(AbstractEntity $entity)
    {
        //Допустимые значения guid'ов находятся в LicensesReestr\Options\ModuleOptions
        $entity->inoutDecision = null;
        $entity->suspensionDecision = null;
        $entity->renewalDecision = null;
        $entity->terminationDecision = null;
        $entity->revocationDecision = null;

        if (empty($entity->decisions)) {
            return;
        }

        /* @var $decision AbstractEntity */
        foreach ($entity->decisions as $decision) {

            if (empty($decision)) {
                continue;
            }

            $formattedDecision = $this->formatDecision($decision->decDate, $decision->decNumber, $decision->type);
            $type = $decision->typeGuid;
            $delimiter = ", ";
            //Решение о вводе
            if (in_array($type, ModuleOptions::$inoutGuids)) {
                if($entity->inoutDecision == null) $delimiter = "";
                $entity->inoutDecision .= $delimiter.$formattedDecision;
                //Решение о приостановлении
            } else if (in_array($type, ModuleOptions::$suspensionGuids)) {
                if($entity->suspensionDecision == null) $delimiter = "";
                $entity->suspensionDecision .= $delimiter.$formattedDecision;
                //Решение о возобновлении
            } else if (in_array($type, ModuleOptions::$renewalGuids)) {
                if($entity->renewalDecision == null) $delimiter = "";
                $entity->renewalDecision .= $delimiter.$formattedDecision;
                //Решение о прекращении
            } else if (in_array($type, ModuleOptions::$terminationGuids)) {
                if($entity->terminationDecision == null) $delimiter = "";
                $entity->terminationDecision .= $delimiter.$formattedDecision;
                //решение о аннулировании
            } else if (in_array($type, ModuleOptions::$revocationGuids)) {
                if($entity->revocationDecision == null) $delimiter = "";
                $entity->revocationDecision .= $delimiter.$formattedDecision;
            }
        }

        if(!empty($entity->suspensionDecision)){
            $this->prepareSuspensionDecision($entity);
        }
    }

    /**
     * Подготавливаем вывод списка приложений
     * @param AbstractEntity $supplement
     */
    private function supplementList(AbstractEntity $supplement)
    {
        $supplement->decision = $this->formatDecision($supplement->decDate, $supplement->decNumber, $supplement->type);
        $supplement->state = null;
        if (array_key_exists($supplement->stateCode, ModuleOptions::$supplementStatesMap)) {
            $supplement->state = ModuleOptions::$supplementStatesMap[$supplement->stateCode];
        }else{
            $supplement->state = $supplement->stateName;
        }
    }

    /**
     * Полготавливаем решения для приложений
     * @param AbstractEntity $supplement
     */
    private function supplementDecisions(AbstractEntity $supplement)
    {
        $this->decisions($supplement);
        $supplement->addresses = str_replace(';', '<br>', $supplement->addresses);
    }

    /**
     * Форматируем данные о решении
     * @param string $decDate
     * @param string $decNumber
     * @param string $decType
     * @return string
     */
    private function formatDecision($decDate, $decNumber, $decType = 'Распоряжение')
    {
        $date = sprintf('от %s', date('d.m.Y', strtotime($decDate)));
        $number = sprintf('№ %s', $decNumber);

        //Связано с возможными значениями с реестров еиис
        if(empty($decType) || $decType == 'NULL') $decType = 'Распоряжение';
        //если $decType не = 'Распоряжение' или 'Приказ', а, например 'Приказ о предоставлении лицензии', то срезаем до первого слова
        //if( $decType!='Приказ' && $decType!='Распоряжение') $decType = explode(' ',trim($decType))[0];
        return vsprintf('%s %s %s',
                        [
            $decType,
            empty($decDate) || $decDate === '0000-00-00' ? '' : $date,
            empty($decNumber) ? '' : $number,
        ]);
    }

    /**
    * Разбиваем решение о приостановлении на части(если выборка из ТРС)
    * @param AbstractEntity $entity
    * @return void
    */
    private function prepareSuspensionDecision(AbstractEntity $entity){

        foreach(explode(';', $entity->suspensionDecision) as $part){
            if(mb_strpos($part,'Возоб') !== false){
                $entity->renewalDecision = trim($part);
            }else{
                $entity->suspensionDecision = trim($part);
            }
        }

    }
}