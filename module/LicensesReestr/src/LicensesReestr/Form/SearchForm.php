<?php

namespace LicensesReestr\Form;

use Zend\Form\Form;

/**
 * Description of SearchForm
 *
 * @author Дмитрий Бубякин
 */
class SearchForm extends Form {

    /**
     * Select elements options
     * @var array
     */
    protected $selectOptions;

    public function __construct($name = 'SearchForm', $options = []) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'role' => 'form',
            'class' => 'form-horizontal',
        ]);

        $this->add([
            'name' => 'regionId',
            'type' => 'select',
            'options' => [
                'label' => 'Субъект РФ',
            ],
        ]);
        
        $this->add([
            'name' => 'licenseOrganId',
            'type' => 'select',
            'options' => [
                'label' => 'Лицензирующий орган',
            ],
        ]);
        
        $this->add([
            'name' => 'eoId',
            'type' => 'hidden',
            'options' => [
                'label' => 'GUID организации',
            ],
        ]);

        $this->add([
            'name' => 'eoName',
            'type' => 'text',
            'options' => [
                'label' => 'Наименование организации',
            ],
        ]);
        $this->add([
            'name' => 'eoInn',
            'type' => 'text',
            'options' => [
                'label' => 'ИНН',
            ],
        ]);
        $this->add([
            'name' => 'eoOgrn',
            'type' => 'text',
            'options' => [
                'label' => 'ОГРН',
            ],
        ]);
        $this->add([
            'name' => 'number',
            'type' => 'text',
            'options' => [
                'label' => 'Регистрационный номер лицензии',
            ],
        ]);
        $this->add([
            'name' => 'licenseStateId',
            'type' => 'select',
            'options' => [
                'label' => 'Состояние лицензии',
            ],
        ]);
        
        $this->add([
            'name' => 'issueFrom',
            'type' => 'date',
            'options' => [
                'label' => 'с',
            ],
        ]);
        $this->add([
            'name' => 'issueTo',
            'type' => 'date',
            'options' => [
                'label' => 'по',
            ],
        ]);
        
        foreach ($this->getElements() as $element) {
            $this->get($element->getName())
                    ->setOptions([
                        'label_attributes' => [
                            'class' => 'control-label col-sm-4',
                        ],
                    ])
                    ->setAttributes([
                        'class' => 'form-control',
            ]);
        }
        
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Поиск',
                'class' => 'btn btn-primary ',
            ],
        ]);
        $this->add([
            'name' => 'reset',
            'attributes' => [
                'value' => 'Очистить',
                'class' => 'btn btn-default ',
            ],
        ]);
        
        $this->add([
            'name' => 'supplementStateId',
            'type' => 'select',
            'options' => [
                'label' => 'Статус приложения',
            ],
        ]);

        $dateLabels = ['issueFrom', 'issueTo'];

        foreach ($dateLabels as $label) {
            $this->get($label)->setOptions([
                'label_attributes' => [
                    'class' => 'control-label col-sm-1',
                ],
            ]);
        }

    }

    /**
     * 
     * @param array $selectOptions
     */
    public function setSelectOptions(array $selectOptions) {
        $this->selectOptions = $selectOptions;
        $this->prepareSelectOptions();
    }

    protected function prepareSelectOptions() {
        foreach ($this->selectOptions as $key => $value) {
            if ($this->has($key)) {
                $this->get($key)->setOptions([
                    'value_options' => $value,
                ]);
            }
        }
    }

    /**
     * 
     * @return array
     */
    public function getSelectOptions() {
        return $this->selectOptions;
    }

}
