<?php

namespace LicensesReestr\Form;

use Zend\Form\Form;

/**
 * Description of CheckForm
 *
 * @author Дмитрий Бубякин
 */
class CheckForm extends Form {

 

    public function __construct($name = 'checkForm', $options = []) {
        parent::__construct($name, $options);

        $this->setAttributes([
            'role' => 'form',
            'class' => 'form-horizontal',
        ]);

        $this->add([
            'name' => 'inn',
            'type' => 'text',
            'options' => [
                'label' => 'ИНН',
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
        
        $this->add([
            'name' => 'number',
            'type' => 'text',
            'options' => [
                'label' => 'Номер дела',
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
        
        $this->add([
            'name' => 'year',
            'type' => 'text',
            'options' => [
                'label' => 'Год регистрации',
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);

        $this->add([
            'name'       => 'submit',
            'type'       => 'submit',
            'attributes' => [
                'value' => 'Поиск',
                'class' => 'btn btn-primary',
            ],
        ]);


    }


}
