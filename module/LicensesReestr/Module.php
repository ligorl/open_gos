<?php

namespace LicensesReestr;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use LicensesReestr\Adapter\TableAdapter;
use Reestr\Model\SearchEngine;

/**
 * Description of Module
 *
 * @author Дмитрий Бубякин
 */
class Module implements
AutoloaderProviderInterface, ConfigProviderInterface, ServiceProviderInterface {

    public function init(\Zend\ModuleManager\ModuleManager $moduleManager) {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
            $controller = $e->getTarget();
            if (is_readable(__DIR__ . '/view/layout/layout.phtml')) {
                $controller->layout('layout/licenses_reestr');
            }
        }, 100);
    }

    public function getAutoloaderConfig() {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return [
            'factories' => [
                'LicensesReestr\SearchEngine' => function(ServiceLocatorInterface $sm) {
                    return new SearchEngine(new TableAdapter($sm));
                }
            ],
        ];
    }

}
