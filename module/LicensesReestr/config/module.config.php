<?php

namespace LicensesReestr;

use Zend\Mvc\Router\Http\Segment;

return [
    'controllers'  => [
        'factories' => [
            Controller\IndexController::class => Factory\IndexControllerFactory::class,
        ],
    ],
    'router'       => [
        'routes' => [
            'rlic' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/rlic/[:action/][:guid/][:xml/]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'guid'   => '[a-fA-F0-9-]*',
                        'xml'    => '^[0-9a-f]{32}.xml',
                    ],
                    'defaults'    => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'layout/licenses_reestr'            => __DIR__ . '/../view/layout/layout.phtml',
            'error/404'                         => __DIR__ . '/../view/error/404.phtml',
            'error/index'                       => __DIR__ . '/../view/error/index.phtml',
            'licenses-reestr/partial/paginator' => __DIR__ . '/../view/licenses-reestr/index/partial/paginator.phtml',
        ],
        'template_path_stack'      => [
            __DIR__ . '/../view',
        ],
        'layout'                   => 'layout/licenses_reestr',
        'strategies'               => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'prepare' => View\Helper\PrepareHelper::class,
        ],
    ],
];
