<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueReasonRepository extends EntityRepository
{
    public function getByCode($code)
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM Application\Entity\ElectronicQueueReason r WHERE r.code = :code');
        $query->setParameter('code', $code);
        return $query->getOneOrNullResult();
    }
}