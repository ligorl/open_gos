<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueLogRepository extends EntityRepository
{
    public function getRecent()
    {
        $query = $this->getEntityManager()->createQuery('SELECT l FROM Application\Entity\ElectronicQueueLog l ORDER BY l.doneAt DESC');
        $query->setMaxResults(100);
        return $query->getResult();
    }
}