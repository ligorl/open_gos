<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ElectronicQueueEntrySlotRepository::class)
 * @ORM\Table(name="ElectronicQueueEntrySlots")
 */
class ElectronicQueueEntrySlot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="entryId", type="string", length=32)
     * @var string
     */
    protected $entryId;

    /**
     * @ORM\Column(name="appealTypeId", type="string", length=32)
     * @var string
     */
    protected $appealTypeId;

    /**
     * @ORM\Column(name="slot", type="datetime")
     * @var \DateTime
     */
    protected $slot;

    /**
     * @return string
     */
    public function getEntryId()
    {
        return $this->entryId;
    }

    /**
     * @param string $entryId
     */
    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
    }

    /**
     * @return string
     */
    public function getAppealTypeId()
    {
        return $this->appealTypeId;
    }

    /**
     * @param string $appealTypeId
     */
    public function setAppealTypeId($appealTypeId)
    {
        $this->appealTypeId = $appealTypeId;
    }

    /**
     * @return \DateTime
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param \DateTime $slot
     */
    public function setSlot($slot)
    {
        $this->slot = clone $slot;
    }
}