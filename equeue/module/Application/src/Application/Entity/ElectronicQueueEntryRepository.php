<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueEntryRepository extends EntityRepository
{
    /**
     * Флак отображающий о необходимости вывода дополнительного блока «Перечень укрупненных групп специальностей».
     * @var bool
     */
    protected  $additionalBlock = false;

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @param string $reasonId
     * @return ElectronicQueueEntry[]
     */
    public function getForDates($from, $to, $reasonId = null)
    {
        $q = 'SELECT e FROM Application\Entity\ElectronicQueueEntry e WHERE e.startReceptionTime >= :from AND e.endReceptionTime <= :to AND e.status != :status_cancelled';
        $reasons = [];
        if ($reasonId !== null) {
            $reasons[] = $reasonId;
            if ($reasonId == '4e53f68a10974c8a99b15258544b3930') {
                $reasons[] = 'fb80f7c6613e458aaddf7dd0b1ca9b5d';
            } elseif ($reasonId == 'fb80f7c6613e458aaddf7dd0b1ca9b5d') {
                $reasons[] = '4e53f68a10974c8a99b15258544b3930';
            }
            $q .= ' AND e.electronicQueueReasonId IN (:reasons)';

        }
        $query = $this->getEntityManager()->createQuery($q);
        $f = clone $from;
        $f->setTime(0, 0, 0);
        $query->setParameter('from', $f);
        $t = clone $to;
        $t->setTime(23, 59, 59);
        $query->setParameter('to', $t);
        $query->setParameter('status_cancelled', 'cancelled');
        if ($reasonId !== null) {
            $query->setParameter('reasons', $reasons);
        }
        return $query->getResult();
    }

    public function getExpiredForConfirm()
    {
        $minimalDateTime = new \DateTime();
        $minimalDateTime->modify('-1 day');
        $query = $this->getEntityManager()->createQuery('SELECT e FROM Application\Entity\ElectronicQueueEntry e WHERE e.status = :status AND e.createdDateTime <= :minimalDateTime');
        $query->setParameters([
            'status' => 'unapproved',
            'minimalDateTime' => $minimalDateTime
        ]);
        return $query->getResult();
    }

    public function deleteWithSlots($ids)
    {
        $query = $this->getEntityManager()->createQuery('UPDATE Application\Entity\ElectronicQueueEntry e SET e.status = :status WHERE e.id IN (:ids)');
        $query->setParameter('status', 'cancelled');
        $query->setParameter('ids', $ids);
        $query->execute();

        $query = $this->getEntityManager()->createQuery('DELETE Application\Entity\ElectronicQueueEntrySlot s WHERE s.entryId IN (:ids)');
        $query->setParameter('ids', $ids);
        $query->execute();
    }

    public function getAddressesForReasons($reasonIds)
    {
        $query = $this->getEntityManager()->createQuery('SELECT e FROM Application\Entity\ElectronicQueueEntry e WHERE e.electronicQueueReasonId IN (:reasons) AND e.startReceptionTime >= :startDate AND e.startReceptionTime <= :endDate');
        $query->setParameters([
            'reasons' => $reasonIds,
            'startDate' => new \DateTime('2016-11-02 00:00:00'),
            'endDate' => new \DateTime('2016-11-03 23:59:59')
        ]);

        $result = [];
        /** @var ElectronicQueueEntry[] $tmp */
        $tmp = $query->execute();
        foreach ($tmp as $entry) {
            $mail = $entry->getEMail();
            if (!in_array($mail, $result)) $result[] = $mail;
        }

        return $result;
    }

    /**
     * @param int $reasonId
     * @param \DateTime $date
     * @param bool $cancelled
     * @return array
     */
    public function getForCalendar($reasonId, $date, $cancelled)
    {
        $q = 'SELECT e FROM Application\Entity\ElectronicQueueEntry e WHERE e.electronicQueueReasonId = :reasonId AND e.startReceptionTime >= :from AND e.endReceptionTime <= :to';
        if ($cancelled) {
            $q .= ' AND e.status = :cancelled';
        } else {
            $q .= ' AND e.status != :cancelled';
        }
        $query = $this->getEntityManager()->createQuery($q);
        $query->setParameter('reasonId', $reasonId);
        $f = clone $date;
        $f->setTime(0, 0, 0);
        $query->setParameter('from', $f);
        $t = clone $date;
        $t->setTime(23, 59, 59);
        $query->setParameter('to', $t);
        $query->setParameter('cancelled', 'cancelled');
        $sql = $query->getSQL();
        return $query->getResult();
    }

    /**
     * Возвращает масив модели ElectronicQueueReason.
     * Если задать $param то вернётся элемент с ключём $param.
     *
     * @param $em
     * @param $id
     * @param $param
     *
     * @return string||array
     */
    public function getServiceType($em, $id, $param = null)
    {
        $entety = $em->createQueryBuilder()
            ->select('e')
            ->from('\Application\Entity\ElectronicQueueReason', 'e')
            ->where('e.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult(\PDO::FETCH_ASSOC);

        if (!empty($param)){
            $result = array_shift($entety);
            return $result[$param];
        }
        return array_shift($entety);
    }

    /**
     * Возвращает масив модели ServiceReasonCatalog.
     * Если задать $param то вернётся элемент с ключём $param.
     * Основание обращения.
     *
     * @param $em
     * @param $id
     * @param $param
     *
     * @return string||array
     */
    public function getServiceReasonCatalog($em, $id, $param = null)
    {
        $entety = $em->createQueryBuilder()
            ->select('e')
            ->from('\Application\Entity\ServiceReasonCatalog', 'e')
            ->where('e.id = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult(\PDO::FETCH_ASSOC);

        if (!empty($param)){
            $result = array_shift($entety);
            return $result[$param];
        }
        return array_shift($entety);
    }

    /**
     * Возвращает причины обращения.
     *
     * @param $em
     * @param $id
     * @param $type
     *
     * @return array|string
     */
    public function getBasisTreatment($em, $id, $type)
    {
        if ($type == 'accreditation'){
            $arrayReasons = $this->getCausesAccreditation($em, $id);
        }elseif ($type == 'licensing') {
            $arrayReasons = $this->getCausesLicensing($em, $id);
        }


        $listCauses = [];
        $reasons = [];
        foreach ($arrayReasons as $array){
            if(!is_null($array['name']) && mb_strlen($array['name']) > 1 ){
                array_push($listCauses, sprintf("- %s", $array['name']));
            }
            $reasons[$array['id']] = $array['name'];
        }

        return array(
            'listCauses' => implode(", \n", $listCauses),
            'reasons' => $reasons,
            'additionalBlock' => $this->additionalBlock,
        );
    }

    /**
     * Получение причин обращения при Аккредетации.
     *
     * @param $em
     * @param $id
     *
     * @return mixed
     */
    public function getCausesAccreditation($em, $id)
	{
        // id Причин при выборе которых необходимо отображать дополниттельный блок «Перечень укрупненных групп специальностей».
        $arrayIdCauses = ['24e060cb-6384-4009-b9ce-93a775678732', '67f97827-57f9-4a0c-b2c7-d9806969bef6'];

        $entetyAccred = $em->createQueryBuilder()
            ->select('a.accredApplicationReasons as accred')
            ->from('\Application\Entity\ElectronicQueueEntryToAccreditationReason', 'a')
            ->where('a.electronicQueueEntryId = :electronicQueueEntryId')
            ->setParameter('electronicQueueEntryId', $id)
            ->getQuery()->getResult(\PDO::FETCH_ASSOC);

        $arrayId = [];
        foreach ($entetyAccred as $array){
            $id = $array['accred'];
            array_push($arrayId, $id);

            if(in_array($id, $arrayIdCauses)){
                $this->additionalBlock = true;
            }
        }

        $arrayCauses = $em->createQueryBuilder()
            ->select('i.id as id, i.name')
            ->from('\Application\Entity\isgaApplicationReasons', 'i')
            ->where('i.id IN (:id)')
            ->setParameter('id', $arrayId)
            ->getQuery()->getResult(\PDO::FETCH_ASSOC);

        return $arrayCauses;
    }

    /**
     * Получение причин обращения при Лицензировании.
     *
     * @param $em
     * @param $id
     *
     * @return mixed
     */
    private function getCausesLicensing($em, $id)
    {
        $arrayId = $this->getIdReasonLic($em, $id);

        $arrayCauses = $em->createQueryBuilder()
            ->select('l.id as id', 'l.title as name')
            ->from('\Application\Entity\LicenseReasonCatalogItem', 'l')
            ->where('l.id IN (:id)')
            ->setParameter('id', $arrayId)
            ->getQuery()->getResult(\PDO::FETCH_ASSOC);

        return $arrayCauses;
    }

    /**
     * Получение id причин обращения при Лицензировании.
     *
     * @param $em
     * @param $id
     *
     * @return mixed
     */
    public function getIdReasonLic($em, $id)
    {
        $entetyLic = $em->createQueryBuilder()
            ->select('e.licenseReasonCatalogItemId as license')
            ->from('\Application\Entity\ElectronicQueueEntryToLicenseReasonCatalogItem', 'e')
            ->where('e.electronicQueueEntryId = :id')
            ->setParameter('id', $id)
            ->getQuery()->getResult(\PDO::FETCH_ASSOC);

        $arrayId = [];
        foreach ($entetyLic as $array){
            array_push($arrayId, $array['license']);
        }

        return $arrayId;
    }


}