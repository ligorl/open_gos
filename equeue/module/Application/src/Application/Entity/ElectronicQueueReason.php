<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\ElectronicQueueReasonRepository;

/**
 * @ORM\Entity(repositoryClass=ElectronicQueueReasonRepository::class)
 * @ORM\Table(name="ElectronicQueueReason")
 */
class ElectronicQueueReason
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", length=32)
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="name", type="string", length=100)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="serviceType", type="string", length=32)
     * @var string
     */
    protected $serviceType;

    /**
     * @ORM\Column(name="roomNumber", type="string", length=16)
     * @var string
     */
    protected $roomNumber;

    /**
     * @ORM\Column(name="baseReceptionTime", type="integer")
     * @var int
     */
    protected $baseReceptionTime;

    /**
     * @ORM\Column(name="startReceptionTime", type="time")
     * @var \DateTime
     */
    protected $startReceptionTime;

    /**
     * @ORM\Column(name="endReceptionTime", type="time")
     * @var \DateTime
     */
    protected $endReceptionTime;

    /**
     * @ORM\Column(name="breakStartTime", type="time")
     * @var \DateTime
     */
    protected $breakStartTime;

    /**
     * @ORM\Column(name="breakEndTime", type="time")
     * @var \DateTime
     */
    protected $breakEndTime;

    /**
     * @ORM\Column(name="receptionDaysRaw", type="string", length=100)
     * @var string
     */
    protected $receptionDaysRaw;

    /**
     * @ORM\Column(name="recordMonthsBelow", type="integer")
     * @var int
     */
    protected $recordMonthsBelow;

    /**
     * @ORM\Column(name="notificationEmails", type="text")
     * @var string
     */
    protected $notificationEmails;

    /**
     * @ORM\Column(name="eqNotifyMails", type="text")
     * @var string[]
     */
    protected $eqNotifyMails;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRoomNumber()
    {
        return $this->roomNumber;
    }

    /**
     * @return int
     */
    public function getBaseReceptionTime()
    {
        return $this->baseReceptionTime;
    }

    /**
     * @return \DateTime
     */
    public function getStartReceptionTime()
    {
        return $this->startReceptionTime;
    }

    /**
     * @return \DateTime
     */
    public function getEndReceptionTime()
    {
        return $this->endReceptionTime;
    }

    /**
     * @return \DateTime
     */
    public function getBreakStartTime()
    {
        return $this->breakStartTime;
    }

    /**
     * @return \DateTime
     */
    public function getBreakEndTime()
    {
        return $this->breakEndTime;
    }

    /**
     * @return string
     */
    public function getReceptionDaysRaw()
    {
        return $this->receptionDaysRaw;
    }

    /**
     * @return int
     */
    public function getRecordMonthsBelow()
    {
        return $this->recordMonthsBelow;
    }

    /**
     * @return string
     */
    public function getNotificationEmails()
    {
        return $this->notificationEmails;
    }

    public function getEqNotifyMails()
    {
        return array_filter(explode(';', $this->eqNotifyMails));
    }

    /**
     * @return string
     */
    public function getServiceType()
    {
        return $this->serviceType;
    }
}