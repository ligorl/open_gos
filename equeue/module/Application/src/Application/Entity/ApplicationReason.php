<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\ApplicationReasonRepository;

/**
 * @ORM\Entity(repositoryClass=ApplicationReasonRepository::class)
 * @ORM\Table(name="isga_ApplicationReasons", schema="f11_mon")
 */
class ApplicationReason
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="Id", type="string", length=255)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="Code", type="string", length=50)
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="Name", type="string", length=2000)
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="ShortName", type="string", length=255)
     * @var string
     */
    protected $shortName;

    /**
     * @ORM\Column(name="ServiceReasonCode", type="string", length=255)
     * @var string
     */
    protected $serviceReasonCode;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getServiceReasonCode()
    {
        return $this->serviceReasonCode;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }
}