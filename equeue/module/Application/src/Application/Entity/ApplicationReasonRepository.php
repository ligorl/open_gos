<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ApplicationReasonRepository extends EntityRepository
{
    /**
     * @return ApplicationReason[]
     */
    public function getAll()
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM Application\Entity\ApplicationReason r WHERE r.serviceReasonCode IS NOT NULL AND r.serviceReasonCode <> :empty');
        $query->setParameter('empty', '');
        return $query->getResult();
    }
}