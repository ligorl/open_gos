<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class NumberOfProgramCatalogRepository extends EntityRepository
{
    public function getAll()
    {
        $query = $this->getEntityManager()->createQuery('SELECT c FROM Application\Entity\NumberOfProgramCatalog c ORDER BY c.coefficient ASC');
        return $query->getResult();
    }
}