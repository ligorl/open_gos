<?php
namespace Application\Entity;

use Doctrine\ORM\EntityRepository;


class PaymentOrderRepository extends EntityRepository
{
    /**
     * Поис модели по типу, основанию и причине.
     *
     * @param $em
     * @param $search
     *
     * @return mixed
     */
    public function getEntity($em, $search)
    {
        $reasonId = key($search['reasons']);
        $searchParameter = array(
            'electronicQueueReasonId' => $search['type'],
            'serviceReasonCatalogId' => $search['serviceReason'],
        );

        if($reasonId){
            $parameter = '= :reasonId';
            $searchParameter['reasonId'] = $reasonId;
        }else{
            $parameter = 'IS NULL';
        }

        $entity = $em->createQueryBuilder()
            ->select('p')
            ->from('\Application\Entity\PaymentOrder', 'p')
            ->where('p.electronicQueueReasonId = :electronicQueueReasonId')
            ->andWhere('p.serviceReasonCatalogId = :serviceReasonCatalogId')
            ->andWhere('p.reasonId ' . $parameter)
            ->setParameters($searchParameter)
            ->getQuery()->getResult(\PDO::FETCH_ASSOC);

        return array_shift($entity);
    }

    /**
     * Поиск модели по типу, основанию и причине.
     *
     * @param $em
     * @param $search
     *
     * @return mixed
     */
    public function getEntityComplex($em, $search)
    {
        $count = count($search['reasons']);
        $reasonsId = array_keys($search['reasons']);
        $strReasonsId = [];
        foreach ($reasonsId as $reasonId){
            array_push($strReasonsId,  "'" . $reasonId . "'");
        }
        $strReasonsId = implode(', ', $strReasonsId);

        $sql = "SELECT * FROM"
                ." (SELECT po.id, po.name, po.uin, po.price, COUNT(po.id) as number FROM"
                ." (SELECT * FROM `PaymentOrder` WHERE numberReasons = " . $count . ") as po"
                ." JOIN `ComplexReason` as cr ON po.id = cr.PaymentOrderId"
                ." WHERE cr.reasonId IN (" . $strReasonsId . ")"
                ." GROUP BY po.id) as result"
                ." WHERE result.number = " . $count;

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
//        var_dump($result[0]);
//        die;
//        $result[0]['name'] = 'ваывывыввы';
//        echo "<pre>";
//        die;
        return array_shift($result);
    }

}