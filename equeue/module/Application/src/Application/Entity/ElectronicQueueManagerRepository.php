<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueManagerRepository extends EntityRepository
{
    public function getByLogin($login)
    {
        $query = $this->getEntityManager()->createQuery('SELECT m FROM Application\Entity\ElectronicQueueManager m WHERE m.login = :login');
        $query->setParameter('login', $login);
        return $query->getOneOrNullResult();
    }

    public function getByIds($ids)
    {
        $query = $this->getEntityManager()->createQuery('SELECT m FROM Application\Entity\ElectronicQueueManager m WHERE m.id IN (:ids)');
        $query->setParameter('ids', $ids);
        $r = $query->getResult();
        $result = [];
        foreach ($r as $ri) {
            $result[$ri->getId()] = $ri;
        }
        return $result;
    }
}