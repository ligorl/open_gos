<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\EducationalOrganizationRepository;

/**
 * @ORM\Entity(repositoryClass=EducationalOrganizationRepository::class)
 * @ORM\Table(name="eiis_EducationalOrganizations", schema="f11_mon")
 */
class EducationalOrganization
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="Id", type="string", length=255)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="FullName", length=1024)
     * @var string
     */
    protected $fullName;

    /**
     * @ORM\Column(name="RegularName", length=1024)
     * @var string
     */
    protected $regularName;

    /**
     * @ORM\Column(name="Inn", type="string", length=20)
     * @var string
     */
    protected $inn;

    /**
     * @ORM\Column(name="Kpp", type="string", length=16)
     * @var string
     */
    protected $kpp;

    /**
     * @ORM\Column(name="Branch", type="boolean")
     * @var bool
     */
    protected $branch;

    /**
     * @ORM\Column(name="lod_stateCode", type="string", length=255)
     * @var string
     */
    protected $stateCode;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @return string
     */
    public function getRegularName()
    {
        return $this->regularName;
    }

    /**
     * @return string
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * @return string
     */
    public function getKpp()
    {
        return $this->kpp;
    }

    /**
     * @return boolean
     */
    public function isBranch()
    {
        return $this->branch;
    }
}