<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class isgaApplicationReasonsRepository extends EntityRepository
{
    /**
     * @return LicenseReasonCatalogItem[]
     */
    public function getAll()
    {
        $query = $this->getEntityManager()->createQuery('SELECT i FROM Application\Entity\isgaApplicationReasonsEntity i');
        return $query->getResult();
    }
}