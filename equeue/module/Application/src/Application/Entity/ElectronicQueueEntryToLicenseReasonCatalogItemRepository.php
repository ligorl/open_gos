<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueEntryToLicenseReasonCatalogItemRepository extends EntityRepository
{
    public function getForEntry($entryId)
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM Application\Entity\ElectronicQueueEntryToLicenseReasonCatalogItem r WHERE r.electronicQueueEntryId = :entryId');
        $query->setParameter('entryId', $entryId);
        return $query->getResult();
    }
}