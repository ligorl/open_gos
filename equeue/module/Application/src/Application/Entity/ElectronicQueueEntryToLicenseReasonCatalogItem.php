<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Doctrine\IdGenerator;

/**
 * @ORM\Entity(repositoryClass=ElectronicQueueEntryToLicenseReasonCatalogItemRepository::class)
 * @ORM\Table(name="ElectronicQueueEntryToLicenseReasonCatalogItem")
 */
class ElectronicQueueEntryToLicenseReasonCatalogItem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=IdGenerator::class)
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="electronicQueueEntryId", type="string", length=32)
     * @var string
     */
    protected $electronicQueueEntryId;

    /**
     * @ORM\Column(name="licenseReasonCatalogItemId", type="string", length=32)
     * @var string
     */
    protected $licenseReasonCatalogItemId;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getElectronicQueueEntryId()
    {
        return $this->electronicQueueEntryId;
    }

    /**
     * @param string $electronicQueueEntryId
     */
    public function setElectronicQueueEntryId($electronicQueueEntryId)
    {
        $this->electronicQueueEntryId = $electronicQueueEntryId;
    }

    /**
     * @return string
     */
    public function getLicenseReasonCatalogItemId()
    {
        return $this->licenseReasonCatalogItemId;
    }

    /**
     * @param string $licenseReasonCatalogItemId
     */
    public function setLicenseReasonCatalogItemId($licenseReasonCatalogItemId)
    {
        $this->licenseReasonCatalogItemId = $licenseReasonCatalogItemId;
    }
}