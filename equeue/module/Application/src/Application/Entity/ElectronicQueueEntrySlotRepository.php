<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueEntrySlotRepository extends EntityRepository
{
    public function getForEntry($entryId)
    {
        $query = $this->getEntityManager()->createQuery('SELECT s FROM Application\Entity\ElectronicQueueEntrySlot s WHERE s.entryId = :entryId');
        $query->setParameter('entryId', $entryId);
        return $query->getResult();
    }
    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return ElectronicQueueEntrySlot[]
     */
    public function getBySlot($from, $to)
    {
        $q = 'SELECT s FROM Application\Entity\ElectronicQueueEntrySlot s WHERE s.slot > :from AND s.slot < :to';

        $query = $this->getEntityManager()->createQuery($q);
        $query->setParameter('from', $from);
        $query->setParameter('to', $to);
        return $query->getResult();
    }
}