<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\ElectronicQueueVacationRepository;

/**
 * @ORM\Entity(repositoryClass=ElectronicQueueVacationRepository::class)
 * @ORM\Table(name="ElectronicQueueVacations")
 */
class ElectronicQueueVacation
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue()
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="date_from", type="datetime")
     * @var \DateTime
     */
    protected $from;

    /**
     * @ORM\Column(name="date_to", type="datetime")
     * @var \DateTime
     */
    protected $to;

    /**
     * @ORM\Column(name="reason_id", type="string", length=32)
     * @var string
     */
    protected $reasonId;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param \DateTime $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param \DateTime $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getReasonId()
    {
        return $this->reasonId;
    }

    /**
     * @param string $reasonId
     */
    public function setReasonId($reasonId)
    {
        $this->reasonId = $reasonId;
    }
}