<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="isga_ExchangeTable")
 */
class Exchange
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="Id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="Timestamp", type="datetime")
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * @ORM\Column(name="SaveType", type="string", length=50)
     * @var string
     */
    protected $saveType;

    /**
     * @ORM\Column(name="TableName", type="string", length=100)
     * @var string
     */
    protected $tableName;

    /**
     * @ORM\Column(name="ObjectId", type="string", length=255)
     * @var string
     */
    protected $objectId;

    /**
     * @ORM\Column(name="Source", type="string", length=10)
     * @var string
     */
    protected $source;

    /**
     * @ORM\Column(name="State", type="string", length=20)
     * @var string
     */
    protected $state = '';

    /**
     * @ORM\Column(name="Error", type="string", length=1000)
     * @var string
     */
    protected $error = '';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     * @return Exchange
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return string
     */
    public function getSaveType()
    {
        return $this->saveType;
    }

    /**
     * @param string $saveType
     * @return Exchange
     */
    public function setSaveType($saveType)
    {
        $this->saveType = $saveType;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return Exchange
     */
    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
        return $this;
    }

    /**
     * @return string
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param string $objectId
     * @return Exchange
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return Exchange
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return Exchange
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     * @return Exchange
     */
    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }
}