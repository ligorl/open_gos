<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\ElectronicQueueManagerRepository;

/**
 * @ORM\Entity(repositoryClass=ElectronicQueueManagerRepository::class)
 * @ORM\Table(name="ElectronicQueueManagers")
 */
class ElectronicQueueManager
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="login", type="string", length=255)
     * @var string
     */
    protected $login;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     * @var string
     */
    protected $password;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return ElectronicQueueManager
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return ElectronicQueueManager
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setPasswordRaw($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    public function checkPassword($password)
    {
        return password_verify($password, $this->password);
    }
}