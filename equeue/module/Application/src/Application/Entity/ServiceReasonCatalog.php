<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\ServiceReasonCatalogRepository;

/**
 * @ORM\Entity(repositoryClass=ServiceReasonCatalogRepository::class)
 * @ORM\Table(name="ServiceReasonCatalog")
 */
class ServiceReasonCatalog
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", length=255)
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="name", type="text")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="appealTypeCode", type="string", length=255)
     * @var string
     */
    protected $appealTypeCode;

    /**
     * @ORM\Column(name="sort", type="integer")
     * @var int
     */
    protected $sort;

    /**
     * @ORM\Column(name="appealReasonRequired", type="boolean")
     * @var bool
     */
    protected $appealReasonRequired;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAppealTypeCode()
    {
        return $this->appealTypeCode;
    }

    public function getAppealReasonRequired()
    {
        return $this->appealReasonRequired;
    }
}