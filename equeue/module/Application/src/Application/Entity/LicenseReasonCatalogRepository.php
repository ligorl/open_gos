<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class LicenseReasonCatalogRepository extends EntityRepository
{
    /**
     * @return LicenseReasonCatalogItem[]
     */
    public function getAll()
    {
        $query = $this->getEntityManager()->createQuery('SELECT i FROM Application\Entity\LicenseReasonCatalogItem i ORDER BY i.code ASC');
        return $query->getResult();
    }
}