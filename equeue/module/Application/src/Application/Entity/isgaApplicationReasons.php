<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=isgaApplicationReasonsRepository::class)
 * @ORM\Table(name="isga_ApplicationReasons", schema="f11_mon")
 */
class isgaApplicationReasons
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string")
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="fk_isgaApplicationType", type="string")
     * @var string
     */
    protected $fkIsgaApplicationType;

    /**
     * @ORM\Column(name="ServiceReasonCode", type="string")
     * @var string
     */
    protected $serviceReasonCode;

    /**
     * @ORM\Column(name="Code", type="string")
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="Name", type="string")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="Ordering", type="integer")
     * @var string
     */
    protected $ordering;

    /**
     * @ORM\Column(name="ShortName", type="string")
     * @var string
     */
    protected $shortName;

    /**
     * @ORM\Column(name="CreateDefaultApplicant", type="integer")
     * @var string
     */
    protected $createDefaultApplicant;

    /**
     * @ORM\Column(name="NamePrepositionalCase", type="string")
     * @var string
     */
    protected $namePrepositionalCase;

    /**
     * @ORM\Column(name="ShowPreviousCertificate", type="integer")
     * @var string
     */
    protected $showPreviousCertificate;

    /**
     * @ORM\Column(name="GroupCode", type="string")
     * @var string
     */
    protected $groupCode;

    /**
     * @ORM\Column(name="AblativeName", type="string")
     * @var string
     */
    protected $ablativeName;

    /**
     * @ORM\Column(name="GroupSubtype", type="integer")
     * @var string
     */
    protected $groupSubtype;

}