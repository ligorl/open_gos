<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Doctrine\IdGenerator;

/**
 * @ORM\Entity(repositoryClass=ElectronicQueueEntryRepository::class)
 * @ORM\Table(name="ElectronicQueueEntry")
 */
class ElectronicQueueEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=IdGenerator::class)
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="organizationId", type="string", length=50)
     * @var string
     */
    protected $organizationId;

    /**
     * @ORM\Column(name="electronicQueueReasonId", type="string", length=32)
     * @var string
     */
    protected $electronicQueueReasonId;

    /**
     * @ORM\Column(name="serviceReasonCatalogId", type="string", length=32)
     * @var string
     */
    protected $serviceReasonCatalogId;

    /**
     * @ORM\Column(name="filialAmount", type="integer")
     * @var int
     */
    protected $filialAmount;

    /**
     * @ORM\Column(name="isBranchesOnly", type="integer")
     * @var int
     */
    protected $isBranchesOnly;

    /**
     * @ORM\Column(name="numberOfProgramCatalogId", type="string", length=32)
     * @var string
     */
    protected $numberOfProgramCatalogId;

    /**
     * @ORM\Column(name="numberOfDocumentsId", type="string", length=32)
     * @var string
     */
    protected $numberOfDocumentsId;

    /**
     * @ORM\Column(name="issueDocumentsAmountId", type="string", length=32)
     * @var string
     */
    protected $issueDocumentsAmountId;

    /**
     * @ORM\Column(name="startReceptionTime", type="datetime")
     * @var \DateTime
     */
    protected $startReceptionTime;

    /**
     * @ORM\Column(name="lastName", type="string", length=128)
     * @var string
     */
    protected $lastName;

    /**
     * @ORM\Column(name="firstName", type="string", length=128)
     * @var string
     */
    protected $firstName;

    /**
     * @ORM\Column(name="middleName", type="string", length=128)
     * @var string
     */
    protected $middleName;

    /**
     * @ORM\Column(name="phoneNumber", type="string", length=32)
     * @var string
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(name="mobilePhoneNumber", type="string", length=32)
     * @var string
     */
    protected $mobilePhoneNumber;

    /**
     * @ORM\Column(name="eMail", type="string", length=64)
     * @var string
     */
    protected $eMail;

    /**
     * @ORM\Column(name="createdDateTime", type="datetime")
     * @var \DateTime
     */
    protected $createdDateTime;

    /**
     * @ORM\Column(name="updatedDateTime", type="datetime")
     * @var \DateTime
     */
    protected $updatedDateTime;

    /**
     * @ORM\Column(name="confirmedDateTime", type="datetime", nullable=true)
     * @var \DateTime
     */
    protected $confirmedDateTime;

    /**
     * @ORM\Column(name="status", type="string", length=64)
     * @var string
     */
    protected $status;

    /**
     * @ORM\Column(name="licensingProcedureId", type="string", length=32)
     * @var string
     */
    protected $licensingProcedureId;

    /**
     * @ORM\Column(name="endReceptionTime", type="datetime")
     * @var \DateTime
     */
    protected $endReceptionTime;

    /**
     * @ORM\Column(name="sevenDaysNotificationSent", type="boolean")
     * @var bool
     */
    protected $sevenDaysNotificationSent;

    /**
     * @ORM\Column(name="secretKey", type="string", length=64)
     * @var string
     */
    protected $secretKey;

    /**
     * @ORM\Column(name="organizationName", type="string", length=255)
     * @var string
     */
    protected $organizationName;

    /**
     * @ORM\Column(name="organizationInn", type="string", length=20)
     * @var string
     */
    protected $organizationInn;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return ElectronicQueueEntry
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * @param string $organizationId
     * @return ElectronicQueueEntry
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getElectronicQueueReasonId()
    {
        return $this->electronicQueueReasonId;
    }

    /**
     * @param string $electronicQueueReasonId
     * @return ElectronicQueueEntry
     */
    public function setElectronicQueueReasonId($electronicQueueReasonId)
    {
        $this->electronicQueueReasonId = $electronicQueueReasonId;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceReasonCatalogId()
    {
        return $this->serviceReasonCatalogId;
    }

    /**
     * @param string $serviceReasonCatalogId
     * @return ElectronicQueueEntry
     */
    public function setServiceReasonCatalogId($serviceReasonCatalogId)
    {
        $this->serviceReasonCatalogId = $serviceReasonCatalogId;
        return $this;
    }

    /**
     * @return int
     */
    public function getFilialAmount()
    {
        return $this->filialAmount;
    }

    /**
     * @return int
     */
    public function getIsBranchesOnly()
    {
        return $this->isBranchesOnly;
    }

    /**
     * @param int $filialAmount
     * @return ElectronicQueueEntry
     */
    public function setFilialAmount($filialAmount)
    {
        $this->filialAmount = $filialAmount;
        return $this;
    }

    /**
     * @param int $isBranchesOnly
     * @return ElectronicQueueEntry
     */
    public function setIsBranchesOnly($isBranchesOnly)
    {
        $this->isBranchesOnly = $isBranchesOnly;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberOfProgramCatalogId()
    {
        return $this->numberOfProgramCatalogId;
    }

    /**
     * @param string $numberOfProgramCatalogId
     * @return ElectronicQueueEntry
     */
    public function setNumberOfProgramCatalogId($numberOfProgramCatalogId)
    {
        $this->numberOfProgramCatalogId = $numberOfProgramCatalogId;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumberOfDocumentsId()
    {
        return $this->numberOfDocumentsId;
    }

    /**
     * @param string $numberOfDocumentsId
     * @return ElectronicQueueEntry
     */
    public function setNumberOfDocumentsId($numberOfDocumentsId)
    {
        $this->numberOfDocumentsId = $numberOfDocumentsId;
        return $this;
    }

    /**
     * @return string
     */
    public function getIssueDocumentsAmountId()
    {
        return $this->issueDocumentsAmountId;
    }

    /**
     * @param string $issueDocumentsAmountId
     * @return ElectronicQueueEntry
     */
    public function setIssueDocumentsAmountId($issueDocumentsAmountId)
    {
        $this->issueDocumentsAmountId = $issueDocumentsAmountId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStartReceptionTime()
    {
        return $this->startReceptionTime;
    }

    /**
     * @param \DateTime $startReceptionTime
     * @return ElectronicQueueEntry
     */
    public function setStartReceptionTime($startReceptionTime)
    {
        $this->startReceptionTime = $startReceptionTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return ElectronicQueueEntry
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return ElectronicQueueEntry
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return ElectronicQueueEntry
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return ElectronicQueueEntry
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getMobilePhoneNumber()
    {
        return $this->mobilePhoneNumber;
    }

    /**
     * @param string $mobilePhoneNumber
     * @return ElectronicQueueEntry
     */
    public function setMobilePhoneNumber($mobilePhoneNumber)
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getEMail()
    {
        return $this->eMail;
    }

    /**
     * @param string $eMail
     * @return ElectronicQueueEntry
     */
    public function setEMail($eMail)
    {
        $this->eMail = $eMail;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDateTime()
    {
        return $this->createdDateTime;
    }

    /**
     * @param \DateTime $createdDateTime
     * @return ElectronicQueueEntry
     */
    public function setCreatedDateTime($createdDateTime)
    {
        $this->createdDateTime = $createdDateTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDateTime()
    {
        return $this->updatedDateTime;
    }

    /**
     * @param \DateTime $updatedDateTime
     * @return ElectronicQueueEntry
     */
    public function setUpdatedDateTime($updatedDateTime)
    {
        $this->updatedDateTime = $updatedDateTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getConfirmedDateTime()
    {
        return $this->confirmedDateTime;
    }

    /**
     * @param \DateTime $confirmedDateTime
     * @return ElectronicQueueEntry
     */
    public function setConfirmedDateTime($confirmedDateTime)
    {
        $this->confirmedDateTime = $confirmedDateTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return ElectronicQueueEntry
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getLicensingProcedureId()
    {
        return $this->licensingProcedureId;
    }

    /**
     * @param string $licensingProcedureId
     * @return ElectronicQueueEntry
     */
    public function setLicensingProcedureId($licensingProcedureId)
    {
        $this->licensingProcedureId = $licensingProcedureId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndReceptionTime()
    {
        return $this->endReceptionTime;
    }

    /**
     * @param \DateTime $endReceptionTime
     * @return ElectronicQueueEntry
     */
    public function setEndReceptionTime($endReceptionTime)
    {
        $this->endReceptionTime = $endReceptionTime;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSevenDaysNotificationSent()
    {
        return $this->sevenDaysNotificationSent;
    }

    /**
     * @param boolean $sevenDaysNotificationSent
     * @return ElectronicQueueEntry
     */
    public function setSevenDaysNotificationSent($sevenDaysNotificationSent)
    {
        $this->sevenDaysNotificationSent = $sevenDaysNotificationSent;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * @param string $secretKey
     * @return ElectronicQueueEntry
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
        return $this;
    }

    /**
     * @param \DateTime $from
     * @param \DateTime $to
     * @return bool
     */
    public function overlapsTime($from, $to)
    {
        return ($from >= $this->startReceptionTime && $from < $this->endReceptionTime)
            || ($to > $this->startReceptionTime && $to <= $this->endReceptionTime);
    }

    /**
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organizationName;
    }

    /**
     * @param string $organizationName
     * @return ElectronicQueueEntry
     */
    public function setOrganizationName($organizationName)
    {
        $this->organizationName = $organizationName;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationInn()
    {
        return $this->organizationInn;
    }

    /**
     * @param string $organizationInn
     * @return ElectronicQueueEntry
     */
    public function setOrganizationInn($organizationInn)
    {
        $this->organizationInn = $organizationInn;
        return $this;
    }

    public function getCalendarData($baseTime)
    {
        $start = $this->startReceptionTime;
        $end = $this->endReceptionTime;
        $span = ceil(($end->getTimestamp() - $start->getTimestamp()) / 60 / $baseTime);
        $name = $this->getOrganizationName();
        return [
            'id' => $this->id,
            'name' => $name,
            'startTime' => $start->format('H:i'),
            'endTime' => $end->format('H:i'),
            'span' => $span,
            'status' => $this->status,
            'reasonId' => $this->electronicQueueReasonId,
            'human' => $this->lastName . ' ' . $this->firstName . ' ' . $this->middleName,
            'wasConfirmed' => $this->confirmedDateTime != null
        ];
    }
}