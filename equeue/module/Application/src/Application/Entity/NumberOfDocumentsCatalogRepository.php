<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class NumberOfDocumentsCatalogRepository extends EntityRepository
{
    public function getAll()
    {
        $query = $this->getEntityManager()->createQuery('SELECT c FROM Application\Entity\NumberOfDocumentsCatalog c ORDER BY c.coefficient ASC');
        return $query->getResult();
    }
}