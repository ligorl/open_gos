<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueVacationRepository extends EntityRepository
{
    public function getAll()
    {
        return $this->getEntityManager()->createQuery('SELECT v FROM Application\Entity\ElectronicQueueVacation v ORDER BY v.from')->getResult();
    }
}