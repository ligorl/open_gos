<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class CatalogItemRepository extends EntityRepository
{
    public function getAppealTypes()
    {
        $query = $this->getEntityManager()->createQuery('SELECT i FROM Application\Entity\CatalogItem i WHERE i.catalog = :catalog ORDER BY i.code ASC');
        $query->setParameter('catalog', 'serviceReason');
        return $query->getResult();
    }
}