<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ElectronicQueueEntryToAccreditationReasonRepository extends EntityRepository
{
    public function getForEntry($entryId)
    {
        $query = $this->getEntityManager()->createQuery('SELECT r FROM Application\Entity\ElectronicQueueEntryToAccreditationReason r WHERE r.electronicQueueEntryId = :entryId');
        $query->setParameter('entryId', $entryId);
        return $query->getResult();
    }
}