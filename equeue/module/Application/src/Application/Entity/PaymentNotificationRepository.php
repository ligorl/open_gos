<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class PaymentNotificationRepository extends EntityRepository
{

    /**
     * Сохранение данных по платёжке.
     *
     * @param $data
     * @param $uin
     * @param $kbk
     *
     * @return string
     */
    public function savePaymentOrder($data, $uin, $kbk)
    {
        $id = $data['paymentNotificationId'];
        $saveType = 'insert';
        if ($id){
            $entity = $this->getEntityManager()->getRepository(PaymentNotification::class)->find($id);
            $saveType = 'update';
        }else{
            $entity = new PaymentNotification();
        }
        $entity->entryId($data['entryId']);
        $entity->organizationName(trim(strip_tags($data['orgName'])));
        $entity->uin($uin);
        $entity->kbk($kbk);
        $entity->summ($data['sum']);
        $entity->bankName($data['bankName']);
        $entity->bankBIK($data['bankBIK']);
        $entity->corAccount($data['corAccount']);
        $entity->numEduAccount($data['numEduAccount']);
        $entity->createDate(date('Y-m-d H:i:s'));
        $entity->isCanceled('0');
        $entity->isReturned('0');

        if (!$id){
            $this->getEntityManager()->persist($entity);
        }
        $this->getEntityManager()->flush();

        $ex = new Exchange();
        $ex->setTimestamp(new \DateTime())
            ->setSaveType($saveType)
            ->setTableName('PaymentNotification')
            ->setObjectId($entity->id())
            ->setSource('vuzi');
        $this->getEntityManager()->persist($ex);
        $this->getEntityManager()->flush();

        return $entity->id();
    }
}