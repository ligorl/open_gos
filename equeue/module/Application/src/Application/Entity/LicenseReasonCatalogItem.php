<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\LicenseReasonCatalogRepository;

/**
 * @ORM\Entity(repositoryClass=LicenseReasonCatalogRepository::class)
 * @ORM\Table(name="LicenseReasonCatalogItem")
 */
class LicenseReasonCatalogItem
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", length=64)
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="title", type="string", length=256)
     * @var string
     */
    protected $title;

    /**
     * @ORM\Column(name="ServiceReasonCode", type="string", length=255)
     * @var string
     */
    protected $serviceReasonCode;

    /**
     * @ORM\Column(name="documentTitle", type="string", length=512)
     * @var string
     */
    protected $documentTitle;

    /**
     * @ORM\Column(name="DocumentPrint", type="string", length=500)
     * @var string
     */
    protected $documentPrint;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function getName()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getServiceReasonCode()
    {
        return $this->serviceReasonCode;
    }

    /**
     * @return string
     */
    public function getDocumentTitle()
    {
        return $this->documentTitle;
    }

    /**
     * @return string
     */
    public function getDocumentPrint()
    {
        return $this->documentPrint;
    }

}