<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\IssueDocumentsAmountRepository;

/**
 * @ORM\Entity(repositoryClass=IssueDocumentsAmountRepository::class)
 * @ORM\Table(name="IssueDocumentsAmount")
 */
class IssueDocumentsAmount
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", length=16)
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="name", type="text")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="coefficient", type="integer")
     * @var int
     */
    protected $coefficient;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }
}