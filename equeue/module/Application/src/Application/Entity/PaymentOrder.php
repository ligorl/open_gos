<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=PaymentOrderRepository::class)
 * @ORM\Table(name="PaymentOrder")
 */
class PaymentOrder
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="uin", type="integer")
     * @var string
     */
    protected $uin;

    /**
     * @ORM\Column(name="name", type="string")
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="electronicQueueReasonId", type="string", length=32)
     * @var string
     */
    protected $electronicQueueReasonId;

    /**
     * @ORM\Column(name="serviceReasonCatalogId", type="string", length=32)
     * @var string
     */
    protected $serviceReasonCatalogId;

    /**
     * @ORM\Column(name="reasonId", type="string", length=120)
     * @var string
     */
    protected $reasonId;

    /**
     * @ORM\Column(name="isComplex", type="integer")
     * @var string
     */
    protected $isComplex;

    /**
     * @ORM\Column(name="price", type="integer")
     * @var string
     */
    protected $price;

    /**
     * @ORM\Column(name="numberReasons", type="integer")
     * @var string
     */
    protected $numberReasons;

    public function id()
    {
        return $this->id;
    }

    public function numberReasons($val = false)
    {
        if ($val !== false) {
            $this->numberReasons = $val;
        }
        return $this->numberReasons;
    }


    public function price($val = false)
    {
        if ($val !== false) {
            $this->price = $val;
        }
        return $this->price;
    }

    public function name($val = false)
    {
        if ($val !== false) {
            $this->name = $val;
        }
        return $this->name;
    }

    public function uin($val = false)
    {
        if ($val !== false) {
            $this->uin = $val;
        }
        return $this->uin;
    }

    /**
     * Возвращает масив с параметрами модели.
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'numberReasons' => $this->numberReasons,
            'id'            => $this->id,
            'price'         => $this->price,
            'name'          => $this->name,
            'uin'           => $this->uin,
        );

    }

}