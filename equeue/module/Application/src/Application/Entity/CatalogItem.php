<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\CatalogItemRepository;

/**
 * @ORM\Entity(repositoryClass=CatalogItemRepository::class)
 * @ORM\Table(name="CatalogItem")
 */
class CatalogItem
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="code", type="string", length=255)
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="title", type="text")
     * @var string
     */
    protected $title;

    /**
     * @ORM\Column(name="catalog", type="string", length=255)
     * @var string
     */
    protected $catalog;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getCatalog()
    {
        return $this->catalog;
    }
}