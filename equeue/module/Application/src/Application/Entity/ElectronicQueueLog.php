<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

use Application\Entity\ElectronicQueueLogRepository;

/**
 * @ORM\Entity(repositoryClass=ElectronicQueueLogRepository::class)
 * @ORM\Table(name="ElectronicQueueLog")
 */
class ElectronicQueueLog
{
    const ACTION_MANAGER_ADD = 1;
    const ACTION_MANAGER_REMOVE = 2;
    const ACTION_MANAGER_REMOVE_ROOT_ATTEMPT = 3;

    const ACTION_VACATION_ADD = 11;
    const ACTION_VACATION_REMOVE = 12;

    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="manager", type="string", length=255)
     * @var string
     */
    protected $manager;

    /**
     * @ORM\Column(name="action", type="integer")
     * @var int
     */
    protected $action;

    /**
     * @ORM\Column(name="payload", type="string", length=255)
     * @var string
     */
    protected $payload;

    /**
     * @ORM\Column(name="done_at", type="datetime")
     * @var \DateTime
     */
    protected $doneAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param string $manager
     * @return ElectronicQueueLog
     */
    public function setManager($manager)
    {
        $this->manager = $manager;
        return $this;
    }

    /**
     * @return int
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param int $action
     * @return ElectronicQueueLog
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param string $payload
     * @return ElectronicQueueLog
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDoneAt()
    {
        return $this->doneAt;
    }
}