<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Doctrine\IdGenerator;

/**
 * @ORM\Entity(repositoryClass=PaymentNotificationRepository::class)
 * @ORM\Table(name="PaymentNotification")
 */
class PaymentNotification
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=IdGenerator::class)
     * @ORM\Column(name="id", type="string", length=32)
     * @var string
     */
    protected $id;

    /**
     * @ORM\Column(name="entryId", type="string", length=32)
     * @var string
     */
    protected $entryId;

    /**
     * @ORM\Column(name="organizationName", type="string", length=1024)
     * @var string
     */
    protected $organizationName;

    /**
     * @ORM\Column(name="UIN", type="string", length=20)
     * @var string
     */
    protected $uin;

    /**
     * @ORM\Column(name="KBK", type="string", length=20)
     * @var string
     */
    protected $kbk;

    /**
     * @ORM\Column(name="Summ", type="string", length=20)
     * @var string
     */
    protected $summ;

    /**
     * @ORM\Column(name="BankName", type="string", length=1024)
     * @var string
     */
    protected $bankName;

    /**
     * @ORM\Column(name="BankBIK", type="string", length=9)
     * @var string
     */
    protected $bankBIK;

    /**
     * @ORM\Column(name="CorAccount", type="string", length=21)
     * @var string
     */
    protected $corAccount;

    /**
     * @ORM\Column(name="NumEduAccount", type="string", length=21)
     * @var string
     */
    protected $numEduAccount;

    /**
     * @ORM\Column(name="CreateDate", type="string")
     * @var string
     */
    protected $createDate;

    /**
     * @ORM\Column(name="IsCanceled", type="string")
     * @var string
     */
    protected $isCanceled;

    /**
     * @ORM\Column(name="IsReturned", type="string")
     * @var string
     */
    protected $isReturned;

    public function id()
    {
        return $this->id;
    }

    public function entryId($val = false)
    {
        if ($val !== false) {
            $this->entryId = $val;
        }
        return $this->entryId;
    }

    public function organizationName($val = false)
    {
        if ($val !== false) {
            $this->organizationName = $val;
        }
        return $this->organizationName;
    }

    public function uin($val = false)
    {
        if ($val !== false) {
            $this->uin = $val;
        }
        return $this->uin;
    }

    public function kbk($val = false)
    {
        if ($val !== false) {
            $this->kbk = $val;
        }
        return $this->kbk;
    }

    public function summ($val = false)
    {
        if ($val !== false) {
            $this->summ = $val;
        }
        return $this->summ;
    }

    public function bankName($val = false)
    {
        if ($val !== false) {
            $this->bankName = $val;
        }
        return $this->bankName;
    }

    public function bankBIK($val = false)
    {
        if ($val !== false) {
            $this->bankBIK = $val;
        }
        return $this->bankBIK;
    }

    public function corAccount($val = false)
    {
        if ($val !== false) {
            $this->corAccount = $val;
        }
        return $this->corAccount;
    }

    public function numEduAccount($val = false)
    {
        if ($val !== false) {
            $this->numEduAccount = $val;
        }
        return $this->numEduAccount;
    }

    public function createDate($val = false)
    {
        if ($val !== false) {
            $this->createDate = $val;
        }
        return $this->createDate;
    }

    public function isCanceled($val = false)
    {
        if ($val !== false) {
            $this->isCanceled = $val;
        }
        return $this->isCanceled;
    }

    public function isReturned($val = false)
    {
        if ($val !== false) {
            $this->isReturned = $val;
        }
        return $this->isReturned;
    }
}