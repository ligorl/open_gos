<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class EducationalOrganizationRepository extends EntityRepository
{
    /**
     * @return EducationalOrganization[]
     */
    public function searchByFullName($queryText)
    {
        $query = $this->getEntityManager()->createQuery('SELECT o FROM Application\Entity\EducationalOrganization o WHERE o.fullName LIKE :query ORDER BY o.fullName ASC');
        $query->setParameter('query', '%' . $queryText . '%');
        return $query->getResult();
    }

    public function searchByInn($queryText)
    {
        $query = $this->getEntityManager()->createQuery('SELECT o FROM Application\Entity\EducationalOrganization o WHERE o.inn LIKE :query AND o.branch = 0 ORDER BY o.stateCode DESC, o.fullName ASC');
        $query->setParameter('query', '%' . $queryText . '%');
        return $query->getResult();
    }

    public function getOneByInn($inn)
    {
        $query = $this->getEntityManager()->createQuery('SELECT o FROM Application\Entity\EducationalOrganization o WHERE o.inn = :inn AND o.branch = 0');
        $query->setParameter('inn', $inn);
        return $query->getOneOrNullResult();
    }

    public function getOneByInnName($inn, $name)
    {
        $query = $this->getEntityManager()->createQuery('SELECT o FROM Application\Entity\EducationalOrganization o WHERE o.inn = :inn AND o.branch = 0');
        $query->setParameter('inn', $inn);
        /** @var EducationalOrganization[] $result */
        $result = $query->getResult();
        switch(count($result)) {
            case 0:
                return null;
            case 1:
                return $result[0];
            default:
                foreach ($result as $r) {
                    if ($r->getFullName() == $name) return $r;
                }
                return $result[0];
        }
    }

    public function checkExists($inn, $name)
    {
        $query = $this->getEntityManager()->createQuery('SELECT o FROM Application\Entity\EducationalOrganization o WHERE o.inn = :inn AND o.fullName = :name');
        $query->setParameter('inn', $inn);
        $query->setParameter('name', $name);
        $result = $query->getResult();
        return count($result) > 0;
    }

    public function checkExistsAndGet($inn, $name)
    {
        $query = $this->getEntityManager()->createQuery('SELECT o FROM Application\Entity\EducationalOrganization o WHERE o.inn = :inn AND o.fullName = :name');
        $query->setParameter('inn', $inn);
        $query->setParameter('name', $name);
        $result = $query->getResult();
        return count($result) > 0 ? $result[0] : null;
    }
}