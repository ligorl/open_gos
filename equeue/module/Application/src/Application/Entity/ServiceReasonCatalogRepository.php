<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class ServiceReasonCatalogRepository extends EntityRepository
{
    /**
     * @return ServiceReasonCatalog[]
     */
    public function getAll()
    {
        $query = $this->getEntityManager()->createQuery('SELECT s FROM Application\Entity\ServiceReasonCatalog s ORDER BY s.sort ASC, s.code ASC');
        return $query->getResult();
    }
    /**
     * @return ServiceReasonCatalog[]
     */
    public function getAllNotTemp()
    {
        $query = $this->getEntityManager()->createQuery('SELECT s FROM Application\Entity\ServiceReasonCatalog s WHERE s.id != :id ORDER BY s.sort ASC, s.code ASC');
        $query->setParameter('id', '9a2b174cab364241bdb0f9a90498ae55');
        return $query->getResult();
    }

    /**
     * @return ServiceReasonCatalog
     */
    public function getNewLicenseReason()
    {
        $query = $this->getEntityManager()->createQuery('SELECT s FROM Application\Entity\ServiceReasonCatalog s WHERE s.code = :code');
        $query->setParameter('code', 'rl_200_grant_of_license');
        return $query->getOneOrNullResult();
    }

    /**
     * @return ServiceReasonCatalog
     */
    public function getNewTemporaryReason()
    {
        $query = $this->getEntityManager()->createQuery('SELECT s FROM Application\Entity\ServiceReasonCatalog s WHERE s.code = :code');
        $query->setParameter('code', 'rl_230_temporary_license');
        return $query->getOneOrNullResult();
    }

    /**
     * @return ServiceReasonCatalog
     */
    public function getNewAccreditationReason()
    {
        $query = $this->getEntityManager()->createQuery('SELECT s FROM Application\Entity\ServiceReasonCatalog s WHERE s.code = :code');
        $query->setParameter('code', 'dl_120_grant_of_accreditation');
        return $query->getOneOrNullResult();
    }

    /**
     * @param string $code
     * @return ServiceReasonCatalog[]
     */
    public function getByAppealTypeCode($code)
    {
        $query = $this->getEntityManager()->createQuery('SELECT s FROM Application\Entity\ServiceReasonCatalog s WHERE s.appealTypeCode = :code');
        $query->setParameter('code', $code);
        return $query->getResult();
    }
}