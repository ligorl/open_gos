<?php

namespace Application\Entity;

use Doctrine\ORM\EntityRepository;

class IssueDocumentsAmountRepository extends EntityRepository
{
    public function getAll()
    {
        $query = $this->getEntityManager()->createQuery('SELECT a FROM Application\Entity\IssueDocumentsAmount a ORDER BY a.coefficient ASC');
        return $query->getResult();
    }
}