<?php

namespace Application\Form;

use Doctrine\ORM\EntityManager;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Mvc\I18n\Translator;
use Zend\Validator\EmailAddress;
use Zend\Validator\Regex;

class PaymentNotificationForm extends Form implements InputFilterProviderInterface
{
    protected $possibleDates = [];

    protected $data = [];

    protected $error = [];

    public function __construct($name)
    {
        parent::__construct($name, []);

        // Тип услуги
        $this->add([
            'type' => Text::class,
            'name' => 'type',
            'options' => [
                'label' => 'Тип услуги'
            ],
            'attributes' => [
                'class' => 'form-control',
                'readonly' => 'readonly',
                'id'     => 'field-type'
            ]
        ]);

        // Основание обращения
        $this->add([
            'type' => Text::class,
            'name' => 'basis_treatment',
            'options' => [
                'label' => 'Основание обращения'
            ],
            'attributes' => [
                'class' => 'form-control',
                'readonly' => 'readonly',
                'id'     => 'field-basis_treatment'

            ]
        ]);

        // Причина обращения
        $this->add([
            'type' => Textarea::class,
            'name' => 'cause',
            'options' => [
                'label' => 'Причина обращения'
            ],
            'attributes' => [
                'class'     => 'form-control',
                'readonly'  => 'readonly',
                'rows'      => '5',
                'id'     => 'field-cause'

            ]
        ]);

        // Организация (филиал)
        $this->add([
            'type' => Textarea::class,
            'name' => 'organization',
            'options' => [
                'label' => 'Организация (филиал)'
            ],
            'attributes' => [
                'class' => 'form-control',
                'rows'      => '5',
                'id'     => 'field-organization'

            ]
        ]);

        // Чекбокс СПО
        $this->add([
            'type' => Checkbox::class,
            'name' => 'checkbox_spo',
            'options' => [
                'label' => 'Укрупненных групп профессий и специальностей СПО'
            ],
            'attributes' => [
                'class' => 'checkbox',
                'id' => 'check-spo',
            ]
        ]);

        // Чекбокс ВО
        $this->add([
            'type' => Checkbox::class,
            'name' => 'checkbox_vo',
            'options' => [
                'label' => 'Укрупненных групп и направлений подготовки ВО'
            ],
            'attributes' => [
                'class' => 'checkbox',
                'id' => 'check-vo',
            ]
        ]);

        // Чекбокс Основные программы основного образования
        $this->add([
            'type' => Checkbox::class,
            'name' => 'checkbox_basis_general',
            'options' => [
                'label' => 'Основные образовательные программы основного общего образования'
            ],
            'attributes' => [
                'class' => 'checkbox',
                'id' => 'check-basis_general',
            ]
        ]);

        // Чекбокс Основные программы начального образования
        $this->add([
            'type' => Checkbox::class,
            'name' => 'checkbox_primary_general',
            'options' => [
                'label' => 'Основные образовательные программы начального общего образования'
            ],
            'attributes' => [
                'class' => 'checkbox',
                'id' => 'check-primary_general',
            ]
        ]);

        // Чекбокс Основные программы среднего образования
        $this->add([
            'type' => Checkbox::class,
            'name' => 'checkbox_secondary_general',
            'options' => [
                'label' => 'Основные образовательные программы среднего общего образования'
            ],
            'attributes' => [
                'class' => 'checkbox',
                'id' => 'check-secondary_general',
            ]
        ]);

        // Поля отображения количества кооличества специальностей
        $this->add([
            'type' => Text::class,
            'name' => 'number_specialties',
            'attributes' => [
                'value' => '1',
                'class' => 'number_specialties form-control',
                'readonly' => 'readonly',
                'id'     => 'field-number_specialties'
            ]
        ]);

        // Поля отображения количества направлений подготовок
        $this->add([
            'type' => Text::class,
            'name' => 'number_directions',
            'attributes' => [
                'value' => '1',
                'class' => 'number_directions form-control',
                'readonly' => 'readonly',
                'id'     => 'field-number_directions'
            ]
        ]);

        // Сумма
        $this->add([
            'type' => Text::class,
            'name' => 'sum',
            'options' => [
                'label' => 'Сумма'
            ],
            'attributes' => [
                'class' => 'form-control',
                'readonly' => 'readonly',
                'id'     => 'field-sum'

            ]
        ]);

        // Расчётный счёт
        $this->add([
            'type' => Text::class,
            'name' => 'checking_account',
            'options' => [
                'label' => 'Расчётный счёт №'
            ],
            'attributes' => [
                'class' => 'form-control',
                'id'     => 'field-checking_account'

            ]
        ]);

        // Банк плательщика
        $this->add([
            'type' => Text::class,
            'name' => 'name_bank',
            'options' => [
                'label' => 'Банк плательщика'
            ],
            'attributes' => [
                'class' => 'form-control',
                'id'     => 'field-name_bank'
            ]
        ]);

        // БИК банка
        $this->add([
            'type' => Text::class,
            'name' => 'BIK_bank',
            'options' => [
                'label' => 'БИК банка'
            ],
            'attributes' => [
                'class' => 'form-control',
                'id'     => 'field-BIK_bank'
            ]
        ]);

        // Корр. счёт №
        $this->add([
            'type' => Text::class,
            'name' => 'corr_account',
            'options' => [
                'label' => 'Корр. счёт №'
            ],
            'attributes' => [
                'class' => 'form-control',
                'id'     => 'field-corr_account'
            ]
        ]);
        // Корр. счёт №
        $this->add([
            'type' => Hidden::class,
            'name' => 'corr_account',
            'options' => [
                'label' => 'Корр. счёт №'
            ],
            'attributes' => [
                'class' => 'form-control',
                'id'     => 'field-corr_account'
            ]
        ]);

    }



    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        // TODO: Implement getInputFilterSpecification() method.
    }

    /**
     * Тип услуги.
     *
     * @param $type
     */
    public function setType($type)
    {
        /** @var Select $el */
        $el = $this->get('type');
        $el->setValue($type);
    }

    /**
     * основание обращения.
     *
     * @param $basisTreatment
     */
    public function setBasisTreatment($basisTreatment)
    {
        /** @var Select $el */
        $el = $this->get('basis_treatment');
        $el->setValue($basisTreatment);
    }

    /**
     *  Оргганизация (филиал).
     *
     * @param $organization
     */
    public function setOrganization($organization)
    {
        /** @var Select $el */
        $el = $this->get('organization');
        $el->setValue($organization);
    }

    /**
     * Причины обращения.
     *
     * @param $causesTreatment
     */
    public function setCausesTreatment($causesTreatment)
    {
        /** @var Select $el */
        $el = $this->get('cause');
        $el->setValue($causesTreatment);
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * Расчётный счёт №
     *
     * @param $checkingAccount
     */
    public function setCheckingAccount($checkingAccount)
    {
        /** @var Select $el */
        $el = $this->get('checking_account');
        $el->setValue($checkingAccount);
    }

    /**
     * Имя банка.
     *
     * @param $nameBank
     */
    public function setNameBank($nameBank)
    {
        /** @var Select $el */
        $el = $this->get('name_bank');
        $el->setValue($nameBank);
    }

    /**
     * БИК банка.
     *
     * @param $BIKBank
     */
    public function setBIKBank($BIKBank)
    {
        /** @var Select $el */
        $el = $this->get('BIK_bank');
        $el->setValue($BIKBank);
    }

    /**
     * Сумма.
     *
     * @param $sum
     */
    public function setSum($sum)
    {
        /** @var Select $el */
        $el = $this->get('sum');
        $el->setValue($sum);
    }

    /**
     * Корр. счёт №
     *
     * @param $corrAccount
     */
    public function setCorrAccount($corrAccount)
    {
        /** @var Select $el */
        $el = $this->get('corr_account');
        $el->setValue($corrAccount);
    }

    /**
     * Валидация данных.
     *
     * @return bool
     */
    public function isValid()
    {
        if(empty($this->data['numEduAccount']) || !is_numeric($this->data['numEduAccount']) || mb_strlen($this->data['numEduAccount']) != 20){
           array_push($this->error, 'field-checking_account');
        }

        if(empty($this->data['bankName']) || is_numeric($this->data['bankName'])){
            array_push($this->error, 'field-name_bank');
        }

        if(empty($this->data['bankBIK']) || !is_numeric($this->data['bankBIK']) || mb_strlen($this->data['bankBIK']) != 9){
            array_push($this->error, 'field-BIK_bank');
        }

        if(empty($this->data['corAccount']) || !is_numeric($this->data['corAccount']) || mb_strlen($this->data['corAccount']) != 20){
            array_push($this->error, 'field-corr_account');
        }

        if(empty($this->data['orgName'])){
            array_push($this->error, 'field-organization');
        }

        return (empty($this->error)) ? true : false;
    }

    /**
     * Ид полей в которых не коректно введены данные.
     *
     * @return array
     */
    public function getErrorIdField()
    {
        return $this->error;

    }



}