<?php

namespace Application\Form;

use Application\Entity\ApplicationReason;
use Application\Entity\CatalogItem;
use Application\Entity\EducationalOrganization;
use Application\Entity\IssueDocumentsAmount;
use Application\Entity\LicenseReasonCatalogItem;
use Application\Entity\NumberOfDocumentsCatalog;
use Application\Entity\NumberOfProgramCatalog;
use Application\Entity\ServiceReasonCatalog;
use Doctrine\ORM\EntityManager;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\MultiCheckbox;
use Zend\Form\Element\Number;
use Zend\Form\Element\Radio;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Mvc\I18n\Translator;

class AppealForm extends Form implements InputFilterProviderInterface
{
    /**
     * @var CatalogItem[]
     */
    protected $appealTypes = [];

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var EducationalOrganization
     */
    protected $org;

    public function __construct($name, EntityManager $em, Translator $translator)
    {
        parent::__construct($name, []);

        $this->em = $em;
        $this->translator = $translator;

        $this->add([
            'type' => Radio::class,
            'name' => 'service_type',
            'options' => [
                'label' => $translator->translate('service_type'),
                'label_attributes' => [
                    'class' => 'radio-inline'
                ],
                'value_options' => [
                    'license' => $translator->translate('service_type.license'),
                    'accreditation' => $translator->translate('service_type.accreditation')
                ]
            ]
        ]);

        $this->appealTypes = $em->getRepository(CatalogItem::class)->getAppealTypes();
        if (count($this->appealTypes)) {
            $value_options = [];
            foreach ($this->appealTypes as $appealType) {
                $value_options[] = [
                    'value' => $appealType->getId(),
                    'label' => $appealType->getTitle(),
                    'attributes' => [
                        //'class' => 'hidden',
                        //'disabled' => 'disabled',
                        'data-code' => $appealType->getCode(),
                        'data-hide' => 'hide'
                        //'hidden' => 'hidden'
                    ]
                ];
            }
            $this->add([
                'type' => Select::class,
                'name' => 'appeal_type',
                'options' => [
                    'label' => $translator->translate('appeal_type'),
                    'value_options' => $value_options
                ],
                'attributes' => [
                    'class' => 'form-control'
                ]
            ]);
        }

        /** @var ServiceReasonCatalog[] $serviceReasons */
        $serviceReasons = $em->getRepository(ServiceReasonCatalog::class)->getAllNotTemp();
        $exception = array('169d45490d0c41e1a55085dd33620f74');
        $value_options = [];
        foreach($serviceReasons as $reason) {
            if(!in_array($reason->getId(), $exception)){
                $value_options[] = [
                    'value' => $reason->getId(),
                    'label' => $reason->getName(),
                    'attributes' => [
                        'data-code' => $reason->getCode(),
                        'data-appeal-code' => $reason->getAppealTypeCode()
                    ]
                ];
            }
        }

        $this->add([
            'type' => Select::class,
            'name' => 'service_reason',
            'options' => [
                'label' => $translator->translate('service_reason'),
                'value_options' => $value_options
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** @var LicenseReasonCatalogItem[] $appealReasons */
        $appealReasons = $em->getRepository(LicenseReasonCatalogItem::class)->getAll();
        $value_options = [];
        foreach($appealReasons as $reason) {
            $value_options[] = [
                'value' => $reason->getId(),
                'label' => $reason->getTitle(),
                'attributes' => [
                    'data-code' => $reason->getCode(),
                    'data-reason-code' => $reason->getServiceReasonCode()
                ]
            ];
        }
        /** @var ApplicationReason[] $appealReasons */
        $appealReasons = $em->getRepository(ApplicationReason::class)->getAll();
        $ignoreIds = [
            //'691a08f0-a0d0-45a2-83e6-754df11fc697'
        ];
        foreach($appealReasons as $reason) {
            if (!in_array($reason->getId(), $ignoreIds)) {
                //ugly ignoring unneeded reasons without modyfing database entries
                $value_options[] = [
                    'value' => $reason->getId(),
                    'label' => $reason->getShortName(),
                    'attributes' => [
                        'data-code' => $reason->getCode(),
                        'data-reason-code' => $reason->getServiceReasonCode()
                    ]
                ];
            }
        }
        $this->add([
            'type' => MultiCheckbox::class,
            'name' => 'appeal_reason',
            'options' => [
                'label' => $translator->translate('appeal_reason'),
                'value_options' => $value_options
            ],
            /*'attributes' => [
                'class' => 'form-control'
            ]*/
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'oo_inn',
            'options' => [
                'label' => $translator->translate('oo_inn')
            ],
            'attributes' => [
                'class' => 'form-control',
                'autocomplete' => 'off'
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'oo_name',
            'options' => [
                'label' => $translator->translate('oo_name')
            ],
            'attributes' => [
                'class' => 'form-control',
                'autocomplete' => 'off'
            ]
        ]);

        $this->add([
            'type' => Number::class,
            'name' => 'branches_number',
            'options' => [
                'label' => $translator->translate('branches_number')
            ],
            'attributes' => [
                'class' => 'form-control',
                'min' => 0
            ]
        ]);

        $this->add([
            'type' => MultiCheckbox::class,
            'name' => 'check_branches_only',
            'options' => [
                'label' => $translator->translate('check_branches_only'),
                'value_options' => [
                    [
                        'value' => 'y',
                        'label' => $translator->translate('check_branches_only.branches_only'),
                        'attributes' => [
                            'class' => 'check-branches-only'
                        ]
                    ]
                ]
            ]
        ]);

        /** @var NumberOfProgramCatalog[] $numbersOfProgram */
        $numbersOfProgram = $em->getRepository(NumberOfProgramCatalog::class)->getAll();
        $value_options = [];
        foreach($numbersOfProgram as $number) {
            $value_options[] = [
                'value' => $number->getId(),
                'label' => $number->getName(),
                'attributes' => [
                    'data-code' => $number->getCode()
                ]
            ];
        }
        $this->add([
            'type' => Select::class,
            'name' => 'programs_number',
            'options' => [
                'label' => $translator->translate('programs_number'),
                'value_options' => $value_options
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        /** @var IssueDocumentsAmount[] $numberOfDocuments */
        $numberOfDocuments = $em->getRepository(IssueDocumentsAmount::class)->getAll();
        $value_options = [];
        foreach($numberOfDocuments as $number) {
            $value_options[] = [
                'value' => $number->getId(),
                'label' => $number->getName(),
                'attributes' => [
                    'data-code' => $number->getCode()
                ]
            ];
        }
        $this->add([
            'type' => Select::class,
            'name' => 'issue_documents_number',
            'options' => [
                'label' => $translator->translate('issue_documents_number'),
                'value_options' => $value_options
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => MultiCheckbox::class,
            'name' => 'check_needed',
            'options' => [
                'label' => $translator->translate('check_needed'),
                'value_options' => [
                    [
                        'value' => 'y',
                        'label' => $translator->translate('check_needed.unverified_documents'),
                        'attributes' => [
                            'class' => 'check-unverified-documents'
                        ]
                    ]
                ]
            ]
        ]);

        /** @var NumberOfDocumentsCatalog[] $numbersOfDocuments */
        $numbersOfDocuments = $em->getRepository(NumberOfDocumentsCatalog::class)->getAll();
        $value_options = [];
        foreach($numbersOfDocuments as $number) {
            $value_options[] = [
                'value' => $number->getId(),
                'label' => $number->getName(),
                'attributes' => [
                    'data-code' => $number->getCode()
                ]
            ];
        }
        $this->add([
            'type' => Select::class,
            'name' => 'documents_number',
            'options' => [
                'label' => $translator->translate('documents_number'),
                'value_options' => $value_options
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        //default
        $this->setData([
            'branches_number' => 0
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'appeal_reason' => [
                'required' => false
            ],
            'branches_number' => [
                'required' => false
            ],
            'check_branches_only' => [
                'required' => false
            ],
            'programs_number' => [
                'required' => false
            ],
            'check_needed' => [
                'required' => false
            ],
            'documents_number' => [
                'required' => false
            ],
            'issue_documents_number' => [
                'required' => false
            ]
        ];
    }

    public function isValid()
    {
        $this->isValid = parent::isValid();

        if (!isset($this->data['oo_inn']) || empty($this->data['oo_inn']) || !isset($this->data['oo_name']) || empty($this->data['oo_name'])) {
            //fields not set
            $this->setErrorMessage('oo_inn', 'isEmpty', 'Field cannot be empty');
            $this->setErrorMessage('oo_name', 'isEmpty', 'Field cannot be empty');
        } else {
            if (isset($this->data['service_reason'])) {
                /** @var ServiceReasonCatalog $newLicenseReason */
                $newLicenseReason = $this->em->getRepository(ServiceReasonCatalog::class)->getNewLicenseReason();
                $newTemporaryReason = $this->em->getRepository(ServiceReasonCatalog::class)->getNewTemporaryReason();
                $newAccreditationReason = $this->em->getRepository(ServiceReasonCatalog::class)->getNewAccreditationReason();
                if (
                    $this->data['service_reason'] != $newLicenseReason->getId()
                    && $this->data['service_reason'] != $newTemporaryReason->getId()
                    && $this->data['service_reason'] != $newAccreditationReason->getId()
                ) {
                    //Проверяем по данным причинам обращения, если они, то можем пропустить без существующей организации
                    $resultAppeal = array();
                    $arrayAppealReason = array('518dfabe3375431684b71d2faa5c7564','f9893ac3e72e43b59101a8270f9eaacf');
                    $appeal = false;
                    if (isset($this->data['appeal_reason']) || count($this->data['appeal_reason'])) {
                        $resultAppeal = array_diff($this->data['appeal_reason'],$arrayAppealReason);
                        if(!count($resultAppeal)){
                            $appeal = true;
                        }
                    }
                    if($appeal){
                        //check if user entered already existing organization for first registration and set its ID implicitly
                        $this->org = $this->em->getRepository(EducationalOrganization::class)->getOneByInnName($this->data['oo_inn'], $this->data['oo_name']);
                    }else{
                        //check org in base
                        $result = $this->em->getRepository(EducationalOrganization::class)->checkExistsAndGet($this->data['oo_inn'], $this->data['oo_name']);
                        if (!$result) {
                            //org not found
                            $this->setErrorMessage('oo_inn', 'valueNotFound', 'Incorrect value');
                            $this->setErrorMessage('oo_name', 'valueNotFound', 'Incorrect value');
                        } else {
                            $this->org = $result;
                        }
                    }
                } else {
                    //check if user entered already existing organization for first registration and set its ID implicitly
                    $this->org = $this->em->getRepository(EducationalOrganization::class)->getOneByInnName($this->data['oo_inn'], $this->data['oo_name']);
                }
            }
        }

        if (isset($this->data['service_reason'])) {
            /** @var ServiceReasonCatalog $serviceReason */
            $serviceReason = $this->em->getRepository(ServiceReasonCatalog::class)->find($this->data['service_reason']);
            if ($serviceReason->getAppealReasonRequired() && (!isset($this->data['appeal_reason']) || !count($this->data['appeal_reason']))) {
                $this->setErrorMessage('appeal_reason', 'isEmpty', 'Field cannot be empty');
            }
        }

        return $this->isValid;
    }

    /**
     * @return EducationalOrganization
     */
    public function getOrg()
    {
        return $this->org;
    }

    protected function setErrorMessage($field, $key, $text)
    {
        $this->isValid = false;
        $messages = $this->get($field)->getMessages();
        if (!is_array($messages) && !($messages instanceof \Traversable)) $messages = [];
        $messages[$key] = $text;
        $this->get($field)->setMessages($messages);
    }
}