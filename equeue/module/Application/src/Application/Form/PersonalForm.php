<?php

namespace Application\Form;

use Doctrine\ORM\EntityManager;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Mvc\I18n\Translator;
use Zend\Validator\EmailAddress;
use Zend\Validator\Regex;

class PersonalForm extends Form implements InputFilterProviderInterface
{
    protected $possibleDates = [];

    public function __construct($name, EntityManager $em, Translator $translator)
    {
        parent::__construct($name, []);

        $this->add([
            'type' => Text::class,
            'name' => 'date_day',
            'options' => [
                'label' => ''
            ],
            'attributes' => [
                'class' => 'form-control',
                'readonly' => 'readonly'
            ]
        ]);

        $this->add([
            'type' => Select::class,
            'name' => 'date_time',
            'options' => [
                'label' => ''
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'last_name',
            'options' => [
                'label' => $translator->translate('last_name')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'first_name',
            'options' => [
                'label' => $translator->translate('first_name')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'middle_name',
            'options' => [
                'label' => $translator->translate('middle_name')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'phone',
            'options' => [
                'label' => $translator->translate('phone')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'cell_phone',
            'options' => [
                'label' => $translator->translate('cell_phone')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'email',
            'options' => [
                'label' => $translator->translate('email')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);
    }

    public function setTimes($times)
    {
        /** @var Select $el */
        $el = $this->get('date_time');
        $el->setValueOptions($times);
    }

    public function setDates($dates)
    {
        $this->possibleDates = $dates;
    }

    public function getInputFilterSpecification()
    {
        return [
            'first_name' => [
                'required' => true
            ],
            'last_name' => [
                'required' => true
            ],
            'email' => [
                'required' => true
            ],
            'date_day' => [
                'required' => true
            ],
            'date_time' => [
                'required' => true
            ]
        ];
    }

    public function isValid()
    {
        $this->isValid = parent::isValid();

        //validate date
        if (isset($this->data['date_day'])) {
            $d = new \DateTime($this->data['date_day']);
            $d = $d->format('Y-n-j');
            if (!in_array($d, $this->possibleDates)) {
                $this->setErrorMessage('date_day', 'invalid', 'Soecified date is not in an allowed range');
            }
        }

        //validate email
        $emailValidator = new EmailAddress();
        if (!$emailValidator->isValid($this->data['email'])) {
            $this->setErrorMessage('email', 'invalid', 'Specified email is invalid');
        }

        //validate phones
        $phoneValidator = new Regex('/^[0-9+\-() ]*$/');
        if (isset($this->data['phone'])) {
            if (!$phoneValidator->isValid($this->data['phone'])) {
                $this->setErrorMessage('phone', 'invalid', 'Specified phone number is invalid');
            }
        }
        if (isset($this->data['cell_phone'])) {
            if (!$phoneValidator->isValid($this->data['cell_phone'])) {
                $this->setErrorMessage('cell_phone', 'invalid', 'Specified phone number is invalid');
            }
        }

        return $this->isValid;
    }

    protected function setErrorMessage($field, $key, $text)
    {
        $this->isValid = false;
        $messages = $this->get($field)->getMessages();
        if (!is_array($messages) && !($messages instanceof \Traversable)) $messages = [];
        $messages[$key] = $text;
        $this->get($field)->setMessages($messages);
    }
}