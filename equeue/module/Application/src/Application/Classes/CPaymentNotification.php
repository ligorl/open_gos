<?php
/**
 * Created by PhpStorm.
 * User: hagri
 * Date: 24.10.2017
 * Time: 14:27
 */

namespace Application\Classes;


use Application\Entity\ElectronicQueueEntry;
use Application\Entity\PaymentOrder;

class CPaymentNotification
{
    protected $em;

    protected $licensing =  array('0d49ba7467504c43a17600691a198f23', '4e53f68a10974c8a99b15258544b3930');

    protected $accreditation =  array('f5c12eeb141e47c5a314c235bdceec55', 'fb80f7c6613e458aaddf7dd0b1ca9b5d');

    /**
     * CMonitoringResults constructor.
     */
    function __construct(\Doctrine\ORM\EntityManager $_em)
    {
        $this->em = $_em;
    }


    /**
     * Поиск в таблице PaymentOrder по id.
     *
     * @param $paymentOrderId
     *
     * @return mixed
     */
    public function getPaymentOrderById($paymentOrderId)
    {
        $entity = current($this->em->getRepository(PaymentOrder::class)->findBy(array('id' => $paymentOrderId)));
        return $entity->toArray();


    }

    /**
     * Поиск платёжного поручения из таблици PaymentOrder соответствующей услуги.
     *
     * @param $entryId
     *
     * @return mixed
     */
    public function getPaymentOrderByEntryId($entryId)
    {
        $entry = $this->em->getRepository(ElectronicQueueEntry::class)->find($entryId);
        $idType = $entry->getElectronicQueueReasonId();
        $serviceReason = $this->em->getRepository(ElectronicQueueEntry::class)->getServiceReasonCatalog($this->em, $entry->getServiceReasonCatalogId());

        if(in_array($idType, $this->licensing)){              // Если лицензирование
            $data = $this->em->getRepository(ElectronicQueueEntry::class)->getBasisTreatment($this->em, $entry->getId(),'licensing');
        }elseif(in_array($idType, $this->accreditation)){     // Если аккредитация
            $data = $this->em->getRepository(ElectronicQueueEntry::class)->getBasisTreatment($this->em, $entry->getId(), 'accreditation');
        }else{
            var_dump('Ошибка');die;
        }

        $dataSearch = array(
            'type' => $idType,
            'serviceReason' => $serviceReason['id'],
            'reasons' => $data['reasons']
        );


        if(count($dataSearch['reasons']) > 1){
            $entity = $this->em->getRepository(PaymentOrder::class)->getEntityComplex($this->em, $dataSearch);
        }else{
            $entity = $this->em->getRepository(PaymentOrder::class)->getEntity($this->em, $dataSearch);
        }

        return $entity;
    }

    /**
     * Генерация УИН для платёжки.
     *
     * @param $code
     * @param $orgInn
     *
     * @return string
     */
    public function generationUIN($code, $orgInn)
    {
        $inn =substr($orgInn, -6);
        $month = date("m");
        $year = substr(date("Y"), -2);

        $uin = sprintf("077%s%s%s%s0106", $code, $inn, $month, $year);

        return $uin;
    }

    /**
     * Генерация КБК для платёжки.
     *
     * @param $entry
     *
     * @return string
     */
    public function generationKBK($entry)
    {
        $idType = $entry->getElectronicQueueReasonId();
        if(in_array($idType, $this->licensing)){              // Если лицензирование
            $article = '0708101';
        }elseif(in_array($idType, $this->accreditation)){     // Если аккредитация
            $article = '0720001';
        }else{
            var_dump('Ошибка');die;
        }
        $group = '0500';

        $kbk = sprintf("077108%s%s110", $article, $group);

        return $kbk;
    }

    /**
     * Извлечения uin из uin платёжки, для нахождения по id записи в таблицу в PaymentOrder.
     *
     * @param $uin
     *
     * @return mixed
     */
    public function extractIdToPaymentOrderUIN($uin)
    {
        $paymentOrderUIN = substr($uin, 3, 3); // возвращает "bcd"
        $entity = current($this->em->getRepository(PaymentOrder::class)->findBy(array('uin'=>$paymentOrderUIN)));
        return $entity->id();
    }

    /**
     * Высчитываем сколько нужно создать платёжных уведомлений.
     * Актуально при лицензировании, при выборе причины "Изменение наименования лицензиата".
     *
     * @param $electronicQueueEntry
     *
     * @return int
     */
    public function getPaymentsAmount($electronicQueueEntry)
    {
        $idType = $electronicQueueEntry->getElectronicQueueReasonId();
        $entryId = $electronicQueueEntry->getId();
        if(in_array($idType, $this->licensing)){
            $idReasons = $this->em->getRepository(ElectronicQueueEntry::class)->getIdReasonLic($this->em, $entryId,'licensing');

            // Массив Id причин к которым нужно создавать по поатёжке на филиал.
            $arrayReasonId = [
                    '3e5b5f379598446ea2833f4acd2ba01b', '0ba32b48a6384b5c95dcb630f34c930d', '518dfabe3375431684b71d2faa5c7564',
                    'c83201802b474c4393305b16b7c05375', 'f9893ac3e72e43b59101a8270f9eaacf', '33b935940d1a4c5383ee72accbdf4ee9',
                    'fd77ff8629184ae89d18858d2b4d7a50', '2b6b9edcdf394ea9b3b67c9ac8a23b7e', 'fb71a4f02e7d4f07be60fcc43fe2e9a2'
                ];

            if(array_uintersect($arrayReasonId, $idReasons, "strcasecmp")){
                $paymentsAmount = (int)$electronicQueueEntry->getFilialAmount() + 1;
                if($electronicQueueEntry->getIsBranchesOnly()) $paymentsAmount--;

                return $paymentsAmount;
            }

        }

        return 1;
    }
}