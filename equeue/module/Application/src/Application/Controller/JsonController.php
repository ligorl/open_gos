<?php

namespace Application\Controller;

use Application\Entity\EducationalOrganization;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class JsonController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     * @return JsonController
     */
    public function setEm($em)
    {
        $this->em = $em;
        return $this;
    }

    public function ooNameAction()
    {
        $query = $this->params()->fromQuery('query');
        if ($query == "") return new JsonModel([]);

        /** @var EducationalOrganization[] $results */
        $results = $this->em->getRepository(EducationalOrganization::class)->searchByFullName($query);
        $arr = [];
        foreach ($results as $r) {
            $arr[] = [
                'id' => $r->getId(),
                'name' => $r->getFullName(),
                'inn' => $r->getInn()
            ];
        }
        return new JsonModel($arr);
    }

    public function ooInnAction()
    {
        $query = $this->params()->fromQuery('query');
        if ($query == "") return new JsonModel([]);

        /** @var EducationalOrganization[] $results */
        $results = $this->em->getRepository(EducationalOrganization::class)->searchByInn($query);
        $arr = [];
        foreach ($results as $r) {
            $arr[] = [
                'id' => $r->getId(),
                'name' => $r->getFullName(),
                'inn' => $r->getInn()
            ];
        }
        return new JsonModel($arr);
    }
}