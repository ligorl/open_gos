<?php

namespace Application\Controller;

use Application\Entity\ApplicationReason;
use Application\Entity\CatalogItem;
use Application\Entity\ElectronicQueueEntry;
use Application\Entity\ElectronicQueueEntrySlot;
use Application\Entity\ElectronicQueueEntryToAccreditationReason;
use Application\Entity\ElectronicQueueEntryToLicenseReasonCatalogItem;
use Application\Entity\ElectronicQueueReason;
use Application\Entity\ElectronicQueueVacation;
use Application\Entity\Exchange;
use Application\Entity\IssueDocumentsAmount;
use Application\Entity\LicenseReasonCatalogItem;
use Application\Entity\NumberOfDocumentsCatalog;
use Application\Entity\NumberOfProgramCatalog;
use Application\Entity\ServiceReasonCatalog;
use Application\Form\AppealForm;
use Application\Form\GenericForm;
use Application\Form\PersonalForm;
use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\EntityManager;
use Zend\Config\Reader\Json;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\I18n\Translator;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

/**
 * @property Request $request
 */
class IndexController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var string
     */
    protected $mailServer = '192.168.66.240';

    protected $isTest = false;

    public function __construct()
    {
        //test server detection
        if (isset($_SERVER['SERVER_NAME']) && in_array($_SERVER['SERVER_NAME'],array('85.192.47.4','146.120.224.100'))) {
            $this->isTest = true;
        }
    }

    /**
     * @param EntityManager $em
     * @return IndexController
     */
    public function setEm($em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * @param Translator $translator
     * @return IndexController
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
        return $this;
    }

    public function indexAction()
    {
        //possible full disable
        //$this->redirect()->toRoute('equeue/maintenance');

        $stepData1 = new Container('stepData1');
        $stepData1->exchangeArray([]);
        $stepData2 = new Container('stepData2');
        $stepData2->exchangeArray([]);
        $commonData = new Container('commonData');
        $commonData->exchangeArray([]);
        $commonData['noTimeCheck'] = isset($_GET['no_min_time_check']);
        $form = new AppealForm('appeal-form', $this->em, $this->translator);
        if ($this->request->isPost()) {
            $form->setData($this->request->getPost());
            if ($form->isValid()) {
                $stepData1 = new Container('stepData1');
                $stepData1->exchangeArray($form->getData());
                $org = $form->getOrg();
                $stepData1['org_id'] = $org ? $org->getId() : '';
                return $this->redirect()->toRoute('equeue/step2');
            }
        }

        return new ViewModel([
            'isTest' => $this->isTest,
            'genericForm' => $form
        ]);
    }

    public function checkStep1Action()
    {
        $result = [
            'hasErrors' => false
        ];
        $form = new AppealForm('appeal-form', $this->em, $this->translator);
        if ($this->request->isPost()) {
            $form->setData($this->request->getPost());
            if ($form->isValid()) {
                $stepData1 = new Container('stepData1');
                $stepData1->exchangeArray($form->getData());
                $org = $form->getOrg();
                $stepData1['org_id'] = $org ? $org->getId() : '';
                return $this->redirect()->toRoute('equeue/step2');
            } else {
                $result['errors'] = $form->getMessages();
                $result['hasErrors'] = true;
            }
        }

        return new JsonModel($result);
    }

    public function step2Action()
    {
        $stepData1 = new Container('stepData1');
        $stepData2 = new Container('stepData2');
        $commonData = new Container('commonData');
        $queueReasonId = null;
        $length = 0;
        $room = 0;
        $dateFree = [];
        $timesPossible = [];

        /*$fieldsToCheck = ['appeal_type', 'branches_number'];
        foreach ($fieldsToCheck as $field) {
            if(empty($stepData1[$field])) $this->redirect()->toRoute('equeue');
        }*/

        /** @var CatalogItem $appealType */
        $appealType = $this->em->getRepository(CatalogItem::class)->find($stepData1['appeal_type']);
        if ($appealType != null) {
            /** @var ElectronicQueueReason $queueReason */
            $queueReason = $this->em->getRepository(ElectronicQueueReason::class)->getByCode($appealType->getCode());
            if ($queueReason) {
                $room = $queueReason->getRoomNumber();
                $length = 0;
                $branchesNumber = (int)$stepData1['branches_number'] + 1;
                if ($branchesNumber > 0 && isset($stepData1['check_branches_only']) && $stepData1['check_branches_only'] == 'y') {
                    $branchesNumber--;
                }
                $queueReasonId = $queueReason->getId();

                $length += $queueReason->getBaseReceptionTime() * $branchesNumber;
                if (!empty($stepData1['programs_number'])) {
                    /** @var NumberOfProgramCatalog $programsNumber */
                    $programsNumber = $this->em->getRepository(NumberOfProgramCatalog::class)->find($stepData1['programs_number']);
                    if ($programsNumber) $length += $programsNumber->getCoefficient() * $queueReason->getBaseReceptionTime() * $branchesNumber;
                }
                if (!empty($stepData1['documents_number'])) {
                    /** @var NumberOfDocumentsCatalog $documentsNumber */
                    $documentsNumber = $this->em->getRepository(NumberOfDocumentsCatalog::class)->find($stepData1['documents_number']);
                    if ($documentsNumber) $length += $documentsNumber->getCoefficient() * $queueReason->getBaseReceptionTime();
                }
                if (!empty($stepData1['issue_documents_number'])) {
                    /** @var IssueDocumentsAmount $documentsNumber */
                    $documentsNumber = $this->em->getRepository(IssueDocumentsAmount::class)->find($stepData1['issue_documents_number']);
                    if ($documentsNumber) $length += ($documentsNumber->getCoefficient() - 1) * $queueReason->getBaseReceptionTime();
                }
                if ($length > 180) $length = 180;
                $stepData1['audienceLength'] = $length;

                $daysEnum = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'];
                $days = explode(';', str_replace(array_values($daysEnum), array_keys($daysEnum), $queueReason->getReceptionDaysRaw()));
                $dateFree = [];
                $dateToday = new \DateTime();
                $dateMax = new \DateTime();
                if ($commonData['noTimeCheck']) {
                    $dateMax->modify('+3 months');
                } else {
                    $dateMax->modify('+1 month');
                }
                $dateMax->setTime(23, 59, 59);
                $timeStep = '+' . $queueReason->getBaseReceptionTime() . ' minutes';
                $timeLength = '+' . $length . ' minutes';

                $timesPossible = $this->getPossibleTimes($queueReason, $length);

                /** @var ElectronicQueueEntry[] $existingEntries */
                $existingEntries = $this->em->getRepository(ElectronicQueueEntry::class)->getForDates($dateToday, $dateMax, $queueReason->getId());

                /** @var ElectronicQueueVacation[] $vacations */
                $vacations = $this->em->getRepository(ElectronicQueueVacation::class)->getAll();

                $dateTmp = clone $dateToday;
                $dateTmp->setTime(0, 0, 0);
                $dateMin = clone $dateToday;
                if ($commonData['noTimeCheck']) {
                    $dateMin->modify('-15 minutes');
                } else {
                    $dateMin->modify('+15 minutes');
                }
                while ($dateTmp <= $dateMax) {
                    $day = $dateTmp->format('w');
                    $closed = false;
                    foreach ($vacations as $v) {
                        if ($v->getReasonId() == $queueReasonId && $dateTmp >= $v->getFrom() && $dateTmp <= $v->getTo()) {
                            $closed = true;
                        }
                    }
                    if (in_array($day, $days) && !$closed) {
                        $date = $dateTmp->format('Y-n-j');
                        $dateFree[$date] = [];

                        $qrStartTime = clone $dateTmp;
                        $qrStartTime->setTime(
                            $queueReason->getStartReceptionTime()->format('H'),
                            $queueReason->getStartReceptionTime()->format('i')
                        );
                        $qrEndTime = clone $dateTmp;
                        $qrEndTime->setTime(
                            $queueReason->getEndReceptionTime()->format('H'),
                            $queueReason->getEndReceptionTime()->format('i')
                        );
                        $qrBreakStartTime = clone $dateTmp;
                        $qrBreakStartTime->setTime(
                            $queueReason->getBreakStartTime()->format('H'),
                            $queueReason->getBreakStartTime()->format('i')
                        );
                        $qrBreakEndTime = clone $dateTmp;
                        $qrBreakEndTime->setTime(
                            $queueReason->getBreakEndTime()->format('H'),
                            $queueReason->getBreakEndTime()->format('i')
                        );

                        $timeTmpStart = clone $qrStartTime;
                        while ($timeTmpStart < $qrEndTime) {
                            if ($timeTmpStart > $dateMin && ($timeTmpStart < $qrBreakStartTime || $timeTmpStart >= $qrBreakEndTime)) {
                                $timeTmpEnd = clone $timeTmpStart;
                                $timeTmpEnd->modify($timeLength);
                                if (
                                    ($timeTmpStart < $qrBreakStartTime && $timeTmpEnd <= $qrBreakStartTime)
                                    || ($timeTmpStart >= $qrBreakEndTime && $timeTmpEnd > $qrBreakEndTime && $timeTmpEnd <= $qrEndTime)
                                ) {
                                    $occupied = false;
                                    foreach ($existingEntries as $ee) {
                                        if ($ee->overlapsTime($timeTmpStart, $timeTmpEnd)) {
                                            $occupied = true;
                                            break;
                                        }
                                    }
                                    $slot = $this->em->getRepository(ElectronicQueueEntrySlot::class)->getBySlot( $timeTmpStart,$timeTmpEnd );
                                    if ($slot) {
                                        $occupied = true;
                                    }
                                    if (!$occupied) {
                                        $dateFree[$date][] = $timeTmpStart->format('H:i');
                                    }
                                }
                            }
                            $timeTmpStart->modify($timeStep);
                        }

                        if (count($dateFree[$date]) == 0) unset($dateFree[$date]);
                    }
                    $dateTmp->modify('+1 day');
                }
            }
        }

        $commonData['datesPossible'] = array_keys($dateFree);

        $form = new PersonalForm('personal-form', $this->em, $this->translator);
        $form->setTimes($timesPossible);

        $this->layout('layout/empty');

        return new ViewModel([
            'audienceLength' => $length,
            'room' => $room,
            'form' => $form,
            'dates' => $dateFree
        ]);
    }

    public function checkStep2Action()
    {
        $result = [
            'hasErrors' => false
        ];

        $stepData1 = new Container('stepData1');
        $stepData2 = new Container('stepData2');
        $commonData = new Container('commonData');

        /** @var CatalogItem $appealType */
        $appealType = $this->em->getRepository(CatalogItem::class)->find($stepData1['appeal_type']);
        if ($appealType != null) {
            /** @var ElectronicQueueReason $queueReason */
            $queueReason = $this->em->getRepository(ElectronicQueueReason::class)->getByCode($appealType->getCode());
            if ($queueReason) {
                $room = $queueReason->getRoomNumber();
                $length = $queueReason->getBaseReceptionTime();
                $queueReasonId = $queueReason->getId();

                $timesPossible = $this->getPossibleTimes($queueReason, $length);
                $form = new PersonalForm('personal-form', $this->em, $this->translator);
                $form->setTimes($timesPossible);
                $form->setDates($commonData['datesPossible']);
                if ($this->request->isPost()) {
                    $form->setData($this->request->getPost());
                    if ($form->isValid()) {
                        $stepData2->exchangeArray($form->getData());
                        $stepData2['queue_reason_id'] = $queueReasonId;
                        //$stepData2['audienceLength'] = $length;
                        $stepData2['audienceRoom'] = $room;
                        return $this->redirect()->toRoute('equeue/step3');
                    } else {
                        $result['errors'] = $form->getMessages();
                        //something gone very wrong with ZF default messages' encodings and broke JSON output
                        foreach($result['errors'] as $el => $errors) {
                            if (isset($errors['isEmpty'])) {
                                $result['errors'][$el]['isEmpty'] = 'Field cannot be empty';
                            }
                        }
                        //---
                        $result['hasErrors'] = true;
                    }
                }
            }
        }

        return new JsonModel($result);
    }

    public function step3Action()
    {
        $stepData1 = new Container('stepData1');
        $stepData2 = new Container('stepData2');

        /*$fieldsToCheck = ['appeal_type', 'branches_number'];
        foreach ($fieldsToCheck as $field) {
            if(empty($stepData1[$field])) $this->redirect()->toRoute('equeue');
        }*/

        $receptionTime = new \DateTime($stepData2['date_day'] . ' ' . $stepData2['date_time'] . ':00');
        $receptionTimeEnd = clone $receptionTime;
        $receptionTimeEnd->modify('+' . $stepData1['audienceLength'] . ' minutes');

        /** @var CatalogItem $appealType */
        $appealType = $this->em->getRepository(CatalogItem::class)->find($stepData1['appeal_type']);

        /** @var ServiceReasonCatalog $serviceReason */
        $serviceReason = $this->em->getRepository(ServiceReasonCatalog::class)->find($stepData1['service_reason']);
        $this->em->beginTransaction();

        $entry = new ElectronicQueueEntry();
        $entry->setCreatedDateTime(new \DateTime())
            ->setUpdatedDateTime(new \DateTime())
            ->setElectronicQueueReasonId($stepData2['queue_reason_id'])
            ->setEMail($stepData2['email'])
            ->setEndReceptionTime($receptionTimeEnd)
            ->setFilialAmount($stepData1['branches_number'])
            ->setIsBranchesOnly((empty($stepData1['check_branches_only'])) ? '0' : '1')
            ->setFirstName($stepData2['first_name'])
            ->setIssueDocumentsAmountId($stepData1['issue_documents_number'])
            ->setLastName($stepData2['last_name'])
            ->setLicensingProcedureId($stepData1['appeal_type'])
            ->setMiddleName($stepData2['middle_name'])
            ->setMobilePhoneNumber($stepData2['cell_phone'])
            ->setNumberOfDocumentsId($stepData1['documents_number'])
            ->setNumberOfProgramCatalogId($stepData1['programs_number'])
            ->setOrganizationId($stepData1['org_id'])
            ->setOrganizationName($stepData1['oo_name'])
            ->setOrganizationInn($stepData1['oo_inn'])
            ->setPhoneNumber($stepData2['phone'])
            ->setSecretKey('')
            ->setServiceReasonCatalogId($stepData1['service_reason'])
            ->setSevenDaysNotificationSent(false)
            ->setStartReceptionTime($receptionTime)
            ->setStatus("unapproved");
        $this->em->persist($entry);
        $this->em->flush();

        $ex = new Exchange();
        $ex->setTimestamp(new \DateTime())
            ->setSaveType('insert')
            ->setTableName('ElectronicQueueEntry')
            ->setObjectId($entry->getId())
            ->setSource('vuzi');
        $this->em->persist($ex);

        if (is_array($stepData1['appeal_reason'])) {
            foreach ($stepData1['appeal_reason'] as $reasonId) {
                if ($appealType->getCode() == 'reception_license') {
                    $reason = new ElectronicQueueEntryToLicenseReasonCatalogItem();
                    $reason->setElectronicQueueEntryId($entry->getId());
                    $reason->setLicenseReasonCatalogItemId($reasonId);
                    $this->em->persist($reason);
                    $this->em->flush();

                    $ex = new Exchange();
                    $ex->setTimestamp(new \DateTime())
                        ->setSaveType('insert')
                        ->setTableName('ElectronicQueueEntryToLicenseReasonCatalogItem')
                        ->setObjectId($reason->getId())
                        ->setSource('vuzi');
                    $this->em->persist($ex);
                } elseif ($appealType->getCode() == 'reception_accreditation') {
                    $reason = new ElectronicQueueEntryToAccreditationReason();
                    $reason->setElectronicQueueEntryId($entry->getId());
                    $reason->setAccredApplicationReasons($reasonId);
                    $this->em->persist($reason);
                    $this->em->flush();

                    $ex = new Exchange();
                    $ex->setTimestamp(new \DateTime())
                        ->setSaveType('insert')
                        ->setTableName('ElectronicQueueEntryToAccreditationReason')
                        ->setObjectId($reason->getId())
                        ->setSource('vuzi');
                    $this->em->persist($ex);
                }
            }
        }

        //fill time slots to prevent concurrent records

        /** @var ElectronicQueueReason $queueReason */
        $queueReason = $this->em->getRepository(ElectronicQueueReason::class)->getByCode($appealType->getCode());
        if ($queueReason) {
            $length = $queueReason->getBaseReceptionTime();
            $qrStartTime = clone $receptionTime;
            $qrStartTime->setTime(
                $queueReason->getStartReceptionTime()->format('H'),
                $queueReason->getStartReceptionTime()->format('i')
            );
            $qrEndTime = clone $receptionTime;
            $qrEndTime->setTime(
                $queueReason->getEndReceptionTime()->format('H'),
                $queueReason->getEndReceptionTime()->format('i')
            );
            $qrBreakStartTime = clone $receptionTime;
            $qrBreakStartTime->setTime(
                $queueReason->getBreakStartTime()->format('H'),
                $queueReason->getBreakStartTime()->format('i')
            );
            $qrBreakEndTime = clone $receptionTime;
            $qrBreakEndTime->setTime(
                $queueReason->getBreakEndTime()->format('H'),
                $queueReason->getBreakEndTime()->format('i')
            );

            $timeTmpStart = clone $qrStartTime;
            $additionalAppealId = null;
            if ($appealType->getId() == '08c58690c64c4d00af201b33583e0adf') {
                $additionalAppealId = 'cb6c9be69d404d1e8678355bbd6501ab';
            } elseif ($appealType->getId() == 'cb6c9be69d404d1e8678355bbd6501ab') {
                $additionalAppealId = '08c58690c64c4d00af201b33583e0adf';
            }
            while ($timeTmpStart < $qrEndTime) {
                if ($timeTmpStart < $qrBreakStartTime || $timeTmpStart >= $qrBreakEndTime) {
                    $timeTmpEnd = clone $timeTmpStart;
                    $timeTmpEnd->modify('+' . $length . ' minutes');
                    if ($entry->overlapsTime($timeTmpStart, $timeTmpEnd)) {
                        $slot = new ElectronicQueueEntrySlot();
                        $slot->setEntryId($entry->getId());
                        $slot->setAppealTypeId($appealType->getId());
                        $slot->setSlot($timeTmpStart);
                        $this->em->persist($slot);
                        if ($additionalAppealId) {
                            $slot = new ElectronicQueueEntrySlot();
                            $slot->setEntryId($entry->getId());
                            $slot->setAppealTypeId($additionalAppealId);
                            $slot->setSlot($timeTmpStart);
                            $this->em->persist($slot);
                        }
                    }
                }
                $timeTmpStart->modify('+' . $length . ' minutes');
            }
        }

        $appealReasons = [];
        if (is_array($stepData1['appeal_reason'])) {
            foreach ($stepData1['appeal_reason'] as $reasonId) {
                $appealReasonObj = $this->em->getRepository(LicenseReasonCatalogItem::class)->find($reasonId);
                if ($appealReasonObj) {
                    $appealReasons[] = $appealReasonObj->getTitle();
                } else {
                    $appealReasonObj = $this->em->getRepository(ApplicationReason::class)->find($reasonId);
                    if ($appealReasonObj) $appealReasons[] = $appealReasonObj->getShortName();
                }
            }
        }

        $commitFailed = false;

        $paymentNotification = '';
        if(!in_array($stepData1['service_reason'], array('9a5eced13c6e40bdb7b6fa2bd447524f', 'c4f78a563d8640ecaa48d8408744e66d'))){
            $paymentNotification = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/payment-notification/' . $entry->getId();
        }

        try {
            $this->em->flush();
            $this->em->commit();

            $link = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/confirm/' . $entry->getId();
            $cancelLink = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/cancel/' . $entry->getId();

            $body = $this->renderMailBody([
                'link' => $link,
                'cancel_link' => $cancelLink,
                'payment_notification' => $paymentNotification,
                'date_day' => $receptionTime->format('d.m.y'),
                'date_time' => $receptionTime->format('H:i'),
                'date_register' => (new \DateTime())->format('d.m.y'),
                'service_type' => $appealType->getTitle(),
                'appeal_reasons' => $appealReasons,
                'service_reason' => $serviceReason->getName(),
                'appeal_type' => $appealType->getTitle(),
                'stepData1' => $stepData1->getArrayCopy(),
                'stepData2' => $stepData2->getArrayCopy()
            ]);
            $message = \Swift_Message::newInstance()
                ->setFrom('noreply@eq.obrnadzor.gov.ru')
                ->setSubject('Запись в электронную очередь')
                ->setTo($stepData2['email'])
                ->setBody($body, 'text/html');
            $transport = $this->isTest ? \Swift_MailTransport::newInstance() : \Swift_SmtpTransport::newInstance($this->mailServer);
            $mailer = \Swift_Mailer::newInstance($transport);
            $mailer->send($message);
        } catch (\Exception $e) {
            $commitFailed = true;
            $this->em->rollback();
        }

        return new ViewModel([
            'commit_failed' => $commitFailed,
            'date_day' => $stepData2['date_day'],
            'date_time' => $stepData2['date_time'],
            'room' => $stepData2['audienceRoom'],
            'audience_length' => $stepData1['audienceLength'],
            'service_type' => $appealType->getTitle(),
            'appeal_reasons' => $appealReasons,
            'appeal_date' => (new \DateTime())->format('d.m.y'),
            'link' => $paymentNotification
        ]);
    }

    public function confirmAction()
    {
        $result = 'failed';
        $entryId = $this->params()->fromRoute('entryId');
        /** @var ElectronicQueueEntry $entry */
        $entry = $this->em->getRepository(ElectronicQueueEntry::class)->find($entryId);
        if ($entry && $entry->getStatus() == 'confirmed') {
            $result = 'already_confirmed';
        } elseif ($entry && $entry->getStatus() == 'unapproved') {
            $entry->setStatus('confirmed');
            $entry->setConfirmedDateTime(new \DateTime());

            $ex = new Exchange();
            $ex->setTimestamp(new \DateTime())
                ->setSaveType('update')
                ->setTableName('ElectronicQueueEntry')
                ->setObjectId($entry->getId())
                ->setSource('vuzi');
            $this->em->persist($ex);
            try {
                $this->em->flush();
                $result = 'confirmed';

                //confirm notification
                /** @var ElectronicQueueReason $queueReason */
                $queueReason = $this->em->getRepository(ElectronicQueueReason::class)->find($entry->getElectronicQueueReasonId());

                $appealReasons = [];
                foreach ($this->em->getRepository(ElectronicQueueEntryToLicenseReasonCatalogItem::class)->getForEntry($entryId) as $r) {
                    $appealReasonObj = $this->em->getRepository(LicenseReasonCatalogItem::class)->find($r->getLicenseReasonCatalogItemId());
                    if ($appealReasonObj) $appealReasons[] = $appealReasonObj->getTitle();
                }
                foreach ($this->em->getRepository(ElectronicQueueEntryToAccreditationReason::class)->getForEntry($entryId) as $r) {
                    $appealReasonObj = $this->em->getRepository(ApplicationReason::class)->find($r->getAccredApplicationReasons());
                    if ($appealReasonObj) $appealReasons[] = $appealReasonObj->getShortName();
                }
                /** @var ServiceReasonCatalog $serviceReason */
                $serviceReason = $this->em->getRepository(ServiceReasonCatalog::class)->find($entry->getServiceReasonCatalogId());

                //delivery entry
                $length = ($entry->getEndReceptionTime()->getTimestamp() - $entry->getStartReceptionTime()->getTimestamp()) / 60;

                $cancelLink = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/cancel/' . $entry->getId();
                $body = $this->renderMailBody([
                    'cancel_link' => $cancelLink,
                    'date_day' => $entry->getStartReceptionTime()->format('d.m.y'),
                    'date_time' => $entry->getStartReceptionTime()->format('H:i'),
                    'date_register' => (new \DateTime())->format('d.m.y'),
                    'audienceRoom' => $queueReason->getRoomNumber(),
                    'audienceLength' => $length,
                    'appeal_type' => $queueReason->getName()
                ], 'mail/body_confirm');
                $message = \Swift_Message::newInstance()
                    ->setFrom('noreply@eq.obrnadzor.gov.ru')
                    ->setSubject('Подтверждение записи в электронную очередь')
                    ->setTo($entry->getEMail())
                    ->setBody($body, 'text/html');
                $transport = $this->isTest ? \Swift_MailTransport::newInstance() : \Swift_SmtpTransport::newInstance($this->mailServer);
                $mailer = \Swift_Mailer::newInstance($transport);
                $mailer->send($message);

                $notifyMails = $queueReason->getEqNotifyMails();
                if (count($notifyMails)) {
                    $body = $this->renderMailBody([
                        'date_day' => $entry->getStartReceptionTime()->format('d.m.y'),
                        'date_time' => $entry->getStartReceptionTime()->format('H:i'),
                        'date_register' => $entry->getCreatedDateTime()->format('d.m.y'),
                        'audienceRoom' => $queueReason->getRoomNumber(),
                        'audienceLength' => $length,
                        'name' => $entry->getLastName() . ' ' . $entry->getFirstName() . ' ' . $entry->getMiddleName(),
                        'org' => $entry->getOrganizationName(),
                        'procedure' => $queueReason->getName(),
                        'appeal_reasons' => $appealReasons,
                        'service_reason' => $serviceReason->getName()
                    ], 'mail/notify_create');
                    $message = \Swift_Message::newInstance()
                        ->setFrom('noreply@eq.obrnadzor.gov.ru')
                        ->setSubject('Запись в электронную очередь')
                        ->setTo($notifyMails)
                        ->setBody($body, 'text/html');
                    $mailer->send($message);
                }
            } catch (\Exception $e) {

            }
        }

        return new ViewModel([
            'result' => $result
        ]);
    }

    public function cancelAction()
    {
        $result = false;
        $entryId = $this->params()->fromRoute('entryId');
        /** @var ElectronicQueueEntry $entry */
        $entry = $this->em->getRepository(ElectronicQueueEntry::class)->find($entryId);
        if ($entry && ($entry->getStatus() == 'unapproved' || $entry->getStatus() == 'confirmed')) {
            $oldStatus = $entry->getStatus();
            //$entry->setStatus('cancelled');
            $this->em->getRepository(ElectronicQueueEntry::class)->deleteWithSlots([$entryId]);

            $ex = new Exchange();
            $ex->setTimestamp(new \DateTime())
                ->setSaveType('update')
                ->setTableName('ElectronicQueueEntry')
                ->setObjectId($entry->getId())
                ->setSource('vuzi');
            $this->em->persist($ex);
            try {
                $this->em->flush();
                $result = true;

                if ($oldStatus == 'confirmed') {
                    //notify cancel
                    /** @var ElectronicQueueReason $queueReason */
                    $queueReason = $this->em->getRepository(ElectronicQueueReason::class)
                        ->find($entry->getElectronicQueueReasonId());
                    $notifyMails = $queueReason->getEqNotifyMails();
                    if (count($notifyMails)) {
                        $body = 'Отменена запись на ' . $entry->getStartReceptionTime()->format('d.m.Y H:i') . '<br/>'
                            . 'Процедура: ' . $queueReason->getName() . '<br/>'
                            . $entry->getLastName() . ' ' . $entry->getFirstName() . ' ' . $entry->getMiddleName() . '<br/>'
                            . $entry->getOrganizationName() . '<br/>';
                        $message = \Swift_Message::newInstance()
                            ->setFrom('noreply@eq.obrnadzor.gov.ru')
                            ->setSubject('Отменена запись в электронную очередь')
                            ->setTo($notifyMails)
                            ->setBody($body, 'text/html');
                        $transport = $this->isTest ? \Swift_MailTransport::newInstance() : \Swift_SmtpTransport::newInstance($this->mailServer);
                        $mailer = \Swift_Mailer::newInstance($transport);
                        $mailer->send($message);
                    }
                }
            } catch (\Exception $e) {

            }
        }

        return new ViewModel([
            'result' => $result
        ]);
    }

    public function checkEntriesAction()
    {
        /** @var ElectronicQueueEntry[] $queries */
        $queries = $this->em->getRepository(ElectronicQueueEntry::class)->getExpiredForConfirm();
        if (count($queries)) {
            foreach ($queries as $q) {
                $q->setStatus('cancelled');
                $slots = $this->em->getRepository(ElectronicQueueEntrySlot::class)->getForEntry($q->getId());
                if (count($slots)) {
                    foreach ($slots as $s) {
                        $this->em->remove($s);
                    }
                }
            }
            $this->em->flush();
        }
    }

    public function killAction()
    {
        $removed = false;
        $ids = [];
        $removedIds = [];
        $tmp = $this->params()->fromPost('ids');
        if ($tmp) {
            $tmp = explode("\n", $tmp);
            foreach($tmp as $t) {
                $t = trim($t);
                if ($t != '') $ids[] = $t;
            }
        }
        if (count($ids)) {
            foreach($ids as $id) {
                /** @var ElectronicQueueEntry $entry */
                $entry = $this->em->getRepository(ElectronicQueueEntry::class)->find($id);
                if ($entry) {
                    /** @var ElectronicQueueReason $queueReason */
                    $queueReason = $this->em->getRepository(ElectronicQueueReason::class)->find($entry->getElectronicQueueReasonId());
                    $removed = true;
                    //$this->em->remove($entry);
                    $this->em->getRepository(ElectronicQueueEntry::class)->deleteWithSlots([$id]);
                    $ex = new Exchange();
                    $ex->setTimestamp(new \DateTime())
                        ->setSaveType('update')
                        ->setTableName('ElectronicQueueEntry')
                        ->setObjectId($id)
                        ->setSource('vuzi');
                    $this->em->persist($ex);

                    $removedIds[] = $id;

                    //kill notification
                    $notifyMails = $queueReason->getEqNotifyMails();
                    if (count($notifyMails)) {
                        $body = 'Отменена запись на ' . $entry->getStartReceptionTime()->format('d.m.Y H:i') . '<br/>'
                            . 'Процедура: ' . $queueReason->getName() . '<br/>'
                            . $entry->getLastName() . ' ' . $entry->getFirstName() . ' ' . $entry->getMiddleName() . '<br/>'
                            . $entry->getOrganizationName() . '<br/>';
                        $message = \Swift_Message::newInstance()
                            ->setFrom('noreply@eq.obrnadzor.gov.ru')
                            ->setSubject('Отменена запись в электронную очередь')
                            ->setTo($notifyMails)
                            ->setBody($body, 'text/html');
                        $transport = $this->isTest ? \Swift_MailTransport::newInstance() : \Swift_SmtpTransport::newInstance($this->mailServer);
                        $mailer = \Swift_Mailer::newInstance($transport);
                        $mailer->send($message);
                    }
                }
            }

            $this->em->flush();
        }

        $viewModel = new ViewModel([
            'removed' => $removed,
            'ids' => $removedIds
        ]);
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function maintenanceAction() {
        return new ViewModel([]);
    }

    /**
     * @param ElectronicQueueReason $queueReason
     * @param int $jobLength
     * @return string[]
     */
    private function getPossibleTimes($queueReason, $jobLength)
    {
        $result = [];
        $timeLength = '+' . $jobLength . ' minutes';
        $timeStep = '+' . $queueReason->getBaseReceptionTime() . ' minutes';

        $timeTmpStart = clone $queueReason->getStartReceptionTime();
        while ($timeTmpStart <= $queueReason->getEndReceptionTime()) {
            if ($timeTmpStart < $queueReason->getBreakStartTime() || $timeTmpStart >= $queueReason->getBreakEndTime()) {
                $timeTmpEnd = clone $timeTmpStart;
                $timeTmpEnd->modify($timeLength);
                if (
                    ($timeTmpStart < $queueReason->getBreakStartTime() && $timeTmpEnd <= $queueReason->getBreakStartTime())
                    || ($timeTmpStart >= $queueReason->getBreakEndTime() && $timeTmpEnd > $queueReason->getBreakEndTime() && $timeTmpEnd <= $queueReason->getEndReceptionTime())
                ) {
                    $t = $timeTmpStart->format('H:i');
                    $result[$t] = $t;
                }
            }
            $timeTmpStart->modify($timeStep);
        }

        return $result;
    }

    private function renderMailBody($params, $tpl = 'mail/body')
    {
        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\RendererInterface');
        $content = new \Zend\View\Model\ViewModel($params);
        $content->setTemplate($tpl);
        return $renderer->render($content);
    }
}
