<?php

namespace Application\Controller;

use Application\Classes\CPaymentNotification;
use Application\Entity\EducationalOrganization;
use Application\Entity\ElectronicQueueEntry;
use Application\Entity\isgaApplicationReasonsEntity;
use Application\Entity\PaymentNotification;
use Application\Entity\PaymentOrder;
use Application\Form\PaymentNotificationForm;
use Eo\Model\Templater;
use Eo\Model\Export;
use Swift_Attachment;
use Zend\Mvc\Controller\AbstractActionController;
use Doctrine\ORM\EntityManager;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;


class PaymentNotificationController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $mailServer = '192.168.66.240';

    protected $isTest = false;

    protected $licensing =  array('0d49ba7467504c43a17600691a198f23', '4e53f68a10974c8a99b15258544b3930');

    protected $accreditation =  array('f5c12eeb141e47c5a314c235bdceec55', 'fb80f7c6613e458aaddf7dd0b1ca9b5d');

    /**
     * @param EntityManager $em
     *
     * @return JsonController
     */
    public function setEm($em)
    {
        $this->em = $em;
        return $this;
    }

    public function __construct()
    {
        //test server detection
        if (isset($_SERVER['SERVER_NAME']) && in_array($_SERVER['SERVER_NAME'],array('85.192.47.4','146.120.224.100'))) {
            $this->isTest = true;
        }
    }

    /**
     * Открываятся форма с калькулятором гос пошлины где создаётся платёжка.
     *
     * @return \Zend\Http\Response|ViewModel
     */
    public function createAction()
    {
        $entryId = $this->params()->fromRoute('entryId');
        $entry = $this->em->getRepository(ElectronicQueueEntry::class)->find($entryId);
        if(empty($entry)){
            return $this->errorMessage('Нет такой записи');
        }

        // Если платёжка создана редирект на её редактировнание.
        $paymentNotification = current($this->em->getRepository(PaymentNotification::class)->findBy(array('entryId' => $entryId)));
        if(!empty($paymentNotification)){
            return $this->errorMessage('Платежное поручение уже создано');
        }

        $dateObj = $entry->getStartReceptionTime();
        $date = $dateObj->format('Y-m-d');
        if(strtotime($date) < strtotime(date('Y-m-d'))){
            return $this->errorMessage('Дата приёма уже наступила, создание не доступно');
        }

        $form = new PaymentNotificationForm('personal-form');
        $idType = $entry->getElectronicQueueReasonId();

        $type = $this->em->getRepository(ElectronicQueueEntry::class)->getServiceType($this->em, $idType, 'serviceType');
        $serviceReason = $this->em->getRepository(ElectronicQueueEntry::class)->getServiceReasonCatalog($this->em, $entry->getServiceReasonCatalogId());

        if(in_array($idType, $this->licensing)){              // Если лицензирование
            $data = $this->em->getRepository(ElectronicQueueEntry::class)->getBasisTreatment($this->em, $entry->getId(),'licensing');
        }elseif(in_array($idType, $this->accreditation)){     // Если аккредитация
            $data = $this->em->getRepository(ElectronicQueueEntry::class)->getBasisTreatment($this->em, $entry->getId(), 'accreditation');
        }else{
            return $this->errorMessage('Не коректные данные');
        }

        // Нужно ли показывать дополнительный блок
        $additionalBlock = $data['additionalBlock'];

        $CPaymentNotification = new CPaymentNotification($this->em);

        // Количество создаваемых платёжек.
        $paymentsAmount = $CPaymentNotification->getPaymentsAmount($entry);

        $countOrg = $entry->getFilialAmount() + 1;
        if($entry->getIsBranchesOnly()) $countOrg--;

        $form->setCausesTreatment($data['listCauses']);

        $form->setOrganization($entry->getOrganizationName());
        $form->setType($type);
        $form->setBasisTreatment($serviceReason['name']);

        return new ViewModel([
            'entryId' => $entryId,
            'form' => $form,
            'additionalBlock' => $additionalBlock,
            'countOrg' => $countOrg,
            'paymentsAmount' => $paymentsAmount,
            'orgName' => $entry->getOrganizationName()
        ]);
    }

    /**
     * Расчёт цены соответствующей услуге.
     *
     * @return ViewModel
     */
    public function getSumAction()
    {
        $dataPost = $this->getRequest()->getPost()->toArray();
        $CPaymentNotification = new CPaymentNotification($this->em);
        $paymentOrder = $CPaymentNotification->getPaymentOrderByEntryId($dataPost['entryId']);

        $jsonModel = new JsonModel(array(
            'status' => (is_null($paymentOrder['uin'])) ? 'error' : 'ok',
            'price' => $paymentOrder['price'],
            'paymentOrderId' => $paymentOrder['id'],
        ));

        return $jsonModel;
    }

    /**
     * Создание платежного уведомления.
     *
     * @return \Zend\Http\Response|JsonModel
     */
    public function createPaymentOrderAction()
    {
        $form = new PaymentNotificationForm('personal-form');
        $dataPost = $this->getRequest()->getPost()->toArray();
        $form->setData($dataPost);

        if ($form->isValid()) {
            $CPaymentNotification = new CPaymentNotification($this->em);
            $paymentOrder = $CPaymentNotification->getPaymentOrderById($dataPost['paymentOrderId']);
            $electronicQueueEntry = $this->em->getRepository(ElectronicQueueEntry::class)->find($dataPost['entryId']);
            $organization = $this->em->getRepository(EducationalOrganization::class)->find($electronicQueueEntry->getOrganizationId());
            $fieldInn = $electronicQueueEntry->getOrganizationInn();
            $fieldUin  = $CPaymentNotification->generationUIN($paymentOrder['uin'], $fieldInn);
            $kbk = $CPaymentNotification->generationKBK($electronicQueueEntry);
            $kpp = (is_null($organization)) ? '' : (is_null($organization->getKpp()) || $organization->getKpp() == 'NULL') ? '' : $organization->getKpp();
            $sum = $dataPost['sum'];
            $data =  array(
                'fieldInn'          => $fieldInn,
                'fieldKpp'          => $kpp,                                                    //КПП
                'fieldUin'          => $fieldUin,                                               //УИН
                'fieldUinDubl'      => $fieldUin,
                'fieldBankName'     => $dataPost['bankName'],                                   // Имя банка
                'fieldSumNumber'    => $sum,                                                    // Сумма цифрами
                'fieldBankBik'      => $dataPost['bankBIK'],                                    // БИК банка
                'fieldBankNumber'   => $dataPost['corAccount'],                                 // Номер банка
                'fieldSumLeter'     =>  Export::num2strFirstUp($sum, true, true),               // Сумма словами
                'fieldBankAccount'  => $dataPost['numEduAccount'],                              // Расчётный счёт плательщика
                'fieldComment'      => $paymentOrder['name'],                                   // Имя платежа
                'fieldPayment'      => trim(strip_tags($dataPost['orgName']))                   // Имя организации
            );

            $id = $this->em->getRepository(PaymentNotification::class)->savePaymentOrder($dataPost, $fieldUin, $kbk);

            try {
                $document = $this->createDocumentPaymentOrder($data);
                $this->sendMail($document, $electronicQueueEntry->getEMail(), $id);
                $jsonModel = new JsonModel(array(
                    'status' => 'ok',
                    'id'     => $id,
                    'documentData' => $data
                ));
                return $jsonModel;
            } catch (\Exception $e) {

            }
        }

        return new JsonModel(array(
            'status' => 'error',
            'idField' => $form->getErrorIdField()
        ));
    }

    /**
     * Редактирование платёжки.
     *
     * @return ViewModel
     */
    public function editingAction()
    {
        // Платёж
        $paymentId = $this->params()->fromRoute('entryId');
        $entryPayment = $this->em->getRepository(PaymentNotification::class)->find($paymentId);
        if(empty($entryPayment)){
            return $this->errorMessage('Нет такой записи');
        }
        // Очередь
        $queueId = $entryPayment->entryId();
        $queue = $this->em->getRepository(ElectronicQueueEntry::class)->find($queueId);

        $dateObj = $queue->getStartReceptionTime();
        $date = $dateObj->format('Y-m-d');

        if(strtotime($date) < strtotime(date('Y-m-d'))){
           return $this->errorMessage('Дата приёма уже наступила, редактирование не доступно');
        }

        $form = new PaymentNotificationForm('personal-form', $this->em, $this->translator);
        $idType = $queue->getElectronicQueueReasonId();

        $type = $this->em->getRepository(ElectronicQueueEntry::class)->getServiceType($this->em, $idType, 'serviceType');
        $serviceReason = $this->em->getRepository(ElectronicQueueEntry::class)->getServiceReasonCatalog($this->em, $queue->getServiceReasonCatalogId());

        if(in_array($idType, $this->licensing)){              // Если лицензирование
            $data = $this->em->getRepository(ElectronicQueueEntry::class)->getBasisTreatment($this->em, $queue->getId(),'licensing');
        }elseif(in_array($idType, $this->accreditation)){     // Если аккредитация
            $data = $this->em->getRepository(ElectronicQueueEntry::class)->getBasisTreatment($this->em, $queue->getId(), 'accreditation');
        }else{
            return $this->errorMessage('Не коректные данные');
        }

        $CPaymentNotification = new CPaymentNotification($this->em);

        $sum = $entryPayment->summ();
        $paymentOrderId = $CPaymentNotification->extractIdToPaymentOrderUIN($entryPayment->uin());

        // Нужно ли показывать дополнительный блок
        $additionalBlock = $data['additionalBlock'];
        $form->setCausesTreatment($data['listCauses']);

        $form->setOrganization($entryPayment->organizationName());
        $form->setType($type);
        $form->setBasisTreatment($serviceReason['name']);
        $form->setSum($sum);
        $form->setCheckingAccount($entryPayment->numEduAccount());
        $form->setNameBank($entryPayment->bankName());
        $form->setBIKBank($entryPayment->bankBIK());
        $form->setCorrAccount($entryPayment->corAccount());

        $countOrg = $queue->getFilialAmount() + 1;
        if($queue->getIsBranchesOnly()) $countOrg--;

        $view = new ViewModel([
            'paymentOrderId' => $paymentOrderId,
            'summ' => $sum,
            'paymentNotificationId' => $entryPayment->id(),
            'entryId' => $queueId,
            'form' => $form,
            'additionalBlock' => $additionalBlock,
            'paymentsAmount' => 1,
            'countOrg' => $countOrg,
            'orgName' => $queue->getOrganizationName()
        ]);

        $view->setTemplate('application/payment-notification/create');
        return $view;
    }

    public function resultAction()
    {
        return new ViewModel();
    }

    /**
     * Отправка платёжного поручения на почту.
     *
     * @param $document
     * @param $email
     * @param $id
     *
     * @return int
     */
    public function sendMail($document, $email, $id)
    {
        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/payment-notification/editing/' . $id;
        $body = $this->renderMailBody([
            'link' => $link,
        ]);
        $attachment = new Swift_Attachment($document, 'Платёжное поручение  .docx', 'application/msword');
        $message = \Swift_Message::newInstance()
            ->setSubject('Платёжное поручение')
            ->setTo($email)
            ->setFrom('noreply@eq.obrnadzor.gov.ru')
            ->attach($attachment)
            ->setBody($body, 'text/html');

        $transport = $this->isTest ? \Swift_MailTransport::newInstance() : \Swift_SmtpTransport::newInstance($this->mailServer);
        $mailer = \Swift_Mailer::newInstance($transport);
        return $mailer->send($message);
    }

    /**
     * Создание документа платёжного поручения.
     *
     * @param $data
     * @param bool $download
     *
     * @return Templater
     */
    public function createDocumentPaymentOrder($data, $download = false)
    {
        $ourDocType='docx';
        $templatePath = $_SERVER['DOCUMENT_ROOT'] . '/public/templates/';
        $templateName = $templatePath . 'Payment.docx'; //шаблон документа
        $outDocName = 'PaymentOrder';
        $templater = new Templater($templateName, array('PAYMENT' => array($data)), $ourDocType, $outDocName );

        if($download){
            return $templater->makeDocumentOut();
        }
        return $templater->makeDocument();
    }

    /**
     * Поиск цены по uin причины.
     *
     * @return JsonModel
     */
    public function getPriceByUinAction()
    {
        $uin = $this->getRequest()->getPost()->toArray();
        $entities  = $this->em->getRepository(PaymentOrder::class)->findBy($uin);
        $priceList = array();
        foreach ($entities as $entity){
            $priceList[$entity->uin()] = array(
                'price' => $entity->price(),
                'paymentOrderId' => $entity->id()
            );
        }

        $jsonModel = new JsonModel(array(
            'status' => (empty($entity)) ? 'error' : 'ok',
            'priceList' => $priceList,
        ));

        return $jsonModel;
    }

    /**
     * Вывод сгенеренного документа во вью либо скачивание.
     *
     * @return Templater|ViewModel
     */
    public function documentAction()
    {
        $operation = $this->params()->fromRoute('operation');
        $data = $this->getRequest()->getPost()->toArray();

        if($operation == 'download' && $data){
            return $this->createDocumentPaymentOrder($data, true);
        }

        $view = new ViewModel([
            'doc' => $this->createDocumentPaymentOrder($data),
            'data' => $data
        ]);

        return $view;
    }

    private function renderMailBody($params, $tpl = 'mail/body_refinement')
    {
        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\RendererInterface');
        $content = new \Zend\View\Model\ViewModel($params);
        $content->setTemplate($tpl);

        return $renderer->render($content);
    }

    private function errorMessage($string)
    {
        $view = new ViewModel([
            'message' => $string,
        ]);
        $view->setTemplate('application/payment-notification/error-message');

        return $view;
    }
}