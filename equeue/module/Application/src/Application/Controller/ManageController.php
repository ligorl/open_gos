<?php

namespace Application\Controller;

use Application\Entity\ApplicationReason;
use Application\Entity\ElectronicQueueEntry;
use Application\Entity\ElectronicQueueEntryToAccreditationReason;
use Application\Entity\ElectronicQueueEntryToLicenseReasonCatalogItem;
use Application\Entity\ElectronicQueueLog;
use Application\Entity\ElectronicQueueManager;
use Application\Entity\ElectronicQueueReason;
use Application\Entity\ElectronicQueueVacation;
use Application\Entity\Exchange;
use Application\Entity\IssueDocumentsAmount;
use Application\Entity\LicenseReasonCatalogItem;
use Application\Entity\NumberOfDocumentsCatalog;
use Application\Entity\NumberOfProgramCatalog;
use Application\Entity\ServiceReasonCatalog;
use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\EntityManager;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\I18n\Translator;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class ManageController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Translator
     */
    protected $translator;

    /**
     * @var string
     */
    protected $mailServer = '192.168.66.240';

    protected $isTest = false;

    public function __construct()
    {
        //test server detection
        if (isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] == '85.192.47.4') {
            $this->isTest = true;
        }
    }

    public function setEventManager(EventManagerInterface $events)
    {
        session_start();
        parent::setEventManager($events);
        $controller = $this;
        $events->attach('dispatch', function ($e) use ($controller) {
            $action = $controller->params('action');
            if ($action != 'login' && !isset($_SESSION['authorizedUserID'])) {
                $controller->redirect()->toRoute('equeue/manage/login');
            }
            $controller->layout('layout/manage');
            $controller->layout()->setVariable('current_action', $action);
        }, 100);
    }

    /**
     * @param EntityManager $em
     * @return ManageController
     */
    public function setEm($em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * @param Translator $translator
     * @return ManageController
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
        return $this;
    }

    public function loginAction()
    {
        unset($_SESSION['authorizedUserID']);
        if (!empty($_POST['login']) && !empty($_POST['password'])) {
            $manager = $this->em->getRepository(ElectronicQueueManager::class)->getByLogin($_POST['login']);
            if ($manager && $manager->checkPassword($_POST['password'])) {
                $_SESSION['authorizedUserID'] = $manager->getId();
                $_SESSION['authorizedUserTag'] = $manager->getLogin() . '#' . $manager->getId();
                $this->redirect()->toRoute('equeue/manage');
            }
        }
        return new ViewModel([
        ]);
    }

    public function logoutAction()
    {
        unset($_SESSION['authorizedUserID']);
        $this->redirect()->toRoute('equeue/manage/login');
    }

    public function indexAction()
    {
        $logs = $this->em->getRepository(ElectronicQueueLog::class)->getRecent();
        return new ViewModel([
            'logs' => $logs,
            'em' => $this->em
        ]);
    }

    public function vacationsAction()
    {
        $reasons = $this->em->getRepository(ElectronicQueueReason::class)->findAll();
        $vacations = $this->em->getRepository(ElectronicQueueVacation::class)->getAll();

        return new ViewModel([
            'reasons' => $reasons,
            'vacations' => $vacations
        ]);
    }

    public function vacationsAddAction()
    {
        if (!empty($_POST['date_from']) && !empty($_POST['date_to']) && !empty($_POST['reason_id'])) {
            $from = new \DateTime($_POST['date_from']);
            $to = new \DateTime($_POST['date_to']);
            $reasonId = $_POST['reason_id'];

            $vacation = new ElectronicQueueVacation();
            $vacation->setFrom($from);
            $vacation->setTo($to);
            $vacation->setReasonId($reasonId);

            $this->em->persist($vacation);
            $this->em->flush();

            $log = new ElectronicQueueLog();
            $log->setManager($_SESSION['authorizedUserTag'])
                ->setAction(ElectronicQueueLog::ACTION_VACATION_ADD)
                ->setPayload($vacation->getId() . '::' . $vacation->getFrom()->format('Y-m-d') . '::' . $vacation->getTo()->format('Y-m-d') . '::' . $vacation->getReasonId());

            $this->em->persist($log);
            $this->em->flush();
        }

        $this->redirect()->toRoute('equeue/manage/vacations');
    }

    public function vacationsRemoveAction()
    {
        $entryId = $this->params()->fromRoute('entryId');
        $vacation = $this->em->getRepository(ElectronicQueueVacation::class)->find($entryId);
        if ($vacation) {
            $log = new ElectronicQueueLog();
            $log->setManager($_SESSION['authorizedUserTag'])
                ->setAction(ElectronicQueueLog::ACTION_VACATION_REMOVE)
                ->setPayload($vacation->getId() . '::' . $vacation->getFrom()->format('Y-m-d') . '::' . $vacation->getTo()->format('Y-m-d') . '::' . $vacation->getReasonId());

            $this->em->remove($vacation);
            $this->em->persist($log);
            $this->em->flush();
        }

        $this->redirect()->toRoute('equeue/manage/vacations');
    }

    public function managersAction()
    {
        $managers = $this->em->getRepository(ElectronicQueueManager::class)->findAll();

        return new ViewModel([
            'managers' => $managers
        ]);
    }

    public function managersAddAction()
    {
        if (!empty($_POST['login']) && !empty($_POST['password'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];

            $manager = new ElectronicQueueManager();
            $manager->setLogin($_POST['login'])
                ->setPasswordRaw($_POST['password']);

            $this->em->persist($manager);
            $this->em->flush();

            $log = new ElectronicQueueLog();
            $log->setManager($_SESSION['authorizedUserTag'])
                ->setAction(ElectronicQueueLog::ACTION_MANAGER_ADD)
                ->setPayload($manager->getLogin() . '#' . $manager->getId());
            $this->em->persist($log);
            $this->em->flush();
        }

        $this->redirect()->toRoute('equeue/manage/managers');
    }

    public function managersRemoveAction()
    {
        $managerId = $this->params()->fromRoute('managerId');
        if ($managerId == 1) {
            $log = new ElectronicQueueLog();
            $log->setManager($_SESSION['authorizedUserTag'])
                ->setAction(ElectronicQueueLog::ACTION_MANAGER_REMOVE_ROOT_ATTEMPT);

            $this->em->persist($log);
            $this->em->flush();
        } else {
            $manager = $this->em->getRepository(ElectronicQueueManager::class)->find($managerId);
            if ($manager) {
                $log = new ElectronicQueueLog();
                $log->setManager($_SESSION['authorizedUserTag'])
                    ->setAction(ElectronicQueueLog::ACTION_MANAGER_REMOVE)
                    ->setPayload($manager->getLogin() . '#' . $manager->getId());

                $this->em->remove($manager);
                $this->em->persist($log);
                $this->em->flush();
            }
        }

        $this->redirect()->toRoute('equeue/manage/managers');
    }

    public function entriesAction()
    {
        $reasons = $this->em->getRepository(ElectronicQueueReason::class)->findAll();
        $startReceptionTime = $reasons[0]->getStartReceptionTime();
        $endReceptionTime = $reasons[0]->getEndReceptionTime();
        $baseReceptionTime = $reasons[0]->getBaseReceptionTime();
        $days = ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'];
        $reasonDays = explode(';', $reasons[0]->getReceptionDaysRaw());
        $reasonDaysJs = [];
        foreach ($reasonDays as $r) {
            $reasonDaysJs[] = array_search(strtolower($r), $days);
        }

        return new ViewModel([
            'serviceReasons' => $reasons,
            'currentDate' => (new \DateTime()),
            'startReceptionTime' => $startReceptionTime,
            'endReceptionTime' => $endReceptionTime,
            'baseReceptionTime' => $baseReceptionTime,
            'receptionDays' => $reasonDays,
            'receptionDaysJs' => $reasonDaysJs
        ]);
    }

    public function entriesCalendarAction()
    {
        $success = true;
        $step = 0;
        $start = '08:00';
        $end = '20:00';
        $events = [];

        $reasonId = null;
        $reason = null;
        if (isset($_GET['reasonId'])) {
            $reasonId = $_GET['reasonId'];
            /** @var ElectronicQueueReason $reason */
            $reason = $this->em->getRepository(ElectronicQueueReason::class)->find($reasonId);
        }
        if ($reason) {
            $step = $reason->getBaseReceptionTime();
            $start = $reason->getStartReceptionTime()->format('H:i');
            $end = $reason->getEndReceptionTime()->format('H:i');

            if (isset($_GET['date'])) {
                $date = new \DateTime($_GET['date']);
                $cancelled = isset($_GET['cancelled']) ? ($_GET['cancelled'] == 'true') : false;
                $entries = $this->em->getRepository(ElectronicQueueEntry::class)->getForCalendar($reasonId, $date, $cancelled);
                foreach ($entries as $e) {
                    $events[] = $e->getCalendarData($step);
                }
            }
        } else {
            $success = false;
        }
        return new JsonModel([
            'events' => $events,
            'step' => $step,
            'start' => $start,
            'end' => $end,
            'success' => $success
        ]);
    }

    public function entriesCalendarFormAction()
    {
        $entryId = $this->params()->fromQuery('entryId');
        /** @var ElectronicQueueEntry $entry */
        $entry = $this->em->getRepository(ElectronicQueueEntry::class)->find($entryId);
        /** @var ElectronicQueueReason $entryReason */
        $entryReason = $this->em->getRepository(ElectronicQueueReason::class)->find($entry->getElectronicQueueReasonId());
        $appealReasons = [];
        $selectedReasons = [];
        /** @var ServiceReasonCatalog $serviceReason */
        $serviceReason = $this->em->getRepository(ServiceReasonCatalog::class)->find($entry->getServiceReasonCatalogId());
        $serviceReasons = $this->em->getRepository(ServiceReasonCatalog::class)->getByAppealTypeCode($entryReason->getCode());
        if (substr($serviceReason->getAppealTypeCode(), -8) == '_license') {
            $appealReasons = $this->em->getRepository(LicenseReasonCatalogItem::class)->findAll();
            $tmp = $this->em->getRepository(ElectronicQueueEntryToLicenseReasonCatalogItem::class)->getForEntry($entryId);
            foreach ($tmp as $e) {
                $selectedReasons[] = $e->getLicenseReasonCatalogItemId();
            }
        } else {
            $appealReasons = $this->em->getRepository(ApplicationReason::class)->findAll();
            $tmp = $this->em->getRepository(ElectronicQueueEntryToAccreditationReason::class)->getForEntry($entryId);
            foreach ($tmp as $e) {
                $selectedReasons[] = $e->getAccredApplicationReasons();
            }
        }
        $view = new ViewModel([
            'programs' => $this->em->getRepository(NumberOfProgramCatalog::class)->findAll(),
            'documents' => $this->em->getRepository(NumberOfDocumentsCatalog::class)->findAll(),
            'issueDocuments' => $this->em->getRepository(IssueDocumentsAmount::class)->findAll(),
            'entry' => $entry,
            'serviceReasons' => $serviceReasons,
            'currentServiceReason' => $serviceReason->getId(),
            'reasons' => $appealReasons,
            'selectedReasons' => $selectedReasons,
            'entryReason' => $entryReason
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function entriesApplyCalendarFormAction()
    {
        $entryId = $this->params()->fromPost('id');
        /** @var ElectronicQueueEntry $entry */
        $entry = $this->em->getRepository(ElectronicQueueEntry::class)->find($entryId);
        if ($entry) {
            $entry->setServiceReasonCatalogId($this->params()->fromPost('serviceReason'))
                ->setFirstName($this->params()->fromPost('firstName'))
                ->setLastName($this->params()->fromPost('lastName'))
                ->setMiddleName($this->params()->fromPost('middleName'))
                ->setPhoneNumber($this->params()->fromPost('phoneNumber'))
                ->setMobilePhoneNumber($this->params()->fromPost('mobilePhoneNumber'))
                ->setEMail($this->params()->fromPost('email'))
                ->setStatus($this->params()->fromPost('status'));

            switch ($entry->getStatus()) {
                case 'cancelled':
                    $this->em->getRepository(ElectronicQueueEntry::class)->deleteWithSlots([$entryId]);
                    //notify cancel
                    /** @var ElectronicQueueReason $queueReason */
                    $queueReason = $this->em->getRepository(ElectronicQueueReason::class)
                        ->find($entry->getElectronicQueueReasonId());
                    $notifyMails = $queueReason->getEqNotifyMails();
                    if (count($notifyMails)) {
                        $body = 'Отменена запись на ' . $entry->getStartReceptionTime()->format('d.m.Y H:i') . '<br/>'
                            . 'Процедура: ' . $queueReason->getName() . '<br/>'
                            . $entry->getLastName() . ' ' . $entry->getFirstName() . ' ' . $entry->getMiddleName() . '<br/>'
                            . $entry->getOrganizationName() . '<br/>';
                        $message = \Swift_Message::newInstance()
                            ->setFrom('noreply@eq.obrnadzor.gov.ru')
                            ->setSubject('Отменена запись в электронную очередь')
                            ->setTo($notifyMails)
                            ->setBody($body, 'text/html');
                        $transport = $this->isTest ? \Swift_MailTransport::newInstance() : \Swift_SmtpTransport::newInstance($this->mailServer);
                        $mailer = \Swift_Mailer::newInstance($transport);
                        $mailer->send($message);
                    }
                    break;
                case 'confirmed':
                    $entry->setConfirmedDateTime(new \DateTime());

                    //confirm notification
                    /** @var ElectronicQueueReason $queueReason */
                    $queueReason = $this->em->getRepository(ElectronicQueueReason::class)->find($entry->getElectronicQueueReasonId());

                    $appealReasons = [];
                    foreach ($this->em->getRepository(ElectronicQueueEntryToLicenseReasonCatalogItem::class)->getForEntry($entryId) as $r) {
                        $appealReasonObj = $this->em->getRepository(LicenseReasonCatalogItem::class)->find($r->getLicenseReasonCatalogItemId());
                        if ($appealReasonObj) $appealReasons[] = $appealReasonObj->getTitle();
                    }
                    foreach ($this->em->getRepository(ElectronicQueueEntryToAccreditationReason::class)->getForEntry($entryId) as $r) {
                        $appealReasonObj = $this->em->getRepository(ApplicationReason::class)->find($r->getAccredApplicationReasons());
                        if ($appealReasonObj) $appealReasons[] = $appealReasonObj->getShortName();
                    }
                    /** @var ServiceReasonCatalog $serviceReason */
                    $serviceReason = $this->em->getRepository(ServiceReasonCatalog::class)->find($entry->getServiceReasonCatalogId());

                    //delivery entry
                    $length = ($entry->getEndReceptionTime()->getTimestamp() - $entry->getStartReceptionTime()->getTimestamp()) / 60;

                    $cancelLink = 'http://' . $_SERVER['SERVER_NAME'] . '/equeue/cancel/' . $entry->getId();
                    $body = $this->renderMailBody([
                        'cancel_link' => $cancelLink,
                        'date_day' => $entry->getStartReceptionTime()->format('d.m.y'),
                        'date_time' => $entry->getStartReceptionTime()->format('H:i'),
                        'date_register' => (new \DateTime())->format('d.m.y'),
                        'audienceRoom' => $queueReason->getRoomNumber(),
                        'audienceLength' => $length,
                        'appeal_type' => $queueReason->getName()
                    ], 'mail/body_confirm');
                    $message = \Swift_Message::newInstance()
                        ->setFrom('noreply@eq.obrnadzor.gov.ru')
                        ->setSubject('Подтверждение записи в электронную очередь')
                        ->setTo($entry->getEMail())
                        ->setBody($body, 'text/html');
                    $transport = $this->isTest ? \Swift_MailTransport::newInstance() : \Swift_SmtpTransport::newInstance($this->mailServer);
                    $mailer = \Swift_Mailer::newInstance($transport);
                    $mailer->send($message);

                    $notifyMails = $queueReason->getEqNotifyMails();
                    if (count($notifyMails)) {
                        $body = $this->renderMailBody([
                            'date_day' => $entry->getStartReceptionTime()->format('d.m.y'),
                            'date_time' => $entry->getStartReceptionTime()->format('H:i'),
                            'date_register' => $entry->getCreatedDateTime()->format('d.m.y'),
                            'audienceRoom' => $queueReason->getRoomNumber(),
                            'audienceLength' => $length,
                            'name' => $entry->getLastName() . ' ' . $entry->getFirstName() . ' ' . $entry->getMiddleName(),
                            'org' => $entry->getOrganizationName(),
                            'procedure' => $queueReason->getName(),
                            'appeal_reasons' => $appealReasons,
                            'service_reason' => $serviceReason->getName()
                        ], 'mail/notify_create');
                        $message = \Swift_Message::newInstance()
                            ->setFrom('noreply@eq.obrnadzor.gov.ru')
                            ->setSubject('Запись в электронную очередь')
                            ->setTo($notifyMails)
                            ->setBody($body, 'text/html');
                        $mailer->send($message);
                    }
                    break;
            }

            $ex = new Exchange();
            $ex->setTimestamp(new \DateTime())
                ->setSaveType('update')
                ->setTableName('ElectronicQueueEntry')
                ->setObjectId($entry->getId())
                ->setSource('vuzi');
            $this->em->persist($ex);

            $serviceReason = $this->em->getRepository(ServiceReasonCatalog::class)->find($entry->getServiceReasonCatalogId());
            if (substr($serviceReason->getAppealTypeCode(), -8) == '_license') {
                $ids = $this->params()->fromPost('appeal_reasons');
                $exist = [];
                $reasons = $this->em->getRepository(ElectronicQueueEntryToLicenseReasonCatalogItem::class)->getForEntry($entryId);
                foreach ($reasons as $reason) {
                    if (in_array($reason->getLicenseReasonCatalogItemId(), $ids)) {
                        $exist[] = $reason->getLicenseReasonCatalogItemId();
                    } else {
                        $ex = new Exchange();
                        $ex->setTimestamp(new \DateTime())
                            ->setSaveType('delete')
                            ->setTableName('ElectronicQueueEntryToLicenseReasonCatalogItem')
                            ->setObjectId($reason->getId())
                            ->setSource('vuzi');
                        $this->em->persist($ex);
                        $this->em->remove($reason);
                    }
                }
                foreach ($ids as $id) {
                    if (!in_array($id, $exist)) {
                        $reason = new ElectronicQueueEntryToLicenseReasonCatalogItem();
                        $reason->setElectronicQueueEntryId($entryId);
                        $reason->setLicenseReasonCatalogItemId($id);
                        $this->em->persist($reason);
                        $this->em->flush();
                        $ex = new Exchange();
                        $ex->setTimestamp(new \DateTime())
                            ->setSaveType('insert')
                            ->setTableName('ElectronicQueueEntryToLicenseReasonCatalogItem')
                            ->setObjectId($reason->getId())
                            ->setSource('vuzi');
                        $this->em->persist($ex);
                    }
                }
            } else {
                $ids = $this->params()->fromPost('appeal_reasons');
                $exist = [];
                $reasons = $this->em->getRepository(ElectronicQueueEntryToAccreditationReason::class)->getForEntry($entryId);
                foreach ($reasons as $reason) {
                    if (in_array($reason->getAccredApplicationReasons(), $ids)) {
                        $exist[] = $reason->getAccredApplicationReasons();
                    } else {
                        $ex = new Exchange();
                        $ex->setTimestamp(new \DateTime())
                            ->setSaveType('delete')
                            ->setTableName('ElectronicQueueEntryToAccreditationReason')
                            ->setObjectId($reason->getId())
                            ->setSource('vuzi');
                        $this->em->persist($ex);
                        $this->em->remove($reason);
                    }
                }
                foreach ($ids as $id) {
                    if (!in_array($id, $exist)) {
                        $reason = new ElectronicQueueEntryToAccreditationReason();
                        $reason->setElectronicQueueEntryId($entryId);
                        $reason->setAccredApplicationReasons($id);
                        $this->em->persist($reason);
                        $this->em->flush();
                        $ex = new Exchange();
                        $ex->setTimestamp(new \DateTime())
                            ->setSaveType('insert')
                            ->setTableName('ElectronicQueueEntryToAccreditationReason')
                            ->setObjectId($reason->getId())
                            ->setSource('vuzi');
                        $this->em->persist($ex);
                    }
                }
            }
        }
        $this->em->flush();
        return new JsonModel([
            'entryId' => $entryId,
            'status' => $entry->getStatus(),
            'success' => true
        ]);
    }

    private function renderMailBody($params, $tpl = 'mail/body')
    {
        $renderer = $this->getServiceLocator()->get('Zend\View\Renderer\RendererInterface');
        $content = new \Zend\View\Model\ViewModel($params);
        $content->setTemplate($tpl);
        return $renderer->render($content);
    }
}