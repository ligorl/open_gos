<?php

namespace Application\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class IdGenerator extends AbstractIdGenerator
{
    public function generate(EntityManager $em, $entity)
    {
        $mtimeParts = explode(" ", microtime());
        $base = $mtimeParts[1];
        $mtimeParts = explode(".", $mtimeParts[0]);
        $base .= $mtimeParts[1];

        $rand1 = rand(1000, 5000);
        $rand2 = rand(1000, 5000);

        $base .= ($rand1 + $rand2);

        $Id = sprintf('%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X',
            10 + $base[0], 10 + $base[1], 10 + $base[2], 10 + $base[3], 10 + $base[4], 10 + $base[5], 10 + $base[6],
            10 + $base[7], 10 + $base[8], 10 + $base[9], 10 + $base[10], 10 + $base[11], 10 + $base[12], 10 + $base[13],
            10 + $base[14], 10 + $base[15], 10 + $base[18], 10 + $base[19], 10 + $base[20], 10 + $base[21]);
        return md5($Id);
    }
}