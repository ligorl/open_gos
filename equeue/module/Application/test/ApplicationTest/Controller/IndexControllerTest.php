<?php

namespace ApplicationTest\Controller;

use Application\Controller\IndexController;
use Application\Entity\CatalogItemRepository;
use Application\Form\DeliveryForm;
use Application\Form\ReceptionForm;
use Doctrine\ORM\Mapping\Index;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class IndexControllerTest extends AbstractHttpControllerTestCase
{
    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__ . '/../../../../../config/application.config.php'
        );
        parent::setUp();
    }

    public function testHomepageIsWorking()
    {
        $this->dispatch('/');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('application');
        $this->assertControllerName('application\controller\index');
        $this->assertActionName('index');
    }

    public function test404Working()
    {
        $this->dispatch('/random/url');
        $this->assertResponseStatusCode(404);
    }

    protected function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}