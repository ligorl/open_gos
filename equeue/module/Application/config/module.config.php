<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'equeue' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/equeue/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => [
                    'checkStep1' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => 'checkStep1',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'checkStep1',
                            ),
                        ),
                    ),
                    'step2' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => 'step2',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'step2',
                            ),
                        ),
                    ),
                    'checkStep2' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => 'checkStep2',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'checkStep2',
                            ),
                        ),
                    ),
                    'step3' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => 'step3',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'step3',
                            ),
                        ),
                    ),
                    /*'kill' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => 'kill',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'kill',
                            ),
                        ),
                    ),*/
                    'confirm' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route'    => 'confirm/:entryId',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'confirm',
                            ),
                        ),
                    ),
                    'cancel' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route'    => 'cancel/:entryId',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'cancel',
                            ),
                        ),
                    ),
                    'maintenance' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route'    => 'maintenance',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Index',
                                'action'     => 'maintenance',
                            ),
                        ),
                    ),
                    'payment-notification' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => 'payment-notification[/:action]/:entryId',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\PaymentNotification',
                                'action'     => 'create',
                            ),
                        ),
                    ),
                    'document' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => 'document/:operation',
                            'constraints' => array(
                                'operation' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\PaymentNotification',
                                'action'     => 'document',
                            ),
                        ),
                    ),
                    'get-sum' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => 'get-sum',
                            'defaults' => array(
                                'controller' => 'Application\Controller\PaymentNotification',
                                'action'     => 'getSum',
                            ),
                        ),
                    ),
                    'get-price-by-uin' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => 'get-price-by-uin',
                            'defaults' => array(
                                'controller' => 'Application\Controller\PaymentNotification',
                                'action'     => 'getPriceByUin',
                            ),
                        ),
                    ),
                    'create-payment-order' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => 'create-payment-order',
                            'defaults' => array(
                                'controller' => 'Application\Controller\PaymentNotification',
                                'action'     => 'createPaymentOrder',
                            ),
                        ),
                    ),
                    'result' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => 'result',
                            'defaults' => array(
                                'controller' => 'Application\Controller\PaymentNotification',
                                'action'     => 'result',
                            ),
                        ),
                    ),
                    'json_oo_name' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => 'json/oo-name',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Json',
                                'action'     => 'ooName',
                            )
                        )
                    ),
                    'json_oo_inn' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => 'json/oo-inn',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Json',
                                'action'     => 'ooInn',
                            )
                        )
                    ),
                    'manage' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => 'manage/',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Manage',
                                'action' => 'index'
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes' => [
                            'login' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                    'route'    => 'login',
                                    'defaults' => array(
                                        'controller' => 'Application\Controller\Manage',
                                        'action'     => 'login',
                                    ),
                                ),
                            ),
                            'logout' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                    'route'    => 'logout',
                                    'defaults' => array(
                                        'controller' => 'Application\Controller\Manage',
                                        'action'     => 'logout',
                                    ),
                                ),
                            ),
                            'vacations' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                    'route'    => 'vacations/',
                                    'defaults' => array(
                                        'controller' => 'Application\Controller\Manage',
                                        'action'     => 'vacations',
                                    ),
                                ),
                                'may_terminate' => true,
                                'child_routes' => [
                                    'add' => array(
                                        'type' => 'Zend\Mvc\Router\Http\Literal',
                                        'options' => array(
                                            'route'    => 'add',
                                            'defaults' => array(
                                                'controller' => 'Application\Controller\Manage',
                                                'action'     => 'vacationsAdd',
                                            ),
                                        ),
                                    ),
                                    'remove' => array(
                                        'type' => 'Zend\Mvc\Router\Http\Segment',
                                        'options' => array(
                                            'route'    => 'remove/:entryId',
                                            'defaults' => array(
                                                'controller' => 'Application\Controller\Manage',
                                                'action'     => 'vacationsRemove',
                                            ),
                                        ),
                                    ),
                                ]
                            ),
                            'managers' => array(
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => array(
                                    'route'    => 'managers/',
                                    'defaults' => array(
                                        'controller' => 'Application\Controller\Manage',
                                        'action'     => 'managers',
                                    ),
                                ),
                                'may_terminate' => true,
                                'child_routes' => [
                                    'add' => array(
                                        'type' => 'Zend\Mvc\Router\Http\Literal',
                                        'options' => array(
                                            'route'    => 'add',
                                            'defaults' => array(
                                                'controller' => 'Application\Controller\Manage',
                                                'action'     => 'managersAdd',
                                            ),
                                        ),
                                    ),
                                    'remove' => array(
                                        'type' => 'Zend\Mvc\Router\Http\Segment',
                                        'options' => array(
                                            'route'    => 'remove/:managerId',
                                            'defaults' => array(
                                                'controller' => 'Application\Controller\Manage',
                                                'action'     => 'managersRemove',
                                            ),
                                        ),
                                    ),
                                ]
                            ),
                            'entries' => [
                                'type' => 'Zend\Mvc\Router\Http\Literal',
                                'options' => [
                                    'route'    => 'entries/',
                                    'defaults' => array(
                                        'controller' => 'Application\Controller\Manage',
                                        'action'     => 'entries',
                                    ),
                                ],
                                'may_terminate' => true,
                                'child_routes' => [
                                    'calendar' => array(
                                        'type' => 'Zend\Mvc\Router\Http\Literal',
                                        'options' => array(
                                            'route'    => 'calendar',
                                            'defaults' => array(
                                                'controller' => 'Application\Controller\Manage',
                                                'action'     => 'entriesCalendar',
                                            ),
                                        ),
                                    ),
                                    'calendarForm' => array(
                                        'type' => 'Zend\Mvc\Router\Http\Literal',
                                        'options' => array(
                                            'route'    => 'calendarForm',
                                            'defaults' => array(
                                                'controller' => 'Application\Controller\Manage',
                                                'action'     => 'entriesCalendarForm',
                                            ),
                                        ),
                                    ),
                                    'applyCalendarForm' => array(
                                        'type' => 'Zend\Mvc\Router\Http\Literal',
                                        'options' => array(
                                            'route'    => 'applyCalendarForm',
                                            'defaults' => array(
                                                'controller' => 'Application\Controller\Manage',
                                                'action'     => 'entriesApplyCalendarForm',
                                            ),
                                        ),
                                    ),
                                ]
                            ]
                        ]
                    )
                ]
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'ru_RU',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => [],
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'mail/body'               => __DIR__ . '/../view/mail/body.phtml',
            'mail/body_confirm'       => __DIR__ . '/../view/mail/body_confirm.phtml',
            'mail/notify_create'      => __DIR__ . '/../view/mail/notify_create.phtml',
            'mail/notify_vacation'    => __DIR__ . '/../view/mail/notify_vacation.phtml',
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/empty'            => __DIR__ . '/../view/layout/empty.phtml',
            'layout/manage'           => __DIR__ . '/../view/layout/manage.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy'
        )
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'check-entries' => [
                    'options' => [
                        'route' => 'check_entries',
                        'defaults' => [
                            'controller' => 'Application\Controller\Index',
                            'action' => 'checkEntries'
                        ]
                    ]
                ]
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'my_annotation_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/Application/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Application\Entity' => 'my_annotation_driver'
                )
            )
        )
    )
);
