<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\Controller\IndexController;
use Application\Controller\JsonController;
use Application\Controller\ManageController;
use Application\Controller\PaymentNotificationController;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                'Application\Controller\Index' => function($controllers) {
                    $sl = $controllers->getServiceLocator();
                    $em = $sl->get('doctrine.entitymanager.orm_default');
                    $tr = $sl->get('translator');

                    $controller = new IndexController();
                    $controller->setEm($em);
                    $controller->setTranslator($tr);

                    return $controller;
                },
                'Application\Controller\Json' => function($controllers) {
                    $sl = $controllers->getServiceLocator();
                    $em = $sl->get('doctrine.entitymanager.orm_default');

                    $controller = new JsonController();
                    $controller->setEm($em);

                    return $controller;
                },
                'Application\Controller\Manage' => function($controllers) {
                    $sl = $controllers->getServiceLocator();
                    $em = $sl->get('doctrine.entitymanager.orm_default');
                    $tr = $sl->get('translator');

                    $controller = new ManageController();
                    $controller->setEm($em);
                    $controller->setTranslator($tr);

                    return $controller;
                },
                'Application\Controller\PaymentNotification' => function($controllers) {
                    $sl = $controllers->getServiceLocator();
                    $em = $sl->get('doctrine.entitymanager.orm_default');

                    $controller = new PaymentNotificationController();
                    $controller->setEm($em);

                    return $controller;
                },
            ]
        ];
    }
}
