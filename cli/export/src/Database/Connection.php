<?php

namespace App\Database;

use PDO;

class Connection
{
    /**
     * @var Connection
     */
    private static $instance;

    /**
     * @var array
     */
    protected $connections = array();

    /**
     * @return Connection
     */
    public static function getInstance()
    {
        return self::$instance ?: self::$instance = new self();
    }

    /**
     * @param string|null $database
     *
     * @return PDO
     */
    public function getConnection($database = null)
    {
        $database = $database ?: env('DB_DATABASE');

        return array_key_exists($database, $this->connections)
            ? $this->connections[$database]
            : $this->connections[$database] = $this->createPdo($database);
    }

    /**
     * @param string $database
     *
     * @return PDO
     */
    protected function createPdo($database)
    {
        $host = env('DB_HOST');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');
        $port = env('DB_PORT');
        $dsn = "mysql:host=$host;port=$port;dbname=$database";

        return new PDO($dsn, $username, $password, array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
        ));
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}
