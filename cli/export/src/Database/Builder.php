<?php

namespace App\Database;

use App\Models\Model;

class Builder
{
    /**
     * Кол-во выполненных запросов.
     *
     * @var int
     */
    public static $executedQueriesCount = 0;

    /**
     * @var \PDO
     */
    protected $connection;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $columns = array('*');

    /**
     * @var string|array
     */
    protected $wheres = array();

    /**
     * @var array
     */
    protected $joins = array();

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $offset;

    /**
     * @var array
     */
    protected $bindings = array();

    /**
     * Builder constructor.
     *
     * @param \PDO        $connection
     * @param string|null $model
     */
    public function __construct($connection, $model = null)
    {
        $this->connection = $connection;
        $this->model = $model;
    }

    /**
     * @param string $table
     *
     * @return Builder
     */
    public function from($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @param array|string $columns
     *
     * @return $this
     */
    public function columns($columns)
    {
        $this->columns = is_array($columns) ? $columns : func_get_args();

        return $this;
    }

    /**
     * @param string       $key
     * @param string       $operator
     * @param int          $type
     * @param string|array $value
     * @param string       $bool
     *
     * @return $this
     */
    public function where($key, $operator, $type, $value, $bool = 'and')
    {
        $this->wheres[] = array($key, $operator, $type, $value, $bool);

        return $this;
    }

    /**
     * @param string $key
     * @param int    $type
     * @param array  $values
     * @param string $bool
     *
     * @return Builder
     */
    public function whereIn($key, $type, $values, $bool = 'and')
    {
        return $this->where($key, 'in', $type, $values, $bool);
    }

    /**
     * @param string $key
     * @param string $operator
     * @param int    $type
     * @param string $value
     *
     * @return Builder
     */
    public function orWhere($key, $operator, $type, $value)
    {
        return $this->where($key, $operator, $type, $value, 'or');
    }

    /**
     * @param string $sql
     * @param array  $bindings
     *
     * @return $this
     */
    public function whereRaw($sql, $bindings)
    {
        $this->wheres = $sql;
        $this->bindings = $bindings;

        return $this;
    }

    /**
     * @param int $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        $this->limit = (int) $limit;

        return $this;
    }

    /**
     * @param int $offset
     *
     * @return $this
     */
    public function offset($offset)
    {
        $this->offset = (int) $offset;

        return $this;
    }

    /**
     * @param string $table
     * @param string $on
     * @param array  $columns
     * @param array  $where
     * @param string $type
     *
     * @return $this
     */
    public function join($table, $on, $columns = array(), $where = array(), $type = 'inner')
    {
        $this->joins[] = array($table, $on, $where, $type);
        $this->columns = array_merge($this->columns, $columns);

        return $this;
    }

    /**
     * @return array|Model[]
     */
    public function get()
    {
        $statement = $this->connection->prepare($this->build());
        foreach ($this->bindings as $index => $binding) {
            list($type, $value) = $binding;
            $statement->bindValue($index + 1, $value, $type);
        }
        // dump($statement, $this->bindings);
        $statement->execute();
        static::$executedQueriesCount++;

        $records = $statement->fetchAll();
        $class = $this->model;

        return ! $class
            ? $records
            : array_map(function ($record) use ($class) {
                return new $class((array) $record);
            }, $records);
    }

    /**
     * @return Model|\stdClass
     */
    public function first()
    {
        return current($this->limit(1)->get());
    }

    /**
     * @return string
     */
    protected function build()
    {
        $columns = implode(', ', $this->columns);
        $joins = $this->buildJoins();
        $wheres = $this->buildWheres($this->wheres);

        $sql = sprintf(
            'SELECT %s FROM %s %s %s ',
            $columns,
            $this->table,
            $joins,
            $wheres
        );

        if ($this->limit) {
            $this->bindings[] = array(\PDO::PARAM_INT, $this->limit);
            $sql .= 'limit ? ';
        }

        if ($this->offset) {
            $this->bindings[] = array(\PDO::PARAM_INT, $this->offset);
            $sql .= 'offset ?';
        }

        return $sql;
    }

    /**
     * @param array $wheres
     * @param mixed $join
     *
     * @return string|null
     */
    protected function buildWheres($wheres, $join = false)
    {
        if (empty($wheres)) {
            return;
        }

        $wheresSql = $join ? array() : array('where');
        $exists = $join;
        foreach ($wheres as $where) {
            list($key, $operator, $type, $value, $bool) = $where;
            $this->appendBindings($type, $value);
            $prefix = $exists ? $bool : '';
            $placeholder = '?';

            if ($operator === 'in') {
                $placeholders = implode(', ', array_fill(0, count((array) $value), $placeholder));
                $placeholder = "($placeholders)";
            }

            $wheresSql[] = $prefix . " $key $operator $placeholder ";

            $exists = true;
        }

        return implode(' ', $wheresSql);
    }

    /**
     * @return string|null
     */
    protected function buildJoins()
    {
        if (empty($this->joins)) {
            return;
        }

        $joins = array();
        foreach ($this->joins as $join) {
            list($table, $on, $where, $type) = $join;
            $joins[] = $this->buildJoin($table, $on, $where, $type);
        }

        return implode(' ', $joins);
    }

    /**
     * @param string $table
     * @param string $on
     * @param array  $where
     * @param string $type
     *
     * @return string
     */
    protected function buildJoin($table, $on, $where, $type)
    {
        return sprintf(
            '%s join %s on %s %s',
            $type,
            $table,
            $on,
            $this->buildWheres($where, true)
        );
    }

    /**
     * @param int   $type
     * @param mixed $value
     *
     * @return void
     */
    protected function appendBindings($type, $value)
    {
        if (is_array($value)) {
            foreach ($value as $item) {
                $this->appendBindings($type, $item);
            }
        } else {
            $this->bindings[] = array($type, $value);
        }
    }
}
