<?php

namespace App\Models;

class Program extends Model
{
    protected $table = 'isga_AccreditedPrograms';

    public function getColumns()
    {
        return array(
            'Id',
            'Name',
            'Code',
        );
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return 'programms';
    }

    /**
     * @return string
     */
    public function getElementName()
    {
        return 'programm';
    }

    /**
     * @return array
     */
    public function toXml()
    {
        return array(
            'ProgrammName' => $this->Name,
            'ProgrammCode' => $this->Code,
        );
    }

    /**
     * @return array
     */
    public function toXmlAttributes()
    {
        return array(
            'guid' => $this->Id,
        );
    }
}
