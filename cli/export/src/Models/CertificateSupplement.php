<?php

namespace App\Models;

class CertificateSupplement extends Model
{
    protected $table = 'isga_CertificateSupplements';

    /**
     * @return array
     */
    public function getColumns()
    {
        return array(
            'Id',
            'FormNumber',
            'SerialNumber',
            'DateIssue',
            'DateEnd',
            'fk_isgaCertificateSupplementStatus',
            'fk_isgaCertificate',
        );
    }

    /**
     * @return array
     */
    public function toXml()
    {
        return array(
            'RegNumber' => $this->parent ? $this->parent->RegNumber : null,
            'FormNumber' => $this->FormNumber,
            'SerialNumber' => $this->SerialNumber,
            'IssueDate' => $this->getDate('DateIssue', true),
            'EndDate' => $this->getDate('DateEnd', true),
            'CertificateStatusId' => $this->fk_isgaCertificateSupplementStatus,
            'fk_isgaCertificate' => $this->fk_isgaCertificate,
        );
    }

    /**
     * @param  string  $property
     * @param  boolean $fromParent
     *
     * @return string|null
     */
    protected function getDate($property, $fromParent = false)
    {
        return is_date_valid($this->$property)
            ? $this->$property
            : $fromParent ? $this->getParentProperty($property) : null;
    }

    /**
     * @param string $property
     *
     * @return string|null
     */
    protected function getParentProperty($property)
    {
        if (! $this->parent) {
            return null;
        }

        $value = $this->parent->{$property};

        return empty($value) ? null : $value;
    }

    /**
     * @param Model|Model[] $certificateSupplements
     *
     * @return Model|Model[]
     */
    public static function loadParents($certificateSupplements)
    {
        return self::eager($certificateSupplements, 'parent', array(
            'fk_isgaCertificate',
            'Id',
            \PDO::PARAM_STR,
            new Certificate,
            array('Id', 'RegNumber', 'DateIssue', 'DateEnd', 'GUID'),
        ));
    }

    /**
     * @return string
     */
    public function getElementName()
    {
        return 'accreditation';
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return 'accreditations';
    }

    /**
     * @return array
     */
    public function toXmlAttributes()
    {
        return array(
            'Id' => $this->Id,
            'guid' => $this->parent ? $this->parent->GUID : null,
        );
    }
}
