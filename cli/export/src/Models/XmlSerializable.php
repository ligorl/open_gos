<?php

namespace App\Models;

interface XmlSerializable
{
    /**
     * @return string
     */
    public function getGroupName();

    /**
     * @return string
     */
    public function getElementName();

    /**
     * @return array
     */
    public function toXml();

    /**
     * @return array
     */
    public function toXmlAttributes();
}
