<?php

namespace App\Models;

class Organization extends Model
{
    protected $table = 'eiis_EducationalOrganizations';

    /**
     * @return array
     */
    public function getColumns()
    {
        return array(
            'Id',
            'FullName',
            'ShortName',
            'ChargeFio',
            'Mails',
            'LawAddress',
            'Phones',
            'Faxes',
            'LawCity',
            'Inn',
            'GosRegNum',
            'lod_stateCode',
            'fk_eiisEducationalOrganizationProperties',
            'Branch',
            'fk_eiisParentEducationalOrganization',
        );
    }

    /**
     * @return boolean
     */
    public function hasParent()
    {
        return (int) $this->Branch;
    }

    /**
     * @return array
     */
    public function toXml()
    {
        return array(
            'name' => $this->FullName,
            'shortname' => $this->ShortName,
            'founder' => Founder::joinFoundersNames($this->founders),
            'rector' => $this->ChargeFio,
            'email' => $this->Mails,
            'address' => $this->LawAddress,
            'phone' => $this->Phones,
            'fax' => $this->Faxes,
            'city' => $this->LawCity,
            'inn' => $this->Inn,
            'ogrn' => $this->GosRegNum,
            'active' => (int) ($this->lod_stateCode === 'VALID'),
            'fk_eiisEducationalOrganizationProperties' => $this->fk_eiisEducationalOrganizationProperties,
            'accreditations' => $this->certificates,
            'licenses' => $this->licenses,
            'programs' => $this->programs,
        );
    }

    /**
     * @param Model|Model[] $organizations
     *
     * @return array($withoutParent, $withParent)
     */
    public static function groupByParent($organizations)
    {
        $withoutParent = array();
        $withParent = array();
        foreach ($organizations as $organization) {
            if ($organization->hasParent()) {
                $withParent[] = $organization;
            } else {
                $withoutParent[] = $organization;
            }
        }

        return array($withoutParent, $withParent);
    }

    /**
     * @param Model|Model[] $organizations
     *
     * @return Model|Model[]
     */
    public static function loadCertificates($organizations)
    {
        list($withoutParent, $withParent) = static::groupByParent($organizations);

        $certificates = static::loadCertificatesWithModel(
            $withoutParent,
            new Certificate
        );
        $certificateSupplements = static::loadCertificatesWithModel(
            $withParent,
            new CertificateSupplement
        );

        CertificateSupplement::loadParents($certificateSupplements);
        // dump((array)$certificateSupplements);die;


        return array_merge((array)$certificates, (array)$certificateSupplements);
    }

    /**
     * @param Model|Model[] $organizations
     *
     * @return Model|Model[]
     */
    public static function loadLicenses($organizations)
    {
        list($withoutParent, $withParent) = static::groupByParent($organizations);

        $licenses = static::loadLicensesWithModel(
            $withoutParent,
            new License
        );
        $licenseSupplements = static::loadLicensesWithModel(
            $withParent,
            new LicenseSupplement,
            'Id',
            'fk_eiisBranch'
        );
        $out = LicenseSupplement::loadParents($licenseSupplements);
        // dump((array)$licenseSupplements);die;

        return array_merge($licenses, $licenseSupplements);
    }

    /**
     * @param Model|Model[] $organizations
     * @param Model $relatedModel
     * @param string $localKey
     * @param array $columns
     *
     * @return Model|Model[]
     */
    public static function loadCertificatesWithModel($organizations, $relatedModel, $localKey = 'Id', array $columns = array())
    {
        return self::eager($organizations, 'certificates', array(
            $localKey,
            'fk_eiisEducationalOrganization',
            \PDO::PARAM_STR,
            $relatedModel,
            $columns,
        ), true);
    }

    /**
     * @param Model|Model[] $organizations
     * @param Model $relatedModel
     * @param string $localKey
     * @param string $foreignKey
     * @param array $columns
     *
     * @return Model|Model[]
     */
    public static function loadLicensesWithModel(
        $organizations,
        $relatedModel,
        $localKey = 'Id',
        $foreignKey = 'fk_eiisEducationalOrganization',
        array $columns = array()
        )
    {
        return self::eager($organizations, 'licenses', array(
            $localKey,
            $foreignKey,
            \PDO::PARAM_STR,
            $relatedModel,
            $columns,
        ), true);
    }

    /**
     * @param Model|Model[] $organizations
     *
     * @return Model|Model[]
     */
    public static function loadPrograms($organizations)
    {
        return self::eager($organizations, 'programs', array(
            'Id',
            'fk_eiisEducationalOrganization',
            \PDO::PARAM_STR,
            new Program,
            array(),
        ), true);
    }

    /**
     * @return string
     */
    public function getElementName()
    {
        return 'organization';
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return 'organizations';
    }

    /**
     * @return array
     */
    public function toXmlAttributes()
    {
        return array(
            'id_org' => $this->Id,
            'parent_id' => $this->hasParent()
                ? $this->fk_eiisParentEducationalOrganization
                : 0,
        );
    }
}
