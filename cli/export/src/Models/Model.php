<?php

namespace App\Models;

use App\Database\Builder;
use App\Database\Connection;

abstract class Model implements XmlSerializable
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    protected $primary = 'Id';

    /**
     * @var int
     */
    protected $primaryType = \PDO::PARAM_STR;

    /**
     * @var \PDO
     */
    protected $connection;

    /**
     * @var array
     */
    protected $attributes = array();

    /**
     * Model constructor.
     *
     * @param array $attributes
     */
    public function __construct($attributes = array())
    {
        $this->connection = Connection::getInstance()->getConnection();
        $this->fill($attributes);
    }

    /**
     * @param Model|null $model
     * @param string     $alias
     *
     * @return Builder
     */
    public static function getBuilder(Model $model = null, $alias = null)
    {
        $model = $model ?: new static;
        $builder = new Builder($model->getConnection(), get_class($model));

        return $builder->from($model->getTable() . ' ' . $alias)
            ->columns($model->getColumns());
    }

    /**
     * @param string|int $id
     *
     * @return Model
     */
    public static function find($id)
    {
        $model = new static;

        return static::getBuilder($model)
            ->where($model->getPrimary(), '=', $model->getPrimaryType(), $id)
            ->first();
    }

    /**
     * @param array $ids
     *
     * @return Model[]|array
     */
    public static function findMany(array $ids)
    {
        $model = new static;

        return empty($ids)
            ? array()
            : static::getBuilder($model)
                ->whereIn($model->getPrimary(), $model->getPrimaryType(), $ids)
                ->get();
    }

    /**
     * @param Model|Model[] $models
     * @param string        $relation
     * @param array         $parameters
     * @param bool          $many
     *
     * @return Model|Model[]
     */
    public static function eager($models, $relation, array $parameters, $many = false)
    {
        /** @var Model $relatedModel */
        list($primaryKey, $foreignKey, $type, $relatedModel, $columns) = $parameters;

        $models = is_array($models) ? $models : array($models);
        $models = array_filter($models);
        if (empty($models)) {
            return array();
        }

        $bindings = array();
        foreach ($models as $model) {
            $bindings[$model->$primaryKey] = $model;
            $model->$relation = array();
        }

        $columns = self::modifyRelatedColumns(
            empty($columns) ? $relatedModel->getColumns() : $columns,
            $foreignKey
        );

        $records = static::getBuilder($relatedModel)->columns($columns)
            ->whereIn($foreignKey, $type, array_keys($bindings))
            ->get();

        foreach ($records as $record) {
            foreach($models as $model) {
                $value = $model->$relation;
                if ($model->$primaryKey === $record->$foreignKey) {
                    $value[] = $record;
                    $model->$relation = $value;
                }
            }

            // $value = $bindings[$record->$foreignKey]->$relation;
            // $value[] = $record;
            // $bindings[$record->$foreignKey]->$relation = $value;
        }

        if (! $many) {
            foreach ($models as $model) {
                $model->$relation = current($model->$relation) ?: null;
            }
        }

        return $many ? $records : current($records) ?: null;
    }

    /**
     * @param $attributes
     *
     * @return Model
     */
    public function fill($attributes)
    {
        foreach ($attributes as $attribute => &$value) {
            $this->$attribute = $value;
        }

        return $this;
    }

    /**
     * Поля для выборки.
     *
     * @return array
     */
    public function getColumns()
    {
        return array('*');
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return string
     */
    public function getPrimary()
    {
        return $this->primary;
    }

    /**
     * @return int
     */
    public function getPrimaryType()
    {
        return $this->primaryType;
    }

    /**
     * @param $columns
     * @param $foreignKey
     *
     * @return array
     */
    private static function modifyRelatedColumns($columns, $foreignKey)
    {
        if (! empty($columns)) {
            if (! in_array($foreignKey, $columns)) {
                $columns[] = $foreignKey;
            }
        } else {
            $columns = array('*');
        }

        return $columns;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    public function __get($key)
    {
        return array_key_exists($key, $this->attributes)
            ? $this->attributes[$key]
            : null;
    }

    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
    }
}
