<?php

namespace App\Models;

class LicenseSupplement extends Model
{
    protected $table = 'eiis_LicenseSupplements';

    /**
     * @return array
     */
    public function getColumns()
    {
        return array(
            'Id',
            'SerDoc',
            'NumDoc',
            'fk_eiisLicenseState',
            'fk_eiisLicenseSupplementState',
            'DateEnd',
            'NumLicDoc',
            'DateLicDoc',
            'fk_eiisLicense',
        );
    }

    /**
     * @return array
     */
    public function toXml()
    {
        return array(
            'LicenseRegNum' => $this->parent ? $this->parent->LicenseRegNum : null,
            'SerDoc' => $this->SerDoc,
            'NumDoc' => $this->NumDoc,
            'fk_eiisLicenseState' => $this->fk_eiisLicenseSupplementState,
            'IsTermless' => 0,
            'DateEnd' => $this->DateEnd,
            'NumLicDoc' => $this->NumLicDoc,
            'DateLicDoc' => $this->DateLicDoc,
            'fk_eiisLicense' => $this->fk_eiisLicense,
        );
    }

    /**
     * @param Model|Model[] $licenseSupplements
     *
     * @return Model|Model[]
     */
    public static function loadParents($licenseSupplements)
    {
        return self::eager($licenseSupplements, 'parent', array(
            'fk_eiisLicense',
            'Id',
            \PDO::PARAM_STR,
            new License,
            array('Id', 'LicenseRegNum'),
        ));
    }

    /**
     * @return string
     */
    public function getElementName()
    {
        return 'license';
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return 'licenses';
    }

    /**
     * @return array
     */
    public function toXmlAttributes()
    {
        return array(
            'Id' => $this->Id,
        );
    }
}
