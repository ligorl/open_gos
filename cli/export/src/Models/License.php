<?php

namespace App\Models;

class License extends Model
{
    protected $table = 'eiis_Licenses';

    /**
     * @return array
     */
    public function getColumns()
    {
        return array(
            'Id',
            'LicenseRegNum',
            'SerDoc',
            'NumDoc',
            'fk_eiisLicenseState',
            'IsTermless',
            'DateEnd',
            'NumLicDoc',
            'DateLicDoc',
        );
    }

    /**
     * @return array
     */
    public function toXml()
    {
        return array(
            'LicenseRegNum' => $this->LicenseRegNum,
            'SerDoc' => $this->SerDoc,
            'NumDoc' => $this->NumDoc,
            'fk_eiisLicenseState' => $this->fk_eiisLicenseState,
            'IsTermless' => $this->IsTermless,
            'DateEnd' => $this->DateEnd,
            'NumLicDoc' => $this->NumLicDoc,
            'DateLicDoc' => $this->DateLicDoc,
        );
    }

    /**
     * @return string
     */
    public function getElementName()
    {
        return 'license';
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return 'licenses';
    }

    /**
     * @return array
     */
    public function toXmlAttributes()
    {
        return array(
            'Id' => $this->Id,
        );
    }
}
