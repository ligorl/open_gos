<?php

namespace App\Models;

class Founder extends Model
{
    protected $table = 'isga_Founders';

    /**
     * @return bool
     */
    public function isNameNotNull()
    {
        $fullName = trim($this->getFullName());

        return (mb_strpos($fullName, 'NULL') === false) && $fullName;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        $fullName = sprintf(
            '%s %s %s',
            $this->LastName,
            $this->FirstName,
            $this->Patronymic
        );

        return rtrim($fullName, ',.;');
    }

    /**
     * @param Model|Model[] $models
     *
     * @return $this|array
     */
    public static function loadForOrganizations($models)
    {
        $models = is_array($models) ? $models : array($models);
        $models = array_filter($models);
        if (empty($models)) {
            return array();
        }

        $bindings = array();
        foreach ($models as $model) {
            $bindings[$model->Id] = $model;
            $model->founders = array();
        }

        $records = static::getBuilder(null, 'f')->columns(array(
            'f.Id',
            'f.FirstName',
            'f.LastName',
            'f.Patronymic',
        ))
            ->join(
                'isga_mtm_Founders_EducationalOrganizations mfe',
                'mfe.fk_isgaFounder = f.Id',
                array(
                    'mfe.fk_eiisEducationalOrganization as eoId',
                ), // cols
                array( // wheres
                    array( //where
                        'fk_eiisEducationalOrganization',
                        'in',
                        \PDO::PARAM_STR,
                        array_keys($bindings),
                        'and',
                    ),
                )
            )->get();

        /** @var Founder $record */
        foreach ($records as $record) {
            if (! $record->isNameNotNull()) {
                continue;
            }

            $value = $bindings[$record->eoId]->founders;
            $value[] = $record;
            $bindings[$record->eoId]->founders = $value;
        }

        return $records;
    }

    /**
     * @param $founders
     *
     * @return string|null
     */
    public static function joinFoundersNames($founders)
    {
        $founders = is_array($founders) ? $founders : array($founders);
        $founders = array_filter($founders);
        if (empty($founders)) {
            return;
        }

        $foundersNames = array_map(function ($founder) {
            return $founder->getFullName();
        }, $founders);

        return implode(';', $foundersNames);
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return 'founders';
    }

    /**
     * @return string
     */
    public function getElementName()
    {
        return 'founder';
    }

    /**
     * @return array
     */
    public function toXml()
    {
        return $this->attributes;
    }

    /**
     * @return array
     */
    public function toXmlAttributes()
    {
        return array();
    }
}
