<?php

namespace App\Models;

class Certificate extends Model
{
    protected $table = 'isga_Certificates';

    /**
     * @return string
     */
    public function getGroupName()
    {
        return 'accreditations';
    }

    /**
     * @return string
     */
    public function getElementName()
    {
        return 'accreditation';
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return array(
            'Id',
            'RegNumber',
            'FormNumber',
            'SerialNumber',
            'DateIssue',
            'DateEnd',
            'fk_isgaCertificateStatus',
            'fk_isgaCertificateType',
            'GUID',
        );
    }

    /**
     * @return array
     */
    public function toXml()
    {
        return array(
            'RegNumber' => $this->RegNumber,
            'FormNumber' => $this->FormNumber,
            'SerialNumber' => $this->SerialNumber,
            'IssueDate' => $this->DateIssue,
            'EndDate' => $this->DateEnd,
            'CertificateStatusId' => $this->fk_isgaCertificateStatus,
            'fk_isgaCertificateType' => $this->fk_isgaCertificateType,
        );
    }

    /**
     * @return array
     */
    public function toXmlAttributes()
    {
        return array(
            'Id' => $this->Id,
            'guid' => $this->GUID,
        );
    }
}
