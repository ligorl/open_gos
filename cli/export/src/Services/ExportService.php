<?php

namespace App\Services;

use App\Database\Builder;
use App\Models\Model;
use App\Models\XmlSerializable;
use Comodojo\Zip\Zip;
use Monolog\Logger;
use Monolog\Registry;
use XMLWriter;

class ExportService extends XMLWriter
{
    /**
     * @var string
     */
    protected $basePath;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Записали ли корневой элемент.
     *
     * @var bool
     */
    protected $started = false;

    /**
     * @var bool
     */
    protected $finished = false;

    /**
     * @var int
     */
    protected $total = 0;

    /**
     * @var string
     */
    protected $defaultRoot;

    /**
     * @var bool
     */
    protected $shouldZip;

    /**
     * ExportService constructor.
     *
     * @param string $basePath
     * @param bool   $shouldZip
     * @param string $defaultRoot
     * @param string $format
     */
    public function __construct($basePath = null, $shouldZip = true, $defaultRoot = 'root', $format = 'd-m-Y')
    {
        $this->defaultRoot = $defaultRoot;
        $this->shouldZip = $shouldZip;
        $this->basePath = $basePath ? $basePath : base_path('export');
        $this->path = $this->basePath . DIRECTORY_SEPARATOR . date($format) . '.xml';
        $this->logger = Registry::getInstance('export');
    }

    /**
     * @return void
     */
    protected function cleanUpDirectory()
    {
        // удаляем старый файлы
        array_map('unlink', glob($this->basePath . '/*') ?: array());
    }

    /**
     * @return ExportService
     */
    public function export()
    {
        $this->cleanUpDirectory();
        $this->logger->info('Export started.');
        $this->openMemory();
        $this->setIndent(true);
        $this->setIndentString('    ');
        $this->startDocument('1.0', 'UTF-8');
        $this->writeToFile(null);

        return $this;
    }

    /**
     * @param callable $callback
     * @param int      $limit
     * @param int      $offset
     * @param int      $max
     *
     * @return void
     */
    public function chunk($callback, $limit = 100, $offset = 0, $max = 0)
    {
        if ($max && $this->total >= $max) {
            $this->finish();

            return;
        }
        $models = $callback($limit, $offset);

        if (! empty($models)) {
            $this->writeBulk(array_filter($models));
            $models = null; // to save memory
            $this->chunk($callback, $limit, $offset + $limit, $max);
        } else {
            $this->writeBulk();
        }
    }

    /**
     * @param array $models
     *
     * @return ExportService
     */
    public function writeBulk(array $models = array())
    {
        $notExists = empty($models);
        $this->total += count($models);

        if ($notExists && $this->started) {
            return $this->finish();
        } elseif ($notExists && ! $this->started) {
            $this->startGroup($this->defaultRoot);

            return $this->finish();
        }

        if (! $this->started) {
            $this->startGroup(current($models) ?: $this->defaultRoot, true);
            $this->started = true;
        }

        $this->writeModels($models);
        $this->bulkLog();

        return $this;
    }

    protected function bulkLog()
    {
        $this->logger->info(
            sprintf('%d root models were written', $this->total)
        );
    }

    /**
     * @return ExportService
     */
    public function finish()
    {
        $this->endGroup(true);
        $this->finished = true;

        $this->logger->info(sprintf(
            'Memory: real peak usage %.2f Mb, peek usage %.2f Mb, file size %.2f Mb',
            memory_get_peak_usage(true) / 1024 / 1024,
            memory_get_peak_usage() / 1024 / 1024,
            filesize($this->path) / 1024 / 1024
        ));

        if ($this->shouldZip) {
            $this->zip();
        }
        $this->logger->info('Total queries: ' . Builder::$executedQueriesCount);
        $this->logger->info('Export finished.');
        $this->logger->info('------------------------------');

        return $this;
    }

    /**
     * @return void
     */
    protected function zip()
    {
        $path = preg_replace('/xml/', 'zip', $this->path);
        Zip::create($path)
            ->add($this->path)
            ->close();
        // удаляем старый файл
        @unlink($this->path);

        $this->logger->info(sprintf(
            'File was successfully zipped. Size %.2f Mb',
            filesize($path) / 1024 / 1024
        ));
    }

    /**
     * @param array $models
     * @param mixed $startGroup
     *
     * @return void
     */
    protected function writeModels(array $models, $startGroup = false)
    {
        if (empty($models)) {
            return;
        }

        if ($startGroup) {
            $this->startGroup(current($models));
        }

        /** @var Model $model */
        foreach ($models as $model) {
            $this->writeRecursive($model);
        }

        if ($startGroup) {
            $this->endGroup();
        }

        $this->writeToFile();
    }

    /**
     * @param Model $model
     *
     * @return void
     */
    protected function writeRecursive($model)
    {
        $this->startElement($model->getElementName());
        $this->writeAttributes($model->toXmlAttributes());
        $options = $model->toXml();

        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $this->writeModels($value, true);
            } elseif ($value instanceof Model) {
                $this->writeRecursive($value);
            } else {
                $this->writeElement($key, $value);
            }
        }

        $this->endElement();
    }

    /**
     * @param array $attributes
     */
    protected function writeAttributes(array $attributes)
    {
        if (empty($attributes)) {
            return;
        }

        foreach ($attributes as $key => $value) {
            $this->writeAttribute($key, $value);
        }
    }

    /**
     * @param int|null $append
     *
     * @return bool|int
     */
    protected function writeToFile($append = FILE_APPEND)
    {
        if (! file_exists($this->basePath)) {
            mkdir($this->basePath);
        }

        return file_put_contents($this->path, $this->flush(true), $append);
    }

    /**
     * @param Model|string $model
     * @param bool $isRoot
     *
     * @return void
     */
    private function startGroup($model, $isRoot = false)
    {
        $groupName = $model instanceof XmlSerializable
            ? $model->getGroupName()
            : $model;

        $success = $this->startElement($groupName);
        $this->writeToFile();

        if ($isRoot) {
            $this->logger->info(sprintf(
                'Root element "%s" was opened %s',
                $groupName,
                $success ? 'successfully' : 'with error'
            ));
        }
    }

    /**
     * @param bool $isFinished
     *
     * @return void
     */
    protected function endGroup($isFinished = false)
    {
        $success = $this->endElement();
        if ($isFinished) {
            $this->logger->info(sprintf(
                'Root element "%s" was closed %s',
                $this->defaultRoot,
                $success ? 'successfully' : 'with error'
            ));
        }
        $this->writeToFile();
    }
}
