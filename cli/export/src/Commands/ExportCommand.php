<?php

namespace App\Commands;

use App\Models\Founder;
use App\Models\Organization;
use App\Services\ExportService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCommand extends Command
{
    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('isga:export')
            ->setDescription('Экспорт данных из ИСГА')
            ->addOption(
                'force',
                null,
                InputOption::VALUE_NONE,
                'Экспортировать все данные что есть в базе.'
            );
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = (int) env('EXPORT_LIMIT', 0);
        $offset = (int) env('EXPORT_OFFSET', 0);
        $chunkSize = (int) env('EXPORT_CHUNK', 500);
        if ($limit > 0 && $chunkSize > $limit) {
            $chunkSize = $limit;
        }

        $path = env('EXPORT_DESTINATION', base_path('export'));
        $zippingEnabled = env('EXPORT_ZIPPING') === 'true';
        $onlyUpdated = env('EXPORT_UPDATED') === 'true';
        if ($input->getOption('force')) {
            $onlyUpdated = false;
        }

        // delete log file
        @array_map('unlink', glob(base_path('logs/*.log')));

        $service = new ExportService($path, $zippingEnabled, 'organizations');
        $service->export()->chunk(function ($limit, $offset) use ($onlyUpdated) {
            $builder = Organization::getBuilder()
                ->limit($limit)->offset($offset);

            if ($onlyUpdated) {
                $builder->where(
                    'updated_at',
                    '>=',
                    \PDO::PARAM_STR,
                    date('Y-m-d H:i:s', strtotime('yesterday'))
                );
            }

            $builder->where('lod_stateCode', '=', \PDO::PARAM_STR, 'VALID');
            // $builder->where('Id', '=', \PDO::PARAM_STR, '014A6BF9283418DD6DB3147DBE2BF1B1');

            $organizations = $builder->get();

            Founder::loadForOrganizations($organizations);
            Organization::loadCertificates($organizations);
            Organization::loadLicenses($organizations);
            Organization::loadPrograms($organizations);

            return $organizations;
        }, $chunkSize, $offset, $limit);

        return 0;
    }
}
