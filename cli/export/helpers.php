<?php

if (! function_exists('base_path')) {

    /**
     * Путь относительно корневой директории.
     *
     * @param string|null $path
     *
     * @return string
     */
    function base_path($path = null)
    {
        return __DIR__ . ($path ? DIRECTORY_SEPARATOR . $path : '');
    }
}

if (! function_exists('env')) {

    /**
     * Поулчаем значение переменной среды.
     *
     * @param string $key
     * @param mixed  $default
     *
     * @return mixed
     */
    function env($key, $default = null)
    {
        return getenv($key) ?: $default;
    }
}

if (! function_exists('is_date_valid')) {

    /**
     * @param  string $date
     *
     * @return string
     */
    function is_date_valid($date)
    {
        $invalidFormats = array('NULL', '', '0000-00-00');

        return $date && in_array($date, $invalidFormats);
    }
}


